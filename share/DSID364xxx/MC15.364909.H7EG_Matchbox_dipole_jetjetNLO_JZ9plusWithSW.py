evgenConfig.description = 'Herwig7 NLO dijets with MMHT2014 and dipole shower, slice JZ 9'
evgenConfig.keywords+=['SM','QCD','jets']
evgenConfig.contact = ['javier.llorente.merino@cern.ch']
evgenConfig.minevents = 5000

include("MC15JobOptions/Herwig7EG_Matchbox_MG_H7UEMMHT2014_dipole_multijet_withGridpack.py")

include("MC15JobOptions/JetFilterAkt6.py")

filtSeq.QCDTruthJetFilter.MinPt = 3200*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 7000*GeV
