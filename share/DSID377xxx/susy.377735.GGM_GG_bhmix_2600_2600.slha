#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.56400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.60000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05427760E+01   # W+
        25     1.25000000E+02   # h
        35     2.00397274E+03   # H
        36     2.00000000E+03   # A
        37     2.00152500E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013294E+03   # ~d_L
   2000001     5.00002541E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994919E+03   # ~u_R
   1000003     5.00013294E+03   # ~s_L
   2000003     5.00002541E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994919E+03   # ~c_R
   1000005     4.99903234E+03   # ~b_1
   2000005     5.00112722E+03   # ~b_2
   1000006     4.97581354E+03   # ~t_1
   2000006     5.02851443E+03   # ~t_2
   1000011     5.00008213E+03   # ~e_L
   2000011     5.00007622E+03   # ~e_R
   1000012     4.99984165E+03   # ~nu_eL
   1000013     5.00008213E+03   # ~mu_L
   2000013     5.00007622E+03   # ~mu_R
   1000014     4.99984165E+03   # ~nu_muL
   1000015     4.99938560E+03   # ~tau_1
   2000015     5.00077328E+03   # ~tau_2
   1000016     4.99984165E+03   # ~nu_tauL
   1000021     2.60000000E+03   # ~g
   1000022     2.53044618E+03   # ~chi_10
   1000023    -2.60005772E+03   # ~chi_20
   1000025     2.61892150E+03   # ~chi_30
   1000035     3.01469003E+03   # ~chi_40
   1000024     2.58553943E+03   # ~chi_1+
   1000037     3.01454646E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.88695303E-01   # N_11
  1  2    -1.00107283E-01   # N_12
  1  3     4.30077610E-01   # N_13
  1  4    -4.27751681E-01   # N_14
  2  1     1.69105538E-03   # N_21
  2  2    -2.76985034E-03   # N_22
  2  3    -7.07063815E-01   # N_23
  2  4    -7.07142298E-01   # N_24
  3  1     6.14517562E-01   # N_31
  3  2     1.57312096E-01   # N_32
  3  3    -5.46239136E-01   # N_33
  3  4     5.47031879E-01   # N_34
  4  1     1.80284722E-02   # N_41
  4  2    -9.82458022E-01   # N_42
  4  3    -1.29293528E-01   # N_43
  4  4     1.33170543E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.81144686E-01   # U_11
  1  2     9.83456457E-01   # U_12
  2  1     9.83456457E-01   # U_21
  2  2     1.81144686E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.86588452E-01   # V_11
  1  2     9.82438166E-01   # V_12
  2  1     9.82438166E-01   # V_21
  2  2     1.86588452E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07487028E-01   # cos(theta_t)
  1  2     7.06726330E-01   # sin(theta_t)
  2  1    -7.06726330E-01   # -sin(theta_t)
  2  2     7.07487028E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.88719817E-01   # cos(theta_b)
  1  2     7.25027595E-01   # sin(theta_b)
  2  1    -7.25027595E-01   # -sin(theta_b)
  2  2     6.88719817E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05600202E-01   # cos(theta_tau)
  1  2     7.08610157E-01   # sin(theta_tau)
  2  1    -7.08610157E-01   # -sin(theta_tau)
  2  2     7.05600202E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90185140E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.60000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52457799E+02   # vev(Q)              
         4     2.65649032E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52662052E-01   # gprime(Q) DRbar
     2     6.26409909E-01   # g(Q) DRbar
     3     1.07628202E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02392972E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.70873597E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79666441E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.56400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.50316683E+06   # M^2_Hd              
        22    -1.15507449E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36707073E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.36795097E-04   # gluino decays
#          BR         NDA      ID1       ID2
     3.82710436E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.30181219E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.44404963E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.30181219E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.44404963E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.12200182E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     4.25033875E-11    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.25033875E-11    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.25033875E-11    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.25033875E-11    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.20548282E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.02415668E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.93464170E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.65211376E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.00908495E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.08323117E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.77697917E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.67707121E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.10146993E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.10019655E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.78934938E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.32320479E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.04996344E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     6.64046766E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.71919403E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.63776242E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.72717554E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.32176716E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.98294185E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.42403108E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.96551940E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.98483746E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.28067025E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.30874101E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.77380368E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.64389239E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.33348554E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.77874593E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.28164356E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.13491905E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.96390359E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.08546650E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67032560E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.43223186E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.61269221E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.41685789E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.39298928E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.21656450E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.84739999E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.58819100E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.49222734E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.44898039E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.08338809E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.41539303E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.39520458E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61346762E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67042955E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.84276989E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.72487975E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.04776995E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.45434360E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.97433740E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.86659199E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.58868187E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.44902260E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.30522929E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.78933272E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.64412704E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.41895465E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90048197E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67032560E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.43223186E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.61269221E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.41685789E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.39298928E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.21656450E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.84739999E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.58819100E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.49222734E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.44898039E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.08338809E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.41539303E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.39520458E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61346762E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67042955E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.84276989E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.72487975E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.04776995E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.45434360E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.97433740E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.86659199E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.58868187E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.44902260E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.30522929E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.78933272E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.64412704E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.41895465E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90048197E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.73370713E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.67266741E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.25574870E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.52193373E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73411522E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.51459070E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59495304E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.34404208E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.33676173E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.80234882E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.66078629E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.42395428E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.73370713E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.67266741E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.25574870E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.52193373E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73411522E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.51459070E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59495304E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.34404208E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.33676173E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.80234882E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.66078629E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.42395428E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.04270539E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.45665410E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.65863685E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.82562379E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.80161068E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.23967338E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.68672525E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.76020742E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.04053120E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.34371691E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     4.74806263E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.86432770E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.86122753E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.20807547E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.80517225E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.73415770E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16934235E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.26276033E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.33757735E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.84855205E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.66724353E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.58157088E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.73415770E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16934235E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.26276033E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.33757735E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.84855205E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.66724353E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.58157088E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.73586585E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.16861227E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.25947451E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.33674223E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.84677355E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.72609101E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.57827826E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.73780377E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.83210795E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     4.43561347E-03    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     2.71225869E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.70359451E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     9.04086920E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     9.04070715E-02    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     8.99525088E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.81540094E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.62809589E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.16271421E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.43424683E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.75617837E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.36505242E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.54810704E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.57969629E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98506999E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88962331E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     8.16244701E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
     4.19168495E-03    2     1000039        35   # BR(~chi_10 -> ~G        H)
     1.76538319E-04    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     9.12508059E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.90857748E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.97331633E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.87208150E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.14765355E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
     7.65599507E-05    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.00331123E-03    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.08726949E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.40802990E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.08311893E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.40793031E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34381574E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.21655596E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.21648596E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.19682338E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.42418874E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.42418874E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.42418874E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.54485337E-04    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.54485337E-04    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.42885777E-04    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.42885777E-04    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.48284599E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.48284599E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.48059928E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.48059928E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.88305290E-05    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.88305290E-05    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
     1.96619354E-27    3     1000021        -2         2   # BR(~chi_20 -> ~g      ub      u)
     2.56049443E-27    3     1000021        -1         1   # BR(~chi_20 -> ~g      db      d)
     1.96619354E-27    3     1000021        -4         4   # BR(~chi_20 -> ~g      cb      c)
     2.56049443E-27    3     1000021        -3         3   # BR(~chi_20 -> ~g      sb      s)
#
#         PDG            Width
DECAY   1000025     1.68579181E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.03266723E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.33420696E-07    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.15817351E-01    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.63272722E-01    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     9.44282213E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
     7.13906546E-03    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.89725576E-04    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     9.31689163E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.16799555E-04    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.09264293E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.18970217E-04    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.32364043E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.32808304E-05    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.32805720E-05    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.64335639E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.43071190E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.43071190E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.43071190E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     9.42920506E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.22109653E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     9.09318907E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.22006830E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     6.48277948E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.78951963E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.78904707E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.65151439E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     5.57128079E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     5.57128079E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     5.57128079E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     6.60873871E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     6.60873871E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     6.55005246E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     6.55005246E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.20291002E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.20291002E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.20279966E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.20279966E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.17210641E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.17210641E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     4.52592471E-09    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     8.66264079E-10    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     4.52592471E-09    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     8.66264079E-10    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     2.95935449E-10    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     1.81330295E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.33425179E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51102339E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.66770629E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.54966461E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.54966461E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.54764249E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.01811187E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.63362592E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.43713314E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.20470279E-05    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.17513006E-08    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.52775415E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     8.82576731E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.09136581E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18347705E-01    2           5        -5   # BR(h -> b       bb     )
     6.36568956E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25321966E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77478033E-04    2           3        -3   # BR(h -> s       sb     )
     2.06009171E-02    2           4        -4   # BR(h -> c       cb     )
     6.68884072E-02    2          21        21   # BR(h -> g       g      )
     2.29926668E-03    2          22        22   # BR(h -> gam     gam    )
     1.53545886E-03    2          22        23   # BR(h -> Z       gam    )
     2.00386955E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55815939E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78067495E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46102499E-03    2           5        -5   # BR(H -> b       bb     )
     2.46450570E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71294519E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11557869E-06    2           3        -3   # BR(H -> s       sb     )
     1.00670447E-05    2           4        -4   # BR(H -> c       cb     )
     9.96086480E-01    2           6        -6   # BR(H -> t       tb     )
     7.97623394E-04    2          21        21   # BR(H -> g       g      )
     2.71844656E-06    2          22        22   # BR(H -> gam     gam    )
     1.16062509E-06    2          23        22   # BR(H -> Z       gam    )
     3.29520472E-04    2          24       -24   # BR(H -> W+      W-     )
     1.64310896E-04    2          23        23   # BR(H -> Z       Z      )
     8.98656188E-04    2          25        25   # BR(H -> h       h      )
     6.94031767E-24    2          36        36   # BR(H -> A       A      )
     2.82448769E-11    2          23        36   # BR(H -> Z       A      )
     6.11051065E-12    2          24       -37   # BR(H -> W+      H-     )
     6.11051065E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82376549E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46388952E-03    2           5        -5   # BR(A -> b       bb     )
     2.43901369E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62279449E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13679097E-06    2           3        -3   # BR(A -> s       sb     )
     9.96191387E-06    2           4        -4   # BR(A -> c       cb     )
     9.97010908E-01    2           6        -6   # BR(A -> t       tb     )
     9.43689766E-04    2          21        21   # BR(A -> g       g      )
     3.12177998E-06    2          22        22   # BR(A -> gam     gam    )
     1.35318720E-06    2          23        22   # BR(A -> Z       gam    )
     3.21174940E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74468213E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34175891E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49242187E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81161167E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48873694E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45703794E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08733550E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99407418E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.29174103E-04    2          24        25   # BR(H+ -> W+      h      )
     2.89658620E-13    2          24        36   # BR(H+ -> W+      A      )
