# based on MC15.361593...
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 Diboson WZ->qqll production mllmin20'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WZ', '2lepton', '2jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'christian.johnson@cern.ch','carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'orel.gueta@cern.ch' ]
evgenConfig.minevents   = 5000
evgenConfig.inputfilecheck = "TXT"
evgenConfig.generators += ["Powheg"]

# Herwig 7 showering                                                                                                                                                    
#--------------------------------------------------------------                                                                                                  
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_EvtGen_Common.py')
#--------------------------------------------------------------                                                 
