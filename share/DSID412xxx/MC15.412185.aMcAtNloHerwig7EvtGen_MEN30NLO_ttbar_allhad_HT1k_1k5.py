#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

# Provide config information
evgenConfig.generators  += ["aMcAtNlo", "Herwig7"]
evgenConfig.tune         = "H7.1-Default"
evgenConfig.description  = 'MG5_aMC@NLO+Herwig7+EvtGen, H7p1 default tune, ME NNPDF 3.0 NLO, with zero lepton filter from DSID 412175 LHE files, filtered on HT'
evgenConfig.keywords     = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact      = ['spalazzo@cern.ch']
evgenConfig.minevents    = 500
evgenConfig.inputFilesPerJob = 10



# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

#include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

## NonAllHad filter
#include('MC15JobOptions/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = 0
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
#filtSeq.Expression="TTbarWToLeptonFilter"

#this is from DSID 410435 
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0


# Configure the HT filter
include('MC15JobOptions/HTFilter.py')
filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 1000.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 1500.*GeV # Max HT to keep event
# EvtGen changes the B decays, which can cause inconsistencies
# between generator-level and DAOD-level filtering
filtSeq.HTFilter.UseNeutrinosFromWZTau = True # Include nu from the MC event in the HT
# These should be irrelevant for the sample, but kept for consistency
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

filtSeq.Expression = "((not TTbarWToLeptonFilter) and HTFilter)"
