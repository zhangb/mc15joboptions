#--------------------------------------------------------------
# General settings
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG-BOX-RES/OpenLoops+Pythia8+EvtGen ttbb (4FS), ME NNPDF30_nlo_as_0118_nf_4 PDF, mur=[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], hdamp=HT/2, A14 NNPDF23 LO tune, single-lepton negative charge channel, decays with MadSpin, pTdef is set 1, starts from LHE 411180'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'bbbar', '1lepton' ]
evgenConfig.contact     = [ 'tpelzer@cern.ch', 'nedaa.asbah@cern.ch' ]
evgenConfig.minevents   = 500


#--------------------------------------------------------------
# Pythia8
#--------------------------------------------------------------
### standard Pythia8 A14 tune + EvtGen
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
### main31 routine for Powheg matching, with Powheg:NFinal = 4 for ttbb ME
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 4' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
