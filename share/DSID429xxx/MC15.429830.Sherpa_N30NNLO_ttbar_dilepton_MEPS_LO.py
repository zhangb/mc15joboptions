include("Sherpa_i/2.2.6_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.6 ttbar production with tt+0,1j@NLO+2,3,4j@LO in the dlepton channel."
evgenConfig.keywords = ["SM", "top", "ttbar", "2lepton" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "ttbar" # universal for all decay channels

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE QCD;
  EXCLUSIVE_CLUSTER_MODE 1;

  %tags for process setup
  NJET:=3; LJET:=0; QCUT:=30.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  INTEGRATION_ERROR=0.05;

  %decay settings
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  HDH_STATUS[24,12,-11]=2
  HDH_STATUS[24,14,-13]=2
  HDH_STATUS[24,16,-15]=2
  HDH_STATUS[-24,-12,11]=2
  HDH_STATUS[-24,-14,13]=2
  HDH_STATUS[-24,-16,15]=2
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;

  % alphas scale factor 1 for better high jet multi
  CSS_IS_AS_FAC=1.0
}(run)

(processes){
  Process : 93 93 ->  6 -6 93{NJET};
  NLO_QCD_Mode 3 {LJET}; CKKW sqr(QCUT/E_CMS);
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN;
  Order (*,0);
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptt", "ppttj" ]

