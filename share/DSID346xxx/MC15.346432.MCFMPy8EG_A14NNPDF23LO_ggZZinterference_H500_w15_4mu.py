include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.generators = ["MCFM", "Pythia8", "EvtGen"]
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.ch>']
evgenConfig.description = "gg -> ZZ -> 4mu interference between SM bkg and a heavy Higgs (500 GeV with 15% width)"
evgenConfig.keywords = ["BSMHiggs", "4lepton"]

include("MC15JobOptions/Pythia8_LHEF.py")

evgenConfig.inputfilecheck = 'ggZZinterference'
