include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:addChannel = 1 0.002 100 313 22',
                             '313:mMax = 5'
                             ]

evgenConfig.process        = 'ttH H->KStar0Gamma'
evgenConfig.description    = 'aMcAtNloPythia8 ttH semilep, H to KStar0'
evgenConfig.keywords       = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs','photon' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.contact = ["Rhys Owen <rhys.owen@cern.ch>"]
