include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Electroweak lllvjj + 0j@LO, QSF*0.5 variation."
evgenConfig.keywords = ["SM", "diboson", "3lepton", "jets", "VBS"]
evgenConfig.contact  = ["chris.g@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.minevents = 100
evgenConfig.inputconfcheck = "222_NNPDF30NNLO_lllvjj_EW6_QSF025"

Sherpa_iRunCard="""
(run){
  ACTIVE[25]=1;
  MASS[25]=126.0;
  WIDTH[25]=0.00418;
  MASSIVE[5]=1; 

  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  % simplified setup as long as only 2->6 taken into account:
  %SCALES=VAR{FSF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSF*Abs2(p[2]+p[3]+p[4]+p[5])};

  %tags for process setup
  NJET:=0; QCUT:=20.;

  EXCLUSIVE_CLUSTER_MODE=1
  SOFT_SPIN_CORRELATIONS=1

  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.99;
}(run)

(processes){
  Process 93 93 -> 90 90 90 91 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

Sherpa_iNCores = 96
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

