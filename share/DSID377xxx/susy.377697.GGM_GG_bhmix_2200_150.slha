#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.02000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05377087E+01   # W+
        25     1.25000000E+02   # h
        35     2.00416739E+03   # H
        36     2.00000000E+03   # A
        37     2.00209611E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013345E+03   # ~d_L
   2000001     5.00002534E+03   # ~d_R
   1000002     4.99989189E+03   # ~u_L
   2000002     4.99994933E+03   # ~u_R
   1000003     5.00013345E+03   # ~s_L
   2000003     5.00002534E+03   # ~s_R
   1000004     4.99989189E+03   # ~c_L
   2000004     4.99994933E+03   # ~c_R
   1000005     4.99999897E+03   # ~b_1
   2000005     5.00016126E+03   # ~b_2
   1000006     5.00071433E+03   # ~t_1
   2000006     5.00375708E+03   # ~t_2
   1000011     5.00008278E+03   # ~e_L
   2000011     5.00007601E+03   # ~e_R
   1000012     4.99984121E+03   # ~nu_eL
   1000013     5.00008278E+03   # ~mu_L
   2000013     5.00007601E+03   # ~mu_R
   1000014     4.99984121E+03   # ~nu_muL
   1000015     5.00003958E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984121E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     1.36439714E+02   # ~chi_10
   1000023    -1.50249833E+02   # ~chi_20
   1000025     3.13615600E+02   # ~chi_30
   1000035     3.00219452E+03   # ~chi_40
   1000024     1.47959302E+02   # ~chi_1+
   1000037     3.00219407E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     2.54065891E-01   # N_11
  1  2    -2.61869866E-02   # N_12
  1  3     6.90095710E-01   # N_13
  1  4    -6.77150409E-01   # N_14
  2  1     1.96740450E-02   # N_21
  2  2    -5.03519628E-03   # N_22
  2  3    -7.04013607E-01   # N_23
  2  4    -7.09896063E-01   # N_24
  3  1     9.66986684E-01   # N_31
  3  2     7.45389563E-03   # N_32
  3  3    -1.66984508E-01   # N_33
  3  4     1.92346994E-01   # N_34
  4  1     4.55709767E-04   # N_41
  4  2    -9.99616590E-01   # N_42
  4  3    -1.57774147E-02   # N_43
  4  4     2.27494515E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.23043582E-02   # U_11
  1  2     9.99751227E-01   # U_12
  2  1     9.99751227E-01   # U_21
  2  2     2.23043582E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.21684162E-02   # V_11
  1  2     9.99482463E-01   # V_12
  2  1     9.99482463E-01   # V_21
  2  2     3.21684162E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13747117E-01   # cos(theta_t)
  1  2     7.00403493E-01   # sin(theta_t)
  2  1    -7.00403493E-01   # -sin(theta_t)
  2  2     7.13747117E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08558278E-01   # cos(theta_b)
  1  2     9.12732235E-01   # sin(theta_b)
  2  1    -9.12732235E-01   # -sin(theta_b)
  2  2     4.08558278E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76619448E-01   # cos(theta_tau)
  1  2     7.36332888E-01   # sin(theta_tau)
  2  1    -7.36332888E-01   # -sin(theta_tau)
  2  2     6.76619448E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90212166E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51759257E+02   # vev(Q)              
         4     3.87654560E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146750E-01   # gprime(Q) DRbar
     2     6.29570746E-01   # g(Q) DRbar
     3     1.07892605E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02738527E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72385704E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79964778E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.02000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04064596E+06   # M^2_Hd              
        22    -5.40760590E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111940E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.39544198E-03   # gluino decays
#          BR         NDA      ID1       ID2
     5.46479133E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.02761142E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.16189771E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.97104505E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.53100626E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.40797578E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.25727751E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.92889462E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.87831017E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.97104505E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.53100626E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.40797578E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.25727751E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.92889462E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.87831017E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.56347335E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.75445872E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.42094015E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.99441353E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.20848735E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.71477738E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     5.24720595E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.24720595E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.24720595E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.24720595E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.32129053E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.32129053E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.99642272E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.26003610E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.23484569E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.17231511E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.35910622E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.06159588E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.70929398E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.91945080E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.97262923E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.84369547E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20715875E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.14569423E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.27094810E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.36631435E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.53567351E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.64692578E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     9.69547488E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.41142483E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.67713571E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.79893821E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -4.81501420E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -6.74220803E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -9.66897032E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.77929213E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.47658205E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.33041607E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.95719527E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.88107654E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.58858246E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.95932047E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.20103307E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     4.97470261E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96840795E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     4.37037255E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.76937810E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.38847351E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.96416009E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.98990408E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.92754680E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.77451586E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.83253382E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.72992860E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.23602436E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.37158127E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.95122975E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.42531894E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96845714E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.22260619E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.30042223E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.87314143E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.96603327E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.56671415E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.93285360E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.77512762E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.75360152E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.74469712E-04    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.84176874E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.40336310E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.29358673E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.84986056E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96840795E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     4.37037255E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.76937810E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.38847351E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.96416009E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.98990408E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.92754680E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.77451586E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.83253382E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.72992860E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.23602436E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.37158127E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.95122975E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.42531894E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96845714E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.22260619E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.30042223E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.87314143E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.96603327E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.56671415E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.93285360E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.77512762E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.75360152E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.74469712E-04    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.84176874E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.40336310E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.29358673E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.84986056E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03671804E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.77152097E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.33329189E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.94740109E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65135283E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.44778427E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30684976E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46265796E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.49364893E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.89265337E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.34674160E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.55616657E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03671804E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.77152097E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.33329189E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.94740109E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65135283E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.44778427E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30684976E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46265796E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.49364893E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.89265337E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.34674160E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.55616657E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.73305802E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     4.01243862E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57088440E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.53162088E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.34677851E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.88031321E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.69566627E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31645120E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.77632387E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.87985647E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06903860E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.97603302E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57423982E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.20306757E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.15093082E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03657087E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.84480035E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.67364593E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.84323717E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65377747E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.34119025E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30341978E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03657087E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.84480035E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.67364593E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.84323717E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65377747E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.34119025E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30341978E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.03978603E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.84284912E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.67187572E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.84128759E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65097059E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.39712576E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29781378E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.85972311E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.67980361E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.45729508E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.21135737E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.15243218E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.15194699E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.02640041E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.13821144E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.04168543E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.82265290E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.99686579E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.65621299E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.04459732E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.81078902E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.83580462E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.59818476E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.79283744E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     2.47264857E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     8.91947447E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.81894191E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.97606477E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     5.02272314E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.21208543E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     3.49631282E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.17680710E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     5.44526116E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.11964420E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.09124485E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.32287333E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.71208889E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.23568944E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.70940906E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.24512200E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.90634329E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.90513674E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.54628925E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.79854218E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.79854218E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.79854218E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.62890696E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.62890696E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.52620279E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.52620279E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.20963611E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.20963611E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.18639232E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.18639232E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.84814031E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.84814031E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     7.13474356E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.99318470E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.40099073E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.80952936E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.80952936E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.95518811E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.83054678E-04    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51217600E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.58243954E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.47942564E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.13824851E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.45026947E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.98108160E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.88835682E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.99052844E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.99052844E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.81862189E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.58449362E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.33876199E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.59842024E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.53059333E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.52757982E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.62309888E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.81407656E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.39599903E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.82833811E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.82833811E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.13630261E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.78242635E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.54998094E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.80548089E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.10697870E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08284212E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17372025E-01    2           5        -5   # BR(h -> b       bb     )
     6.37949388E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25810588E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78512743E-04    2           3        -3   # BR(h -> s       sb     )
     2.06431779E-02    2           4        -4   # BR(h -> c       cb     )
     6.70297134E-02    2          21        21   # BR(h -> g       g      )
     2.32878927E-03    2          22        22   # BR(h -> gam     gam    )
     1.53838820E-03    2          22        23   # BR(h -> Z       gam    )
     2.00953647E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56349971E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01390032E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38954702E-03    2           5        -5   # BR(H -> b       bb     )
     2.32144844E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20718450E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05080775E-06    2           3        -3   # BR(H -> s       sb     )
     9.48366361E-06    2           4        -4   # BR(H -> c       cb     )
     9.38368734E-01    2           6        -6   # BR(H -> t       tb     )
     7.51438514E-04    2          21        21   # BR(H -> g       g      )
     2.63036836E-06    2          22        22   # BR(H -> gam     gam    )
     1.09181939E-06    2          23        22   # BR(H -> Z       gam    )
     3.18201995E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58666983E-04    2          23        23   # BR(H -> Z       Z      )
     8.52897748E-04    2          25        25   # BR(H -> h       h      )
     8.45478616E-24    2          36        36   # BR(H -> A       A      )
     3.37869376E-11    2          23        36   # BR(H -> Z       A      )
     2.49749856E-12    2          24       -37   # BR(H -> W+      H-     )
     2.49749856E-12    2         -24        37   # BR(H -> W-      H+     )
     8.42569735E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.14129523E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.45773103E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.36264661E-05    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     5.04257078E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.74284609E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.04412839E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05855540E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39244209E-03    2           5        -5   # BR(A -> b       bb     )
     2.29791526E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12396055E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07102691E-06    2           3        -3   # BR(A -> s       sb     )
     9.38561106E-06    2           4        -4   # BR(A -> c       cb     )
     9.39333217E-01    2           6        -6   # BR(A -> t       tb     )
     8.89096736E-04    2          21        21   # BR(A -> g       g      )
     2.69488930E-06    2          22        22   # BR(A -> gam     gam    )
     1.27329595E-06    2          23        22   # BR(A -> Z       gam    )
     3.10135349E-04    2          23        25   # BR(A -> Z       h      )
     6.35193588E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     9.73060063E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.66493165E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     6.15454956E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     5.35247023E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.92913178E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.96122845E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97804105E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23439575E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34688147E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29707376E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42060811E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13818577E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02380288E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41035542E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17861061E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33735376E-12    2          24        36   # BR(H+ -> W+      A      )
     3.76589270E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.33940965E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.44987076E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
