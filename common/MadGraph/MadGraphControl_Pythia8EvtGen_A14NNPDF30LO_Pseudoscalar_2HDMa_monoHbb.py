from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat', 'w')

# This control file makes use of UFO v2, registered in https://its.cern.ch/jira/browse/AGENE-1652 

# Z's don't contribute because sinbma (sin(beta-alpha)) is 1.0, so excluding them with '/z' reduces runtime for the LHE generation,
# without changing the physics/cross-sections/kinematic ditributions

# Process definition
if initialGluons:
    # For the gluon-gluon fusion production use the 4FS to take into account the b-quark loops
    fcard.write("""
    import model Pseudoscalar_2HDM -modelname
    define p = g d u s c d~ u~ s~ c~
    define j = g d u s c d~ u~ s~ c~
    """)
    fcard.write("generate g g > h1 xd xd~ / z [QCD]\n")
else:
    # For b-initiated production use 5FS
    fcard.write("""
    import model Pseudoscalar_2HDM-bbMET_5FS -modelname
    define p = g d u s c b d~ u~ s~ c~ b~
    define j = g d u s c b d~ u~ s~ c~ b~
    """)    
    fcard.write("generate p p > h1 xd xd~ / z\n")
fcard.write("""
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

process_dir = new_process()

# Build run_card
extras = {}
if initialGluons:
    extras = {
        'pdlabel'       : "'lhapdf'",
        'lhaid'         : '260000',
        'maxjetflavor'  : 4,
        'asrwgtflavor'  : 4,
        'lhe_version': '3.0',
        'cut_decays': 'F',
        'use_syst': 'True',
	'sys_scalefact': '0.5 1 2',
	'sys_pdf': 'NNPDF30_nlo_as_0118'
    }
else:
    extras = {
        'pdlabel'       : "'lhapdf'",
        'lhaid'         : '260000',
        'maxjetflavor'  : 5,
        'asrwgtflavor'  : 5,
        'lhe_version': '3.0',
        'cut_decays': 'F',
        'use_syst': 'True',
	'sys_scalefact': '0.5 1 2',
	'sys_pdf': 'NNPDF30_nlo_as_0118'
    }

qcut = 0 # for consistency with iCKKW=0

# Number of events to generate
nevents=5000
if runArgs.maxEvents>0:
    nevents = runArgs.maxEvents * multiplier
else:
    nevents *= multiplier
    
# Run card
build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat', xqcut = qcut,
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()

# Build param_card.dat
paramcard = subprocess.Popen(['get_files','-data','MadGraph_param_card_Pseudoscalar2HDM.dat'])
paramcard.wait()
if not os.access('MadGraph_param_card_Pseudoscalar2HDM.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_param_card_Pseudoscalar2HDM.dat','r')
    newcard = open('param_card.dat','w')
    import re
    THDM_regexp = re.compile('\s+([0-9]+)\s+([0-9+-.e]+)\s+#\s+(\w+)\s*')
    for line in oldcard:
        isTHDMparam = False
        for param_name, newvalue in THDMparams.items():
          if param_name in line and "NUMBERS" not in line: # protect against picking up lines with "NUMBERS" when changing bottom quark mass "MB"
            THDM_match = THDM_regexp.match(line.rstrip('\n'))
            if THDM_match:
              THDM_pdgID = int(THDM_match.group(1))
              THDM_oldvalue = float(THDM_match.group(2))
              THDM_param_name = str(THDM_match.group(3))
              if THDM_param_name != param_name:
                print param_name, THDM_param_name
                raise RuntimeError('Mismatching parameter names, please double-check logic')
              newcard.write('     %d %s # %s\n' % (THDM_pdgID, str(newvalue), THDM_param_name))
              isTHDMparam = True
            else:
              print line.rstrip('\n')
              raise RuntimeError('Unable to parse line')
        if not isTHDMparam:
          newcard.write(line)
    oldcard.close()
    newcard.close()

runName='run_01'

if initialGluons:
    # Hack for copying MadLoop card in PROC directory. Needed for MG 2.6 which is affected by this bug
    from shutil import copyfile
    copyfile(os.environ['MADPATH']+'/Template/loop_material/StandAlone/Cards/MadLoopParams.dat',process_dir+'/Cards/MadLoopParams_default.dat')
    if os.path.isfile(process_dir+'/Cards/MadLoopParams_default.dat'):
        print "MadLoopParams_default.dat copied"
    else:
        raise RunTimeError("MadLopParams_default.dat not found")
    
    # Another hack for making IREGI compile
    os.environ['FC'] = "gfortran"

# Create reweighting card
if reweight: 
    reweight_card_path = os.getcwd()+'/reweight_card.dat'
    with open(reweight_card_path,'w') as rw:
        for param_name, value in Reweight.items():
            if param_name == "tanbeta":
                rw.write("launch --rwgt_name=tanbeta\n")
                rw.write("set FRBlock 2  %s\n" % value)
            elif param_name == "sinp":
                rw.write("launch --rwgt_name=sinp\n")
                rw.write("set Higgs 5  %s\n" % value)
    
# Generate the events    
if reweight:
    generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name=runName,proc_dir=process_dir,reweight_card_loc=reweight_card_path)
else:
    generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name=runName,proc_dir=process_dir)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)                                                                         
                                                                                                                                                                       
# Metadata                                                                                                                                                          
if initialGluons:
    evgenConfig.process = "g g > xd xd~ h1"
    initialStateString = "gluon fusion"
else:
    evgenConfig.process = "p p > xd xd~ h1"
    initialStateString = "b quark annihilation"

if decayChannel is "monoHyy":
   evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Higgs(-> gamma gamma) " +initialStateString + " initiated process  \
   with tan(beta) = " + str(THDMparams['tanbeta']) + ", sin(theta) = " + str(THDMparams['sinp']) + ", mA = " + str(THDMparams['mh3']) + ", ma"\
    + str(THDMparams['mh4'])
   evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel"]
   evgenConfig.contact = ["Kristian Bjoerke <kristian.bjoerke@cern.ch>"]

if decayChannel is "monoHbb":
   evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Higgs(-> b bbar) " +initialStateString + " initiated process  \
   with tan(beta) = " + str(THDMparams['tanbeta']) + ", sin(theta) = " + str(THDMparams['sinp']) + ", mA = " + str(THDMparams['mh3']) + ", ma"\
    + str(THDMparams['mh4'])
   evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel","bbbar"]
   evgenConfig.contact = ["Spyros Argyropoulos <spyridon.argyropoulos@cern.ch>, Chen Zhou <chen.zhou@cern.ch>"]

if decayChannel is "monoHtautau":
   evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Higgs(-> tau tau) " +initialStateString + " initiated process  \
   with tan(beta) = " + str(THDMparams['tanbeta']) + ", sin(theta) = " + str(THDMparams['sinp']) + ", mA = " + str(THDMparams['mh3']) + ", ma"\
 + str(THDMparams['mh4'])
   evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel"]
   evgenConfig.contact = ["Julia Djuvsland <julia.djuvsland@cern.ch>"]

evgenConfig.inputfilecheck = runName                                                                                                                                   
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'                                                                                                             
# Shower includes
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false",
			    "1000022:mayDecay = off"
                            ]

if decayChannel is "monoHyy":
   # Force h->gamgam decay in Pythia
   genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 22 22 "]

   # Generator filter
   if not hasattr( filtSeq, "DiPhotonFilter" ):
       from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
       filtSeq += DiPhotonFilter ()
   
   DiPhotonFilter = filtSeq.DiPhotonFilter
   DiPhotonFilter.PtCut1st = 30000.
   DiPhotonFilter.PtCut2nd = 20000.

if decayChannel is "monoHbb":
   # Force h->bb decay in Pythia
   genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 5 -5 "]
   
   # Generator filter
   include("MC15JobOptions/MissingEtFilter.py")
   filtSeq.MissingEtFilter.METCut = 100*GeV
   filtSeq.MissingEtFilter.UseNeutrinosFromHadrons = True

if decayChannel is "monoHtautau":
    # Force h->tautau decay in Pythia
    genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 15 -15 "]

    # Generator filter
    include("MC15JobOptions/MissingEtFilter.py")
    filtSeq.MissingEtFilter.METCut = 50*GeV
    filtSeq.MissingEtFilter.UseNeutrinosFromHadrons = True
