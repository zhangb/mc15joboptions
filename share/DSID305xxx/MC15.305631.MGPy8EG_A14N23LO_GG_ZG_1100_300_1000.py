runArgs.EventMultiplier = 19.0
include ( 'MC15JobOptions/MadGraphControl_SimplifiedModel_GG_ZG.py' )

from GeneratorFilters.GeneratorFiltersConf import ZtoLeptonFilter
filtSeq += ZtoLeptonFilter("ZtoLeptonFilter")
filtSeq.ZtoLeptonFilter.Ptcut = 0.
filtSeq.ZtoLeptonFilter.Etacut = 10.0

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq.MultiMuonFilter.Ptcut = 0.
filtSeq.MultiMuonFilter.Etacut = 10.0
filtSeq.MultiMuonFilter.NMuons = 2

evgenConfig.description = "GGM SUSY (Mg = 1100 GeV, Mchi = 300 GeV, ctau = 1 m)"
evgenConfig.process = "gg->GG->chi chi -> mu mu f f gravitino gravitino"
evgenConfig.keywords += ["2muon","longLived","exotic"]
evgenConfig.generators += ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ["Nathan Bernard <nathan.rogers.bernard@cern.ch>"]

