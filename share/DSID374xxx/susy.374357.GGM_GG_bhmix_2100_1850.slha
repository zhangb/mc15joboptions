#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.10000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419686E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399726E+03   # H
        36     2.00000000E+03   # A
        37     2.00151821E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.93206223E+03   # ~d_L
   2000001     4.93195196E+03   # ~d_R
   1000002     4.93181566E+03   # ~u_L
   2000002     4.93187387E+03   # ~u_R
   1000003     4.93206223E+03   # ~s_L
   2000003     4.93195196E+03   # ~s_R
   1000004     4.93181566E+03   # ~c_L
   2000004     4.93187387E+03   # ~c_R
   1000005     4.93124155E+03   # ~b_1
   2000005     4.93277400E+03   # ~b_2
   1000006     5.04389286E+03   # ~t_1
   2000006     5.33454820E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.15114318E+03   # ~g
   1000022     1.90930278E+03   # ~chi_10
   1000023    -1.96180213E+03   # ~chi_20
   1000025     1.99666424E+03   # ~chi_30
   1000035     3.12547648E+03   # ~chi_40
   1000024     1.95661689E+03   # ~chi_1+
   1000037     3.12546944E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.35558072E-01   # N_11
  1  2    -4.32630896E-02   # N_12
  1  3     4.79353237E-01   # N_13
  1  4    -4.76763152E-01   # N_14
  2  1     2.23208745E-03   # N_21
  2  2    -3.05250625E-03   # N_22
  2  3    -7.07041518E-01   # N_23
  2  4    -7.07161928E-01   # N_24
  3  1     6.77453415E-01   # N_31
  3  2     5.06544664E-02   # N_32
  3  3    -5.17972173E-01   # N_33
  3  4     5.19803639E-01   # N_34
  4  1     2.49226065E-03   # N_41
  4  2    -9.97774079E-01   # N_42
  4  3    -4.49176406E-02   # N_43
  4  4     4.92248097E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.34345553E-02   # U_11
  1  2     9.97986001E-01   # U_12
  2  1     9.97986001E-01   # U_21
  2  2     6.34345553E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.95235087E-02   # V_11
  1  2     9.97580313E-01   # V_12
  2  1     9.97580313E-01   # V_21
  2  2     6.95235087E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99861899E-01   # cos(theta_t)
  1  2     1.66187523E-02   # sin(theta_t)
  2  1    -1.66187523E-02   # -sin(theta_t)
  2  2     9.99861899E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81192641E-01   # cos(theta_b)
  1  2     7.32104218E-01   # sin(theta_b)
  2  1    -7.32104218E-01   # -sin(theta_b)
  2  2     6.81192641E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954886E-01   # cos(theta_tau)
  1  2     7.09252147E-01   # sin(theta_tau)
  2  1    -7.09252147E-01   # -sin(theta_tau)
  2  2     7.04954886E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200089E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305872E+02   # vev(Q)              
         4     3.10439900E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52717908E-01   # gprime(Q) DRbar
     2     6.26763840E-01   # g(Q) DRbar
     3     1.07966842E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02491086E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71397888E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738254E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.10000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.86181031E+05   # M^2_Hd              
        22    -8.56048869E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36865035E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.35458452E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.30861202E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.64368341E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.09311436E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.59768854E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.93255340E-09    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.19285640E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     4.59684906E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.47687153E-09    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     4.91117684E-05    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.59768854E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.93255340E-09    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.19285640E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     4.59684906E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.47687153E-09    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.91117684E-05    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.58758983E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.69341022E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.25628150E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     4.56671606E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.56671606E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.56671606E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.56671606E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     5.04838937E-06    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     5.04838937E-06    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.54741353E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.72334514E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01994321E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.64377737E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.89011858E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     4.53583875E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.69004154E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.98895050E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.65381624E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.93862837E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.18175442E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.59784033E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.03644557E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.06747236E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.26142673E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.80927766E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.54517711E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.11382686E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.58189916E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.76719308E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.30516529E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.48935597E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.04636765E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.52068255E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.66838948E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.91232389E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.44926228E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.07538946E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.94524935E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.42006248E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.12496962E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.61939145E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.34671769E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.55846861E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.74420794E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.31425327E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.54544955E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.10406771E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.92885881E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.25225424E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.74025749E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.12982516E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.45185481E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.61893393E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.90754148E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.92988619E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.79704142E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.27867343E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.05748197E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.86994637E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.30277024E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58513466E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.92894435E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28938055E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.23417144E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.93827282E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.45880999E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.51055388E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.91411941E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.93035619E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74118007E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.87960642E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.30887527E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.82498375E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.36161062E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89295323E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.92885881E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.25225424E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.74025749E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.12982516E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.45185481E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.61893393E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.90754148E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.92988619E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.79704142E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.27867343E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.05748197E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.86994637E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.30277024E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58513466E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.92894435E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28938055E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.23417144E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.93827282E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.45880999E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.51055388E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.91411941E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.93035619E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74118007E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.87960642E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.30887527E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.82498375E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.36161062E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89295323E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.62939710E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.44960595E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71642151E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.79281455E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73835246E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.28889426E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49449938E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.77963561E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.49035587E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.96001923E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.50956247E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.20674204E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.62939710E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.44960595E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71642151E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.79281455E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73835246E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.28889426E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49449938E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.77963561E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.49035587E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.96001923E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.50956247E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.20674204E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.20951656E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.72502947E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45346101E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.36594827E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61293740E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.36081236E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.23639563E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.62764351E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.20677647E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.59169083E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02318285E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.44019918E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64789435E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.82199218E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.30637046E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.62965255E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.13313558E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.87259018E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.73645350E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75305317E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.15084784E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.48855870E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.62965255E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.13313558E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.87259018E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.73645350E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75305317E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.15084784E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.48855870E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.63195182E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13214568E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.86396550E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.73144214E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75064811E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01703728E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48379299E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.63982908E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00438325E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.00085091E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.00085091E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.00028467E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.00028467E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.93345592E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.65784862E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53812125E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.05670853E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46114574E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.41819849E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52573253E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.34311947E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.42860603E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00722645E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88564354E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.07130011E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.19028505E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.85762443E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.06786776E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.91663721E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.22945603E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09496783E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.41790402E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.09496783E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.41790402E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.30979712E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.23865988E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.23865988E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.20984259E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.46804601E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.46804601E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.46804601E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.28725120E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.28725120E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.28725120E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.28725120E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.09575054E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.09575054E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.09575054E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.09575054E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.19526321E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.19526321E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.95178062E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.30096606E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.27134428E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.08522856E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.35000714E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.89951498E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.54429075E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.83739879E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.54429075E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.83739879E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.97807244E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.21751890E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.21751890E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.51634322E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.96107889E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.96107889E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.96107889E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.66383513E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.15454818E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.66383513E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.15454818E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.81130566E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.92124649E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.92124649E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.84037113E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     9.82835705E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     9.82835705E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     9.82835705E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.20879505E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.20879505E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.20879505E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.20879505E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.02931099E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.02931099E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.02931099E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.02931099E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.99011969E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.99011969E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.65777469E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.40550125E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52472968E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.91177208E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46907814E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46907814E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07848537E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.16273537E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.44805520E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.22796169E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.11387316E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.17885193E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.36394432E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.78652612E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22202176E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02250888E-01    2           5        -5   # BR(h -> b       bb     )
     6.21844145E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20105717E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65701197E-04    2           3        -3   # BR(h -> s       sb     )
     2.00914466E-02    2           4        -4   # BR(h -> c       cb     )
     6.63525295E-02    2          21        21   # BR(h -> g       g      )
     2.30439847E-03    2          22        22   # BR(h -> gam     gam    )
     1.62527552E-03    2          22        23   # BR(h -> Z       gam    )
     2.16448652E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80565885E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78094270E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46617813E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431197E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71226030E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548868E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668872E-05    2           4        -4   # BR(H -> c       cb     )
     9.96071576E-01    2           6        -6   # BR(H -> t       tb     )
     7.97749187E-04    2          21        21   # BR(H -> g       g      )
     2.71748401E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030622E-06    2          23        22   # BR(H -> Z       gam    )
     3.34038823E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66563889E-04    2          23        23   # BR(H -> Z       Z      )
     9.01530846E-04    2          25        25   # BR(H -> h       h      )
     7.19204614E-24    2          36        36   # BR(H -> A       A      )
     2.91246322E-11    2          23        36   # BR(H -> Z       A      )
     6.51092189E-12    2          24       -37   # BR(H -> W+      H-     )
     6.51092189E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380216E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46910191E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899030E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62271179E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678006E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181833E-06    2           4        -4   # BR(A -> c       cb     )
     9.97001346E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680715E-04    2          21        21   # BR(A -> g       g      )
     3.13388912E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290211E-06    2          23        22   # BR(A -> Z       gam    )
     3.25524784E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471499E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35345046E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239155E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150447E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49621963E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697405E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732277E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402954E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33629341E-04    2          24        25   # BR(H+ -> W+      h      )
     2.83265399E-13    2          24        36   # BR(H+ -> W+      A      )
