#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980542E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03952514E+01   # W+
        25     1.25433141E+02   # h
        35     3.00008262E+03   # H
        36     2.99999993E+03   # A
        37     3.00109879E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03529704E+03   # ~d_L
   2000001     3.02998692E+03   # ~d_R
   1000002     3.03438013E+03   # ~u_L
   2000002     3.03192144E+03   # ~u_R
   1000003     3.03529704E+03   # ~s_L
   2000003     3.02998692E+03   # ~s_R
   1000004     3.03438013E+03   # ~c_L
   2000004     3.03192144E+03   # ~c_R
   1000005     8.42615208E+02   # ~b_1
   2000005     3.02864054E+03   # ~b_2
   1000006     8.40225802E+02   # ~t_1
   2000006     3.02277982E+03   # ~t_2
   1000011     3.00677498E+03   # ~e_L
   2000011     3.00121632E+03   # ~e_R
   1000012     3.00537721E+03   # ~nu_eL
   1000013     3.00677498E+03   # ~mu_L
   2000013     3.00121632E+03   # ~mu_R
   1000014     3.00537721E+03   # ~nu_muL
   1000015     2.98608835E+03   # ~tau_1
   2000015     3.02201595E+03   # ~tau_2
   1000016     3.00543859E+03   # ~nu_tauL
   1000021     2.34353398E+03   # ~g
   1000022     1.00943680E+02   # ~chi_10
   1000023     2.17291449E+02   # ~chi_20
   1000025    -2.99914844E+03   # ~chi_30
   1000035     2.99922860E+03   # ~chi_40
   1000024     2.17451203E+02   # ~chi_1+
   1000037     3.00012569E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891864E-01   # N_11
  1  2     7.64208840E-04   # N_12
  1  3    -1.46838610E-02   # N_13
  1  4     2.45417608E-04   # N_14
  2  1    -3.82047695E-04   # N_21
  2  2     9.99661297E-01   # N_22
  2  3     2.60183834E-02   # N_23
  2  4     4.34807505E-04   # N_24
  3  1     1.02201043E-02   # N_31
  3  2    -1.86991385E-02   # N_32
  3  3     7.06779169E-01   # N_33
  3  4     7.07113214E-01   # N_34
  4  1    -1.05670965E-02   # N_41
  4  2     1.80845100E-02   # N_42
  4  3    -7.06803108E-01   # N_43
  4  4     7.07100172E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99323568E-01   # U_11
  1  2     3.67750806E-02   # U_12
  2  1    -3.67750806E-02   # U_21
  2  2     9.99323568E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999812E-01   # V_11
  1  2    -6.13321905E-04   # V_12
  2  1    -6.13321905E-04   # V_21
  2  2    -9.99999812E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98558814E-01   # cos(theta_t)
  1  2    -5.36683797E-02   # sin(theta_t)
  2  1     5.36683797E-02   # -sin(theta_t)
  2  2     9.98558814E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99709895E-01   # cos(theta_b)
  1  2    -2.40858016E-02   # sin(theta_b)
  2  1     2.40858016E-02   # -sin(theta_b)
  2  2     9.99709895E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06909392E-01   # cos(theta_tau)
  1  2     7.07304115E-01   # sin(theta_tau)
  2  1    -7.07304115E-01   # -sin(theta_tau)
  2  2    -7.06909392E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00159311E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49805425E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44078833E+02   # higgs               
         4     1.05680680E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49805425E+03  # The gauge couplings
     1     3.62142150E-01   # gprime(Q) DRbar
     2     6.39753648E-01   # g(Q) DRbar
     3     1.02637584E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49805425E+03  # The trilinear couplings
  1  1     2.13031144E-06   # A_u(Q) DRbar
  2  2     2.13034468E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49805425E+03  # The trilinear couplings
  1  1     7.43859327E-07   # A_d(Q) DRbar
  2  2     7.43951110E-07   # A_s(Q) DRbar
  3  3     1.55902843E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49805425E+03  # The trilinear couplings
  1  1     3.49278793E-07   # A_e(Q) DRbar
  2  2     3.49296349E-07   # A_mu(Q) DRbar
  3  3     3.54309098E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49805425E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53078797E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49805425E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.93212357E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49805425E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03342710E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49805425E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.05729815E+04   # M^2_Hd              
        22    -9.06944644E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42091588E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.19781200E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47997731E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47997731E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52002269E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52002269E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.05438821E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.28263060E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.09646173E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.77527521E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16840148E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.65447176E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.60767196E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.24719144E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.96813115E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.93158308E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68948620E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62008147E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.18465040E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.85378718E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.39228720E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.48628213E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.37448915E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.52034288E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.01436858E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.59451312E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.39847808E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.32124446E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.04908929E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.32634154E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.46615995E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.06828268E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.55664905E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07111039E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.95235155E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64561861E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.88999496E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.64860317E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29389582E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.31913962E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.00096150E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18389483E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59529376E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.31243370E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.98453648E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.55936187E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40470582E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.07612957E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.85435929E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64544662E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.59130114E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.33137037E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28815001E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.76726806E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.00785612E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66763859E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54661976E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.59041977E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.27782803E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.42283360E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54533791E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07111039E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.95235155E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64561861E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.88999496E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.64860317E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29389582E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.31913962E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.00096150E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18389483E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59529376E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.31243370E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.98453648E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.55936187E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40470582E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.07612957E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.85435929E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64544662E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.59130114E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.33137037E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28815001E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.76726806E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.00785612E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66763859E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54661976E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.59041977E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.27782803E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.42283360E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54533791E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02180150E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.75522080E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00867772E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.30296117E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.12053607E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01580001E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.60690791E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56220695E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.44792859E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.98703014E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.96280203E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02180150E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.75522080E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00867772E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.30296117E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.12053607E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01580001E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.60690791E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56220695E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.44792859E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.98703014E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.96280203E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82030649E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.46356531E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17941300E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35702169E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76176705E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54440769E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15229646E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.18406522E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.00531501E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30296037E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.16536152E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02210440E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.69741620E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00962647E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.12691209E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.94382759E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02063185E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.79030157E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02210440E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.69741620E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00962647E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.12691209E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.94382759E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02063185E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.79030157E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02252506E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.69660102E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00937449E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.18837898E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.00245930E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02095814E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.20754654E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.14237720E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.26714237E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.40348912E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.51553065E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.53394823E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.26265350E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.98059370E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.10327399E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.46323572E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.84297749E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.26941040E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04370954E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.02646345E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.10583305E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.10583305E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.80362436E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.52697824E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32258844E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32258844E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.92947346E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.92947346E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.22485915E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.22485915E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.30407831E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.73050181E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.22684277E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.08270984E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.08270984E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.22607776E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.11357619E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.22425438E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.22425438E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.90493276E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.90493276E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.01479569E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.01479569E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.68940356E-03   # h decays
#          BR         NDA      ID1       ID2
     5.58195142E-01    2           5        -5   # BR(h -> b       bb     )
     7.05304768E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.49677897E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29587084E-04    2           3        -3   # BR(h -> s       sb     )
     2.30104992E-02    2           4        -4   # BR(h -> c       cb     )
     7.52246325E-02    2          21        21   # BR(h -> g       g      )
     2.58672242E-03    2          22        22   # BR(h -> gam     gam    )
     1.75995027E-03    2          22        23   # BR(h -> Z       gam    )
     2.37972079E-01    2          24       -24   # BR(h -> W+      W-     )
     2.99412330E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.73248264E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00834711E-01    2           5        -5   # BR(H -> b       bb     )
     6.66249500E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.35569902E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.89280583E-04    2           3        -3   # BR(H -> s       sb     )
     8.10887960E-08    2           4        -4   # BR(H -> c       cb     )
     8.08912671E-03    2           6        -6   # BR(H -> t       tb     )
     8.50291783E-06    2          21        21   # BR(H -> g       g      )
     5.24283295E-08    2          22        22   # BR(H -> gam     gam    )
     3.30787356E-09    2          23        22   # BR(H -> Z       gam    )
     7.82509253E-07    2          24       -24   # BR(H -> W+      W-     )
     3.90771970E-07    2          23        23   # BR(H -> Z       Z      )
     5.44771445E-06    2          25        25   # BR(H -> h       h      )
    -2.57762926E-24    2          36        36   # BR(H -> A       A      )
     1.14296310E-19    2          23        36   # BR(H -> Z       A      )
     8.92594386E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.15358722E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.46697562E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.73047067E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.22518083E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.41810135E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.64980851E+01   # A decays
#          BR         NDA      ID1       ID2
     9.21261603E-01    2           5        -5   # BR(A -> b       bb     )
     6.81327239E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.40900694E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.95876075E-04    2           3        -3   # BR(A -> s       sb     )
     8.34920346E-08    2           4        -4   # BR(A -> c       cb     )
     8.32428236E-03    2           6        -6   # BR(A -> t       tb     )
     2.45142151E-05    2          21        21   # BR(A -> g       g      )
     5.79808942E-08    2          22        22   # BR(A -> gam     gam    )
     2.41243252E-08    2          23        22   # BR(A -> Z       gam    )
     7.97190753E-07    2          23        25   # BR(A -> Z       h      )
     9.29281697E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.28119720E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.65038808E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.82004376E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.97906404E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46885383E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.25178379E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21047826E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.40065295E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.29904112E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.67254531E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24193177E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.32488143E-07    2          24        25   # BR(H+ -> W+      h      )
     5.29784755E-14    2          24        36   # BR(H+ -> W+      A      )
     5.15842145E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.56103354E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07928635E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
