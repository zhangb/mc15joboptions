from MadGraphControl.MadGraphUtils import *

mode = 0



cmdsps = """set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/W+:PrintDecayModes
do /Herwig/Particles/W-:PrintDecayModes
do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma; h0->W+,W-;
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/W+:PrintDecayModes
do /Herwig/Particles/W-:PrintDecayModes
"""


# LHEHandler generated events/number of attempts ~75%
safefactor = 3
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> WW, yyWW"
evgenConfig.keywords = ["BSM", "BSMHiggs", "resonance", "diphoton"]
run_number_min = 345901
run_number_max = 345905
offset = 4

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
filtSeq.Expression = "hyyFilter and hWWFilter"

evgenConfig.generators  = [  "aMcAtNlo", "Herwigpp", "EvtGen"]
