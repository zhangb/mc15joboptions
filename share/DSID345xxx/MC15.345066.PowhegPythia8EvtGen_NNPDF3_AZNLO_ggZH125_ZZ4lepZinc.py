#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
# genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, gg->H+Z, H->ZZ->llll mh=125 GeV CPS"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "ZZ", "ZHiggs", "mH125" ]
evgenConfig.contact     = [ 'bianca.ciungu@cern.ch' ]
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.process = "gg->ZH, H->ZZ4lep, Z->inc"
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents = 2000



