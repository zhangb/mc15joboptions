import subprocess
from fileinput import FileInput
from MadGraphControl import MadGraphUtils

evgenConfig.description = "MadGraph5+Pythia8 for Ypp331  (Costantini-Corcella-Coriano-Frampton)"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.inputfilecheck = "Ypp331"
evgenConfig.contact = ["Antonio Sidoti <antonio.sidoti@cern.ch>"]
#evgenConfig.process = "pp > ypp yppc"

proc_name = 'PROC_Ypp331_' + str(mYpp)

#directory where param_card to be used are stored
#dir_path = os.getcwd()
#print 'dir_path is: ',str(dir_path)
#dir_param = 'mycards'
file_name = 'param_card_MGPy8EG_Ypp331_'
param_card_to_use = str(file_name)+str(mYpp)+'.dat'

#define p = g d1 d1bar d2 d2bar u1 u1bar u2 u2bar
#compute_widths ypp hpm5 hpm6 h3 ah4 h4 ah5 dpm4 h5 zp fdd1 fdd2  --body_decay=2

proc_card = """
import model Ypp331 -modelname
define p = g d d~ s s~ u u~ c c~
generate p p > ypp yppc @1
"""
proc_card += "\noutput %s -f" % proc_name


# modifications to the param_card.dat (generated from the proc_card i.e. the specific model)
# if you want to see the resulting param_card, run Generate_tf with this jobo, and look at the param_card.dat in the cwd
# If you want to see the auto-calculated values of the decay widths, look
# at the one in <proc_name>/Cards/param_card.dat (again, after running a
# Generate_tf)
# we don't need this since we have to generate the param_card with
# a gcc version (>5) that is not available in athena
param_card_extras = {
#    'MASS': {'MYpp': mYpp},  # set mass of Ypp
#    'DECAY' :{'WYpp':'Auto', 'WHpm5':'Auto', 'WHpm6':'Auto', 'Wh3':'Auto', 
#              'WAh4':'Auto', 'Wh4':'Auto', 'WAh5':'Auto', 'WDpm4':'Auto', 
#              'Wh5':'Auto', 'WZp':'Auto', 'Wfdd1':'Auto', 'Wfdd2':'Auto'}, 
}
#              'Wft':'Auto' },

run_card_extras = {
    'pdlabel': 'lhapdf',
    'lhaid': '263000'  # NNPDF30_lo_as_0130 pdf set
}

save_proc_dir = True


safefactor = 1.3
nevents = 50 * safefactor
if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents * safefactor

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError('No center of mass energy found')

# make the proc card
fcard = open('proc_card_mg5.dat', 'w')
fcard.write(proc_card)
fcard.close()

# does mg5 to determine all the diagrams etc ... generates the process
# directory corresponding to proc_name
process_dir = MadGraphUtils.new_process()

if proc_name != '' and process_dir != proc_name:
    raise RuntimeError(
        'Unexpected process_dir %s when expected %s' % (process_dir, proc_name))

# create the param_card
if os.access('param_card.dat', os.R_OK):
    print('Deleting old param_card.dat')
    os.remove('param_card.dat')
#param_card = '%s/Cards/param_card.dat' % process_dir

#if(MadGraphUtils.build_param_card(param_card_old=param_card, param_card_new='param_card.dat', params=param_card_extras) == -1):
#    raise RuntimeError('Could not create param_card.dat')

# create the run card: FIXME: Should check for success
if os.access('run_card.dat', os.R_OK):
    print('Deleting old run_card.dat')
    os.remove('run_card.dat')
run_card = MadGraphUtils.get_default_runcard(proc_dir=process_dir)
if MadGraphUtils.build_run_card(run_card_old=run_card, run_card_new='run_card.dat', nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, extras=run_card_extras) == -1:
    raise RuntimeError('Could not create run_card.dat')

# ready to generate
#MadGraphUtils.generate(run_card_loc='run_card.dat', param_card_loc='param_card.dat',

MadGraphUtils.generate(run_card_loc='run_card.dat', param_card_loc=param_card_to_use,
                       mode=0, njobs=1, run_name='Test', proc_dir=process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber) + '.MadGraph_' + str(proc_name)

MadGraphUtils.arrange_output(run_name='Test', proc_dir=process_dir,
                             outputDS=stringy + '._00001.events.tar.gz', saveProcDir=save_proc_dir)


outputDS = 'tmp_' + stringy + '._00001.events.events'
outputZip = 'tmp_' + stringy + '._00001.events.tar.gz'

if 'ATHENA_PROC_NUMBER' in os.environ:
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts, 'nprocs'):
        mglog.warning('Did not see option!')
    else:
        opts.nprocs = 0

evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile = stringy + '._00001.events.tar.gz'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_MadGraph.py')

