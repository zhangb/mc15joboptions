model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 500.
mHD = 500.
widthZp = 2.346527e+00
widthhd = 1.989039e-01
filteff = 9.746589e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
