from MadGraphControl.MadGraphUtils import *
import fnmatch
import os
import fileinput

nevents=200000
mode=0


### DSID lists (extensions can include systematics samples)
test=[343433]

fcard = open('proc_card_mg5.dat','w')
#fcard = open('../proc_card_mg5_h2.dat')
if runArgs.runNumber in test:
    fcard.write("""
    set group_subprocesses Auto
    set ignore_six_quark_processes False
    set loop_optimized_output True
    set gauge unitary
    set complex_mass_scheme False
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    import model mssm-full
    define susy = go ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ sve svm svt el- mul- ta1- er- mur- ta2- w- h- sve~ svm~ svt~ el+ mul+ ta1+ er+ mur+ ta2+ n1 n2 n3 n4 x1+ x2+ x1- x2-
    generate p p > b b~ h3, (h3 > t t~, (t > b w+, w+ > l+ vl), (t~ > b~ w-, w- > j j))
    output -f""")
    fcard.close()
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'"}
build_run_card(run_card_old=get_default_runcard(),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

    
print_cards()
    
runName='run_01'     


process_dir = new_process()


str_param_card='MadGraph_EffDM_paramcard_05TeV.dat'
for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, str_param_card):
        param_grid_location=(os.path.join(root, filename))

#generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)
generate(run_card_loc='run_card.dat',param_card_loc=param_grid_location,mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')  

   


#### Shower 
evgenConfig.description = 'MadGraph_ttbb'
#evgenConfig.keywords+=['ttbb','Higgs','jets']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

include("MC15JobOptions/Pythia8_MadGraph.py")

