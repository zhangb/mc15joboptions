model  = 'dmA'
mR     = 3500
mDM    = 10000
gSM    = 0.10
gDM    = 1.50
widthR = 16.670410

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet.py")
