
#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13 15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->Zllgam"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2lepton","photon", "mH125" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.inputfilecheck = "TXT"
#runArgs.inputGeneratorFile='../mc15_13TeV.344235.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau.evgen.TXT.e5500/TXT.09954509._000053.tar.gz.1'

