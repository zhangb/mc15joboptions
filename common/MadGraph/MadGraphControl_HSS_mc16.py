from MadGraphControl.MadGraphUtils import *

#############################################
### Production of scalar long-lived particles
### though a higgs-like scalar mediator
############################################

# PDF
pdflabel = 'lhapdf'
lhaid = 315000 # NNPDF31_lo_as_0118

# parameter mass and average life-time
dictmH = { 
    305588 : 125, 304794 : 125, 303680 : 125, 304795 : 125, 303681 : 125, 304796 : 125, 
    305589 : 125, 304797 : 125, 304798 : 125, 304799 : 125, 304800 : 125, 304801 : 125,
    304802 : 100, 304803 : 100, 304804 : 200, 304805 : 200, 304806 : 200, 304807 : 200, 
    304808 : 200, 304809 : 200, 304810 : 400, 304811 : 400, 304812 : 400, 304813 : 400,
    304814 : 600, 304815 : 600, 304816 : 600, 304817 : 600, 304818 : 1000, 304819 : 1000, 
    304820 : 1000, 304821 : 1000, 304822 : 1000, 304823 : 1000, 
    305812 : 125, 305813 : 600, 308368 : 125, 308369 : 600,
    311309 : 125, 311310 : 125, 311311 : 125, 311312 : 125, 311313 : 125, 311314 : 125, 311315 : 125,
    311417 : 60, 311418 : 60, 311419 : 200, 311420 : 400, 
    311421 : 600, 311422 : 600, 311423 : 600, 311424 : 600, 
    311425 : 1000, 311426 : 1000, 311427 : 1000, 311428 : 1000, 
    312899 : 125,
    }

dictmS = { 
    305588 : 5, 304794 : 8, 303680 : 15, 304795 : 25, 303681 : 40, 304796 : 55, 
    305589 : 5, 304797 : 8, 304798 : 15, 304799 : 25, 304800 : 40, 304801 : 55, 
    304802 : 8, 304803 : 25, 304804 : 8, 304805 : 25, 304806 : 50, 304807 : 8, 
    304808 : 25, 304809 : 50, 304810 : 50, 304811 : 100, 304812 : 50, 304813 : 100, 
    304814 : 50, 304815 : 150, 304816 : 50, 304817 : 150, 304818 : 50, 304819 : 150, 
    304820 : 400, 304821 : 50, 304822 : 150, 304823 : 400, 
    305812 : 40, 305813 : 50, 308368 : 40, 308369 : 50,
    311309 : 5, 311310 : 5, 311311 : 15, 311312 : 35, 311313 : 35, 311314 : 55, 311315 : 55,
    311417 : 5, 311418 : 15, 311419 : 50, 311420 : 100, 
    311421 : 50, 311422 : 150, 311423 : 150, 311424 : 275, 
    311425 : 50, 311426 : 275, 311427 : 275, 311428 : 475, 
    312899 : 35,
    }

dictAvgtau = { 
    305588 : 127, 304794 : 200, 303680 : 580, 304795 : 760, 303681 : 1180, 304796 : 1540, 
    305589 : 228, 304797 : 375, 304798 : 710, 304799 : 1210, 304800 : 1900, 304801 : 2730, 
    304802 : 240, 304803 : 740, 304804 : 170, 304805 : 540, 304806 : 1070, 304807 : 290, 
    304808 : 950, 304809 : 1900, 304810 : 700, 304811 : 1460, 304812 : 1260, 304813 : 2640, 
    304814 : 520, 304815 : 1720, 304816 : 960, 304817 : 3140, 304818 : 380, 304819 : 1170, 
    304820 : 3960, 304821 : 670, 304822 : 2110, 304823 : 7200, 
    305812 : 1180, 305813 : 520, 308368 : 215, 308369 : 55,
    311309 : 127, 311310 : 411, 311311 : 580, 311312 : 1310, 311313 : 2630, 311314 : 1050, 
    311315 : 5320,
    311417 : 217, 311418 : 661, 311419 : 1255, 311420 : 1608,
    311421 : 590, 311422 : 1840, 311423 : 3309, 311424 : 4288, 
    311425 : 406, 311426 : 2399, 311427 : 4328, 311428 : 6039, 
    312899 : 10000,
    }


#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

try :    
    modelcode='HAHM_variableMW_v3_UFO'
    process = 'generate g g > h HIG=1 HIW=0 QED=0 QCD=0, (h > h2 h2, h2 > f f)'
    mH = dictmH[runArgs.runNumber]
    mhS = dictmS[runArgs.runNumber]
    avgtau = dictAvgtau[runArgs.runNumber]
except KeyError:
    raise RuntimeError('Bad runNumber')

# basename for madgraph LHEF file
rname = 'run_'+str(runArgs.runNumber)

# writing proc card for MG
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model """+modelcode+"""
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define f = u c d s u~ c~ d~ s~ b b~ e+ e- mu+ mu- ta+ ta- t t~
"""+process+"""
output -f
""")
fcard.close()

#---------------------------------------------------------------------------
# Energy
#---------------------------------------------------------------------------
    
beamEnergy = -999.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

run_card_extras = { 
    'lhe_version':'3.0',
    'cut_decays':'F',
    'event_norm':'sum',
    'pdlabel':pdflabel,
    'lhaid':lhaid,
    'ptj':'0',
    'ptb':'0',
    'pta':'0',
    'ptl':'0',
    'etaj':'-1',
    'etab':'-1',
    'etaa':'-1',
    'etal':'-1',
    'drjj':'0',
    'drbb':'0',
    'drll':'0',
    'draa':'0',
    'drbj':'0',
    'draj':'0',
    'drjl':'0',
    'drab':'0',
    'drbl':'0',
    'dral':'0' ,
    'use_syst':'T',
    'sys_scalefact': '1 0.5 2',
    'sys_pdf'      : "NNPDF31_lo_as_0118"
    }

safefactor=1.1 #generate extra 10% events in case any fail showering
if runArgs.maxEvents > 0: 
    nevents = runArgs.maxEvents*safefactor
else: nevents = 5000*safefactor

process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(process_dir),
               run_card_new='run_card.dat',
               nevts=nevents,
               rand_seed=runArgs.randomSeed,
               beamEnergy=beamEnergy,
               extras=run_card_extras)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------

if mH <= 125: 
    param_card_extras = { 
        "HIDDEN": { 'epsilon': '1e-10', #kinetic mixing parameter
                    'kap': '1e-4', #higgs mixing parameter
                    'mhsinput':mhS, #dark higgs mass
                    'mzdinput': '1.000000e+03' # Z' mass
                    }, 
        "HIGGS": { 'mhinput':mH}, #higgs mass
        #auto-calculate decay widths and BR of Zp, H, t, hs
        "DECAY": { 'wzp':'Auto', 'wh':'Auto', 'wt':'Auto', 'whs':'Auto'} 
        }
elif mH > 125:
    param_card_extras = { 
        "HIDDEN": { 'epsilon': '1e-10', #kinetic mixing parameter
                    'kap': '1e-4', #higgs mixing parameter
                    'mhsinput':mhS, #dark higgs mass
                    'mzdinput': '1.000000e+03' # Z' mass
                    }, 
        "HIGGS": { 'mhinput':mH}, #higgs mass
        #auto-calculate decay widths and BR of Zp, H, t, hs
        "DECAY": { 'wzp':'5', 'wh':'5', 'wt':'Auto', 'whs':'5'} 
        }

build_param_card(param_card_old="{}/Cards/param_card.dat".format(process_dir),
                 param_card_new='param_card.dat',
                 params=param_card_extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------

generate(run_card_loc='run_card.dat', 
         param_card_loc='param_card.dat', 
         mode=0, njobs=1, 
         run_name=rname, 
         proc_dir=process_dir)

#---------------------------------------------------------------------------
# Arrange LHE file output
#---------------------------------------------------------------------------

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)
# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)
    

# replacing lifetime of scalar, manually
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
    
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:  
        if 'vent' in line or line.startswith("<"):
            newlhe.write(line)
            continue
        newline = line.rstrip('\n')
        columns = (' '.join(newline.split())).split()
        pdgid = int(columns[0])
        if pdgid == 35:
            part1 = line[:-22]
            part2 = "%.11E" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        else:
            newlhe.write(line)

oldlhe.close()
newlhe.close()
    
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',
            process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(run_name=rname, proc_dir=process_dir,
               outputDS=rname+'._00001.events.tar.gz',
               lhe_version=3,
               saveProcDir=True)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------

if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.description = "Displaced hadronic jets process Higgs > S S with mH={}GeV, mS={}GeV".format(mH, mhS)
evgenConfig.keywords = ["exotic", "BSM", "BSMHiggs", "longLived"]
evgenConfig.contact  = ['simon.berlendis@cern.ch', 'hao.zhou@cern.ch',
                        'Cristiano.Alpigiani@cern.ch', 'hrussell@cern.ch' ]
evgenConfig.process="Higgs --> LLPs"
evgenConfig.inputfilecheck = rname
runArgs.inputGeneratorFile=rname+'._00001.events.tar.gz'
