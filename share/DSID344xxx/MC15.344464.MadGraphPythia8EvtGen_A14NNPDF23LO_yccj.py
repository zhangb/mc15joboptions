include('MC15JobOptions/MadGraphPythia8EvtGenControl_photonHF_ECFA16.py')

evgenConfig.description = "Photon + cc+jet suitable for hh->bbaa searches"
evgenConfig.process = "Part of suite of samples with diphoton and dijet mass cuts loosely consistent with hh production"
evgenConfig.keywords = ["jets","multijet","3jet","photon","charm"]
