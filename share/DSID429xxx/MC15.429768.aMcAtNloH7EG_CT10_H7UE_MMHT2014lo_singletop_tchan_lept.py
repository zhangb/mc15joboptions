# based on the JobOptions MC15.410141 and MC15.429302
# using LHE files from group.phys-gener.MG5_aMCatNLO222.410141.singletop_tchan_lept_CT104f_13TeV.TXT.mc15_v2

# Provide config information
evgenConfig.generators    += ["aMcAtNlo", "Herwig7", "EvtGen"] 
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "MG5_aMC@NLO+Herwig7 t-channel single top, CT10f4 ME, H7-UE-MMHT tune, with EvtGen"
evgenConfig.keywords       = ['SM', 'top', 'lepton']
evgenConfig.contact        = ['paolo.francavilla@cern.ch', 'dominic.hirschbuehl@cern.ch', 'daniel.rauch@desy.de']

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10f4")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()
