#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17312046E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04030149E+01   # W+
        25     1.24657757E+02   # h
        35     2.99999969E+03   # H
        36     2.99999995E+03   # A
        37     3.00108155E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04338391E+03   # ~d_L
   2000001     3.03819932E+03   # ~d_R
   1000002     3.04248048E+03   # ~u_L
   2000002     3.03939087E+03   # ~u_R
   1000003     3.04338391E+03   # ~s_L
   2000003     3.03819932E+03   # ~s_R
   1000004     3.04248048E+03   # ~c_L
   2000004     3.03939087E+03   # ~c_R
   1000005     1.10368049E+03   # ~b_1
   2000005     3.03549457E+03   # ~b_2
   1000006     1.10154674E+03   # ~t_1
   2000006     3.02495049E+03   # ~t_2
   1000011     3.00641353E+03   # ~e_L
   2000011     3.00196814E+03   # ~e_R
   1000012     3.00502910E+03   # ~nu_eL
   1000013     3.00641353E+03   # ~mu_L
   2000013     3.00196814E+03   # ~mu_R
   1000014     3.00502910E+03   # ~nu_muL
   1000015     2.98615137E+03   # ~tau_1
   2000015     3.02154254E+03   # ~tau_2
   1000016     3.00482559E+03   # ~nu_tauL
   1000021     2.35417919E+03   # ~g
   1000022     4.02104252E+02   # ~chi_10
   1000023     8.37871189E+02   # ~chi_20
   1000025    -2.99408514E+03   # ~chi_30
   1000035     2.99455572E+03   # ~chi_40
   1000024     8.38034792E+02   # ~chi_1+
   1000037     2.99529121E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888435E-01   # N_11
  1  2    -9.76560416E-05   # N_12
  1  3    -1.48845856E-02   # N_13
  1  4    -1.24809367E-03   # N_14
  2  1     5.13467545E-04   # N_21
  2  2     9.99605480E-01   # N_22
  2  3     2.74297414E-02   # N_23
  2  4     6.01910901E-03   # N_24
  3  1    -9.63845587E-03   # N_31
  3  2     1.51438521E-02   # N_32
  3  3    -7.06866218E-01   # N_33
  3  4     7.07119448E-01   # N_34
  4  1     1.13997668E-02   # N_41
  4  2    -2.36545607E-02   # N_42
  4  3     7.06658480E-01   # N_43
  4  4     7.07067394E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99247439E-01   # U_11
  1  2     3.87885898E-02   # U_12
  2  1    -3.87885898E-02   # U_21
  2  2     9.99247439E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99963786E-01   # V_11
  1  2    -8.51033889E-03   # V_12
  2  1    -8.51033889E-03   # V_21
  2  2    -9.99963786E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98416174E-01   # cos(theta_t)
  1  2    -5.62596080E-02   # sin(theta_t)
  2  1     5.62596080E-02   # -sin(theta_t)
  2  2     9.98416174E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99712457E-01   # cos(theta_b)
  1  2    -2.39792268E-02   # sin(theta_b)
  2  1     2.39792268E-02   # -sin(theta_b)
  2  2     9.99712457E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06968597E-01   # cos(theta_tau)
  1  2     7.07244938E-01   # sin(theta_tau)
  2  1    -7.07244938E-01   # -sin(theta_tau)
  2  2    -7.06968597E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00152788E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73120465E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43813854E+02   # higgs               
         4     1.01406372E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73120465E+03  # The gauge couplings
     1     3.62630601E-01   # gprime(Q) DRbar
     2     6.36771185E-01   # g(Q) DRbar
     3     1.02308033E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73120465E+03  # The trilinear couplings
  1  1     2.85898352E-06   # A_u(Q) DRbar
  2  2     2.85902420E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73120465E+03  # The trilinear couplings
  1  1     1.02379557E-06   # A_d(Q) DRbar
  2  2     1.02391078E-06   # A_s(Q) DRbar
  3  3     2.04083770E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73120465E+03  # The trilinear couplings
  1  1     4.14370117E-07   # A_e(Q) DRbar
  2  2     4.14390110E-07   # A_mu(Q) DRbar
  3  3     4.19960964E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73120465E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49195018E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73120465E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.71572337E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73120465E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00971136E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73120465E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.99534501E+04   # M^2_Hd              
        22    -9.01526167E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40735807E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.84434698E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47843185E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47843185E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52156815E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52156815E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.31996662E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.92565596E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.50924296E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.99819144E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09756544E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.85705172E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.79283054E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.63142013E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62351348E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.05665161E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.65748655E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.58826704E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.09884187E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.13981427E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.84285960E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.85911644E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.55659760E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.18829969E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.12599003E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.07670133E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.32145658E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.10625126E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.16011953E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.66044426E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.70499217E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.62308292E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.39290222E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.55677079E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.20321977E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52827796E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.59319688E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.18579263E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.05795824E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.79587342E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.35173004E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.13491294E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57370732E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.68089843E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.51625667E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.07850736E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42629195E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.56207602E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.21299596E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52705874E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.04638618E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.37321950E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.05233288E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.01910330E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.35847042E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.63725091E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47064670E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04557923E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.09361982E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.60848680E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55293513E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.55677079E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.20321977E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52827796E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.59319688E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.18579263E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.05795824E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.79587342E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.35173004E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.13491294E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57370732E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.68089843E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.51625667E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.07850736E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42629195E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.56207602E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.21299596E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52705874E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.04638618E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.37321950E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.05233288E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.01910330E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.35847042E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.63725091E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47064670E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04557923E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.09361982E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.60848680E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55293513E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47028122E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.09241734E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97190102E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.18192675E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.37930277E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93568098E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.73500940E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51451227E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999762E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.32489603E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.65049478E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.27875611E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47028122E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.09241734E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97190102E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.18192675E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.37930277E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93568098E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.73500940E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51451227E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999762E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.32489603E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.65049478E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.27875611E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51457635E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.76085284E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08178826E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15735890E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46755358E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84440277E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05382151E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.82281276E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59063154E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10124901E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85362172E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47019790E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09265315E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96666937E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.85969952E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.53694272E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94067723E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.11820978E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47019790E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09265315E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96666937E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.85969952E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.53694272E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94067723E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.11820978E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47022513E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09256519E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96637743E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.57107013E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.47798226E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94103001E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.71436327E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.38927587E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.19816867E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.27801261E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.53660182E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.09392925E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.48628937E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.15528256E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.30868956E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.90357328E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.11791137E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.54441579E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.45558421E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.21236624E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.29165170E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.58704438E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.29280612E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.29280612E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.61185748E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.91993437E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.30661495E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.30661495E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.26111342E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.26111342E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.28889457E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.28889457E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.12509250E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.33978932E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.90566508E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.36398374E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.36398374E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.36626090E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.02436997E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.26915457E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.26915457E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.32931688E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.32931688E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.62313569E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.62313569E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.56595085E-03   # h decays
#          BR         NDA      ID1       ID2
     5.68056892E-01    2           5        -5   # BR(h -> b       bb     )
     7.25181622E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.56718150E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.45178179E-04    2           3        -3   # BR(h -> s       sb     )
     2.36886091E-02    2           4        -4   # BR(h -> c       cb     )
     7.65036979E-02    2          21        21   # BR(h -> g       g      )
     2.60862827E-03    2          22        22   # BR(h -> gam     gam    )
     1.69942192E-03    2          22        23   # BR(h -> Z       gam    )
     2.26006248E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81164438E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.37071519E+01   # H decays
#          BR         NDA      ID1       ID2
     8.93286187E-01    2           5        -5   # BR(H -> b       bb     )
     7.37735458E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.60845629E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.20320429E-04    2           3        -3   # BR(H -> s       sb     )
     8.97872871E-08    2           4        -4   # BR(H -> c       cb     )
     8.95685145E-03    2           6        -6   # BR(H -> t       tb     )
     1.52056947E-05    2          21        21   # BR(H -> g       g      )
     7.52940617E-08    2          22        22   # BR(H -> gam     gam    )
     3.66854959E-09    2          23        22   # BR(H -> Z       gam    )
     8.46886041E-07    2          24       -24   # BR(H -> W+      W-     )
     4.22920979E-07    2          23        23   # BR(H -> Z       Z      )
     6.35905228E-06    2          25        25   # BR(H -> h       h      )
     6.60873652E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.30849041E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.30162301E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.49456464E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20907936E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.87573030E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.29803004E+01   # A decays
#          BR         NDA      ID1       ID2
     9.13017760E-01    2           5        -5   # BR(A -> b       bb     )
     7.53999794E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.66595936E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.27435169E-04    2           3        -3   # BR(A -> s       sb     )
     9.23975635E-08    2           4        -4   # BR(A -> c       cb     )
     9.21217708E-03    2           6        -6   # BR(A -> t       tb     )
     2.71289798E-05    2          21        21   # BR(A -> g       g      )
     6.92285817E-08    2          22        22   # BR(A -> gam     gam    )
     2.67332465E-08    2          23        22   # BR(A -> Z       gam    )
     8.62386935E-07    2          23        25   # BR(A -> Z       h      )
     9.39787949E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.66546332E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.69402453E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.92027701E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.63251201E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.45845653E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.84818249E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.42135029E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.33411026E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.42296624E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.92749913E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.18419588E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.84316158E-07    2          24        25   # BR(H+ -> W+      h      )
     5.36163870E-14    2          24        36   # BR(H+ -> W+      A      )
     4.96578094E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.14742667E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05843190E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
