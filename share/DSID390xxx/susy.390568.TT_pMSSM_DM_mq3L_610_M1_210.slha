#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11091887E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.55959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04182367E+01   # W+
        25     1.26015697E+02   # h
        35     4.00000592E+03   # H
        36     3.99999628E+03   # A
        37     4.00101769E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02016995E+03   # ~d_L
   2000001     4.01797399E+03   # ~d_R
   1000002     4.01951674E+03   # ~u_L
   2000002     4.01911497E+03   # ~u_R
   1000003     4.02016995E+03   # ~s_L
   2000003     4.01797399E+03   # ~s_R
   1000004     4.01951674E+03   # ~c_L
   2000004     4.01911497E+03   # ~c_R
   1000005     6.66089132E+02   # ~b_1
   2000005     4.02222897E+03   # ~b_2
   1000006     6.48042260E+02   # ~t_1
   2000006     2.07175113E+03   # ~t_2
   1000011     4.00352091E+03   # ~e_L
   2000011     4.00307485E+03   # ~e_R
   1000012     4.00240737E+03   # ~nu_eL
   1000013     4.00352091E+03   # ~mu_L
   2000013     4.00307485E+03   # ~mu_R
   1000014     4.00240737E+03   # ~nu_muL
   1000015     4.00483041E+03   # ~tau_1
   2000015     4.00759396E+03   # ~tau_2
   1000016     4.00435429E+03   # ~nu_tauL
   1000021     1.97020717E+03   # ~g
   1000022     2.02200229E+02   # ~chi_10
   1000023    -2.71014255E+02   # ~chi_20
   1000025     2.79708581E+02   # ~chi_30
   1000035     2.05468441E+03   # ~chi_40
   1000024     2.68062217E+02   # ~chi_1+
   1000037     2.05484899E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99217009E-01   # N_11
  1  2    -1.04113973E-02   # N_12
  1  3    -3.52012539E-01   # N_13
  1  4    -2.59591111E-01   # N_14
  2  1    -6.90968245E-02   # N_21
  2  2     2.54940048E-02   # N_22
  2  3    -7.00635335E-01   # N_23
  2  4     7.09708258E-01   # N_24
  3  1     4.32010977E-01   # N_31
  3  2     2.79610014E-02   # N_32
  3  3     6.20634611E-01   # N_33
  3  4     6.53756360E-01   # N_34
  4  1    -9.56536106E-04   # N_41
  4  2     9.99229624E-01   # N_42
  4  3    -3.15894064E-03   # N_43
  4  4    -3.91058206E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.47261724E-03   # U_11
  1  2     9.99989998E-01   # U_12
  2  1    -9.99989998E-01   # U_21
  2  2     4.47261724E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.53165197E-02   # V_11
  1  2    -9.98468869E-01   # V_12
  2  1    -9.98468869E-01   # V_21
  2  2    -5.53165197E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95102448E-01   # cos(theta_t)
  1  2    -9.88489655E-02   # sin(theta_t)
  2  1     9.88489655E-02   # -sin(theta_t)
  2  2     9.95102448E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999667E-01   # cos(theta_b)
  1  2    -8.16088163E-04   # sin(theta_b)
  2  1     8.16088163E-04   # -sin(theta_b)
  2  2     9.99999667E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05250681E-01   # cos(theta_tau)
  1  2     7.08958022E-01   # sin(theta_tau)
  2  1    -7.08958022E-01   # -sin(theta_tau)
  2  2    -7.05250681E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00297280E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10918868E+03  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43964515E+02   # higgs               
         4     1.61838835E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10918868E+03  # The gauge couplings
     1     3.61452395E-01   # gprime(Q) DRbar
     2     6.36322732E-01   # g(Q) DRbar
     3     1.03385755E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10918868E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.58431214E-07   # A_c(Q) DRbar
  3  3     2.55959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10918868E+03  # The trilinear couplings
  1  1     3.50815290E-07   # A_d(Q) DRbar
  2  2     3.50848597E-07   # A_s(Q) DRbar
  3  3     6.29707138E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10918868E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.67784299E-08   # A_mu(Q) DRbar
  3  3     7.75572568E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10918868E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71832541E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10918868E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83835767E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10918868E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03584920E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10918868E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58118479E+07   # M^2_Hd              
        22    -7.86458962E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40471031E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.56783975E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44176349E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44176349E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55823651E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55823651E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.10239941E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.84934854E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.21169092E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.83947048E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.16390374E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26508310E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.57780133E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15504710E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.00262111E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.27528288E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.46609260E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.61104090E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.13320962E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.36501810E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.64344411E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.74615504E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.41290929E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.17397516E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.76669605E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66753425E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52680817E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77625360E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63683573E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.79028807E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59505249E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.43874684E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14316190E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.43800386E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.62505467E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.62030787E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.26087841E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78543011E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22081553E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.33367457E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04252138E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78430544E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29984934E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55643551E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52785932E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61117510E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.46678386E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.62747607E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02651999E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.78720337E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44804186E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78562732E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83724715E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.28862619E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.50841027E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78912360E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.46499347E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58827510E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53005597E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54349204E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16532337E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.85471433E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.67804497E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.26857703E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85600167E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78543011E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22081553E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.33367457E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04252138E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78430544E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29984934E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55643551E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52785932E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61117510E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.46678386E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.62747607E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02651999E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.78720337E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44804186E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78562732E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83724715E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.28862619E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.50841027E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78912360E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.46499347E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58827510E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53005597E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54349204E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16532337E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.85471433E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.67804497E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.26857703E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85600167E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14357560E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27774342E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.61662564E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.79564179E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77588689E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.03387322E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56564046E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06847346E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09312415E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.75917818E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.85927907E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.99355693E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14357560E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27774342E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.61662564E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.79564179E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77588689E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.03387322E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56564046E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06847346E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09312415E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.75917818E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.85927907E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.99355693E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10022689E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92076713E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28713276E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.06941994E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39705440E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.45431247E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80109195E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10030812E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.04210387E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40664697E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53270299E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42223399E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10729177E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85156529E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14464929E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38543872E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.12912899E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.40000791E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77910142E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10912949E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54307649E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14464929E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38543872E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.12912899E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.40000791E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77910142E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10912949E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54307649E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47523330E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25426395E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.92754853E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17278260E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51771155E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.69752756E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02171800E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.59383278E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33452246E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33452246E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11151623E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11151623E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10792261E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.19508797E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.77724440E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.65041480E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.54044661E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.32842324E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.39642112E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.61780913E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.43588885E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.96743416E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.45490399E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17517243E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52399597E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17517243E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52399597E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45704014E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49242237E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49242237E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47080237E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98195853E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98195853E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98195819E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.92609337E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.92609337E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.92609337E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.92609337E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.75365721E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.75365721E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.75365721E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.75365721E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.51105411E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.51105411E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.96523920E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.42902614E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.34031528E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.16054918E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50284797E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.16054918E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50284797E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45784433E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.42633540E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.42633540E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.41433742E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.89278507E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.89278507E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.89277848E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.13869851E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.47677053E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.13869851E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.47677053E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.38468165E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.38468165E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.58932253E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.76507107E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.76507107E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.76507115E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.65476138E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.65476138E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.65476138E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.65476138E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.51582662E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.51582662E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.51582662E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.51582662E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.92565272E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.92565272E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.19406693E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.56442116E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.81576115E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.36715388E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.37989076E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.37989076E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.88284612E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.69308071E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.33819153E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85378170E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85378170E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.86028353E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.86028353E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09898087E-03   # h decays
#          BR         NDA      ID1       ID2
     5.86253263E-01    2           5        -5   # BR(h -> b       bb     )
     6.38136569E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25897852E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78712497E-04    2           3        -3   # BR(h -> s       sb     )
     2.07886206E-02    2           4        -4   # BR(h -> c       cb     )
     6.80785671E-02    2          21        21   # BR(h -> g       g      )
     2.38166008E-03    2          22        22   # BR(h -> gam     gam    )
     1.66869642E-03    2          22        23   # BR(h -> Z       gam    )
     2.27356355E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89545708E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49598133E+01   # H decays
#          BR         NDA      ID1       ID2
     3.51668522E-01    2           5        -5   # BR(H -> b       bb     )
     6.03276853E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13304083E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52301960E-04    2           3        -3   # BR(H -> s       sb     )
     7.07742432E-08    2           4        -4   # BR(H -> c       cb     )
     7.09305268E-03    2           6        -6   # BR(H -> t       tb     )
     8.42126040E-07    2          21        21   # BR(H -> g       g      )
     1.74187492E-10    2          22        22   # BR(H -> gam     gam    )
     1.82180355E-09    2          23        22   # BR(H -> Z       gam    )
     1.93974455E-06    2          24       -24   # BR(H -> W+      W-     )
     9.69200398E-07    2          23        23   # BR(H -> Z       Z      )
     7.39589724E-06    2          25        25   # BR(H -> h       h      )
     2.19988675E-24    2          36        36   # BR(H -> A       A      )
     6.61584268E-20    2          23        36   # BR(H -> Z       A      )
     1.85004347E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.62565031E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.62565031E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.94999272E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03341469E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.05223577E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82349924E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.83553547E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.01867178E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.43131263E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.95627286E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.32918993E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.03171742E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.56589604E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.56589604E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.39444876E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49569873E+01   # A decays
#          BR         NDA      ID1       ID2
     3.51712397E-01    2           5        -5   # BR(A -> b       bb     )
     6.03311204E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13316061E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52357280E-04    2           3        -3   # BR(A -> s       sb     )
     7.12268347E-08    2           4        -4   # BR(A -> c       cb     )
     7.10615493E-03    2           6        -6   # BR(A -> t       tb     )
     1.46013527E-05    2          21        21   # BR(A -> g       g      )
     5.32491770E-08    2          22        22   # BR(A -> gam     gam    )
     1.60537157E-08    2          23        22   # BR(A -> Z       gam    )
     1.93572613E-06    2          23        25   # BR(A -> Z       h      )
     1.85379494E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.62575525E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.62575525E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.69977731E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.28965337E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.69110100E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.49426695E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.47876584E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.02786786E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.03726784E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.04699119E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.22755996E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.62870374E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.62870374E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49809811E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.63172011E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03201908E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13277417E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.60429779E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20825685E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48577288E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.58716250E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.93704152E-06    2          24        25   # BR(H+ -> W+      h      )
     2.66082632E-14    2          24        36   # BR(H+ -> W+      A      )
     6.79519662E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.83872255E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.96721496E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62714450E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.05026648E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60068176E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.25706341E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.07103768E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.17966860E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
