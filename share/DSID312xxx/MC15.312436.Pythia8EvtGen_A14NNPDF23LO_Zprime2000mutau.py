###############################################################
#
# Job options file
# Pythia8 Z'->mutau
# July 2019
#===============================================================

evgenConfig.description = "Zprime(2000)->mutau production with the A14 NNPDF23LO tune"
evgenConfig.process = "Zprime -> mu+- tau-+"
evgenConfig.contact = [ "mcanobre@cern.ch" ]
evgenConfig.keywords = [ "muon", "tau", "exotic", "Zprime" ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",
				"Zprime:gmZmode = 3",
				"32:m0 = 2000.0", # set Z' mass
				"32:8:products = 13 -15",
				"32:10:products = 15 -13",
				"32:0:onMode = 0",
				"32:1:onMode = 0",
				"32:2:onMode = 0",
				"32:3:onMode = 0",
				"32:4:onMode = 0",
				"32:5:onMode = 0",
				"32:6:onMode = 0",
				"32:7:onMode = 0",
				"32:9:onMode = 0",
				"32:11:onMode = 0",
				"32:12:onMode = 0",
				"32:13:onMode = 0",
				"32:14:onMode = 0",
				"32:15:onMode = 0",
				"32:16:onMode = 0"]


#==============================================================
#
# End of job options file
#
###############################################################
