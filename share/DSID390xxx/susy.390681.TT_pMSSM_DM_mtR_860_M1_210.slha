#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13068184E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.50900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     8.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04188296E+01   # W+
        25     1.25822593E+02   # h
        35     4.00000631E+03   # H
        36     3.99999674E+03   # A
        37     4.00104894E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02608417E+03   # ~d_L
   2000001     4.02273139E+03   # ~d_R
   1000002     4.02543549E+03   # ~u_L
   2000002     4.02334155E+03   # ~u_R
   1000003     4.02608417E+03   # ~s_L
   2000003     4.02273139E+03   # ~s_R
   1000004     4.02543549E+03   # ~c_L
   2000004     4.02334155E+03   # ~c_R
   1000005     2.03540885E+03   # ~b_1
   2000005     4.02563853E+03   # ~b_2
   1000006     8.78134482E+02   # ~t_1
   2000006     2.04993066E+03   # ~t_2
   1000011     4.00451327E+03   # ~e_L
   2000011     4.00362987E+03   # ~e_R
   1000012     4.00340307E+03   # ~nu_eL
   1000013     4.00451327E+03   # ~mu_L
   2000013     4.00362987E+03   # ~mu_R
   1000014     4.00340307E+03   # ~nu_muL
   1000015     4.00506436E+03   # ~tau_1
   2000015     4.00735246E+03   # ~tau_2
   1000016     4.00482944E+03   # ~nu_tauL
   1000021     1.99058228E+03   # ~g
   1000022     1.98705509E+02   # ~chi_10
   1000023    -2.61639301E+02   # ~chi_20
   1000025     2.72905574E+02   # ~chi_30
   1000035     2.06379832E+03   # ~chi_40
   1000024     2.58419116E+02   # ~chi_1+
   1000037     2.06397803E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.73307787E-01   # N_11
  1  2    -1.18202080E-02   # N_12
  1  3    -3.87510167E-01   # N_13
  1  4    -2.95007902E-01   # N_14
  2  1    -7.06039264E-02   # N_21
  2  2     2.55573213E-02   # N_22
  2  3    -7.00315367E-01   # N_23
  2  4     7.09873436E-01   # N_24
  3  1     4.82024569E-01   # N_31
  3  2     2.71362771E-02   # N_32
  3  3     5.99487731E-01   # N_33
  3  4     6.38381075E-01   # N_34
  4  1    -9.53955004E-04   # N_41
  4  2     9.99235072E-01   # N_42
  4  3    -2.95238947E-03   # N_43
  4  4    -3.89826229E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.18039238E-03   # U_11
  1  2     9.99991262E-01   # U_12
  2  1    -9.99991262E-01   # U_21
  2  2     4.18039238E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.51420096E-02   # V_11
  1  2    -9.98478522E-01   # V_12
  2  1    -9.98478522E-01   # V_21
  2  2    -5.51420096E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.17759399E-01   # cos(theta_t)
  1  2     9.93042156E-01   # sin(theta_t)
  2  1    -9.93042156E-01   # -sin(theta_t)
  2  2    -1.17759399E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999479E-01   # cos(theta_b)
  1  2    -1.02078388E-03   # sin(theta_b)
  2  1     1.02078388E-03   # -sin(theta_b)
  2  2     9.99999479E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05453586E-01   # cos(theta_tau)
  1  2     7.08756120E-01   # sin(theta_tau)
  2  1    -7.08756120E-01   # -sin(theta_tau)
  2  2    -7.05453586E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00294116E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30681839E+03  # DRbar Higgs Parameters
         1    -2.50900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43574383E+02   # higgs               
         4     1.60966408E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30681839E+03  # The gauge couplings
     1     3.62100740E-01   # gprime(Q) DRbar
     2     6.35990443E-01   # g(Q) DRbar
     3     1.02938324E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30681839E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37187143E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30681839E+03  # The trilinear couplings
  1  1     5.03341933E-07   # A_d(Q) DRbar
  2  2     5.03388992E-07   # A_s(Q) DRbar
  3  3     9.01725669E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30681839E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09934261E-07   # A_mu(Q) DRbar
  3  3     1.11068821E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30681839E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67610538E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30681839E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83162904E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30681839E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03771251E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30681839E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58269203E+07   # M^2_Hd              
        22    -3.28332067E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     8.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40321769E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.77978983E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.23902097E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.33216696E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.18390884E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.22286235E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.76001212E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.49124606E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     4.98676098E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.22995841E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.69247932E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.43652681E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.96438498E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.07084851E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.92565044E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.31929514E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.82023231E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.59572346E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.55071658E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.10306254E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -7.34547928E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64227193E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58949553E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80546815E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63348473E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.13515861E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65716496E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.13399240E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12906836E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     5.06036416E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.66491404E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.51877570E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.15031926E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76113487E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.08905113E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.29905794E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.23565064E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83325402E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34554881E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65418405E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51253372E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58674273E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.29921204E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.80063977E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.30446241E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.81507963E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43683163E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76132451E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.78020825E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.33383904E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.63618544E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83812647E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.07246439E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68619931E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51476459E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51919748E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.12241489E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.31175053E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.40561531E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.34789871E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85297111E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76113487E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.08905113E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.29905794E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.23565064E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83325402E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34554881E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65418405E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51253372E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58674273E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.29921204E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.80063977E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.30446241E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.81507963E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43683163E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76132451E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.78020825E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.33383904E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.63618544E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83812647E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.07246439E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68619931E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51476459E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51919748E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.12241489E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.31175053E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.40561531E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.34789871E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85297111E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12775493E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.20773394E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.09482003E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.64162806E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77103324E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.78549137E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55579664E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07625759E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.63456171E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.97200941E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.31571326E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.93593098E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12775493E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.20773394E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.09482003E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.64162806E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77103324E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.78549137E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55579664E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07625759E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.63456171E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.97200941E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.31571326E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.93593098E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09680802E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.80167197E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.29126627E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.04756193E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38954659E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46111192E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78598169E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09609990E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.86036849E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.44855935E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.73658801E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41242050E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.13933611E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83183345E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12884008E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.32740745E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.20696374E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.11935076E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77421834E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10467223E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53332278E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12884008E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.32740745E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.20696374E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.11935076E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77421834E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10467223E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53332278E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45937463E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.20100898E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.99681729E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.82232860E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51133693E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.76463317E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00898974E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.37447129E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33485077E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33485077E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11162780E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11162780E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10704286E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.67756503E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.17333993E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.76786812E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44061982E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.61586495E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38480965E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.00422141E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38766076E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.19709090E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.54847128E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17639212E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52612986E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17639212E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52612986E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44394115E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50049136E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50049136E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47605999E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99909630E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99909630E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99909607E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.07791435E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.07791435E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.07791435E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.07791435E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.02597412E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.02597412E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.02597412E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.02597412E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.39823766E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.39823766E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.36109170E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.58066867E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.55400954E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.09076715E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.41229407E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.09076715E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.41229407E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.36574637E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.21731195E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.21731195E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.20463087E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.48706335E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.48706335E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.48705720E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.50506066E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.43942177E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.50506066E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.43942177E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     6.15028940E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.93603024E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.93603024E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.66890593E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.87016606E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.87016606E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.87016610E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.71458655E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.71458655E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.71458655E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.71458655E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.57150456E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.57150456E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.57150456E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.57150456E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.38902917E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.38902917E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.67795771E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.75233508E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.41181298E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.51906018E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.37691612E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.37691612E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.90931862E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.01118547E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.17857707E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.05323193E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.05323193E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.93723656E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.93723656E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.08628515E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90738998E-01    2           5        -5   # BR(h -> b       bb     )
     6.39127875E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26249597E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79601006E-04    2           3        -3   # BR(h -> s       sb     )
     2.08274797E-02    2           4        -4   # BR(h -> c       cb     )
     6.80942450E-02    2          21        21   # BR(h -> g       g      )
     2.37056884E-03    2          22        22   # BR(h -> gam     gam    )
     1.64587334E-03    2          22        23   # BR(h -> Z       gam    )
     2.23340062E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83641346E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43775150E+01   # H decays
#          BR         NDA      ID1       ID2
     3.47900228E-01    2           5        -5   # BR(H -> b       bb     )
     6.09737085E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15588265E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55003752E-04    2           3        -3   # BR(H -> s       sb     )
     7.15312269E-08    2           4        -4   # BR(H -> c       cb     )
     7.16891821E-03    2           6        -6   # BR(H -> t       tb     )
     9.16414324E-07    2          21        21   # BR(H -> g       g      )
     1.03367881E-10    2          22        22   # BR(H -> gam     gam    )
     1.84188290E-09    2          23        22   # BR(H -> Z       gam    )
     1.94316321E-06    2          24       -24   # BR(H -> W+      W-     )
     9.70908577E-07    2          23        23   # BR(H -> Z       Z      )
     7.30259821E-06    2          25        25   # BR(H -> h       h      )
    -1.14912451E-24    2          36        36   # BR(H -> A       A      )
     4.47367134E-20    2          23        36   # BR(H -> Z       A      )
     1.86077608E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63486625E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63486625E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.27524914E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.07472055E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.28318693E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.64995326E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.33833615E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.47023596E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.15227123E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.97172687E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.95516514E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.32666443E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.18634389E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.18634389E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43736420E+01   # A decays
#          BR         NDA      ID1       ID2
     3.47950869E-01    2           5        -5   # BR(A -> b       bb     )
     6.09783873E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15604638E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55064710E-04    2           3        -3   # BR(A -> s       sb     )
     7.19909960E-08    2           4        -4   # BR(A -> c       cb     )
     7.18239374E-03    2           6        -6   # BR(A -> t       tb     )
     1.47580020E-05    2          21        21   # BR(A -> g       g      )
     5.29060629E-08    2          22        22   # BR(A -> gam     gam    )
     1.62276222E-08    2          23        22   # BR(A -> Z       gam    )
     1.93919402E-06    2          23        25   # BR(A -> Z       h      )
     1.86405718E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63500697E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63500697E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.97318559E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33971107E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.05643118E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.29603556E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.06639331E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.46328241E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.28087498E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.10671979E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.82384880E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.26398226E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.26398226E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42957251E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.55204810E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10819576E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15970837E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55330773E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22351439E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51716255E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.53828588E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.94418987E-06    2          24        25   # BR(H+ -> W+      h      )
     3.12580975E-14    2          24        36   # BR(H+ -> W+      A      )
     6.45096770E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.34806879E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.42695809E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63950590E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.94731567E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61128379E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.18138948E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.55501833E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
