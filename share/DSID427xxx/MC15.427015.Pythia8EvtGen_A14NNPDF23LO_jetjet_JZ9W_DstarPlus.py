# JO for Pythia 8 jet jet JZ9W slice

evgenConfig.description = "Dijet truth jet slice JZ9W, with the A14 NNPDF23 LO tune and DstarPlusFilter"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.process = "pp to jetjet"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 2200."]

include("MC15JobOptions/JetFilter_JZ9W.py")
include("MC15JobOptions/DstarPlusFilter.py")

genSeq.EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02Kpi.DEC"
evgenConfig.auxfiles += [ 'DstarP2D0PiP_D02Kpi.DEC']

evgenConfig.minevents = 100
