#################################################################################
# job options fragment for hf->NegD10->rhomunu->mu4mu2mu1X 
# For HF tau3mu based on Meson Decay(MD) study for run2
################################################################################# 
# All production channels of bb and cc are included in this fragment.
# D_d meson force decaying into semileptonic channel munu and rho which also must decay into dimuon+X.
# D_d pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>4GeV, mu2>2GeV, mu3>1GeV, and D_d>10GeV
# original version from Dai Kobayashi
#################################################################################

f = open("NegD_RhoMuNu_3MuX_USER.DEC", "w")
f.write("Alias myRho0 rho0\n")
f.write("Decay D-\n")
f.write("1.0000   myRho0   mu-  anti-nu_mu   PHOTOS ISGW2;\n")
f.write("Enddecay\n")
f.write("Decay myRho0\n")
f.write("1.0000   mu+  mu-          PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description = "hf->NegD->rhomunu->3muX inclusive production"
evgenConfig.keywords = ["charmonium", "3muon", "heavyFlavour", "SM", "singlyResonant"]
evgenConfig.minevents = 200
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch']
evgenConfig.process = "hf>NegD>rhomunu>3muX"

genSeq.Pythia8B.Commands += ['HardQCD:all = on']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops = 1

genSeq.EvtInclusiveDecay.userDecayFile = "NegD_RhoMuNu_3MuX_USER.DEC"

from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
filtSeq += TripletChainFilter("LowPtTripletFilter")
filtSeq += TripletChainFilter("HighPtTripletFilter")
filtSeq.Expression = "LowPtTripletFilter or HighPtTripletFilter"

TripletChainFilter = filtSeq.LowPtTripletFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = 13
TripletChainFilter.PdgId2 = -13
TripletChainFilter.PdgId3 = 13
TripletChainFilter.NStep1 = 2
TripletChainFilter.NStep2 = 2
TripletChainFilter.NStep3 = 1
TripletChainFilter.PtMin1 = 3500
TripletChainFilter.PtMin2 = 3500
TripletChainFilter.PtMin3 = 3500
TripletChainFilter.EtaMax1 = 3.0
TripletChainFilter.EtaMax2 = 3.0
TripletChainFilter.EtaMax3 = 3.0
TripletChainFilter.TripletPdgId = -411
TripletChainFilter.TripletPtMin = 10000
TripletChainFilter.TripletEtaMax = 3.0
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.DoubletPdgId = 113
TripletChainFilter.DoubletPtMin = 0
TripletChainFilter.DoubletEtaMax = 100
TripletChainFilter.DoubletMassMin = 0
TripletChainFilter.DoubletMassMax = 10000000

TripletChainFilter2 = filtSeq.HighPtTripletFilter
TripletChainFilter2.NTriplet = 1
TripletChainFilter2.PdgId1 = 13
TripletChainFilter2.PdgId2 = -13
TripletChainFilter2.PdgId3 = 13
TripletChainFilter2.NStep1 = 2
TripletChainFilter2.NStep2 = 2
TripletChainFilter2.NStep3 = 1
TripletChainFilter2.PtMin1 = 10500
TripletChainFilter2.PtMin2 = 5500
TripletChainFilter2.PtMin3 = 2000
TripletChainFilter2.EtaMax1 = 3.0
TripletChainFilter2.EtaMax2 = 3.0
TripletChainFilter2.EtaMax3 = 3.0
TripletChainFilter2.TripletPdgId = -411
TripletChainFilter2.TripletPtMin = 10000
TripletChainFilter2.TripletEtaMax = 3.0
TripletChainFilter2.TripletMassMin = 0
TripletChainFilter2.TripletMassMax = 10000000
TripletChainFilter2.DoubletPdgId = 113
TripletChainFilter2.DoubletPtMin = 0
TripletChainFilter2.DoubletEtaMax = 100
TripletChainFilter2.DoubletMassMin = 0
TripletChainFilter2.DoubletMassMax = 10000000