include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'Production of ggF to CP mix higgs bosons decaying to WWmue'
evgenConfig.contact = ['Remco Castelijn <Remco.Castelijn@cern.ch>']
evgenConfig.keywords+=['gluonFusionHiggs','WW']
evgenConfig.inputfilecheck = 'MIX_mue'
evgenConfig.minevents=1000

genSeq.Pythia8.Commands += [ '25:onMode = off',      # decay of Higgs
                             '25:m0 = 125.0',        # Higgs mass
                             '25:mWidth = 0.00407',  # Higgs width
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off',        #decay of W
                             '24:oneChannel = 1 0.333 100 -15 16',
                             '24:addChannel = 2 0.667 100 -13 14',
                             '24:addChannel = 3 0.667 100 -11 12',
                             '15:onMode=off',        #decay of tau
                             '15:onIfAny=11 13' ]


