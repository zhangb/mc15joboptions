#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12002282E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04172176E+01   # W+
        25     1.25660724E+02   # h
        35     4.00000628E+03   # H
        36     3.99999653E+03   # A
        37     4.00103416E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02302053E+03   # ~d_L
   2000001     4.02024215E+03   # ~d_R
   1000002     4.02236612E+03   # ~u_L
   2000002     4.02125830E+03   # ~u_R
   1000003     4.02302053E+03   # ~s_L
   2000003     4.02024215E+03   # ~s_R
   1000004     4.02236612E+03   # ~c_L
   2000004     4.02125830E+03   # ~c_R
   1000005     7.71412662E+02   # ~b_1
   2000005     4.02379127E+03   # ~b_2
   1000006     7.55693629E+02   # ~t_1
   2000006     2.07084813E+03   # ~t_2
   1000011     4.00406763E+03   # ~e_L
   2000011     4.00320951E+03   # ~e_R
   1000012     4.00295214E+03   # ~nu_eL
   1000013     4.00406763E+03   # ~mu_L
   2000013     4.00320951E+03   # ~mu_R
   1000014     4.00295214E+03   # ~nu_muL
   1000015     4.00494854E+03   # ~tau_1
   2000015     4.00740758E+03   # ~tau_2
   1000016     4.00464774E+03   # ~nu_tauL
   1000021     1.97470392E+03   # ~g
   1000022     2.01926741E+02   # ~chi_10
   1000023    -2.71251132E+02   # ~chi_20
   1000025     2.79848217E+02   # ~chi_30
   1000035     2.05343583E+03   # ~chi_40
   1000024     2.68230856E+02   # ~chi_1+
   1000037     2.05360057E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99205457E-01   # N_11
  1  2    -1.04069517E-02   # N_12
  1  3    -3.52033214E-01   # N_13
  1  4    -2.59603268E-01   # N_14
  2  1    -6.91051650E-02   # N_21
  2  2     2.54819477E-02   # N_22
  2  3    -7.00636168E-01   # N_23
  2  4     7.09707057E-01   # N_24
  3  1     4.32033688E-01   # N_31
  3  2     2.79477971E-02   # N_32
  3  3     6.20621952E-01   # N_33
  3  4     6.53753934E-01   # N_34
  4  1    -9.56203777E-04   # N_41
  4  2     9.99230347E-01   # N_42
  4  3    -3.15746502E-03   # N_43
  4  4    -3.90874629E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.47052021E-03   # U_11
  1  2     9.99990007E-01   # U_12
  2  1    -9.99990007E-01   # U_21
  2  2     4.47052021E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52905047E-02   # V_11
  1  2    -9.98470310E-01   # V_12
  2  1    -9.98470310E-01   # V_21
  2  2    -5.52905047E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94833696E-01   # cos(theta_t)
  1  2    -1.01518064E-01   # sin(theta_t)
  2  1     1.01518064E-01   # -sin(theta_t)
  2  2     9.94833696E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999663E-01   # cos(theta_b)
  1  2    -8.20974961E-04   # sin(theta_b)
  2  1     8.20974961E-04   # -sin(theta_b)
  2  2     9.99999663E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05318254E-01   # cos(theta_tau)
  1  2     7.08890796E-01   # sin(theta_tau)
  2  1    -7.08890796E-01   # -sin(theta_tau)
  2  2    -7.05318254E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00291118E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20022822E+03  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43816219E+02   # higgs               
         4     1.61436077E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20022822E+03  # The gauge couplings
     1     3.61715818E-01   # gprime(Q) DRbar
     2     6.36409391E-01   # g(Q) DRbar
     3     1.03202442E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20022822E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14517934E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20022822E+03  # The trilinear couplings
  1  1     4.20097516E-07   # A_d(Q) DRbar
  2  2     4.20137058E-07   # A_s(Q) DRbar
  3  3     7.52385876E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20022822E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.15793530E-08   # A_mu(Q) DRbar
  3  3     9.25142461E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20022822E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69309715E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20022822E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82968880E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20022822E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03626366E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20022822E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58169447E+07   # M^2_Hd              
        22    -6.11640064E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40513754E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.05527611E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43900674E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43900674E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56099326E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56099326E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.45378440E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.01005970E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.30314698E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.91273182E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.08311523E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22337633E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.74284393E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19106948E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03277870E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.34537125E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.01977511E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.57132724E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.10073054E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.28443819E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.90379070E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.47749662E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.11561167E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.97860292E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.84282888E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66722749E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52952391E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77778441E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63853913E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.81313279E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59826710E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.48466752E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14228952E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.32744320E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.50197305E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.59904867E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.73710137E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78678487E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22445241E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.32951195E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04361794E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79470487E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29660249E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57722464E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52469679E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61134510E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.47562669E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.63331826E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02866894E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.79527134E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44693684E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78698540E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.84118711E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.28885935E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.51538890E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79952410E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.46287515E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60908542E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52688830E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54362751E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16768856E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.87029980E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.68378577E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.29030336E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85570618E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78678487E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22445241E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.32951195E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04361794E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79470487E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29660249E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57722464E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52469679E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61134510E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.47562669E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.63331826E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02866894E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.79527134E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44693684E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78698540E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.84118711E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.28885935E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.51538890E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79952410E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.46287515E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60908542E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52688830E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54362751E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16768856E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.87029980E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.68378577E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.29030336E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85570618E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14827080E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27791276E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.66210543E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.79553058E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77583493E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.02974787E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56553007E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07157909E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09295321E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.76020776E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.85943972E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.99461788E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14827080E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27791276E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.66210543E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.79553058E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77583493E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.02974787E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56553007E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07157909E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09295321E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.76020776E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.85943972E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.99461788E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10403369E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92105241E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28370354E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.06795783E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39730480E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44885839E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80159081E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10359579E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.04363615E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40252591E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53279396E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42191685E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10474358E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85092609E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14933441E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38551315E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.12784855E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.40118316E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77906038E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10282436E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54300142E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14933441E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38551315E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.12784855E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.40118316E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77906038E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10282436E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54300142E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47964004E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25452960E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.92669286E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17418894E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51784475E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68950101E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02198973E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.75778555E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33450192E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33450192E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11150924E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11150924E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10797768E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.86050922E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.70914514E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.57698603E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.89776011E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.72869858E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.74384248E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.92315254E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.80106332E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.10216534E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.38290374E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17498556E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52389499E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17498556E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52389499E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45706901E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49299079E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49299079E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47157677E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98338032E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98338032E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98338003E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.13865090E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.13865090E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.13865090E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.13865090E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.04621832E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.04621832E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.04621832E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.04621832E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.78571139E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.78571139E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.15964781E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.51806245E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.91762159E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.16086917E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50343097E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.16086917E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50343097E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45961693E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.42867436E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.42867436E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.41696685E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.89721500E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.89721500E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.89720933E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.02732124E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.33244948E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.02732124E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.33244948E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.05460394E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.05460394E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.32006952E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.10558734E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.10558734E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.10558741E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.55897031E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.55897031E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.55897031E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.55897031E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.19652563E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.19652563E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.19652563E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.19652563E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.63793149E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.63793149E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.85943989E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.76020219E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.02414332E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.49652163E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.72674459E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.72674459E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.10321293E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.84459552E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52856685E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.81766871E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.81766871E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.82564839E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.82564839E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05189735E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91940338E-01    2           5        -5   # BR(h -> b       bb     )
     6.43713147E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27873472E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83164341E-04    2           3        -3   # BR(h -> s       sb     )
     2.09824842E-02    2           4        -4   # BR(h -> c       cb     )
     6.85607468E-02    2          21        21   # BR(h -> g       g      )
     2.37778626E-03    2          22        22   # BR(h -> gam     gam    )
     1.63630746E-03    2          22        23   # BR(h -> Z       gam    )
     2.21379594E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80403906E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48396712E+01   # H decays
#          BR         NDA      ID1       ID2
     3.49880030E-01    2           5        -5   # BR(H -> b       bb     )
     6.04598597E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13771420E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52854738E-04    2           3        -3   # BR(H -> s       sb     )
     7.09275544E-08    2           4        -4   # BR(H -> c       cb     )
     7.10841765E-03    2           6        -6   # BR(H -> t       tb     )
     8.80009620E-07    2          21        21   # BR(H -> g       g      )
     1.37577700E-10    2          22        22   # BR(H -> gam     gam    )
     1.82579863E-09    2          23        22   # BR(H -> Z       gam    )
     1.91055364E-06    2          24       -24   # BR(H -> W+      W-     )
     9.54614998E-07    2          23        23   # BR(H -> Z       Z      )
     7.33507714E-06    2          25        25   # BR(H -> h       h      )
     1.39289284E-24    2          36        36   # BR(H -> A       A      )
    -2.40768185E-20    2          23        36   # BR(H -> Z       A      )
     1.85224226E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63059085E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63059085E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.95465717E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03542372E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.05484055E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82983097E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.83932277E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.02549643E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.45392244E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.98025484E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.34852781E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.07331658E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.53869042E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.53869042E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.36780831E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48362111E+01   # A decays
#          BR         NDA      ID1       ID2
     3.49927839E-01    2           5        -5   # BR(A -> b       bb     )
     6.04640028E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13785901E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52913109E-04    2           3        -3   # BR(A -> s       sb     )
     7.13837149E-08    2           4        -4   # BR(A -> c       cb     )
     7.12180655E-03    2           6        -6   # BR(A -> t       tb     )
     1.46335117E-05    2          21        21   # BR(A -> g       g      )
     5.33593660E-08    2          22        22   # BR(A -> gam     gam    )
     1.60862193E-08    2          23        22   # BR(A -> Z       gam    )
     1.90664971E-06    2          23        25   # BR(A -> Z       h      )
     1.85606251E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63071491E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63071491E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.70381991E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29221532E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.71287013E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.50215298E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48180164E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.03404572E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.03975861E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.07157835E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.24597932E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.60373870E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.60373870E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48541604E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.60186780E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.04598977E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13771386E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.58519232E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21105465E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49152886E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.56868011E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.90817704E-06    2          24        25   # BR(H+ -> W+      h      )
     2.88552032E-14    2          24        36   # BR(H+ -> W+      A      )
     6.81149761E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.85167397E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.97189527E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63230036E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.06378216E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60573762E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.26099921E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.09223114E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.12816737E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
