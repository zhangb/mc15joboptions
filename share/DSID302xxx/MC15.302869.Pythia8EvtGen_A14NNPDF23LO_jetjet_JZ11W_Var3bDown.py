# JO for Pythia 8 jet jet JZ11W slice

evgenConfig.description = "Dijet truth jet slice JZ11W, with the A14 NNPDF23 LO Var3bDown tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

evgenConfig.contact = ["marco.vanadia@cern.ch"]
evgenConfig.process = "QCD dijet"
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 3500."]

include("MC15JobOptions/JetFilter_JZ11W.py")
evgenConfig.minevents = 1000
