
#evgenConfig.generators = ["MadGraph"]

############################
# Shower JOs will go here
## pythia shower
#evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_EFT'
evgenConfig.keywords += ['top', 'photon']
evgenConfig.contact = ["sreelakshmi.sindhu@cern.ch"]
#evgenConfig.inputconfcheck ='412172'
evgenConfig.minevents = 9000
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

                                                                                                                                                                        


