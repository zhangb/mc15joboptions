#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Protos+Pythia8+EvtGen single top-quark t-channel production (2->3, SM) (lept.), A14 tune, ME CTEQ6L1 LO, A14 NNPDF23 LO'
evgenConfig.keywords = ["top", "singleTop", "tChannel", "leptonic"]
evgenConfig.contact  = ["cescobar@cern.ch"]
evgenConfig.inputfilecheck = "st_tchan_lept_tbj"
evgenConfig.minevents  = 5000

#--------------------------------------------------------------
# Base configuration for ProtosLHEF_i interface
#--------------------------------------------------------------
include('MC15JobOptions/ProtosLHEF_Common.py')
# genSeq.protos2lhef.OutputLevel = DEBUG

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_LHEF.py")

# include shower weights (not needed)
# include("MC15JobOptions/Pythia8_ShowerWeights.py")
