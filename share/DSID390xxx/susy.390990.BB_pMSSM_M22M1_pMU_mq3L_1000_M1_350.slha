#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17330148E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04025409E+01   # W+
        25     1.23493280E+02   # h
        35     3.00005855E+03   # H
        36     3.00000003E+03   # A
        37     3.00086831E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04348300E+03   # ~d_L
   2000001     3.03837220E+03   # ~d_R
   1000002     3.04257555E+03   # ~u_L
   2000002     3.03922257E+03   # ~u_R
   1000003     3.04348300E+03   # ~s_L
   2000003     3.03837220E+03   # ~s_R
   1000004     3.04257555E+03   # ~c_L
   2000004     3.03922257E+03   # ~c_R
   1000005     1.10426256E+03   # ~b_1
   2000005     3.03720158E+03   # ~b_2
   1000006     1.10292763E+03   # ~t_1
   2000006     3.02250260E+03   # ~t_2
   1000011     3.00622469E+03   # ~e_L
   2000011     3.00231467E+03   # ~e_R
   1000012     3.00483537E+03   # ~nu_eL
   1000013     3.00622469E+03   # ~mu_L
   2000013     3.00231467E+03   # ~mu_R
   1000014     3.00483537E+03   # ~nu_muL
   1000015     2.98549163E+03   # ~tau_1
   2000015     3.02196825E+03   # ~tau_2
   1000016     3.00450488E+03   # ~nu_tauL
   1000021     2.35428362E+03   # ~g
   1000022     3.51216385E+02   # ~chi_10
   1000023     7.33995829E+02   # ~chi_20
   1000025    -2.99405862E+03   # ~chi_30
   1000035     2.99501256E+03   # ~chi_40
   1000024     7.34159401E+02   # ~chi_1+
   1000037     2.99552487E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99883600E-01   # N_11
  1  2    -7.18828297E-04   # N_12
  1  3     1.50355478E-02   # N_13
  1  4    -2.49018943E-03   # N_14
  2  1     1.15373224E-03   # N_21
  2  2     9.99586931E-01   # N_22
  2  3    -2.76543335E-02   # N_23
  2  4     7.73777308E-03   # N_24
  3  1    -8.85830211E-03   # N_31
  3  2     1.40926253E-02   # N_32
  3  3     7.06878843E-01   # N_33
  3  4     7.07138762E-01   # N_34
  4  1    -1.23687041E-02   # N_41
  4  2     2.50369245E-02   # N_42
  4  3     7.06633902E-01   # N_43
  4  4    -7.07028074E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99234467E-01   # U_11
  1  2    -3.91213579E-02   # U_12
  2  1     3.91213579E-02   # U_21
  2  2     9.99234467E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99940074E-01   # V_11
  1  2    -1.09475567E-02   # V_12
  2  1     1.09475567E-02   # V_21
  2  2     9.99940074E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98708018E-01   # cos(theta_t)
  1  2    -5.08162846E-02   # sin(theta_t)
  2  1     5.08162846E-02   # -sin(theta_t)
  2  2     9.98708018E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99901298E-01   # cos(theta_b)
  1  2     1.40497067E-02   # sin(theta_b)
  2  1    -1.40497067E-02   # -sin(theta_b)
  2  2     9.99901298E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06966715E-01   # cos(theta_tau)
  1  2     7.07246820E-01   # sin(theta_tau)
  2  1    -7.07246820E-01   # -sin(theta_tau)
  2  2     7.06966715E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01756793E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73301479E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43977116E+02   # higgs               
         4     7.89438704E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73301479E+03  # The gauge couplings
     1     3.62658734E-01   # gprime(Q) DRbar
     2     6.37214232E-01   # g(Q) DRbar
     3     1.02308152E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73301479E+03  # The trilinear couplings
  1  1     2.84360520E-06   # A_u(Q) DRbar
  2  2     2.84364592E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73301479E+03  # The trilinear couplings
  1  1     7.86303232E-07   # A_d(Q) DRbar
  2  2     7.86435384E-07   # A_s(Q) DRbar
  3  3     1.68466701E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73301479E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.61038049E-07   # A_mu(Q) DRbar
  3  3     1.62954319E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73301479E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.48094289E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73301479E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.17439703E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73301479E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07501088E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73301479E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.55790547E+04   # M^2_Hd              
        22    -9.00986042E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40933796E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.83309462E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48137611E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48137611E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51862389E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51862389E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.14466786E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.97420579E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.88586993E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.81670949E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.66715395E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.70315042E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.16624064E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.37555102E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.50992711E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.44248293E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.52379722E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.46917402E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.88017907E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.97001586E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.36959243E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.58830278E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.07473797E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17631112E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.92207505E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.55947833E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.88113293E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.78786939E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.28987590E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30099260E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.87388066E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.18927077E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.99915004E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.67532101E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.10323240E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.56052233E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.08648770E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.92508270E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.12171421E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.52470160E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.25672938E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.13815823E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58535184E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.92756240E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.27223519E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.37971460E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41464586E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68058604E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.19356138E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55851734E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.16118990E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.49787457E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.11603304E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.98975793E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.26350600E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.63925542E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.50535475E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.47757864E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.48657688E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.51636213E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54946388E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.67532101E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.10323240E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.56052233E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.08648770E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.92508270E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.12171421E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.52470160E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.25672938E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.13815823E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58535184E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.92756240E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.27223519E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.37971460E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41464586E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68058604E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.19356138E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55851734E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.16118990E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.49787457E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.11603304E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.98975793E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.26350600E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.63925542E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.50535475E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.47757864E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.48657688E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.51636213E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54946388E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59995798E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05994730E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98414159E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80283032E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.05608227E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95591051E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.21285757E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52807102E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998784E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20989710E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.43370080E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.71282853E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59995798E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05994730E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98414159E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80283032E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.05608227E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95591051E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.21285757E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52807102E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998784E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20989710E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.43370080E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.71282853E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58954959E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68376643E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10844700E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20778657E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53580400E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.77075480E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07941774E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.70781325E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.88227158E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.14927435E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.94103996E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.60004895E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06476995E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97451127E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.32757984E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.47782272E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96071854E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.09361090E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.60004895E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06476995E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97451127E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.32757984E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.47782272E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96071854E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.09361090E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59989966E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06469050E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97422441E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.94698910E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.38041307E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96105961E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.52859653E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.96620626E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.68731770E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.80180850E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.94532002E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.88564004E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.68669755E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.41676508E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.48384496E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.15765485E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.74653591E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.61441559E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.43855844E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.69792028E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.64962133E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.81819879E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.46078555E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.46078555E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.78525264E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.88027340E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51355204E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.51355204E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.29036480E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.29036480E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.98487030E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.98487030E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.62584459E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.54376828E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.86525473E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.53641625E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.53641625E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.73704884E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.31713320E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.47583894E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.47583894E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31825191E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31825191E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.56730253E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.56730253E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.57312763E-03   # h decays
#          BR         NDA      ID1       ID2
     6.89430907E-01    2           5        -5   # BR(h -> b       bb     )
     5.63769536E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.99582011E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.24582459E-04    2           3        -3   # BR(h -> s       sb     )
     1.83322384E-02    2           4        -4   # BR(h -> c       cb     )
     5.81953032E-02    2          21        21   # BR(h -> g       g      )
     1.95597471E-03    2          22        22   # BR(h -> gam     gam    )
     1.19103266E-03    2          22        23   # BR(h -> Z       gam    )
     1.54990416E-01    2          24       -24   # BR(h -> W+      W-     )
     1.89030094E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40153205E+01   # H decays
#          BR         NDA      ID1       ID2
     7.51623381E-01    2           5        -5   # BR(H -> b       bb     )
     1.77427617E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.27341653E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.70378188E-04    2           3        -3   # BR(H -> s       sb     )
     2.17330200E-07    2           4        -4   # BR(H -> c       cb     )
     2.16800754E-02    2           6        -6   # BR(H -> t       tb     )
     1.98474380E-05    2          21        21   # BR(H -> g       g      )
     1.08174752E-08    2          22        22   # BR(H -> gam     gam    )
     8.37377409E-09    2          23        22   # BR(H -> Z       gam    )
     2.97205801E-05    2          24       -24   # BR(H -> W+      W-     )
     1.48419695E-05    2          23        23   # BR(H -> Z       Z      )
     7.91695053E-05    2          25        25   # BR(H -> h       h      )
    -4.16094636E-24    2          36        36   # BR(H -> A       A      )
     8.41141672E-20    2          23        36   # BR(H -> Z       A      )
     1.78741855E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.06216304E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.91590804E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.33638228E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.43032234E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.30371634E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.34112073E+01   # A decays
#          BR         NDA      ID1       ID2
     7.85567653E-01    2           5        -5   # BR(A -> b       bb     )
     1.85420595E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.55601997E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.05215389E-04    2           3        -3   # BR(A -> s       sb     )
     2.27220369E-07    2           4        -4   # BR(A -> c       cb     )
     2.26542151E-02    2           6        -6   # BR(A -> t       tb     )
     6.67144930E-05    2          21        21   # BR(A -> g       g      )
     5.32198808E-08    2          22        22   # BR(A -> gam     gam    )
     6.57359536E-08    2          23        22   # BR(A -> Z       gam    )
     3.09470017E-05    2          23        25   # BR(A -> Z       h      )
     2.59793746E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21403292E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29575062E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.83620603E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04602518E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.12105209E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.37798649E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.40798023E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.17472228E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94120251E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.01656425E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.28155487E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.97367783E-05    2          24        25   # BR(H+ -> W+      h      )
     6.20821272E-14    2          24        36   # BR(H+ -> W+      A      )
     1.85048618E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.34970553E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.91206375E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
