#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11627816E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03976665E+01   # W+
        25     1.25047926E+02   # h
        35     3.00021709E+03   # H
        36     2.99999962E+03   # A
        37     3.00097503E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02099408E+03   # ~d_L
   2000001     3.01565009E+03   # ~d_R
   1000002     3.02006955E+03   # ~u_L
   2000002     3.01781561E+03   # ~u_R
   1000003     3.02099408E+03   # ~s_L
   2000003     3.01565009E+03   # ~s_R
   1000004     3.02006955E+03   # ~c_L
   2000004     3.01781561E+03   # ~c_R
   1000005     5.03964124E+02   # ~b_1
   2000005     3.01522234E+03   # ~b_2
   1000006     5.00478314E+02   # ~t_1
   2000006     2.99413725E+03   # ~t_2
   1000011     3.00685457E+03   # ~e_L
   2000011     3.00097531E+03   # ~e_R
   1000012     3.00545416E+03   # ~nu_eL
   1000013     3.00685457E+03   # ~mu_L
   2000013     3.00097531E+03   # ~mu_R
   1000014     3.00545416E+03   # ~nu_muL
   1000015     2.98603176E+03   # ~tau_1
   2000015     3.02223789E+03   # ~tau_2
   1000016     3.00562434E+03   # ~nu_tauL
   1000021     2.32545712E+03   # ~g
   1000022     1.00765956E+02   # ~chi_10
   1000023     2.14720449E+02   # ~chi_20
   1000025    -3.00334258E+03   # ~chi_30
   1000035     3.00380470E+03   # ~chi_40
   1000024     2.14879684E+02   # ~chi_1+
   1000037     3.00449602E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888474E-01   # N_11
  1  2    -1.53963189E-03   # N_12
  1  3     1.48038458E-02   # N_13
  1  4    -1.23068340E-03   # N_14
  2  1     1.93132310E-03   # N_21
  2  2     9.99649496E-01   # N_22
  2  3    -2.62274376E-02   # N_23
  2  4     3.04588346E-03   # N_24
  3  1    -9.56935448E-03   # N_31
  3  2     1.64087410E-02   # N_32
  3  3     7.06829968E-01   # N_33
  3  4     7.07128402E-01   # N_34
  4  1    -1.13020505E-02   # N_41
  4  2     2.07187994E-02   # N_42
  4  3     7.06742077E-01   # N_43
  4  4    -7.07077528E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99310844E-01   # U_11
  1  2    -3.71192317E-02   # U_12
  2  1     3.71192317E-02   # U_21
  2  2     9.99310844E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990703E-01   # V_11
  1  2    -4.31203011E-03   # V_12
  2  1     4.31203011E-03   # V_21
  2  2     9.99990703E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98898904E-01   # cos(theta_t)
  1  2    -4.69145989E-02   # sin(theta_t)
  2  1     4.69145989E-02   # -sin(theta_t)
  2  2     9.98898904E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99922654E-01   # cos(theta_b)
  1  2     1.24372834E-02   # sin(theta_b)
  2  1    -1.24372834E-02   # -sin(theta_b)
  2  2     9.99922654E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06891660E-01   # cos(theta_tau)
  1  2     7.07321837E-01   # sin(theta_tau)
  2  1    -7.07321837E-01   # -sin(theta_tau)
  2  2     7.06891660E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01939559E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.16278164E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44817493E+02   # higgs               
         4     6.48395920E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.16278164E+03  # The gauge couplings
     1     3.61317550E-01   # gprime(Q) DRbar
     2     6.39536568E-01   # g(Q) DRbar
     3     1.03204272E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.16278164E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.22562371E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.16278164E+03  # The trilinear couplings
  1  1     3.01881261E-07   # A_d(Q) DRbar
  2  2     3.01943332E-07   # A_s(Q) DRbar
  3  3     7.05325635E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.16278164E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.63583152E-08   # A_mu(Q) DRbar
  3  3     6.71474574E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.16278164E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.57559644E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.16278164E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.10935355E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.16278164E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05008495E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.16278164E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.04387606E+04   # M^2_Hd              
        22    -9.14738438E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41984494E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.41386257E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48254355E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48254355E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51745645E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51745645E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.87730623E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.34052609E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.48735154E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.37859585E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05692840E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.16370817E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.74232838E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.50285125E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.15795828E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.65709354E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52442690E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.03590529E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.49782431E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.06691123E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.15043727E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.64287161E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.25789755E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.87590115E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.88134530E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.93431123E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.84633843E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.83127932E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28629423E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.91255988E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.23612408E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.11207957E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.08079095E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.74388221E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63535649E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.16154606E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.91666135E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27052057E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.82691468E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.03668398E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20921954E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57036917E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.81803059E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.59597670E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.08975532E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42962497E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08589215E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.93245347E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63232620E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.90090459E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.33041825E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26477105E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.48343103E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.04357735E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69591983E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46785073E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.65526791E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.31487612E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.29134053E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55321326E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.08079095E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.74388221E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63535649E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.16154606E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.91666135E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27052057E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.82691468E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.03668398E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20921954E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57036917E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.81803059E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.59597670E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.08975532E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42962497E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08589215E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.93245347E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63232620E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.90090459E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.33041825E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26477105E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.48343103E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.04357735E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69591983E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46785073E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.65526791E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.31487612E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.29134053E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55321326E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01846295E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.64022720E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01776759E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.00858252E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.57125625E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01820967E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.06298427E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55498267E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996299E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.70106650E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01846295E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.64022720E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01776759E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.00858252E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.57125625E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01820967E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.06298427E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55498267E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996299E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.70106650E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81581020E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.44835935E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18812341E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36351724E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75584964E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.52963292E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16121100E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.86675470E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.02820077E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30892904E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.80887676E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01880983E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.74049882E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00293323E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.85698767E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69152435E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02301688E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.59829028E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.01880983E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.74049882E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00293323E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.85698767E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69152435E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02301688E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.59829028E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.01937784E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.73967627E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00268237E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.33520599E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.27472726E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02335000E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.25029288E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.02526657E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.61766762E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.68822281E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.33185620E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.16195611E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.65844930E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.05430843E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.51172222E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.83452057E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.68765909E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.05666517E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.23568976E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.06863950E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.47528893E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.47528893E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.05541886E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.58681233E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.69067618E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.69067618E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.21953132E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.21953132E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.95845481E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.95845481E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.95845481E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.95845481E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.61976735E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.61976735E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.96373650E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.95008333E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.57999183E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54405224E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.54405224E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.27906003E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.25510297E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.67065219E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.67065219E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.24474649E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.24474649E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.94183478E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.94183478E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.94183478E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.94183478E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.78147416E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.78147416E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.94247266E-03   # h decays
#          BR         NDA      ID1       ID2
     6.78311527E-01    2           5        -5   # BR(h -> b       bb     )
     5.28607381E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.87128406E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.97115595E-04    2           3        -3   # BR(h -> s       sb     )
     1.71338530E-02    2           4        -4   # BR(h -> c       cb     )
     5.63232039E-02    2          21        21   # BR(h -> g       g      )
     1.90344694E-03    2          22        22   # BR(h -> gam     gam    )
     1.26961662E-03    2          22        23   # BR(h -> Z       gam    )
     1.70309572E-01    2          24       -24   # BR(h -> W+      W-     )
     2.13037986E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38999145E+01   # H decays
#          BR         NDA      ID1       ID2
     7.40578434E-01    2           5        -5   # BR(H -> b       bb     )
     1.78909860E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.32582512E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.76808529E-04    2           3        -3   # BR(H -> s       sb     )
     2.19304210E-07    2           4        -4   # BR(H -> c       cb     )
     2.18770210E-02    2           6        -6   # BR(H -> t       tb     )
     3.45303334E-05    2          21        21   # BR(H -> g       g      )
     4.22481038E-08    2          22        22   # BR(H -> gam     gam    )
     8.37579691E-09    2          23        22   # BR(H -> Z       gam    )
     3.52263300E-05    2          24       -24   # BR(H -> W+      W-     )
     1.75914437E-05    2          23        23   # BR(H -> Z       Z      )
     8.90665472E-05    2          25        25   # BR(H -> h       h      )
    -1.62170405E-23    2          36        36   # BR(H -> A       A      )
     3.78849470E-17    2          23        36   # BR(H -> Z       A      )
     2.41149127E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13562407E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20066740E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.40111713E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.25764664E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.30971132E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31709263E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81626253E-01    2           5        -5   # BR(A -> b       bb     )
     1.88803250E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.67562241E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.19905065E-04    2           3        -3   # BR(A -> s       sb     )
     2.31365587E-07    2           4        -4   # BR(A -> c       cb     )
     2.30674996E-02    2           6        -6   # BR(A -> t       tb     )
     6.79315879E-05    2          21        21   # BR(A -> g       g      )
     3.36447118E-08    2          22        22   # BR(A -> gam     gam    )
     6.68789926E-08    2          23        22   # BR(A -> Z       gam    )
     3.70308521E-05    2          23        25   # BR(A -> Z       h      )
     2.65932070E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22408348E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32403120E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.04475274E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.79126870E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.07929891E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.54055148E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.98277037E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.90750205E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.27897091E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.08605407E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.04047414E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.98926209E-05    2          24        25   # BR(H+ -> W+      h      )
     1.18653236E-13    2          24        36   # BR(H+ -> W+      A      )
     2.12466298E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.92032450E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.65995467E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
