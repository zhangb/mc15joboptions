include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa 2.2.5 ttbar production with tt+0,1j@NLO(QCD+EWvirt)+2,3,4j@LO in the l^+ + jets channel."
evgenConfig.keywords = ["SM", "top", "ttbar", "1lepton"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "ttbar" # universal for all decay channels

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{sqr(172.5)+0.5*(PPerp2(p[2])+PPerp2(p[3]))};
  EXCLUSIVE_CLUSTER_MODE 1;

  %tags for process setup
  NJET:=4; LJET:=2,3; QCUT:=30.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  OL_PARAMETERS=preset 2 ew_renorm_scheme 1 add_associated_ew 3;
  INTEGRATION_ERROR=0.05;

  %decay settings
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
  MASS[5]=4.95

  % alphas scale factor 1 for better high jet multi
  CSS_IS_AS_FAC=1.0

  % stores veto information as named weight instead of doing a veto
  SHERPA_LDADD=FUSING_Hook
  USERHOOK = FUSING_HOOK
  FUSING_STORE_IN_HEPMC=1
}(run)

(processes){
  Process : 93 93 ->  6 -6 93{NJET};
  NLO_QCD_Mode 3 {LJET}; CKKW sqr(QCUT/E_CMS);
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN;
  Order (*,0);
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0", "OL_PARAMETERS=preset=2=ew_renorm_scheme=1=add_associated_ew=3" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "SCALE_VARIATIONS=None", "PDF_VARIATIONS=None" ]

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptt", "ppttj", "pptt_ew", "ppttj_ew" ]
genSeq.Sherpa_i.ExtraFiles = [ "libFUSING_Hook.so" ]

