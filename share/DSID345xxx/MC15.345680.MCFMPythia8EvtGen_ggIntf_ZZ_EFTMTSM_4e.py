##########################################################################################################
# 
# MCFM 6.8, gg->(H->)ZZ interference->4e
#  mll > 4 GeV,
# lepton pt > 2 GeV, |eta|<3 and leading lepton pt > 5 GeV
#

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'MCFM', 'Pythia8' ]
evgenConfig.description = 'MCFM, gg->(H->)ZZ interference->4e using CT10NNLO PDF'

evgenConfig.keywords = ['diboson', '4lepton', 'electroweak', 'Higgs', 'ZZ']
evgenConfig.contact = ['lailin.xu@cern.ch', 'heling.zhu@cern.ch']
evgenConfig.inputfilecheck = 'MCFMPythia8EvtGen_ggIntf_ZZ_EFTMTSM_4e'
evgenConfig.minevents = 5000

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_LHEF.py')

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]

## m4l filter
include("MC15JobOptions/FourLeptonInvMassFilter.py")
filtSeq.FourLeptonInvMassFilter.MinPt = 3.*GeV
filtSeq.FourLeptonInvMassFilter.MaxEta = 5.
filtSeq.FourLeptonInvMassFilter.MinMass = 130.*GeV
filtSeq.FourLeptonInvMassFilter.MaxMass = 13000.*GeV
