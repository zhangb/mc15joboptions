include('MC15JobOptions/MadGraphPythia8EvtGenControl_photonHF.py')

evgenConfig.description = "Diphoton + bb suitable for hh->bbaa searches"
evgenConfig.process = "Part of suite of samples with diphoton and dijet mass cuts loosely consistent with hh production"
evgenConfig.keywords = ["jets","dijet","2jet","diphoton","heavyFlavour","bottom"]
