#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954640E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04055170E+01   # W+
        25     1.24964088E+02   # h
        35     3.00013275E+03   # H
        36     2.99999987E+03   # A
        37     3.00092686E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03879469E+03   # ~d_L
   2000001     3.03375021E+03   # ~d_R
   1000002     3.03789207E+03   # ~u_L
   2000002     3.03484289E+03   # ~u_R
   1000003     3.03879469E+03   # ~s_L
   2000003     3.03375021E+03   # ~s_R
   1000004     3.03789207E+03   # ~c_L
   2000004     3.03484289E+03   # ~c_R
   1000005     9.54554473E+02   # ~b_1
   2000005     3.03276712E+03   # ~b_2
   1000006     9.50409361E+02   # ~t_1
   2000006     3.01618367E+03   # ~t_2
   1000011     3.00622637E+03   # ~e_L
   2000011     3.00204627E+03   # ~e_R
   1000012     3.00484419E+03   # ~nu_eL
   1000013     3.00622637E+03   # ~mu_L
   2000013     3.00204627E+03   # ~mu_R
   1000014     3.00484419E+03   # ~nu_muL
   1000015     2.98548026E+03   # ~tau_1
   2000015     3.02201974E+03   # ~tau_2
   1000016     3.00461568E+03   # ~nu_tauL
   1000021     2.34803973E+03   # ~g
   1000022     4.02022185E+02   # ~chi_10
   1000023     8.34600206E+02   # ~chi_20
   1000025    -2.99589293E+03   # ~chi_30
   1000035     2.99693933E+03   # ~chi_40
   1000024     8.34761512E+02   # ~chi_1+
   1000037     2.99740343E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99881912E-01   # N_11
  1  2    -6.79799260E-04   # N_12
  1  3     1.51043336E-02   # N_13
  1  4    -2.74952641E-03   # N_14
  2  1     1.12960036E-03   # N_21
  2  2     9.99563248E-01   # N_22
  2  3    -2.81883866E-02   # N_23
  2  4     8.80072137E-03   # N_24
  3  1    -8.72445260E-03   # N_31
  3  2     1.37184916E-02   # N_32
  3  3     7.06886451E-01   # N_33
  3  4     7.07140178E-01   # N_34
  4  1    -1.26004304E-02   # N_41
  4  2     2.61659070E-02   # N_42
  4  3     7.06603721E-01   # N_43
  4  4    -7.07013264E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99204632E-01   # U_11
  1  2    -3.98761129E-02   # U_12
  2  1     3.98761129E-02   # U_21
  2  2     9.99204632E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99922483E-01   # V_11
  1  2    -1.24510490E-02   # V_12
  2  1     1.24510490E-02   # V_21
  2  2     9.99922483E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98507512E-01   # cos(theta_t)
  1  2    -5.46145446E-02   # sin(theta_t)
  2  1     5.46145446E-02   # -sin(theta_t)
  2  2     9.98507512E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99909048E-01   # cos(theta_b)
  1  2     1.34868724E-02   # sin(theta_b)
  2  1    -1.34868724E-02   # -sin(theta_b)
  2  2     9.99909048E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06965468E-01   # cos(theta_tau)
  1  2     7.07248066E-01   # sin(theta_tau)
  2  1    -7.07248066E-01   # -sin(theta_tau)
  2  2     7.06965468E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01789002E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59546404E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44061295E+02   # higgs               
         4     7.41795572E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59546404E+03  # The gauge couplings
     1     3.62380412E-01   # gprime(Q) DRbar
     2     6.36833375E-01   # g(Q) DRbar
     3     1.02496942E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59546404E+03  # The trilinear couplings
  1  1     2.43882534E-06   # A_u(Q) DRbar
  2  2     2.43885779E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59546404E+03  # The trilinear couplings
  1  1     6.76343775E-07   # A_d(Q) DRbar
  2  2     6.76455342E-07   # A_s(Q) DRbar
  3  3     1.44889185E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59546404E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.33707083E-07   # A_mu(Q) DRbar
  3  3     1.35255833E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59546404E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51851457E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59546404E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15890888E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59546404E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07894331E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59546404E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.61261031E+04   # M^2_Hd              
        22    -9.05062268E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40752328E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.65329679E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47709361E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47709361E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52290639E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52290639E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.05401005E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.65675197E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.34324803E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12390208E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.69496734E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.16739166E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.38120203E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.32575853E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.97003922E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70565614E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60285403E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.13877271E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.22882900E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.01668036E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.98331964E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.20043296E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.85585886E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.17139325E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.06550477E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.97676680E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.97062268E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29859786E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.91457134E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.20396863E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.03556821E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.57148414E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.13208957E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52405037E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.23986472E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.28449725E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.04879544E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.44452059E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.36583180E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15030620E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56297624E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.77089435E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.23291724E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.82668114E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43702171E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.57681906E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.21762187E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52211851E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.78659968E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.24534929E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.04316873E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.75284710E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.37253017E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65495066E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43526380E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.02481749E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.48098980E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.90085881E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55647304E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.57148414E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.13208957E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52405037E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.23986472E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.28449725E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.04879544E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.44452059E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.36583180E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15030620E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56297624E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.77089435E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.23291724E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.82668114E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43702171E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.57681906E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.21762187E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52211851E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.78659968E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.24534929E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.04316873E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.75284710E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.37253017E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65495066E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43526380E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.02481749E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.48098980E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.90085881E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55647304E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47382745E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08749657E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97493144E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.26005081E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.79413207E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93757154E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.81406938E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51246959E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998870E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12667999E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.32389363E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.90282427E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47382745E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08749657E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97493144E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.26005081E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.79413207E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93757154E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.81406938E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51246959E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998870E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12667999E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.32389363E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.90282427E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51768143E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75051631E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08616261E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16332108E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46599795E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.83868232E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05675702E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53582686E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.69591056E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10406314E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.74344088E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47386037E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09215701E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96544080E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.31066992E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.07065444E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94240201E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.64714566E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47386037E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09215701E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96544080E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.31066992E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.07065444E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94240201E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.64714566E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47385022E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09207108E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96514854E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.09402558E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.00980560E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94276335E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.68794216E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.70735365E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.13800341E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.83593757E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.08273707E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.84132483E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.25978423E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.28851831E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.06828327E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.72006747E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.46437015E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.84140254E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.41585975E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.15590512E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.59582298E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.67102768E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.04044350E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.04044350E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.93341911E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.58906269E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.58551100E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.58551100E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27978953E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27978953E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.92906483E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.92906483E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.07416016E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.73040438E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.57906250E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.11717593E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.11717593E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.68127290E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.16844889E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.54715036E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.54715036E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31036037E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31036037E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.77059719E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.77059719E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.79861060E-03   # h decays
#          BR         NDA      ID1       ID2
     6.71487604E-01    2           5        -5   # BR(h -> b       bb     )
     5.43763008E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92493850E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.08558446E-04    2           3        -3   # BR(h -> s       sb     )
     1.76380289E-02    2           4        -4   # BR(h -> c       cb     )
     5.73054376E-02    2          21        21   # BR(h -> g       g      )
     1.95918166E-03    2          22        22   # BR(h -> gam     gam    )
     1.29827161E-03    2          22        23   # BR(h -> Z       gam    )
     1.73620613E-01    2          24       -24   # BR(h -> W+      W-     )
     2.17135096E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40629297E+01   # H decays
#          BR         NDA      ID1       ID2
     7.37764757E-01    2           5        -5   # BR(H -> b       bb     )
     1.76831262E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.25233085E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.67786338E-04    2           3        -3   # BR(H -> s       sb     )
     2.16626877E-07    2           4        -4   # BR(H -> c       cb     )
     2.16099262E-02    2           6        -6   # BR(H -> t       tb     )
     2.97208283E-05    2          21        21   # BR(H -> g       g      )
     4.33780414E-08    2          22        22   # BR(H -> gam     gam    )
     8.33943924E-09    2          23        22   # BR(H -> Z       gam    )
     3.05068738E-05    2          24       -24   # BR(H -> W+      W-     )
     1.52346375E-05    2          23        23   # BR(H -> Z       Z      )
     7.98423458E-05    2          25        25   # BR(H -> h       h      )
    -5.40226013E-24    2          36        36   # BR(H -> A       A      )
     3.16236454E-18    2          23        36   # BR(H -> Z       A      )
     1.59555962E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.03745071E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.95966716E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.00522459E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.91439737E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.69490116E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32514969E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83012491E-01    2           5        -5   # BR(A -> b       bb     )
     1.87655320E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63503442E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.14920010E-04    2           3        -3   # BR(A -> s       sb     )
     2.29958875E-07    2           4        -4   # BR(A -> c       cb     )
     2.29272483E-02    2           6        -6   # BR(A -> t       tb     )
     6.75185541E-05    2          21        21   # BR(A -> g       g      )
     6.46768482E-08    2          22        22   # BR(A -> gam     gam    )
     6.65623382E-08    2          23        22   # BR(A -> Z       gam    )
     3.22514558E-05    2          23        25   # BR(A -> Z       h      )
     2.61105317E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.23022553E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30238387E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.89925823E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03055972E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09776997E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.41371969E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.53432409E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.02571685E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.01543960E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03183720E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.14053911E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.15350796E-05    2          24        25   # BR(H+ -> W+      h      )
     8.73963436E-14    2          24        36   # BR(H+ -> W+      A      )
     1.83226499E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.17956805E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.96600980E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
