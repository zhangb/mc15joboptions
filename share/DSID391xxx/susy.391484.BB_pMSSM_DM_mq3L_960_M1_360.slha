#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13002981E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.89990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04189883E+01   # W+
        25     1.26077728E+02   # h
        35     4.00000820E+03   # H
        36     3.99999689E+03   # A
        37     4.00104982E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02590751E+03   # ~d_L
   2000001     4.02256614E+03   # ~d_R
   1000002     4.02525809E+03   # ~u_L
   2000002     4.02329776E+03   # ~u_R
   1000003     4.02590751E+03   # ~s_L
   2000003     4.02256614E+03   # ~s_R
   1000004     4.02525809E+03   # ~c_L
   2000004     4.02329776E+03   # ~c_R
   1000005     1.01572592E+03   # ~b_1
   2000005     4.02543364E+03   # ~b_2
   1000006     9.88312025E+02   # ~t_1
   2000006     1.80258131E+03   # ~t_2
   1000011     4.00453178E+03   # ~e_L
   2000011     4.00344614E+03   # ~e_R
   1000012     4.00342139E+03   # ~nu_eL
   1000013     4.00453178E+03   # ~mu_L
   2000013     4.00344614E+03   # ~mu_R
   1000014     4.00342139E+03   # ~nu_muL
   1000015     4.00440245E+03   # ~tau_1
   2000015     4.00785995E+03   # ~tau_2
   1000016     4.00485100E+03   # ~nu_tauL
   1000021     1.97719008E+03   # ~g
   1000022     3.48135992E+02   # ~chi_10
   1000023    -4.01332973E+02   # ~chi_20
   1000025     4.15147839E+02   # ~chi_30
   1000035     2.05213571E+03   # ~chi_40
   1000024     3.99018974E+02   # ~chi_1+
   1000037     2.05230016E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40447519E-01   # N_11
  1  2    -1.55975762E-02   # N_12
  1  3    -4.09422897E-01   # N_13
  1  4    -3.54651343E-01   # N_14
  2  1    -4.34957177E-02   # N_21
  2  2     2.40456211E-02   # N_22
  2  3    -7.03938412E-01   # N_23
  2  4     7.08520037E-01   # N_24
  3  1     5.40143475E-01   # N_31
  3  2     2.81613565E-02   # N_32
  3  3     5.80353233E-01   # N_33
  3  4     6.08803819E-01   # N_34
  4  1    -1.05720100E-03   # N_41
  4  2     9.99192405E-01   # N_42
  4  3    -5.80759294E-03   # N_43
  4  4    -3.97453453E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.21897910E-03   # U_11
  1  2     9.99966224E-01   # U_12
  2  1    -9.99966224E-01   # U_21
  2  2     8.21897910E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62189250E-02   # V_11
  1  2    -9.98418466E-01   # V_12
  2  1    -9.98418466E-01   # V_21
  2  2    -5.62189250E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85708195E-01   # cos(theta_t)
  1  2    -1.68461730E-01   # sin(theta_t)
  2  1     1.68461730E-01   # -sin(theta_t)
  2  2     9.85708195E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999190E-01   # cos(theta_b)
  1  2    -1.27279195E-03   # sin(theta_b)
  2  1     1.27279195E-03   # -sin(theta_b)
  2  2     9.99999190E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06006078E-01   # cos(theta_tau)
  1  2     7.08205774E-01   # sin(theta_tau)
  2  1    -7.08205774E-01   # -sin(theta_tau)
  2  2    -7.06006078E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00276326E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30029814E+03  # DRbar Higgs Parameters
         1    -3.89990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43597320E+02   # higgs               
         4     1.60987219E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30029814E+03  # The gauge couplings
     1     3.61918076E-01   # gprime(Q) DRbar
     2     6.35965780E-01   # g(Q) DRbar
     3     1.03007600E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30029814E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36769718E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30029814E+03  # The trilinear couplings
  1  1     5.03256736E-07   # A_d(Q) DRbar
  2  2     5.03303402E-07   # A_s(Q) DRbar
  3  3     9.00890101E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30029814E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11145837E-07   # A_mu(Q) DRbar
  3  3     1.12285627E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30029814E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68657337E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30029814E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85961638E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30029814E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03302261E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30029814E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57389220E+07   # M^2_Hd              
        22    -1.03314281E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40310193E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.68922348E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.36947257E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.36947257E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.61900696E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.61900696E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     1.15204720E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     1.15204720E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.06776599E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.13146162E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.17660099E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.55612422E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13581317E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11126578E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.44567070E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08460674E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.18588195E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.10851842E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.66577507E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.30813061E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.46981390E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.12518941E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.74583499E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.02780299E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.44064546E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.87857166E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65958111E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.56042608E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82883416E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62808546E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.21137540E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67484647E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.40157367E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12314351E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.63524519E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.21814108E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.68385599E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.38986639E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77791413E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.78965397E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14788148E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46703281E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82448691E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.44350804E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.63682340E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51564380E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60068223E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.90571492E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.04110000E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.60334119E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.45119940E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44805294E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77811036E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.65030295E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.98099990E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.74691530E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82970028E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.16352476E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66940513E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51782506E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53379083E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01881848E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.71573858E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.18235842E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.00034394E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85602290E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77791413E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.78965397E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14788148E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46703281E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82448691E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.44350804E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.63682340E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51564380E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60068223E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.90571492E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.04110000E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.60334119E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.45119940E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44805294E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77811036E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.65030295E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.98099990E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.74691530E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82970028E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.16352476E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66940513E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51782506E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53379083E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01881848E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.71573858E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.18235842E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.00034394E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85602290E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14272578E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08093482E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.51195548E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.64856154E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77961297E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.78909316E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57391463E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05117574E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07684821E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88595717E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.90428604E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.17953032E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14272578E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08093482E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.51195548E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.64856154E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77961297E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.78909316E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57391463E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05117574E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07684821E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88595717E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.90428604E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.17953032E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08642361E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55404586E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44889656E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23229625E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40321455E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51698172E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81385552E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08045900E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.61056659E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05537333E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98227240E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43266989E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.96078831E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87287495E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14378810E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.23083714E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19559147E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.90900155E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78350159E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17445402E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55106066E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14378810E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.23083714E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19559147E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.90900155E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78350159E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17445402E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55106066E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46904584E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11584431E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08389543E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.54381356E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52470535E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.59264158E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03496587E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.29963789E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33550236E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33550236E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11184907E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11184907E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10529714E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.93633918E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.51141396E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.79430220E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.32306765E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.03309333E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.76458690E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.83471648E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.04824416E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.94660000E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.25922069E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.56209919E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17951957E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53010183E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17951957E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53010183E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42074429E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50914116E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50914116E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47903229E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01569451E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01569451E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01569435E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.05637859E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.05637859E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.05637859E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.05637859E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.85460130E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.85460130E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.85460130E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.85460130E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.02765547E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.02765547E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.72810180E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.06759462E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.62772855E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.17160048E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.96564188E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.17160048E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.96564188E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.63912874E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.79493981E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.79493981E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.79373678E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.66748720E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.66748720E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.66747810E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.99318056E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.07200250E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.99318056E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.07200250E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.39507306E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.08073404E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.08073404E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.89121230E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.15937981E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.15937981E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.15937985E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.14963998E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.14963998E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.14963998E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.14963998E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.71652898E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.71652898E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.71652898E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.71652898E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.61734636E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.61734636E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.93496236E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.07005131E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.17566060E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.15161844E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.82945220E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.82945220E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.08972341E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.82794418E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.00780035E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.71105491E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.71105491E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.65653038E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.65653038E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.70704661E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.70704661E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09327576E-03   # h decays
#          BR         NDA      ID1       ID2
     5.83797975E-01    2           5        -5   # BR(h -> b       bb     )
     6.39287790E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26305114E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79530181E-04    2           3        -3   # BR(h -> s       sb     )
     2.08258496E-02    2           4        -4   # BR(h -> c       cb     )
     6.82815206E-02    2          21        21   # BR(h -> g       g      )
     2.38935584E-03    2          22        22   # BR(h -> gam     gam    )
     1.68013278E-03    2          22        23   # BR(h -> Z       gam    )
     2.29174476E-01    2          24       -24   # BR(h -> W+      W-     )
     2.92160763E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53491944E+01   # H decays
#          BR         NDA      ID1       ID2
     3.60291770E-01    2           5        -5   # BR(H -> b       bb     )
     5.99033274E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11803656E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50527196E-04    2           3        -3   # BR(H -> s       sb     )
     7.02705003E-08    2           4        -4   # BR(H -> c       cb     )
     7.04256719E-03    2           6        -6   # BR(H -> t       tb     )
     4.32483772E-07    2          21        21   # BR(H -> g       g      )
     3.48544317E-09    2          22        22   # BR(H -> gam     gam    )
     1.81081928E-09    2          23        22   # BR(H -> Z       gam    )
     1.81460447E-06    2          24       -24   # BR(H -> W+      W-     )
     9.06673744E-07    2          23        23   # BR(H -> Z       Z      )
     7.17276611E-06    2          25        25   # BR(H -> h       h      )
    -5.94025012E-24    2          36        36   # BR(H -> A       A      )
    -9.51933332E-21    2          23        36   # BR(H -> Z       A      )
     1.84641397E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57662855E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57662855E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.30421906E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.75108647E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.45115520E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.53629517E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.62460981E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.54103540E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.23517452E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.16588850E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.11111275E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.14381493E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.80720579E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     5.52214364E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.52214364E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.30265281E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53435875E+01   # A decays
#          BR         NDA      ID1       ID2
     3.60354003E-01    2           5        -5   # BR(A -> b       bb     )
     5.99096892E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11825984E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50594483E-04    2           3        -3   # BR(A -> s       sb     )
     7.07292922E-08    2           4        -4   # BR(A -> c       cb     )
     7.05651614E-03    2           6        -6   # BR(A -> t       tb     )
     1.44993547E-05    2          21        21   # BR(A -> g       g      )
     6.19930573E-08    2          22        22   # BR(A -> gam     gam    )
     1.59436591E-08    2          23        22   # BR(A -> Z       gam    )
     1.81092812E-06    2          23        25   # BR(A -> Z       h      )
     1.86778605E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57668629E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57668629E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.99800061E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.33174206E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.22921050E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.06642341E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.08087678E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.75473792E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.37263826E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.50697366E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.55026619E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.19965609E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.19965609E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.54771410E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.78885522E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.97811971E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11371667E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.70486420E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19745921E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46355867E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.68467666E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.80862827E-06    2          24        25   # BR(H+ -> W+      h      )
     3.06986903E-14    2          24        36   # BR(H+ -> W+      A      )
     5.66915855E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.55947448E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.81605378E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57506607E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.29275221E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56398608E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.06578202E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.92387187E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.18159919E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
