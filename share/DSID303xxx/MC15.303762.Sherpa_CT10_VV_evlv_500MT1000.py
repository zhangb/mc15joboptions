include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Lepton+MET Mt-Binned Diboson Sample evlv 500MT1000"
evgenConfig.keywords = ["diboson", "lepton", "neutrino", "2jet", "SM" ]
evgenConfig.contact  = [ "oleksandr.viazlo@cern.ch", "daniel.hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.inputconfcheck = "Sherpa_CT10_VV_mtbinned"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=Internal;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  % massive b-quarks such that top-quark processes are not included by the 93 container
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0

  SHERPA_LDADD=SherpaTwoParticleMass

  PARTICLE_CONTAINER 98 lepCon 11 -11;
  PARTICLE_CONTAINER 89 nuCon 12 -12;
}(run)

(processes){
  Process 93 93 -> 11 -11 91 91 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -12 -13 14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -12 -15 16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> -11 12 13 -14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> -11 12 15 -16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

}(processes)

(selector){
  "PT" 90 1.0,E_CMS:1.0,E_CMS [PT_UP]
  "PT" 91 1.0,E_CMS:1.0,E_CMS [PT_UP]
  TwoParticleMass 11 0 500 1000 1
  Mass 11 -11 0.2500 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.8040 E_CMS
  Mass 12 -12 0.2500 E_CMS
  Mass 14 -14 0.2500 E_CMS
  Mass 16 -16 0.2500 E_CMS
  Mass 11 -12 0.25 E_CMS
  Mass -11 12 0.25 E_CMS
  Mass 13 -14 0.3557 E_CMS
  Mass -13 14 0.3557 E_CMS
  Mass 15 -16 2.0268 E_CMS
  Mass -15 16 2.0268 E_CMS
}(selector)
"""
