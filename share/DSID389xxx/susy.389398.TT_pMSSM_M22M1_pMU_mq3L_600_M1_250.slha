#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13425138E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04020408E+01   # W+
        25     1.24365901E+02   # h
        35     3.00018792E+03   # H
        36     2.99999962E+03   # A
        37     3.00094406E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02904139E+03   # ~d_L
   2000001     3.02394356E+03   # ~d_R
   1000002     3.02812610E+03   # ~u_L
   2000002     3.02563003E+03   # ~u_R
   1000003     3.02904139E+03   # ~s_L
   2000003     3.02394356E+03   # ~s_R
   1000004     3.02812610E+03   # ~c_L
   2000004     3.02563003E+03   # ~c_R
   1000005     6.84636759E+02   # ~b_1
   2000005     3.02330908E+03   # ~b_2
   1000006     6.82308883E+02   # ~t_1
   2000006     3.00953082E+03   # ~t_2
   1000011     3.00645988E+03   # ~e_L
   2000011     3.00143991E+03   # ~e_R
   1000012     3.00506752E+03   # ~nu_eL
   1000013     3.00645988E+03   # ~mu_L
   2000013     3.00143991E+03   # ~mu_R
   1000014     3.00506752E+03   # ~nu_muL
   1000015     2.98572488E+03   # ~tau_1
   2000015     3.02209881E+03   # ~tau_2
   1000016     3.00506860E+03   # ~nu_tauL
   1000021     2.33581676E+03   # ~g
   1000022     2.51880161E+02   # ~chi_10
   1000023     5.27190384E+02   # ~chi_20
   1000025    -3.00025196E+03   # ~chi_30
   1000035     3.00098397E+03   # ~chi_40
   1000024     5.27352767E+02   # ~chi_1+
   1000037     3.00156815E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886525E-01   # N_11
  1  2    -8.47925983E-04   # N_12
  1  3     1.49098431E-02   # N_13
  1  4    -1.97864685E-03   # N_14
  2  1     1.25999194E-03   # N_21
  2  2     9.99621127E-01   # N_22
  2  3    -2.68833016E-02   # N_23
  2  4     5.77082626E-03   # N_24
  3  1    -9.12838928E-03   # N_31
  3  2     1.49391734E-02   # N_32
  3  3     7.06861365E-01   # N_33
  3  4     7.07135421E-01   # N_34
  4  1    -1.19173139E-02   # N_41
  4  2     2.31020354E-02   # N_42
  4  3     7.06683801E-01   # N_43
  4  4    -7.07051822E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99276482E-01   # U_11
  1  2    -3.80330381E-02   # U_12
  2  1     3.80330381E-02   # U_21
  2  2     9.99276482E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966662E-01   # V_11
  1  2    -8.16550482E-03   # V_12
  2  1     8.16550482E-03   # V_21
  2  2     9.99966662E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98869973E-01   # cos(theta_t)
  1  2    -4.75265930E-02   # sin(theta_t)
  2  1     4.75265930E-02   # -sin(theta_t)
  2  2     9.98869973E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99917736E-01   # cos(theta_b)
  1  2     1.28265830E-02   # sin(theta_b)
  2  1    -1.28265830E-02   # -sin(theta_b)
  2  2     9.99917736E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06936114E-01   # cos(theta_tau)
  1  2     7.07277407E-01   # sin(theta_tau)
  2  1    -7.07277407E-01   # -sin(theta_tau)
  2  2     7.06936114E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01893888E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.34251376E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44494692E+02   # higgs               
         4     6.96532041E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.34251376E+03  # The gauge couplings
     1     3.61803238E-01   # gprime(Q) DRbar
     2     6.37647204E-01   # g(Q) DRbar
     3     1.02883809E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.34251376E+03  # The trilinear couplings
  1  1     1.68813705E-06   # A_u(Q) DRbar
  2  2     1.68816204E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.34251376E+03  # The trilinear couplings
  1  1     4.42807239E-07   # A_d(Q) DRbar
  2  2     4.42889019E-07   # A_s(Q) DRbar
  3  3     9.86288699E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.34251376E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.28688391E-08   # A_mu(Q) DRbar
  3  3     9.39655789E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.34251376E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54397325E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.34251376E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13928217E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.34251376E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06512921E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.34251376E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.68911010E+04   # M^2_Hd              
        22    -9.09641057E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41127582E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.84756103E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48243283E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48243283E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51756717E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51756717E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.00289790E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.12195551E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.38780445E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03919115E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.25477757E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.30888258E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.64786530E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.05885066E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.24905124E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.61127103E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51129540E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.99284195E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.61895179E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.39208972E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.60791028E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.24205022E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.87141425E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.90808482E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.85757353E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.51445340E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -2.85377293E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27654377E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.95892299E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.26514837E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.13752985E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88633163E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.92146222E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59555309E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.16322370E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.74997057E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.19167115E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.08737191E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.15356067E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18656221E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57110329E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.38313639E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.35840583E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.01708418E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42889422E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.89152722E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.02599729E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59338415E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.96864797E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.33438401E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.18597096E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.87215188E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.16038241E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67958312E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46630334E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.77437698E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.08499714E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.73964574E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55336896E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88633163E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.92146222E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59555309E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.16322370E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.74997057E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.19167115E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.08737191E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.15356067E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18656221E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57110329E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.38313639E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.35840583E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.01708418E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42889422E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.89152722E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.02599729E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59338415E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.96864797E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.33438401E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.18597096E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.87215188E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.16038241E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67958312E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46630334E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.77437698E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.08499714E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.73964574E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55336896E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80910230E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01009442E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00094915E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.17500032E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.12894866E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98895632E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     9.76366977E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54098381E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998487E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.51270014E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.29480562E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.32966878E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.80910230E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01009442E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00094915E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.17500032E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.12894866E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98895632E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     9.76366977E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54098381E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998487E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.51270014E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.29480562E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.32966878E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70260865E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56094298E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14955060E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28950643E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64542324E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.64543202E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.12144601E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.78595998E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.10934540E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23280240E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.10768656E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80932466E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01560792E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99064005E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.32438427E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.09893527E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99375199E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.30509464E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80932466E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01560792E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99064005E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.32438427E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.09893527E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99375199E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.30509464E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80966011E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01551887E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99037793E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.32486077E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.09985923E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99409996E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.20285362E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.68143577E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.73837478E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.74911833E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.24757157E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.28473983E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.83938073E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.12755057E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.66930111E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.13887068E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.42164801E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.98553672E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50144633E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.75928520E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.38245795E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.80258076E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.64267093E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.64267093E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.35415043E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.03954465E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65513113E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65513113E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27596318E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27596318E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.53947103E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.53947103E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.67722740E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.18917652E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.03048895E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.70865009E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.70865009E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.44351616E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.12236021E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.62815304E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.62815304E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30210195E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30210195E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.60838244E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.60838244E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.79407249E-03   # h decays
#          BR         NDA      ID1       ID2
     6.84610882E-01    2           5        -5   # BR(h -> b       bb     )
     5.41892286E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.91833847E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.07536277E-04    2           3        -3   # BR(h -> s       sb     )
     1.75866706E-02    2           4        -4   # BR(h -> c       cb     )
     5.68717416E-02    2          21        21   # BR(h -> g       g      )
     1.91945826E-03    2          22        22   # BR(h -> gam     gam    )
     1.23105940E-03    2          22        23   # BR(h -> Z       gam    )
     1.62834907E-01    2          24       -24   # BR(h -> W+      W-     )
     2.01566831E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39301013E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43175567E-01    2           5        -5   # BR(H -> b       bb     )
     1.78520505E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31205844E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75118992E-04    2           3        -3   # BR(H -> s       sb     )
     2.18787339E-07    2           4        -4   # BR(H -> c       cb     )
     2.18254552E-02    2           6        -6   # BR(H -> t       tb     )
     3.20224073E-05    2          21        21   # BR(H -> g       g      )
     4.92132055E-08    2          22        22   # BR(H -> gam     gam    )
     8.38185108E-09    2          23        22   # BR(H -> Z       gam    )
     3.37992085E-05    2          24       -24   # BR(H -> W+      W-     )
     1.68787704E-05    2          23        23   # BR(H -> Z       Z      )
     8.65487266E-05    2          25        25   # BR(H -> h       h      )
     8.32179900E-24    2          36        36   # BR(H -> A       A      )
     1.83542254E-17    2          23        36   # BR(H -> Z       A      )
     2.11703984E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09986661E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.05569269E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.89543948E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.09241635E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.19661441E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32296633E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82585011E-01    2           5        -5   # BR(A -> b       bb     )
     1.87965003E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.64598404E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.16264858E-04    2           3        -3   # BR(A -> s       sb     )
     2.30338373E-07    2           4        -4   # BR(A -> c       cb     )
     2.29650848E-02    2           6        -6   # BR(A -> t       tb     )
     6.76299859E-05    2          21        21   # BR(A -> g       g      )
     3.79824244E-08    2          22        22   # BR(A -> gam     gam    )
     6.66322731E-08    2          23        22   # BR(A -> Z       gam    )
     3.54527686E-05    2          23        25   # BR(A -> Z       h      )
     2.65511463E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22377514E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32392715E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.99200848E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.99781735E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.09185003E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.48803960E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.79710118E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.98782919E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.16986423E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.06360732E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.11250662E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.69867455E-05    2          24        25   # BR(H+ -> W+      h      )
     9.88895814E-14    2          24        36   # BR(H+ -> W+      A      )
     2.01412169E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.14283323E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.47904121E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
