#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 W+H+jet->lvlvjj+ jet production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
#--------------------------------------------------------------
# Higgs->WW at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny =  1 2 3 4 5 6 11 12 13 14 15 16']
#--------------------------------------------------------------
# Filters
#--------------------------------------------------------------
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25                 
filtSeq.XtoVVDecayFilterExtended.PDGParent = 24                       
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22                   
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]     
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6] 
filtSeq.Expression = "XtoVVDecayFilterExtended"
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->lvWW->lvlvqq + jet production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'ada.farilla@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents   = 20
evgenConfig.process = "WpH, W->lv, H->WW->lvqq"
