evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, veto at least one lepton filter and require boosted filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mdshapiro@lbl.gov']
##evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"
evgenConfig.minevents   = 50
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# filter boosted : hadronic top pT > 200 GeV and ttbar pT > 150 GeV
include('MC15JobOptions/BoostedHadTopAndTopPair.py')
filtSeq.BoostedHadTopAndTopPair.tHadPtMin  =   200000.
filtSeq.BoostedHadTopAndTopPair.tHadPtMax  = 10000000.
filtSeq.BoostedHadTopAndTopPair.tPairPtMin =   150000.
filtSeq.BoostedHadTopAndTopPair.tPairPtMax = 10000000.

include('MC15JobOptions/TTbarPlusHFFilter.py')
filtSeq.TTbarPlusBFilter.SelectC        = True

filtSeq.Expression = "((not TTbarWToLeptonFilter) and (BoostedHadTopAndTopPair) and (TTbarPlusBFilter))"
