#--------------------------------------------------------------
# Powheg+Pythia+Evtgen, ggH+mumu, H+jet production with NNLOPS and A14 tune, Hee mh=125 GeV"
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

evgenConfig.process     = "ggH H->ee"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+jet production with NNLOPS and A14 tune, Hee mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2electron", "mH125" ]

evgenConfig.contact     = [ 'r.turner@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents = 2000

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
#include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 11 11' ]
