#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980703E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04019608E+01   # W+
        25     1.25258157E+02   # h
        35     3.00009441E+03   # H
        36     2.99999992E+03   # A
        37     3.00109143E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03513868E+03   # ~d_L
   2000001     3.02997749E+03   # ~d_R
   1000002     3.03423002E+03   # ~u_L
   2000002     3.03192951E+03   # ~u_R
   1000003     3.03513868E+03   # ~s_L
   2000003     3.02997749E+03   # ~s_R
   1000004     3.03423002E+03   # ~c_L
   2000004     3.03192951E+03   # ~c_R
   1000005     8.47190510E+02   # ~b_1
   2000005     3.02874576E+03   # ~b_2
   1000006     8.44670149E+02   # ~t_1
   2000006     3.02296607E+03   # ~t_2
   1000011     3.00662583E+03   # ~e_L
   2000011     3.00117732E+03   # ~e_R
   1000012     3.00523875E+03   # ~nu_eL
   1000013     3.00662583E+03   # ~mu_L
   2000013     3.00117732E+03   # ~mu_R
   1000014     3.00523875E+03   # ~nu_muL
   1000015     2.98616488E+03   # ~tau_1
   2000015     3.02176911E+03   # ~tau_2
   1000016     3.00530602E+03   # ~nu_tauL
   1000021     2.34353097E+03   # ~g
   1000022     3.02408290E+02   # ~chi_10
   1000023     6.32711712E+02   # ~chi_20
   1000025    -2.99918467E+03   # ~chi_30
   1000035     2.99944756E+03   # ~chi_40
   1000024     6.32874675E+02   # ~chi_1+
   1000037     3.00026323E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890406E-01   # N_11
  1  2    -8.33083472E-07   # N_12
  1  3    -1.47859936E-02   # N_13
  1  4    -7.42517463E-04   # N_14
  2  1     3.98400344E-04   # N_21
  2  2     9.99635721E-01   # N_22
  2  3     2.66822892E-02   # N_23
  2  4     4.03996282E-03   # N_24
  3  1    -9.92748175E-03   # N_31
  3  2     1.60133987E-02   # N_32
  3  3    -7.06848124E-01   # N_33
  3  4     7.07114379E-01   # N_34
  4  1     1.09755785E-02   # N_41
  4  2    -2.17254607E-02   # N_42
  4  3     7.06707265E-01   # N_43
  4  4     7.07087252E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99287985E-01   # U_11
  1  2     3.77296097E-02   # U_12
  2  1    -3.77296097E-02   # U_21
  2  2     9.99287985E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99983690E-01   # V_11
  1  2    -5.71135131E-03   # V_12
  2  1    -5.71135131E-03   # V_21
  2  2    -9.99983690E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98559139E-01   # cos(theta_t)
  1  2    -5.36623324E-02   # sin(theta_t)
  2  1     5.36623324E-02   # -sin(theta_t)
  2  2     9.98559139E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99723605E-01   # cos(theta_b)
  1  2    -2.35098619E-02   # sin(theta_b)
  2  1     2.35098619E-02   # -sin(theta_b)
  2  2     9.99723605E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06948120E-01   # cos(theta_tau)
  1  2     7.07265407E-01   # sin(theta_tau)
  2  1    -7.07265407E-01   # -sin(theta_tau)
  2  2    -7.06948120E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00125731E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49807026E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44073571E+02   # higgs               
         4     1.06755155E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49807026E+03  # The gauge couplings
     1     3.62146653E-01   # gprime(Q) DRbar
     2     6.37242688E-01   # g(Q) DRbar
     3     1.02636410E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49807026E+03  # The trilinear couplings
  1  1     2.13990871E-06   # A_u(Q) DRbar
  2  2     2.13994025E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49807026E+03  # The trilinear couplings
  1  1     7.62432232E-07   # A_d(Q) DRbar
  2  2     7.62520645E-07   # A_s(Q) DRbar
  3  3     1.54809334E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49807026E+03  # The trilinear couplings
  1  1     3.29982329E-07   # A_e(Q) DRbar
  2  2     3.29998474E-07   # A_mu(Q) DRbar
  3  3     3.34530078E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49807026E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52997781E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49807026E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.83803111E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49807026E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01614089E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49807026E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.38799734E+04   # M^2_Hd              
        22    -9.06741897E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40948930E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.17456285E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47984106E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47984106E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52015894E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52015894E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.81649569E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.64068453E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.00672008E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.52921147E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16190743E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.59628666E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.16062428E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.35687672E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.04876158E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.94900606E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69694554E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61534199E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.16865537E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.55755185E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.29546116E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.48952626E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.88092763E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.41704617E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.02284792E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.63904029E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.31402027E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.20018343E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -6.65981641E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.44801042E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.24278770E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.02872198E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.49605272E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.78851154E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.05137150E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57694826E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.09937986E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.99799642E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.15547160E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.45297197E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.20706575E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17305473E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57378990E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.33704874E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.48541161E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.02104327E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42620968E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.79370478E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.04878361E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57583575E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.53341711E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.16466647E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.14981062E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.96123219E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.21386186E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66468321E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47719563E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.64795583E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.14485701E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.57830303E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55228032E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.78851154E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.05137150E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57694826E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.09937986E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.99799642E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.15547160E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.45297197E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.20706575E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17305473E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57378990E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.33704874E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.48541161E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.02104327E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42620968E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.79370478E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.04878361E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57583575E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.53341711E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.16466647E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.14981062E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.96123219E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.21386186E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66468321E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47719563E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.64795583E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.14485701E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.57830303E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55228032E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.70856583E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03612995E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99034055E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.60955203E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.78680800E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97352931E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.66659015E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53412787E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999852E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47948124E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.77291417E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.63309731E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.70856583E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03612995E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99034055E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.60955203E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.78680800E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97352931E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.66659015E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53412787E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999852E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47948124E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.77291417E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.63309731E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64618850E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.62216527E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12781722E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25001751E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59436322E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70443062E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10032636E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.20008904E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.01992240E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19490254E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.18488909E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.70864574E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03561615E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98593441E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.48668853E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.79436048E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97844938E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.33744234E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.70864574E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03561615E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98593441E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.48668853E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.79436048E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97844938E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.33744234E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.70907350E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03552084E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98566928E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.54185038E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.88240089E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97880301E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.79863895E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.13318918E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.10848064E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.36195521E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61551357E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.53636856E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.04082930E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.96620865E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.88044005E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.33778886E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.08402213E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.77221205E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.22778795E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.12877347E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.10898392E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.90202334E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.86444385E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.86444385E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.39209386E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.15807078E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33877215E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33877215E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.69363860E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.69363860E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.23249827E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.23249827E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.03473813E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.17297189E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.14614943E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.92758944E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.92758944E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16504206E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.20892315E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.30900942E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.30900942E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.76356940E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.76356940E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.10471870E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.10471870E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.64196387E-03   # h decays
#          BR         NDA      ID1       ID2
     5.58324806E-01    2           5        -5   # BR(h -> b       bb     )
     7.13397095E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.52543429E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.35811633E-04    2           3        -3   # BR(h -> s       sb     )
     2.32840609E-02    2           4        -4   # BR(h -> c       cb     )
     7.59044206E-02    2          21        21   # BR(h -> g       g      )
     2.60614595E-03    2          22        22   # BR(h -> gam     gam    )
     1.75582832E-03    2          22        23   # BR(h -> Z       gam    )
     2.36317368E-01    2          24       -24   # BR(h -> W+      W-     )
     2.96793060E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.70315680E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00380242E-01    2           5        -5   # BR(H -> b       bb     )
     6.71528492E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.37436427E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.91572531E-04    2           3        -3   # BR(H -> s       sb     )
     8.17202636E-08    2           4        -4   # BR(H -> c       cb     )
     8.15212036E-03    2           6        -6   # BR(H -> t       tb     )
     8.64075579E-06    2          21        21   # BR(H -> g       g      )
     5.85412221E-08    2          22        22   # BR(H -> gam     gam    )
     3.34238042E-09    2          23        22   # BR(H -> Z       gam    )
     6.99336222E-07    2          24       -24   # BR(H -> W+      W-     )
     3.49237011E-07    2          23        23   # BR(H -> Z       Z      )
     5.49034763E-06    2          25        25   # BR(H -> h       h      )
    -1.16127697E-24    2          36        36   # BR(H -> A       A      )
     1.72904275E-19    2          23        36   # BR(H -> Z       A      )
     7.38527407E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.05223581E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.69010972E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.49223430E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23679307E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.24179748E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.62140544E+01   # A decays
#          BR         NDA      ID1       ID2
     9.20724069E-01    2           5        -5   # BR(A -> b       bb     )
     6.86670958E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.42790102E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.98196662E-04    2           3        -3   # BR(A -> s       sb     )
     8.41468712E-08    2           4        -4   # BR(A -> c       cb     )
     8.38957056E-03    2           6        -6   # BR(A -> t       tb     )
     2.47064827E-05    2          21        21   # BR(A -> g       g      )
     6.58780388E-08    2          22        22   # BR(A -> gam     gam    )
     2.43416652E-08    2          23        22   # BR(A -> Z       gam    )
     7.12406020E-07    2          23        25   # BR(A -> Z       h      )
     8.91312810E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.27595318E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.45289316E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.73322604E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.97199325E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46873458E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.26289762E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21440784E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.39988978E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.30135084E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.67729715E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24132080E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.50641938E-07    2          24        25   # BR(H+ -> W+      h      )
     5.13216438E-14    2          24        36   # BR(H+ -> W+      A      )
     4.81606714E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.04834692E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07763680E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
