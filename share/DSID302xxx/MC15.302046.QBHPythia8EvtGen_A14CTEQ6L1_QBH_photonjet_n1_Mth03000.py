evgenConfig.description = "Quantum black holes RS1 (n = 1, M_th = 3.0 TeV) decaying to photon+jet."
evgenConfig.process = "QBH -> photon + jet"
evgenConfig.keywords = ["exotic", "blackhole", "extraDimensions", "RandallSundrum", "warpedED"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_photonjet"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")
