#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13016094E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.46900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04181961E+01   # W+
        25     1.25986149E+02   # h
        35     4.00000814E+03   # H
        36     3.99999687E+03   # A
        37     4.00104843E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02593956E+03   # ~d_L
   2000001     4.02258226E+03   # ~d_R
   1000002     4.02528894E+03   # ~u_L
   2000002     4.02336545E+03   # ~u_R
   1000003     4.02593956E+03   # ~s_L
   2000003     4.02258226E+03   # ~s_R
   1000004     4.02528894E+03   # ~c_L
   2000004     4.02336545E+03   # ~c_R
   1000005     9.69051040E+02   # ~b_1
   2000005     4.02543466E+03   # ~b_2
   1000006     9.46754762E+02   # ~t_1
   2000006     1.90098170E+03   # ~t_2
   1000011     4.00456902E+03   # ~e_L
   2000011     4.00341721E+03   # ~e_R
   1000012     4.00345719E+03   # ~nu_eL
   1000013     4.00456902E+03   # ~mu_L
   2000013     4.00341721E+03   # ~mu_R
   1000014     4.00345719E+03   # ~nu_muL
   1000015     4.00459986E+03   # ~tau_1
   2000015     4.00767527E+03   # ~tau_2
   1000016     4.00488803E+03   # ~nu_tauL
   1000021     1.97813046E+03   # ~g
   1000022     2.99480157E+02   # ~chi_10
   1000023    -3.57982521E+02   # ~chi_20
   1000025     3.69869380E+02   # ~chi_30
   1000035     2.05213772E+03   # ~chi_40
   1000024     3.55494847E+02   # ~chi_1+
   1000037     2.05230192E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.63840803E-01   # N_11
  1  2    -1.37623932E-02   # N_12
  1  3    -3.86644144E-01   # N_13
  1  4    -3.22639069E-01   # N_14
  2  1    -4.96367374E-02   # N_21
  2  2     2.44990940E-02   # N_22
  2  3    -7.03235105E-01   # N_23
  2  4     7.08799249E-01   # N_24
  3  1     5.01312497E-01   # N_31
  3  2     2.81740141E-02   # N_32
  3  3     5.96609278E-01   # N_33
  3  4     6.26058603E-01   # N_34
  4  1    -1.02022148E-03   # N_41
  4  2     9.99207994E-01   # N_42
  4  3    -4.90528900E-03   # N_43
  4  4    -3.94750735E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.94274746E-03   # U_11
  1  2     9.99975899E-01   # U_12
  2  1    -9.99975899E-01   # U_21
  2  2     6.94274746E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.58373814E-02   # V_11
  1  2    -9.98439876E-01   # V_12
  2  1    -9.98439876E-01   # V_21
  2  2    -5.58373814E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89911797E-01   # cos(theta_t)
  1  2    -1.41684982E-01   # sin(theta_t)
  2  1     1.41684982E-01   # -sin(theta_t)
  2  2     9.89911797E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999373E-01   # cos(theta_b)
  1  2    -1.11982124E-03   # sin(theta_b)
  2  1     1.11982124E-03   # -sin(theta_b)
  2  2     9.99999373E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05852880E-01   # cos(theta_tau)
  1  2     7.08358463E-01   # sin(theta_tau)
  2  1    -7.08358463E-01   # -sin(theta_tau)
  2  2    -7.05852880E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00281212E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30160941E+03  # DRbar Higgs Parameters
         1    -3.46900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43603751E+02   # higgs               
         4     1.61045444E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30160941E+03  # The gauge couplings
     1     3.61939353E-01   # gprime(Q) DRbar
     2     6.36127109E-01   # g(Q) DRbar
     3     1.03008575E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30160941E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37057304E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30160941E+03  # The trilinear couplings
  1  1     5.03733222E-07   # A_d(Q) DRbar
  2  2     5.03780167E-07   # A_s(Q) DRbar
  3  3     9.02041955E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30160941E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10588489E-07   # A_mu(Q) DRbar
  3  3     1.11721928E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30160941E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68215086E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30160941E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84695245E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30160941E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03433056E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30160941E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57704806E+07   # M^2_Hd              
        22    -8.06760636E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40384853E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.95258931E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40046681E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40046681E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59953319E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59953319E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.06962705E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.62817306E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.25082226E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.69630555E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09005488E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17504038E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.15355732E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.10677962E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.84538725E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.16787027E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.64420248E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.25172907E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.42952411E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.12060563E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.57848439E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.00747534E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.61147949E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.88025608E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66260920E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54525443E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81432982E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63747155E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.74162349E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65152277E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.73003181E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12919026E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.28776801E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.72491206E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.88244614E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.07128256E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78185944E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.95229371E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96361951E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.30947631E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81652631E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.38186955E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62090043E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51806139E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60487548E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.13135328E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.35782245E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.38357651E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.20607539E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44714888E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78205769E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.73426707E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.92477937E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.85867250E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82159404E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.32091525E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65319942E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52024362E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53765519E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.07777177E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.54222938E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.60941403E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.36166563E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85577438E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78185944E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.95229371E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96361951E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.30947631E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81652631E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.38186955E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62090043E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51806139E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60487548E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.13135328E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.35782245E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.38357651E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.20607539E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44714888E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78205769E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.73426707E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.92477937E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.85867250E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82159404E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.32091525E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65319942E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52024362E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53765519E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.07777177E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.54222938E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.60941403E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.36166563E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85577438E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14637989E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.15623024E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.06279593E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.94663364E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77805679E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.86130943E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57049285E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06033654E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.47338970E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.45560165E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.50204856E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.72976504E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14637989E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.15623024E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.06279593E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.94663364E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77805679E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.86130943E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57049285E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06033654E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.47338970E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.45560165E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.50204856E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.72976504E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09463341E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.69553497E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40732704E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.10425298E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40100778E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.49195024E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80927654E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09027957E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.77829329E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.14071561E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.81450879E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42823634E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.01049510E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86384050E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14744084E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.29249673E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.40149211E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.32573011E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78170743E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14246008E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54778331E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14744084E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.29249673E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.40149211E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.32573011E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78170743E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14246008E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54778331E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47446035E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17127359E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.27005072E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.01382305E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52207491E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62583439E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02998525E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.88673374E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33509234E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33509234E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11171166E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11171166E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10639199E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.12568163E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.55474596E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.27875900E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.38248392E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.77826635E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.39459322E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.59013408E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.16614802E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.69328076E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.76509222E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.09885212E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17756449E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52751252E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17756449E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52751252E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43554855E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50288502E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50288502E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47637854E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00324144E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00324144E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00324125E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.10428470E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.10428470E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.10428470E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.10428470E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.01428917E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.01428917E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.01428917E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.01428917E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.99876296E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.99876296E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.87473329E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.59409152E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.57686605E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.47482115E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22488674E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.47482115E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.22488674E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.17940315E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.77568144E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.77568144E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.76998509E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.63117909E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.63117909E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.63116977E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.24741449E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.91540327E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.24741449E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.91540327E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.45762141E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.68620393E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.68620393E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.85985920E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.33655141E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.33655141E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.33655142E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.96780257E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.96780257E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.96780257E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.96780257E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.55927996E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.55927996E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.55927996E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.55927996E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.08703899E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.08703899E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.12431468E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.96252740E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.85430294E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.32826083E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.58187000E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.58187000E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.24133862E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.89508577E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.95369262E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.73451762E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.73451762E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.73470894E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.73470894E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.08608573E-03   # h decays
#          BR         NDA      ID1       ID2
     5.85753429E-01    2           5        -5   # BR(h -> b       bb     )
     6.39958908E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26543079E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80102129E-04    2           3        -3   # BR(h -> s       sb     )
     2.08502932E-02    2           4        -4   # BR(h -> c       cb     )
     6.83479425E-02    2          21        21   # BR(h -> g       g      )
     2.38525260E-03    2          22        22   # BR(h -> gam     gam    )
     1.66964670E-03    2          22        23   # BR(h -> Z       gam    )
     2.27349974E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89409266E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51218862E+01   # H decays
#          BR         NDA      ID1       ID2
     3.56469927E-01    2           5        -5   # BR(H -> b       bb     )
     6.01503491E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12677065E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51560289E-04    2           3        -3   # BR(H -> s       sb     )
     7.05616536E-08    2           4        -4   # BR(H -> c       cb     )
     7.07174681E-03    2           6        -6   # BR(H -> t       tb     )
     5.79273098E-07    2          21        21   # BR(H -> g       g      )
     1.92366605E-09    2          22        22   # BR(H -> gam     gam    )
     1.81759156E-09    2          23        22   # BR(H -> Z       gam    )
     1.84789486E-06    2          24       -24   # BR(H -> W+      W-     )
     9.23307367E-07    2          23        23   # BR(H -> Z       Z      )
     7.24211085E-06    2          25        25   # BR(H -> h       h      )
     2.93030811E-24    2          36        36   # BR(H -> A       A      )
    -1.52505233E-21    2          23        36   # BR(H -> Z       A      )
     1.84843766E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59805143E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59805143E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.17909126E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.79540591E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.32256629E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.67206230E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.20144225E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.33235804E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.11572774E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.13218059E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.56697556E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.45350359E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     8.18397882E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     3.87172382E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.87172382E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.31371623E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51167207E+01   # A decays
#          BR         NDA      ID1       ID2
     3.56529033E-01    2           5        -5   # BR(A -> b       bb     )
     6.01562841E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12697883E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51625958E-04    2           3        -3   # BR(A -> s       sb     )
     7.10204219E-08    2           4        -4   # BR(A -> c       cb     )
     7.08556156E-03    2           6        -6   # BR(A -> t       tb     )
     1.45590358E-05    2          21        21   # BR(A -> g       g      )
     5.94245047E-08    2          22        22   # BR(A -> gam     gam    )
     1.60070773E-08    2          23        22   # BR(A -> Z       gam    )
     1.84414504E-06    2          23        25   # BR(A -> Z       h      )
     1.86208220E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59815413E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59815413E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.88623356E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.59180199E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.10989620E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.25718939E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.80752790E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.45851354E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.23955996E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.72009774E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.84759885E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.19886928E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.19886928E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52107645E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.72082925E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00696038E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12391403E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.66132760E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20323625E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47544390E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.64252521E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.84310091E-06    2          24        25   # BR(H+ -> W+      h      )
     3.06465300E-14    2          24        36   # BR(H+ -> W+      A      )
     6.10254453E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.41030096E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.50185987E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59757860E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.79071264E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58320730E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14167353E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.34543569E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.08725256E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
