##############################################################
# Sample for B to lepton-lepton X study
# Pythia8B_i generation with ISR/FSR for decay:
#  B0bar -> K-pi+ J/psi(ee) 
##############################################################
f = open("Bdbar_Kpi_JPSI_EE_USER.DEC","w") 

f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_J/psi J/psi\n")
f.write("Decay  my_J/psi\n")
f.write("1.0000   e+ e-   PHOTOS VLL; #[Reconstructed PDG2011]\n")
f.write("Enddecay\n")
f.write("Decay anti-B0\n")
f.write("1.0000   my_J/psi K- pi+   PHSP;\n")
f.write("Enddecay\n") 
f.write("End\n") 
f.close() 
##############################################################
evgenConfig.description = "Exclusive B0bar -> K-pi+ J/psi(ee)  production"
evgenConfig.keywords    = ["exclusive", "bottom", "2electron"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7']
genSeq.Pythia8B.NDecayLoops = 1
genSeq.Pythia8B.NHadronizationLoops = 2
        
genSeq.Pythia8B.QuarkPtCut      = 7.5
genSeq.Pythia8B.QuarkEtaCut     = 3.0
genSeq.Pythia8B.AntiQuarkPtCut  = 0.0
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [-511]
genSeq.EvtInclusiveDecay.userDecayFile = "Bdbar_Kpi_JPSI_EE_USER.DEC"
filtSeq.BSignalFilter.LVL1MuonCutOn  = False
filtSeq.BSignalFilter.LVL2MuonCutOn  = False

if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 2
filtSeq.MultiElectronFilter.Ptcut      = 4000.0
filtSeq.MultiElectronFilter.Etacut     = 2.7
filtSeq.BSignalFilter.B_PDGCode = -511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7
