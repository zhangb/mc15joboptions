#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, at least one lepton filter and 1000<HT<1500 GeV'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton' ]
evgenConfig.contact     = [ 'teng.jian.khoo@cern.ch' ]
evgenConfig.minevents = 2000

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 400.
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

  # Configure the HT filter
  include('MC15JobOptions/HTFilter.py')
  filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
  filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
  filtSeq.HTFilter.MinHT = 1000.*GeV # Min HT to keep event
  filtSeq.HTFilter.MaxHT = 1500.*GeV # Max HT to keep event
  filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
  filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
  filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')

