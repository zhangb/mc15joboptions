from MadGraphControl.MadGraphUtils import *

# adapated from: MC15.364665.aMcAtNloPy8EG_Zee0123jets_FxFx_NLO.py

multiplier=9
# General settings
minnevents=5000
if hasattr(runArgs,"maxEvents"):
    minevents=runArgs.maxEvents

nevents=minevents*multiplier
print "Asking MadGraph5_aMC@NLO to generate",nevents,"events."

keyword=['SM','WZ'] 
gridpack_mode=True
gridpack_dir='madevent/'

### DSID lists (extensions can include systematics samples)
# WZ_EW=[999999]


name='WZ_FxFx_lvll'
#process="pp>WZ>lvll~"

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define wpm = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > wpm z [QCD] @0
add process p p > wpm z  j [QCD] @1
add process p p > wpm z j j [QCD] @2
output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#process_dir = new_process(grid_pack=gridpack_dir)
process_dir = gridpack_dir

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version':'3.0',
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : 260000,  # NNPDF30_nlo_as_0118
          'maxjetflavor'  : 5,
          'parton_shower' : 'PYTHIA8',
          'ickkw'         : 3,
          'reweight_scale': '.true.',
          'rw_Rscale_down':  0.5,
          'rw_Rscale_up'  :  2.0,
          'rw_Fscale_down':  0.5,  
          'rw_Fscale_up'  :  2.0, 
          'reweight_PDF'  : '.true.',
          'PDF_set_min'   : 260001, 
          'PDF_set_max'   : 260100, 
          'jetradius'     : 1.0,
          'ptl'        : '0.0',
          'etal'       : '-1.0',
          'drll'       : '0.0',
          'ptj'           : 10, 
          'etaj'          : -1,
          'mll_sf'        : -1,
          'mll'           : -1}

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

param_card_loc='param_card.Torrielli.dat'
paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.wait()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card_loc)

madspin_card_loc='madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          * 
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer * 
#*                                                          * 
#*    Part of the MadGraph5_aMC@NLO Framework:              * 
#*    The MadGraph5_aMC@NLO Development Team - Find us at   * 
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     * 
#*                                                          * 
#************************************************************ 
#Some options (uncomment to apply)   
# 
# set seed 1 
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight 
# set BW_cut 15  
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event  
# set seed %i  
# specify the decay for the final state particles   
decay t > w+ b, w+ > all all 
decay t~ > w- b~, w- > all all 
decay w+ > l+ vl
decay w- > l- vl~
decay z > l+ l-
# running the actual code
launch"""%runArgs.randomSeed) 
mscard.close()

print_cards()

runName='WZ_FxFx_lvlljj'
    
generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,madspin_card_loc=madspin_card_loc,mode=0,njobs=1,proc_dir=process_dir,run_name=runName,
         grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed,gridpack_compile=False)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+stringy+'.events.tar.gz')

evgenConfig.generators = ["MadGraph"]

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

############################

evgenConfig.description = 'aMcAtNloPythia8EvtGen_'+str(name)
evgenConfig.keywords+=keyword 
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = minevents
runArgs.inputGeneratorFile=stringy+'.events.tar.gz'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 3 
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

PYTHIA8_nJetMax=2
PYTHIA8_qCut=25.
#include("MC15JobOptions/Pythia8_FxFx.py")

print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = %f"%PYTHIA8_qCut,
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx        = on",
                            "JetMatching:qCutME        = 8.0",
                            "JetMatching:nJetMax       = %i"%PYTHIA8_nJetMax ]

genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS = True

#evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
#evgenConfig.inputfilecheck = runName
#runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
	
#evgenConfig.contact = ['Louis Helary <Louis.Helary@cern.ch>','Benjamin Freund <benjamin.freund@cern.ch>']
#evgenConfig.keywords+=keyword
#evgenConfig.description = description	
#evgenConfig.description = 'WZ_FxFx'
#evgenConfig.keywords+=['SM']
#runArgs.inputGeneratorFile=name+'._00001.events.tar.gz'

