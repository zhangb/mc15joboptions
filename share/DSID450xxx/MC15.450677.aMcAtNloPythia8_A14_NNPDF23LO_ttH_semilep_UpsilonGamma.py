include("MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# Higgs at Pythia 8
#--------------------------------------------------------------



genSeq.Pythia8.Commands += [  '25:onMode = off', #decay of higgs
                              '25:addChannel = 1 0.002 100 553 22',
                              '25:addChannel = 1 0.002 100 100553 22',
                              '25:addChannel = 1 0.002 100 200553 22',
                              '553:onMode = off',
                              '553:onIfMatch = 13 -13',
                              '100553:onMode = off',
                              '100553:onIfMatch = 13 -13',
                              '200553:onMode = off',
                              '200553:onIfMatch = 13 -13',
                           ]


evgenConfig.process     = 'ttH H->UpsilonGamma'
evgenConfig.description = 'aMcAtNloPythia8 ttH semilep, H to Upsilon Gamma'
evgenConfig.keywords    = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs', 'photon' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.contact = ["Will Heidorn <william.dale.heidorn@cern.ch>"]
