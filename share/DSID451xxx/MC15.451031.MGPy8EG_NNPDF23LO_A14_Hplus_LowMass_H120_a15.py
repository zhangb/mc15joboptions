#--------------------------------------------------------------
# on-the-fly generation of H+ MG5 events, montoya@cern.ch
#--------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import fnmatch

#
safefactor = 3.0
nevents=10000
mode=0

### DSID lists (extensions can include systematics samples) -> To be filled
my_card="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme False
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~       
    import model 2HDMtypeII             
    generate p p > t t~, (t~ > b~ w-), (t > b h+)
    add process p p > t t~, (t > b w+), (t~ > b~ h-)
    output -f"""

#generate p p > t~ h+ b [QCD]               
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

import math
mhc=120.0
mh1=15.0                
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2)) 
mh3=mh2

extras = { 'lhe_version':'3.0',                        # These two are not present in NLO cards, 
           'cut_decays':'F',                           # lhe version fixed by arrange_output function   
           'pdlabel':"'nn23lo'",
           'parton_shower':'PYTHIA8',
           'fixed_ren_scale':'F',#use dynamic
           'fixed_fac_scale':'F',
           # 'muR_ref_fixed':str(scale), 
           # 'muF1_ref_fixed':str(scale), 
           # 'muF2_ref_fixed':str(scale),
            'PDF_set_min':'246800',
            'PDF_set_max':'246900'
           }
process_dir = new_process(my_card)

build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',nevts=nevents*safefactor,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
#               nevts=runArgs.maxEvents/0.53*safefactor,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)



#Build param_card.dat from UFO, and set Higgs masses
#write_param_card is part of the 2HDMTypeII model
masses = {'25':str(mh1)+'  #  mh1',
          '35':str(mh2)+'  #  mh2',
          '36':str(mh2)+'  #  mh2',
          '37':str(mhc)+'  #  mhc'}

#decayss={'25': 'DECAY  25 1.800663e-02', 
#         '37': 'DECAY  37 1.727255e+00'}

#build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card.dat',masses=masses,decays=decayss) 

build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card.dat',masses=masses)

#build_param_card(param_card_old='param_card.TMP.dat',param_card_new='param_card.dat',masses=masses) 


print_cards()

runName='run_01'     

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,lhe_version=3,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')                   

#### Shower     
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/MultiLeptonFilter.py")

evgenConfig.minevents = nevents
evgenConfig.description = 'MadGraph charged Higgs to a1'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['Waleed Ahmed <waleed.syed.ahmed@cern.ch>']

#evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

genSeq.Pythia8.Commands += ["Higgs:useBSM=on",
                            "HiggsH1:parity=2",
                            "HiggsHchg:tanBeta=1.",
                            "37:onMode=off",
                            "37:onIfAny=25",
                            "25:onMode=off",
                            "25:onIfAny=13"]

filtSeq.MultiLeptonFilter.Ptcut = 3000.
filtSeq.MultiLeptonFilter.Etacut = 3.0
filtSeq.MultiLeptonFilter.NLeptons = 3          
