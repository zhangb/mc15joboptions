evgenConfig.description = "Protos TT pair production (T singlet, M = 1600 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["Steffen Henkelmann <steffen.henkelmann@cern.ch>"]
evgenConfig.inputfilecheck = "protoslhef"
evgenConfig.process = "pp>TT"

include("MC15JobOptions/ProtosLHEF_Common.py")
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
