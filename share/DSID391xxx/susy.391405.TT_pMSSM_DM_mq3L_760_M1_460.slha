#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13068493E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.77990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04170482E+01   # W+
        25     1.25481570E+02   # h
        35     4.00001334E+03   # H
        36     3.99999760E+03   # A
        37     4.00101123E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02603420E+03   # ~d_L
   2000001     4.02259251E+03   # ~d_R
   1000002     4.02537968E+03   # ~u_L
   2000002     4.02365535E+03   # ~u_R
   1000003     4.02603420E+03   # ~s_L
   2000003     4.02259251E+03   # ~s_R
   1000004     4.02537968E+03   # ~c_L
   2000004     4.02365535E+03   # ~c_R
   1000005     8.37988229E+02   # ~b_1
   2000005     4.02547118E+03   # ~b_2
   1000006     8.26874758E+02   # ~t_1
   2000006     2.27867049E+03   # ~t_2
   1000011     4.00473462E+03   # ~e_L
   2000011     4.00305939E+03   # ~e_R
   1000012     4.00361914E+03   # ~nu_eL
   1000013     4.00473462E+03   # ~mu_L
   2000013     4.00305939E+03   # ~mu_R
   1000014     4.00361914E+03   # ~nu_muL
   1000015     4.00390596E+03   # ~tau_1
   2000015     4.00812358E+03   # ~tau_2
   1000016     4.00503086E+03   # ~nu_tauL
   1000021     1.98157019E+03   # ~g
   1000022     4.45723697E+02   # ~chi_10
   1000023    -4.89709259E+02   # ~chi_20
   1000025     5.07089966E+02   # ~chi_30
   1000035     2.05234740E+03   # ~chi_40
   1000024     4.87529272E+02   # ~chi_1+
   1000037     2.05251183E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.89419749E-01   # N_11
  1  2    -1.93211181E-02   # N_12
  1  3    -4.54128088E-01   # N_13
  1  4    -4.12566157E-01   # N_14
  2  1    -3.48165603E-02   # N_21
  2  2     2.31961205E-02   # N_22
  2  3    -7.04835993E-01   # N_23
  2  4     7.08135559E-01   # N_24
  3  1     6.12864550E-01   # N_31
  3  2     2.80705237E-02   # N_32
  3  3     5.44898337E-01   # N_33
  3  4     5.71572298E-01   # N_34
  4  1    -1.14432022E-03   # N_41
  4  2     9.99149979E-01   # N_42
  4  3    -7.72695150E-03   # N_43
  4  4    -4.04759763E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.09336517E-02   # U_11
  1  2     9.99940226E-01   # U_12
  2  1    -9.99940226E-01   # U_21
  2  2     1.09336517E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.72507123E-02   # V_11
  1  2    -9.98359833E-01   # V_12
  2  1    -9.98359833E-01   # V_21
  2  2    -5.72507123E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96249827E-01   # cos(theta_t)
  1  2    -8.65233044E-02   # sin(theta_t)
  2  1     8.65233044E-02   # -sin(theta_t)
  2  2     9.96249827E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998834E-01   # cos(theta_b)
  1  2    -1.52708829E-03   # sin(theta_b)
  2  1     1.52708829E-03   # -sin(theta_b)
  2  2     9.99998834E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06206586E-01   # cos(theta_tau)
  1  2     7.08005832E-01   # sin(theta_tau)
  2  1    -7.08005832E-01   # -sin(theta_tau)
  2  2    -7.06206586E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00240808E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30684926E+03  # DRbar Higgs Parameters
         1    -4.77990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43745291E+02   # higgs               
         4     1.62140840E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30684926E+03  # The gauge couplings
     1     3.61867799E-01   # gprime(Q) DRbar
     2     6.35879564E-01   # g(Q) DRbar
     3     1.03009169E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30684926E+03  # The trilinear couplings
  1  1     1.38113691E-06   # A_u(Q) DRbar
  2  2     1.38115005E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30684926E+03  # The trilinear couplings
  1  1     5.10358477E-07   # A_d(Q) DRbar
  2  2     5.10405566E-07   # A_s(Q) DRbar
  3  3     9.11403721E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30684926E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.12467770E-07   # A_mu(Q) DRbar
  3  3     1.13607120E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30684926E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65822807E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30684926E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86143373E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30684926E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02905967E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30684926E+03  # The soft SUSY breaking masses at the scale Q
         1     4.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56643892E+07   # M^2_Hd              
        22    -2.33169288E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40280629E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.71527401E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44942606E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44942606E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55057394E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55057394E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.76465951E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.01218270E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.14298063E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.66251729E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.18231938E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.32521114E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.20329311E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13740721E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.37457372E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.10903768E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.25885802E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.49465985E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.36137415E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.41038195E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.62969962E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.03645994E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.08385000E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.81554245E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.67632796E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.31760437E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.71905252E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.67016559E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.56772046E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80604741E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56793410E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.02596235E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62147245E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.02391685E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13193262E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.50477597E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.42089614E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.75236640E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.14054354E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78895641E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.44856129E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.33240075E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76117496E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79240979E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.55558887E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57275545E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52559728E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61113405E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.39089363E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.56285050E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.02921984E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.01628811E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45733197E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78916434E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.43419645E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.52491093E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.57556635E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79789277E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.02639572E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60572269E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52776579E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54463654E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.83969376E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.71085973E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.28993701E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.04663269E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85853250E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78895641E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.44856129E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.33240075E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76117496E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79240979E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.55558887E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57275545E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52559728E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61113405E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.39089363E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.56285050E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.02921984E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.01628811E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45733197E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78916434E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.43419645E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.52491093E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.57556635E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79789277E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.02639572E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60572269E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52776579E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54463654E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.83969376E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.71085973E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.28993701E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.04663269E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85853250E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13650046E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.25836196E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.70166653E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.06081963E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78379461E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.19170768E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58303850E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02865543E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.24919053E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20925623E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73870959E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.31549517E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13650046E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.25836196E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.70166653E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.06081963E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78379461E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.19170768E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58303850E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02865543E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.24919053E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20925623E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73870959E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.31549517E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06670397E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25162525E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49771899E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.50641969E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40932298E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56379369E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82648081E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05773553E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.26934623E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.91661845E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.31340112E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44435445E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.84575689E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89666066E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13755000E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09905760E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.21190594E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.11048825E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78824352E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.26533309E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55978482E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13755000E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09905760E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.21190594E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.11048825E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78824352E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.26533309E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55978482E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45835961E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97474386E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.36050303E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.63816282E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53175990E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.50198160E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04839076E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.97113804E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33660005E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33660005E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11221454E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11221454E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10237082E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.57298411E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.67682421E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.54531127E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.05830880E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.14075879E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.88020113E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.73224249E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.96713398E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.15844154E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.74996076E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18438754E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53642567E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18438754E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53642567E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38307142E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52370562E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52370562E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48409864E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04461506E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04461506E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04461493E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.12629543E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.12629543E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.12629543E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.12629543E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.04209925E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.04209925E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.04209925E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.04209925E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.47706889E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.47706889E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.47322002E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.20937368E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.72620392E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.33188647E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.19730433E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.33188647E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.19730433E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.14180977E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.64287255E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.64287255E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.65548182E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.54085698E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.54085698E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.54082672E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.73055256E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.24498181E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.73055256E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.24498181E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.07493254E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.14901840E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.14901840E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.85213038E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02928813E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02928813E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02928814E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.06625453E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.06625453E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.06625453E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.06625453E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.55413871E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.55413871E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.55413871E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.55413871E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.41263416E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.41263416E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.57137058E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.49800033E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.83232081E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.47773249E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.87987710E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.87987710E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42575488E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.19744177E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.32522111E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.79880211E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.79880211E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.81279662E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.81279662E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.99712691E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91462838E-01    2           5        -5   # BR(h -> b       bb     )
     6.51470110E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30620214E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89125366E-04    2           3        -3   # BR(h -> s       sb     )
     2.12455929E-02    2           4        -4   # BR(h -> c       cb     )
     6.94847433E-02    2          21        21   # BR(h -> g       g      )
     2.39404500E-03    2          22        22   # BR(h -> gam     gam    )
     1.63270641E-03    2          22        23   # BR(h -> Z       gam    )
     2.20110892E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78024260E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.52173127E+01   # H decays
#          BR         NDA      ID1       ID2
     3.64728954E-01    2           5        -5   # BR(H -> b       bb     )
     6.00464997E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12309879E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51125932E-04    2           3        -3   # BR(H -> s       sb     )
     7.04284211E-08    2           4        -4   # BR(H -> c       cb     )
     7.05839425E-03    2           6        -6   # BR(H -> t       tb     )
     8.44335333E-07    2          21        21   # BR(H -> g       g      )
     2.33903572E-09    2          22        22   # BR(H -> gam     gam    )
     1.81683062E-09    2          23        22   # BR(H -> Z       gam    )
     1.63711138E-06    2          24       -24   # BR(H -> W+      W-     )
     8.17988582E-07    2          23        23   # BR(H -> Z       Z      )
     6.66849482E-06    2          25        25   # BR(H -> h       h      )
     2.38211756E-24    2          36        36   # BR(H -> A       A      )
     3.57480874E-20    2          23        36   # BR(H -> Z       A      )
     1.86895823E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54543807E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.54543807E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.49512941E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.37876821E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.63878570E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.26412306E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.45439024E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.96043662E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.49696260E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.28276896E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.26418584E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.54822899E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.08247496E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.08247496E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.41016477E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.52090676E+01   # A decays
#          BR         NDA      ID1       ID2
     3.64808779E-01    2           5        -5   # BR(A -> b       bb     )
     6.00556731E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12342147E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51205109E-04    2           3        -3   # BR(A -> s       sb     )
     7.09016389E-08    2           4        -4   # BR(A -> c       cb     )
     7.07371083E-03    2           6        -6   # BR(A -> t       tb     )
     1.45346822E-05    2          21        21   # BR(A -> g       g      )
     6.74243644E-08    2          22        22   # BR(A -> gam     gam    )
     1.59771090E-08    2          23        22   # BR(A -> Z       gam    )
     1.63391353E-06    2          23        25   # BR(A -> Z       h      )
     1.91292020E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.54539843E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.54539843E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.18912026E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.69728560E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41968755E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.68431272E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.86087516E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.41615006E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.64975954E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.12268231E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.96170740E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     9.36227954E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     9.36227954E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.54688999E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.88247246E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.97895022E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11401032E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.76477919E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19762702E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46390390E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.74315484E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.62806589E-06    2          24        25   # BR(H+ -> W+      h      )
     2.53850007E-14    2          24        36   # BR(H+ -> W+      A      )
     4.84769043E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.01400140E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.45370736E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.54042470E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.36082309E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.53337231E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.20555482E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.36761948E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.83672457E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
