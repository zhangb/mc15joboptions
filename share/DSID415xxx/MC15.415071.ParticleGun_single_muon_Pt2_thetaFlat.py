evgenConfig.description = "Single muon with flat Theta-phi and fixed pT = 2 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]
    
include("MC15JobOptions/ParticleGun_Common.py")
    
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 13
genSeq.ParticleGun.sampler.mom = PG.PtThetaMPhiSampler(pt=2000.0, theta=[[-0.18, -0.03], [0.03, 0.18]])
