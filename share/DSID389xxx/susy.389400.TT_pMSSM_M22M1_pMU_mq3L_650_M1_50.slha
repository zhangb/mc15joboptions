#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13937428E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04010132E+01   # W+
        25     1.25850461E+02   # h
        35     3.00018980E+03   # H
        36     2.99999961E+03   # A
        37     3.00098783E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03138356E+03   # ~d_L
   2000001     3.02609013E+03   # ~d_R
   1000002     3.03046359E+03   # ~u_L
   2000002     3.02765431E+03   # ~u_R
   1000003     3.03138356E+03   # ~s_L
   2000003     3.02609013E+03   # ~s_R
   1000004     3.03046359E+03   # ~c_L
   2000004     3.02765431E+03   # ~c_R
   1000005     7.38048487E+02   # ~b_1
   2000005     3.02541720E+03   # ~b_2
   1000006     7.32657580E+02   # ~t_1
   2000006     3.01050777E+03   # ~t_2
   1000011     3.00662657E+03   # ~e_L
   2000011     3.00159016E+03   # ~e_R
   1000012     3.00522515E+03   # ~nu_eL
   1000013     3.00662657E+03   # ~mu_L
   2000013     3.00159016E+03   # ~mu_R
   1000014     3.00522515E+03   # ~nu_muL
   1000015     2.98596602E+03   # ~tau_1
   2000015     3.02203707E+03   # ~tau_2
   1000016     3.00517925E+03   # ~nu_tauL
   1000021     2.33844332E+03   # ~g
   1000022     5.00175143E+01   # ~chi_10
   1000023     1.08426581E+02   # ~chi_20
   1000025    -2.99922178E+03   # ~chi_30
   1000035     2.99961458E+03   # ~chi_40
   1000024     1.08578131E+02   # ~chi_1+
   1000037     3.00032103E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886282E-01   # N_11
  1  2    -2.69786363E-03   # N_12
  1  3     1.48044970E-02   # N_13
  1  4    -9.85278757E-04   # N_14
  2  1     3.08604635E-03   # N_21
  2  2     9.99651803E-01   # N_22
  2  3    -2.61159579E-02   # N_23
  2  4     2.16948741E-03   # N_24
  3  1    -9.72271476E-03   # N_31
  3  2     1.69610372E-02   # N_32
  3  3     7.06817887E-01   # N_33
  3  4     7.07125355E-01   # N_34
  4  1    -1.11070927E-02   # N_41
  4  2     2.00329320E-02   # N_42
  4  3     7.06758275E-01   # N_43
  4  4    -7.07084193E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99315800E-01   # U_11
  1  2    -3.69855652E-02   # U_12
  2  1     3.69855652E-02   # U_21
  2  2     9.99315800E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995276E-01   # V_11
  1  2    -3.07368648E-03   # V_12
  2  1     3.07368648E-03   # V_21
  2  2     9.99995276E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98594935E-01   # cos(theta_t)
  1  2    -5.29920352E-02   # sin(theta_t)
  2  1     5.29920352E-02   # -sin(theta_t)
  2  2     9.98594935E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99919801E-01   # cos(theta_b)
  1  2     1.26645793E-02   # sin(theta_b)
  2  1    -1.26645793E-02   # -sin(theta_b)
  2  2     9.99919801E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06873545E-01   # cos(theta_tau)
  1  2     7.07339940E-01   # sin(theta_tau)
  2  1    -7.07339940E-01   # -sin(theta_tau)
  2  2     7.06873545E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01782085E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39374276E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44350434E+02   # higgs               
         4     7.06198801E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39374276E+03  # The gauge couplings
     1     3.61884813E-01   # gprime(Q) DRbar
     2     6.41644671E-01   # g(Q) DRbar
     3     1.02803658E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39374276E+03  # The trilinear couplings
  1  1     1.83405036E-06   # A_u(Q) DRbar
  2  2     1.83407739E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39374276E+03  # The trilinear couplings
  1  1     4.46386489E-07   # A_d(Q) DRbar
  2  2     4.46478415E-07   # A_s(Q) DRbar
  3  3     1.05597280E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39374276E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.83058591E-08   # A_mu(Q) DRbar
  3  3     9.94888592E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39374276E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55257351E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39374276E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.09819416E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39374276E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04713110E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39374276E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.41197664E+04   # M^2_Hd              
        22    -9.10357164E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42907185E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.65383546E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47804276E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47804276E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52195724E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52195724E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.70939528E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.12568420E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.06403276E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.82339882E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17928118E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.57825111E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.89293565E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.79644296E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.67203292E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.85839432E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72822528E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63006740E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.21663131E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.50442372E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.33338596E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.52508038E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.34158102E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.22001152E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.94236681E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.80145112E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.81304730E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.68480201E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -4.29118384E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31046285E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.84206657E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.13654950E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.97239120E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12133868E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.68641980E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65473362E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.86358117E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.55671143E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.30788159E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.43413088E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.98052014E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19046431E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59000994E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.51232002E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.16019932E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.82470148E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40997479E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12633868E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.02010042E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65025597E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.03643240E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.10958190E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30210595E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.19934056E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.98743416E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67807150E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52637615E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.30520400E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.57246844E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.99417086E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54735804E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12133868E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.68641980E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65473362E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.86358117E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.55671143E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.30788159E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.43413088E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.98052014E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19046431E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59000994E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.51232002E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.16019932E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.82470148E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40997479E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12633868E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.02010042E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65025597E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.03643240E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.10958190E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30210595E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.19934056E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.98743416E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67807150E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52637615E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.30520400E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.57246844E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.99417086E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54735804E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07201341E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.51933559E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02439001E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.64068903E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.24425118E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02367626E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.45240733E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56284669E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990493E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.50619288E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.35411781E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.13794842E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07201341E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.51933559E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02439001E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.64068903E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.24425118E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02367626E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.45240733E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56284669E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990493E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.50619288E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.35411781E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.13794842E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84661675E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42478719E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19779599E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37741683E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78623903E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50479845E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17142424E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.00734571E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.15124043E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32344619E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.15260186E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07238382E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.69784421E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00171957E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.42532424E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.90836021E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02849595E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.07275714E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07238382E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.69784421E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00171957E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.42532424E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.90836021E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02849595E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.07275714E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07266140E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.69703494E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00146890E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.38828951E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.86086070E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02882155E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.00964417E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.76938633E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35844979E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.37299650E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35844979E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.37299650E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09614419E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.45769444E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09614419E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.45769444E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09081184E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.64203741E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.57709926E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.24052182E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.15027481E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.94922235E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.13322863E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.80989343E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.12528664E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.18137514E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.30109866E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.06163611E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.88147186E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.06163611E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.88147186E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     8.40178323E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.29827941E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.29827941E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47003153E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.30640478E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.30640478E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.30548016E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.68508062E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.25480919E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.01391926E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.75341026E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.75341026E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.84146074E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.92955053E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.64769925E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.64769925E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.17834007E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.17834007E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.46770282E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.46770282E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.57788839E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.73445855E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.91424485E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.83916254E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.83916254E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30189505E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.18991277E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.62640346E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.62640346E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.20699173E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.20699173E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.18498396E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.18498396E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.99154272E-03   # h decays
#          BR         NDA      ID1       ID2
     6.62976751E-01    2           5        -5   # BR(h -> b       bb     )
     5.26447904E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.86361083E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.94998322E-04    2           3        -3   # BR(h -> s       sb     )
     1.70530296E-02    2           4        -4   # BR(h -> c       cb     )
     5.62494391E-02    2          21        21   # BR(h -> g       g      )
     1.94342497E-03    2          22        22   # BR(h -> gam     gam    )
     1.34983329E-03    2          22        23   # BR(h -> Z       gam    )
     1.83860433E-01    2          24       -24   # BR(h -> W+      W-     )
     2.32997438E-02    2          23        23   # BR(h -> Z       Z      )
     4.11956736E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.40785027E+01   # H decays
#          BR         NDA      ID1       ID2
     7.33143495E-01    2           5        -5   # BR(H -> b       bb     )
     1.76639030E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24553401E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66949756E-04    2           3        -3   # BR(H -> s       sb     )
     2.16384866E-07    2           4        -4   # BR(H -> c       cb     )
     2.15857931E-02    2           6        -6   # BR(H -> t       tb     )
     3.58120934E-05    2          21        21   # BR(H -> g       g      )
     2.53890060E-08    2          22        22   # BR(H -> gam     gam    )
     8.31989887E-09    2          23        22   # BR(H -> Z       gam    )
     3.02839933E-05    2          24       -24   # BR(H -> W+      W-     )
     1.51233280E-05    2          23        23   # BR(H -> Z       Z      )
     8.07739136E-05    2          25        25   # BR(H -> h       h      )
     2.99264357E-23    2          36        36   # BR(H -> A       A      )
     1.93565033E-17    2          23        36   # BR(H -> Z       A      )
     2.42760825E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13349336E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20559167E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.40109500E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.25853782E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.89821139E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31983139E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82097159E-01    2           5        -5   # BR(A -> b       bb     )
     1.88411467E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.66176993E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.18203693E-04    2           3        -3   # BR(A -> s       sb     )
     2.30885484E-07    2           4        -4   # BR(A -> c       cb     )
     2.30196326E-02    2           6        -6   # BR(A -> t       tb     )
     6.77906245E-05    2          21        21   # BR(A -> g       g      )
     4.30215557E-08    2          22        22   # BR(A -> gam     gam    )
     6.67787168E-08    2          23        22   # BR(A -> Z       gam    )
     3.21761746E-05    2          23        25   # BR(A -> Z       h      )
     2.64672770E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22663566E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31439682E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.03264297E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00695891E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.08180885E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.47034195E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.73452658E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.92356574E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.13308081E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05603978E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.04771808E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.22417521E-05    2          24        25   # BR(H+ -> W+      h      )
     1.23152611E-13    2          24        36   # BR(H+ -> W+      A      )
     2.07354016E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.97583940E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.30086404E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
