#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980741E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.01990000E+02   # M_1(MX)             
         2     8.03990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04042668E+01   # W+
        25     1.25199305E+02   # h
        35     3.00009902E+03   # H
        36     2.99999992E+03   # A
        37     3.00109008E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03506234E+03   # ~d_L
   2000001     3.02997030E+03   # ~d_R
   1000002     3.03415580E+03   # ~u_L
   2000002     3.03193422E+03   # ~u_R
   1000003     3.03506234E+03   # ~s_L
   2000003     3.02997030E+03   # ~s_R
   1000004     3.03415580E+03   # ~c_L
   2000004     3.03193422E+03   # ~c_R
   1000005     8.50705891E+02   # ~b_1
   2000005     3.02879732E+03   # ~b_2
   1000006     8.48104395E+02   # ~t_1
   2000006     3.02309598E+03   # ~t_2
   1000011     3.00655695E+03   # ~e_L
   2000011     3.00114849E+03   # ~e_R
   1000012     3.00517264E+03   # ~nu_eL
   1000013     3.00655695E+03   # ~mu_L
   2000013     3.00114849E+03   # ~mu_R
   1000014     3.00517264E+03   # ~nu_muL
   1000015     2.98619009E+03   # ~tau_1
   2000015     3.02165941E+03   # ~tau_2
   1000016     3.00524436E+03   # ~nu_tauL
   1000021     2.34352811E+03   # ~g
   1000022     4.05235757E+02   # ~chi_10
   1000023     8.39826457E+02   # ~chi_20
   1000025    -2.99913871E+03   # ~chi_30
   1000035     2.99958957E+03   # ~chi_40
   1000024     8.39987193E+02   # ~chi_1+
   1000037     3.00033514E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888432E-01   # N_11
  1  2    -9.92718109E-05   # N_12
  1  3    -1.48838973E-02   # N_13
  1  4    -1.25851648E-03   # N_14
  2  1     5.15827953E-04   # N_21
  2  2     9.99604007E-01   # N_22
  2  3     2.74728181E-02   # N_23
  2  4     6.06690510E-03   # N_24
  3  1    -9.63057498E-03   # N_31
  3  2     1.51405448E-02   # N_32
  3  3    -7.06866283E-01   # N_33
  3  4     7.07119562E-01   # N_34
  4  1     1.14065799E-02   # N_41
  4  2    -2.37188363E-02   # N_42
  4  3     7.06656757E-01   # N_43
  4  4     7.07066853E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99245073E-01   # U_11
  1  2     3.88495074E-02   # U_12
  2  1    -3.88495074E-02   # U_21
  2  2     9.99245073E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99963209E-01   # V_11
  1  2    -8.57792369E-03   # V_12
  2  1    -8.57792369E-03   # V_21
  2  2    -9.99963209E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98559207E-01   # cos(theta_t)
  1  2    -5.36610670E-02   # sin(theta_t)
  2  1     5.36610670E-02   # -sin(theta_t)
  2  2     9.98559207E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99728756E-01   # cos(theta_b)
  1  2    -2.32897923E-02   # sin(theta_b)
  2  1     2.32897923E-02   # -sin(theta_b)
  2  2     9.99728756E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06957917E-01   # cos(theta_tau)
  1  2     7.07255614E-01   # sin(theta_tau)
  2  1    -7.07255614E-01   # -sin(theta_tau)
  2  2    -7.06957917E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00113598E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49807410E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44090989E+02   # higgs               
         4     1.07510943E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49807410E+03  # The gauge couplings
     1     3.62145083E-01   # gprime(Q) DRbar
     2     6.36593483E-01   # g(Q) DRbar
     3     1.02635400E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49807410E+03  # The trilinear couplings
  1  1     2.14603445E-06   # A_u(Q) DRbar
  2  2     2.14606511E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49807410E+03  # The trilinear couplings
  1  1     7.75374137E-07   # A_d(Q) DRbar
  2  2     7.75460694E-07   # A_s(Q) DRbar
  3  3     1.54656892E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49807410E+03  # The trilinear couplings
  1  1     3.22015853E-07   # A_e(Q) DRbar
  2  2     3.22031399E-07   # A_mu(Q) DRbar
  3  3     3.26359889E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49807410E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52916220E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49807410E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.80178317E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49807410E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00881020E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49807410E+03  # The soft SUSY breaking masses at the scale Q
         1     4.01990000E+02   # M_1(Q)              
         2     8.03990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.50117063E+04   # M^2_Hd              
        22    -9.06557223E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40651784E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.15658375E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47975357E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47975357E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52024643E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52024643E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.44320727E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.62168905E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.78310947E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15883191E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.52603231E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.36819686E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.77741358E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.11365556E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.95754373E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69982687E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61432289E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.16464631E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.98366115E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.70255206E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.97447936E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.37681284E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.99920156E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.69167022E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.29687643E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.14426789E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -2.42130299E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.49664575E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.15488382E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01375288E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.47354787E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56599897E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.15485465E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.51823170E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.91951109E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.90619934E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.03786157E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.48937788E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.38235735E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16293445E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55361906E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.66433917E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.02885835E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.09641039E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44638039E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.57135733E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.16475114E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.51701336E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.27220230E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.42976046E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.03225224E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.21262132E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.38908260E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66195806E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.41226591E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04049681E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.02491426E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.76039959E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55877326E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56599897E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.15485465E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.51823170E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.91951109E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.90619934E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.03786157E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.48937788E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.38235735E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16293445E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55361906E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.66433917E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.02885835E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.09641039E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44638039E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.57135733E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.16475114E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.51701336E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.27220230E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.42976046E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.03225224E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.21262132E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.38908260E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66195806E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.41226591E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04049681E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.02491426E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.76039959E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55877326E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.46510091E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.09055070E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97252964E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.93148314E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.22533258E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93691945E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.80482257E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.50915384E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999765E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.34563816E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.72534088E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.45645561E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.46510091E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.09055070E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97252964E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.93148314E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.22533258E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93691945E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.80482257E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.50915384E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999765E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.34563816E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.72534088E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.45645561E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.50943554E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75642933E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08326863E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16030204E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46249615E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.83989765E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05538785E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.24472780E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.04906552E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10436243E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.22691962E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.46502304E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09079712E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96727801E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.39210352E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.39628973E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94192480E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.32409623E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.46502304E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09079712E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96727801E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.39210352E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.39628973E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94192480E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.32409623E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46545959E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09068766E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96699902E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.44888175E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.50918124E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94230647E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.77397699E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.41601604E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.03398610E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.34771320E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.65882723E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.52991246E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.92966468E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.96330697E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.77012254E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.27145624E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.17745818E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.49030437E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.50969563E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.05058288E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.17732759E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.20020237E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.75509487E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.75509487E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.69435699E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.74295012E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.36337011E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.36337011E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.61619410E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.61619410E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.20231771E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.20231771E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.96103867E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.48252693E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.73338732E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.81573533E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.81573533E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.23907148E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.56147527E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.33102618E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.33102618E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.68286792E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.68286792E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.15431327E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.15431327E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.62494770E-03   # h decays
#          BR         NDA      ID1       ID2
     5.58218727E-01    2           5        -5   # BR(h -> b       bb     )
     7.16373606E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53597404E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.38097385E-04    2           3        -3   # BR(h -> s       sb     )
     2.33845165E-02    2           4        -4   # BR(h -> c       cb     )
     7.61610794E-02    2          21        21   # BR(h -> g       g      )
     2.61328489E-03    2          22        22   # BR(h -> gam     gam    )
     1.75500146E-03    2          22        23   # BR(h -> Z       gam    )
     2.35837423E-01    2          24       -24   # BR(h -> W+      W-     )
     2.96009126E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.68890372E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00269323E-01    2           5        -5   # BR(H -> b       bb     )
     6.74124242E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.38354223E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.92699526E-04    2           3        -3   # BR(H -> s       sb     )
     8.20321445E-08    2           4        -4   # BR(H -> c       cb     )
     8.18323275E-03    2           6        -6   # BR(H -> t       tb     )
     8.72869119E-06    2          21        21   # BR(H -> g       g      )
     7.13598869E-08    2          22        22   # BR(H -> gam     gam    )
     3.35822834E-09    2          23        22   # BR(H -> Z       gam    )
     6.70949096E-07    2          24       -24   # BR(H -> W+      W-     )
     3.35061031E-07    2          23        23   # BR(H -> Z       Z      )
     5.51135570E-06    2          25        25   # BR(H -> h       h      )
    -7.40682838E-25    2          36        36   # BR(H -> A       A      )
     2.44122551E-19    2          23        36   # BR(H -> Z       A      )
     6.03993528E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.92938055E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.01743517E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.27746406E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24106118E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.17424916E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.60794729E+01   # A decays
#          BR         NDA      ID1       ID2
     9.20486827E-01    2           5        -5   # BR(A -> b       bb     )
     6.89232339E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.43695744E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.99308978E-04    2           3        -3   # BR(A -> s       sb     )
     8.44607510E-08    2           4        -4   # BR(A -> c       cb     )
     8.42086486E-03    2           6        -6   # BR(A -> t       tb     )
     2.47986414E-05    2          21        21   # BR(A -> g       g      )
     6.32967664E-08    2          22        22   # BR(A -> gam     gam    )
     2.44421719E-08    2          23        22   # BR(A -> Z       gam    )
     6.83397771E-07    2          23        25   # BR(A -> Z       h      )
     8.60753891E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.26099138E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.29924298E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.67127308E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.96666289E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46864060E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.27131081E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21738254E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.39928831E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.30309908E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.68089382E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24083517E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.22663812E-07    2          24        25   # BR(H+ -> W+      h      )
     5.10735331E-14    2          24        36   # BR(H+ -> W+      A      )
     4.54723431E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.06042348E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07671306E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
