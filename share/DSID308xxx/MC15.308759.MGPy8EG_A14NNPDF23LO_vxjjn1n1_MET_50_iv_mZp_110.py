model="InelasticVectorEFT"
mDM1 = 55.
mDM2 = 220.
mZp = 110.
mHD = 125.
widthZp = 4.376743e-01
widthN2 = 4.911753e-03
filteff = 7.64526e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
