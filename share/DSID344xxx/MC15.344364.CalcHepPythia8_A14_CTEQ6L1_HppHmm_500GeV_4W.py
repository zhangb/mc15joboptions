evgenConfig.description = "CalcHep+Pythia8 production of H++H-- with the A14 CTEQ6L1 tune"
evgenConfig.keywords = ["Higgs"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "group.phys-gener.calchep.344364.HppHmm_H500_4W2L.TXT_v1"
evgenConfig.contact = ["Yanwen Liu<Yanwen.Liu@cern.ch>"]
include( "MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_Common.py" )
include( "MC15JobOptions/Pythia8_LHEF.py" )
evgenConfig.generators += ["CalcHep", "Pythia8"]
#include("Pythia8_AU2_CTEQ6L1_Common.py")
#include("MC15JobOptions/Pythia8_MadGraph.py")
#include("MC15JobOptions/Pythia8_Photos.py")
if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()
    pass


MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 10.0
MultiLeptonFilter.NLeptons = 2
