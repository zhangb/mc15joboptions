evgenConfig.description = "Protos BB pair production (B singlet, M = 1200 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["Mark Cooke <mark.stephen.cooke@cern.ch>"]
evgenConfig.inputfilecheck = "protoslhef"
evgenConfig.process = "pp>BB"

include("MC15JobOptions/ProtosLHEF_Common.py") 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
