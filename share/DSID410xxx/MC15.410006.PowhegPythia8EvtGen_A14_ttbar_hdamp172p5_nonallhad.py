#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ttbar production with Powheg hdamp equal top mass, A14 tune, at least one lepton filter"
evgenConfig.keywords    = [ "SM", "top", "ttbar", "lepton"]
evgenConfig.contact     = [ "james.robinson@cern.ch", "andrea.helen.knue@cern.ch", "onofrio@liverpool.ac.uk" ]

#--------------------------------------------------------------
# Powheg tt setup
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_tt_Common.py")

if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"
PowhegConfig.hdamp = 172.5

# Compensate for TTbarWToLeptonFilter efficiency
PowhegConfig.nEvents *= 3.
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
if "PYTHIA8VER" in os.environ:
  genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
  genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
else:
  genSeq.Pythia8.UserModes += [ "Main31:pTHard = 0" ]
  genSeq.Pythia8.UserModes += [ "Main31:NFinal = 2" ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
