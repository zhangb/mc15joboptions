model="LightVector"
mDM1 = 5.
mDM2 = 830.
mZp = 800.
mHD = 125.
widthZp = 3.810994e+00
widthN2 = 2.105499e-01
filteff = 8.361204e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
