#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 t-channel single top production (2->3) production (top-quark), A14 var3c up, scale 0.5, ME CT10f4 NLO, NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton', 'singleTop', 'tChannel']
evgenConfig.contact     = [ 'cescobar@cern.ch', 'ian.connelly@cern.ch']


#--------------------------------------------------------------
# Read in LHE files
#--------------------------------------------------------------

evgenConfig.inputfilecheck = "410018.singletop_tchan2to3nlo_top_lept"


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py')

include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#-------------------------------------------------------------
# Filters
#-------------------------------------------------------------
