evgenConfig.description = "Dijet truth jet slice JZ3, with the A14 NNPDF23 LO tune, filtered for 4 jets (2 b-jets)"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["bill.balunas@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection = on",
                            "PhaseSpace:pTHatMin = 150."]

include("MC15JobOptions/Pythia8_ShowerWeights.py")
include("MC15JobOptions/JetFilter_JZ3.py")
include("MC15JobOptions/MultiBJetFilter.py")

filtSeq.MultiBjetFilter.JetPtMin = 25000.
filtSeq.MultiBjetFilter.NJetsMin = 4
filtSeq.MultiBjetFilter.NBJetsMin = 2

evgenConfig.minevents = 2000
