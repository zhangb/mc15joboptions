# JO for Pythia 8 jet jet JZ1W slice

evgenConfig.description = "Dijet truth jet slice JZ1W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ1W.py")
evgenConfig.minevents = 1000
