include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "JHU+Pythia8, Higgs Transverse, mVV = 2000, @LO"
evgenConfig.keywords = ["BSM", "spin2", "exotic", "diboson", "jets" ]
evgenConfig.contact  = [ "sebastian.andres.olivares.pino@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.generators = ["JHU", "Pythia8", "EvtGen"]
evgenConfig.process = "gg -> H -> ZZ -> jet+jet"
evgenConfig.inputfilecheck = 'gg_HiggsTransverse_ZZ4q_m2000'
