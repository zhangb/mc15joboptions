#--------------------------------------------------------------
# Powheg Wj setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Zj_Common.py')

PowhegConfig.foldx=10
PowhegConfig.foldy=10
PowhegConfig.foldphi=10
PowhegConfig.bornktmin=0.
PowhegConfig.bornsuppfact=0.
PowhegConfig.PDF=13000
PowhegConfig.vdecaymode=2
# PowhegConfig.NNLO_reweighting_inputs = { 'DYNNLO':'Wp_CM8_MMHT14NNLO_11.top' }
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_SM_Common.py')
#include('MC15JobOptions/Pythia8_Powheg.py')
#include('MC15JobOptions/Pythia8_Powheg_Main31.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')
genSeq.Photospp.PhotonSplitting = True

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z+j production with A14 NNPDF2.3 tune'
evgenConfig.keywords    = [ 'SM', 'Z', '1jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]
