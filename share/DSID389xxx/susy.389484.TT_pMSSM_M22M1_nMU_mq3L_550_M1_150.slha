#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12808313E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03982529E+01   # W+
        25     1.25977114E+02   # h
        35     3.00016192E+03   # H
        36     2.99999996E+03   # A
        37     3.00110088E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02630707E+03   # ~d_L
   2000001     3.02090005E+03   # ~d_R
   1000002     3.02539022E+03   # ~u_L
   2000002     3.02377624E+03   # ~u_R
   1000003     3.02630707E+03   # ~s_L
   2000003     3.02090005E+03   # ~s_R
   1000004     3.02539022E+03   # ~c_L
   2000004     3.02377624E+03   # ~c_R
   1000005     6.30599418E+02   # ~b_1
   2000005     3.02145704E+03   # ~b_2
   1000006     6.27236115E+02   # ~t_1
   2000006     3.02058525E+03   # ~t_2
   1000011     3.00716634E+03   # ~e_L
   2000011     3.00025571E+03   # ~e_R
   1000012     3.00577317E+03   # ~nu_eL
   1000013     3.00716634E+03   # ~mu_L
   2000013     3.00025571E+03   # ~mu_R
   1000014     3.00577317E+03   # ~nu_muL
   1000015     2.98625314E+03   # ~tau_1
   2000015     3.02225618E+03   # ~tau_2
   1000016     3.00615600E+03   # ~nu_tauL
   1000021     2.33245677E+03   # ~g
   1000022     1.51728470E+02   # ~chi_10
   1000023     3.22052720E+02   # ~chi_20
   1000025    -3.00484431E+03   # ~chi_30
   1000035     3.00485490E+03   # ~chi_40
   1000024     3.22214141E+02   # ~chi_1+
   1000037     3.00578314E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891886E-01   # N_11
  1  2     3.82113095E-04   # N_12
  1  3    -1.46992976E-02   # N_13
  1  4     3.31788755E-07   # N_14
  2  1     1.67038009E-06   # N_21
  2  2     9.99658475E-01   # N_22
  2  3     2.61001062E-02   # N_23
  2  4     1.31068268E-03   # N_24
  3  1     1.03972213E-02   # N_31
  3  2    -1.93800318E-02   # N_32
  3  3     7.06763462E-01   # N_33
  3  4     7.07107999E-01   # N_34
  4  1    -1.03977472E-02   # N_41
  4  2     1.75271731E-02   # N_42
  4  3    -7.06815481E-01   # N_43
  4  4     7.07104349E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99319019E-01   # U_11
  1  2     3.68984992E-02   # U_12
  2  1    -3.68984992E-02   # U_21
  2  2     9.99319019E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998286E-01   # V_11
  1  2    -1.85169805E-03   # V_12
  2  1    -1.85169805E-03   # V_21
  2  2    -9.99998286E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98628115E-01   # cos(theta_t)
  1  2    -5.23630397E-02   # sin(theta_t)
  2  1     5.23630397E-02   # -sin(theta_t)
  2  2     9.98628115E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99718040E-01   # cos(theta_b)
  1  2    -2.37453258E-02   # sin(theta_b)
  2  1     2.37453258E-02   # -sin(theta_b)
  2  2     9.99718040E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06913051E-01   # cos(theta_tau)
  1  2     7.07300458E-01   # sin(theta_tau)
  2  1    -7.07300458E-01   # -sin(theta_tau)
  2  2    -7.06913051E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00147325E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28083126E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44385255E+02   # higgs               
         4     1.12587641E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.28083126E+03  # The gauge couplings
     1     3.61622673E-01   # gprime(Q) DRbar
     2     6.38592474E-01   # g(Q) DRbar
     3     1.02988403E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28083126E+03  # The trilinear couplings
  1  1     1.53254489E-06   # A_u(Q) DRbar
  2  2     1.53256852E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28083126E+03  # The trilinear couplings
  1  1     5.42436196E-07   # A_d(Q) DRbar
  2  2     5.42501857E-07   # A_s(Q) DRbar
  3  3     1.12933140E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28083126E+03  # The trilinear couplings
  1  1     2.55514857E-07   # A_e(Q) DRbar
  2  2     2.55527590E-07   # A_mu(Q) DRbar
  3  3     2.59145832E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28083126E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.56714453E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28083126E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.99090749E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28083126E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02776275E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28083126E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.57449358E+04   # M^2_Hd              
        22    -9.12694812E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41563018E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.04293518E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48051573E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48051573E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51948427E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51948427E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.02646914E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.74281363E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.65811443E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.16760421E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21327674E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.41265608E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.56257615E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.15476369E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.67095290E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.88045190E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71619541E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63498577E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.21901688E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.73807359E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.29207733E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.92270043E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.84809183E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.65721714E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.91841257E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -1.44304416E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.08574001E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.05017138E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.49427901E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.22886221E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.72604970E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.10024451E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.60623510E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02389678E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.91750973E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62637659E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.09884533E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.76599475E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25489194E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.26762201E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.05955617E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20274958E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57509308E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.32399706E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.18269095E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.17954486E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42490686E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02897389E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.86738791E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62572801E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.79452641E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.49504553E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24917946E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.83263221E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.06641734E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68296415E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48903666E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.23230486E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.59168369E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.58387694E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55109632E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02389678E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.91750973E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62637659E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.09884533E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.76599475E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25489194E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.26762201E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.05955617E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20274958E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57509308E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.32399706E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.18269095E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.17954486E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42490686E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02897389E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.86738791E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62572801E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.79452641E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.49504553E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24917946E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.83263221E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.06641734E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68296415E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48903666E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.23230486E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.59168369E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.58387694E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55109632E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96158784E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.83507384E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00690912E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.33583318E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.85038635E-11    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00958348E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.09248730E-10    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55278573E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.74083308E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.96158784E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.83507384E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00690912E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.33583318E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.85038635E-11    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00958348E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.09248730E-10    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55278573E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.74083308E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.78491185E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48664434E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17233645E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34101921E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72779208E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56759634E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14528081E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.08146614E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.73438995E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28692807E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.66187473E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96185550E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.80330796E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00523698E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.50331770E-11    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.29734390E-11    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01443222E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.96185550E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.80330796E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00523698E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.50331770E-11    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.29734390E-11    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01443222E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96271437E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.80244428E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00498609E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.49558286E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.26366869E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01476948E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.40053372E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.79387080E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.45047182E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.59539459E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.87855940E-17    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.87855940E-17    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     8.21801646E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.87989337E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.86809081E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.73134323E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.06192629E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.29253137E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.24659012E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.75340988E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.75769839E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.59131108E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.65404042E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.75448018E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.75448018E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.06472398E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.20475273E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.37411409E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.37411409E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.19208433E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.19208433E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     8.06610000E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     8.06610000E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     8.06610000E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     8.06610000E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     9.62930719E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     9.62930719E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.88089421E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.47767449E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.20924436E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.68207824E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.68207824E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.19123653E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.09391640E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.24929279E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.24929279E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.10383240E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.10383240E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     8.00309397E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.00309397E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.00309397E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.00309397E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     7.90380596E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.90380596E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.75394326E-03   # h decays
#          BR         NDA      ID1       ID2
     5.48379504E-01    2           5        -5   # BR(h -> b       bb     )
     6.96158847E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.46437690E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.22274772E-04    2           3        -3   # BR(h -> s       sb     )
     2.26938115E-02    2           4        -4   # BR(h -> c       cb     )
     7.47319870E-02    2          21        21   # BR(h -> g       g      )
     2.59009531E-03    2          22        22   # BR(h -> gam     gam    )
     1.81465523E-03    2          22        23   # BR(h -> Z       gam    )
     2.47938663E-01    2          24       -24   # BR(h -> W+      W-     )
     3.14666875E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.05806817E+01   # H decays
#          BR         NDA      ID1       ID2
     9.07885632E-01    2           5        -5   # BR(H -> b       bb     )
     6.12811470E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16675492E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.66077257E-04    2           3        -3   # BR(H -> s       sb     )
     7.45810472E-08    2           4        -4   # BR(H -> c       cb     )
     7.43994143E-03    2           6        -6   # BR(H -> t       tb     )
     4.86409517E-06    2          21        21   # BR(H -> g       g      )
     3.97758672E-08    2          22        22   # BR(H -> gam     gam    )
     3.04683278E-09    2          23        22   # BR(H -> Z       gam    )
     6.90106957E-07    2          24       -24   # BR(H -> W+      W-     )
     3.44627950E-07    2          23        23   # BR(H -> Z       Z      )
     4.95798305E-06    2          25        25   # BR(H -> h       h      )
    -3.48785524E-26    2          36        36   # BR(H -> A       A      )
     2.91215908E-18    2          23        36   # BR(H -> Z       A      )
     7.97631919E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.80642422E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.98848718E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.47468790E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.14121961E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.34315220E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.97157991E+01   # A decays
#          BR         NDA      ID1       ID2
     9.27655460E-01    2           5        -5   # BR(A -> b       bb     )
     6.26127143E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.21383286E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.71904646E-04    2           3        -3   # BR(A -> s       sb     )
     7.67276369E-08    2           4        -4   # BR(A -> c       cb     )
     7.64986166E-03    2           6        -6   # BR(A -> t       tb     )
     2.25281104E-05    2          21        21   # BR(A -> g       g      )
     5.72228177E-08    2          22        22   # BR(A -> gam     gam    )
     2.21812815E-08    2          23        22   # BR(A -> Z       gam    )
     7.02365581E-07    2          23        25   # BR(A -> Z       h      )
     8.45848412E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.92934316E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.22932694E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.57214594E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.32070019E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47847431E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.75746159E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.03569799E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.46222400E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19632715E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46122964E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.29604371E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.46729163E-07    2          24        25   # BR(H+ -> W+      h      )
     4.92484217E-14    2          24        36   # BR(H+ -> W+      A      )
     4.69349392E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.28021017E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.04014247E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
