include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "ttbb production with NLO+PS in the 4FS (CSSKIN), dilepton channel, cf. arXiv:1309.5912."
evgenConfig.keywords = ["top", "SM", "2lepton", "ttbar"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_NNPDF30NNLO_ttbb"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES VAR{FSF*H_TM2/4}{RSF*sqrt(MPerp(p[2])*MPerp(p[3])*MPerp(p[4])*MPerp(p[5]))}{QSF*H_TM2/4}

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % settings relevant for 4-flavour scheme
  MASSIVE[5]=1
  MASS[5]=4.75
  PDF_LIBRARY LHAPDFSherpa
  #LHAPDF_GRID_PATH=./PDFsets
  PDF_SET NNPDF30_nnlo_as_0118_nf_4
  USE_PDF_ALPHAS=1
  CSS_ENHANCE S{G}{t}{tb} 0
  CSS_ENHANCE S{G}{tb}{t} 0
  MCATNLO_MASSIVE_SPLITTINGS=0     # deactivates g->bb first emission in MC@NLO
  EXCLUSIVE_CLUSTER_MODE=1
  CSS_KMODE=34
  CSS_KIN_SCHEME=0

  % decays
  HARD_DECAYS=1
  STABLE[6]=0
  WIDTH[6]=0
  STABLE[24]=0
  HDH_STATUS[24,12,-11]=2                                                                                                                                                                                                                                                        
  HDH_STATUS[24,14,-13]=2                                                                                                                                                                                                                                                        
  HDH_STATUS[24,16,-15]=2                                                                                                                                                                                                                                                        
  HDH_STATUS[-24,-12,11]=2                                                                                                                                                                                                                                                       
  HDH_STATUS[-24,-14,13]=2                                                                                                                                                                                                                                                       
  HDH_STATUS[-24,-16,15]=2       

}(run)

(processes){
  Process 93 93 -> 6 -6 5 -5
  Order (*,0)
  NLO_QCD_Mode MC@NLO
  Loop_Generator LOOPGEN
  ME_Generator Amegic
  RS_ME_Generator Comix
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]

genSeq.Sherpa_i.Parameters += [ "PDF_VARIATIONS=PDF4LHC15_nlo_nf4_30[all]", "SCALE_VARIATIONS=None" ]
