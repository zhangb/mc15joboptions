#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig++ t-channel single top production (2->3) (antitop) with UEEE5 tune'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwigpp", "EvtGen"]

evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_antitop_lept"

#--------------------------------------------------------------
# HerwigPP (UEEE5) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')

