from MadGraphControl.MadGraphUtils import *

minevents=100
nevents=max(minevents,runArgs.maxEvents)
mode=0

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~ h [QCD]
output -f""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

print_cards()
    
runName='run_01'


process_dir = new_process()

#Fetch default NLO run_card.dat and set parameters
extras = { 'pdlabel':"'lhapdf'",
           'lhaid'  : 260000,
           'parton_shower':'PYTHIA8' }
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

# MadSpin
madspin_card_loc='madspin_card.dat'

mscard = open(madspin_card_loc,'w')

mscard.write("""
#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************                   

#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
## set BW_cut 15                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > all all                                                    
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
# running the actual code
launch"""%runArgs.randomSeed)

mscard.close()


generate(run_card_loc='run_card.dat',param_card_loc=None,madspin_card_loc=madspin_card_loc,mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  


evgenConfig.generators = ["aMcAtNlo","Pythia8"]

#### Shower
evgenConfig.description = 'MG5_aMcAtNlo_Pythia8_ttH_Hmumu'
evgenConfig.keywords+=['Higgs','top','2muon']
evgenConfig.contact = [ 'Xin.Chen@cern.ch' ]
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.000219 100 13 -13'
]
