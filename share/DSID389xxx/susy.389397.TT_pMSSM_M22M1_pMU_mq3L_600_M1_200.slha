#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13425095E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04004738E+01   # W+
        25     1.24408495E+02   # h
        35     3.00018475E+03   # H
        36     2.99999963E+03   # A
        37     3.00094401E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02910502E+03   # ~d_L
   2000001     3.02394181E+03   # ~d_R
   1000002     3.02818818E+03   # ~u_L
   2000002     3.02563825E+03   # ~u_R
   1000003     3.02910502E+03   # ~s_L
   2000003     3.02394181E+03   # ~s_R
   1000004     3.02818818E+03   # ~c_L
   2000004     3.02563825E+03   # ~c_R
   1000005     6.82865237E+02   # ~b_1
   2000005     3.02331683E+03   # ~b_2
   1000006     6.80631053E+02   # ~t_1
   2000006     3.00977176E+03   # ~t_2
   1000011     3.00653092E+03   # ~e_L
   2000011     3.00143943E+03   # ~e_R
   1000012     3.00513651E+03   # ~nu_eL
   1000013     3.00653092E+03   # ~mu_L
   2000013     3.00143943E+03   # ~mu_R
   1000014     3.00513651E+03   # ~nu_muL
   1000015     2.98579756E+03   # ~tau_1
   2000015     3.02210560E+03   # ~tau_2
   1000016     3.00514013E+03   # ~nu_tauL
   1000021     2.33581741E+03   # ~g
   1000022     2.01425176E+02   # ~chi_10
   1000023     4.24106969E+02   # ~chi_20
   1000025    -3.00019617E+03   # ~chi_30
   1000035     3.00083777E+03   # ~chi_40
   1000024     4.24269354E+02   # ~chi_1+
   1000037     3.00145413E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887585E-01   # N_11
  1  2    -9.61854874E-04   # N_12
  1  3     1.48629757E-02   # N_13
  1  4    -1.72722629E-03   # N_14
  2  1     1.36515083E-03   # N_21
  2  2     9.99633908E-01   # N_22
  2  3    -2.65855632E-02   # N_23
  2  4     4.83667711E-03   # N_24
  3  1    -9.27078235E-03   # N_31
  3  2     1.53901703E-02   # N_32
  3  3     7.06851899E-01   # N_33
  3  4     7.07133359E-01   # N_34
  4  1    -1.17049340E-02   # N_41
  4  2     2.22321106E-02   # N_42
  4  3     7.06705520E-01   # N_43
  4  4    -7.07061550E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99292339E-01   # U_11
  1  2    -3.76141095E-02   # U_12
  2  1     3.76141095E-02   # U_21
  2  2     9.99292339E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976577E-01   # V_11
  1  2    -6.84433111E-03   # V_12
  2  1     6.84433111E-03   # V_21
  2  2     9.99976577E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98869944E-01   # cos(theta_t)
  1  2    -4.75272025E-02   # sin(theta_t)
  2  1     4.75272025E-02   # -sin(theta_t)
  2  2     9.98869944E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99918280E-01   # cos(theta_b)
  1  2     1.27841043E-02   # sin(theta_b)
  2  1    -1.27841043E-02   # -sin(theta_b)
  2  2     9.99918280E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06927901E-01   # cos(theta_tau)
  1  2     7.07285616E-01   # sin(theta_tau)
  2  1    -7.07285616E-01   # -sin(theta_tau)
  2  2     7.06927901E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01888899E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.34250949E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44492346E+02   # higgs               
         4     7.00631871E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.34250949E+03  # The gauge couplings
     1     3.61803148E-01   # gprime(Q) DRbar
     2     6.38146649E-01   # g(Q) DRbar
     3     1.02884183E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.34250949E+03  # The trilinear couplings
  1  1     1.68572088E-06   # A_u(Q) DRbar
  2  2     1.68574609E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.34250949E+03  # The trilinear couplings
  1  1     4.34119602E-07   # A_d(Q) DRbar
  2  2     4.34202208E-07   # A_s(Q) DRbar
  3  3     9.80587321E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.34250949E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.26771137E-08   # A_mu(Q) DRbar
  3  3     9.37798045E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.34250949E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54417133E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.34250949E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13221705E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.34250949E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06073076E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.34250949E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.64193398E+04   # M^2_Hd              
        22    -9.09681163E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41355647E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.85506155E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48250125E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48250125E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51749875E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51749875E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.93898465E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.41199282E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.43533051E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.32347020E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.04091771E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.27394989E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.15433999E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.33576453E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.17806742E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.24552185E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.60899874E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51234743E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.99612908E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.63918691E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.29927008E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.13611780E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.53395519E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24018777E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.89115629E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.05524422E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.85171464E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.56937859E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -2.41355243E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28051356E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.94156015E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.24700407E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.11359361E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96329142E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.87612894E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61445544E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.23765775E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.54935775E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.22936243E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.70076724E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.09742041E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18975416E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57720219E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.85551132E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.51408023E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.84676800E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42279484E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96843088E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.99447081E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61213964E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.07972414E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.04821206E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.22364492E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.83661666E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.10426829E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68040169E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48607033E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.12171731E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.12383638E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.69843490E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55139213E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96329142E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.87612894E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61445544E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.23765775E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.54935775E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.22936243E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.70076724E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.09742041E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18975416E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57720219E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.85551132E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.51408023E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.84676800E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42279484E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96843088E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.99447081E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61213964E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.07972414E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.04821206E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.22364492E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.83661666E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.10426829E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68040169E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48607033E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.12171731E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.12383638E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.69843490E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55139213E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89384535E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.92777159E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00693800E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.69197801E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.08907945E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00028473E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.00770419E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54887673E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998193E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.80659619E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.95107135E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.22214059E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89384535E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.92777159E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00693800E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.69197801E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.08907945E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00028473E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.00770419E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54887673E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998193E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.80659619E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.95107135E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.22214059E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.74957304E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51794665E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16404313E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31801022E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69114522E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60140867E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13629583E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.65284091E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.09813084E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26197965E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.09512079E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89411551E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98996630E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99592862E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.43817945E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.12927789E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00507472E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.75689168E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89411551E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98996630E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99592862E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.43817945E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.12927789E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00507472E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.75689168E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89445556E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.98910626E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99567092E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.44015862E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.13267419E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00541491E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     3.49922161E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.19381718E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.73648586E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.71482177E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.25325211E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.12694214E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.84150766E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.12398789E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.67483816E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.11226211E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.22554351E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.76303539E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.52369646E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.76017056E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.34578676E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.60426727E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.64581205E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.64581205E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.68885163E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.24147128E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65618767E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65618767E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26205198E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26205198E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.40079161E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.40079161E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.67512359E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.52614922E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.23081897E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.71362167E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.71362167E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.40356580E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.89035183E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63096456E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.63096456E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28798304E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28798304E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.39480776E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.39480776E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.80184379E-03   # h decays
#          BR         NDA      ID1       ID2
     6.84125439E-01    2           5        -5   # BR(h -> b       bb     )
     5.41190283E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.91585172E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.06980924E-04    2           3        -3   # BR(h -> s       sb     )
     1.75630474E-02    2           4        -4   # BR(h -> c       cb     )
     5.68388916E-02    2          21        21   # BR(h -> g       g      )
     1.91926885E-03    2          22        22   # BR(h -> gam     gam    )
     1.23376394E-03    2          22        23   # BR(h -> Z       gam    )
     1.63369099E-01    2          24       -24   # BR(h -> W+      W-     )
     2.02328955E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39393138E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43115487E-01    2           5        -5   # BR(H -> b       bb     )
     1.78402341E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30788045E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.74606042E-04    2           3        -3   # BR(H -> s       sb     )
     2.18638199E-07    2           4        -4   # BR(H -> c       cb     )
     2.18105769E-02    2           6        -6   # BR(H -> t       tb     )
     3.20377773E-05    2          21        21   # BR(H -> g       g      )
     4.01706554E-08    2          22        22   # BR(H -> gam     gam    )
     8.37469834E-09    2          23        22   # BR(H -> Z       gam    )
     3.36310240E-05    2          24       -24   # BR(H -> W+      W-     )
     1.67947790E-05    2          23        23   # BR(H -> Z       Z      )
     8.64908908E-05    2          25        25   # BR(H -> h       h      )
    -6.30732097E-23    2          36        36   # BR(H -> A       A      )
     1.70400629E-17    2          23        36   # BR(H -> Z       A      )
     2.23293876E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11189718E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.11320117E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.08714395E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.09247660E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.16962971E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32356198E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82687535E-01    2           5        -5   # BR(A -> b       bb     )
     1.87880413E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.64299314E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.15897512E-04    2           3        -3   # BR(A -> s       sb     )
     2.30234713E-07    2           4        -4   # BR(A -> c       cb     )
     2.29547497E-02    2           6        -6   # BR(A -> t       tb     )
     6.75995499E-05    2          21        21   # BR(A -> g       g      )
     3.33353864E-08    2          22        22   # BR(A -> gam     gam    )
     6.65843054E-08    2          23        22   # BR(A -> Z       gam    )
     3.52837809E-05    2          23        25   # BR(A -> Z       h      )
     2.65090856E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22057265E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32151060E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.99415359E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.99635801E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.09169116E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.48840278E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.79838529E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.98681239E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.17061889E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.06376257E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.11155866E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.67906659E-05    2          24        25   # BR(H+ -> W+      h      )
     9.88748481E-14    2          24        36   # BR(H+ -> W+      A      )
     2.04188048E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.77099781E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.48211958E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
