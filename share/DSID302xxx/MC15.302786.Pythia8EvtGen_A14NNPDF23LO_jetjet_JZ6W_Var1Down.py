# JO for Pythia 8 jet jet JZ6W slice

evgenConfig.description = "Dijet truth jet slice JZ6W, with the A14 NNPDF23 LO Var1Down tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

evgenConfig.contact = ["marco.vanadia@cern.ch"]
evgenConfig.process = "QCD dijet"
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 600."]

include("MC15JobOptions/JetFilter_JZ6W.py")
evgenConfig.minevents = 1000
