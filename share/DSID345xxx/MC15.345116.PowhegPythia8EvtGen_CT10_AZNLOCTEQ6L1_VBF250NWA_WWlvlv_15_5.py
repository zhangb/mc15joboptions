#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 250.
PowhegConfig.width_H = 0.00407

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 1
# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 0

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.

# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onIfMatch = 11 12',
                             '24:onIfMatch = 13 14',
                             '24:onIfMatch = 15 16' ]

#--------------------------------------------------------------
# Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter)"


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->WW->lvlv"
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "VBF", "WW" ]
evgenConfig.contact     = [ 'Olivier.Arnaez@cern.ch' ]
