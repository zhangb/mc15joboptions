from MadGraphControl.MadGraphUtils import *
import math

#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------
minevents=5000
nevents=5500

#mode=0
# gridpack_dir='madevent/'
# gridpack_mode=True
gridpack_dir=None
gridpack_mode=False
safefactor=1.1

DSID = runArgs.runNumber

if DSID == 346583:
    modelExtension="-SMlimit_massless_SMEFT_H4l"
elif DSID == 346584:
    modelExtension="-cHW0p5_massless_SMEFT_H4l"
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')

#---------------------------------------------------------------------------------------------------
# generating VH-Lep to H4l
#---------------------------------------------------------------------------------------------------

fcard.write("""
import model SMEFTsim_A_U35_MwScheme_UFO"""+modelExtension+"""
set gauge unitary

define v = a z w+ w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-

generate p p > h l+ l- NP=1, h > l+ l- l+ l- NP=1
add process p p > h vl vl~ NP=1, h > l+ l- l+ l- NP=1
add process p p > h l- vl~ NP=1, h > l+ l- l+ l- NP=1
add process p p > h l+ vl NP=1, h > l+ l- l+ l- NP=1

output -f
""")
fcard.close()


#---------------------------------------------------------------------------------------------------
# require beam energy to be set as argument
#---------------------------------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#---------------------------------------------------------------------------------------------------
# creating run_card.dat for VHlep
#---------------------------------------------------------------------------------------------------

extras = {'lhe_version'    :'3.0',
          'pdlabel'        : "'nn23lo1'",
          'lhaid'          : 230000,
          'parton_shower'  :'PYTHIA8',
          'event_norm'     : 'sum',
          'cut_decays'     : 'T',
          'drll'           : '0.05',
          'bwcutoff'       : '15.0',
          'pta'            : '0.',
		  'ptl'            : '0.',
          'etal'           : '-1.0',
          'drjj'           : '0.0',
          'draa'           : '0.0',
          'etaj'           : '-1.0',
          'draj'           : '0.0',
          'drjl'           : '0.0',
          'dral'           : '0.0',
          'etaa'           : '-1.0',
          'ptj'            : '10.',
          'ptj1min'        : '0.',
          'ptj1max'        : '-1.0',
          'mmjj'           : '3',
          'mmjjmax'        : '-1'
          }

process_dir = new_process(grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()

param_card_loc='restrict_'+modelExtension[1:]+'.dat'
paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.wait()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card_loc)

runName='run_01'

generate(run_card_loc='run_card.dat',
        param_card_loc=param_card_loc,
        mode=0,
        grid_pack=gridpack_mode,
        gridpack_dir=gridpack_dir,
        njobs=1,
        run_name=runName,
        proc_dir=process_dir)

#---------------------------------------------------------------------------------------------------
# Multi-core capability
#---------------------------------------------------------------------------------------------------
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
#### Showering with Pythia 8
#---------------------------------------------------------------------------------------------------
evgenConfig.description = "VH-Lep 125 GeV Higgs production in the SMEFT model decaying to zz4l."
evgenConfig.keywords = ["Higgs", "leptonic", "BSM", "mH125"]
evgenConfig.process = "generate p p > h l+ l- NP=1, h > l+ l- l+ l- NP=1, add process p p > h vl vl~ NP=1, h > l+ l- l+ l- NP=1, add process p p > h l- vl~ NP=1, h > l+ l- l+ l- NP=1, add process p p > h l+ vl NP=1, h > l+ l- l+ l- NP=1"
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.minevents = minevents
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
evgenConfig.contact = ["Verena Maria Walbrecht <vwalbrec@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
