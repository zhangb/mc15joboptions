#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13998163E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.67990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04183265E+01   # W+
        25     1.25653556E+02   # h
        35     4.00000720E+03   # H
        36     3.99999762E+03   # A
        37     4.00105143E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02851794E+03   # ~d_L
   2000001     4.02461886E+03   # ~d_R
   1000002     4.02786719E+03   # ~u_L
   2000002     4.02528938E+03   # ~u_R
   1000003     4.02851794E+03   # ~s_L
   2000003     4.02461886E+03   # ~s_R
   1000004     4.02786719E+03   # ~c_L
   2000004     4.02528938E+03   # ~c_R
   1000005     1.12309099E+03   # ~b_1
   2000005     4.02683590E+03   # ~b_2
   1000006     1.10016188E+03   # ~t_1
   2000006     1.88242671E+03   # ~t_2
   1000011     4.00505904E+03   # ~e_L
   2000011     4.00341699E+03   # ~e_R
   1000012     4.00394716E+03   # ~nu_eL
   1000013     4.00505904E+03   # ~mu_L
   2000013     4.00341699E+03   # ~mu_R
   1000014     4.00394716E+03   # ~nu_muL
   1000015     4.00350232E+03   # ~tau_1
   2000015     4.00851995E+03   # ~tau_2
   1000016     4.00512873E+03   # ~nu_tauL
   1000021     1.98338658E+03   # ~g
   1000022     5.42077067E+02   # ~chi_10
   1000023    -5.80518410E+02   # ~chi_20
   1000025     6.01896242E+02   # ~chi_30
   1000035     2.05148375E+03   # ~chi_40
   1000024     5.78430026E+02   # ~chi_1+
   1000037     2.05164752E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.37054912E-01   # N_11
  1  2     2.30095255E-02   # N_12
  1  3     4.93643115E-01   # N_13
  1  4     4.61017456E-01   # N_14
  2  1    -2.89576721E-02   # N_21
  2  2     2.23539413E-02   # N_22
  2  3    -7.05378942E-01   # N_23
  2  4     7.07885798E-01   # N_24
  3  1     6.75211045E-01   # N_31
  3  2     2.79214053E-02   # N_32
  3  3     5.08587247E-01   # N_33
  3  4     5.33525494E-01   # N_34
  4  1    -1.24736798E-03   # N_41
  4  2     9.99095220E-01   # N_42
  4  3    -9.79983169E-03   # N_43
  4  4    -4.13660392E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.38652071E-02   # U_11
  1  2     9.99903873E-01   # U_12
  2  1    -9.99903873E-01   # U_21
  2  2     1.38652071E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.85075020E-02   # V_11
  1  2    -9.98286969E-01   # V_12
  2  1    -9.98286969E-01   # V_21
  2  2    -5.85075020E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86721933E-01   # cos(theta_t)
  1  2    -1.62418678E-01   # sin(theta_t)
  2  1     1.62418678E-01   # -sin(theta_t)
  2  2     9.86721933E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998207E-01   # cos(theta_b)
  1  2    -1.89367283E-03   # sin(theta_b)
  2  1     1.89367283E-03   # -sin(theta_b)
  2  2     9.99998207E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06400333E-01   # cos(theta_tau)
  1  2     7.07812524E-01   # sin(theta_tau)
  2  1    -7.07812524E-01   # -sin(theta_tau)
  2  2    -7.06400333E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00230426E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39981632E+03  # DRbar Higgs Parameters
         1    -5.67990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43560845E+02   # higgs               
         4     1.60986843E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39981632E+03  # The gauge couplings
     1     3.62083574E-01   # gprime(Q) DRbar
     2     6.35638386E-01   # g(Q) DRbar
     3     1.02835541E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39981632E+03  # The trilinear couplings
  1  1     1.59781704E-06   # A_u(Q) DRbar
  2  2     1.59783209E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39981632E+03  # The trilinear couplings
  1  1     5.92743366E-07   # A_d(Q) DRbar
  2  2     5.92797427E-07   # A_s(Q) DRbar
  3  3     1.05606554E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39981632E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.31990003E-07   # A_mu(Q) DRbar
  3  3     1.33340003E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39981632E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65222547E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39981632E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88334014E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39981632E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02732528E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39981632E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55756008E+07   # M^2_Hd              
        22    -2.69500869E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40168657E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.01174176E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37530219E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37530219E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62469781E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62469781E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.73529048E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.03235586E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.11210631E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.70467019E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.15086764E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.00417387E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.97530371E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12715468E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.21655830E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.19727171E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.64823329E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.24420333E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.36395079E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.24794640E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.57398950E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.19496963E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.77881044E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.84522304E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65888386E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60993243E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83568702E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54532819E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.32258325E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67599205E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.60621607E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11672140E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.48859378E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.83313788E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.03772314E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.25886139E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77733912E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.15858746E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.39421775E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.04784624E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82982649E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.77117377E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64753303E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51418912E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59752044E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.95178094E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.53212077E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.45637695E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.82663297E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45873052E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77754202E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.24999997E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.53783966E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.44974247E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83573035E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24131572E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68145690E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51635362E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53209028E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.69320609E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.18120135E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.40201981E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.25768702E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85892950E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77733912E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.15858746E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.39421775E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.04784624E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82982649E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.77117377E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64753303E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51418912E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59752044E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.95178094E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.53212077E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.45637695E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.82663297E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45873052E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77754202E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.24999997E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.53783966E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.44974247E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83573035E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24131572E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68145690E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51635362E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53209028E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.69320609E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.18120135E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.40201981E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.25768702E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85892950E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13068619E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.80318072E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.69133091E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.35977342E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78843017E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.89523631E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59321005E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.00452590E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.45411151E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.37270836E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.53750696E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.81468487E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13068619E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.80318072E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.69133091E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.35977342E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78843017E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.89523631E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59321005E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.00452590E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.45411151E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.37270836E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.53750696E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.81468487E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.04622732E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.95229316E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52655587E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.77687668E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41602124E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.61794428E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84035891E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.03425082E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.95353107E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.80992677E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61261687E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45724748E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.72675445E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92293645E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13172986E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.71172586E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.43255258E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.24579304E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79356169E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.37254801E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56952839E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13172986E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.71172586E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.43255258E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.24579304E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79356169E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.37254801E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56952839E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44771279E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.82444370E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.75352245E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.67519503E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53934673E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.41167760E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06276812E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.54402870E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33767208E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33767208E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11257046E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11257046E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09951491E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.45019194E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.38642693E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.20599071E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.18178308E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.71414089E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.34720673E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.48669279E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.14337950E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.61442008E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.78692029E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.30908567E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18951269E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54331228E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18951269E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54331228E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34485243E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.54090434E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.54090434E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49195921E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07929958E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07929958E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07929950E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.10123832E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.10123832E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.10123832E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.10123832E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.36708034E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.36708034E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.36708034E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.36708034E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.54477124E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.54477124E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.25551489E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.15681129E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.13548194E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     8.20556517E-04    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.03650303E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     8.20556517E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.03650303E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.00226563E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.16737832E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.16737832E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.20898835E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.86458584E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.86458584E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.86453102E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.20666966E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.86306071E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.20666966E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.86306071E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.80694258E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.56914415E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.56914415E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.31514267E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.31325062E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.31325062E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.31325063E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.20696883E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.20696883E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.20696883E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.20696883E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.02317345E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.02317345E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.02317345E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.02317345E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.91087429E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.91087429E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.44814916E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01657431E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.31138657E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.39021524E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.49103552E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.49103552E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.38598917E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.37482525E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.83185736E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.64246448E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.64246448E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.64288957E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.64288957E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01304480E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88223073E-01    2           5        -5   # BR(h -> b       bb     )
     6.49750590E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30010749E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87703002E-04    2           3        -3   # BR(h -> s       sb     )
     2.11846675E-02    2           4        -4   # BR(h -> c       cb     )
     6.92474282E-02    2          21        21   # BR(h -> g       g      )
     2.40022551E-03    2          22        22   # BR(h -> gam     gam    )
     1.65117163E-03    2          22        23   # BR(h -> Z       gam    )
     2.23313811E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82868503E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56605832E+01   # H decays
#          BR         NDA      ID1       ID2
     3.69925999E-01    2           5        -5   # BR(H -> b       bb     )
     5.95682153E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10618781E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49125705E-04    2           3        -3   # BR(H -> s       sb     )
     6.98645510E-08    2           4        -4   # BR(H -> c       cb     )
     7.00188261E-03    2           6        -6   # BR(H -> t       tb     )
     5.69275284E-07    2          21        21   # BR(H -> g       g      )
     1.22368314E-08    2          22        22   # BR(H -> gam     gam    )
     1.80367635E-09    2          23        22   # BR(H -> Z       gam    )
     1.57313277E-06    2          24       -24   # BR(H -> W+      W-     )
     7.86021497E-07    2          23        23   # BR(H -> Z       Z      )
     6.49584437E-06    2          25        25   # BR(H -> h       h      )
    -6.17776968E-25    2          36        36   # BR(H -> A       A      )
     7.67783609E-20    2          23        36   # BR(H -> Z       A      )
     1.87045922E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.49306593E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.49306593E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.51987864E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.42586747E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.67709931E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.97115236E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.00205928E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.25483601E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.72127646E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.27665174E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.45391813E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.60303599E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.85440898E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.47357392E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.47357392E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.31981116E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56590950E+01   # A decays
#          BR         NDA      ID1       ID2
     3.69961743E-01    2           5        -5   # BR(A -> b       bb     )
     5.95700978E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10625272E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49174010E-04    2           3        -3   # BR(A -> s       sb     )
     7.03283694E-08    2           4        -4   # BR(A -> c       cb     )
     7.01651691E-03    2           6        -6   # BR(A -> t       tb     )
     1.44171631E-05    2          21        21   # BR(A -> g       g      )
     7.16482876E-08    2          22        22   # BR(A -> gam     gam    )
     1.58514548E-08    2          23        22   # BR(A -> Z       gam    )
     1.56986206E-06    2          23        25   # BR(A -> Z       h      )
     1.94685220E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.49260455E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.49260455E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.25580730E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.57411730E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.49466802E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.27979205E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.70584251E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.01670507E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.86178240E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.63191409E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.32502834E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.64114839E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.64114839E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.59698985E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.97254448E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.92549089E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09510841E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.82242526E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18691723E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44187042E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.79878991E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.56292548E-06    2          24        25   # BR(H+ -> W+      h      )
     3.05558918E-14    2          24        36   # BR(H+ -> W+      A      )
     4.03806661E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.19535676E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.96175643E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.48680600E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.25036441E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.48214860E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.75869132E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.33604145E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.13580691E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
