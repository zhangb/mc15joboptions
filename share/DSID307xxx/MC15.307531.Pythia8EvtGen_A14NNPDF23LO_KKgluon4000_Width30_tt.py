# kkg mass in GeV
m_kkg = 4000.0

evgenConfig.description = "Kaluza-Klein gluon ("+str(m_kkg)+") to ttbar"
evgenConfig.process = "pp>kkgluon>ttbar"
evgenConfig.keywords = ["BSM" ,"resonance", "ttbar"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Saverio D'Auria <saverio.dauria@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


# turn on the Z' process
genSeq.Pythia8.Commands += ["ExtraDimensionsG*:qqbar2KKgluon* = on"]
# set mass and disable all decay modes except KKgluon -> ttbar
genSeq.Pythia8.Commands += ["5100021:m0 ="+str(m_kkg) ]
genSeq.Pythia8.Commands += ["5100021:onMode = off"]
genSeq.Pythia8.Commands += ["5100021:onIfAny = 6 -6"]

# only KK gluon and no SM or interference
genSeq.Pythia8.Commands += ["ExtraDimensionsG*:KKintMode = 2"]
# now changing the Right couplings to top to obtain \Gamma=30%
genSeq.Pythia8.Commands += ["ExtraDimensionsG*:KKgtR = 6.2391"]
