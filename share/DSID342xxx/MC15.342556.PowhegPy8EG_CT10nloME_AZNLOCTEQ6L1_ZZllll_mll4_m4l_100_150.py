#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = 'ZZllll'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
#PowhegConfig.generate()
PowhegConfig.nEvents *= 40.
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.UserModes += ['Main31:NFinal = 2']

include("MC15JobOptions/FourLeptonInvMassFilter.py")
filtSeq.FourLeptonInvMassFilter.MinPt = 3.*GeV
filtSeq.FourLeptonInvMassFilter.MaxEta = 5.
filtSeq.FourLeptonInvMassFilter.MinMass = 100.*GeV
filtSeq.FourLeptonInvMassFilter.MaxMass = 150.*GeV



#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ->llll production with AZNLO CTEQ6L1 tune and mllmin4 and 100<m4l<150 GeV'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '4lepton' ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch', 'jochen.meyer@cern.ch' ]
evgenConfig.minevents   = 1000
