include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa+OpenLoops gg->llll + 0,1j, cf. arXiv:1309.0500, including the gg->h diagrams (+interference)."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_CT10_ggllll"

evgenConfig.process="""
(run){
  NJET:=1; QCUT:=20;
  FSCF:=4.00; RSCF:=1.0; QSCF:=1.0;

  LOOPGEN:=OpenLoopsReplace # cf. Sherpa ticket #317
  ME_SIGNAL_GENERATOR=Amegic LOOPGEN
  INTEGRATION_ERROR=0.02
  EXCLUSIVE_CLUSTER_MODE 1;
  OL_RUNNING_FLAVOURS=5
  AMEGIC_ALLOW_MAPPING=0
  SHERPA_LDADD=Proc_fsrchannels4 Proc_fsrchannels5 SherpaOpenLoopsReplace
  
  SCALES STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};

  ## for mu_F=mu_R=mu_Q = mVV/2
  CORE_SCALE VAR{Abs2(p[2]+p[3]+p[4]+p[5])/4.0}

  1/ALPHAQED(default)=132.348904521624493
  MASS[25]=125.5
  WIDTH[25]=0.00414
}(run)


(processes){
  Process 93 93 -> 90 90 90 90 93{1}
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  Order_QCD 2 {4}
  Order_QCD 3 {5}
  Integrator fsrchannels4 {4}
  Integrator fsrchannels5 {5}
  Enable_MHV 10 # initialises external process
  Loop_Generator LOOPGEN;
  End process;
}(processes)

(selector){
  Mass 11 -11 10.0 E_CMS
  Mass 13 -13 10.0 E_CMS
  Mass 15 -15 10.0 E_CMS
}(selector)
"""
