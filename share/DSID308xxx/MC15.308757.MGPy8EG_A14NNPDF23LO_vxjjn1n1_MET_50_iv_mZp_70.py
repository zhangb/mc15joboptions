model="InelasticVectorEFT"
mDM1 = 35.
mDM2 = 140.
mZp = 70.
mHD = 125.
widthZp = 2.785143e-01
widthN2 = 1.261089e-03
filteff = 5.175983e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
