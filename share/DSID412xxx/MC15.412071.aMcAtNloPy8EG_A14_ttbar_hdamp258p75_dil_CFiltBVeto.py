evgenConfig.description = 'aMC@NLO+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, two leptons filter, C filter and B veto, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410440 LHE files without Shower Weights'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'tpelzer@cern.ch']
evgenConfig.minevents   = 1000
evgenConfig.inputFilesPerJob = 25
#evgenConfig.inputfilecheck="410440.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttbar_incl_LHE"

#--------------------------------------------------------------
# dilepton filter
# This is a filter on the input LHE files, applied before any other algorithm is processed
#--------------------------------------------------------------
include('MC15JobOptions/LHEFilter.py')
include('MC15JobOptions/LHEFilter_NLeptons.py')

nleptonFilter = LHEFilter_NLeptons()
nleptonFilter.NumLeptons = 2
nleptonFilter.Ptcut = 0.
lheFilters.addFilter(nleptonFilter)

lheFilters.run_filters()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# B filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBFilter.py')

#--------------------------------------------------------------
# C filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusCFilter.py')

# Combine the filters
filtSeq.Expression = "(TTbarPlusCFilter) and (not TTbarPlusBFilter)"
