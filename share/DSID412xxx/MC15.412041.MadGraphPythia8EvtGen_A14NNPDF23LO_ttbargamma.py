from AthenaCommon.AppMgr import ServiceMgr
ServiceMgr.MessageSvc.OutputLevel = DEBUG
from MadGraphControl.MadGraphUtils import *

mode=0
runName='ttbarphoton'
# Pythia8 Showering with A14_NNPDF23LO				
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
if (runArgs.runNumber == 412041):
	evgenConfig.description = "ttbar + photon; only events with photons from top quark"
	evgenConfig.keywords = ["ttbar","photon"]

evgenConfig.contact = ['Andreas Kirchhoff <andreas.kirchhoff@cern.ch>']
evgenConfig.inputfilecheck = runName
evgenConfig.minevents   = 5000

#---------------------------------------------------------------------------------------------------
# Generator Filters->still have to chose mine
#---------------------------------------------------------------------------------------------------
#include("MC15JobOptions/XtoVVDecayFilterExtended.py")
#filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 1560
#filtSeq.XtoVVDecayFilterExtended.PDGParent = 25
#filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
#filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [5]
#filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [5]
#filtSeq.Expression = "XtoVVDecayFilterExtended"
