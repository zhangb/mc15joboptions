dmmodel = 'Gaugephobic_SU2L'
dpionmass = 1100.0
fermioneta = 0.35
ndark = 4

include("MC15JobOptions/MadGraphControl_DarkPions_Pythia8EvtGen_A14_NNPDF23LO.py")
