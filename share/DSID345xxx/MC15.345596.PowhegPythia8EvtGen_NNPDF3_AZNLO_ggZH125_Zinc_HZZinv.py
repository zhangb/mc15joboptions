#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z-> inv + inc production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2'] 

#--------------------------------------------------------------
# H->ZZ->vvvv decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 12 14 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z production: H->inv (H->ZZ->vvvv) + Z->inc"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs","invisible" ]
evgenConfig.contact     = [ 'adrian.buzatu@cern.ch' ]
evgenConfig.inputfilecheck = 'TXT'
evgenConfig.process = "gg->ZH, H->inv, Z->all"
evgenConfig.minevents   = 2000
