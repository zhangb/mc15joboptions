include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Mass-Binned 1000 to 2000 GeV, Sherpa ttbar production with tt+2j@LO in the dileptonic channel."
evgenConfig.keywords = ["SM", "top", "ttbar", "2lepton"]
evgenConfig.contact  = [ "Antonio.Salvucci@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_221_NNPDF30NNLO_ttbar_etau_1000M2000"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{sqr(172.5)+0.5*(PPerp2(p[2]+p[3]+p[4])+PPerp2(p[5]+p[6]+p[7]))};
  EXCLUSIVE_CLUSTER_MODE 1;

  %tags for process setup
  NJET:=2; QCUT:=30.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  INTEGRATION_ERROR=0.05;
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  # l l'
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -11 12
  Decay -24[d] -> 15 -16
  CKKW sqr(QCUT/E_CMS)
  Order (*,4);
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process

  # l' l
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -15 16
  Decay -24[d] -> 11 -12
  CKKW sqr(QCUT/E_CMS)
  Order (*,4);
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process
}(processes)

(selector){
  Mass 90 90 1000.0 2000.0
  DecayMass 24 40.0 120.0
  DecayMass -24 40.0 120.0
  DecayMass 6 150.0 200.0
  DecayMass -6 150.0 200.0
}(selector)
"""
