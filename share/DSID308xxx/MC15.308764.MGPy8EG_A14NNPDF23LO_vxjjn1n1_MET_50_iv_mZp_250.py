model="InelasticVectorEFT"
mDM1 = 125.
mDM2 = 500.
mZp = 250.
mHD = 125.
widthZp = 9.947182e-01
widthN2 = 5.714060e-02
filteff = 9.451796e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
