#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11091412E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.55959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.06900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04185856E+01   # W+
        25     1.26011518E+02   # h
        35     4.00000740E+03   # H
        36     3.99999644E+03   # A
        37     4.00101002E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02016579E+03   # ~d_L
   2000001     4.01796903E+03   # ~d_R
   1000002     4.01951288E+03   # ~u_L
   2000002     4.01911090E+03   # ~u_R
   1000003     4.02016579E+03   # ~s_L
   2000003     4.01796903E+03   # ~s_R
   1000004     4.01951288E+03   # ~c_L
   2000004     4.01911090E+03   # ~c_R
   1000005     6.66945260E+02   # ~b_1
   2000005     4.02225373E+03   # ~b_2
   1000006     6.48895679E+02   # ~t_1
   2000006     2.07178303E+03   # ~t_2
   1000011     4.00351846E+03   # ~e_L
   2000011     4.00305321E+03   # ~e_R
   1000012     4.00240543E+03   # ~nu_eL
   1000013     4.00351846E+03   # ~mu_L
   2000013     4.00305321E+03   # ~mu_R
   1000014     4.00240543E+03   # ~nu_muL
   1000015     4.00464624E+03   # ~tau_1
   2000015     4.00774373E+03   # ~tau_2
   1000016     4.00434907E+03   # ~nu_tauL
   1000021     1.97020439E+03   # ~g
   1000022     2.51890438E+02   # ~chi_10
   1000023    -3.17207207E+02   # ~chi_20
   1000025     3.26943588E+02   # ~chi_30
   1000035     2.05467018E+03   # ~chi_40
   1000024     3.14592921E+02   # ~chi_1+
   1000037     2.05483466E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.91172958E-01   # N_11
  1  2    -1.16085176E-02   # N_12
  1  3    -3.56466008E-01   # N_13
  1  4    -2.80371159E-01   # N_14
  2  1    -5.74766343E-02   # N_21
  2  2     2.49699030E-02   # N_22
  2  3    -7.02270838E-01   # N_23
  2  4     7.09146395E-01   # N_24
  3  1     4.50006911E-01   # N_31
  3  2     2.83720968E-02   # N_32
  3  3     6.16223120E-01   # N_33
  3  4     6.45722751E-01   # N_34
  4  1    -9.88029277E-04   # N_41
  4  2     9.99218079E-01   # N_42
  4  3    -4.08914659E-03   # N_43
  4  4    -3.93132585E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.78838962E-03   # U_11
  1  2     9.99983247E-01   # U_12
  2  1    -9.99983247E-01   # U_21
  2  2     5.78838962E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.56093166E-02   # V_11
  1  2    -9.98452605E-01   # V_12
  2  1    -9.98452605E-01   # V_21
  2  2    -5.56093166E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95094255E-01   # cos(theta_t)
  1  2    -9.89314089E-02   # sin(theta_t)
  2  1     9.89314089E-02   # -sin(theta_t)
  2  2     9.95094255E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999535E-01   # cos(theta_b)
  1  2    -9.64364964E-04   # sin(theta_b)
  2  1     9.64364964E-04   # -sin(theta_b)
  2  2     9.99999535E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05547059E-01   # cos(theta_tau)
  1  2     7.08663071E-01   # sin(theta_tau)
  2  1    -7.08663071E-01   # -sin(theta_tau)
  2  2    -7.05547059E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00287564E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10914123E+03  # DRbar Higgs Parameters
         1    -3.06900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43985339E+02   # higgs               
         4     1.62067752E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10914123E+03  # The gauge couplings
     1     3.61419866E-01   # gprime(Q) DRbar
     2     6.36142652E-01   # g(Q) DRbar
     3     1.03385809E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10914123E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.58209934E-07   # A_c(Q) DRbar
  3  3     2.55959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10914123E+03  # The trilinear couplings
  1  1     3.51191596E-07   # A_d(Q) DRbar
  2  2     3.51224890E-07   # A_s(Q) DRbar
  3  3     6.30122204E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10914123E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.72237576E-08   # A_mu(Q) DRbar
  3  3     7.80060374E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10914123E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71727445E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10914123E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84838178E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10914123E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03412834E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10914123E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57861072E+07   # M^2_Hd              
        22    -1.04828228E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40390010E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.56362108E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44169238E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44169238E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55830762E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55830762E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.00172262E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     9.93743422E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.12632950E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.66192588E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.21800120E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.25756462E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.76127353E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14439734E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.77629216E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.25979470E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.43086368E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.62257998E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.14022546E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.37924580E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.54621278E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.99332789E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.64022479E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.23371035E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.71327370E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66813367E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51715075E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78831039E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64161922E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.05688388E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60659773E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.19529448E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13989446E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.03576551E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.37083249E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.30533386E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     7.79367701E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78525258E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.14016428E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.71140507E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.11233739E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78194076E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33647134E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55173713E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52859958E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61088779E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.37576909E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.81220788E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.11004178E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.97377416E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44960641E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78545028E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81934683E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.10908749E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.78622113E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78686929E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.78229169E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58373938E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53079253E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54340488E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.14143780E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.72720883E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.89558232E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.75417443E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85642760E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78525258E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.14016428E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.71140507E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.11233739E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78194076E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33647134E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55173713E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52859958E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61088779E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.37576909E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.81220788E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.11004178E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.97377416E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44960641E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78545028E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81934683E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.10908749E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.78622113E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78686929E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.78229169E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58373938E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53079253E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54340488E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.14143780E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.72720883E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.89558232E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.75417443E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85642760E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14046729E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.24596000E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99280376E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.08511769E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77692095E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.39642763E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56796836E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06178936E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.95077097E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.29187004E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01630499E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.34408036E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14046729E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.24596000E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99280376E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.08511769E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77692095E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.39642763E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56796836E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06178936E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.95077097E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.29187004E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01630499E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.34408036E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09388746E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.85749548E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35892341E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.55094989E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39889484E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.47709016E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80491333E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09185051E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.98116572E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25089595E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60463975E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42538762E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05706687E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85801062E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14154134E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.36480027E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68164346E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.61430421E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78033667E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13277862E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54528842E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14154134E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.36480027E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68164346E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.61430421E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78033667E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13277862E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54528842E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47045488E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23606036E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52302306E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.36771274E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51980086E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.66475953E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02566133E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.92088371E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33468494E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33468494E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11157352E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11157352E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10748307E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.18121914E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.78177927E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.65416729E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.51923471E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.62555867E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.36955370E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.47349162E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.41569858E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.52957954E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.55060070E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17586355E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52494923E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17586355E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52494923E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45155934E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49494950E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49494950E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47197719E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98688931E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98688931E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98688902E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.87723153E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.87723153E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.87723153E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.87723153E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.25744534E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.25744534E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.25744534E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.25744534E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.07344343E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.07344343E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.11379559E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.46334655E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.18262358E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.12130403E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.45119216E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.12130403E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.45119216E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.40408309E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.30181273E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.30181273E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.29245105E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.65971904E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.65971904E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.65971004E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.70245343E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.80182535E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.70245343E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.80182535E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.44177178E-07    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.10062946E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.10062946E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.95745381E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.19990699E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.19990699E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.19990702E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.10707324E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.10707324E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.10707324E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.10707324E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.36901302E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.36901302E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.36901302E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.36901302E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.23775294E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.23775294E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.18014246E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.93442630E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.94521808E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.17989350E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.35860961E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.35860961E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.82698901E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.54060636E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.37608926E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85585088E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85585088E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.86239072E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.86239072E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09121712E-03   # h decays
#          BR         NDA      ID1       ID2
     5.85612735E-01    2           5        -5   # BR(h -> b       bb     )
     6.39301474E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26310241E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79589750E-04    2           3        -3   # BR(h -> s       sb     )
     2.08275164E-02    2           4        -4   # BR(h -> c       cb     )
     6.81978179E-02    2          21        21   # BR(h -> g       g      )
     2.38619058E-03    2          22        22   # BR(h -> gam     gam    )
     1.67127369E-03    2          22        23   # BR(h -> Z       gam    )
     2.27673757E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89946624E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51479195E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55726530E-01    2           5        -5   # BR(H -> b       bb     )
     6.01219394E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12576616E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51441480E-04    2           3        -3   # BR(H -> s       sb     )
     7.05301221E-08    2           4        -4   # BR(H -> c       cb     )
     7.06858669E-03    2           6        -6   # BR(H -> t       tb     )
     7.71622949E-07    2          21        21   # BR(H -> g       g      )
     5.44056838E-10    2          22        22   # BR(H -> gam     gam    )
     1.81642941E-09    2          23        22   # BR(H -> Z       gam    )
     1.88082754E-06    2          24       -24   # BR(H -> W+      W-     )
     9.39762319E-07    2          23        23   # BR(H -> Z       Z      )
     7.25907667E-06    2          25        25   # BR(H -> h       h      )
     2.32173144E-24    2          36        36   # BR(H -> A       A      )
     4.45323382E-20    2          23        36   # BR(H -> Z       A      )
     1.84830699E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60712332E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60712332E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.96012292E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.15205107E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.11674008E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82164289E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.68303472E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.01856330E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.55908363E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.03804355E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.06276871E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.04638324E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.82340115E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.82340115E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40151545E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51447913E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55772428E-01    2           5        -5   # BR(A -> b       bb     )
     6.01256560E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12589589E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51497848E-04    2           3        -3   # BR(A -> s       sb     )
     7.09842634E-08    2           4        -4   # BR(A -> c       cb     )
     7.08195409E-03    2           6        -6   # BR(A -> t       tb     )
     1.45516254E-05    2          21        21   # BR(A -> g       g      )
     5.66038202E-08    2          22        22   # BR(A -> gam     gam    )
     1.60000141E-08    2          23        22   # BR(A -> Z       gam    )
     1.87693968E-06    2          23        25   # BR(A -> Z       h      )
     1.85629653E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60720566E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60720566E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.70159076E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.02378058E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.30209311E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.46132056E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.36880102E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.07941980E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.05521688E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.85892124E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.17894488E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.93692402E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.93692402E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52213970E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.70621966E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00574614E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12348470E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65197747E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20299448E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47494649E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63338676E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.87641738E-06    2          24        25   # BR(H+ -> W+      h      )
     2.54933256E-14    2          24        36   # BR(H+ -> W+      A      )
     6.58474507E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.83006457E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.08253244E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60706960E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.09840482E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58830691E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.22455754E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.59160095E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.74361981E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
