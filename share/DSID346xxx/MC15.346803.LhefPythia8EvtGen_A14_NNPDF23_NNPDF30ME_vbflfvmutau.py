evgenConfig.process     = "VBF LFV->mutau"
evgenConfig.generators += ["Lhef","Pythia8"]
evgenConfig.description = 'Grace_Pythia8'
evgenConfig.keywords+=['Higgs', "VBF"]
evgenConfig.inputfilecheck = 'vbflfv'
evgenConfig.contact     = [ 'Soshi.Tsuno@cern.ch' ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

