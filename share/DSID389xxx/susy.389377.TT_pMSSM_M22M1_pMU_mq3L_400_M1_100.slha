#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10963720E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03979832E+01   # W+
        25     1.25273000E+02   # h
        35     3.00022553E+03   # H
        36     2.99999973E+03   # A
        37     3.00098553E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01759718E+03   # ~d_L
   2000001     3.01222563E+03   # ~d_R
   1000002     3.01667168E+03   # ~u_L
   2000002     3.01456730E+03   # ~u_R
   1000003     3.01759718E+03   # ~s_L
   2000003     3.01222563E+03   # ~s_R
   1000004     3.01667168E+03   # ~c_L
   2000004     3.01456730E+03   # ~c_R
   1000005     4.37354583E+02   # ~b_1
   2000005     3.01185265E+03   # ~b_2
   1000006     4.33071417E+02   # ~t_1
   2000006     2.99316968E+03   # ~t_2
   1000011     3.00693336E+03   # ~e_L
   2000011     3.00079660E+03   # ~e_R
   1000012     3.00553332E+03   # ~nu_eL
   1000013     3.00693336E+03   # ~mu_L
   2000013     3.00079660E+03   # ~mu_R
   1000014     3.00553332E+03   # ~nu_muL
   1000015     2.98606601E+03   # ~tau_1
   2000015     3.02229017E+03   # ~tau_2
   1000016     3.00576501E+03   # ~nu_tauL
   1000021     2.32108251E+03   # ~g
   1000022     1.00870492E+02   # ~chi_10
   1000023     2.14596336E+02   # ~chi_20
   1000025    -3.00457687E+03   # ~chi_30
   1000035     3.00503664E+03   # ~chi_40
   1000024     2.14755313E+02   # ~chi_1+
   1000037     3.00572737E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888474E-01   # N_11
  1  2    -1.54023003E-03   # N_12
  1  3     1.48037893E-02   # N_13
  1  4    -1.23067870E-03   # N_14
  2  1     1.93207287E-03   # N_21
  2  2     9.99649221E-01   # N_22
  2  3    -2.62377194E-02   # N_23
  2  4     3.04707520E-03   # N_24
  3  1    -9.56929569E-03   # N_31
  3  2     1.64151753E-02   # N_32
  3  3     7.06829806E-01   # N_33
  3  4     7.07128415E-01   # N_34
  4  1    -1.13019790E-02   # N_41
  4  2     2.07269197E-02   # N_42
  4  3     7.06741859E-01   # N_43
  4  4    -7.07077510E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99310304E-01   # U_11
  1  2    -3.71337732E-02   # U_12
  2  1     3.71337732E-02   # U_21
  2  2     9.99310304E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990696E-01   # V_11
  1  2    -4.31371705E-03   # V_12
  2  1     4.31371705E-03   # V_21
  2  2     9.99990696E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98905125E-01   # cos(theta_t)
  1  2    -4.67819543E-02   # sin(theta_t)
  2  1     4.67819543E-02   # -sin(theta_t)
  2  2     9.98905125E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99923444E-01   # cos(theta_b)
  1  2     1.23736066E-02   # sin(theta_b)
  2  1    -1.23736066E-02   # -sin(theta_b)
  2  2     9.99923444E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06887690E-01   # cos(theta_tau)
  1  2     7.07325805E-01   # sin(theta_tau)
  2  1    -7.07325805E-01   # -sin(theta_tau)
  2  2     7.06887690E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01962729E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.09637200E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44948740E+02   # higgs               
         4     6.22668851E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.09637200E+03  # The gauge couplings
     1     3.61121798E-01   # gprime(Q) DRbar
     2     6.39444653E-01   # g(Q) DRbar
     3     1.03334553E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.09637200E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.07077210E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.09637200E+03  # The trilinear couplings
  1  1     2.62859262E-07   # A_d(Q) DRbar
  2  2     2.62913695E-07   # A_s(Q) DRbar
  3  3     6.15758237E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.09637200E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     5.77590139E-08   # A_mu(Q) DRbar
  3  3     5.84421248E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.09637200E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58788409E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.09637200E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.10755857E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.09637200E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04960994E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.09637200E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.27922764E+04   # M^2_Hd              
        22    -9.16838236E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41942284E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.56702940E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48249904E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48249904E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51750096E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51750096E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.67836622E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.42646851E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.90298764E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.95436551E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.06829046E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.08915002E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.71193764E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.44158616E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.14877716E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66140695E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52851319E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.04423417E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.23410013E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.74708961E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.88541265E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.83987839E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.26692644E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.85877383E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.90503805E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.04113411E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.37012110E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.00200884E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28591604E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.91151482E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.24438762E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.12603321E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.08246761E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.72911480E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63247155E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.74795001E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.12710575E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.26475281E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.94027511E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.04548442E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21600040E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56428181E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.79998155E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.73436710E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.84102331E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43571237E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08757965E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.91734986E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62944563E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.92532607E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.22758641E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25900795E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.41744991E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.05237246E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.70240017E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44962586E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.64979557E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.30819533E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.61676400E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55503576E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.08246761E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.72911480E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63247155E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.74795001E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.12710575E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.26475281E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.94027511E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.04548442E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21600040E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56428181E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.79998155E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.73436710E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.84102331E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43571237E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08757965E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.91734986E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62944563E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.92532607E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.22758641E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25900795E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.41744991E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.05237246E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.70240017E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44962586E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.64979557E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.30819533E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.61676400E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55503576E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01714322E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.63311518E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01800607E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.06083007E-11    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.96346818E-11    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01868240E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.40015607E-10    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55319804E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996296E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.70399855E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01714322E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.63311518E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01800607E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.06083007E-11    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.96346818E-11    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01868240E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.40015607E-10    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55319804E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996296E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.70399855E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81427702E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.44654939E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18872867E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36472194E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75433742E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.52775802E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16184757E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.99189640E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.07452955E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.31019565E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.80919627E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01749328E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.73338962E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00316866E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.86624182E-11    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.44240769E-11    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02349238E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.01749328E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.73338962E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00316866E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.86624182E-11    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.44240769E-11    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02349238E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.01814511E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.73256424E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00291769E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.04968048E-11    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.25087286E-11    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02382588E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.18011420E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.11024121E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.60919377E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.95883788E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.35063900E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.16283468E-14    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.16283468E-14    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     9.15560606E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.60665544E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03700470E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.46226599E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.77143972E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.67282098E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.14214309E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.22504280E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.03691949E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.42599320E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.42599320E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.96421826E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.56331861E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.69979763E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.69979763E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.21509223E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.21509223E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.93770580E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.93770580E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.93770580E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.93770580E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.68994495E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.68994495E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.04874220E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.87254520E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.55975298E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.49392562E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.49392562E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.26623923E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.21510886E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.68016155E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.68016155E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.24017136E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.24017136E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     8.75176123E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.75176123E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.75176123E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.75176123E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.09687063E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.09687063E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.99292750E-03   # h decays
#          BR         NDA      ID1       ID2
     6.76152680E-01    2           5        -5   # BR(h -> b       bb     )
     5.24258130E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.85587955E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.93708166E-04    2           3        -3   # BR(h -> s       sb     )
     1.69852724E-02    2           4        -4   # BR(h -> c       cb     )
     5.62365815E-02    2          21        21   # BR(h -> g       g      )
     1.89672467E-03    2          22        22   # BR(h -> gam     gam    )
     1.28226819E-03    2          22        23   # BR(h -> Z       gam    )
     1.72752537E-01    2          24       -24   # BR(h -> W+      W-     )
     2.16888269E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38795015E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39683325E-01    2           5        -5   # BR(H -> b       bb     )
     1.79173450E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.33514503E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.77952718E-04    2           3        -3   # BR(H -> s       sb     )
     2.19647543E-07    2           4        -4   # BR(H -> c       cb     )
     2.19112721E-02    2           6        -6   # BR(H -> t       tb     )
     3.50078154E-05    2          21        21   # BR(H -> g       g      )
     4.90949159E-08    2          22        22   # BR(H -> gam     gam    )
     8.38085266E-09    2          23        22   # BR(H -> Z       gam    )
     3.59758553E-05    2          24       -24   # BR(H -> W+      W-     )
     1.79657449E-05    2          23        23   # BR(H -> Z       Z      )
     8.98548589E-05    2          25        25   # BR(H -> h       h      )
    -2.22757947E-23    2          36        36   # BR(H -> A       A      )
     4.54921497E-17    2          23        36   # BR(H -> Z       A      )
     2.41704275E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13724343E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20343041E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.41479267E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.31593537E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.37376027E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31434274E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81165119E-01    2           5        -5   # BR(A -> b       bb     )
     1.89198274E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.68958951E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.21620509E-04    2           3        -3   # BR(A -> s       sb     )
     2.31849662E-07    2           4        -4   # BR(A -> c       cb     )
     2.31157626E-02    2           6        -6   # BR(A -> t       tb     )
     6.80737147E-05    2          21        21   # BR(A -> g       g      )
     3.37059108E-08    2          22        22   # BR(A -> gam     gam    )
     6.70225763E-08    2          23        22   # BR(A -> Z       gam    )
     3.78412136E-05    2          23        25   # BR(A -> Z       h      )
     2.66702630E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22659855E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32786664E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.06464214E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.71795804E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.07480339E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.55972594E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     9.05056657E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.87873070E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.31881078E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.09425041E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.01471764E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.12623446E-05    2          24        25   # BR(H+ -> W+      h      )
     1.26052785E-13    2          24        36   # BR(H+ -> W+      A      )
     2.14149799E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.98942429E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.72286972E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
