## Pythia8 W->mu+HNL

evgenConfig.description = "W->mu+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W", "exotic", "BSM"]
evgenConfig.contact = ["Fabian Thiele, fabian.thiele@cern.ch"]
evgenConfig.process = "W -> HNL mu-+"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from EvgenProdTools.EvgenProdToolsConf import TestHepMC
genSeq += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm

genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 20.0 0.0 0.0 0.0 0.1 0 1 0 1 0",
                            "50:isResonance = false",
                            "50:addChannel = 1 1.0 23 13 -11 12",#decay in mu e ve
                            "50:mayDecay = on",
                            "WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
             "24:addchannel = 1 1.0 103 -13 50",
             "ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
             "ParticleDecays:tau0Max = 600.0"]

# import generator filters for electron and muons
from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter

# define electron filter
filtSeq += MultiElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 5000.
filtSeq.ElectronFilter.Etacut = 2.8
filtSeq.ElectronFilter.NElectrons = 1

# separate filters for the leading and subleading muon
filtSeq += MultiMuonFilter("LeadingMuon")
filtSeq.LeadingMuon.Ptcut = 15000.
filtSeq.LeadingMuon.Etacut = 2.8
filtSeq.LeadingMuon.NMuons = 1

filtSeq += MultiMuonFilter("SubleadingMuon")
filtSeq.SubleadingMuon.Ptcut = 5000.
filtSeq.SubleadingMuon.Etacut = 2.8
filtSeq.SubleadingMuon.NMuons = 2
