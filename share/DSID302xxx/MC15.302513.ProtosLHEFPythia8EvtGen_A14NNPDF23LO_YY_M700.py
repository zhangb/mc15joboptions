evgenConfig.description = "Protos YY pair production (X[-4/3] in doublet, M = 700 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["Mark Cooke <mark.stephen.cooke@cern.ch>"]
evgenConfig.inputfilecheck = "protoslhef"
evgenConfig.process = "pp>YY"

include("MC15JobOptions/ProtosLHEF_Common.py") 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
