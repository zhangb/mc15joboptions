#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413799E+01   # W+
        25     1.26000000E+02   # h
        35     2.00401299E+03   # H
        36     2.00000000E+03   # A
        37     2.00151023E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98044639E+03   # ~d_L
   2000001     4.98033677E+03   # ~d_R
   1000002     4.98020130E+03   # ~u_L
   2000002     4.98025920E+03   # ~u_R
   1000003     4.98044639E+03   # ~s_L
   2000003     4.98033677E+03   # ~s_R
   1000004     4.98020130E+03   # ~c_L
   2000004     4.98025920E+03   # ~c_R
   1000005     4.97979398E+03   # ~b_1
   2000005     4.98099059E+03   # ~b_2
   1000006     5.09821108E+03   # ~t_1
   2000006     5.39138200E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.70498828E+03   # ~g
   1000022     1.49247308E+03   # ~chi_10
   1000023    -1.54193365E+03   # ~chi_20
   1000025     1.57969545E+03   # ~chi_30
   1000035     3.12742724E+03   # ~chi_40
   1000024     1.53811726E+03   # ~chi_1+
   1000037     3.12742448E+03   # ~chi_2+
   1000039     2.62625000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.22989713E-01   # N_11
  1  2    -3.27814981E-02   # N_12
  1  3     4.89546413E-01   # N_13
  1  4    -4.86369774E-01   # N_14
  2  1     2.83790089E-03   # N_21
  2  2    -3.32576570E-03   # N_22
  2  3    -7.07012505E-01   # N_23
  2  4    -7.07187530E-01   # N_24
  3  1     6.90851635E-01   # N_31
  3  2     3.62810336E-02   # N_32
  3  3    -5.09352338E-01   # N_33
  3  4     5.11828000E-01   # N_34
  4  1     1.35631714E-03   # N_41
  4  2    -9.98798278E-01   # N_42
  4  3    -3.22152499E-02   # N_43
  4  4     3.69098741E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.55235596E-02   # U_11
  1  2     9.98963265E-01   # U_12
  2  1     9.98963265E-01   # U_21
  2  2     4.55235596E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.21631055E-02   # V_11
  1  2     9.98638578E-01   # V_12
  2  1     9.98638578E-01   # V_21
  2  2     5.21631055E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99946553E-01   # cos(theta_t)
  1  2     1.03388173E-02   # sin(theta_t)
  2  1    -1.03388173E-02   # -sin(theta_t)
  2  2     9.99946553E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73941986E-01   # cos(theta_b)
  1  2     7.38784271E-01   # sin(theta_b)
  2  1    -7.38784271E-01   # -sin(theta_b)
  2  2     6.73941986E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04328407E-01   # cos(theta_tau)
  1  2     7.09874281E-01   # sin(theta_tau)
  2  1    -7.09874281E-01   # -sin(theta_tau)
  2  2     7.04328407E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203265E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52208661E+02   # vev(Q)              
         4     3.33220311E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52758833E-01   # gprime(Q) DRbar
     2     6.27023099E-01   # g(Q) DRbar
     3     1.08402975E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02520795E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71683616E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781585E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.91038432E+05   # M^2_Hd              
        22    -7.40530868E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36980792E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.77810702E-07   # gluino decays
#          BR         NDA      ID1       ID2
     3.90971585E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.93009445E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     9.62056715E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     8.65709720E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.22934127E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.81491741E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     7.17145841E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     3.67703542E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.39807686E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.79980420E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.22934127E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.81491741E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     7.17145841E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     3.67703542E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.39807686E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.79980420E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.21802830E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.26535739E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.54624793E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.81017039E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.81017039E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.81017039E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.81017039E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.95678880E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.89346887E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.02035936E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.48258587E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.56602942E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     4.34847718E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.08454233E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.06619135E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.16785219E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.86732782E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.64659351E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.68467281E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.98521717E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.92684227E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.17715799E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.79986402E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.04594123E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.13802945E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.00721659E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64771629E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.59185574E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.46414856E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.38727306E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.29867591E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.54565812E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.11834177E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.24637785E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.03573058E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.51621091E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.52308580E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.50228343E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.32447448E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.42605145E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.67725359E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.57503557E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.34520553E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.18034924E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.12829610E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.24814514E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.32675897E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.33887174E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.86176224E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.07283054E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.72684373E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.14684024E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.04835636E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.12095421E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.16144194E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.28617643E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.92706675E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.38145870E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59114551E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.24820661E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.96531455E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.01827269E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.88994124E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.07642031E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.83860741E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.15175396E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.04879070E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.05596608E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.57453177E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.47531589E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.97006345E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.72134912E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89455311E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.24814514E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.32675897E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.33887174E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.86176224E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.07283054E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.72684373E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.14684024E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.04835636E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.12095421E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.16144194E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.28617643E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.92706675E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.38145870E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59114551E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.24820661E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.96531455E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.01827269E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.88994124E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.07642031E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.83860741E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.15175396E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.04879070E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.05596608E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.57453177E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.47531589E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.97006345E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.72134912E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89455311E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.68799778E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.44166801E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78114378E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06452069E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68552450E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.47155895E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.38105460E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03123658E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.28617075E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.03763287E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.71374056E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.30950399E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.68799778E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.44166801E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78114378E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06452069E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68552450E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.47155895E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.38105460E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03123658E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.28617075E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.03763287E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.71374056E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.30950399E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36454731E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.82711552E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.65540074E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.59520698E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51005446E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.72160054E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.02575161E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.20537675E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.36258455E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.68114047E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.57012305E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67218413E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54413351E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.00072250E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.09397105E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.68813270E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16597093E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44314744E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.31927857E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69314599E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.24469595E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37636395E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.68813270E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16597093E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44314744E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.31927857E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69314599E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.24469595E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37636395E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.69076441E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.16483054E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.44173596E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.31211993E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69051195E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.21799877E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.37112135E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.24508730E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.72295545E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33609783E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33609783E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11203387E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11203387E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10373088E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.87874772E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53246062E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.10795885E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46241897E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.36074483E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53641673E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.28278084E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.77731050E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.33590150E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99213250E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89638030E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11487193E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.51110471E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.11829139E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     6.83660212E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.82979470E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.51064937E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18538185E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53490760E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18538185E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53490760E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40465994E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50555237E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50555237E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47166612E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00083744E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00083744E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00083744E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.97397179E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.97397179E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.97397179E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.97397179E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.57990635E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.57990635E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.57990635E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.57990635E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.29020709E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.29020709E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.04733521E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.09886225E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.66783058E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.08196877E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.73222545E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.85687303E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09848602E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29953393E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.09848602E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29953393E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.98353476E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.23198669E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.23198669E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.80300201E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.38270213E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.38270213E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.38270213E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.30915323E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.99004172E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.30915323E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.99004172E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.57766229E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.82892207E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.82892207E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.73019899E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.36377697E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.36377697E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.36377697E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.34537030E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.34537030E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.34537030E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.34537030E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.48456132E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.48456132E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.48456132E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.48456132E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.44410993E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.44410993E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.87875697E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.37063383E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51847809E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.24757929E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46471302E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46471302E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13483589E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.14010584E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.39424070E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.07311292E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.75325299E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.03762259E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.73857494E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.14845933E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21991171E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02034288E-01    2           5        -5   # BR(h -> b       bb     )
     6.22160981E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20217863E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65938394E-04    2           3        -3   # BR(h -> s       sb     )
     2.01014073E-02    2           4        -4   # BR(h -> c       cb     )
     6.63839939E-02    2          21        21   # BR(h -> g       g      )
     2.30607254E-03    2          22        22   # BR(h -> gam     gam    )
     1.62605485E-03    2          22        23   # BR(h -> Z       gam    )
     2.16575312E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80706170E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78102916E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46954582E-03    2           5        -5   # BR(H -> b       bb     )
     2.46426448E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71209240E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546588E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668206E-05    2           4        -4   # BR(H -> c       cb     )
     9.96065415E-01    2           6        -6   # BR(H -> t       tb     )
     7.97762259E-04    2          21        21   # BR(H -> g       g      )
     2.71480264E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010989E-06    2          23        22   # BR(H -> Z       gam    )
     3.35005439E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67045863E-04    2          23        23   # BR(H -> Z       Z      )
     9.02870606E-04    2          25        25   # BR(H -> h       h      )
     7.36686856E-24    2          36        36   # BR(H -> A       A      )
     2.97010657E-11    2          23        36   # BR(H -> Z       A      )
     6.82808529E-12    2          24       -37   # BR(H -> W+      H-     )
     6.82808529E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381872E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47246508E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897974E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62267445E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677514E-06    2           3        -3   # BR(A -> s       sb     )
     9.96177519E-06    2           4        -4   # BR(A -> c       cb     )
     9.96997029E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676629E-04    2          21        21   # BR(A -> g       g      )
     3.15260681E-06    2          22        22   # BR(A -> gam     gam    )
     1.35269787E-06    2          23        22   # BR(A -> Z       gam    )
     3.26465031E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472278E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36087105E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237643E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145100E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50096885E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45694389E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731676E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401988E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34589727E-04    2          24        25   # BR(H+ -> W+      h      )
     2.75899126E-13    2          24        36   # BR(H+ -> W+      A      )
