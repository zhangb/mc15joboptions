model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 550.
mHD = 125.
widthZp = 2.596678e+00
widthhd = 9.598012e-01
filteff = 8.564577e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
