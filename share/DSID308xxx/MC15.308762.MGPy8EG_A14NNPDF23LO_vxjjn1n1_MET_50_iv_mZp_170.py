model="InelasticVectorEFT"
mDM1 = 85.
mDM2 = 340.
mZp = 170.
mHD = 125.
widthZp = 6.764080e-01
widthN2 = 1.803212e-02
filteff = 8.833922e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
