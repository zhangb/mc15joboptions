model="LightVector"
mDM1 = 5.
mDM2 = 90.
mZp = 60.
mHD = 125.
widthZp = 2.387215e-01
widthN2 = 1.977182e+00
filteff = 2.402691e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
