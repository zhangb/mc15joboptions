#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15500842E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03956862E+01   # W+
        25     1.24053582E+02   # h
        35     3.00011345E+03   # H
        36     2.99999986E+03   # A
        37     3.00090942E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03730977E+03   # ~d_L
   2000001     3.03211231E+03   # ~d_R
   1000002     3.03639074E+03   # ~u_L
   2000002     3.03335016E+03   # ~u_R
   1000003     3.03730977E+03   # ~s_L
   2000003     3.03211231E+03   # ~s_R
   1000004     3.03639074E+03   # ~c_L
   2000004     3.03335016E+03   # ~c_R
   1000005     8.94996584E+02   # ~b_1
   2000005     3.03124828E+03   # ~b_2
   1000006     8.93398533E+02   # ~t_1
   2000006     3.01798992E+03   # ~t_2
   1000011     3.00643185E+03   # ~e_L
   2000011     3.00192185E+03   # ~e_R
   1000012     3.00503049E+03   # ~nu_eL
   1000013     3.00643185E+03   # ~mu_L
   2000013     3.00192185E+03   # ~mu_R
   1000014     3.00503049E+03   # ~nu_muL
   1000015     2.98581806E+03   # ~tau_1
   2000015     3.02194391E+03   # ~tau_2
   1000016     3.00486008E+03   # ~nu_tauL
   1000021     2.34598019E+03   # ~g
   1000022     1.00254419E+02   # ~chi_10
   1000023     2.15320631E+02   # ~chi_20
   1000025    -2.99681335E+03   # ~chi_30
   1000035     2.99729722E+03   # ~chi_40
   1000024     2.15480509E+02   # ~chi_1+
   1000037     2.99798524E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888451E-01   # N_11
  1  2    -1.53690931E-03   # N_12
  1  3     1.48056694E-02   # N_13
  1  4    -1.23083489E-03   # N_14
  2  1     1.92791051E-03   # N_21
  2  2     9.99650819E-01   # N_22
  2  3    -2.61778707E-02   # N_23
  2  4     3.04013821E-03   # N_24
  3  1    -9.57064041E-03   # N_31
  3  2     1.63777258E-02   # N_32
  3  3     7.06830733E-01   # N_33
  3  4     7.07128339E-01   # N_34
  4  1    -1.13035788E-02   # N_41
  4  2     2.06796567E-02   # N_42
  4  3     7.06743112E-01   # N_43
  4  4    -7.07077616E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99313445E-01   # U_11
  1  2    -3.70491381E-02   # U_12
  2  1     3.70491381E-02   # U_21
  2  2     9.99313445E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990738E-01   # V_11
  1  2    -4.30389859E-03   # V_12
  2  1     4.30389859E-03   # V_21
  2  2     9.99990738E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98805877E-01   # cos(theta_t)
  1  2    -4.88550926E-02   # sin(theta_t)
  2  1     4.88550926E-02   # -sin(theta_t)
  2  2     9.98805877E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99913274E-01   # cos(theta_b)
  1  2     1.31698321E-02   # sin(theta_b)
  2  1    -1.31698321E-02   # -sin(theta_b)
  2  2     9.99913274E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06911824E-01   # cos(theta_tau)
  1  2     7.07301685E-01   # sin(theta_tau)
  2  1    -7.07301685E-01   # -sin(theta_tau)
  2  2     7.06911824E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01802745E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.55008422E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44200608E+02   # higgs               
         4     7.61358118E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.55008422E+03  # The gauge couplings
     1     3.62278648E-01   # gprime(Q) DRbar
     2     6.39939703E-01   # g(Q) DRbar
     3     1.02562342E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.55008422E+03  # The trilinear couplings
  1  1     2.26625680E-06   # A_u(Q) DRbar
  2  2     2.26629160E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.55008422E+03  # The trilinear couplings
  1  1     5.68892718E-07   # A_d(Q) DRbar
  2  2     5.69005564E-07   # A_s(Q) DRbar
  3  3     1.31104137E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.55008422E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.26087032E-07   # A_mu(Q) DRbar
  3  3     1.27626151E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.55008422E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51015619E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.55008422E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12791869E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.55008422E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05220144E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.55008422E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.25129683E+04   # M^2_Hd              
        22    -9.04805108E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42172321E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.94259908E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48207962E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48207962E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51792038E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51792038E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.81611774E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.23543401E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.12561692E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.75083968E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01165741E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.52913120E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.02721578E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.07560046E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.95793937E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.32293469E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.56002559E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.49723814E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.95698986E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.61653763E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.38094888E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.45795261E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.40395251E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.20601980E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.96758275E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.99019346E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.74841521E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.72244475E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.54238276E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30105566E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.86179291E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16472220E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.99182937E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07204340E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.81657476E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64925664E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.73402778E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.32451736E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29831027E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.49972753E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.99426660E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17582428E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60045948E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.90855231E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.75294639E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.32567432E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39953438E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.07707822E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.00681624E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64620762E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.44705688E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.83790516E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.29254410E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.48762155E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.00117550E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66455941E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.55745194E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.68250314E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.59944856E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.53006465E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54425306E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07204340E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.81657476E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64925664E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.73402778E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.32451736E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29831027E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.49972753E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.99426660E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17582428E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60045948E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.90855231E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.75294639E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.32567432E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39953438E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.07707822E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.00681624E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64620762E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.44705688E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.83790516E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.29254410E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.48762155E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.00117550E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66455941E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.55745194E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.68250314E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.59944856E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.53006465E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54425306E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02435376E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.67644838E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01655401E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.49179327E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.28476593E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01580085E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.63039410E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56379705E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996310E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.68772100E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.06183065E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.21406640E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02435376E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.67644838E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01655401E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.49179327E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.28476593E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01580085E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.63039410E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56379705E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996310E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.68772100E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.06183065E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.21406640E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82302864E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45756588E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18504545E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35738867E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76299002E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53916133E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15796467E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.24511951E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.40270931E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30246723E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.41986867E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02468605E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.77672601E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00173218E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.30978748E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.89281968E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02059512E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.47164127E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02468605E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.77672601E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00173218E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.30978748E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.89281968E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02059512E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.47164127E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02479398E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.77590695E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00147795E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13324280E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.63646100E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02091856E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.26948953E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.59488370E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.25362036E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.65095469E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.13687246E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.90215677E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.22153306E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.23542111E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.05580284E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.46752467E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.75942278E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.28240394E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.34578573E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.41254371E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.01607468E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.01607468E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.93562467E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.82393901E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.59622013E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.59622013E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.23377722E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.23377722E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.13591200E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.13591200E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.19148139E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.75481294E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.80018032E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.09588507E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.09588507E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.40378706E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.65495521E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.57261681E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.57261681E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.26069801E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.26069801E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.68454111E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.68454111E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.70291496E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85545121E-01    2           5        -5   # BR(h -> b       bb     )
     5.50805322E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94990328E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.14447083E-04    2           3        -3   # BR(h -> s       sb     )
     1.78913426E-02    2           4        -4   # BR(h -> c       cb     )
     5.74144947E-02    2          21        21   # BR(h -> g       g      )
     1.93868014E-03    2          22        22   # BR(h -> gam     gam    )
     1.21936194E-03    2          22        23   # BR(h -> Z       gam    )
     1.60552905E-01    2          24       -24   # BR(h -> W+      W-     )
     1.97481250E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40209638E+01   # H decays
#          BR         NDA      ID1       ID2
     7.46647060E-01    2           5        -5   # BR(H -> b       bb     )
     1.77359367E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.27100339E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.70079983E-04    2           3        -3   # BR(H -> s       sb     )
     2.17285937E-07    2           4        -4   # BR(H -> c       cb     )
     2.16756687E-02    2           6        -6   # BR(H -> t       tb     )
     2.69169479E-05    2          21        21   # BR(H -> g       g      )
     8.91131467E-09    2          22        22   # BR(H -> gam     gam    )
     8.34319584E-09    2          23        22   # BR(H -> Z       gam    )
     3.09802129E-05    2          24       -24   # BR(H -> W+      W-     )
     1.54709988E-05    2          23        23   # BR(H -> Z       Z      )
     8.25564681E-05    2          25        25   # BR(H -> h       h      )
     3.51035752E-23    2          36        36   # BR(H -> A       A      )
     1.44680673E-18    2          23        36   # BR(H -> Z       A      )
     2.38103860E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12632083E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.18550904E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.32464510E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.83471654E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.75553065E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33456419E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84505768E-01    2           5        -5   # BR(A -> b       bb     )
     1.86331532E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.58822847E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.09171268E-04    2           3        -3   # BR(A -> s       sb     )
     2.28336662E-07    2           4        -4   # BR(A -> c       cb     )
     2.27655111E-02    2           6        -6   # BR(A -> t       tb     )
     6.70422540E-05    2          21        21   # BR(A -> g       g      )
     3.32476584E-08    2          22        22   # BR(A -> gam     gam    )
     6.59809154E-08    2          23        22   # BR(A -> Z       gam    )
     3.24268279E-05    2          23        25   # BR(A -> Z       h      )
     2.61432181E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.20857852E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30163090E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92587922E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.02386000E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10609972E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.42949997E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.59011932E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07902714E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.04823307E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03858387E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.19441673E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.23321076E-05    2          24        25   # BR(H+ -> W+      h      )
     8.00056220E-14    2          24        36   # BR(H+ -> W+      A      )
     2.02837993E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.53409487E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.24763535E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
