evgenConfig.description = "MG Z -> e e + 4j@LO with ptll>75 MJJ>800,DPHI<2.5 "
evgenConfig.keywords = ["SM", "Z", "jets", "LO" ]
evgenConfig.contact  = [ "schae@cern.ch"]

include("MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_VBF.py")
evgenConfig.maxeventsstrategy='IGNORE'
evgenConfig.minevents=100
evgenConfig.inputconfcheck="MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_TightVBF_Np4"
