#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.84191201E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.28900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04168820E+01   # W+
        25     1.24932229E+02   # h
        35     3.99999920E+03   # H
        36     3.99999475E+03   # A
        37     4.00101377E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01000070E+03   # ~d_L
   2000001     4.00976900E+03   # ~d_R
   1000002     4.00933400E+03   # ~u_L
   2000002     4.01177955E+03   # ~u_R
   1000003     4.01000070E+03   # ~s_L
   2000003     4.00976900E+03   # ~s_R
   1000004     4.00933400E+03   # ~c_L
   2000004     4.01177955E+03   # ~c_R
   1000005     3.34603043E+02   # ~b_1
   2000005     4.01630205E+03   # ~b_2
   1000006     3.23184366E+02   # ~t_1
   2000006     2.32377287E+03   # ~t_2
   1000011     4.00183869E+03   # ~e_L
   2000011     4.00221089E+03   # ~e_R
   1000012     4.00071278E+03   # ~nu_eL
   1000013     4.00183869E+03   # ~mu_L
   2000013     4.00221089E+03   # ~mu_R
   1000014     4.00071278E+03   # ~nu_muL
   1000015     4.00455011E+03   # ~tau_1
   2000015     4.00797136E+03   # ~tau_2
   1000016     4.00354110E+03   # ~nu_tauL
   1000021     1.95677742E+03   # ~g
   1000022     9.47834907E+01   # ~chi_10
   1000023    -1.38442414E+02   # ~chi_20
   1000025     1.53592007E+02   # ~chi_30
   1000035     2.05930285E+03   # ~chi_40
   1000024     1.33585040E+02   # ~chi_1+
   1000037     2.05946721E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.72471162E-01   # N_11
  1  2     1.34977932E-02   # N_12
  1  3     5.24825947E-01   # N_13
  1  4     3.57300768E-01   # N_14
  2  1    -1.34798161E-01   # N_21
  2  2     2.73061627E-02   # N_22
  2  3    -6.85713577E-01   # N_23
  2  4     7.14759204E-01   # N_24
  3  1     6.20577910E-01   # N_31
  3  2     2.41895576E-02   # N_32
  3  3     5.04335505E-01   # N_33
  3  4     5.99953016E-01   # N_34
  4  1    -9.04713196E-04   # N_41
  4  2     9.99243238E-01   # N_42
  4  3    -5.59862039E-04   # N_43
  4  4    -3.88821256E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.96131522E-04   # U_11
  1  2     9.99999683E-01   # U_12
  2  1    -9.99999683E-01   # U_21
  2  2     7.96131522E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50013007E-02   # V_11
  1  2    -9.98486283E-01   # V_12
  2  1    -9.98486283E-01   # V_21
  2  2    -5.50013007E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97869378E-01   # cos(theta_t)
  1  2    -6.52434246E-02   # sin(theta_t)
  2  1     6.52434246E-02   # -sin(theta_t)
  2  2     9.97869378E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999922E-01   # cos(theta_b)
  1  2    -3.94968345E-04   # sin(theta_b)
  2  1     3.94968345E-04   # -sin(theta_b)
  2  2     9.99999922E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.02655369E-01   # cos(theta_tau)
  1  2     7.11530345E-01   # sin(theta_tau)
  2  1    -7.11530345E-01   # -sin(theta_tau)
  2  2    -7.02655369E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00306835E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.41912015E+02  # DRbar Higgs Parameters
         1    -1.28900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44743226E+02   # higgs               
         4     1.62179179E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.41912015E+02  # The gauge couplings
     1     3.60656818E-01   # gprime(Q) DRbar
     2     6.36797984E-01   # g(Q) DRbar
     3     1.04014299E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.41912015E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     4.83559924E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.41912015E+02  # The trilinear couplings
  1  1     1.77186091E-07   # A_d(Q) DRbar
  2  2     1.77203375E-07   # A_s(Q) DRbar
  3  3     3.17124070E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.41912015E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     3.84344021E-08   # A_mu(Q) DRbar
  3  3     3.88159488E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.41912015E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74754248E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.41912015E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82168287E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.41912015E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03690086E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.41912015E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58435261E+07   # M^2_Hd              
        22    -1.15677682E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40686873E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.63572623E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46884768E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46884768E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53115232E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53115232E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.56339541E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.80856526E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.53147442E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.65996032E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.40314403E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.35219212E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.22732487E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.63143975E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.75540964E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.37970688E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.05657448E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.01991161E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.11521955E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.80821514E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.87863906E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.64286161E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.43492517E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.05580615E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.50908884E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.44979245E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66175932E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.76439983E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.67468654E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40661459E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.05789924E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.50005613E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.94685473E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.16468481E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.86617175E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.04170492E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.47929949E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
    -3.74930504E-09    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77829671E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.53135207E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.01567701E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.74428006E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76468108E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.28564106E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.51710276E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53377664E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60831578E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.29199981E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.00136742E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.12132035E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.46717782E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44865406E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77849631E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.21998269E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.43107974E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.54626670E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76933173E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.10750720E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.54876186E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53601236E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54008636E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.59020690E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.61298440E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.53540510E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.43335942E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85613083E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77829671E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.53135207E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.01567701E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.74428006E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76468108E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.28564106E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.51710276E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53377664E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60831578E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.29199981E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.00136742E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.12132035E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.46717782E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44865406E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77849631E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.21998269E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.43107974E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.54626670E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76933173E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.10750720E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.54876186E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53601236E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54008636E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.59020690E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.61298440E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.53540510E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.43335942E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85613083E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13599742E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.24334975E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.23485114E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.24263876E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77519135E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.51056426E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56385478E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06750093E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.97145184E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.81606201E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.84693752E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.43293477E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13599742E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.24334975E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.23485114E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.24263876E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77519135E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.51056426E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56385478E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06750093E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.97145184E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.81606201E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.84693752E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.43293477E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09558145E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33477624E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01133478E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.54635440E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38965180E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.42042340E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78604174E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10753950E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.17391324E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.32307328E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.29337959E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42389238E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.21801067E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85470639E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13708801E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04512662E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.51335135E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.49381786E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77801371E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10542578E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54129011E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13708801E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04512662E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.51335135E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.49381786E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77801371E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10542578E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54129011E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47168978E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.45066368E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.98551296E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.96785347E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51460160E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77832070E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01585948E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.32381727E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33716218E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33716218E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11239508E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11239508E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10088547E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.96892259E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87023487E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.78248458E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.98019215E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.35547746E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.91655665E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.39874219E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.82183711E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.17936952E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.81070315E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18427124E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53505640E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18427124E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53505640E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38514080E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51365625E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51365625E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47031950E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02323822E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02323822E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02323767E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.39645963E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.39645963E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.39645963E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.39645963E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.98821839E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.98821839E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.98821839E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.98821839E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.20863304E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.20863304E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.89654497E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.16476629E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.69884760E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.88175316E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.60489162E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.88175316E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.60489162E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.11845353E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.72570097E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.72570097E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.70804814E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.48674052E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.48674052E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.48673171E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.52857487E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.16680960E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.52857487E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.16680960E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.58265898E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.64090076E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.64090076E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.51575040E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.27860932E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.27860932E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.27860947E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.57796086E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.57796086E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.57796086E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.57796086E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.52594648E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.52594648E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.52594648E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.52594648E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.42940940E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.42940940E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.96791513E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.38172994E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.14176331E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.09874018E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.84191790E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.84191790E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.33198424E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.83465687E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.33846352E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90530305E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90530305E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.92125539E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.92125539E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.98267227E-03   # h decays
#          BR         NDA      ID1       ID2
     6.03901802E-01    2           5        -5   # BR(h -> b       bb     )
     6.51136949E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30504712E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89296517E-04    2           3        -3   # BR(h -> s       sb     )
     2.12475241E-02    2           4        -4   # BR(h -> c       cb     )
     7.09372206E-02    2          21        21   # BR(h -> g       g      )
     2.34278137E-03    2          22        22   # BR(h -> gam     gam    )
     1.56041959E-03    2          22        23   # BR(h -> Z       gam    )
     2.08118957E-01    2          24       -24   # BR(h -> W+      W-     )
     2.60577994E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44139706E+01   # H decays
#          BR         NDA      ID1       ID2
     3.40467239E-01    2           5        -5   # BR(H -> b       bb     )
     6.09327421E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15443418E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54832480E-04    2           3        -3   # BR(H -> s       sb     )
     7.14868242E-08    2           4        -4   # BR(H -> c       cb     )
     7.16446799E-03    2           6        -6   # BR(H -> t       tb     )
     1.02512860E-06    2          21        21   # BR(H -> g       g      )
     1.96784599E-10    2          22        22   # BR(H -> gam     gam    )
     1.83878607E-09    2          23        22   # BR(H -> Z       gam    )
     2.01203151E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00531876E-06    2          23        23   # BR(H -> Z       Z      )
     7.29385404E-06    2          25        25   # BR(H -> h       h      )
     4.43602705E-24    2          36        36   # BR(H -> A       A      )
     6.18808787E-20    2          23        36   # BR(H -> Z       A      )
     1.87312436E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66525584E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66525584E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.32308137E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.57751941E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.66660865E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.54806230E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.14998384E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.75570853E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.98672072E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.41452799E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.52758017E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.39590336E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.25344221E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.25344221E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.44805314E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44154998E+01   # A decays
#          BR         NDA      ID1       ID2
     3.40483701E-01    2           5        -5   # BR(A -> b       bb     )
     6.09314509E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15438683E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54868398E-04    2           3        -3   # BR(A -> s       sb     )
     7.19355875E-08    2           4        -4   # BR(A -> c       cb     )
     7.17686574E-03    2           6        -6   # BR(A -> t       tb     )
     1.47466527E-05    2          21        21   # BR(A -> g       g      )
     4.09504626E-08    2          22        22   # BR(A -> gam     gam    )
     1.62096424E-08    2          23        22   # BR(A -> Z       gam    )
     2.00781396E-06    2          23        25   # BR(A -> Z       h      )
     1.87604228E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66523129E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66523129E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.90609783E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.19130103E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.32091120E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.04964051E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.67371345E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.46271332E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.23452652E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.28028760E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.95520058E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.30219492E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.30219492E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43022371E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.42644693E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10740956E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15943040E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.47292304E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22335825E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51684133E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.45983272E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.01424443E-06    2          24        25   # BR(H+ -> W+      h      )
     2.66281366E-14    2          24        36   # BR(H+ -> W+      A      )
     4.98908115E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.08839227E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.92589222E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67085800E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.24734721E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57438273E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.50959244E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.92306669E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.33669869E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
