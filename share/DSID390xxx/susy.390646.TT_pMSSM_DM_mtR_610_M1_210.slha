#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11041170E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.43900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.02485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04200448E+01   # W+
        25     1.25515104E+02   # h
        35     4.00000532E+03   # H
        36     3.99999616E+03   # A
        37     4.00101768E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02006437E+03   # ~d_L
   2000001     4.01800063E+03   # ~d_R
   1000002     4.01941374E+03   # ~u_L
   2000002     4.01858792E+03   # ~u_R
   1000003     4.02006437E+03   # ~s_L
   2000003     4.01800063E+03   # ~s_R
   1000004     4.01941374E+03   # ~c_L
   2000004     4.01858792E+03   # ~c_R
   1000005     2.05229010E+03   # ~b_1
   2000005     4.02252544E+03   # ~b_2
   1000006     6.33708706E+02   # ~t_1
   2000006     2.06457832E+03   # ~t_2
   1000011     4.00320317E+03   # ~e_L
   2000011     4.00363297E+03   # ~e_R
   1000012     4.00209235E+03   # ~nu_eL
   1000013     4.00320317E+03   # ~mu_L
   2000013     4.00363297E+03   # ~mu_R
   1000014     4.00209235E+03   # ~nu_muL
   1000015     4.00473133E+03   # ~tau_1
   2000015     4.00797316E+03   # ~tau_2
   1000016     4.00405363E+03   # ~nu_tauL
   1000021     1.98058069E+03   # ~g
   1000022     1.97180214E+02   # ~chi_10
   1000023    -2.54247646E+02   # ~chi_20
   1000025     2.67669017E+02   # ~chi_30
   1000035     2.06578282E+03   # ~chi_40
   1000024     2.51006589E+02   # ~chi_1+
   1000037     2.06595724E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.50583422E-01   # N_11
  1  2    -1.29553310E-02   # N_12
  1  3    -4.14938343E-01   # N_13
  1  4    -3.22747848E-01   # N_14
  2  1    -7.17104610E-02   # N_21
  2  2     2.56739661E-02   # N_22
  2  3    -7.00061180E-01   # N_23
  2  4     7.10009015E-01   # N_24
  3  1     5.20926617E-01   # N_31
  3  2     2.65199217E-02   # N_32
  3  3     5.81147644E-01   # N_33
  3  4     6.24659563E-01   # N_34
  4  1    -9.54982419E-04   # N_41
  4  2     9.99234557E-01   # N_42
  4  3    -2.81646243E-03   # N_43
  4  4    -3.90058320E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     3.98813563E-03   # U_11
  1  2     9.99992047E-01   # U_12
  2  1    -9.99992047E-01   # U_21
  2  2     3.98813563E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.51748947E-02   # V_11
  1  2    -9.98476705E-01   # V_12
  2  1    -9.98476705E-01   # V_21
  2  2    -5.51748947E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -9.69593151E-02   # cos(theta_t)
  1  2     9.95288346E-01   # sin(theta_t)
  2  1    -9.95288346E-01   # -sin(theta_t)
  2  2    -9.69593151E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999495E-01   # cos(theta_b)
  1  2    -1.00498744E-03   # sin(theta_b)
  2  1     1.00498744E-03   # -sin(theta_b)
  2  2     9.99999495E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05283856E-01   # cos(theta_tau)
  1  2     7.08925019E-01   # sin(theta_tau)
  2  1    -7.08925019E-01   # -sin(theta_tau)
  2  2    -7.05283856E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00286683E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10411704E+03  # DRbar Higgs Parameters
         1    -2.43900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44006870E+02   # higgs               
         4     1.61676443E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10411704E+03  # The gauge couplings
     1     3.61570127E-01   # gprime(Q) DRbar
     2     6.35688862E-01   # g(Q) DRbar
     3     1.03311977E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10411704E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.36759424E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10411704E+03  # The trilinear couplings
  1  1     3.43898617E-07   # A_d(Q) DRbar
  2  2     3.43931392E-07   # A_s(Q) DRbar
  3  3     6.15605355E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10411704E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.58543902E-08   # A_mu(Q) DRbar
  3  3     7.66318688E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10411704E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71045562E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10411704E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84585126E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10411704E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03608796E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10411704E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58189339E+07   # M^2_Hd              
        22    -6.09327955E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.02485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40183416E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.37354079E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.32218502E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.03481557E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.00504784E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.14591530E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.04555531E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.31195674E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.81726941E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.31093370E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.69220109E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.36838775E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.95386362E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.92443588E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.51934911E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.09214546E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.02513011E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.85060970E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.05278495E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.25643823E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     8.24782696E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64152942E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.61140428E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79849683E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60183116E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.84090108E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64517927E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.54865245E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13207822E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.77614490E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.26205858E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.44959260E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.09091922E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75708781E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.93902480E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.28213946E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.37843036E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81472255E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.35228626E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61717192E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51815550E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58498567E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.06556032E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.88109422E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.51910955E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.80355332E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43865164E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75727444E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.67588308E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.35546878E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.58385245E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81959944E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.79655067E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64916040E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52039790E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51775341E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.06125382E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.52065981E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.96540429E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.31675351E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85346844E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75708781E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.93902480E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.28213946E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.37843036E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81472255E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.35228626E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61717192E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51815550E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58498567E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.06556032E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.88109422E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.51910955E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.80355332E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43865164E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75727444E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.67588308E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.35546878E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.58385245E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81959944E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.79655067E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64916040E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52039790E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51775341E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.06125382E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.52065981E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.96540429E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.31675351E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85346844E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11788162E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.13862508E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.16943723E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.33031547E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77108227E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.62886704E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55592878E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07014099E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.24309164E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.13162211E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.70558719E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.93977813E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11788162E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.13862508E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.16943723E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.33031547E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77108227E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.62886704E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55592878E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07014099E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.24309164E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.13162211E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.70558719E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.93977813E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08864967E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.67985003E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28764552E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.17043822E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38917871E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46507701E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78526079E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08976787E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.69367630E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.46863066E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.89846446E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41304028E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.14861089E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83309480E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11898145E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.26658909E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.25993011E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.72026232E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77424625E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11568270E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53338230E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11898145E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.26658909E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.25993011E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.72026232E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77424625E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11568270E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53338230E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45000235E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14562899E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.04411177E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.36498837E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51107751E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77878003E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00847555E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.50950204E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33525267E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33525267E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11176288E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11176288E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10596890E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.68110106E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.02010671E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44748168E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.40877133E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39339433E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.93161917E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38461701E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.49288233E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.80088814E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17868993E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52887879E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17868993E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52887879E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43061272E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50544679E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50544679E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47756956E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00850727E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00850727E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00850697E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     9.21330691E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     9.21330691E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     9.21330691E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     9.21330691E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.07110640E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.07110640E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.07110640E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.07110640E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.81530439E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.81530439E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.48267325E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.40750298E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.85164898E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.48406180E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22710919E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.48406180E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.22710919E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.18023545E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.78914247E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.78914247E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.77646997E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.63657387E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.63657387E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.63656437E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.32988295E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.02224400E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.32988295E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.02224400E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.09319881E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.93045306E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.93045306E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.26047374E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.38531515E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.38531515E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.38531517E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.32609487E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.32609487E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     2.32609487E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     2.32609487E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     7.75356709E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     7.75356709E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     7.75356709E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     7.75356709E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     7.33249845E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     7.33249845E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.68043638E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.13195959E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.40877897E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.25301934E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38622021E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38622021E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.32431635E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.01960596E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.12741239E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.98626236E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.98626236E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.79012802E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.79012802E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04491571E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95852981E-01    2           5        -5   # BR(h -> b       bb     )
     6.44063766E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27998224E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83538053E-04    2           3        -3   # BR(h -> s       sb     )
     2.09990926E-02    2           4        -4   # BR(h -> c       cb     )
     6.82464197E-02    2          21        21   # BR(h -> g       g      )
     2.37061524E-03    2          22        22   # BR(h -> gam     gam    )
     1.61837917E-03    2          22        23   # BR(h -> Z       gam    )
     2.18206415E-01    2          24       -24   # BR(h -> W+      W-     )
     2.75881843E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44793972E+01   # H decays
#          BR         NDA      ID1       ID2
     3.48935369E-01    2           5        -5   # BR(H -> b       bb     )
     6.08596708E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15185055E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54526832E-04    2           3        -3   # BR(H -> s       sb     )
     7.13953209E-08    2           4        -4   # BR(H -> c       cb     )
     7.15529758E-03    2           6        -6   # BR(H -> t       tb     )
     8.39033642E-07    2          21        21   # BR(H -> g       g      )
     1.12461270E-10    2          22        22   # BR(H -> gam     gam    )
     1.83952287E-09    2          23        22   # BR(H -> Z       gam    )
     1.89913938E-06    2          24       -24   # BR(H -> W+      W-     )
     9.48911979E-07    2          23        23   # BR(H -> Z       Z      )
     7.08764396E-06    2          25        25   # BR(H -> h       h      )
     2.65839212E-24    2          36        36   # BR(H -> A       A      )
     6.76876476E-20    2          23        36   # BR(H -> Z       A      )
     1.86159459E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63140549E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63140549E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.48969818E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.09752557E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.44033398E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.47538594E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.62403920E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.83307134E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.31554851E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.92841887E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.59969537E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.99324023E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.34199944E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.34199944E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.44764735E+01   # A decays
#          BR         NDA      ID1       ID2
     3.48979983E-01    2           5        -5   # BR(A -> b       bb     )
     6.08632738E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15197625E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54583211E-04    2           3        -3   # BR(A -> s       sb     )
     7.18550944E-08    2           4        -4   # BR(A -> c       cb     )
     7.16883512E-03    2           6        -6   # BR(A -> t       tb     )
     1.47301452E-05    2          21        21   # BR(A -> g       g      )
     5.22513003E-08    2          22        22   # BR(A -> gam     gam    )
     1.62004173E-08    2          23        22   # BR(A -> Z       gam    )
     1.89525508E-06    2          23        25   # BR(A -> Z       h      )
     1.86437070E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63151993E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63151993E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.15165901E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.36694132E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.18256031E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.09185491E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.57606138E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.81108803E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.47308884E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.10403347E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.44425663E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.40229546E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.40229546E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43882807E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.56695864E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09775345E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15601623E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.56285047E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22142392E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51286178E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.54747729E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.90044766E-06    2          24        25   # BR(H+ -> W+      h      )
     2.69131252E-14    2          24        36   # BR(H+ -> W+      A      )
     6.07563955E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.69309256E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.79014428E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63627263E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.66082467E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60696488E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.10782469E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.83423901E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
