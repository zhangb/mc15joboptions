include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa W+/W- -> tau nu + 0,1,2@NLO + 3,4j@LO with 140 GeV < pTV < 220 GeV, 500 < Mjj < 1000 GeV."
evgenConfig.keywords = ["SM", "W", "tau", "jets", "LO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch", "bjr@sas.upenn.edu" ]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "Wtaunu_PTV140_220_MJJ500_1000_KtMerging"

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=2,3,4; QCUT:=20.;

  % Fastjet/antikt merging criterion.
  JET_CRITERION FASTJET[A:kt,R:0.4,y:5];

  % Handle hadronic tau decays.
  SOFT_SPIN_CORRELATIONS=1

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  % Custom decay data to fix soft spin correlations.
  DECAYPATH=.
}(run)

(processes){
  Process 93 93 -> 15 -16 93 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;

  Process 93 93 -> -15 16 93 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 15 -16 2.0 E_CMS
  Mass -15 16 2.0 E_CMS
  "PT" 15,-16 140,220
  "PT" -15,16 140,220
  FastjetSelector Mass(p[4]+p[5])>500.&&Mass(p[4]+p[5])<1000. antikt 2 20. 0. 0.4
}(selector)
"""

genSeq.Sherpa_i.OpenLoopsLibs = [ "ppln", "pplnj", "pplnjj" ]
genSeq.Sherpa_i.ExtraFiles = [ "Decaydata.db" ]
genSeq.Sherpa_i.NCores = 36
