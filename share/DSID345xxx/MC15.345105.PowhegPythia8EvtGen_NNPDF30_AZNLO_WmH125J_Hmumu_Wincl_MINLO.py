#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Wm+jet-> Winc + Hmumu production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# H->mumu decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Wm+jet production: W->all, H->mumu"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs","2muon" ]
evgenConfig.contact     = [ 'paul.daniel.thompson@cern.ch' ]
evgenConfig.process     = "qq->WmH, H->mumu, W->all"
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = 'TXT'
