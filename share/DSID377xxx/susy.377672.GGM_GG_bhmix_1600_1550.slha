#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.54200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.55000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05415419E+01   # W+
        25     1.25000000E+02   # h
        35     2.00400850E+03   # H
        36     2.00000000E+03   # A
        37     2.00151169E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013291E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994925E+03   # ~u_R
   1000003     5.00013291E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994925E+03   # ~c_R
   1000005     4.99945269E+03   # ~b_1
   2000005     5.00070696E+03   # ~b_2
   1000006     4.98649711E+03   # ~t_1
   2000006     5.01792273E+03   # ~t_2
   1000011     5.00008217E+03   # ~e_L
   2000011     5.00007612E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008217E+03   # ~mu_L
   2000013     5.00007612E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99966593E+03   # ~tau_1
   2000015     5.00049296E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.60000000E+03   # ~g
   1000022     1.50034202E+03   # ~chi_10
   1000023    -1.55007751E+03   # ~chi_20
   1000025     1.58754500E+03   # ~chi_30
   1000035     3.00419049E+03   # ~chi_40
   1000024     1.54591879E+03   # ~chi_1+
   1000037     3.00418690E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.22525084E-01   # N_11
  1  2    -3.56721073E-02   # N_12
  1  3     4.89790283E-01   # N_13
  1  4    -4.86611223E-01   # N_14
  2  1     2.82336597E-03   # N_21
  2  2    -3.41011188E-03   # N_22
  2  3    -7.07011445E-01   # N_23
  2  4    -7.07188247E-01   # N_24
  3  1     6.91337066E-01   # N_31
  3  2     3.96210312E-02   # N_32
  3  3    -5.08916464E-01   # N_33
  3  4     5.11358258E-01   # N_34
  4  1     1.61016650E-03   # N_41
  4  2    -9.98572003E-01   # N_42
  4  3    -3.52750311E-02   # N_43
  4  4     4.00878256E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.98403706E-02   # U_11
  1  2     9.98757196E-01   # U_12
  2  1     9.98757196E-01   # U_21
  2  2     4.98403706E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.66467297E-02   # V_11
  1  2     9.98394285E-01   # V_12
  2  1     9.98394285E-01   # V_21
  2  2     5.66467297E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07745190E-01   # cos(theta_t)
  1  2     7.06467795E-01   # sin(theta_t)
  2  1    -7.06467795E-01   # -sin(theta_t)
  2  2     7.07745190E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.76113949E-01   # cos(theta_b)
  1  2     7.36797074E-01   # sin(theta_b)
  2  1    -7.36797074E-01   # -sin(theta_b)
  2  2     6.76113949E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04516225E-01   # cos(theta_tau)
  1  2     7.09687881E-01   # sin(theta_tau)
  2  1    -7.09687881E-01   # -sin(theta_tau)
  2  2     7.04516225E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202406E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.55000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52233792E+02   # vev(Q)              
         4     3.28606888E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52747510E-01   # gprime(Q) DRbar
     2     6.26951334E-01   # g(Q) DRbar
     3     1.08402978E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02510133E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71641660E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79770685E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.54200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.54611941E+05   # M^2_Hd              
        22    -7.68287114E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36948748E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.20914158E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.19312695E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.32070812E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.80920594E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.92126144E-06    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.24587703E-11    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.70359977E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.04632225E-05    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.57224359E-12    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     6.72969588E-10    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.92126144E-06    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.24587703E-11    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.70359977E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.04632225E-05    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.57224359E-12    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     6.72969588E-10    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.58335743E-06    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.54668496E-09    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.95870279E-11    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.89325424E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.89325424E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.89325424E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.89325424E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.19028591E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.13933135E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.44341800E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.74423010E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03037351E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.98526270E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.03649825E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.66208861E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.10308691E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.23048507E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.33523407E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.54723766E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.47364588E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.80142889E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.91985517E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.66921133E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.46986414E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.18238000E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.30357166E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.02615304E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.36159598E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.02785054E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.75149880E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.49802429E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.57840478E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.30452415E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.20251290E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.60586653E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.68890611E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.30191245E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.41365148E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.13780763E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.33665046E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.67163773E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.48058588E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87301234E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.29827451E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.24360694E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.59820907E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.98070079E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.18554635E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.10257634E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.16791890E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.87972162E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.16320972E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60176652E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.33670228E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.96150210E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.02515845E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.29902434E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.30270847E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28524792E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.60389012E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.98113060E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.12031652E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.41826645E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.16362101E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.84398142E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.33057862E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89737657E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.33665046E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.67163773E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.48058588E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87301234E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.29827451E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.24360694E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.59820907E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.98070079E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.18554635E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.10257634E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.16791890E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.87972162E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.16320972E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60176652E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.33670228E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.96150210E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.02515845E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.29902434E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.30270847E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28524792E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.60389012E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.98113060E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.12031652E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.41826645E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.16362101E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.84398142E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.33057862E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89737657E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.90809440E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.65556076E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.82248994E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.98467325E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73222189E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.73185905E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.47641790E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02673959E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.27981613E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.95491273E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.72009139E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.29305269E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.90809440E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.65556076E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.82248994E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.98467325E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73222189E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.73185905E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.47641790E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02673959E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.27981613E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.95491273E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.72009139E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.29305269E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.47201326E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.68746485E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.42913859E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.48897126E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.59031062E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.97566028E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.18761261E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.45492479E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.47105941E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.54803355E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.28183287E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.56198176E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.62455307E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.98866328E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.25616113E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.90822414E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.08832453E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.37223067E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.63313127E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.74160282E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.52855064E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.47133679E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.90822414E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.08832453E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.37223067E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.63313127E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.74160282E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.52855064E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.47133679E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.91084974E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.08734286E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.37099291E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.62714818E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73912989E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.42554156E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.46641991E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.28607357E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.92145839E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.21037613E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.19514091E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.07012661E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.07009807E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.06211243E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.71655781E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53360921E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.10635889E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46238599E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.36373892E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53383149E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.54892206E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     9.81367187E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.96366155E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92462662E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11711829E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.60746124E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.62666632E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.98458513E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.21828602E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.00475317E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.14907138E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.48791088E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.14173768E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.48771430E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36290179E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.39831291E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.39819739E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.36571787E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.78673504E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.78673504E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.78673504E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.84840262E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.84840262E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.63263837E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.63263837E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.49467601E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.49467601E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.46415643E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.46415643E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.93316372E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.93316372E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.12165989E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.89145482E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.60501122E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.11662871E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.85315450E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.35279496E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.13297592E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.34587999E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.65747562E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.35569385E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.21764682E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.33156342E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.33153685E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     5.18718762E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.59193153E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.59193153E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.59193153E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.13528879E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.76494857E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.11360174E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.76432946E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.37819453E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.31501159E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.31468548E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.22258102E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.26115758E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.26115758E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.26115758E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.30546473E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.30546473E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.29801632E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.29801632E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.35154280E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.35154280E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.35140315E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.35140315E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.31238247E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.31238247E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.71655654E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.77092492E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52008207E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.51634858E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46554448E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46554448E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13222067E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.00773939E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.39616813E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80702519E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.74095369E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     7.03821992E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     9.58223601E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.84349436E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08599035E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17804668E-01    2           5        -5   # BR(h -> b       bb     )
     6.37439278E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25630028E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78130381E-04    2           3        -3   # BR(h -> s       sb     )
     2.06275422E-02    2           4        -4   # BR(h -> c       cb     )
     6.69775024E-02    2          21        21   # BR(h -> g       g      )
     2.30327685E-03    2          22        22   # BR(h -> gam     gam    )
     1.53741301E-03    2          22        23   # BR(h -> Z       gam    )
     2.00686663E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56152467E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78101000E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46954705E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427428E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71212705E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547069E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668264E-05    2           4        -4   # BR(H -> c       cb     )
     9.96065861E-01    2           6        -6   # BR(H -> t       tb     )
     7.97661707E-04    2          21        21   # BR(H -> g       g      )
     2.71371787E-06    2          22        22   # BR(H -> gam     gam    )
     1.16016262E-06    2          23        22   # BR(H -> Z       gam    )
     3.34743372E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66915191E-04    2          23        23   # BR(H -> Z       Z      )
     9.02916366E-04    2          25        25   # BR(H -> h       h      )
     7.39056696E-24    2          36        36   # BR(H -> A       A      )
     2.95355588E-11    2          23        36   # BR(H -> Z       A      )
     6.74735225E-12    2          24       -37   # BR(H -> W+      H-     )
     6.74735225E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381798E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47246090E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898021E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62267613E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677536E-06    2           3        -3   # BR(A -> s       sb     )
     9.96177713E-06    2           4        -4   # BR(A -> c       cb     )
     9.96997223E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676813E-04    2          21        21   # BR(A -> g       g      )
     3.15525259E-06    2          22        22   # BR(A -> gam     gam    )
     1.35275272E-06    2          23        22   # BR(A -> Z       gam    )
     3.26272321E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472413E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36073621E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237734E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145424E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50088255E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45694535E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731706E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402185E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34392584E-04    2          24        25   # BR(H+ -> W+      h      )
     2.77235120E-13    2          24        36   # BR(H+ -> W+      A      )
