m_dch = 600.0

evgenConfig.description = "Doubly charged higgs ("+str(m_dch)+") in lepton mode."
evgenConfig.process = "DCH -> same sign 2lepton"
evgenConfig.keywords = ["BSM", "chargedHiggs" ,"2lepton"]
evgenConfig.contact = ["Miha Muskinja <miha.muskinja@ijs.si>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
"9900042:m0 = " + str(m_dch), # H++_R mass [GeV]
"9900041:m0 = " + str(m_dch), # H++_L mass [GeV]
"LeftRightSymmmetry:ffbar2HLHL=on", #HL pair production
"LeftRightSymmmetry:ffbar2HRHR=on", #HR pair production
# set all couplings to leptons to 0.1
"LeftRightSymmmetry:coupHee=0.1",
"LeftRightSymmmetry:coupHmue=0.1",
"LeftRightSymmmetry:coupHmumu=0.1",
"LeftRightSymmmetry:coupHtaue=0.1",
"LeftRightSymmmetry:coupHtaumu=0.1",
"LeftRightSymmmetry:coupHtautau=0.1"]
