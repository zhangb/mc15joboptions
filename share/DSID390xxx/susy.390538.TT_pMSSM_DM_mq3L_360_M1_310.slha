#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90805978E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.50900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04176583E+01   # W+
        25     1.24630324E+02   # h
        35     4.00000845E+03   # H
        36     3.99999630E+03   # A
        37     4.00097211E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01278921E+03   # ~d_L
   2000001     4.01198969E+03   # ~d_R
   1000002     4.01212462E+03   # ~u_L
   2000002     4.01387288E+03   # ~u_R
   1000003     4.01278921E+03   # ~s_L
   2000003     4.01198969E+03   # ~s_R
   1000004     4.01212462E+03   # ~c_L
   2000004     4.01387288E+03   # ~c_R
   1000005     4.08316257E+02   # ~b_1
   2000005     4.01807079E+03   # ~b_2
   1000006     3.98982080E+02   # ~t_1
   2000006     2.32356961E+03   # ~t_2
   1000011     4.00235084E+03   # ~e_L
   2000011     4.00225012E+03   # ~e_R
   1000012     4.00122774E+03   # ~nu_eL
   1000013     4.00235084E+03   # ~mu_L
   2000013     4.00225012E+03   # ~mu_R
   1000014     4.00122774E+03   # ~nu_muL
   1000015     4.00416419E+03   # ~tau_1
   2000015     4.00814691E+03   # ~tau_2
   1000016     4.00380418E+03   # ~nu_tauL
   1000021     1.96127405E+03   # ~g
   1000022     3.02200809E+02   # ~chi_10
   1000023    -3.60550237E+02   # ~chi_20
   1000025     3.71761052E+02   # ~chi_30
   1000035     2.05812733E+03   # ~chi_40
   1000024     3.58173853E+02   # ~chi_1+
   1000037     2.05829124E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.75903914E-01   # N_11
  1  2    -1.31594202E-02   # N_12
  1  3    -3.71706146E-01   # N_13
  1  4    -3.07333212E-01   # N_14
  2  1    -4.93855853E-02   # N_21
  2  2     2.45509687E-02   # N_22
  2  3    -7.03273062E-01   # N_23
  2  4     7.08777337E-01   # N_24
  3  1     4.79950356E-01   # N_31
  3  2     2.86773402E-02   # N_32
  3  3     6.05983883E-01   # N_33
  3  4     6.33726123E-01   # N_34
  4  1    -1.02566805E-03   # N_41
  4  2     9.99200525E-01   # N_42
  4  3    -5.00741168E-03   # N_43
  4  4    -3.96507765E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.08728936E-03   # U_11
  1  2     9.99974885E-01   # U_12
  2  1    -9.99974885E-01   # U_21
  2  2     7.08728936E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.60862086E-02   # V_11
  1  2    -9.98425930E-01   # V_12
  2  1    -9.98425930E-01   # V_21
  2  2    -5.60862086E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97834101E-01   # cos(theta_t)
  1  2    -6.57807486E-02   # sin(theta_t)
  2  1     6.57807486E-02   # -sin(theta_t)
  2  2     9.97834101E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999405E-01   # cos(theta_b)
  1  2    -1.09087105E-03   # sin(theta_b)
  2  1     1.09087105E-03   # -sin(theta_b)
  2  2     9.99999405E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05621194E-01   # cos(theta_tau)
  1  2     7.08589254E-01   # sin(theta_tau)
  2  1    -7.08589254E-01   # -sin(theta_tau)
  2  2    -7.05621194E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00240491E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.08059782E+02  # DRbar Higgs Parameters
         1    -3.50900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44675722E+02   # higgs               
         4     1.63957481E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.08059782E+02  # The gauge couplings
     1     3.60714143E-01   # gprime(Q) DRbar
     2     6.35779083E-01   # g(Q) DRbar
     3     1.03842753E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.08059782E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.86367703E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.08059782E+02  # The trilinear couplings
  1  1     2.16003821E-07   # A_d(Q) DRbar
  2  2     2.16024594E-07   # A_s(Q) DRbar
  3  3     3.85871994E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.08059782E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.77527655E-08   # A_mu(Q) DRbar
  3  3     4.82274353E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.08059782E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72357837E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.08059782E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85561023E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.08059782E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02903410E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.08059782E+02  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57447557E+07   # M^2_Hd              
        22    -2.03568024E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40233634E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.47025343E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46850380E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46850380E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53149620E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53149620E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.44169679E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.36206983E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.09971745E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18436829E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.00419528E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.47442803E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.35288695E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.29697176E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.02620250E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.13720520E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.88842290E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.89631132E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.46212392E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.69757928E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.99355237E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.30886835E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66549895E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.50656329E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76426651E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61012278E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.57239085E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55019421E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.69627483E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15048302E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.37934086E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.86074118E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.13423151E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.44722724E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77994457E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00755873E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.98836779E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.22483278E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.75672556E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.40755526E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.50132282E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53626486E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60876511E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.19561527E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.32761965E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.25266903E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.17796487E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45384363E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78014851E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.76050079E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.87090867E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.30990663E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76178800E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.63524870E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.53356437E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53847640E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54124773E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.09431867E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.46274040E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.26724874E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.28352185E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85754929E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77994457E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00755873E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.98836779E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.22483278E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.75672556E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.40755526E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.50132282E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53626486E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60876511E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.19561527E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.32761965E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.25266903E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.17796487E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45384363E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78014851E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.76050079E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.87090867E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.30990663E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76178800E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.63524870E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.53356437E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53847640E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54124773E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.09431867E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.46274040E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.26724874E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.28352185E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85754929E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12526665E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.19149125E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.09399379E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.58449992E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77834098E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.09034037E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57114780E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04564616E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.68257427E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.43042756E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.29311569E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.76505787E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12526665E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.19149125E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.09399379E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.58449992E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77834098E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.09034037E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57114780E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04564616E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.68257427E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.43042756E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.29311569E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.76505787E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07557400E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75844846E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41063785E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.04021975E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40085267E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50406345E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80900898E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07268217E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.86220860E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.14162578E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.72486837E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43031852E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00382779E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86805916E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12634301E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.32381844E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.39916185E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.00183243E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78195747E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18583508E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54819088E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12634301E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.32381844E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.39916185E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.00183243E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78195747E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18583508E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54819088E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45347616E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.19920811E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26746748E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.71929233E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52238956E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.63267220E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03053120E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.72688111E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33509308E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33509308E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11171220E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11171220E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10638944E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.78278601E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87440150E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.78314257E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.96537017E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.61963238E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.82950604E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.84333025E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.82438959E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.59567723E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.74468014E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17535705E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52396929E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17535705E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52396929E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45557835E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49091861E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49091861E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46443332E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.97795790E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.97795790E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.97795755E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.84851943E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.84851943E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.84851943E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.84851943E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.16173743E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.16173743E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.16173743E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.16173743E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.37599272E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.37599272E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.86031084E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.19016803E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.81621040E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.82887271E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.27041827E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.82887271E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.27041827E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.34571806E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.87828203E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.87828203E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.87221350E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.83158416E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.83158416E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.83156821E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.72464803E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.23626124E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.72464803E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.23626124E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.66936895E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.12302305E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.12302305E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.40915297E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02387439E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02387439E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02387440E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.52763999E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.52763999E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.52763999E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.52763999E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.09208799E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.09208799E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.09208799E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.09208799E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.68415508E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.68415508E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.78170175E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.10986212E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.73411289E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.81958915E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.82248879E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.82248879E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.61096940E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.18740073E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.06168533E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90609511E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90609511E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.92291245E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.92291245E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.90466961E-03   # h decays
#          BR         NDA      ID1       ID2
     6.05376144E-01    2           5        -5   # BR(h -> b       bb     )
     6.62360160E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.34479144E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.97969509E-04    2           3        -3   # BR(h -> s       sb     )
     2.16298410E-02    2           4        -4   # BR(h -> c       cb     )
     7.11751912E-02    2          21        21   # BR(h -> g       g      )
     2.37184274E-03    2          22        22   # BR(h -> gam     gam    )
     1.54898684E-03    2          22        23   # BR(h -> Z       gam    )
     2.05340711E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55888182E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.52339525E+01   # H decays
#          BR         NDA      ID1       ID2
     3.58849898E-01    2           5        -5   # BR(H -> b       bb     )
     6.00283369E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12245660E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51050011E-04    2           3        -3   # BR(H -> s       sb     )
     7.04070395E-08    2           4        -4   # BR(H -> c       cb     )
     7.05625128E-03    2           6        -6   # BR(H -> t       tb     )
     7.53974692E-07    2          21        21   # BR(H -> g       g      )
     1.02547856E-09    2          22        22   # BR(H -> gam     gam    )
     1.81654776E-09    2          23        22   # BR(H -> Z       gam    )
     1.63503282E-06    2          24       -24   # BR(H -> W+      W-     )
     8.16950048E-07    2          23        23   # BR(H -> Z       Z      )
     6.47931465E-06    2          25        25   # BR(H -> h       h      )
    -2.87374261E-24    2          36        36   # BR(H -> A       A      )
     4.53709818E-20    2          23        36   # BR(H -> Z       A      )
     1.86013147E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58691516E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58691516E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.05687101E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.75588271E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.22862277E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.75226820E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.40527506E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.13356645E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.02801403E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.08943675E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.69266959E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.65686770E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.77031821E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.77031821E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.46685210E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.52313399E+01   # A decays
#          BR         NDA      ID1       ID2
     3.58892365E-01    2           5        -5   # BR(A -> b       bb     )
     6.00314358E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12256450E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51103738E-04    2           3        -3   # BR(A -> s       sb     )
     7.08730274E-08    2           4        -4   # BR(A -> c       cb     )
     7.07085631E-03    2           6        -6   # BR(A -> t       tb     )
     1.45288229E-05    2          21        21   # BR(A -> g       g      )
     5.96698594E-08    2          22        22   # BR(A -> gam     gam    )
     1.59723630E-08    2          23        22   # BR(A -> Z       gam    )
     1.63174230E-06    2          23        25   # BR(A -> Z       h      )
     1.87394331E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58693552E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58693552E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.78384333E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.54527998E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.03213467E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.34862101E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.15179203E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.26031900E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.13764543E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.65456584E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.00108899E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.85499934E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.85499934E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53868793E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.77085943E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.98774571E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11712019E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69334690E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19939029E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46753152E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67354896E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.62892066E-06    2          24        25   # BR(H+ -> W+      h      )
     2.10211857E-14    2          24        36   # BR(H+ -> W+      A      )
     6.26967146E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.03515231E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.30528175E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58455760E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.38867072E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57052268E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.16825982E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.38933741E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     9.59524497E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
