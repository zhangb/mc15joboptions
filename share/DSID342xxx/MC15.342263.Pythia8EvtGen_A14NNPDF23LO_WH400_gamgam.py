evgenConfig.description = "PYTHIA8+EVTGEN, WH, W->any, H->gamgam, mH=400GeV"
evgenConfig.keywords    = [ "BSM", "Higgs", "WHiggs", "diphoton" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 400.

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 22 22', # Higgs decay
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]

