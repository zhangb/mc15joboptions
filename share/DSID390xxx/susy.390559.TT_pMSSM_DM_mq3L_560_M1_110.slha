#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11088619E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.20485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04149757E+01   # W+
        25     1.24099141E+02   # h
        35     4.00000063E+03   # H
        36     3.99999589E+03   # A
        37     4.00104954E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02014474E+03   # ~d_L
   2000001     4.01791853E+03   # ~d_R
   1000002     4.01948011E+03   # ~u_L
   2000002     4.01921552E+03   # ~u_R
   1000003     4.02014474E+03   # ~s_L
   2000003     4.01791853E+03   # ~s_R
   1000004     4.01948011E+03   # ~c_L
   2000004     4.01921552E+03   # ~c_R
   1000005     6.20398939E+02   # ~b_1
   2000005     4.02206980E+03   # ~b_2
   1000006     6.12772858E+02   # ~t_1
   2000006     2.22287608E+03   # ~t_2
   1000011     4.00360921E+03   # ~e_L
   2000011     4.00294859E+03   # ~e_R
   1000012     4.00248286E+03   # ~nu_eL
   1000013     4.00360921E+03   # ~mu_L
   2000013     4.00294859E+03   # ~mu_R
   1000014     4.00248286E+03   # ~nu_muL
   1000015     4.00534121E+03   # ~tau_1
   2000015     4.00706972E+03   # ~tau_2
   1000016     4.00443673E+03   # ~nu_tauL
   1000021     1.97171939E+03   # ~g
   1000022     9.35370669E+01   # ~chi_10
   1000023    -1.36874796E+02   # ~chi_20
   1000025     1.52598699E+02   # ~chi_30
   1000035     2.05501215E+03   # ~chi_40
   1000024     1.31803748E+02   # ~chi_1+
   1000037     2.05517701E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61877938E-01   # N_11
  1  2     1.38176098E-02   # N_12
  1  3     5.33928021E-01   # N_13
  1  4     3.66431370E-01   # N_14
  2  1    -1.35909456E-01   # N_21
  2  2     2.72813777E-02   # N_22
  2  3    -6.85301435E-01   # N_23
  2  4     7.14944956E-01   # N_24
  3  1     6.33300728E-01   # N_31
  3  2     2.39021216E-02   # N_32
  3  3     4.95260075E-01   # N_33
  3  4     5.94202268E-01   # N_34
  4  1    -9.02782076E-04   # N_41
  4  2     9.99246460E-01   # N_42
  4  3    -5.19799988E-04   # N_43
  4  4    -3.87998211E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.39432586E-04   # U_11
  1  2     9.99999727E-01   # U_12
  2  1    -9.99999727E-01   # U_21
  2  2     7.39432586E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48847007E-02   # V_11
  1  2    -9.98492699E-01   # V_12
  2  1    -9.98492699E-01   # V_21
  2  2    -5.48847007E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97260840E-01   # cos(theta_t)
  1  2    -7.39649715E-02   # sin(theta_t)
  2  1     7.39649715E-02   # -sin(theta_t)
  2  2     9.97260840E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999924E-01   # cos(theta_b)
  1  2    -3.89871766E-04   # sin(theta_b)
  2  1     3.89871766E-04   # -sin(theta_b)
  2  2     9.99999924E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03054252E-01   # cos(theta_tau)
  1  2     7.11136217E-01   # sin(theta_tau)
  2  1    -7.11136217E-01   # -sin(theta_tau)
  2  2    -7.03054252E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00305142E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10886193E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44098785E+02   # higgs               
         4     1.61242701E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10886193E+03  # The gauge couplings
     1     3.61582046E-01   # gprime(Q) DRbar
     2     6.37171356E-01   # g(Q) DRbar
     3     1.03386003E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10886193E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.50612882E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10886193E+03  # The trilinear couplings
  1  1     3.50252662E-07   # A_d(Q) DRbar
  2  2     3.50286236E-07   # A_s(Q) DRbar
  3  3     6.23183076E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10886193E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.53068261E-08   # A_mu(Q) DRbar
  3  3     7.60759463E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10886193E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68565159E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10886193E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80093497E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10886193E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03946882E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10886193E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58626949E+07   # M^2_Hd              
        22    -3.54849594E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.20485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40860262E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.75215006E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46316319E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46316319E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53683681E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53683681E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.84443256E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.26961879E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.43893884E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.19808331E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09335906E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22150797E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.01700811E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.34490695E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03798876E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.60389651E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.58681404E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.58275401E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.22379544E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.03801986E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.82563388E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.16560806E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.94156086E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.03485443E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.77327177E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.82503129E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66589961E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.78801898E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.68647790E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40538557E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.83341350E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.52845931E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.49212851E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15848075E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.72561296E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.89481100E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.22329870E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
    -3.02035045E-09    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78722124E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48326159E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.15869261E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80117629E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79131869E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25863148E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57037809E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52572515E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61283522E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.21693731E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02261707E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.21930345E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.48154498E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44614950E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78742751E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.17926990E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45068441E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.02152226E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79595485E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.54532395E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60201186E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52793747E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54471770E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.39421872E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66839693E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.79100505E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.47238951E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85547930E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78722124E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48326159E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.15869261E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80117629E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79131869E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25863148E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57037809E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52572515E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61283522E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.21693731E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02261707E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.21930345E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.48154498E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44614950E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78742751E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.17926990E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45068441E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.02152226E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79595485E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.54532395E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60201186E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52793747E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54471770E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.39421872E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66839693E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.79100505E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.47238951E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85547930E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15263767E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97377750E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27118704E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.51237323E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77507705E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.59597195E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56359041E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07851475E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80894358E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84622217E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00642977E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.42842238E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15263767E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97377750E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27118704E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.51237323E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77507705E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.59597195E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56359041E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07851475E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80894358E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84622217E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00642977E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.42842238E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11031628E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27768535E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00045181E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60192223E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39114393E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.40188111E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78901519E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11944041E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11816575E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33041736E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35415997E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42197334E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.21818729E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85084047E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15370023E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01916463E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.57399962E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75175446E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77793596E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08114317E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54117254E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15370023E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01916463E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.57399962E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75175446E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77793596E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08114317E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54117254E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48772280E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22009105E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.04264427E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20345708E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51487232E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75913238E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01643319E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.21007306E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33727435E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33727435E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11243252E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11243252E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10058625E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.35411956E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.77077780E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.67206313E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.50592732E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.55561096E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.43336496E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.69590910E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.38077836E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.18568728E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.79173330E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18516951E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53670438E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18516951E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53670438E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37389603E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52005081E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52005081E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47608554E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03709532E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03709532E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03709494E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.96568372E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.96568372E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.96568372E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.96568372E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.88563687E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.88563687E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.88563687E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.88563687E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.49431420E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.49431420E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.17274406E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.57714697E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.85508048E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.01756553E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.48816655E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.01756553E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.48816655E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.04435717E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.47227533E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.47227533E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.45756635E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.97798359E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.97798359E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.97797794E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.26980912E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.13025002E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.26980912E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.13025002E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.16459590E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.86288114E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.86288114E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.73086927E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.72269703E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.72269703E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.72269716E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.66299827E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.66299827E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.66299827E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.66299827E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.88761800E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.88761800E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.88761800E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.88761800E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.78522112E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.78522112E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.35299872E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.47368046E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.41325834E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.24374454E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.34976291E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.34976291E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.33169609E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.11206670E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.51775057E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85224493E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85224493E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.86941089E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.86941089E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.87450586E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17808187E-01    2           5        -5   # BR(h -> b       bb     )
     6.64836464E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.35358225E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.00251118E-04    2           3        -3   # BR(h -> s       sb     )
     2.17234375E-02    2           4        -4   # BR(h -> c       cb     )
     6.99915107E-02    2          21        21   # BR(h -> g       g      )
     2.35002706E-03    2          22        22   # BR(h -> gam     gam    )
     1.48729860E-03    2          22        23   # BR(h -> Z       gam    )
     1.95310349E-01    2          24       -24   # BR(h -> W+      W-     )
     2.41099344E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43097749E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37597175E-01    2           5        -5   # BR(H -> b       bb     )
     6.10496671E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15856836E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55321469E-04    2           3        -3   # BR(H -> s       sb     )
     7.16235127E-08    2           4        -4   # BR(H -> c       cb     )
     7.17816706E-03    2           6        -6   # BR(H -> t       tb     )
     1.06257168E-06    2          21        21   # BR(H -> g       g      )
     2.59693831E-10    2          22        22   # BR(H -> gam     gam    )
     1.84160344E-09    2          23        22   # BR(H -> Z       gam    )
     2.00646258E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00253610E-06    2          23        23   # BR(H -> Z       Z      )
     7.44228675E-06    2          25        25   # BR(H -> h       h      )
    -5.10372798E-24    2          36        36   # BR(H -> A       A      )
     5.97931911E-20    2          23        36   # BR(H -> Z       A      )
     1.86882724E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67374532E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67374532E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.36275315E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60946103E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68745810E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48381339E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.94026575E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.95283231E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.05190255E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.43674614E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.39484943E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.27583696E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.71986749E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.71986749E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.39911340E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43095848E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37624287E-01    2           5        -5   # BR(A -> b       bb     )
     6.10502973E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15858894E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55365507E-04    2           3        -3   # BR(A -> s       sb     )
     7.20758947E-08    2           4        -4   # BR(A -> c       cb     )
     7.19086391E-03    2           6        -6   # BR(A -> t       tb     )
     1.47754101E-05    2          21        21   # BR(A -> g       g      )
     4.07485101E-08    2          22        22   # BR(A -> gam     gam    )
     1.62358641E-08    2          23        22   # BR(A -> Z       gam    )
     2.00239740E-06    2          23        25   # BR(A -> Z       h      )
     1.87195016E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67377211E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67377211E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93747591E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.23118148E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33464277E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97240158E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.55682858E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.63667474E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.31308473E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.31888432E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.82662950E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.76241945E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.76241945E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.41877846E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.37840863E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.12036403E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16401078E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.44217854E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22595175E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52217699E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42998050E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.00918756E-06    2          24        25   # BR(H+ -> W+      h      )
     3.15368639E-14    2          24        36   # BR(H+ -> W+      A      )
     4.85179817E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.29066794E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.08199564E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67972387E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.62141110E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58079549E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.24899758E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.85387903E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.25737346E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
