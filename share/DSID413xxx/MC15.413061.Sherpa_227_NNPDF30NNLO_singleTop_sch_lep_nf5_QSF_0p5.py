# SHERPA run card for s-channel single top-quark production at MC@NLO
# and N_f = 5
include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa tj Production s-channel with NF5"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "kai.chung.tam@cern.ch" ]
evgenConfig.minevents = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
 
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=0.5
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2}
  CORE_SCALE SingleTop

  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  PARTICLE_CONTAINER 900 lj 1 -1 2 -2 3 -3 4 -4 21
}(run)

(processes){
  Process 900 900 -> 6 93
  NLO_QCD_Mode MC@NLO
  Order (*,2)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Max_N_TChannels 0  # require s-channel W
  End process
}(processes)
"""

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptj", "pptjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "EW_SCHEME=3" ]
