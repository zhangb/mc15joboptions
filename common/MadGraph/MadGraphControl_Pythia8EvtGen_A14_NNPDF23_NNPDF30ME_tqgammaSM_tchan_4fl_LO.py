from MadGraphControl.MadGraphUtils import *
import os,subprocess,fileinput

# General settings 

nevents=int(1.1*runArgs.maxEvents)
mode=0
#gridpack_mode=True
#gridpack_dir='madevent/'

name = 'tqgammaSM_tchan_4fl'
runName='madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

fcard = open('proc_card_mg5.dat','w')

fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~ 
define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
define vl = ve vm vt 
define vl~ = ve~ vm~ vt~
generate p p > t b~ j a $$ w+, (t > l+ vl b)
add process p p > t b~ j $$ w+, (t > l+ vl b a)
add process p p > t~ b j a $$ w-, (t~ > l- vl~ b~)
add process p p > t~ b j $$ w-, (t~ > l- vl~ b~ a) 
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#Fetch default LO run_card.dat and set parameters

extras = { 'lhaid'         :260400, #NNPDF30_nlo_as_0118_nf_4
           'pdlabel'       :'lhapdf',
           'cut_decays'    : 'T', 
           #'parton_shower' :'PYTHIA8',
           'pta'           :10,
           'maxjetflavor'  :4,
           'drab'          :0.2, 
           'draj'          :0.2, 
           'dral'          :0.2,
           'dynamical_scale_choice': '3', #sum of the transverse mass divided by 2
           'use_syst'      : 'T',
           'sys_scalefact' : '1 0.5 2',
           'sys_pdf'       : 'NNPDF30_nlo_as_0118_nf_4',
           'ptl'           :0.0,
           'ptj'           :0.0,
           'etal'          :5.0,
           'etaa'          :5.0,
           'etaj'          :-1,
           'etab'          :-1,
           'drjj'          :0.0,
           'drjl'          :0.0,
           'drll'          :0.0,
           'draa'          :0.0,
           }

process_dir = new_process()#grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)


print_cards()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,random_seed=runArgs.randomSeed,nevents=nevents)#,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,gridpack_compile=True)
outputDS=arrange_output(proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3)

#### Shower
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.keywords+= ['SM', 'top',  'photon','singleTop','lepton'] 
evgenConfig.contact = ['harish.potti@cern.ch']
runArgs.inputGeneratorFile=outputDS


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")



if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): 
        mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")
