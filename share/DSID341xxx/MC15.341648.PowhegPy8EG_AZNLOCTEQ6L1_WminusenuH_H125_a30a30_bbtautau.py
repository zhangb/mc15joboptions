#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+W+jet->mu-vmubbar production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HWj_Common.py')

PowhegConfig.runningscales = 1 # CHECK
PowhegConfig.idvecbos = -24
PowhegConfig.vdecaymode = 1 # W->e-ve
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 120 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 200
#PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
#PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations
#PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
#PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.nEvents *= 6.0

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()


#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
H_Mass = 125.0
H_Width = 0.00407
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 36 36', # h->aa

                             '36:onMode = off', # decay of the a
                             '36:oneChannel = 1 0.5 100 5 -5', #a->bb
                             '36:addChannel = 1 0.5 100 15 -15', #a->tautau
                             '36:m0 30.0', #scalar mass
                             '36:mMin 29.5', #scalar mass
                             '36:mMax 30.5', #scalar mass
                             '36:mWidth 0.01', # narrow width
                             '36:tau0 0', #scalarlife time
                             ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = [ 'roger.caminal@cern.ch', 'philip.chang@cern.ch' ]


if not hasattr(genSeq, "XtoVVDecayFilter"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
    genSeq += XtoVVDecayFilter()
    genSeq += XtoVVDecayFilter("scalarFilter")

### Add this filter to the algs required to be successful for streaming
#if "XtoVVDecayFilter" not in StreamEVGEN.RequireAlgs:
#    StreamEVGEN.RequireAlgs += ["XtoVVDecayFilter"]
genSeq.XtoVVDecayFilter.PDGGrandParent = 36
genSeq.XtoVVDecayFilter.PDGParent = 15
genSeq.XtoVVDecayFilter.StatusParent = 2
genSeq.XtoVVDecayFilter.PDGChild1 = [11,13]
genSeq.XtoVVDecayFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]

genSeq.scalarFilter.PDGGrandParent = 35
genSeq.scalarFilter.PDGParent = 36
genSeq.scalarFilter.StatusParent = 22
genSeq.scalarFilter.PDGChild1 = [15]
genSeq.scalarFilter.PDGChild2 = [5]

evgenConfig.minevents=500
