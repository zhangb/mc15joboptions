evgenConfig.description = "gammagamma->qqbar in pp with EPA parameterization, M>4 GeV"
evgenConfig.keywords = ["QED", "exclusive", "diphoton"]
evgenConfig.contact = ["Mateusz DYndal <mateusz.dyndal@cern.ch"]

include("MC15JobOptions/Herwigpp_QED_Common.py")
from Herwigpp_i import config as hw

cmds = """\

# Cuts
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*MeV
set /Herwig/Partons/BudnevPDF:Q2Max 4
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0
set QCDCuts:MHatMin 4*GeV
set JetKtCut:MinKT 2*GeV'
set JetKtCut:MinEta -2.8
set JetKtCut:MaxEta 2.8 

# Selected the hard process

cd /Herwig/MatrixElements
insert SimpleQCD:MatrixElements 0 /Herwig/MatrixElements/MEgg2ff
set /Herwig/MatrixElements/MEgg2ff:Process Quarks
"""

genSeq.Herwigpp.Commands += cmds.splitlines()



evgenConfig.minevents = 5000

# To avoid warning from displaced vertices, bugfix needed in herwig++
testSeq.TestHepMC.MaxTransVtxDisp = 1000000
testSeq.TestHepMC.MaxVtxDisp      = 1000000000
