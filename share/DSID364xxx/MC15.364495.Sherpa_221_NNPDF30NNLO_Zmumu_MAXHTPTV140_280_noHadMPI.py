include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2j@NLO + 3,4j@LO with 140 GeV < max(HT, pTV) < 280 GeV with light-jet filter."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Zmumu_MAXHTPTV140_280"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  %settings for MAX(HT,PTV) slicing
  HTMIN:=140
  HTMAX:=280
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "SHERPA_LDADD=SherpaFastjetMAXHTPTV" ]


# Turn off hadronisation and MPI
genSeq.Sherpa_i.Parameters += [
           "MI_HANDLER=None",   # (multiparton interactions)
           "FRAGMENTATION=Off", # (hadronisation)
           "K_PERP_MEAN_1=0.0", # (intrinsic kperp)
           "K_PERP_MEAN_2=0.0",
           "K_PERP_SIGMA_1=0.0",
           "K_PERP_SIGMA_2=0.0",
           ]
 
import os

from Rivet_i.Rivet_iConf import Rivet_i
genSeq += Rivet_i()
genSeq.Rivet_i.AnalysisPath = os.environ['PWD']
#genSeq.Rivet_i.HistoFile = "myanalysis"
genSeq.Rivet_i.CrossSection = 1
