#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11039724E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.41900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.19485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194492E+01   # W+
        25     1.25196543E+02   # h
        35     4.00000541E+03   # H
        36     3.99999627E+03   # A
        37     4.00101083E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02006977E+03   # ~d_L
   2000001     4.01801990E+03   # ~d_R
   1000002     4.01941865E+03   # ~u_L
   2000002     4.01853113E+03   # ~u_R
   1000003     4.02006977E+03   # ~s_L
   2000003     4.01801990E+03   # ~s_R
   1000004     4.01941865E+03   # ~c_L
   2000004     4.01853113E+03   # ~c_R
   1000005     2.22008779E+03   # ~b_1
   2000005     4.02266013E+03   # ~b_2
   1000006     6.04250070E+02   # ~t_1
   2000006     2.23023802E+03   # ~t_2
   1000011     4.00316387E+03   # ~e_L
   2000011     4.00370951E+03   # ~e_R
   1000012     4.00205247E+03   # ~nu_eL
   1000013     4.00316387E+03   # ~mu_L
   2000013     4.00370951E+03   # ~mu_R
   1000014     4.00205247E+03   # ~nu_muL
   1000015     4.00471344E+03   # ~tau_1
   2000015     4.00803312E+03   # ~tau_2
   1000016     4.00401547E+03   # ~nu_tauL
   1000021     1.98345280E+03   # ~g
   1000022     1.96550262E+02   # ~chi_10
   1000023    -2.52286108E+02   # ~chi_20
   1000025     2.66261077E+02   # ~chi_30
   1000035     2.06777192E+03   # ~chi_40
   1000024     2.48984191E+02   # ~chi_1+
   1000037     2.06793958E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.43304260E-01   # N_11
  1  2    -1.32930857E-02   # N_12
  1  3    -4.23131476E-01   # N_13
  1  4    -3.31090581E-01   # N_14
  2  1    -7.20424589E-02   # N_21
  2  2     2.57008489E-02   # N_22
  2  3    -6.99985514E-01   # N_23
  2  4     7.10049034E-01   # N_24
  3  1     5.32585108E-01   # N_31
  3  2     2.63171089E-02   # N_32
  3  3     5.75301941E-01   # N_33
  3  4     6.20232368E-01   # N_34
  4  1    -9.55163081E-04   # N_41
  4  2     9.99234792E-01   # N_42
  4  3    -2.77690979E-03   # N_43
  4  4    -3.90026463E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     3.93218648E-03   # U_11
  1  2     9.99992269E-01   # U_12
  2  1    -9.99992269E-01   # U_21
  2  2     3.93218648E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.51703746E-02   # V_11
  1  2    -9.98476955E-01   # V_12
  2  1    -9.98476955E-01   # V_21
  2  2    -5.51703746E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.05284036E-02   # cos(theta_t)
  1  2     9.96752314E-01   # sin(theta_t)
  2  1    -9.96752314E-01   # -sin(theta_t)
  2  2    -8.05284036E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999438E-01   # cos(theta_b)
  1  2    -1.06018851E-03   # sin(theta_b)
  2  1     1.06018851E-03   # -sin(theta_b)
  2  2     9.99999438E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05281361E-01   # cos(theta_tau)
  1  2     7.08927501E-01   # sin(theta_tau)
  2  1    -7.08927501E-01   # -sin(theta_tau)
  2  2    -7.05281361E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00280738E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10397242E+03  # DRbar Higgs Parameters
         1    -2.41900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44053377E+02   # higgs               
         4     1.61891534E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10397242E+03  # The gauge couplings
     1     3.61580904E-01   # gprime(Q) DRbar
     2     6.35639936E-01   # g(Q) DRbar
     3     1.03305143E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10397242E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.34793591E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10397242E+03  # The trilinear couplings
  1  1     3.43387180E-07   # A_d(Q) DRbar
  2  2     3.43419859E-07   # A_s(Q) DRbar
  3  3     6.14468188E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10397242E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.55901510E-08   # A_mu(Q) DRbar
  3  3     7.63629732E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10397242E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.70034105E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10397242E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84233023E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10397242E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03576571E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10397242E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58193139E+07   # M^2_Hd              
        22    -8.46013841E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.19485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40164439E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.44272760E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.20587695E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.04739528E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.97757586E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.09174494E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.12593967E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.79600537E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.99792378E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.33954476E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.70883456E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.19003793E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.18000206E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.60146545E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.56503268E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.25584527E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.35358124E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.28031643E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.41038721E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.67977179E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.51554422E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.86495262E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.10529442E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.46754998E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64354135E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.61487377E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78819420E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58668592E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.92242446E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62490075E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.70901205E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13651752E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.38342339E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     5.66196709E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.91663361E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     9.81680083E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75915826E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.89229840E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.27261061E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.42140269E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80169582E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34480387E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59119592E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52210175E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58739007E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.99042034E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.90383044E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.58563685E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.79656955E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43949017E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75934667E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.64097728E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.36009336E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.89041683E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80656059E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.71527812E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62309754E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52434675E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52021985E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.04154753E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.57933994E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.13869829E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.29807247E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85370026E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75915826E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.89229840E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.27261061E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.42140269E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80169582E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34480387E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59119592E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52210175E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58739007E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.99042034E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.90383044E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.58563685E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.79656955E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43949017E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75934667E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.64097728E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.36009336E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.89041683E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80656059E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.71527812E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62309754E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52434675E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52021985E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.04154753E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.57933994E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.13869829E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.29807247E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85370026E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11382217E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.11862335E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.19682934E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.55265161E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77032291E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.58549056E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55443320E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07030214E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.11988010E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.17989874E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.82831598E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.93488239E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11382217E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.11862335E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.19682934E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.55265161E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77032291E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.58549056E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55443320E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07030214E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.11988010E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.17989874E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.82831598E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.93488239E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08656840E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.64314144E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28679343E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.21072349E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38798592E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46587198E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78288261E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08784392E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.64500978E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.47575521E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.94988996E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41174419E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.15270384E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83051017E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11492610E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.24898980E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.27779457E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.91686868E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77347890E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11911157E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53187537E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11492610E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.24898980E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.27779457E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.91686868E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77347890E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11911157E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53187537E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44588463E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12959025E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.06005081E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.54244226E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51012224E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.78851367E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00659141E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.36000662E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33536601E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33536601E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11180082E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11180082E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10566635E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.67396523E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.34912788E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46598774E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.69404235E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.41158513E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.92072482E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39738528E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.37316368E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.74872318E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17918009E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52954085E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17918009E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52954085E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42693079E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50711716E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50711716E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47831825E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01187687E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01187687E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01187658E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.09842447E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.09842447E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.09842447E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.09842447E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.66141981E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.66141981E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.66141981E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.66141981E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.63829096E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.63829096E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.35415706E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.11456295E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.21043423E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     8.88728082E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.14971028E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     8.88728082E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.14971028E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.10452398E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.61175512E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.61175512E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.59962957E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.28212189E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.28212189E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.28211233E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.06074521E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.97036130E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.06074521E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.97036130E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.09059769E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     9.10502081E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.10502081E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.29363053E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.81999385E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.81999385E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.81999388E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.99439424E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.99439424E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     2.99439424E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     2.99439424E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     9.98120423E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     9.98120423E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     9.98120423E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     9.98120423E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     9.47562636E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     9.47562636E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.67289411E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.26931788E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.41749491E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.21967485E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40478403E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40478403E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.47068660E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.02795420E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.11768470E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.65665102E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.65665102E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.00554723E-03   # h decays
#          BR         NDA      ID1       ID2
     6.00775906E-01    2           5        -5   # BR(h -> b       bb     )
     6.48723837E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29649290E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87280766E-04    2           3        -3   # BR(h -> s       sb     )
     2.11621528E-02    2           4        -4   # BR(h -> c       cb     )
     6.88443302E-02    2          21        21   # BR(h -> g       g      )
     2.36421404E-03    2          22        22   # BR(h -> gam     gam    )
     1.58872414E-03    2          22        23   # BR(h -> Z       gam    )
     2.12896287E-01    2          24       -24   # BR(h -> W+      W-     )
     2.67790719E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43190156E+01   # H decays
#          BR         NDA      ID1       ID2
     3.47819138E-01    2           5        -5   # BR(H -> b       bb     )
     6.10393692E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15820425E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55278365E-04    2           3        -3   # BR(H -> s       sb     )
     7.16044227E-08    2           4        -4   # BR(H -> c       cb     )
     7.17625394E-03    2           6        -6   # BR(H -> t       tb     )
     8.83758387E-07    2          21        21   # BR(H -> g       g      )
     8.67864094E-11    2          22        22   # BR(H -> gam     gam    )
     1.84512755E-09    2          23        22   # BR(H -> Z       gam    )
     1.87265528E-06    2          24       -24   # BR(H -> W+      W-     )
     9.35679067E-07    2          23        23   # BR(H -> Z       Z      )
     6.94670524E-06    2          25        25   # BR(H -> h       h      )
     6.84431956E-27    2          36        36   # BR(H -> A       A      )
    -3.20861583E-21    2          23        36   # BR(H -> Z       A      )
     1.86729004E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63436498E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63436498E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.55883639E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.10799582E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.48988069E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.43004641E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.60000124E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.95345099E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.37112800E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.93635261E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.50124300E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.59552807E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.23310586E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.23310586E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43157084E+01   # A decays
#          BR         NDA      ID1       ID2
     3.47866195E-01    2           5        -5   # BR(A -> b       bb     )
     6.10434202E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15834579E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55336738E-04    2           3        -3   # BR(A -> s       sb     )
     7.20677748E-08    2           4        -4   # BR(A -> c       cb     )
     7.19005380E-03    2           6        -6   # BR(A -> t       tb     )
     1.47737437E-05    2          21        21   # BR(A -> g       g      )
     5.22368050E-08    2          22        22   # BR(A -> gam     gam    )
     1.62466831E-08    2          23        22   # BR(A -> Z       gam    )
     1.86886666E-06    2          23        25   # BR(A -> Z       h      )
     1.86998473E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63449172E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63449172E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.20922683E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.37968775E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.22222421E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.03928979E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.73866578E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.92653947E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.53848697E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.12150042E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.34075684E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.27272445E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.27272445E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42188390E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.54742847E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11679934E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16275039E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55035116E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22523921E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52071106E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.53550039E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.87428603E-06    2          24        25   # BR(H+ -> W+      h      )
     2.60895049E-14    2          24        36   # BR(H+ -> W+      A      )
     5.98080537E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.81641364E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.91418008E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63950754E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.89833584E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60979080E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.08774499E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.57549293E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
