evgenConfig.description = "MG W -> tau nu + 0,1,2,3j@LO with 500<HT ptll>100 MJJ>800,DPHI<2.5 "
evgenConfig.keywords = ["SM", "W", "jets", "LO" ]
evgenConfig.contact  = [ "schae@cern.ch"]

import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=500
ihtmax=-1
HTrange='highHT'
include('MC15JobOptions/MadGraphControl_Wjets_LO_Pythia8_25ns_VBF.py')
evgenConfig.maxeventsstrategy='IGNORE'
evgenConfig.minevents=1000

filtSeq.Expression = "QCDTruthJetFilter and VBFMjjIntervalFilter"

