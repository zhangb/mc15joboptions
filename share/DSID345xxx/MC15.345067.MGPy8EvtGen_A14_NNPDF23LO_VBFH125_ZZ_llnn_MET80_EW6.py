############################################
# MadGraph5, ZZjj, ZZ->llnn                #
############################################

#### Shower 
evgenConfig.description = 'qq->h->ZZjj, Z->ll Z->nn sample, off-shell Higgs included'
evgenConfig.keywords+=['VBF','ZZ', 'mH125']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact     = ['Monica Trovatelli <Monica.Trovatelli@cern.ch>', 'Lailin.Xu <Lailin.Xu@cern.ch>']
evgenConfig.inputfilecheck = 'MGPy8EvtGen_VBFH125_ZZ_llnn'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include('MC15JobOptions/Pythia8_LHEF.py')


# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]


# MET filter
include("MC15JobOptions/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 80*GeV
