include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.com>']
evgenConfig.description = "Event generation of gg > A > ZH (H is heavy scalar), H -> yy, Z -> vv"
evgenConfig.keywords = ["BSMHiggs", "diphoton"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'HiggsBSM:gg2A3 = on',
                            '36:m0 = 350.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:onMode = off',
                            '36:onIfMatch = 25 23',
                            '25:m0 = 250.0',
                            '25:doForceWidth = yes',
                            '25:mWidth = 0.01',
                            '25:onMode = off',
                            '25:onIfMatch = 22 22',
                            '23:onMode = off'
                            ]

genSeq.Pythia8.Commands.append("23:onIfAny = 12 14 16")

