#--------------------------------------------------------------
# Powheg+Pythia+Evtgen, ggH+mumu, H+jet production with NNLOPS and AZNLO tune, Hmumu mh=125 GeV"
#--------------------------------------------------------------
evgenConfig.process     = "ggH H->mumu"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+jet production with NNLOPS and AZNLO tune, Hmumu mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125" ]

evgenConfig.contact     = [ 'paul.daniel.thompson@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents   = 2000

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
#include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_Var1Down_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 13 13' ]
