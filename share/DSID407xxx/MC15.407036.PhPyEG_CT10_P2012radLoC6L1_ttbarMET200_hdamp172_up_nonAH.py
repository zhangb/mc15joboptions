#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, scale=2.0, Perugia 2012 radLo tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton','systematic']
evgenConfig.contact     = [ 'teng.jian.khoo@cern.ch' ]

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  PowhegConfig.mu_F         = 2.0
  PowhegConfig.mu_R         = 2.0
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 200.
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
 
  include('MC15JobOptions/PowhegPythia_Perugia2012radLo_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

  include('MC15JobOptions/MissingEtFilter.py')
  filtSeq.MissingEtFilter.METCut = 200*GeV

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')

