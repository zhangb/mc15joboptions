include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa eegamma + 0,1,2,3j@LO, isolationCut epsilon=0.2, with 140<pT_y<E_CMS."
evgenConfig.keywords = ["SM", "2electron", "photon" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "eegamma_LO_isoEps20_pty_140_E_CMS"

genSeq.Sherpa_i.RunCard = """
(run){
  % tags for process setup
  NJET:=3; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix;
}(run)

(processes){
  Process 93 93 -> 22 11 -11 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PT 22  140  E_CMS
  IsolationCut  22  0.1  2  0.20
  DeltaR  22  90  0.1 1000.0
  Mass  11  -11  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 24
