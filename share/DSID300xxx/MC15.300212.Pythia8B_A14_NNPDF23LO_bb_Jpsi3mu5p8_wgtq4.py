evgenConfig.description = "Inclusive bb->J/psi(mumu)+mu"
evgenConfig.keywords = ["inclusive","bottom","Jpsi","3muon","bbbar"]
evgenConfig.minevents = 10

include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']
genSeq.Pythia8B.QuarkPtCut = 5.0
genSeq.Pythia8B.AntiQuarkPtCut = 5.0
genSeq.Pythia8B.QuarkEtaCut = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->mumu
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.NHadronizationLoops = 10  # 1 (old value)


##g->bb weight
genSeq.Pythia8B.Commands += ['TimeShower:weightGluonToQuark = 4']



genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [5.8]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [3]

