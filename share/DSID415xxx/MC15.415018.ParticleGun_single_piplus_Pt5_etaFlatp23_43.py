evgenConfig.description = "Single piplus with flat eta-phi and fixed pT = 5 GeV"
evgenConfig.keywords = ["singleParticle", "pi+"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 211
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=5000.0, eta=[2.3, 4.3])

