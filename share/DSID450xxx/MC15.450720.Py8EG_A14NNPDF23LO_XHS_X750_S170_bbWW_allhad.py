include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.process     = "gg->X->SH->bb+jets"
evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.description = "Generation of gg > X > SH where S decays to W+W- which both decay hadronically and H decays to bb"
evgenConfig.keywords = ["BSMHiggs"]
evgenConfig.minevents = 10000

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'HiggsA3:parity = 1',
                            'Higgs:clipWings = off',
                            '36:m0 = 750.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 170.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes',
                            '25:onMode = off',
                            '25:onIfMatch = 5 -5',
                            '35:onMode = off',
                            '35:onIfMatch = 24 -24',
                            '24:onMode = off',
                            '24:onIfAny = 1 2 3 4 5'
                            ]

