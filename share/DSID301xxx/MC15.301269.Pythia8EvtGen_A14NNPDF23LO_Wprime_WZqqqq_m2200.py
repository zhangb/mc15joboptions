# W' -> WZ -> qqqq                                                                                                                                                                
# Wprime Mass (in GeV)                                                                                                                                                                                     
M_Wprime = 2200.0

evgenConfig.contact = ["Alex Martyniuk, martyniu@cern.ch"]
evgenConfig.description = "Wprime->WZ->qqqq 2200GeV with NNPDF23LO PDF and A14 tunee"
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>Wprime>WZ>qqqq"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]# Allow Wprime                                                                                                                               
genSeq.Pythia8.Commands += ["Wprime:coup2WZ = 1."]    # Wprime Coupling to WZ                                                                                                                              
genSeq.Pythia8.Commands += ["34:m0 = 2200.0"] # Wprime mass
genSeq.Pythia8.Commands += ["34:onMode = off"]# Turn off  decays                                                                                                                                           
genSeq.Pythia8.Commands += ["34:onIfAll = 23 24"]# Turn on ->WZ                                                                                                                                            
genSeq.Pythia8.Commands += ["24:onMode = off"]# Turn off all Z decays                                                                                                                                      
genSeq.Pythia8.Commands += ["24:onIfAny = 1 2 3 4 5"]# Turn on hadronic Z                                                                                                                                  
genSeq.Pythia8.Commands += ["23:onMode = off"]# Turn off all W decays                                                                                                                                      
genSeq.Pythia8.Commands += ["23:onIfAny = 1 2 3 4 5"]# Turn on hadronic W                                                                                                                                  
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on"]
genSeq.Pythia8.Commands += ["Next:numberShowEvent = 5"]