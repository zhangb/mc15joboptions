######################################################################
# Graviton to mu mu decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 750. GeV, kappaMG = 1.084."
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = [" sebastien.rettie@cern.ch"]
evgenConfig.process = "RS Graviton -> mu mu"

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
"ExtraDimensionsG*:gg2G* = on",
"ExtraDimensionsG*:ffbar2G* = on",
"5100039:m0 = 750.",
"5100039:onMode = off",
"5100039:onIfAny = 13",
"ExtraDimensionsG*:kappaMG = 1.084",
"5100039:mMin = 50.",
"PhaseSpace:mHatMin = 50.",
]

genSeq.Pythia8.UserModes += ["GravFlat:EnergyMode = 13"]
