#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.64000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416868E+01   # W+
        25     1.25000000E+02   # h
        35     2.00400170E+03   # H
        36     2.00000000E+03   # A
        37     2.00151785E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994925E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994925E+03   # ~c_R
   1000005     4.99941313E+03   # ~b_1
   2000005     5.00074652E+03   # ~b_2
   1000006     4.98547937E+03   # ~t_1
   2000006     5.01893443E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007613E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007613E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99963923E+03   # ~tau_1
   2000015     5.00051965E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.60000000E+03   # ~g
   1000022     1.59914941E+03   # ~chi_10
   1000023    -1.65007489E+03   # ~chi_20
   1000025     1.68643100E+03   # ~chi_30
   1000035     3.00449448E+03   # ~chi_40
   1000024     1.64561339E+03   # ~chi_1+
   1000037     3.00449003E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.29212192E-01   # N_11
  1  2    -3.77918662E-02   # N_12
  1  3     4.84635794E-01   # N_13
  1  4    -4.81611358E-01   # N_14
  2  1     2.65348974E-03   # N_21
  2  2    -3.33657715E-03   # N_22
  2  3    -7.07020116E-01   # N_23
  2  4    -7.07180586E-01   # N_24
  3  1     6.84279997E-01   # N_31
  3  2     4.29859895E-02   # N_32
  3  3    -5.13613971E-01   # N_33
  3  4     5.15862171E-01   # N_34
  4  1     1.85035331E-03   # N_41
  4  2    -9.98355071E-01   # N_42
  4  3    -3.80971358E-02   # N_43
  4  4     4.28058130E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.38208163E-02   # U_11
  1  2     9.98550610E-01   # U_12
  2  1     9.98550610E-01   # U_21
  2  2     5.38208163E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.04793428E-02   # V_11
  1  2     9.98169449E-01   # V_12
  2  1     9.98169449E-01   # V_21
  2  2     6.04793428E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07706377E-01   # cos(theta_t)
  1  2     7.06506677E-01   # sin(theta_t)
  2  1    -7.06506677E-01   # -sin(theta_t)
  2  2     7.07706377E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.77993309E-01   # cos(theta_b)
  1  2     7.35068074E-01   # sin(theta_b)
  2  1    -7.35068074E-01   # -sin(theta_b)
  2  2     6.77993309E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04680369E-01   # cos(theta_tau)
  1  2     7.09524896E-01   # sin(theta_tau)
  2  1    -7.09524896E-01   # -sin(theta_tau)
  2  2     7.04680369E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200471E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52258679E+02   # vev(Q)              
         4     3.16954274E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52737399E-01   # gprime(Q) DRbar
     2     6.26887239E-01   # g(Q) DRbar
     3     1.07628033E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02506044E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71364740E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79759578E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.64000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.54603485E+03   # M^2_Hd              
        22    -7.82581835E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36920132E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.02286694E-03   # gluino decays
#          BR         NDA      ID1       ID2
     9.19591380E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.75123043E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.55309436E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.70897345E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.47904445E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.74142721E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.67603308E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.89967540E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.09745809E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.70897345E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.47904445E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.74142721E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.67603308E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.89967540E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.09745809E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.77776352E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.68041513E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.83261508E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     9.54051960E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.51540887E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.00758995E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.92857418E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.92857418E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.92857418E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.92857418E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.00490335E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.00490335E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.51026868E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.67322230E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.15893441E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.69323527E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.29477885E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.23008773E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.55505816E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.88934840E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.38810388E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.93686179E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16642646E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.53285604E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.93235713E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.09458988E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.82441871E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.81633429E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.79435491E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.69270125E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.71824914E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.62542457E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.86502818E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.35310824E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.77358131E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.99887772E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.90343763E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.12051476E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.19362206E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.31751264E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.29056003E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.70816243E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.63602892E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.53360478E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67230043E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.88239581E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.20050478E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.59434669E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.60102233E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.54453762E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.20505232E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.57801493E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.51887791E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.98778249E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.89853774E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.56466630E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.80177381E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44475024E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67237928E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.76284092E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.31353571E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.43961380E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.60792633E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.18294041E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.21308711E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.57863456E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.45568087E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.79391003E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.01697053E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.69017767E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.55695353E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85515785E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67230043E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.88239581E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.20050478E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.59434669E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.60102233E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.54453762E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.20505232E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.57801493E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.51887791E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.98778249E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.89853774E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.56466630E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.80177381E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44475024E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67237928E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.76284092E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.31353571E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.43961380E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.60792633E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.18294041E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.21308711E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.57863456E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.45568087E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.79391003E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.01697053E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.69017767E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.55695353E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85515785E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.89374239E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.55394762E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.82277798E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.71903449E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.74264651E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.11133422E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49892371E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.97114504E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.38149644E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.02105703E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.61841580E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.75525060E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.89374239E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.55394762E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.82277798E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.71903449E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.74264651E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.11133422E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49892371E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.97114504E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.38149644E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.02105703E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.61841580E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.75525060E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43705164E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.69621202E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.39330857E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.41171002E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61163827E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.32578730E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.23131275E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.47575754E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43585201E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.55962987E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.15839734E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48453079E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64581412E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.13933435E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.29972748E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.89389887E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09289213E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.25088915E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.20601233E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75353061E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.92830963E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.49356784E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.89389887E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09289213E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.25088915E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.20601233E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75353061E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.92830963E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.49356784E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.89645027E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09192943E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.24978728E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.20054564E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75110511E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.80361248E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48874979E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.41825226E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.86837954E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.17843777E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.16393808E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.05948048E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.05945332E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.05185240E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.68720506E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53480023E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08123507E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46211629E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.39016347E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53160881E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.61124507E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.14532372E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00381582E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88709250E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09091686E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.80733634E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.55631353E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.56452289E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.48331356E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.66433983E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.14037235E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.47666478E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.13335977E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.47647800E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35775425E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.37271238E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.37260143E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.34141300E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.73566311E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.73566311E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.73566311E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.59771534E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.59771534E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.21991756E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.21991756E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.19923853E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.19923853E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.19588759E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.19588759E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.59609403E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.59609403E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.96961114E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.20458731E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.53583259E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.03970536E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.70974850E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.94728665E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.48906842E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.78257416E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.29530132E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.79765706E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.65819927E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.15670820E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.15667212E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.54490908E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.67456890E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.67456890E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.67456890E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.00694672E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.59879233E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.98548555E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.59817642E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.21497499E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.93566715E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.93534578E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.84451727E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.18540943E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.18540943E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.18540943E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.29357895E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.29357895E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.28590022E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.28590022E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.31192374E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.31192374E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.31177973E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.31177973E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.27155756E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.27155756E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.68718747E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.90525848E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52181105E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.65614396E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46644949E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46644949E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10586444E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.36777562E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42242023E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82018848E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.79006257E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     7.82975015E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.11691668E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.32585508E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08797813E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17995215E-01    2           5        -5   # BR(h -> b       bb     )
     6.37125641E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25519012E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77895180E-04    2           3        -3   # BR(h -> s       sb     )
     2.06175655E-02    2           4        -4   # BR(h -> c       cb     )
     6.69449242E-02    2          21        21   # BR(h -> g       g      )
     2.30200882E-03    2          22        22   # BR(h -> gam     gam    )
     1.53667319E-03    2          22        23   # BR(h -> Z       gam    )
     2.00584844E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56027914E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78095973E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46633054E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430507E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71223591E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548520E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668726E-05    2           4        -4   # BR(H -> c       cb     )
     9.96070251E-01    2           6        -6   # BR(H -> t       tb     )
     7.97662667E-04    2          21        21   # BR(H -> g       g      )
     2.71455998E-06    2          22        22   # BR(H -> gam     gam    )
     1.16021987E-06    2          23        22   # BR(H -> Z       gam    )
     3.34155725E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66622171E-04    2          23        23   # BR(H -> Z       Z      )
     9.02619446E-04    2          25        25   # BR(H -> h       h      )
     7.20251838E-24    2          36        36   # BR(H -> A       A      )
     2.92864081E-11    2          23        36   # BR(H -> Z       A      )
     6.57415998E-12    2          24       -37   # BR(H -> W+      H-     )
     6.57415998E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380349E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46925725E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898946E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62270880E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677967E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181488E-06    2           4        -4   # BR(A -> c       cb     )
     9.97001001E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680388E-04    2          21        21   # BR(A -> g       g      )
     3.14917782E-06    2          22        22   # BR(A -> gam     gam    )
     1.35280668E-06    2          23        22   # BR(A -> Z       gam    )
     3.25699798E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471572E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35372876E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239061E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150117E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49639774E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697214E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732239E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402775E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33808407E-04    2          24        25   # BR(H+ -> W+      h      )
     2.82930087E-13    2          24        36   # BR(H+ -> W+      A      )
