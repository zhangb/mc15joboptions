#--------------------------------------------------------------
# Powheg DMV setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_DMV_Common.py')
PowhegConfig.vdecaymode = -2 #axial-vector mediator
PowhegConfig.DM_mass = 500
PowhegConfig.V_mass  = 1500
PowhegConfig.V_width = 60.649014
PowhegConfig.gDM = 1
PowhegConfig.gSM = 0.25
PowhegConfig.runningwidth = 0
PowhegConfig.bornktmin = 150 #Intended for analyses with MET>300 GeV
PowhegConfig.bornsuppfact = 1000 #Ensure sufficient statistics at high MET
PowhegConfig.PDF = range(260000, 260101) # NNPDF30_nlo_as_0118
#The mass_low and mass_high parameters are computed internally
PowhegConfig.mass_low = -1
PowhegConfig.mass_high = -1
#Keep negative weights to make sure no events are discarded
PowhegConfig.withnegweights = 1
#The paramters below are set following the recommendation of the authors.
#It is a compromise between fast production with too many events with negative weights
#and small amount of events with negative weights at the cost of large running time.
PowhegConfig.foldsci = 2 
PowhegConfig.foldy = 2
PowhegConfig.foldphi = 1
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with A14 NNPDF23LO 
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('MC15JobOptions/Pythia8_Powheg.py')

# id:all = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
genSeq.Pythia8.Commands += [ '1000022:all = X Xbar 2 0 0 %f 0.0 0.0 0.0 0.0'%PowhegConfig.DM_mass,
                             '1000022:isVisible = false' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 DMV axial-vector mediator (DM mass = 500 GeV, mediator mass = 1500 GeV, gSM = 0.25, gDM = 1) for mono-jet analysis (suitable for MET > 300 GeV)'
evgenConfig.keywords    = [ 'BSM', 'WIMP', 'invisible', 'exotic' ]
evgenConfig.contact     = [ 'David Salek <david.salek@cern.ch>' ]
evgenConfig.process     = 'pp->A(XXbar)j'
