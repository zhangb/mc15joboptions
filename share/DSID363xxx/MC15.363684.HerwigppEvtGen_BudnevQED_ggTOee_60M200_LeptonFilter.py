evgenConfig.description = "gammagamma->ee with Budnev parameterization, 60<M<200 GeV central lepton filter pt>9 GeV"
evgenConfig.keywords = ["QED", "2lepton", "exclusive", "diphoton"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]

include("MC15JobOptions/Herwigpp_QED_Common.py")
from Herwigpp_i import config as hw

cmds = """\

# Cuts
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*MeV
set /Herwig/Partons/BudnevPDF:Q2Max 4
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0
set QCDCuts:MHatMin 60*GeV
set QCDCuts:MHatMax 200*GeV
set LeptonKtCut:MinKT 2*GeV

# Selected the hard process

cd /Herwig/MatrixElements
insert SimpleQCD:MatrixElements 0 /Herwig/MatrixElements/MEgg2ff
set /Herwig/MatrixElements/MEgg2ff:Process Electron 
"""

genSeq.Herwigpp.Commands += cmds.splitlines()

include("MC15JobOptions/Herwigpp_EvtGen.py")

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 9000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

evgenConfig.minevents = 5000

# To avoid warning from displaced vertices, bugfix needed in herwig++
testSeq.TestHepMC.MaxTransVtxDisp = 1000000
testSeq.TestHepMC.MaxVtxDisp      = 1000000000
