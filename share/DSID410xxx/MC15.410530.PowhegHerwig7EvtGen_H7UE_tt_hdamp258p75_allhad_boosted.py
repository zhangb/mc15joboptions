#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 ttbar production, with Powheg hdamp equal 1.5  * mTop, H7UE tune, all-hadronic boosed, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'dominic@hirschbuehl.de', 'daniel.rauch@cern.ch' ]
evgenConfig.generators += ['Powheg']

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22   # All hadronic, This topdecaymode works in 19.2.5.5 but must be adjusted to 00022 in newer caches as described in https://its.cern.ch/jira/browse/AGENE-1316  
PowhegConfig.hdamp   = 258.75 
PowhegConfig.PDF     = 260000
PowhegConfig.mu_F    = 1.0
PowhegConfig.mu_R    = 1.0

PowhegConfig.bornsuppfact = 500
PowhegConfig.foldx = 2
PowhegConfig.foldy = 2
PowhegConfig.foldphi = 2

PowhegConfig.nEvents   *= 1.1 #Safety factor
PowhegConfig.generate()
#--------------------------------------------------------------
# Herwig7 (H7-UE-MMHT) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py')

from Herwig7_i import config as hw
genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()
