#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.beam_1_type = 1   # p
PowhegConfig.beam_2_type = 2   # n
PowhegConfig.vdecaymode = 2    # mumu

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 1.2 # increase number of generated events by 20%
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("ParentChildFilter")

filtSeq.ParentChildFilter.PDGParent = [23]  # Select Z
filtSeq.ParentChildFilter.PDGChild = [-13, 13]   # Select mumu in Z decay

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 pn -> Z->mumu production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Andrzej Olszewski <andrzej.olszewski@ifj.edu.pl>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2muon' ]
evgenConfig.minevents = 1000
