mchi = 50
mphi = 800
gx = 2.2231301564
filter_string = "T"
evt_multiplier = 15
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
