#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12853791E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03970676E+01   # W+
        25     1.24680089E+02   # h
        35     3.00019164E+03   # H
        36     2.99999959E+03   # A
        37     3.00095579E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02673785E+03   # ~d_L
   2000001     3.02144300E+03   # ~d_R
   1000002     3.02581509E+03   # ~u_L
   2000002     3.02329595E+03   # ~u_R
   1000003     3.02673785E+03   # ~s_L
   2000003     3.02144300E+03   # ~s_R
   1000004     3.02581509E+03   # ~c_L
   2000004     3.02329595E+03   # ~c_R
   1000005     6.24361914E+02   # ~b_1
   2000005     3.02089846E+03   # ~b_2
   1000006     6.21816043E+02   # ~t_1
   2000006     3.00716653E+03   # ~t_2
   1000011     3.00671346E+03   # ~e_L
   2000011     3.00129323E+03   # ~e_R
   1000012     3.00531255E+03   # ~nu_eL
   1000013     3.00671346E+03   # ~mu_L
   2000013     3.00129323E+03   # ~mu_R
   1000014     3.00531255E+03   # ~nu_muL
   1000015     2.98596661E+03   # ~tau_1
   2000015     3.02214267E+03   # ~tau_2
   1000016     3.00537144E+03   # ~nu_tauL
   1000021     2.33272590E+03   # ~g
   1000022     1.00587714E+02   # ~chi_10
   1000023     2.14930389E+02   # ~chi_20
   1000025    -3.00116158E+03   # ~chi_30
   1000035     3.00162903E+03   # ~chi_40
   1000024     2.15089917E+02   # ~chi_1+
   1000037     3.00232053E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888471E-01   # N_11
  1  2    -1.53864131E-03   # N_12
  1  3     1.48041626E-02   # N_13
  1  4    -1.23070971E-03   # N_14
  2  1     1.93008142E-03   # N_21
  2  2     9.99649961E-01   # N_22
  2  3    -2.62100152E-02   # N_23
  2  4     3.04386405E-03   # N_24
  3  1    -9.56959691E-03   # N_31
  3  2     1.63978386E-02   # N_32
  3  3     7.06830240E-01   # N_33
  3  4     7.07128380E-01   # N_34
  4  1    -1.13023402E-02   # N_41
  4  2     2.07050402E-02   # N_42
  4  3     7.06742445E-01   # N_43
  4  4    -7.07077559E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99311759E-01   # U_11
  1  2    -3.70945925E-02   # U_12
  2  1     3.70945925E-02   # U_21
  2  2     9.99311759E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990715E-01   # V_11
  1  2    -4.30917174E-03   # V_12
  2  1     4.30917174E-03   # V_21
  2  2     9.99990715E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98881253E-01   # cos(theta_t)
  1  2    -4.72889248E-02   # sin(theta_t)
  2  1     4.72889248E-02   # -sin(theta_t)
  2  2     9.98881253E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99920682E-01   # cos(theta_b)
  1  2     1.25948286E-02   # sin(theta_b)
  2  1    -1.25948286E-02   # -sin(theta_b)
  2  2     9.99920682E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06898533E-01   # cos(theta_tau)
  1  2     7.07314968E-01   # sin(theta_tau)
  2  1    -7.07314968E-01   # -sin(theta_tau)
  2  2     7.06898533E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01897944E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28537911E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44597295E+02   # higgs               
         4     6.90439919E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.28537911E+03  # The gauge couplings
     1     3.61651874E-01   # gprime(Q) DRbar
     2     6.39686809E-01   # g(Q) DRbar
     3     1.02981641E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28537911E+03  # The trilinear couplings
  1  1     1.53124588E-06   # A_u(Q) DRbar
  2  2     1.53126978E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28537911E+03  # The trilinear couplings
  1  1     3.79451298E-07   # A_d(Q) DRbar
  2  2     3.79528371E-07   # A_s(Q) DRbar
  3  3     8.82514462E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28537911E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     8.35332411E-08   # A_mu(Q) DRbar
  3  3     8.45358635E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28537911E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55394711E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28537911E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11369876E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28537911E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05086560E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28537911E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.69162199E+04   # M^2_Hd              
        22    -9.11216673E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42053862E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.06188931E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48254136E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48254136E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51745864E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51745864E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.85660123E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.27907636E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.87283874E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.99925362E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05157019E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.23535067E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.78537930E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.58952522E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.85799233E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.21638720E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.61592767E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51933695E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.01643235E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.55311252E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.64638308E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.71959965E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.11576204E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24251817E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.90520282E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.90694189E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.60199407E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.46061224E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -4.04513399E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28864687E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.90741393E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.21803998E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.08175330E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07872567E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.76841802E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64004320E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.09522621E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.70926655E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27989040E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.73877229E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.02238192E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19847507E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58047668E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.84794450E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.66031835E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.89798081E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41951739E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08380744E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.95754254E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63700613E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.82790026E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.77563082E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27413436E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.38741291E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02928223E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68579018E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.49794358E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.66427552E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.79689057E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.17260640E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55020396E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07872567E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.76841802E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64004320E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.09522621E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.70926655E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27989040E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.73877229E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.02238192E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19847507E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58047668E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.84794450E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.66031835E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.89798081E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41951739E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08380744E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.95754254E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63700613E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.82790026E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.77563082E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27413436E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.38741291E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02928223E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68579018E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.49794358E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.66427552E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.79689057E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.17260640E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55020396E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02063279E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.65255521E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01735433E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.00255032E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.12281444E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01739007E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.14222448E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55803982E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996304E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.69621151E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.06520157E-13    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     4.02063279E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.65255521E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01735433E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.00255032E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.12281444E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01739007E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.14222448E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55803982E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996304E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.69621151E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.06520157E-13    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.81838549E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45149498E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18707493E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36143009E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75839232E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53287973E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16010691E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.55369777E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.85443439E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30673189E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.73798513E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02097439E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.75282484E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00252484E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.10257498E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.34042605E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02219265E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.47494894E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02097439E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.75282484E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00252484E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.10257498E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.34042605E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02219265E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.47494894E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02139142E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.75200538E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00227351E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.13391943E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.38343089E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02252381E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.12321296E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.36993171E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.83480499E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.63288161E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.28764496E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.55645474E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.78332487E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.09519419E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.63172505E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.98035914E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.71277337E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.86564636E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.26064237E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.14485718E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.59448608E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.59448608E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.26177995E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.64119751E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.66919971E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.66919971E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.22702024E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.22702024E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.90563925E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.90563925E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.77351703E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.13207971E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.62869188E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.66543533E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.66543533E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.30817752E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.34719957E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.64832571E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.64832571E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.25259047E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.25259047E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.69233411E-13    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.69233411E-13    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     5.69233411E-13    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.69233411E-13    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     7.86217505E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.86217505E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.85825393E-03   # h decays
#          BR         NDA      ID1       ID2
     6.81504616E-01    2           5        -5   # BR(h -> b       bb     )
     5.36096326E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.89780867E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.02976582E-04    2           3        -3   # BR(h -> s       sb     )
     1.73896045E-02    2           4        -4   # BR(h -> c       cb     )
     5.66035795E-02    2          21        21   # BR(h -> g       g      )
     1.91461756E-03    2          22        22   # BR(h -> gam     gam    )
     1.24963513E-03    2          22        23   # BR(h -> Z       gam    )
     1.66440006E-01    2          24       -24   # BR(h -> W+      W-     )
     2.06955515E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39407632E+01   # H decays
#          BR         NDA      ID1       ID2
     7.42320771E-01    2           5        -5   # BR(H -> b       bb     )
     1.78384186E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30723854E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.74526980E-04    2           3        -3   # BR(H -> s       sb     )
     2.18623777E-07    2           4        -4   # BR(H -> c       cb     )
     2.18091393E-02    2           6        -6   # BR(H -> t       tb     )
     3.30059816E-05    2          21        21   # BR(H -> g       g      )
     2.97850519E-08    2          22        22   # BR(H -> gam     gam    )
     8.36391861E-09    2          23        22   # BR(H -> Z       gam    )
     3.38922125E-05    2          24       -24   # BR(H -> W+      W-     )
     1.69252061E-05    2          23        23   # BR(H -> Z       Z      )
     8.72988723E-05    2          25        25   # BR(H -> h       h      )
    -2.19485306E-24    2          36        36   # BR(H -> A       A      )
     2.02270650E-17    2          23        36   # BR(H -> Z       A      )
     2.40103243E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13241820E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19546158E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.37482372E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.14558796E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.17624586E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32254296E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82533376E-01    2           5        -5   # BR(A -> b       bb     )
     1.88025173E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.64811149E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.16526153E-04    2           3        -3   # BR(A -> s       sb     )
     2.30412106E-07    2           4        -4   # BR(A -> c       cb     )
     2.29724361E-02    2           6        -6   # BR(A -> t       tb     )
     6.76516358E-05    2          21        21   # BR(A -> g       g      )
     3.35215278E-08    2          22        22   # BR(A -> gam     gam    )
     6.65965033E-08    2          23        22   # BR(A -> Z       gam    )
     3.55878363E-05    2          23        25   # BR(A -> Z       h      )
     2.64475312E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21915952E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31677984E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.00658867E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.92985439E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08763093E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.50507830E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85734584E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.96082696E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.20526607E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07089061E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.08821911E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.74739499E-05    2          24        25   # BR(H+ -> W+      h      )
     1.05923536E-13    2          24        36   # BR(H+ -> W+      A      )
     2.09370745E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.79465751E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.54258004E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
