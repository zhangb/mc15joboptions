model="LightVector"
mDM1 = 90.
mDM2 = 360.
mZp = 180.
mHD = 125.
widthZp = 7.161968e-01
widthN2 = 1.182528e+01
filteff = 8.976661e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
