#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13385297E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04013883E+01   # W+
        25     1.25724193E+02   # h
        35     3.00014965E+03   # H
        36     2.99999993E+03   # A
        37     3.00109660E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02871463E+03   # ~d_L
   2000001     3.02346621E+03   # ~d_R
   1000002     3.02780242E+03   # ~u_L
   2000002     3.02608155E+03   # ~u_R
   1000003     3.02871463E+03   # ~s_L
   2000003     3.02346621E+03   # ~s_R
   1000004     3.02780242E+03   # ~c_L
   2000004     3.02608155E+03   # ~c_R
   1000005     6.88392486E+02   # ~b_1
   2000005     3.02351926E+03   # ~b_2
   1000006     6.85166215E+02   # ~t_1
   2000006     3.02135682E+03   # ~t_2
   1000011     3.00692189E+03   # ~e_L
   2000011     3.00050292E+03   # ~e_R
   1000012     3.00553344E+03   # ~nu_eL
   1000013     3.00692189E+03   # ~mu_L
   2000013     3.00050292E+03   # ~mu_R
   1000014     3.00553344E+03   # ~nu_muL
   1000015     2.98622885E+03   # ~tau_1
   2000015     3.02201225E+03   # ~tau_2
   1000016     3.00582700E+03   # ~nu_tauL
   1000021     2.33558985E+03   # ~g
   1000022     2.52542562E+02   # ~chi_10
   1000023     5.29345443E+02   # ~chi_20
   1000025    -3.00324026E+03   # ~chi_30
   1000035     3.00340354E+03   # ~chi_40
   1000024     5.29507496E+02   # ~chi_1+
   1000037     3.00426100E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891104E-01   # N_11
  1  2     7.59707725E-05   # N_12
  1  3    -1.47489156E-02   # N_13
  1  4    -4.93246340E-04   # N_14
  2  1     3.15403816E-04   # N_21
  2  2     9.99645848E-01   # N_22
  2  3     2.64277609E-02   # N_23
  2  4     3.10701346E-03   # N_24
  3  1    -1.00786636E-02   # N_31
  3  2     1.64921593E-02   # N_32
  3  3    -7.06837952E-01   # N_33
  3  4     7.07111405E-01   # N_34
  4  1     1.07750004E-02   # N_41
  4  2    -2.08849819E-02   # N_42
  4  3     7.06727778E-01   # N_43
  4  4     7.07095159E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99301569E-01   # U_11
  1  2     3.73681008E-02   # U_12
  2  1    -3.73681008E-02   # U_21
  2  2     9.99301569E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990355E-01   # V_11
  1  2    -4.39195646E-03   # V_12
  2  1    -4.39195646E-03   # V_21
  2  2    -9.99990355E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98614301E-01   # cos(theta_t)
  1  2    -5.26258286E-02   # sin(theta_t)
  2  1     5.26258286E-02   # -sin(theta_t)
  2  2     9.98614301E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99724763E-01   # cos(theta_b)
  1  2    -2.34605679E-02   # sin(theta_b)
  2  1     2.34605679E-02   # -sin(theta_b)
  2  2     9.99724763E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06933834E-01   # cos(theta_tau)
  1  2     7.07279686E-01   # sin(theta_tau)
  2  1    -7.07279686E-01   # -sin(theta_tau)
  2  2    -7.06933834E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00126737E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33852975E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44294883E+02   # higgs               
         4     1.11386237E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.33852975E+03  # The gauge couplings
     1     3.61770384E-01   # gprime(Q) DRbar
     2     6.37501160E-01   # g(Q) DRbar
     3     1.02889380E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33852975E+03  # The trilinear couplings
  1  1     1.68913533E-06   # A_u(Q) DRbar
  2  2     1.68916075E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33852975E+03  # The trilinear couplings
  1  1     6.01978970E-07   # A_d(Q) DRbar
  2  2     6.02049902E-07   # A_s(Q) DRbar
  3  3     1.23344336E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33852975E+03  # The trilinear couplings
  1  1     2.70464714E-07   # A_e(Q) DRbar
  2  2     2.70478024E-07   # A_mu(Q) DRbar
  3  3     2.74226503E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33852975E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55674215E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33852975E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.91835083E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33852975E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01940279E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33852975E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.21529495E+04   # M^2_Hd              
        22    -9.10907797E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41065810E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.83801336E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48027331E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48027331E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51972669E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51972669E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.00935651E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.15870056E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.38412994E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19834607E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.44232715E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.87346712E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.77893014E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.97714535E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.90098202E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71441368E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62889623E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.20232525E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.70162160E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.38486924E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.61513076E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.55773822E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.95128515E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.00000397E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59103520E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.52557794E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.06042685E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.32518386E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.53161953E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.06854620E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.55766857E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88204180E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.98131917E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59433094E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.10855521E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.82700717E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.19035945E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.85083058E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.15549611E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19186791E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56897975E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.49064854E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.32141180E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.87066123E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43102001E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88719830E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.96914293E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59330925E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.89168638E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.89996387E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.18468459E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.44126150E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.16231282E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67740454E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46711677E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.24374417E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.72613735E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.09423814E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55328826E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88204180E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.98131917E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59433094E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.10855521E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.82700717E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.19035945E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.85083058E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.15549611E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19186791E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56897975E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.49064854E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.32141180E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.87066123E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43102001E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88719830E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.96914293E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59330925E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.89168638E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.89996387E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.18468459E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.44126150E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.16231282E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67740454E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46711677E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.24374417E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.72613735E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.09423814E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55328826E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80651682E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01399000E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99750599E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.21957434E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.81087201E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98850398E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.79108901E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54010613E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.47414838E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.80651682E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01399000E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99750599E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.21957434E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.81087201E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98850398E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.79108901E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54010613E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.47414838E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.69929547E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56620212E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14633167E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28746621E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64548178E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.64789293E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11905850E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.28955685E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.83161911E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23281803E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.93332754E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80667074E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01292488E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99367881E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.66404254E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.67068396E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99339630E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     8.82453865E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80667074E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01292488E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99367881E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.66404254E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.67068396E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99339630E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     8.82453865E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80741605E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01282833E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99342257E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.66036503E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.04454933E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99374909E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     4.64168393E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.57799188E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.41259429E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.63568742E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.08271112E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.84868147E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.88551330E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.69682573E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.08609159E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.63455032E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.04248208E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.95751792E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.60191911E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.02970755E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.59081747E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.68072471E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.68072471E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.28495869E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.27331742E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34569414E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34569414E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.95108197E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.95108197E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.74647400E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.74647400E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.74647400E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.74647400E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.27557258E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.27557258E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.50436557E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.10647303E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.26564235E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.74054459E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.74054459E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07631789E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.83701750E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31921129E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31921129E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.02246191E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.02246191E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.56054163E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.56054163E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.56054163E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.56054163E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     6.80342539E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.80342539E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.70588632E-03   # h decays
#          BR         NDA      ID1       ID2
     5.51006461E-01    2           5        -5   # BR(h -> b       bb     )
     7.03709387E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.49111749E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.28148883E-04    2           3        -3   # BR(h -> s       sb     )
     2.29509411E-02    2           4        -4   # BR(h -> c       cb     )
     7.53195305E-02    2          21        21   # BR(h -> g       g      )
     2.60171633E-03    2          22        22   # BR(h -> gam     gam    )
     1.79808603E-03    2          22        23   # BR(h -> Z       gam    )
     2.44275860E-01    2          24       -24   # BR(h -> W+      W-     )
     3.08992054E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.94870757E+01   # H decays
#          BR         NDA      ID1       ID2
     9.05710271E-01    2           5        -5   # BR(H -> b       bb     )
     6.29781016E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.22675517E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.73445430E-04    2           3        -3   # BR(H -> s       sb     )
     7.66400140E-08    2           4        -4   # BR(H -> c       cb     )
     7.64533599E-03    2           6        -6   # BR(H -> t       tb     )
     5.73641478E-06    2          21        21   # BR(H -> g       g      )
     4.80970690E-08    2          22        22   # BR(H -> gam     gam    )
     3.13496167E-09    2          23        22   # BR(H -> Z       gam    )
     6.58322340E-07    2          24       -24   # BR(H -> W+      W-     )
     3.28755347E-07    2          23        23   # BR(H -> Z       Z      )
     5.08568312E-06    2          25        25   # BR(H -> h       h      )
    -9.25116582E-25    2          36        36   # BR(H -> A       A      )
     2.05367689E-18    2          23        36   # BR(H -> Z       A      )
     7.44169870E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.84686902E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.71883188E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.42072659E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.17563637E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.27508948E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.86357391E+01   # A decays
#          BR         NDA      ID1       ID2
     9.25670096E-01    2           5        -5   # BR(A -> b       bb     )
     6.43630487E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.27572041E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.79505723E-04    2           3        -3   # BR(A -> s       sb     )
     7.88725531E-08    2           4        -4   # BR(A -> c       cb     )
     7.86371306E-03    2           6        -6   # BR(A -> t       tb     )
     2.31578825E-05    2          21        21   # BR(A -> g       g      )
     6.18760611E-08    2          22        22   # BR(A -> gam     gam    )
     2.28136813E-08    2          23        22   # BR(A -> Z       gam    )
     6.70208385E-07    2          23        25   # BR(A -> Z       h      )
     8.48597232E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.01914571E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.24021867E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.59262184E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.22169075E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47585691E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.89248039E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08343734E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.44547266E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22438256E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51894864E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.28132806E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.14413538E-07    2          24        25   # BR(H+ -> W+      h      )
     4.94376073E-14    2          24        36   # BR(H+ -> W+      A      )
     4.63821192E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.31749845E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05201693E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
