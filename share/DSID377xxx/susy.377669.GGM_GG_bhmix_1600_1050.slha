#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05405943E+01   # W+
        25     1.25000000E+02   # h
        35     2.00410235E+03   # H
        36     2.00000000E+03   # A
        37     2.00157738E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013294E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989241E+03   # ~u_L
   2000002     4.99994929E+03   # ~u_R
   1000003     5.00013294E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989241E+03   # ~c_L
   2000004     4.99994929E+03   # ~c_R
   1000005     4.99965306E+03   # ~b_1
   2000005     5.00050664E+03   # ~b_2
   1000006     4.99157908E+03   # ~t_1
   2000006     5.01286753E+03   # ~t_2
   1000011     5.00008223E+03   # ~e_L
   2000011     5.00007607E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008223E+03   # ~mu_L
   2000013     5.00007607E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99979938E+03   # ~tau_1
   2000015     5.00035953E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.60000000E+03   # ~g
   1000022     1.00239682E+03   # ~chi_10
   1000023    -1.05009580E+03   # ~chi_20
   1000025     1.08955937E+03   # ~chi_30
   1000035     3.00313962E+03   # ~chi_40
   1000024     1.04698060E+03   # ~chi_1+
   1000037     3.00313817E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.14711767E-01   # N_11
  1  2    -2.71046420E-02   # N_12
  1  3     4.96479599E-01   # N_13
  1  4    -4.91894740E-01   # N_14
  2  1     4.16757719E-03   # N_21
  2  2    -3.83355889E-03   # N_22
  2  3    -7.06934142E-01   # N_23
  2  4    -7.07256710E-01   # N_24
  3  1     6.99406112E-01   # N_31
  3  2     2.90077989E-02   # N_32
  3  3    -5.03099350E-01   # N_33
  3  4     5.06833979E-01   # N_34
  4  1     9.00965566E-04   # N_41
  4  2    -9.99204278E-01   # N_42
  4  3    -2.53608131E-02   # N_43
  4  4     3.07705640E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.58457972E-02   # U_11
  1  2     9.99357333E-01   # U_12
  2  1     9.99357333E-01   # U_21
  2  2     3.58457972E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.34982872E-02   # V_11
  1  2     9.99053502E-01   # V_12
  2  1     9.99053502E-01   # V_21
  2  2     4.34982872E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08050257E-01   # cos(theta_t)
  1  2     7.06162045E-01   # sin(theta_t)
  2  1    -7.06162045E-01   # -sin(theta_t)
  2  2     7.08050257E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.61045875E-01   # cos(theta_b)
  1  2     7.50345488E-01   # sin(theta_b)
  2  1    -7.50345488E-01   # -sin(theta_b)
  2  2     6.61045875E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03207452E-01   # cos(theta_tau)
  1  2     7.10984725E-01   # sin(theta_tau)
  2  1    -7.10984725E-01   # -sin(theta_tau)
  2  2     7.03207452E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90205401E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52098628E+02   # vev(Q)              
         4     3.51203245E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52814009E-01   # gprime(Q) DRbar
     2     6.27372845E-01   # g(Q) DRbar
     3     1.08402972E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02565781E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71862992E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79827990E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.81550520E+06   # M^2_Hd              
        22    -6.47962300E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37136995E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.70523193E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.84020063E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.55297676E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.80975007E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     7.05124583E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.24834183E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.92884359E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.15437538E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.41569982E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.11222207E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     7.05124583E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.24834183E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.92884359E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.15437538E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.41569982E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.11222207E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.12247162E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.94213592E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.02817107E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     6.99660212E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     6.23513846E-02    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.25338927E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.64546244E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.64546244E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.64546244E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.64546244E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.70126537E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.70126537E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.30439770E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.78567322E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01720966E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.94502487E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03929961E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.06007384E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.06440927E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.43927581E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.21255471E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.59640895E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.01417949E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.10776416E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.36768392E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.72400185E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.72023267E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.43421136E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.46498756E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.60085678E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.92808592E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.55003617E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.25245773E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.03528392E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.51991985E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.51517658E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.65783762E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.22705538E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.02971576E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.83548159E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.75124479E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.52648525E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.52449036E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.89428615E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.33758109E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.51266053E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.88738865E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88188508E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.30905554E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.79901304E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.61835826E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.97712121E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.19574073E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.27880242E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.68546708E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.14928664E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.61093501E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.55718325E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.33762357E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.93394282E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57440198E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.65234134E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.31174960E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.90087708E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.62329645E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.97758701E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.12286435E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.89268521E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.98736185E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.55777701E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.16579844E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88549335E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.33758109E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.51266053E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.88738865E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88188508E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.30905554E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.79901304E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.61835826E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.97712121E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.19574073E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.27880242E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.68546708E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.14928664E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.61093501E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.55718325E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.33762357E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.93394282E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57440198E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.65234134E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.31174960E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.90087708E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.62329645E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.97758701E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.12286435E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.89268521E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.98736185E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.55777701E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.16579844E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88549335E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.97058438E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.52935185E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.33656651E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06644196E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68600703E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.54828053E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.37911965E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26448614E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.14624492E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73558090E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85357790E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.62765683E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.97058438E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.52935185E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.33656651E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06644196E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68600703E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.54828053E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.37911965E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26448614E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.14624492E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73558090E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85357790E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.62765683E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62156881E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78731195E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47911714E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.66953611E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.50165422E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.83561156E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.00729038E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.37210718E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62267836E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62888733E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.88923685E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.73585783E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54130849E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.89538642E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08666757E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97059163E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.11742197E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.29759184E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.93524317E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69103232E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.27977347E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37499390E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.97059163E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.11742197E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.29759184E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.93524317E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69103232E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.27977347E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37499390E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97353007E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11631773E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.29532136E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.92740155E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.68837304E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.26474749E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.36969207E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.10626838E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.40750304E-03    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.32036867E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.30388453E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10679090E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10676002E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09812085E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55733377E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53025704E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13650644E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46355128E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.33022154E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53938492E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.87789639E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.55088308E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00992255E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87916652E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10910924E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.25611201E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.35689346E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.59825695E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.19168824E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.31928877E-03    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18010405E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52797100E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17205090E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52775284E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38954673E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48923147E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48910561E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45370203E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.96793547E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.96793547E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.96793547E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.62192859E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.62192859E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.12692274E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.12692274E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.87397639E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.87397639E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.85755611E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.85755611E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.53774810E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.53774810E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.29038310E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.61536665E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.31812570E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.00384378E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.59943026E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.28689281E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18408773E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42239281E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.39101841E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42626806E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.14957404E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55167883E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55164925E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.68079032E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.90860280E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.90860280E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.90860280E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.50140523E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.23876628E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.47812455E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.23810829E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.82613393E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.39597766E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.39562500E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.29613602E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.47695297E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.47695297E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.47695297E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31135413E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31135413E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30420607E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30420607E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37117360E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37117360E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37103960E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37103960E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33358714E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33358714E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55737143E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07447270E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.50710803E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24635296E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46339064E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46339064E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16090791E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.11813948E-07    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.29067454E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.35900387E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89036502E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.98669396E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.14716894E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.45803684E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.16561597E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08473598E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17657420E-01    2           5        -5   # BR(h -> b       bb     )
     6.37640728E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25701334E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78281404E-04    2           3        -3   # BR(h -> s       sb     )
     2.06337939E-02    2           4        -4   # BR(h -> c       cb     )
     6.69986260E-02    2          21        21   # BR(h -> g       g      )
     2.30514109E-03    2          22        22   # BR(h -> gam     gam    )
     1.53783324E-03    2          22        23   # BR(h -> Z       gam    )
     2.00776018E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56231124E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78122392E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47153297E-03    2           5        -5   # BR(H -> b       bb     )
     2.46424038E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71200719E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11544800E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667530E-05    2           4        -4   # BR(H -> c       cb     )
     9.96061190E-01    2           6        -6   # BR(H -> t       tb     )
     7.97644591E-04    2          21        21   # BR(H -> g       g      )
     2.70400016E-06    2          22        22   # BR(H -> gam     gam    )
     1.15983609E-06    2          23        22   # BR(H -> Z       gam    )
     3.35684063E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67384258E-04    2          23        23   # BR(H -> Z       Z      )
     9.04222691E-04    2          25        25   # BR(H -> h       h      )
     8.30756266E-24    2          36        36   # BR(H -> A       A      )
     3.31556892E-11    2          23        36   # BR(H -> Z       A      )
     7.13606323E-12    2          24       -37   # BR(H -> W+      H-     )
     7.13606323E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382939E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47445520E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897293E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62265039E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677197E-06    2           3        -3   # BR(A -> s       sb     )
     9.96174739E-06    2           4        -4   # BR(A -> c       cb     )
     9.96994247E-01    2           6        -6   # BR(A -> t       tb     )
     9.43673995E-04    2          21        21   # BR(A -> g       g      )
     3.25325027E-06    2          22        22   # BR(A -> gam     gam    )
     1.35242938E-06    2          23        22   # BR(A -> Z       gam    )
     3.27160404E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74484657E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36522980E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237765E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145535E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50375848E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45692179E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731235E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401247E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35326229E-04    2          24        25   # BR(H+ -> W+      h      )
     3.42916311E-13    2          24        36   # BR(H+ -> W+      A      )
