evgenConfig.description = 'bbH(mH=800 GeV, width=20 GeV)->tautau->hadhad with A14NNPDF23LO tune'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'MSSM', 'bbHiggs', '2tau' ]
evgenConfig.contact     = [ 'lorenz.hauswald@cern.ch' ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["Higgs:useBSM on",
                            "HiggsBSM:gg2H2bbbar on",
                            "25:m0 125.0",
                            "35:m0 800.0",
                            "35:onMode = off",
                            "35:onIfMatch = 15 15", 
                            "SLHA:file = narrowWidth.slha"] 

include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 35
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]

# set width for heavy Higgs
nwf=open('./narrowWidth.slha', 'w')
nwfinput = """#           PDG      WIDTH
DECAY   35  20.00
#          BR         NDA          ID1       ID2       ID3       ID4
1       2       15      -15
"""
nwf.write(nwfinput)
nwf.close()

