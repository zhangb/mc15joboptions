
evgenConfig.process     = "VBF H->tautau->ll"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->tautau, for systematics"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 2000
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = off', # decay of taus
                             '15:onIfAny = 11 13' ]

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep13lep7filter = TauFilter("lep13lep7filter")
  filtSeq += lep13lep7filter

filtSeq.lep13lep7filter.UseNewOptions = True
filtSeq.lep13lep7filter.Ntaus = 2
filtSeq.lep13lep7filter.Nleptaus = 2
filtSeq.lep13lep7filter.Nhadtaus = 0
filtSeq.lep13lep7filter.EtaMaxlep = 2.6
filtSeq.lep13lep7filter.EtaMaxhad = 2.6
filtSeq.lep13lep7filter.Ptcutlep = 7000.0 #MeV
filtSeq.lep13lep7filter.Ptcutlep_lead = 13000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad_lead = 20000.0 #MeV
