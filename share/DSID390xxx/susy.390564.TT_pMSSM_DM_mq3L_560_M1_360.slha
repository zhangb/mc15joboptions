#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11086213E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.92900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.20485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04166744E+01   # W+
        25     1.24092919E+02   # h
        35     4.00000970E+03   # H
        36     3.99999685E+03   # A
        37     4.00100170E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02012046E+03   # ~d_L
   2000001     4.01789152E+03   # ~d_R
   1000002     4.01945803E+03   # ~u_L
   2000002     4.01919859E+03   # ~u_R
   1000003     4.02012046E+03   # ~s_L
   2000003     4.01789152E+03   # ~s_R
   1000004     4.01945803E+03   # ~c_L
   2000004     4.01919859E+03   # ~c_R
   1000005     6.25437496E+02   # ~b_1
   2000005     4.02218737E+03   # ~b_2
   1000006     6.17783096E+02   # ~t_1
   2000006     2.22295800E+03   # ~t_2
   1000011     4.00359705E+03   # ~e_L
   2000011     4.00283362E+03   # ~e_R
   1000012     4.00247449E+03   # ~nu_eL
   1000013     4.00359705E+03   # ~mu_L
   2000013     4.00283362E+03   # ~mu_R
   1000014     4.00247449E+03   # ~nu_muL
   1000015     4.00428248E+03   # ~tau_1
   2000015     4.00794485E+03   # ~tau_2
   1000016     4.00441046E+03   # ~nu_tauL
   1000021     1.97170484E+03   # ~g
   1000022     3.49917555E+02   # ~chi_10
   1000023    -4.03587256E+02   # ~chi_20
   1000025     4.16619754E+02   # ~chi_30
   1000035     2.05489522E+03   # ~chi_40
   1000024     4.01239292E+02   # ~chi_1+
   1000037     2.05505942E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.50973724E-01   # N_11
  1  2    -1.51073946E-02   # N_12
  1  3    -3.97779607E-01   # N_13
  1  4    -3.42617676E-01   # N_14
  2  1    -4.33718954E-02   # N_21
  2  2     2.40751807E-02   # N_22
  2  3    -7.03956642E-01   # N_23
  2  4     7.08508511E-01   # N_24
  3  1     5.23413291E-01   # N_31
  3  2     2.85828743E-02   # N_32
  3  3     5.88372172E-01   # N_33
  3  4     6.15662029E-01   # N_34
  4  1    -1.06133687E-03   # N_41
  4  2     9.99187256E-01   # N_42
  4  3    -5.88367986E-03   # N_43
  4  4    -3.98633200E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.32666133E-03   # U_11
  1  2     9.99965333E-01   # U_12
  2  1    -9.99965333E-01   # U_21
  2  2     8.32666133E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.63859647E-02   # V_11
  1  2    -9.98409046E-01   # V_12
  2  1    -9.98409046E-01   # V_21
  2  2    -5.63859647E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97230250E-01   # cos(theta_t)
  1  2    -7.43762629E-02   # sin(theta_t)
  2  1     7.43762629E-02   # -sin(theta_t)
  2  2     9.97230250E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999243E-01   # cos(theta_b)
  1  2    -1.23044684E-03   # sin(theta_b)
  2  1     1.23044684E-03   # -sin(theta_b)
  2  2     9.99999243E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05902205E-01   # cos(theta_tau)
  1  2     7.08309309E-01   # sin(theta_tau)
  2  1    -7.08309309E-01   # -sin(theta_tau)
  2  2    -7.05902205E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00230717E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10862129E+03  # DRbar Higgs Parameters
         1    -3.92900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44223324E+02   # higgs               
         4     1.62617985E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10862129E+03  # The gauge couplings
     1     3.61360572E-01   # gprime(Q) DRbar
     2     6.35897574E-01   # g(Q) DRbar
     3     1.03386228E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10862129E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.48909617E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10862129E+03  # The trilinear couplings
  1  1     3.51316278E-07   # A_d(Q) DRbar
  2  2     3.51349477E-07   # A_s(Q) DRbar
  3  3     6.24678030E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10862129E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.74287543E-08   # A_mu(Q) DRbar
  3  3     7.82131762E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10862129E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67934510E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10862129E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84811048E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10862129E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02938324E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10862129E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57269538E+07   # M^2_Hd              
        22    -1.73987657E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.20485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40290209E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.72848826E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46288638E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46288638E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53711362E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53711362E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.93314225E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.16821370E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.74625782E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.39231664E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.69321185E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18537565E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.94994075E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27390028E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03679170E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.53211237E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.01444924E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.76033798E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.27221003E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.30486183E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.88347056E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.20352552E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.16255332E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.64198293E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.96566026E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.22298035E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66854006E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52454925E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77959648E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60099867E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.17729048E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.57573178E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.33376757E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14398410E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.70254465E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.30596781E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.87963146E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.02391567E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78602226E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.83399471E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.15102653E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.39342306E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77484739E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.43920124E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.53759226E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53082755E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61136164E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.95967522E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02359828E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.48881308E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.42892410E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45412723E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78623116E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.67124127E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.91757148E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.25292672E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78003319E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.18620502E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57003520E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53301744E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54426072E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.03258060E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66927037E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.88242277E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.93777219E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85765070E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78602226E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.83399471E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.15102653E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.39342306E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77484739E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.43920124E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.53759226E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53082755E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61136164E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.95967522E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02359828E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.48881308E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.42892410E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45412723E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78623116E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.67124127E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.91757148E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.25292672E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78003319E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.18620502E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57003520E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53301744E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54426072E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.03258060E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66927037E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.88242277E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.93777219E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85765070E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13393624E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.11056882E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.64551746E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.34406721E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77985709E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.98293954E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57446742E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04445288E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.25439700E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87488163E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.72684797E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.21496301E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13393624E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.11056882E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.64551746E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.34406721E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77985709E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.98293954E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57446742E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04445288E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.25439700E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87488163E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.72684797E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.21496301E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07757641E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.60969737E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44441006E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.17578648E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40362990E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51724628E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81472062E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07198046E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.68203884E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.04915822E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.90806186E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43412756E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95025831E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87583008E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13499567E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.25765452E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19437527E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.63198930E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78373686E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.20011061E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55146484E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13499567E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.25765452E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19437527E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.63198930E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78373686E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.20011061E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55146484E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45986142E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14013447E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08277366E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.29262304E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52532292E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58313286E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03613928E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.28217900E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33546175E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33546175E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183582E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183582E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10540486E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.26796558E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.79700125E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.69479554E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.38016646E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.30024929E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.22716495E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.90859576E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.26585565E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.24014627E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.91086191E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17913077E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52925536E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17913077E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52925536E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42553421E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50523306E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50523306E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47553439E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00723536E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00723536E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00723514E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.24497307E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.24497307E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.24497307E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.24497307E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.48325013E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.48325013E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.48325013E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.48325013E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.56961585E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.56961585E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.54393487E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.03948421E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.80115235E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.00622361E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.04435341E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.00622361E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.04435341E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.66881295E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.03958472E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.03958472E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.03861371E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.15999380E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.15999380E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.15998096E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.53777333E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.18235025E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.53777333E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.18235025E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.49203838E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.64640456E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.64640456E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.47766649E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.29084935E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.29084935E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.29084938E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.30262000E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.30262000E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.30262000E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.30262000E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.43419256E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.43419256E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.43419256E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.43419256E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.34343179E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.34343179E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.26667751E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.20625602E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.11904568E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.74038064E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.22282958E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.22282958E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.97314322E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.22473528E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.22757234E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.86427907E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.86427907E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.88195428E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.88195428E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.83152382E-03   # h decays
#          BR         NDA      ID1       ID2
     6.13791647E-01    2           5        -5   # BR(h -> b       bb     )
     6.72060982E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.37915800E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.05694118E-04    2           3        -3   # BR(h -> s       sb     )
     2.19662613E-02    2           4        -4   # BR(h -> c       cb     )
     7.07310407E-02    2          21        21   # BR(h -> g       g      )
     2.37932466E-03    2          22        22   # BR(h -> gam     gam    )
     1.50321864E-03    2          22        23   # BR(h -> Z       gam    )
     1.97317701E-01    2          24       -24   # BR(h -> W+      W-     )
     2.43610987E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51965584E+01   # H decays
#          BR         NDA      ID1       ID2
     3.58697862E-01    2           5        -5   # BR(H -> b       bb     )
     6.00690290E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12389537E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51220183E-04    2           3        -3   # BR(H -> s       sb     )
     7.04520066E-08    2           4        -4   # BR(H -> c       cb     )
     7.06075793E-03    2           6        -6   # BR(H -> t       tb     )
     7.52566059E-07    2          21        21   # BR(H -> g       g      )
     1.39823861E-09    2          22        22   # BR(H -> gam     gam    )
     1.81807499E-09    2          23        22   # BR(H -> Z       gam    )
     1.58778936E-06    2          24       -24   # BR(H -> W+      W-     )
     7.93344635E-07    2          23        23   # BR(H -> Z       Z      )
     6.46254591E-06    2          25        25   # BR(H -> h       h      )
    -1.53720971E-24    2          36        36   # BR(H -> A       A      )
     2.44871629E-20    2          23        36   # BR(H -> Z       A      )
     1.86140780E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57686937E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57686937E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.22780311E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.75476362E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.38937949E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.61450619E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.01204044E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.39378512E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.16789427E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.17693950E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.24733589E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.89271244E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.60102926E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     6.60102926E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43930735E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51917125E+01   # A decays
#          BR         NDA      ID1       ID2
     3.58754750E-01    2           5        -5   # BR(A -> b       bb     )
     6.00745463E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12408878E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51284060E-04    2           3        -3   # BR(A -> s       sb     )
     7.09239224E-08    2           4        -4   # BR(A -> c       cb     )
     7.07593400E-03    2           6        -6   # BR(A -> t       tb     )
     1.45392537E-05    2          21        21   # BR(A -> g       g      )
     6.24315054E-08    2          22        22   # BR(A -> gam     gam    )
     1.59810930E-08    2          23        22   # BR(A -> Z       gam    )
     1.58469698E-06    2          23        25   # BR(A -> Z       h      )
     1.88324500E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57689699E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57689699E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.93424455E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.33965980E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.17787736E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.15629619E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     8.34086821E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.60341974E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.29436499E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.49926257E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.71223478E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.75016228E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.75016228E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53787993E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.77366041E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.98866364E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11744475E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69513952E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19957304E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46790750E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67530892E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.58108685E-06    2          24        25   # BR(H+ -> W+      h      )
     2.43446773E-14    2          24        36   # BR(H+ -> W+      A      )
     5.83553706E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.66887878E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.66426800E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57371164E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.98939395E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56271899E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.09451105E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.01961159E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.33144899E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
