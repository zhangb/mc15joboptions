
include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa Z/gamma* -> e e + 0,1,2j@NLO + 3,4j@LO."
evgenConfig.keywords = ["SM", "Z", "2electron", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 200

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5

  CSS_REWEIGHT 1
  REWEIGHT_SPLITTING_ALPHAS_SCALES 1
  REWEIGHT_SPLITTING_PDF_SCALES 1
  CSS_REWEIGHT_SCALE_CUTOFF 5

  % Kt merging
  JET_CRITERION FASTJET[A:kt,R:0.4,y:5];

  %tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

}(run)

(processes){
  Process 93 93 -> 11 -11 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);

  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 11 -11 40.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 16 
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppll", "ppllj","pplljj", "ppll_ew", "ppllj_ew","pplljj_ew" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_cheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
