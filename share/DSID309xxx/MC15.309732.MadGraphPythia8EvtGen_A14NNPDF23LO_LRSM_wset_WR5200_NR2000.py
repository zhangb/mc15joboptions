evgenConfig.description = "MadGraph5+Pythia8 for LRSM WR and NR (Keung-Senjanovic process) with particle widths set manually"
evgenConfig.keywords = ["exotic","Wprime","BSM"]
evgenConfig.contact = ["Xanthe Hoad <xanthe.hoad@cern.ch>"]
evgenConfig.process = "pp > WR > l NR, NR > l WR, WR > j j"
include("MC15JobOptions/MadGraphControl_LRSM_wset.py")
evgenConfig.minevents = 50
