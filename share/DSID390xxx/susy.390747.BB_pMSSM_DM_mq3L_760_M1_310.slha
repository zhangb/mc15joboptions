#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13068550E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.47900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04162829E+01   # W+
        25     1.25484700E+02   # h
        35     4.00000967E+03   # H
        36     3.99999718E+03   # A
        37     4.00102902E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02604167E+03   # ~d_L
   2000001     4.02260309E+03   # ~d_R
   1000002     4.02538667E+03   # ~u_L
   2000002     4.02367319E+03   # ~u_R
   1000003     4.02604167E+03   # ~s_L
   2000003     4.02260309E+03   # ~s_R
   1000004     4.02538667E+03   # ~c_L
   2000004     4.02367319E+03   # ~c_R
   1000005     8.35397794E+02   # ~b_1
   2000005     4.02544401E+03   # ~b_2
   1000006     8.24303489E+02   # ~t_1
   2000006     2.27849235E+03   # ~t_2
   1000011     4.00474700E+03   # ~e_L
   2000011     4.00312908E+03   # ~e_R
   1000012     4.00363055E+03   # ~nu_eL
   1000013     4.00474700E+03   # ~mu_L
   2000013     4.00312908E+03   # ~mu_R
   1000014     4.00363055E+03   # ~nu_muL
   1000015     4.00453127E+03   # ~tau_1
   2000015     4.00760871E+03   # ~tau_2
   1000016     4.00505171E+03   # ~nu_tauL
   1000021     1.98157185E+03   # ~g
   1000022     2.99932209E+02   # ~chi_10
   1000023    -3.58987403E+02   # ~chi_20
   1000025     3.70382358E+02   # ~chi_30
   1000035     2.05237622E+03   # ~chi_40
   1000024     3.56453613E+02   # ~chi_1+
   1000037     2.05254089E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.66966982E-01   # N_11
  1  2    -1.36037649E-02   # N_12
  1  3    -3.82859831E-01   # N_13
  1  4    -3.18749964E-01   # N_14
  2  1    -4.95790378E-02   # N_21
  2  2     2.45017307E-02   # N_22
  2  3    -7.03245439E-01   # N_23
  2  4     7.08792943E-01   # N_24
  3  1     4.95892254E-01   # N_31
  3  2     2.82909074E-02   # N_32
  3  3     5.99032479E-01   # N_33
  3  4     6.28053012E-01   # N_34
  4  1    -1.02126468E-03   # N_41
  4  2     9.99206799E-01   # N_42
  4  3    -4.92868656E-03   # N_43
  4  4    -3.95023763E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.97585465E-03   # U_11
  1  2     9.99975668E-01   # U_12
  2  1    -9.99975668E-01   # U_21
  2  2     6.97585465E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.58760309E-02   # V_11
  1  2    -9.98437714E-01   # V_12
  2  1    -9.98437714E-01   # V_21
  2  2    -5.58760309E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96267274E-01   # cos(theta_t)
  1  2    -8.63221800E-02   # sin(theta_t)
  2  1     8.63221800E-02   # -sin(theta_t)
  2  2     9.96267274E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999394E-01   # cos(theta_b)
  1  2    -1.10090855E-03   # sin(theta_b)
  2  1     1.10090855E-03   # -sin(theta_b)
  2  2     9.99999394E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05839679E-01   # cos(theta_tau)
  1  2     7.08371617E-01   # sin(theta_tau)
  2  1    -7.08371617E-01   # -sin(theta_tau)
  2  2    -7.05839679E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00269368E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30685498E+03  # DRbar Higgs Parameters
         1    -3.47900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43695760E+02   # higgs               
         4     1.61621919E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30685498E+03  # The gauge couplings
     1     3.61932335E-01   # gprime(Q) DRbar
     2     6.36230842E-01   # g(Q) DRbar
     3     1.03009413E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30685498E+03  # The trilinear couplings
  1  1     1.38124420E-06   # A_u(Q) DRbar
  2  2     1.38125739E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30685498E+03  # The trilinear couplings
  1  1     5.08299759E-07   # A_d(Q) DRbar
  2  2     5.08347065E-07   # A_s(Q) DRbar
  3  3     9.09320536E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30685498E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10821339E-07   # A_mu(Q) DRbar
  3  3     1.11949026E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30685498E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66073417E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30685498E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83550706E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30685498E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03348794E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30685498E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57700691E+07   # M^2_Hd              
        22    -1.25364685E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40439010E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.73037267E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44961496E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44961496E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55038504E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55038504E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.95001922E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09304207E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.30385519E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.52390447E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07919828E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.35285632E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.22296152E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16628258E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.67539232E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.83719230E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.31070194E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -9.12976489E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.20128919E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.37712145E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.42166896E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.99376306E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.29063774E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.76838863E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.20641635E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.74850653E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.82766885E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66907831E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53165099E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79338585E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62437213E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.62515355E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60931693E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.70692208E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13813951E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.30206775E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.73461933E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.92028639E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.22888033E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78937804E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.96339166E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.95979239E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.28475830E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79727156E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36795210E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58242086E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52398533E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61250926E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.14150851E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.34815012E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.34736139E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19732464E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44976454E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78958419E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.73596406E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.87951035E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.71114549E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80231600E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.36712885E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61461554E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52616444E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54503431E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.08029537E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.51658519E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.51452597E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.33706135E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85647346E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78937804E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.96339166E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.95979239E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.28475830E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79727156E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36795210E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58242086E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52398533E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61250926E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.14150851E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.34815012E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.34736139E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19732464E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44976454E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78958419E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.73596406E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.87951035E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.71114549E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80231600E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.36712885E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61461554E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52616444E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54503431E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.08029537E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.51658519E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.51452597E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.33706135E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85647346E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14709006E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.16531986E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.91074195E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.85228509E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77816585E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.90808011E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57072586E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06009317E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.52736621E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.44968631E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.44813118E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.73999046E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14709006E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.16531986E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.91074195E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.85228509E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77816585E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.90808011E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57072586E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06009317E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.52736621E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.44968631E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.44813118E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.73999046E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09426245E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.71220108E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40381633E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.08747836E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40125890E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48892743E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80978729E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08990512E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.80112530E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.13566397E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.79145757E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42865633E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00504649E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86468975E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14814564E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.30048300E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.39969339E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.24242927E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78181910E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14700397E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54798799E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14814564E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.30048300E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.39969339E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.24242927E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78181910E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14700397E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54798799E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47486622E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17863415E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26855344E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.93864207E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52243167E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.61703898E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03068055E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.94763246E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33505716E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33505716E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11169990E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11169990E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10648589E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.61081741E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.66348598E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.53516620E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.11715423E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.24286288E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.94571096E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.67680775E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.03094239E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.82071174E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.43067322E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17732291E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52715562E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17732291E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52715562E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43736504E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50181413E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50181413E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47564847E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00102509E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00102509E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00102490E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.23758890E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.23758890E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.23758890E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.23758890E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.45863693E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.45863693E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.45863693E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.45863693E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.88103720E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.88103720E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.97614794E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.45945254E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.47273807E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.74693371E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.26013770E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.74693371E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.26013770E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.21259708E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.85618706E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.85618706E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.85048053E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.79231870E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.79231870E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.79230944E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.81518851E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.35464179E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.81518851E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.35464179E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.93774566E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.39976172E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.39976172E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.67220304E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.07938359E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.07938359E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.07938360E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.67649927E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.67649927E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.67649927E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.67649927E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.58827895E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.58827895E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.58827895E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.58827895E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.16115610E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.16115610E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.60943919E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.31398013E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.44485908E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.14129960E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.93828273E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.93828273E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.33534353E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.64256104E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63558473E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.79304031E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.79304031E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.80666957E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.80666957E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01670780E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93323495E-01    2           5        -5   # BR(h -> b       bb     )
     6.48384461E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29527881E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86805537E-04    2           3        -3   # BR(h -> s       sb     )
     2.11424420E-02    2           4        -4   # BR(h -> c       cb     )
     6.91580543E-02    2          21        21   # BR(h -> g       g      )
     2.38214323E-03    2          22        22   # BR(h -> gam     gam    )
     1.62515299E-03    2          22        23   # BR(h -> Z       gam    )
     2.19136319E-01    2          24       -24   # BR(h -> W+      W-     )
     2.76776135E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48792182E+01   # H decays
#          BR         NDA      ID1       ID2
     3.54884598E-01    2           5        -5   # BR(H -> b       bb     )
     6.04163556E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13617600E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52672768E-04    2           3        -3   # BR(H -> s       sb     )
     7.08703377E-08    2           4        -4   # BR(H -> c       cb     )
     7.10268342E-03    2           6        -6   # BR(H -> t       tb     )
     8.94916714E-07    2          21        21   # BR(H -> g       g      )
     6.23887770E-10    2          22        22   # BR(H -> gam     gam    )
     1.82563279E-09    2          23        22   # BR(H -> Z       gam    )
     1.79354725E-06    2          24       -24   # BR(H -> W+      W-     )
     8.96152283E-07    2          23        23   # BR(H -> Z       Z      )
     7.03891408E-06    2          25        25   # BR(H -> h       h      )
    -1.18538445E-24    2          36        36   # BR(H -> A       A      )
    -1.49117525E-20    2          23        36   # BR(H -> Z       A      )
     1.85869555E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60444292E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60444292E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.15948779E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.81668041E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.30571347E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.70642137E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.25881582E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.29486319E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.09873169E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.16807655E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.63311427E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.17098053E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.56047550E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.56047550E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.37017384E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48722845E+01   # A decays
#          BR         NDA      ID1       ID2
     3.54954993E-01    2           5        -5   # BR(A -> b       bb     )
     6.04242633E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13645392E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52746877E-04    2           3        -3   # BR(A -> s       sb     )
     7.13367970E-08    2           4        -4   # BR(A -> c       cb     )
     7.11712565E-03    2           6        -6   # BR(A -> t       tb     )
     1.46238906E-05    2          21        21   # BR(A -> g       g      )
     5.97806456E-08    2          22        22   # BR(A -> gam     gam    )
     1.60730279E-08    2          23        22   # BR(A -> Z       gam    )
     1.79000671E-06    2          23        25   # BR(A -> Z       h      )
     1.87261717E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60459546E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60459546E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.87017391E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.62051908E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.09610026E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.29758196E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.02872492E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.42118230E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.21951621E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.74826891E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.92116723E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.66987394E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.66987394E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49853557E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.69932647E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03155626E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13261053E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.64756583E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20816371E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48558127E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.62938061E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.78833414E-06    2          24        25   # BR(H+ -> W+      h      )
     2.79924539E-14    2          24        36   # BR(H+ -> W+      A      )
     6.17698531E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.63095413E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.46332758E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60343434E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.71425225E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58904317E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.15524852E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.65268791E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.20395278E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
