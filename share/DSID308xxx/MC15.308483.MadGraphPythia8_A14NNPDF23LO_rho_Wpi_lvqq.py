###########################################################
#
# LHE file generated with pythia6 stand-alone with parameters:
#   ecm = centre-of-mass energy
#   resmass = mass of rho_T
#   pimass = mass of pi_T
#
###########################################################
#  choose the pdf
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

include("MC15JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
    "Next:numberShowLHA = 10",
    "Next:numberShowEvent = 10"]

evgenConfig.generators = [ 'MadGraph', 'Pythia8' ]
evgenConfig.description = 'MadGraphPythia8, technirho -> W+technipion -> lvjj mrho=6500 GeV mpi=6000 GeV'
evgenConfig.keywords = [ 'technicolor', 'lepton', 'dijet', 'W', 'rho' ]
evgenConfig.inputfilecheck = 'Wpi'
evgenConfig.contact = [ 'vpascuzz@cern.ch' ]
