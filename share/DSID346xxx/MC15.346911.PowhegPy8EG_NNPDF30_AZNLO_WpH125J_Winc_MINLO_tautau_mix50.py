
evgenConfig.process     = "qq->WpH, W->all, H->tautau 50% CP even/odd"
evgenConfig.description = "POWHEG+MiNLO+PYTHIA8, H+Wp+jet, W->all, H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch', 'Ki.Lie@cern.ch' ]
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'HiggsH1:parity = 4',
                             'HiggsH1:phiParity = 0.785398', # pi/4
                             '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]
