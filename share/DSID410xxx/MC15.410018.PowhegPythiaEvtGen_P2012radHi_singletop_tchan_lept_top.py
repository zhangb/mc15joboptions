#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 t-channel single top production (2->3) (top), scale=0.5, Perugia 2012 radHi tune, EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.contact     = [ 'onofrio@liverpool.ac.uk','dominic.hirschbuehl@cern.ch' ]

if runArgs.trfSubstepName == 'generate' :

  evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_top_lept"

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012radHi_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')


#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')
