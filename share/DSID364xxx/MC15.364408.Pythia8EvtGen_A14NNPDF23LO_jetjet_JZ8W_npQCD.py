# JO for Pythia 8 jet jet JZ8W slice
	
evgenConfig.description = "Dijet truth jet slice JZ8W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 1500.",
                            "HadronLevel:all = off",
                            "BeamRemnants:primordialKT = off",
                            "PartonLevel:MPI = off"]

include("MC15JobOptions/JetFilter_JZ8W.py")
