include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "2 charged leptons + 2 neutrinos with 0,1j@NLO + 2,3j@LO and mll>2*ml+250 MeV, pTl1>5 GeV, pTl2>5 GeV."
evgenConfig.keywords = ["diboson", "2lepton", "neutrino", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_CT10_llvv"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=4,5; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16

  % massive b-quarks such that top-quark processes are not included by the 93 container
  MASSIVE[5]=1;
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0
}(run)

(processes){
  Process 93 93 -> 90 91 90 91 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
}(selector)
"""
