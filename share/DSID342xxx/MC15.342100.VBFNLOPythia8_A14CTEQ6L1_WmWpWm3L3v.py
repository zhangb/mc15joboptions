evgenConfig.description = "VBFNLO+Pythia8 production W+W+W- with the A14 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson",  "WWW"]
evgenConfig.inputfilecheck = "vbfnlo270.342100"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Yanwen Liu<Yanwen.Liu@cern.ch>"]
include( "MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_Common.py" )
include( "MC15JobOptions/Pythia8_LHEF.py" )
evgenConfig.generators += ["VBFNLO", "Pythia8"]
#include("Pythia8_AU2_CTEQ6L1_Common.py")
#include("MC15JobOptions/Pythia8_MadGraph.py")
#include("MC15JobOptions/Pythia8_Photos.py")
