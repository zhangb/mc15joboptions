model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 190.
mHD = 190.
widthZp = 7.559856e-01
widthhd = 7.549382e-02
filteff = 8.756567e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
