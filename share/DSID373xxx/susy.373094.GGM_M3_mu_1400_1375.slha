#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.37200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.37500000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05412495E+01   # W+
        25     1.26000000E+02   # h
        35     2.00401533E+03   # H
        36     2.00000000E+03   # A
        37     2.00150819E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.99643628E+03   # ~d_L
   2000001     4.99632688E+03   # ~d_R
   1000002     4.99619167E+03   # ~u_L
   2000002     4.99624947E+03   # ~u_R
   1000003     4.99643628E+03   # ~s_L
   2000003     4.99632688E+03   # ~s_R
   1000004     4.99619167E+03   # ~c_L
   2000004     4.99624947E+03   # ~c_R
   1000005     4.99581573E+03   # ~b_1
   2000005     4.99694883E+03   # ~b_2
   1000006     5.11492840E+03   # ~t_1
   2000006     5.40802591E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007610E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007610E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99971264E+03   # ~tau_1
   2000015     5.00044625E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.52199631E+03   # ~g
   1000022     1.41380077E+03   # ~chi_10
   1000023    -1.46300256E+03   # ~chi_20
   1000025     1.50102077E+03   # ~chi_30
   1000035     3.12777034E+03   # ~chi_40
   1000024     1.45936531E+03   # ~chi_1+
   1000037     3.12776796E+03   # ~chi_2+
   1000039     1.95025000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.22360591E-01   # N_11
  1  2    -3.12957606E-02   # N_12
  1  3     4.90140442E-01   # N_13
  1  4    -4.86803963E-01   # N_14
  2  1     2.99106398E-03   # N_21
  2  2    -3.38281745E-03   # N_22
  2  3    -7.07004649E-01   # N_23
  2  4    -7.07194482E-01   # N_24
  3  1     6.91509011E-01   # N_31
  3  2     3.44861283E-02   # N_32
  3  3    -5.08895514E-01   # N_33
  3  4     5.11518671E-01   # N_34
  4  1     1.23186972E-03   # N_41
  4  2    -9.98909325E-01   # N_42
  4  3    -3.05307854E-02   # N_43
  4  4     3.53060160E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.31460847E-02   # U_11
  1  2     9.99068774E-01   # U_12
  2  1     9.99068774E-01   # U_21
  2  2     4.31460847E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.98999296E-02   # V_11
  1  2     9.98754223E-01   # V_12
  2  1     9.98754223E-01   # V_21
  2  2     4.98999296E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99946799E-01   # cos(theta_t)
  1  2     1.03149973E-02   # sin(theta_t)
  2  1    -1.03149973E-02   # -sin(theta_t)
  2  2     9.99946799E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.72105765E-01   # cos(theta_b)
  1  2     7.40455158E-01   # sin(theta_b)
  2  1    -7.40455158E-01   # -sin(theta_b)
  2  2     6.72105765E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04169226E-01   # cos(theta_tau)
  1  2     7.10032183E-01   # sin(theta_tau)
  2  1    -7.10032183E-01   # -sin(theta_tau)
  2  2     7.04169226E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202185E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.37500000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52188016E+02   # vev(Q)              
         4     3.37851258E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52767805E-01   # gprime(Q) DRbar
     2     6.27079839E-01   # g(Q) DRbar
     3     1.08619147E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02511700E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71737248E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79790829E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.37200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     9.28800066E+05   # M^2_Hd              
        22    -7.22774672E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37006139E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.21210304E-08   # gluino decays
#          BR         NDA      ID1       ID2
     6.85362346E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.48291972E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     6.04439591E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.11755390E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.46427019E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.54423876E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.24734715E-06    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.64388950E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.19408092E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     4.83791076E-06    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.46427019E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.54423876E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.24734715E-06    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.64388950E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.19408092E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.83791076E-06    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.23254458E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     9.57988177E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.93149862E-07    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.60435471E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.60435471E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.60435471E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.60435471E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.08279884E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.82282567E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.00265596E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.37301334E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.48510965E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     7.90752099E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.92786501E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.12657492E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.07138100E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.83831928E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.77755399E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.60146872E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.85568394E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.84745982E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.23563255E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.77381324E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.05591650E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.19318665E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.53724839E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.56910119E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.34146556E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.22051017E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.50176464E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.19191995E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.35533490E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.09807500E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.19477033E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.00607892E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.41018374E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.56979019E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.62439563E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.24463968E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.25513737E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.62860748E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.52639702E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32275876E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.07973525E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.16697002E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.35990427E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.34699883E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.32636316E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.78467004E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.96072148E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.33403678E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.92235455E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.08315933E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.23253871E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.10147534E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.55876776E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.88341704E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.67916094E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60150694E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.35995865E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.88306360E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.04031030E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.00058883E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.96390911E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.49271464E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.92698079E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.08357667E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.16586215E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.41553777E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.17100900E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.85360145E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.90447923E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89730762E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.35990427E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.34699883E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.32636316E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.78467004E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.96072148E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.33403678E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.92235455E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.08315933E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.23253871E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.10147534E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.55876776E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.88341704E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.67916094E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60150694E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.35995865E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.88306360E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.04031030E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.00058883E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.96390911E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.49271464E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.92698079E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.08357667E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.16586215E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.41553777E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.17100900E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.85360145E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.90447923E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89730762E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.69804484E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.63231483E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.75195088E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.07633432E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.67622468E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.25866155E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.36160539E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07370423E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.27348789E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.93058355E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.72641609E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.71267246E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.69804484E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.63231483E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.75195088E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.07633432E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.67622468E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.25866155E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.36160539E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07370423E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.27348789E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.93058355E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.72641609E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.71267246E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.39082456E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.85213660E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.67709685E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.62264106E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.49340035E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.52204310E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.99192442E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.95533565E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.38898929E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.70353529E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.66663655E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.69937488E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.52791249E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.50068987E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.06101002E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.69815865E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17525454E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.55498183E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.54211103E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.68308742E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.02079501E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.35708349E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.69815865E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17525454E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.55498183E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.54211103E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.68308742E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.02079501E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.35708349E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.70084614E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17408509E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.55343454E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.53460621E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.68041760E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.01142065E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.35176713E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.23259760E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.04931409E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33610695E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33610695E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11203693E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11203693E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10370419E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.87457331E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53185101E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11081535E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46257777E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35725461E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53750125E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.33022336E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.92164207E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.28936639E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00306277E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88558943E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11347796E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.46866594E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.86953275E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.24218322E-12    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.62153733E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     6.45216804E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18546458E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53499806E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18546458E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53499806E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40350651E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50568095E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50568095E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47154037E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00104402E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00104402E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00104402E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.59776305E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.59776305E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.59776305E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.59776305E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.32587715E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.32587715E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.32587715E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.32587715E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.65396701E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.65396701E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.07026692E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.05976989E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.88407647E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     4.27691513E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.79843949E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.08833197E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11504440E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32316282E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.11504440E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.32316282E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.83485614E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.28889243E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.28889243E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.59979464E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.48659356E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.48659356E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.48659356E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.35797460E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.05322581E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.35797460E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.05322581E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.63723345E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.97307349E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.97307349E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.87333239E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.39255457E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.39255457E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.39255457E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.33848063E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.33848063E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.33848063E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.33848063E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.46159579E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.46159579E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.46159579E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.46159579E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.42150137E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.42150137E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.87458984E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.10811709E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51683908E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.12546715E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46428934E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46428934E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13773584E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.30025687E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.39061026E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.58498053E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.77147096E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.76958560E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.86097500E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.83418270E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21934373E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01977043E-01    2           5        -5   # BR(h -> b       bb     )
     6.22242726E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20246797E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65999641E-04    2           3        -3   # BR(h -> s       sb     )
     2.01041423E-02    2           4        -4   # BR(h -> c       cb     )
     6.63925695E-02    2          21        21   # BR(h -> g       g      )
     2.30651356E-03    2          22        22   # BR(h -> gam     gam    )
     1.62626612E-03    2          22        23   # BR(h -> Z       gam    )
     2.16608551E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80743958E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78102333E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47041699E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427472E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71212860E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547036E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668138E-05    2           4        -4   # BR(H -> c       cb     )
     9.96064805E-01    2           6        -6   # BR(H -> t       tb     )
     7.97766383E-04    2          21        21   # BR(H -> g       g      )
     2.71403580E-06    2          22        22   # BR(H -> gam     gam    )
     1.16007463E-06    2          23        22   # BR(H -> Z       gam    )
     3.34678401E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66882786E-04    2          23        23   # BR(H -> Z       Z      )
     9.03095724E-04    2          25        25   # BR(H -> h       h      )
     7.43530364E-24    2          36        36   # BR(H -> A       A      )
     2.97877096E-11    2          23        36   # BR(H -> Z       A      )
     6.88804419E-12    2          24       -37   # BR(H -> W+      H-     )
     6.88804419E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382080E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47332235E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897842E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266977E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677452E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176978E-06    2           4        -4   # BR(A -> c       cb     )
     9.96996487E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676116E-04    2          21        21   # BR(A -> g       g      )
     3.15828049E-06    2          22        22   # BR(A -> gam     gam    )
     1.35265319E-06    2          23        22   # BR(A -> Z       gam    )
     3.26144490E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472263E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36277521E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237398E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81144237E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50218751E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693929E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731585E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402316E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34260375E-04    2          24        25   # BR(H+ -> W+      h      )
     2.74041090E-13    2          24        36   # BR(H+ -> W+      A      )
