model="LightVector"
mDM1 = 5.
mDM2 = 280.
mZp = 250.
mHD = 125.
widthZp = 9.947182e-01
widthN2 = 6.152863e-01
filteff = 6.226650e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
