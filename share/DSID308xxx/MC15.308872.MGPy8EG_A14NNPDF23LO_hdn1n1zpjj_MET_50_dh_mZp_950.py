model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 950.
mHD = 125.
widthZp = 4.530812e+00
widthhd = 2.863539e+00
filteff = 8.939746e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
