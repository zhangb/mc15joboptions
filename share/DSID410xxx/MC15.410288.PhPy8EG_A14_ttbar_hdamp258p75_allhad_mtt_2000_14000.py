#
#  These JO take unfiltered Powheg LHE files as input
#  Filter efficiency: 5.3e-3
#  For standard 16,500 event LHE files, 24 files are required per job to get 50 events
#  Change to 2 events per file, 25 files for 50 events total
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5* top mass, A14 tune, allhadronic, 1.1TeV<Mtt<1.3TeV '
evgenConfig.process     = 'SM ttbar'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'jiahang.zhong@cern.ch','mdshapiro@lbl.gov' ]
evgenConfig.minevents      = 1
 
 
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
 
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
 
 
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

if not hasattr( filtSeq, "MassRangeFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TTbarMassFilter
    filtSeq += TTbarMassFilter()
    pass

TTbarMassFilter = filtSeq.TTbarMassFilter
TTbarMassFilter.TopPairMassLowThreshold =2000000.
TTbarMassFilter.TopPairMassHighThreshold=14000000.

filtSeq.Expression = "(not TTbarWToLeptonFilter) and TTbarMassFilter"
