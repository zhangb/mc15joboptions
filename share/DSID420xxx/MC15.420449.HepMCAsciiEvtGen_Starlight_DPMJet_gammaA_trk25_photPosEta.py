evgenConfig.description = "Photonuclear events generated with STARlight using DPMJET"
evgenConfig.keywords = ["QCD","coherent"]
evgenConfig.inputfilecheck = 'dpmjet.*.gammaA_5TeV_PbPb_trk25_photPosEta'
evgenConfig.tune = "none"

include("MC15JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt']

#genSeq
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"

#fixSeq
#fixSeq.FixHepMC.OutputLevel = DEBUG
#from EvgenProdTools.EvgenProdToolsConf import CountHepMC
#fixSeq += CountHepMC("CountHep_fixSeq")

#testSeq
testSeq.TestHepMC.EnergyDifference = 1e+20
#testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
#testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV
testSeq.TestHepMC.EffFailThreshold=1e-10
testSeq.TestHepMC.CmEnergy=1.04417e+9
testSeq.TestHepMC.CmeDifference=1e+12
#testSeq.TestHepMC.OutputLevel=DEBUG
#testSeq.TestHepMC.MinTau=1e-15
#testSeq.TestHepMC.DumpEvent=True
testSeq.TestHepMC.NoDecayVertexStatuses=[1,2,3,4,12,13,14]
#testSeq += CountHepMC("CountHep_testSeq")

#postSeq
#postSeq.CopyEventWeight.OutputLevel = DEBUG
#postSeq.CountHepMC.OutputLevel = DEBUG
#from TruthIO.TruthIOConf import PrintMC
#postSeq += PrintMC()


