#Originally adapted from: MC15.361204.Pythia8_A2_MSTW2008LO_SD_minbias.py
# Pythia8 minimum bias SD with A3

evgenConfig.description = "SD Minbias with Pythia8 A3"
evgenConfig.keywords = ["minBias", "diffraction","SD"]
evgenConfig.generators = ["Pythia8"]

include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

