include("MadGraphControl_Pythia8EvtGen_2l2vjj_EW6.py")

# MET filter
include("MC15JobOptions/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 80*GeV
