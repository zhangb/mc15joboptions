include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", 
                            "23:onMode = off",                 
                            "23:onIfAny  = 11", 
                            "PhaseSpace:mHatMin = 9000.",
                            "PhaseSpace:mHatMax = 10000."]

# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime decaying to two electrons'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.generators += [ 'Pythia8' ]
