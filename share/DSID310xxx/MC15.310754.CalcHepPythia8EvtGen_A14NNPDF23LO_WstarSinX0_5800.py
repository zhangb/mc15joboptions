###############################################################
#
# Job options file CalcHEP+Pythia8
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Calchep production of Wstar->QQ of mass 5800GeV produced with AU14 NNPDF23LO tune"
evgenConfig.generators += ["CalcHep", "Pythia8"]
evgenConfig.keywords = ["BSM","exotic","excited","W","dijet"]
evgenConfig.inputfilecheck = "Wstar"
evgenConfig.contact=["wei.ding@cern.ch"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_CalcHep.py")
###############################################################





