import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=500
ihtmax=700
HTslice='midupperHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns_Mll10to40_ptl5.py')
evgenConfig.minevents=50

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Zmumu_Mll10to40_ptl5_Ht500_700_13TeV"
