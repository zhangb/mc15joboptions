#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

# Turn on the heavy quark effect
PowhegConfig.use_massive_b = True
PowhegConfig.use_massive_c = True

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 3

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
hfact_scale    = 1.2
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 6.

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

H_Mass = 125.0
H_Width = 0.00407

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 1' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 36 36', # h->aa

                             '36:onMode = off', # decay of the a
                             '36:oneChannel = 1 0.5 100 5 -5', #a->bb
                             '36:addChannel = 1 0.5 100 15 -15', #a->tautau
                             '36:m0 60.0', #scalar mass
                             '36:mMin 59.5', #scalar mass
                             '36:mMax 60.5', #scalar mass
                             '36:mWidth 0.01', # narrow width
                             '36:tau0 0', #scalarlife time
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->aa->bbtautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"]
evgenConfig.contact     = [ 'roger.caminal@cern.ch', 'philip.chang@cern.ch' ]


if not hasattr(genSeq, "XtoVVDecayFilter"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
    genSeq += XtoVVDecayFilter()
    genSeq += XtoVVDecayFilter("scalarFilter")

### Add this filter to the algs required to be successful for streaming
#if "XtoVVDecayFilter" not in StreamEVGEN.RequireAlgs:
#    StreamEVGEN.RequireAlgs += ["XtoVVDecayFilter"]
genSeq.XtoVVDecayFilter.PDGGrandParent = 36
genSeq.XtoVVDecayFilter.PDGParent = 15
genSeq.XtoVVDecayFilter.StatusParent = 2
genSeq.XtoVVDecayFilter.PDGChild1 = [11,13]
genSeq.XtoVVDecayFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]

genSeq.scalarFilter.PDGGrandParent = 35
genSeq.scalarFilter.PDGParent = 36
genSeq.scalarFilter.StatusParent = 22
genSeq.scalarFilter.PDGChild1 = [15]
genSeq.scalarFilter.PDGChild2 = [5]
