# Script to perform Diagram Removal 1, i.e. set the amplitude of overlapping diagrams to zero.
# Works for tW, tWZ and tWH. Removes the overlap (and interference) with tt, ttZ and ttH respectively.
# Contact: olga.bylund@cern.ch
# Please let me know if you have any questions or problems.

import os
import sys
import glob 
import DR_functions

my_ms_dir=sys.argv[1]
pdir=my_ms_dir+'/production_me/SubProcesses/'
fdir=my_ms_dir+'/full_me/SubProcesses/'
full_files=os.listdir(fdir)



P_folders= glob.glob(pdir+"P*_*")
mfiles = DR_functions.find_matrix_files(P_folders)

for mfile in mfiles:
    DR_functions.make_support_file(mfile, "the_process.txt", "Process:")

    with open("the_process.txt","r") as f:
        myline = f.readline()
        m2 = myline.split("Process: ")[1]
        the_process = m2.split("WEIGHTED")[0]

        bindex, windex = DR_functions.return_out_index(the_process)  

    redefine_twidth=False
    DRmode=1
    DR_functions.find_W_prepare_DRXhack(mfile,bindex,windex,redefine_twidth,DRmode)


    if os.path.getsize("mytmp.txt")>0: #if there are hacks to make
        DR_functions.do_DR1_hacks(mfile,"mytmp.txt")
            
        prefix_full = mfile.replace("production_me","full_me")
        prefix_full = prefix_full.replace("/matrix_prod.f","")
        for f_file in full_files:
            if prefix_full in fdir+f_file:
                full_file_matrix = fdir+f_file+"/matrix.f"
                full_file_matrix_prod = fdir+f_file+"/matrix_prod.f"
                DR_functions.do_DR1_hacks(full_file_matrix, "mytmp.txt")
                DR_functions.do_DR1_hacks(full_file_matrix_prod, "mytmp.txt")

os.remove("the_process.txt")
