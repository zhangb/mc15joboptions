include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "e+- nu gamma gamma production with up to two jets in ME+PS and 10<pT_gamma<50 GeV."
evgenConfig.keywords = ["electroweak","diphoton","electron","neutrino", "SM","triboson"]
evgenConfig.contact  = [ "andrey.loginov@yale.edu", "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.inputconfcheck = "enugammagammaPt15_50"
evgenConfig.minevents = 2000
evgenConfig.generators  = [ "Sherpa"] 
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  12 -11 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process;

  Process 93 93 ->  -12 11 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process;
}(processes)

(selector){
  PT 22 15 50
  DeltaR 22 90 0.2 1000
  DeltaR 22 22 0.2 1000
  DeltaR 93 90 0.2 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""
