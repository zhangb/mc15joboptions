#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13058968E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.77990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04183477E+01   # W+
        25     1.25837370E+02   # h
        35     4.00001066E+03   # H
        36     3.99999734E+03   # A
        37     4.00102701E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02604039E+03   # ~d_L
   2000001     4.02264306E+03   # ~d_R
   1000002     4.02538891E+03   # ~u_L
   2000002     4.02348473E+03   # ~u_R
   1000003     4.02604039E+03   # ~s_L
   2000003     4.02264306E+03   # ~s_R
   1000004     4.02538891E+03   # ~c_L
   2000004     4.02348473E+03   # ~c_R
   1000005     9.26049186E+02   # ~b_1
   2000005     4.02550953E+03   # ~b_2
   1000006     9.08282272E+02   # ~t_1
   2000006     2.01932841E+03   # ~t_2
   1000011     4.00461594E+03   # ~e_L
   2000011     4.00328177E+03   # ~e_R
   1000012     4.00350364E+03   # ~nu_eL
   1000013     4.00461594E+03   # ~mu_L
   2000013     4.00328177E+03   # ~mu_R
   1000014     4.00350364E+03   # ~nu_muL
   1000015     4.00395859E+03   # ~tau_1
   2000015     4.00816861E+03   # ~tau_2
   1000016     4.00491431E+03   # ~nu_tauL
   1000021     1.97937338E+03   # ~g
   1000022     4.45545268E+02   # ~chi_10
   1000023    -4.89754450E+02   # ~chi_20
   1000025     5.07301189E+02   # ~chi_30
   1000035     2.05211981E+03   # ~chi_40
   1000024     4.87613561E+02   # ~chi_1+
   1000037     2.05228437E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.89447074E-01   # N_11
  1  2    -1.93129335E-02   # N_12
  1  3    -4.54098593E-01   # N_13
  1  4    -4.12546720E-01   # N_14
  2  1    -3.48080834E-02   # N_21
  2  2     2.31872532E-02   # N_22
  2  3    -7.04837398E-01   # N_23
  2  4     7.08134868E-01   # N_24
  3  1     6.12829835E-01   # N_31
  3  2     2.80604460E-02   # N_32
  3  3     5.44921141E-01   # N_33
  3  4     5.71588273E-01   # N_34
  4  1    -1.14360715E-03   # N_41
  4  2     9.99150626E-01   # N_42
  4  3    -7.72402358E-03   # N_43
  4  4    -4.04605779E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.09295021E-02   # U_11
  1  2     9.99940271E-01   # U_12
  2  1    -9.99940271E-01   # U_21
  2  2     1.09295021E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.72289151E-02   # V_11
  1  2    -9.98361083E-01   # V_12
  2  1    -9.98361083E-01   # V_21
  2  2    -5.72289151E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92893653E-01   # cos(theta_t)
  1  2    -1.19005016E-01   # sin(theta_t)
  2  1     1.19005016E-01   # -sin(theta_t)
  2  2     9.92893653E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998802E-01   # cos(theta_b)
  1  2    -1.54790134E-03   # sin(theta_b)
  2  1     1.54790134E-03   # -sin(theta_b)
  2  2     9.99998802E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06215755E-01   # cos(theta_tau)
  1  2     7.07996686E-01   # sin(theta_tau)
  2  1    -7.07996686E-01   # -sin(theta_tau)
  2  2    -7.06215755E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00250155E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30589677E+03  # DRbar Higgs Parameters
         1    -4.77990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43678469E+02   # higgs               
         4     1.61572696E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30589677E+03  # The gauge couplings
     1     3.61878598E-01   # gprime(Q) DRbar
     2     6.35810916E-01   # g(Q) DRbar
     3     1.03004147E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30589677E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37991704E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30589677E+03  # The trilinear couplings
  1  1     5.09560070E-07   # A_d(Q) DRbar
  2  2     5.09607007E-07   # A_s(Q) DRbar
  3  3     9.10536923E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30589677E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.12912055E-07   # A_mu(Q) DRbar
  3  3     1.14062262E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30589677E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67256663E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30589677E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87080415E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30589677E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02962607E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30589677E+03  # The soft SUSY breaking masses at the scale Q
         1     4.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56640967E+07   # M^2_Hd              
        22    -2.01301281E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40244666E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.20702358E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42115436E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42115436E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57884564E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57884564E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.66789619E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.80280042E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.14994080E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.91163472E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13562405E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16825325E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.17551964E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11421388E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.02806940E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.19942721E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.66916463E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.19778721E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.39904817E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.13441015E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.44215874E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.34819154E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.18841356E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.80212362E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66721462E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57542151E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82107831E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.57786067E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.05435265E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65154145E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.08038212E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12565229E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.49823561E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.42394393E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.73746700E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.65245863E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78518379E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45213161E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.33457171E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76449881E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80233541E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.56197653E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59259062E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52254566E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60722782E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.39956480E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.57580685E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.03394292E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.02153494E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45599125E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78538690E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.43943699E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.53451066E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.59096165E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80783044E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.02873384E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62560839E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52471446E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54092298E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.86263728E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.71430375E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.30245626E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.04811726E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85817753E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78518379E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45213161E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.33457171E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76449881E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80233541E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.56197653E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59259062E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52254566E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60722782E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.39956480E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.57580685E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.03394292E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.02153494E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45599125E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78538690E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.43943699E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.53451066E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.59096165E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80783044E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.02873384E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62560839E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52471446E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54092298E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.86263728E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.71430375E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.30245626E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.04811726E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85817753E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13617150E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.26092057E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.67801520E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.06030409E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78373064E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.19062127E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58289950E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02890101E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.24972984E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20865773E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73817627E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.30807284E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13617150E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.26092057E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.67801520E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.06030409E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78373064E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.19062127E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58289950E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02890101E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.24972984E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20865773E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73817627E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.30807284E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06704842E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25189949E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.50018710E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.50616255E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40916759E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56587900E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82616377E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05804133E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.26955384E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.91890042E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.31337993E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44413089E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.84838479E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89620681E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13722592E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09924759E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.20584075E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.11084005E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78817704E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.26234591E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55966206E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13722592E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09924759E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.20584075E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.11084005E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78817704E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.26234591E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55966206E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45819786E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97590152E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.35452397E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.63821878E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53155461E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.50688524E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04799031E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.16772380E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33655859E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33655859E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11220070E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11220070E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10248143E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.25808069E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.60974752E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.00665760E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.44939815E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.47098979E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.26594942E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.28192067E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.00806333E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.38151944E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.31967709E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.83313995E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18436618E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53643070E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18436618E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53643070E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38379239E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52390832E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52390832E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48459807E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04508131E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04508131E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04508119E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.78187556E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.78187556E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.78187556E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.78187556E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.27292544E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.27292544E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.27292544E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.27292544E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.98041619E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.98041619E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.66446709E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.16617933E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.62228693E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.42786681E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.20970596E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.42786681E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.20970596E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.15327303E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.67089097E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.67089097E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.68352528E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.59835184E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.59835184E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.59832144E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.76206240E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.28590706E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.76206240E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.28590706E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.11159716E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.24316757E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.24316757E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.94642881E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.04811767E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.04811767E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.04811768E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.06935019E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.06935019E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.06935019E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.06935019E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.56445747E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.56445747E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.56445747E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.56445747E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.42431265E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.42431265E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.25657859E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.93715539E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.11496078E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.56404194E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.28134937E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.28134937E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.51035595E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.32777686E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52369120E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.76246430E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76246430E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.76767365E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.76767365E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04643059E-03   # h decays
#          BR         NDA      ID1       ID2
     5.86136931E-01    2           5        -5   # BR(h -> b       bb     )
     6.45385487E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28464711E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84286623E-04    2           3        -3   # BR(h -> s       sb     )
     2.10346139E-02    2           4        -4   # BR(h -> c       cb     )
     6.89054634E-02    2          21        21   # BR(h -> g       g      )
     2.39594799E-03    2          22        22   # BR(h -> gam     gam    )
     1.66420949E-03    2          22        23   # BR(h -> Z       gam    )
     2.25915950E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86955847E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54891264E+01   # H decays
#          BR         NDA      ID1       ID2
     3.65803063E-01    2           5        -5   # BR(H -> b       bb     )
     5.97523160E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11269717E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49895620E-04    2           3        -3   # BR(H -> s       sb     )
     7.00860030E-08    2           4        -4   # BR(H -> c       cb     )
     7.02407677E-03    2           6        -6   # BR(H -> t       tb     )
     8.38048882E-07    2          21        21   # BR(H -> g       g      )
     4.69268872E-09    2          22        22   # BR(H -> gam     gam    )
     1.80783121E-09    2          23        22   # BR(H -> Z       gam    )
     1.67578238E-06    2          24       -24   # BR(H -> W+      W-     )
     8.37310751E-07    2          23        23   # BR(H -> Z       Z      )
     6.78935946E-06    2          25        25   # BR(H -> h       h      )
     4.32019072E-24    2          36        36   # BR(H -> A       A      )
     5.76113462E-20    2          23        36   # BR(H -> Z       A      )
     1.85838564E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.53814569E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.53814569E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.48255331E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.35431450E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.63045492E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.25299279E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.43885786E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.94660720E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.48926777E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.24364263E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.24402949E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.07909426E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.80631863E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.80631863E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.37089199E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54904525E+01   # A decays
#          BR         NDA      ID1       ID2
     3.65819883E-01    2           5        -5   # BR(A -> b       bb     )
     5.97511347E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11265374E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49931267E-04    2           3        -3   # BR(A -> s       sb     )
     7.05421019E-08    2           4        -4   # BR(A -> c       cb     )
     7.03784056E-03    2           6        -6   # BR(A -> t       tb     )
     1.44609791E-05    2          21        21   # BR(A -> g       g      )
     6.70645545E-08    2          22        22   # BR(A -> gam     gam    )
     1.58996881E-08    2          23        22   # BR(A -> Z       gam    )
     1.67219435E-06    2          23        25   # BR(A -> Z       h      )
     1.90181171E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.53784304E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.53784304E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.17760927E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.66456633E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41230941E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67068233E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.84758727E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.39891856E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.64095523E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.08807404E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.93787144E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.03824961E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.03824961E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57338661E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.89537540E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95054897E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10396833E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.77303708E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19193745E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45219863E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.75088434E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.66676021E-06    2          24        25   # BR(H+ -> W+      h      )
     2.73281505E-14    2          24        36   # BR(H+ -> W+      A      )
     4.82461370E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.97441195E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.43629346E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.53339711E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.33140306E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.52639638E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.16392625E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.53669935E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.01784351E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
