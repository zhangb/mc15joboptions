#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '0.5000000',  # CV
                     'C2V': '1.0000000',  # C2V
                     'C3':  '1.0000000'}  # C3

include("MC15JobOptions/MadGraphControl_Py8EvtGen_A14NNPDF23LO_hh_bbbb_vbf.py")
