from MadGraphControl.MadGraphUtils import *

mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
# ctr: the SM-Higgs trilinear coupling 
# ctrH:the H coupling to 2 SM-Higgs - set to zero in order to get rid of the resonant contribution
# cyH: the H coupling to the top - set to zero in order to get rid of the resonant contribution
# NOTE: Since setting parameters to zero causes some numerical problems, they are set to a small value
#---------------------------------------------------------------------------------------------------
parameters={'188':'2.00000000', # ctr
            '190':'0.00000001', #ctrH
            '191':'0.00000001'} # cyH

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',      
           'parton_shower':'PYTHIA8'}

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
# Due to the parameters settings above, the resonant contribution is eliminated
# Thus, the non-resonant hh production with different trilinear Higgs couplings is enabled
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
if (runArgs.runNumber == 344725):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    import model HeavyHiggsTHDM
    generate p p > h h 
    output -f""")
    fcard.close()
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Filter efficiency is ~1%
# Thus, setting the number of generated events to 100 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=150
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

process_dir = new_process()
#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0,extras=extras)



#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
build_param_card(param_card_old='param_card.HeavyScalar.dat',param_card_new='param_card_new.dat',masses=higgsMass,extras=parameters)
   
print_cards()
    
runName='run_01'     

#process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
if (runArgs.runNumber == 344725):
    evgenConfig.description = "Non-resonant di-Higgs production with trilinear coupling of lambda02 (SMx2) which decays to yybb."
    evgenConfig.keywords = ["BSM", "BSMHiggs", "nonResonant", "diphoton", "bottom"]

evgenConfig.minevents = 1000
evgenConfig.contact = ['Tulin Varol <Tulin.Varol@cern.ch>']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

#---------------------------------------------------------------------------------------------------
# Decaying hh to bbyy with Pythia8
# Preferred way to select particular decay channels without modifying the Higgs
# width or cross section is to turn off all decays but those of interest, not to
# modify the branching ratios
#---------------------------------------------------------------------------------------------------
genSeq.Pythia8.Commands += [ "25:onMode=off",    # turn off decay modes
                             "25:onIfAny=5 22" ] # allow modes with b or gamma

#---------------------------------------------------------------------------------------------------
# Generator Filters
# Use ParentTwoChildren filter to require:
#   Higgs(25) -> b(5)bbar(-5) AND 
#   Higgs(25) -> gamma(22)gamma(22)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentTwoChildrenFilter
filtSeq += ParentTwoChildrenFilter("HiggsToBBGamGamFilter")
filtSeq.HiggsToBBGamGamFilter.PDGParent = [25]
filtSeq.HiggsToBBGamGamFilter.PDGChild = [5,22]


