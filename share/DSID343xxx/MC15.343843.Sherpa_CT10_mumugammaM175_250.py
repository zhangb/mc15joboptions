include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z to mu mu gamma production with up to twojets in ME+PS and 175<M_mumugamma<250 GeV."
evgenConfig.keywords = [ "electroweak", "2muon", "photon","SM" ]
evgenConfig.inputconfcheck = "mumugammaM175_250"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  
  Process 93 93 -> 13 -13 22 93{2}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  PT 22 40 E_CMS
  PT 90 8 E_CMS
  Mass 90 90 40 E_CMS
  "m" 90,90,22 175,250
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""
evgenConfig.generators  = [ "Sherpa"] 
