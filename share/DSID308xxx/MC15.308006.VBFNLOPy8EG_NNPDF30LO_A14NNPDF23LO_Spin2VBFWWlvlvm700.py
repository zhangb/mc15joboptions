evgenConfig.description = "VBFNLO+Pythia8 production WWlvlv VBF with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["WW",  "VBF"]
evgenConfig.inputfilecheck = "vbfnlo.308006.Spin2VBF_WWlvlv"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Guangyi Zhang<g.zhang@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
evgenConfig.generators += ["VBFNLO", "Pythia8", "EvtGen"]
