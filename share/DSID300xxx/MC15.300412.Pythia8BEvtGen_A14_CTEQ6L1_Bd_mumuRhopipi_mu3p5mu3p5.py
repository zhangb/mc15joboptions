##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B_d -> (Rho0->pipi)mumu
##############################################################
f = open("Bd_Rhopipi_2mu_USER.DEC","w")
f.write("Define dm_incohMix_B0 0.0\n")
f.write("Define dm_incohMix_B_s0 0.0\n")
f.write("Define mu 5.0\n")
f.write("Define Nf 5\n")
f.write("Define res_swch 0\n")
f.write("Define ias 1\n")
f.write("Define A 0.88\n")
f.write("Define lambda 0.227\n")
f.write("Define barrho 0.22\n")
f.write("Define bareta 0.34\n")
f.write("Alias myrho   rho0\n")
f.write("Decay B0\n")
f.write("1.0000    myrho  mu+  mu-        BTOSLLBALL;\n")
#f.write("1.000      myrho  mu+  mu-        BTOSLLMS mu Nf res_swch ias A lambda barrho bareta;\n")
f.write("Enddecay\n")
f.write("Decay myrho\n")
f.write("1.0000    pi+     pi-        VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
##############################################################

evgenConfig.description = "Exclusive Bd->Rhopipi_2mu production"
evgenConfig.keywords = ["exclusive","Bd","2muon"]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")

### Put the content of MC15JobOptions/Pythia8B_exclusiveB_Common.py
### except actual closing B decays

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")
###
###

include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 7.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1

# Make Pythia select the events with Bd
genSeq.Pythia8B.SignalPDGCodes = [511]

genSeq.EvtInclusiveDecay.userDecayFile = "Bd_Rhopipi_2mu_USER.DEC"

# Final state selections
filtSeq.BSignalFilter.B_PDGCode = 511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500 
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

