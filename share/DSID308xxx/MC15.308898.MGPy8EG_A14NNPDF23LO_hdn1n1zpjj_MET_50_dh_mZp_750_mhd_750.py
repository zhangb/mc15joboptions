model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 750.
mHD = 750.
widthZp = 3.570290e+00
widthhd = 2.983890e-01
filteff = 9.861933e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
