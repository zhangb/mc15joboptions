#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.02000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05377081E+01   # W+
        25     1.25000000E+02   # h
        35     2.00416710E+03   # H
        36     2.00000000E+03   # A
        37     2.00209535E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013345E+03   # ~d_L
   2000001     5.00002534E+03   # ~d_R
   1000002     4.99989189E+03   # ~u_L
   2000002     4.99994933E+03   # ~u_R
   1000003     5.00013345E+03   # ~s_L
   2000003     5.00002534E+03   # ~s_R
   1000004     4.99989189E+03   # ~c_L
   2000004     4.99994933E+03   # ~c_R
   1000005     4.99999898E+03   # ~b_1
   2000005     5.00016125E+03   # ~b_2
   1000006     5.00071426E+03   # ~t_1
   2000006     5.00375695E+03   # ~t_2
   1000011     5.00008278E+03   # ~e_L
   2000011     5.00007601E+03   # ~e_R
   1000012     4.99984121E+03   # ~nu_eL
   1000013     5.00008278E+03   # ~mu_L
   2000013     5.00007601E+03   # ~mu_R
   1000014     4.99984121E+03   # ~nu_muL
   1000015     5.00003958E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984121E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.36439727E+02   # ~chi_10
   1000023    -1.50249833E+02   # ~chi_20
   1000025     3.13615588E+02   # ~chi_30
   1000035     3.00219452E+03   # ~chi_40
   1000024     1.47959303E+02   # ~chi_1+
   1000037     3.00219406E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     2.54065784E-01   # N_11
  1  2    -2.61869788E-02   # N_12
  1  3     6.90095724E-01   # N_13
  1  4    -6.77150435E-01   # N_14
  2  1     1.96740342E-02   # N_21
  2  2    -5.03519451E-03   # N_22
  2  3    -7.04013610E-01   # N_23
  2  4    -7.09896060E-01   # N_24
  3  1     9.66986713E-01   # N_31
  3  2     7.45388995E-03   # N_32
  3  3    -1.66984438E-01   # N_33
  3  4     1.92346912E-01   # N_34
  4  1     4.55709373E-04   # N_41
  4  2    -9.99616591E-01   # N_42
  4  3    -1.57774095E-02   # N_43
  4  4     2.27494439E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.23043508E-02   # U_11
  1  2     9.99751227E-01   # U_12
  2  1     9.99751227E-01   # U_21
  2  2     2.23043508E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.21684054E-02   # V_11
  1  2     9.99482463E-01   # V_12
  2  1     9.99482463E-01   # V_21
  2  2     3.21684054E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13747261E-01   # cos(theta_t)
  1  2     7.00403346E-01   # sin(theta_t)
  2  1    -7.00403346E-01   # -sin(theta_t)
  2  2     7.13747261E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08497505E-01   # cos(theta_b)
  1  2     9.12759436E-01   # sin(theta_b)
  2  1    -9.12759436E-01   # -sin(theta_b)
  2  2     4.08497505E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76619265E-01   # cos(theta_tau)
  1  2     7.36333057E-01   # sin(theta_tau)
  2  1    -7.36333057E-01   # -sin(theta_tau)
  2  2     6.76619265E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90209656E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51759103E+02   # vev(Q)              
         4     3.87144094E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146780E-01   # gprime(Q) DRbar
     2     6.29570923E-01   # g(Q) DRbar
     3     1.07402615E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02736324E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72312705E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79964892E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.02000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04070026E+06   # M^2_Hd              
        22    -5.25318119E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38112020E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.24822170E-02   # gluino decays
#          BR         NDA      ID1       ID2
     2.78117407E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.06989608E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.15309713E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.70234539E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.58840785E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.31340328E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.18932933E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.02534883E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.84547788E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.70234539E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.58840785E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.31340328E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.18932933E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.02534883E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.84547788E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.26458426E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.70858350E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.32556524E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.03569532E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.26071243E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.77118685E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     4.97551327E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.97551327E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.97551327E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.97551327E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.31399452E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.31399452E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.37174957E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.57387449E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.54250208E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.40552770E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.76894452E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.25369965E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.52630257E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     4.95984630E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.35549216E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.22563778E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.50296712E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.24858401E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.54411707E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.77456924E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.08091940E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     4.50946381E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     7.88583720E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.20114939E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.16221056E-03    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.26162612E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -8.18758201E-01    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.12480243E+01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.64403429E+00    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.41674805E+01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.14374156E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     7.15443229E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.66196455E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -3.62715029E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.53209734E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.09232852E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.09476814E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     3.57373580E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.36090473E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.27726186E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.54145806E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.86824413E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.72151649E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.85819617E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.14419168E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.23148577E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.22487357E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     5.54148372E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.32208681E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.98259029E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.39171979E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.14599385E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.36096753E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.50119755E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.04857694E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.12777858E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.72410020E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.37408560E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.14493364E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.23239841E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.14647562E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.48012820E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.87328133E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.13214720E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.97439187E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.77189525E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.36090473E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.27726186E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.54145806E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.86824413E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.72151649E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.85819617E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.14419168E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.23148577E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.22487357E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.54148372E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.32208681E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.98259029E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.39171979E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.14599385E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.36096753E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     7.50119755E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.04857694E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.12777858E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.72410020E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.37408560E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.14493364E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.23239841E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.14647562E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.48012820E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.87328133E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.13214720E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.97439187E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.77189525E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03671952E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.77150954E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.33328704E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.94740056E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65135303E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.44778048E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30685020E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46265838E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.49364345E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.89264909E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.34674215E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.55615177E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03671952E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.77150954E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.33328704E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.94740056E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65135303E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.44778048E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30685020E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46265838E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.49364345E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.89264909E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.34674215E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.55615177E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.73305879E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     4.01243638E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57088856E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.53162213E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.34677817E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.88031406E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.69566559E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31645083E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.77632501E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.87985187E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06903873E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.97603058E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57424078E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.20307046E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.15093275E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03657234E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.84479839E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.67364397E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.84323673E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65377767E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.34118945E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30342022E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03657234E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.84479839E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.67364397E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.84323673E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65377767E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.34118945E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30342022E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.03978751E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.84284716E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.67187377E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.84128715E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65097079E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.39712578E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29781421E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.85971572E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.67983251E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.45729533E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.21135711E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.15243226E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.15194707E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.02640024E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.13821230E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.04168443E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.82265236E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.99686504E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.65621093E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.04459755E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.81079471E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.83580593E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.59819232E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.79284522E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     2.47265118E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     8.91947186E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.81894600E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.97606134E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     5.02272654E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.21212289E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     3.49629991E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.17680988E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     5.44527579E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.11967376E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.09124782E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.32287361E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.71208920E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.23568953E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.70940936E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.24510667E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.90634373E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.90513718E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.54628897E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.79854288E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.79854288E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.79854288E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.62892468E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.62892468E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.52620639E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.52620639E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.20964202E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.20964202E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.18639815E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.18639815E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.84812757E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.84812757E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     7.13473932E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.99318510E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.40099058E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.80952955E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.80952955E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.95518776E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.83067375E-04    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51217792E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.58244016E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.47952385E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.13824940E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.45026759E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.98108064E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.88834601E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.99052769E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.99052769E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.81862063E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.58455765E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.33876339E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.59858614E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.53059237E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.52758062E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.62309696E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.81440461E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.39599771E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.82834595E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.82834595E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.13630137E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.78242506E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.55003472E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.80547531E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.10697790E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08292528E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17379948E-01    2           5        -5   # BR(h -> b       bb     )
     6.37931615E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25804297E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78499479E-04    2           3        -3   # BR(h -> s       sb     )
     2.06428269E-02    2           4        -4   # BR(h -> c       cb     )
     6.70286138E-02    2          21        21   # BR(h -> g       g      )
     2.32873965E-03    2          22        22   # BR(h -> gam     gam    )
     1.53835653E-03    2          22        23   # BR(h -> Z       gam    )
     2.00949574E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56344753E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01386657E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38939915E-03    2           5        -5   # BR(H -> b       bb     )
     2.32147543E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20727992E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05082005E-06    2           3        -3   # BR(H -> s       sb     )
     9.48367109E-06    2           4        -4   # BR(H -> c       cb     )
     9.38369467E-01    2           6        -6   # BR(H -> t       tb     )
     7.51439148E-04    2          21        21   # BR(H -> g       g      )
     2.63035514E-06    2          22        22   # BR(H -> gam     gam    )
     1.09182941E-06    2          23        22   # BR(H -> Z       gam    )
     3.17481980E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58307957E-04    2          23        23   # BR(H -> Z       Z      )
     8.52908791E-04    2          25        25   # BR(H -> h       h      )
     8.42109411E-24    2          36        36   # BR(H -> A       A      )
     3.37754683E-11    2          23        36   # BR(H -> Z       A      )
     2.50035449E-12    2          24       -37   # BR(H -> W+      H-     )
     2.50035449E-12    2         -24        37   # BR(H -> W-      H+     )
     8.42586867E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.14142629E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.45779063E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.36309243E-05    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     5.04260101E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.74290145E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.04416581E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05855189E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39228036E-03    2           5        -5   # BR(A -> b       bb     )
     2.29791725E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12396758E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07102784E-06    2           3        -3   # BR(A -> s       sb     )
     9.38561918E-06    2           4        -4   # BR(A -> c       cb     )
     9.39334030E-01    2           6        -6   # BR(A -> t       tb     )
     8.89097504E-04    2          21        21   # BR(A -> g       g      )
     2.69489170E-06    2          22        22   # BR(A -> gam     gam    )
     1.27329686E-06    2          23        22   # BR(A -> Z       gam    )
     3.09431387E-04    2          23        25   # BR(A -> Z       h      )
     6.35193703E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     9.73060216E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.66493154E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     6.15455055E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     5.35247181E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.92913658E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.96123037E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97803597E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23404654E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34688357E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29708120E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42038461E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13819065E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02380385E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41036210E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17139328E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33493302E-12    2          24        36   # BR(H+ -> W+      A      )
     3.76589305E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.33940992E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.44987609E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
