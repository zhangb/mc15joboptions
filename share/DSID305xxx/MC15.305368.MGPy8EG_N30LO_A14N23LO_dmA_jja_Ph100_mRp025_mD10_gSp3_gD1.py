model='dmA'
mR  = 25
mDM= 10000
gSM= 0.3
gDM= 1.0
widthR = 0.856446291447
phminpt = 100
filteff = 0.0033

evgenConfig.description = "Zprime sample - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>", "Chase Shimmin <cshimmin@cern.ch>"]
evgenConfig.minevents = 1000

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")
