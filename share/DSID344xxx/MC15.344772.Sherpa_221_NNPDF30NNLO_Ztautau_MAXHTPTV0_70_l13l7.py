include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4j@LO with 0 GeV < max(HT, pTV) < 70 GeV with light jet filter."
evgenConfig.keywords = ["SM", "Z", "2tau", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "xin.chen@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Ztautau_MAXHTPTV0_70"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  SOFT_SPIN_CORRELATIONS=1

  %settings for MAX(HT,PTV) slicing
  SHERPA_LDADD=SherpaFastjetMAXHTPTV
  HTMIN:=0
  HTMAX:=70

  %settings for decays of first two taus from hard scattering
  DECAYFILE=HadronDecaysTauLL.dat
}(run)

(processes){
  Process 93 93 -> 15 -15 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  Enhance_Factor 0.0 {6}
  End process;
}(processes)

(selector){
  Mass 15 -15 40.0 E_CMS
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep13lep7filter = TauFilter("lep13lep7filter")
  filtSeq += lep13lep7filter

filtSeq.lep13lep7filter.UseNewOptions = True
filtSeq.lep13lep7filter.Ntaus = 2
filtSeq.lep13lep7filter.Nleptaus = 2
filtSeq.lep13lep7filter.Nhadtaus = 0
filtSeq.lep13lep7filter.EtaMaxlep = 2.6
filtSeq.lep13lep7filter.EtaMaxhad = 2.6
filtSeq.lep13lep7filter.Ptcutlep = 7000.0 #MeV
filtSeq.lep13lep7filter.Ptcutlep_lead = 13000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad_lead = 20000.0 #MeV
filtSeq.lep13lep7filter.HasTightRegion = True
filtSeq.lep13lep7filter.Ptcutlep_tight = 10000.0 #MeV
filtSeq.lep13lep7filter.Ptcutlep_tight_lead = 35000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad_tight = 20000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad_tight_lead = 20000.0 #MeV
filtSeq.lep13lep7filter.LooseRejectionFactor = 2.4
