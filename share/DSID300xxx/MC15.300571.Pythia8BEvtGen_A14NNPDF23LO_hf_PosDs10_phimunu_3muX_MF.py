#################################################################################
# job options fragment for hf->PosDs10->phimunu->mu4mu2mu1X 
# For HF tau3mu based on Meson Decay(MD) study for run2
################################################################################# 
# All production channels of bb and cc are included in this fragment.
# D_s meson force decaying into semileptonic channel munu and phi which also must decay into dimuon+X.
# D_s pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>4GeV, mu2>2GeV, mu3>1GeV, and D_s>10GeV
# original version from Dai Kobayashi
#################################################################################

f = open("PosDs_PhiMuNu_3MuX_USER.DEC", "w")
f.write("Alias myPhi phi\n")
f.write("Decay D_s+\n")
f.write("1.0000   myPhi   mu+  nu_mu        PHOTOS ISGW2;\n")
f.write("Enddecay\n")
f.write("Decay myPhi\n")
f.write("0.9193   mu+     mu-               PHOTOS VLL;\n")
f.write("0.0449   gamma   mu+  mu-          PHSP;\n")
f.write("0.0358   pi0     mu+  mu-          PHOTOS PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description = "hf->PosDs->phimunu->3muX inclusive production"
evgenConfig.keywords = ["charmonium", "3muon", "heavyFlavour", "SM", "singlyResonant"]
evgenConfig.minevents = 100
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch']
evgenConfig.process = "hf>PosDs>phimunu>3muX"

genSeq.Pythia8B.Commands += ['HardQCD:all = on']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops = 1

genSeq.EvtInclusiveDecay.userDecayFile = "PosDs_PhiMuNu_3MuX_USER.DEC"

# Add the Filter:
from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
filtSeq += TripletChainFilter("LowPtTripletFilter")
filtSeq += TripletChainFilter("HighPtTripletFilter")
filtSeq.Expression = "LowPtTripletFilter or HighPtTripletFilter"

TripletChainFilter = filtSeq.LowPtTripletFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = 13
TripletChainFilter.PdgId2 = -13
TripletChainFilter.PdgId3 = -13
TripletChainFilter.NStep1 = 2
TripletChainFilter.NStep2 = 2
TripletChainFilter.NStep3 = 1
TripletChainFilter.PtMin1 = 3500
TripletChainFilter.PtMin2 = 3500
TripletChainFilter.PtMin3 = 3500
TripletChainFilter.EtaMax1 = 3.0
TripletChainFilter.EtaMax2 = 3.0
TripletChainFilter.EtaMax3 = 3.0
TripletChainFilter.TripletPdgId = 431
TripletChainFilter.TripletPtMin = 10000
TripletChainFilter.TripletEtaMax = 3.0
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.DoubletPdgId = 333
TripletChainFilter.DoubletPtMin = 0
TripletChainFilter.DoubletEtaMax = 100
TripletChainFilter.DoubletMassMin = 0
TripletChainFilter.DoubletMassMax = 10000000

TripletChainFilter2 = filtSeq.HighPtTripletFilter
TripletChainFilter2.NTriplet = 1
TripletChainFilter2.PdgId1 = 13
TripletChainFilter2.PdgId2 = -13
TripletChainFilter2.PdgId3 = -13
TripletChainFilter2.NStep1 = 2
TripletChainFilter2.NStep2 = 2
TripletChainFilter2.NStep3 = 1
TripletChainFilter2.PtMin1 = 10500
TripletChainFilter2.PtMin2 = 5500
TripletChainFilter2.PtMin3 = 2000
TripletChainFilter2.EtaMax1 = 3.0
TripletChainFilter2.EtaMax2 = 3.0
TripletChainFilter2.EtaMax3 = 3.0
TripletChainFilter2.TripletPdgId = 431
TripletChainFilter2.TripletPtMin = 10000
TripletChainFilter2.TripletEtaMax = 3.0
TripletChainFilter2.TripletMassMin = 0
TripletChainFilter2.TripletMassMax = 10000000
TripletChainFilter2.DoubletPdgId = 333
TripletChainFilter2.DoubletPtMin = 0
TripletChainFilter2.DoubletEtaMax = 100
TripletChainFilter2.DoubletMassMin = 0
TripletChainFilter2.DoubletMassMax = 10000000