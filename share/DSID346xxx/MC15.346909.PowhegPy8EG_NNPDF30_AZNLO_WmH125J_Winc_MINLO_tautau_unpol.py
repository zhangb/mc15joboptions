
evgenConfig.process     = "qq->WmH, W->all, H->tautau unpolarized"
evgenConfig.description = "POWHEG+MiNLO+PYTHIA8, H+Wm+jet, W->all, H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch', 'Ki.Lie@cern.ch' ]
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]
