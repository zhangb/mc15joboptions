# JO for Pythia 8 jet jet + CI JZ11W slice

evgenConfig.description = "Dijet + Contact Interaction truth jet slice JZ11W, with the A14 NNPDF23 LO tune"
evgenConfig.process = "QCD dijet + Contact Interaction"
evgenConfig.keywords = ["exotic", "QCD", "contactInteraction", "jets", "BSM"]
evgenConfig.contact = ["matteo.bauce@cern.ch", "marco.vanadia@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qqbar2gg = on",
                            'ContactInteractions:QCqq2qq = on',
                            'ContactInteractions:QCqqbar2qqbar  = on',
                            'ContactInteractions:Lambda = 25000.',
                            'ContactInteractions:etaLL = -1',
                            "PhaseSpace:pTHatMin = 3500."
                            ]

include("MC15JobOptions/JetFilter_JZ11W.py")
evgenConfig.minevents = 500
