#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15475337E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03968814E+01   # W+
        25     1.25248031E+02   # h
        35     3.00006718E+03   # H
        36     2.99999992E+03   # A
        37     3.00109451E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03709528E+03   # ~d_L
   2000001     3.03184581E+03   # ~d_R
   1000002     3.03618219E+03   # ~u_L
   2000002     3.03360443E+03   # ~u_R
   1000003     3.03709528E+03   # ~s_L
   2000003     3.03184581E+03   # ~s_R
   1000004     3.03618219E+03   # ~c_L
   2000004     3.03360443E+03   # ~c_R
   1000005     8.94879100E+02   # ~b_1
   2000005     3.03016487E+03   # ~b_2
   1000006     8.92581798E+02   # ~t_1
   2000006     3.02327114E+03   # ~t_2
   1000011     3.00665777E+03   # ~e_L
   2000011     3.00139233E+03   # ~e_R
   1000012     3.00526399E+03   # ~nu_eL
   1000013     3.00665777E+03   # ~mu_L
   2000013     3.00139233E+03   # ~mu_R
   1000014     3.00526399E+03   # ~nu_muL
   1000015     2.98608118E+03   # ~tau_1
   2000015     3.02189356E+03   # ~tau_2
   1000016     3.00526316E+03   # ~nu_tauL
   1000021     2.34584349E+03   # ~g
   1000022     1.51224785E+02   # ~chi_10
   1000023     3.22651276E+02   # ~chi_20
   1000025    -2.99804700E+03   # ~chi_30
   1000035     2.99805724E+03   # ~chi_40
   1000024     3.22813288E+02   # ~chi_1+
   1000037     2.99898689E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891844E-01   # N_11
  1  2     3.81745612E-04   # N_12
  1  3    -1.47021989E-02   # N_13
  1  4     3.31791518E-07   # N_14
  2  1     1.66877510E-06   # N_21
  2  2     9.99659266E-01   # N_22
  2  3     2.60698805E-02   # N_23
  2  4     1.30916482E-03   # N_24
  3  1     1.03992653E-02   # N_31
  3  2    -1.93575882E-02   # N_32
  3  3     7.06764044E-01   # N_33
  3  4     7.07108001E-01   # N_34
  4  1    -1.03997913E-02   # N_41
  4  2     1.75068738E-02   # N_42
  4  3    -7.06815954E-01   # N_43
  4  4     7.07104350E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99320594E-01   # U_11
  1  2     3.68557975E-02   # U_12
  2  1    -3.68557975E-02   # U_21
  2  2     9.99320594E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998290E-01   # V_11
  1  2    -1.84955803E-03   # V_12
  2  1    -1.84955803E-03   # V_21
  2  2    -9.99998290E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98535820E-01   # cos(theta_t)
  1  2    -5.40945115E-02   # sin(theta_t)
  2  1     5.40945115E-02   # -sin(theta_t)
  2  2     9.98535820E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99711694E-01   # cos(theta_b)
  1  2    -2.40110158E-02   # sin(theta_b)
  2  1     2.40110158E-02   # -sin(theta_b)
  2  2     9.99711694E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06926616E-01   # cos(theta_tau)
  1  2     7.07286900E-01   # sin(theta_tau)
  2  1    -7.07286900E-01   # -sin(theta_tau)
  2  2    -7.06926616E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00154988E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.54753368E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44006014E+02   # higgs               
         4     1.04625257E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.54753368E+03  # The gauge couplings
     1     3.62255653E-01   # gprime(Q) DRbar
     2     6.38843803E-01   # g(Q) DRbar
     3     1.02564080E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.54753368E+03  # The trilinear couplings
  1  1     2.27721091E-06   # A_u(Q) DRbar
  2  2     2.27724573E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.54753368E+03  # The trilinear couplings
  1  1     7.96097302E-07   # A_d(Q) DRbar
  2  2     7.96194544E-07   # A_s(Q) DRbar
  3  3     1.65673551E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.54753368E+03  # The trilinear couplings
  1  1     3.64445448E-07   # A_e(Q) DRbar
  2  2     3.64463660E-07   # A_mu(Q) DRbar
  3  3     3.69636919E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.54753368E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52269376E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.54753368E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.88504285E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.54753368E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02893525E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.54753368E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.92392840E+04   # M^2_Hd              
        22    -9.05732707E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41680522E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.95040164E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47972930E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47972930E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52027070E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52027070E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.31041581E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.43374546E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.08627867E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.77034679E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15460963E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.71357249E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.82272488E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.67792141E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.04132383E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.95387215E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68458797E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61362555E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.16701512E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.12587487E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.56812137E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.48616200E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.35702586E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.44949635E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.05004254E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.84483914E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.97942725E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.91336386E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.43272688E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.39565257E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.31408586E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.04568657E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.52145446E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.01812054E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.96675020E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63549358E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.63945336E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.90454862E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27313287E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.84248297E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.03170538E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17728548E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59605938E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.37310488E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.09167071E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.09117030E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40394040E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02315039E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.91634332E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63484707E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.72224868E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76966698E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26741004E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.26101376E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.03857514E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66300927E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54761679E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.24599437E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.82174115E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.82036505E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54523826E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.01812054E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.96675020E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63549358E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.63945336E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.90454862E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27313287E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.84248297E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.03170538E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17728548E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59605938E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.37310488E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.09167071E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.09117030E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40394040E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02315039E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.91634332E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63484707E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.72224868E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76966698E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26741004E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.26101376E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.03857514E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66300927E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54761679E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.24599437E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.82174115E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.82036505E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54523826E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96477823E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.86020980E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00606792E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.82482406E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.35423693E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00791086E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.17205037E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55887563E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999999E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.73527643E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.39653859E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.36411263E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96477823E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.86020980E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00606792E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.82482406E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.35423693E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00791086E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.17205037E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55887563E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999999E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.73527643E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.39653859E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.36411263E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.78945464E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.49298156E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17022042E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33679803E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73217684E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57413302E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14304019E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.29498194E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.10894816E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28245793E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.28467888E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96503595E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82844683E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00440279E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.51611772E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.86757843E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01275244E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.66603144E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96503595E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82844683E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00440279E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.51611772E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.86757843E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01275244E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.66603144E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96537101E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82761362E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00414806E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.51470000E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.86636304E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01308018E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.03149620E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.42019889E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.06576888E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.37619272E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.52517509E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.89987283E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.30184696E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.00692987E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.13879374E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.53871231E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.31030808E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.23323462E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.76676538E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.02579707E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03180083E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.84651374E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.16796682E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.16796682E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.73072184E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.37748915E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34478938E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34478938E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.81965628E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.81965628E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.51683239E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.51683239E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.15163602E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01825357E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.43754624E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.08245382E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.08245382E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.28766042E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.41684002E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.21100666E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.21100666E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.72729616E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.72729616E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.32757432E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.32757432E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.65899005E-03   # h decays
#          BR         NDA      ID1       ID2
     5.60511976E-01    2           5        -5   # BR(h -> b       bb     )
     7.10102945E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.51377345E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.33345179E-04    2           3        -3   # BR(h -> s       sb     )
     2.31741997E-02    2           4        -4   # BR(h -> c       cb     )
     7.55440693E-02    2          21        21   # BR(h -> g       g      )
     2.59263204E-03    2          22        22   # BR(h -> gam     gam    )
     1.74576907E-03    2          22        23   # BR(h -> Z       gam    )
     2.35132339E-01    2          24       -24   # BR(h -> W+      W-     )
     2.95039984E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.65307424E+01   # H decays
#          BR         NDA      ID1       ID2
     8.99068936E-01    2           5        -5   # BR(H -> b       bb     )
     6.80728569E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.40689355E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.95567488E-04    2           3        -3   # BR(H -> s       sb     )
     8.28496557E-08    2           4        -4   # BR(H -> c       cb     )
     8.26478281E-03    2           6        -6   # BR(H -> t       tb     )
     9.64136478E-06    2          21        21   # BR(H -> g       g      )
     5.29275013E-08    2          22        22   # BR(H -> gam     gam    )
     3.38115285E-09    2          23        22   # BR(H -> Z       gam    )
     7.87537194E-07    2          24       -24   # BR(H -> W+      W-     )
     3.93282903E-07    2          23        23   # BR(H -> Z       Z      )
     5.61228261E-06    2          25        25   # BR(H -> h       h      )
    -2.94506942E-24    2          36        36   # BR(H -> A       A      )
     1.40848676E-19    2          23        36   # BR(H -> Z       A      )
     8.83709988E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.23089505E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.41891460E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.74641420E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23926980E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.34354915E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.57176691E+01   # A decays
#          BR         NDA      ID1       ID2
     9.19561093E-01    2           5        -5   # BR(A -> b       bb     )
     6.96213949E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.46164271E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.02340841E-04    2           3        -3   # BR(A -> s       sb     )
     8.53163000E-08    2           4        -4   # BR(A -> c       cb     )
     8.50616438E-03    2           6        -6   # BR(A -> t       tb     )
     2.50498403E-05    2          21        21   # BR(A -> g       g      )
     6.35992643E-08    2          22        22   # BR(A -> gam     gam    )
     2.46583588E-08    2          23        22   # BR(A -> Z       gam    )
     8.02426127E-07    2          23        25   # BR(A -> Z       h      )
     9.38238009E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.37158362E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.69128486E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.85734705E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.90352151E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46660201E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.37276176E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.25325312E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.38624134E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.32417905E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.72426210E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.22932929E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.35493569E-07    2          24        25   # BR(H+ -> W+      h      )
     5.29619603E-14    2          24        36   # BR(H+ -> W+      A      )
     5.19022044E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.79600667E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08327141E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
