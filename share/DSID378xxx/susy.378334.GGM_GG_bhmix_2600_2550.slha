#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.55566603E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.55000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05429437E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414015E+03   # H
        36     2.00000000E+03   # A
        37     2.00169321E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002534E+03   # ~d_R
   1000002     4.99989277E+03   # ~u_L
   2000002     4.99994932E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002534E+03   # ~s_R
   1000004     4.99989277E+03   # ~c_L
   2000004     4.99994932E+03   # ~c_R
   1000005     4.99904381E+03   # ~b_1
   2000005     5.00111534E+03   # ~b_2
   1000006     4.97627998E+03   # ~t_1
   2000006     5.02807191E+03   # ~t_2
   1000011     5.00008189E+03   # ~e_L
   2000011     5.00007602E+03   # ~e_R
   1000012     4.99984209E+03   # ~nu_eL
   1000013     5.00008189E+03   # ~mu_L
   2000013     5.00007602E+03   # ~mu_R
   1000014     4.99984209E+03   # ~nu_muL
   1000015     4.99940027E+03   # ~tau_1
   2000015     5.00075817E+03   # ~tau_2
   1000016     4.99984209E+03   # ~nu_tauL
   1000021     2.60000000E+03   # ~g
   1000022     2.54346232E+03   # ~chi_10
   1000023    -2.55145203E+03   # ~chi_20
   1000025     2.56204305E+03   # ~chi_30
   1000035     3.00161269E+03   # ~chi_40
   1000024     2.55054771E+03   # ~chi_1+
   1000037     3.00161222E+03   # ~chi_2+
   1000039     5.90745646E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.68556618E-01   # N_11
  1  2    -2.69930396E-02   # N_12
  1  3    -5.85103126E-01   # N_13
  1  4    -5.77640961E-01   # N_14
  2  1     8.53496860E-03   # N_21
  2  2    -1.39455009E-02   # N_22
  2  3     7.06972013E-01   # N_23
  2  4    -7.07052508E-01   # N_24
  3  1     8.22597341E-01   # N_31
  3  2     2.12733809E-02   # N_32
  3  3     3.97036281E-01   # N_33
  3  4     4.06501229E-01   # N_34
  4  1     2.03473084E-03   # N_41
  4  2    -9.99311934E-01   # N_42
  4  3     1.43908388E-02   # N_43
  4  4     3.41236215E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.04092646E-02   # U_11
  1  2     9.99791709E-01   # U_12
  2  1     9.99791709E-01   # U_21
  2  2    -2.04092646E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     4.82863719E-02   # V_11
  1  2    -9.98833533E-01   # V_12
  2  1     9.98833533E-01   # V_21
  2  2     4.82863719E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07492545E-01   # cos(theta_t)
  1  2    -7.06720807E-01   # sin(theta_t)
  2  1     7.06720807E-01   # -sin(theta_t)
  2  2     7.07492545E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.88562764E-01   # cos(theta_b)
  1  2     7.25176751E-01   # sin(theta_b)
  2  1    -7.25176751E-01   # -sin(theta_b)
  2  2    -6.88562764E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05576492E-01   # cos(theta_tau)
  1  2     7.08633766E-01   # sin(theta_tau)
  2  1    -7.08633766E-01   # -sin(theta_tau)
  2  2    -7.05576492E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90195080E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.55000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52134650E+02   # vev(Q)              
         4     5.17084309E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52650955E-01   # gprime(Q) DRbar
     2     6.26336830E-01   # g(Q) DRbar
     3     1.07627160E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02731766E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73450588E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79487110E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.55566603E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.21819779E+06   # M^2_Hd              
        22    -1.13601565E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36674466E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.50633008E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.41583072E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     6.90350323E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.15554126E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.58733700E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.02669491E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.23548898E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.98650085E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.74375840E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.15554126E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.58733700E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.02669491E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.23548898E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.98650085E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.74375840E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.53045153E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     9.96885804E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.15477121E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     8.40216733E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     8.40216733E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     8.40216733E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     8.40216733E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.08438233E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.92063449E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.83215309E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.95465855E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.09054076E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.67955266E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.15070221E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.53717582E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.26122804E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.92903017E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.32417611E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.19447966E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.52168356E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.51558326E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.01633556E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.64987117E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.74118085E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.61689950E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.64140181E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.91209629E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11067070E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.07814637E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.25552782E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.22947968E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.77319689E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.24142577E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.26731032E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.72250629E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.28368425E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.16028721E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.60414251E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.10002349E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67010159E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.93789793E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     9.40154036E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.89286202E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.61738723E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.87875505E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.23299309E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.58912268E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.49244383E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.26345682E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.83480362E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.61798842E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.20823824E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61182592E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67018362E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.10787540E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.49159690E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.08702913E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.62494173E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.14326683E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.25169844E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.58972345E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.44904890E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.25335296E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.29951211E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.74122278E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.11120710E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90004663E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67010159E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.93789793E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     9.40154036E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.89286202E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.61738723E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.87875505E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.23299309E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.58912268E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.49244383E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.26345682E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.83480362E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.61798842E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.20823824E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61182592E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67018362E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.10787540E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.49159690E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.08702913E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.62494173E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.14326683E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.25169844E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.58972345E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.44904890E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.25335296E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.29951211E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.74122278E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.11120710E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90004663E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.73328311E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.37003173E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.26218593E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.11012765E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.90981079E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.25475369E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.83859230E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.34999567E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.25506341E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.30307579E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.74417524E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.10425707E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.73328311E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.37003173E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.26218593E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.11012765E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.90981079E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.25475369E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.83859230E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.34999567E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.25506341E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.30307579E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.74417524E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.10425707E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.04070051E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35133239E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.94213144E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.81014295E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.93712567E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.25997983E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.88685705E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     5.04888666E-10    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.04784027E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.25491438E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.54302678E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.86422167E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.95341654E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.45234219E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.91955915E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.73307941E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     4.72549644E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.37292896E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.57981653E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.92294656E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.82177525E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.82693146E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.73307941E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     4.72549644E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.37292896E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.57981653E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.92294656E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.82177525E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.82693146E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.73483357E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     4.72246545E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.37204835E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.57495474E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.92107175E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.46263311E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.82318785E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.24055855E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.43112046E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.11982746E-04    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.59904688E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.96919187E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.19968809E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.19835635E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     8.87484942E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.30239768E-01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53089512E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.50527652E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47958696E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.94602653E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.48963874E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.61276015E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.13130275E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.50068805E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.55282186E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.83815176E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     4.21651673E-04    2     1000039        35   # BR(~chi_10 -> ~G        H)
     1.04121807E-02    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.19588503E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     7.40937144E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.00048897E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.89709096E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.16456224E-04    2     1000039        35   # BR(~chi_20 -> ~G        H)
     8.87821350E-06    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.41517479E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.83269550E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.14212998E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.82423333E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.18679883E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.18307120E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.01560329E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.36210483E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.36210483E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.36210483E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.30495880E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.30495880E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.34986333E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.34986333E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.06873037E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.06873037E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.93509801E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.69436928E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.26330232E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.60913971E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     6.61575362E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     5.21445530E-08    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.53920319E-06    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     5.30126510E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.23692110E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.95084072E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.22865556E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.10187345E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.97354299E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.97259225E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.74239071E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.13833251E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.13833251E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.13833251E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.55564469E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.19476984E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.94213206E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.17580798E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.64231296E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.64365104E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.64281032E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.38589573E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.28271574E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.28271574E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.28271574E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.83268422E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.83268422E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     2.63057234E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     2.63057234E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     9.44217194E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     9.44217194E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     9.43818609E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     9.43818609E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     8.40689951E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     8.40689951E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     9.30236852E-01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.71474583E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.12104827E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.26990678E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.49558877E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.49558877E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.26382764E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.35104822E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.90376463E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.06207884E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.54467454E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.67235737E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.01655589E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.94383263E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07176333E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16517347E-01    2           5        -5   # BR(h -> b       bb     )
     6.39652541E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26413442E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79790703E-04    2           3        -3   # BR(h -> s       sb     )
     2.06998194E-02    2           4        -4   # BR(h -> c       cb     )
     6.72095344E-02    2          21        21   # BR(h -> g       g      )
     2.30749378E-03    2          22        22   # BR(h -> gam     gam    )
     1.54286147E-03    2          22        23   # BR(h -> Z       gam    )
     2.01346737E-01    2          24       -24   # BR(h -> W+      W-     )
     2.57047489E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78120476E+01   # H decays
#          BR         NDA      ID1       ID2
     1.49084103E-03    2           5        -5   # BR(H -> b       bb     )
     2.46433342E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71233612E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548747E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666574E-05    2           4        -4   # BR(H -> c       cb     )
     9.96052771E-01    2           6        -6   # BR(H -> t       tb     )
     7.97540606E-04    2          21        21   # BR(H -> g       g      )
     2.73120142E-06    2          22        22   # BR(H -> gam     gam    )
     1.16053532E-06    2          23        22   # BR(H -> Z       gam    )
     3.32566149E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65829664E-04    2          23        23   # BR(H -> Z       Z      )
     8.98072988E-04    2          25        25   # BR(H -> h       h      )
     8.40052342E-24    2          36        36   # BR(H -> A       A      )
     3.47133302E-11    2          23        36   # BR(H -> Z       A      )
     6.09967979E-12    2          24       -37   # BR(H -> W+      H-     )
     6.09967979E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82388996E+01   # A decays
#          BR         NDA      ID1       ID2
     1.49352640E-03    2           5        -5   # BR(A -> b       bb     )
     2.43893430E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62251382E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675396E-06    2           3        -3   # BR(A -> s       sb     )
     9.96158961E-06    2           4        -4   # BR(A -> c       cb     )
     9.96978456E-01    2           6        -6   # BR(A -> t       tb     )
     9.43659049E-04    2          21        21   # BR(A -> g       g      )
     3.05510721E-06    2          22        22   # BR(A -> gam     gam    )
     1.35319968E-06    2          23        22   # BR(A -> Z       gam    )
     3.24096610E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74512519E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.40758440E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49233645E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81130968E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.53086560E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45678884E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08728583E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404310E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32224729E-04    2          24        25   # BR(H+ -> W+      h      )
     4.88658844E-13    2          24        36   # BR(H+ -> W+      A      )
