include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.description = "Generation of gg > H > Sh where S goes to everything with a single lepton filter"
evgenConfig.keywords = ["BSMHiggs"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'Higgs:clipWings = off',
                            '36:m0 = 320.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 160.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes',
                            '25:onMode = off',
                            '25:onIfMatch = 22 22',
                            '35:onMode = off',
                            '35:onIfMatch = 24 -24'
                            ]

if not hasattr( filtSeq, "DecaysFinalStateFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
  filtSeq += DecaysFinalStateFilter()
  pass

DecaysFinalStateFilter = filtSeq.DecaysFinalStateFilter
DecaysFinalStateFilter.PDGAllowedParents = [ -24, 24 ]
DecaysFinalStateFilter.NChargedLeptons = 1

