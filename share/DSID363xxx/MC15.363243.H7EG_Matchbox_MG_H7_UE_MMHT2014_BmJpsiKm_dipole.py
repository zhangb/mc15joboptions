##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B-\n")
f.write("1.0000  myJ/psi   K-             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

## Provide config information
evgenConfig.description = "Exclusive Bplus->Jpsi_mu3p5mu3p5_Kplus production"
evgenConfig.keywords = ["exclusive","Bplus","Jpsi","2muon"]
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "MMHT2014"
evgenConfig.contact     = ["Javier Llorente (javier.llorente.merino@cern.ch)"]
evgenConfig.minevents = 500

# initialize Herwig7 generator configuration for built-in matrix elements
from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigMatchbox import Hw7ConfigMatchbox
genSeq += Herwig7()
## initialize generator configuration object
Herwig7Config = Hw7ConfigMatchbox(genSeq, runArgs, run_name="HerwigMatchbox", beams="pp")
# configure Herwig7
include("MC15JobOptions/Herwig7_EvtGen.py")
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()

import os

Herwig7Config.add_commands("""
## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in

## Set the order of the couplings
cd /Herwig/MatrixElements/Matchbox
set Factory:OrderInAlphaS 2
set Factory:OrderInAlphaEW 0

## Select the process
## You may use identifiers such as p, pbar, j, l, mu+, h0 etc.
do Factory:Process p p -> j j

#read Matchbox/MadGraph-NJet.in
read Matchbox/MadGraph-OpenLoops.in

## cuts on additional jets
cd /Herwig/Cuts/
read Matchbox/DefaultPPJets.in

insert JetCuts:JetRegions 0 FirstJet
set FirstJet:PtMin 11

## Scale choice
cd /Herwig/MatrixElements/Matchbox
set Factory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/MaxJetPtScale

## Matching and shower selection
#read Matchbox/LO-DefaultShower.in
read Matchbox/LO-DipoleShower.in

## PDF choice
read Matchbox/FiveFlavourNoBMassScheme.in
read Matchbox/MMHT2014.in

do /Herwig/MatrixElements/Matchbox/Factory:ProductionMode
""")

#Replicate authors NLO sampler commands in Interface
Herwig7Config.sampler_commands("MonacoSampler", 20000, 4, 50000, 1, 100)

## run generator
Herwig7Config.run(cleanup_herwig_scratch=True)

##Add EvtGen
#include("MC15JobOptions/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
genSeq.EvtInclusiveDecay.readExisting = True
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[-521]
genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"
evgenConfig.auxfiles += [ 'BU_JPSI_K_USER.DEC' ]

##Filter for Bp
include("MC15JobOptions/BSignalFilter.py")
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 5500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 5500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = -521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

#Jet filter
include("MC15JobOptions/JetFilterAkt4.py")
filtSeq.QCDTruthJetFilter.MinPt = 15*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 13000*GeV
