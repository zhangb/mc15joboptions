evgenConfig.description = "Protos VLT single production (T singlet, M = 900 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["You Zhou <y.zhou@cern.ch>"]
evgenConfig.inputfilecheck = "VLQ_Tbj_M900V01MOD1"
evgenConfig.process = "pp>Tbq"

include("MC15JobOptions/ProtosLHEF_Common.py") 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

