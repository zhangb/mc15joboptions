#---------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
genSeq.Pythia8.Commands += ["25:oneChannel = 1 0.5 100 24 -24",
                            "25:addChannel = 1 0.5 100 5 -5",
                            "24:oneChannel = 1 0.333 0 11 -12",
                            "24:addChannel = 1 0.333 0 13 -14",
                            "24:addChannel = 1 0.333 0 15 -16"]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["aMcAtNlo", "Pythia8"]
evgenConfig.description    = "SM diHiggs production, decay to WWbb (leplep), with MG5_aMC@NLO, inclusive of box diagram FF."
evgenConfig.keywords       = ["SM", "SMHiggs", "nonResonant", "bbbar", "WW"]
evgenConfig.contact        = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 
evgenConfig.minevents      = 5000
evgenConfig.maxeventsfactor = 1.0

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
include("MC15JobOptions/MultiElecMuTauFilter.py")
filtSeq.MultiElecMuTauFilter.MinPt = 7000. # 7 GeV, in accordance with 2015 to 2018 dilepton triggers?
filtSeq.MultiElecMuTauFilter.NLeptons = 2
filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
filtSeq.Expression = "hbbFilter and hWWFilter and MultiElecMuTauFilter"
