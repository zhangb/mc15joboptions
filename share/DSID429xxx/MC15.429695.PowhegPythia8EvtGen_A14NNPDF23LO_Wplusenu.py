## POWHEG+Pythia8 Wplus->enu

evgenConfig.description = "POWHEG+Pythia8 Wplus->enu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "lepton", "electron"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_Powheg.py")
include("MC15JobOptions/nonStandard/Pythia8_Photospp.py")
