include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "WWW (-> lvlv jj) +Oj@NLO+1,2j@LO."
evgenConfig.keywords = ["SM", "triboson", "multilepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "ssun@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "222_NNPDF30NNLO_WWW"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  %tags for process setup
  NJET:=2; LJET:=3; QCUT:=30;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;

  %decay setup
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,12,-11]=2
  HDH_STATUS[24,14,-13]=2
  HDH_STATUS[24,16,-15]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2
  HDH_STATUS[-24,-12,11]=2
  HDH_STATUS[-24,-14,13]=2
  HDH_STATUS[-24,-16,15]=2
}(run);

(processes){
  Process 901 901 -> 24 -24 -24 901{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes);
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[24]=0" ]

evgenLog.info('2Lep6GeV filter is applied')

include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
filtSeq.MultiElecMuTauFilter.MinPt  = 6000.
filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
filtSeq.MultiElecMuTauFilter.NLeptons = 2
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0

#include( 'MC15JobOptions/LeptonFilter.py' )
#filtSeq.LeptonFilter.Ptcut = 15000.
#filtSeq.LeptonFilter.Etacut = 2.8

filtSeq.Expression = "MultiElecMuTauFilter"

evt_multiplier = 10

if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevts=runArgs.maxEvents*evt_multiplier
    else:
        nevts=5000*evt_multiplier
