include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2,3,4j@LO with 140 GeV < pTV < 220 GeV, Mjj > 1000 GeV."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch", "bjr@sas.upenn.edu"]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Zmumu_PTV140_220_MJJ1000_E_CMS_KtMerging_LO"

genSeq.Sherpa_i.RunCard = """
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=0; QCUT:=20.;

  % Fastjet/antikt merging criterion.
  % SHERPA_LDADD=SherpaFastJetCriterion SherpaFastjetSelector;
  JET_CRITERION FASTJET[A:antikt,R:0.4,y:5];

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93 93{NJET};

  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
  "PT" 13,-13  140,220
  FastjetSelector Mass(p[4]+p[5])>1000. antikt 2 20. 0. 0.4
}(selector)
"""

genSeq.Sherpa_i.OpenLoopsLibs = ["ppll", "ppllj", "pplljj"]
genSeq.Sherpa_i.ExtraFiles = [ "libSherpaFastJetCriterion.so", "libSherpaFastjetSelector.so" ]
