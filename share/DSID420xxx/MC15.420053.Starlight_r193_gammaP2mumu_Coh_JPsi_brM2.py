evgenConfig.description = "Starlight gamma + P UPC collisions at 5020 GeV to JPsi -> 2 mu (coherent production), breakup mode 2"
evgenConfig.keywords = ["2photon","2lepton"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["marcin.guzik@cern.ch"]

# TODO: Sort out proper param setting based on runArgs.ecmEnergy
if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)

include("MC15JobOptions/Starlight_Common.py")

genSeq.Starlight.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     # TODO: Calculate this from runArgs.ecmEnergy
     "beam1Gamma 2705.", #2672.89",   #Gamma of the colliding ion1, for sqrt(nn)=2.76 TeV
     "beam2Gamma 2705.", #2672.89",   #Gamma of the colliding ion2, for sqrt(nn)=2.76 TeV
     "maxW -1", #Max value of w
     "minW -1", #Min value of w
     "nmbWBins 1000", #Bins n w
     "maxRapidity 9.", #max y
     "nmbRapidityBins 1000", #Bins n y
     "accCutPt 0", #Cut in pT? 0 = (no, 1 = yes)
     "minPt 2.0", #Minimum pT in GeV
     "maxPt 50.0", #Maximum pT in GeV
     "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "minEta -2.7", #Minimum pseudorapidity
     "maxEta 2.7", #Maximum pseudorapidity
     "productionMode 3", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 443013", #Channel of interest
     "beamBreakupMode 2", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120", #Number of pt bins when interference is turned on
     "xsecMethod 0", #Set to 0 to use old method for calculating gamma-gamma luminosity
     "nThreads 1", #Number of threads used for calculating luminosity (when using the new method)
     "pythFullRec 0" #Write full pythia information to output (vertex, parents, daughter etc)
    ]

#   "beamLorentzGamma 5020",   #Gamma of the colliding ions
#   "outputFormat 2", #Form of the output

