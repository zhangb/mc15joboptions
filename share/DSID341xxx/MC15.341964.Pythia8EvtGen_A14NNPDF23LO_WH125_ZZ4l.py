
#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 23 23', # Higgs decay
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16',
                             '23:mMin = 2.0',
                             '23:onMode = off',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 15 15'
                             ]




#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "PYTHIA8+EVTGEN, WH, W->any, H->ZZ->4l mh=125 GeV "
evgenConfig.keywords    = [  "Higgs", "WHiggs", "ZZ"]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch, jochen.meyer@cern.ch,roberth@cern.ch' ]


evgenConfig.generators  = [ "Pythia8", "EvtGen"] 
