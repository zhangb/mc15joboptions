##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B-\n")
f.write("1.0000  myJ/psi   K-             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
include("MC15JobOptions/Sherpa_2.2.5_Base_Fragment.py")
evgenConfig.tune = "CT10"

evgenConfig.description = "QCD dijets with a B+/- meson"
evgenConfig.keywords = [ "jets", "dijet", "LO", "QCD", "SM" ]
evgenConfig.contact  = [ "javier.llorente.merino@cern.ch" ]
evgenConfig.minevents = 100

evgenConfig.process="""
(run){
 ACTIVE[25]=0;
 PDF_LIBRARY LHAPDFSherpa; PDF_SET CT14nnlo;
 CORE_SCALE QCD;
 CSS_FS_AS_FAC 1;
 CSS_FS_PT2MIN 3;
 CSS_IS_AS_FAC 1;
 CSS_IS_PT2MIN 3;
}(run)

(processes){
 Process 93 93 -> 93 93;
 Order (*,0);
 Integration_Error 0.02 {2};
 End process;                 
}(processes)

(selector){
 NJetFinder  2  7.0  0.0  0.4  -1  999.0  10.0
 NJetFinder  1  11.0  0.0  0.4  -1  999.0  10.0
}(selector)           
"""

genSeq.Sherpa_i.Parameters += [ "PDF_VARIATIONS=CT14nnlo[all] NNPDF30_nnlo_as_0118 MMHT2014nnlo68cl" ]

#Let EvtGen do the B decay 
evgenConfig.generators += ["Sherpa"]
include("MC15JobOptions/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
genSeq.EvtInclusiveDecay.readExisting = True
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[-521]
genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"
evgenConfig.auxfiles += [ 'BU_JPSI_K_USER.DEC' ]

##Filter for Bp
include("MC15JobOptions/BSignalFilter.py")
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 5500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 5500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = -521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

#Jet filter
include("MC15JobOptions/JetFilterAkt4.py")
filtSeq.QCDTruthJetFilter.MinPt = 15*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 13000*GeV
