include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Jason Veatch <Jason.Veacth@cern.ch>']
evgenConfig.description = "Generation of gg > H > Sh where S and h decay to W+W- with a final state consisting of 3 leptons, 2 positive and 1 negative"
evgenConfig.keywords = ["BSMHiggs"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'Higgs:clipWings = off',
                            '36:m0 = 340.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '35:m0 = 200.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes',
                            '35:mayDecay = on',
                            '35:onMode = off',
                            '35:onIfAll = 24 24',
                            '25:mMin = 50.0',
                            '25:onMode = off',
                            '25:onIfMatch = 24 -24',
                            '24:onMode = off',
                            '24:onIfAny = 11 13 15',
                            '24:onNegIfAny = 1 2 3 4'
                            ]

if not hasattr( filtSeq, "DecaysFinalStateFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
  filtSeq += DecaysFinalStateFilter()
  pass

DecaysFinalStateFilter = filtSeq.DecaysFinalStateFilter
DecaysFinalStateFilter.PDGAllowedParents = [ -24, 24 ]
DecaysFinalStateFilter.NChargedLeptons = 3

