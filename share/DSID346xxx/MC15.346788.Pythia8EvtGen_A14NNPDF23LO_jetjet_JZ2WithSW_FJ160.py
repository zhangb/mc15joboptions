# JO for Pythia 8 jet jet JZ2 slice with fat jet filter of 160 GeV

evgenConfig.description = "Dijet truth jet slice JZ2 with fat jet filter of 160 GeV, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["amoroso@cern.ch","christopher.young@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 15."]



include("MC15JobOptions/JetFilter_JZ2.py")

include("MC15JobOptions/AntiKt10TruthJets.py")
from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
if "QCDTruthJetFilterLargeR" not in filtSeq:
    filtSeq += QCDTruthJetFilter("QCDTruthJetFilterLargeR")
filtSeq.QCDTruthJetFilterLargeR.TruthJetContainer = "AntiKt10TruthJets"
filtSeq.QCDTruthJetFilterLargeR.MinPt = 160*GeV

evgenConfig.minevents = 1000

