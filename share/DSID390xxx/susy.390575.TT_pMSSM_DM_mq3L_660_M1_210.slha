#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12089389E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04162036E+01   # W+
        25     1.25348144E+02   # h
        35     4.00000640E+03   # H
        36     3.99999667E+03   # A
        37     4.00102902E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02325380E+03   # ~d_L
   2000001     4.02039079E+03   # ~d_R
   1000002     4.02259689E+03   # ~u_L
   2000002     4.02157479E+03   # ~u_R
   1000003     4.02325380E+03   # ~s_L
   2000003     4.02039079E+03   # ~s_R
   1000004     4.02259689E+03   # ~c_L
   2000004     4.02157479E+03   # ~c_R
   1000005     7.30320164E+02   # ~b_1
   2000005     4.02388250E+03   # ~b_2
   1000006     7.19125936E+02   # ~t_1
   2000006     2.25052959E+03   # ~t_2
   1000011     4.00420915E+03   # ~e_L
   2000011     4.00304153E+03   # ~e_R
   1000012     4.00309101E+03   # ~nu_eL
   1000013     4.00420915E+03   # ~mu_L
   2000013     4.00304153E+03   # ~mu_R
   1000014     4.00309101E+03   # ~nu_muL
   1000015     4.00495121E+03   # ~tau_1
   2000015     4.00731538E+03   # ~tau_2
   1000016     4.00476473E+03   # ~nu_tauL
   1000021     1.97681492E+03   # ~g
   1000022     2.01994425E+02   # ~chi_10
   1000023    -2.71275629E+02   # ~chi_20
   1000025     2.79738671E+02   # ~chi_30
   1000035     2.05356890E+03   # ~chi_40
   1000024     2.68227055E+02   # ~chi_1+
   1000037     2.05373357E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99181382E-01   # N_11
  1  2    -1.04103298E-02   # N_12
  1  3    -3.52075572E-01   # N_13
  1  4    -2.59629081E-01   # N_14
  2  1    -6.91181510E-02   # N_21
  2  2     2.54877919E-02   # N_22
  2  3    -7.00633570E-01   # N_23
  2  4     7.09708147E-01   # N_24
  3  1     4.32081715E-01   # N_31
  3  2     2.79537627E-02   # N_32
  3  3     6.20600853E-01   # N_33
  3  4     6.53741968E-01   # N_34
  4  1    -9.56602531E-04   # N_41
  4  2     9.99229996E-01   # N_42
  4  3    -3.15818100E-03   # N_43
  4  4    -3.90963701E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.47153650E-03   # U_11
  1  2     9.99990003E-01   # U_12
  2  1    -9.99990003E-01   # U_21
  2  2     4.47153650E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.53031124E-02   # V_11
  1  2    -9.98469612E-01   # V_12
  2  1    -9.98469612E-01   # V_21
  2  2    -5.53031124E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96539122E-01   # cos(theta_t)
  1  2    -8.31250764E-02   # sin(theta_t)
  2  1     8.31250764E-02   # -sin(theta_t)
  2  2     9.96539122E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999668E-01   # cos(theta_b)
  1  2    -8.14861884E-04   # sin(theta_b)
  2  1     8.14861884E-04   # -sin(theta_b)
  2  2     9.99999668E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05312996E-01   # cos(theta_tau)
  1  2     7.08896027E-01   # sin(theta_tau)
  2  1    -7.08896027E-01   # -sin(theta_tau)
  2  2    -7.05312996E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00285461E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20893888E+03  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43852394E+02   # higgs               
         4     1.61636951E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20893888E+03  # The gauge couplings
     1     3.61730822E-01   # gprime(Q) DRbar
     2     6.36460527E-01   # g(Q) DRbar
     3     1.03189235E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20893888E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16291937E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20893888E+03  # The trilinear couplings
  1  1     4.26995729E-07   # A_d(Q) DRbar
  2  2     4.27035941E-07   # A_s(Q) DRbar
  3  3     7.64223016E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20893888E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.27689429E-08   # A_mu(Q) DRbar
  3  3     9.37131327E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20893888E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68014081E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20893888E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82413924E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20893888E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03588643E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20893888E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58172167E+07   # M^2_Hd              
        22    -8.38642241E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40540775E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.27159219E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45364950E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45364950E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54635050E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54635050E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.63559437E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.45061082E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.32117686E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.84215819E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09160386E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.32644901E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.73086951E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20567828E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.05532709E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.71386031E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.38072210E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.05170999E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.23072001E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.37079799E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.12198914E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.97911755E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.00325632E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.56867372E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.21190266E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.04374783E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.81756758E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66853208E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52697355E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77037917E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63253916E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.78376596E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58333585E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.42638010E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14539244E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.32549441E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.45176847E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.59363061E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.21581640E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78865510E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22217907E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.32923308E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04297863E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79068238E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29458843E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56918505E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52593417E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61324566E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.47078480E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.63160565E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02785133E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.79481408E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44750450E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78885935E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83855141E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.28829846E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.51329614E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79549658E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.46154899E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60102782E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52812584E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54536998E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16643427E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.86588222E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.68167226E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.28874681E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85585319E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78865510E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22217907E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.32923308E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04297863E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79068238E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29458843E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56918505E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52593417E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61324566E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.47078480E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.63160565E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02785133E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.79481408E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44750450E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78885935E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83855141E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.28829846E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.51329614E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79549658E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.46154899E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60102782E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52812584E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54536998E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16643427E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.86588222E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.68167226E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.28874681E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85585319E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14873787E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27778130E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.66257012E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.79640175E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77584750E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.03076949E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56556170E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07165977E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09250532E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.76199513E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.85986973E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.99803239E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14873787E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27778130E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.66257012E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.79640175E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77584750E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.03076949E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56556170E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07165977E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09250532E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.76199513E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.85986973E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.99803239E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10404524E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92111054E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28172755E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.06909536E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39737493E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44697631E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80173461E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10350181E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.04370570E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40074851E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53285845E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42199704E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10273874E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85109009E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14979831E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38541523E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.12871756E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.40157888E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77907369E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10439161E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54302210E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14979831E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38541523E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.12871756E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.40157888E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77907369E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10439161E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54302210E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47995522E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25450514E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.92757829E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17465843E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51796627E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68560192E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02222677E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.73367829E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33450522E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33450522E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11151036E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11151036E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10796884E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.98847363E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.72977680E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.61250102E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.75576055E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.57117405E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.60525254E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.80158575E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.65750556E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.09271003E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.51363487E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17496179E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52385490E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17496179E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52385490E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45720236E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49284616E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49284616E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47141539E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98307298E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98307298E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98307269E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.29927986E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.29927986E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.29927986E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.29927986E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.09976138E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.09976138E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.09976138E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.09976138E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.94928907E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.94928907E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.07624850E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.56339005E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.93371011E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.16121354E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50385501E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.16121354E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50385501E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45952277E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.42948277E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.42948277E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.41775654E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.89908555E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.89908555E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.89907993E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     9.69901303E-05    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.25796741E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     9.69901303E-05    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.25796741E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.88381246E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.88381246E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.16761760E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     5.76419129E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     5.76419129E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     5.76419135E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.52076297E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.52076297E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.52076297E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.52076297E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.06916885E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.06916885E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.06916885E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.06916885E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.51492423E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.51492423E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.98732349E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.68345961E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.94130935E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.44517507E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.58863183E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.58863183E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.01753408E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.78456419E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.45369105E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82907169E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.82907169E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.84232318E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.84232318E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01055241E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96678644E-01    2           5        -5   # BR(h -> b       bb     )
     6.48712926E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29644758E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87156244E-04    2           3        -3   # BR(h -> s       sb     )
     2.11563383E-02    2           4        -4   # BR(h -> c       cb     )
     6.90844353E-02    2          21        21   # BR(h -> g       g      )
     2.37436128E-03    2          22        22   # BR(h -> gam     gam    )
     1.60807753E-03    2          22        23   # BR(h -> Z       gam    )
     2.16255258E-01    2          24       -24   # BR(h -> W+      W-     )
     2.72547916E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.47346559E+01   # H decays
#          BR         NDA      ID1       ID2
     3.48914852E-01    2           5        -5   # BR(H -> b       bb     )
     6.05758648E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14181586E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53339891E-04    2           3        -3   # BR(H -> s       sb     )
     7.10620340E-08    2           4        -4   # BR(H -> c       cb     )
     7.12189531E-03    2           6        -6   # BR(H -> t       tb     )
     9.17190787E-07    2          21        21   # BR(H -> g       g      )
     1.24854120E-10    2          22        22   # BR(H -> gam     gam    )
     1.82925895E-09    2          23        22   # BR(H -> Z       gam    )
     1.88371611E-06    2          24       -24   # BR(H -> W+      W-     )
     9.41205468E-07    2          23        23   # BR(H -> Z       Z      )
     7.22383421E-06    2          25        25   # BR(H -> h       h      )
    -9.73888359E-25    2          36        36   # BR(H -> A       A      )
     4.93914058E-20    2          23        36   # BR(H -> Z       A      )
     1.85659704E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63352140E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63352140E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.95894654E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03786665E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.05713287E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.83523082E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.84251487E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.02953886E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.47505512E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.99467473E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.35981399E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.62542435E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.44691097E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.44691097E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.38283013E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.47307491E+01   # A decays
#          BR         NDA      ID1       ID2
     3.48965459E-01    2           5        -5   # BR(A -> b       bb     )
     6.05805146E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14197858E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53400460E-04    2           3        -3   # BR(A -> s       sb     )
     7.15212682E-08    2           4        -4   # BR(A -> c       cb     )
     7.13552996E-03    2           6        -6   # BR(A -> t       tb     )
     1.46617092E-05    2          21        21   # BR(A -> g       g      )
     5.34707843E-08    2          22        22   # BR(A -> gam     gam    )
     1.61143709E-08    2          23        22   # BR(A -> Z       gam    )
     1.87991022E-06    2          23        25   # BR(A -> Z       h      )
     1.86044084E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63365904E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63365904E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.70759133E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29527897E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.73172075E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.50887172E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48436770E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.03826275E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.04209354E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.08591094E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.25676099E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.48818854E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.48818854E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47536929E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.58741849E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05707577E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14163360E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.57594477E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21327545E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49609777E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.55978424E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.88123589E-06    2          24        25   # BR(H+ -> W+      h      )
     2.81813578E-14    2          24        36   # BR(H+ -> W+      A      )
     6.82413249E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.85613122E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.97615906E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63509216E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.07168312E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60846656E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.26309518E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.61027993E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.92091342E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
