#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.89527279E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04188418E+01   # W+
        25     1.25366227E+02   # h
        35     4.00000490E+03   # H
        36     3.99999553E+03   # A
        37     4.00099333E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01234724E+03   # ~d_L
   2000001     4.01173998E+03   # ~d_R
   1000002     4.01168823E+03   # ~u_L
   2000002     4.01317016E+03   # ~u_R
   1000003     4.01234724E+03   # ~s_L
   2000003     4.01173998E+03   # ~s_R
   1000004     4.01168823E+03   # ~c_L
   2000004     4.01317016E+03   # ~c_R
   1000005     4.30837729E+02   # ~b_1
   2000005     4.01788006E+03   # ~b_2
   1000006     4.12019583E+02   # ~t_1
   2000006     2.02364883E+03   # ~t_2
   1000011     4.00201588E+03   # ~e_L
   2000011     4.00275979E+03   # ~e_R
   1000012     4.00089818E+03   # ~nu_eL
   1000013     4.00201588E+03   # ~mu_L
   2000013     4.00275979E+03   # ~mu_R
   1000014     4.00089818E+03   # ~nu_muL
   1000015     4.00425947E+03   # ~tau_1
   2000015     4.00836968E+03   # ~tau_2
   1000016     4.00352257E+03   # ~nu_tauL
   1000021     1.95753594E+03   # ~g
   1000022     2.02883678E+02   # ~chi_10
   1000023    -2.70439600E+02   # ~chi_20
   1000025     2.79322324E+02   # ~chi_30
   1000035     2.05798881E+03   # ~chi_40
   1000024     2.67592582E+02   # ~chi_1+
   1000037     2.05815298E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99140689E-01   # N_11
  1  2    -1.04359668E-02   # N_12
  1  3    -3.52146003E-01   # N_13
  1  4    -2.59673458E-01   # N_14
  2  1    -6.91331602E-02   # N_21
  2  2     2.55464712E-02   # N_22
  2  3    -7.00622345E-01   # N_23
  2  4     7.09715657E-01   # N_24
  3  1     4.32163983E-01   # N_31
  3  2     2.80166006E-02   # N_32
  3  3     6.20573527E-01   # N_33
  3  4     6.53710838E-01   # N_34
  4  1    -9.58996846E-04   # N_41
  4  2     9.99226470E-01   # N_42
  4  3    -3.16536461E-03   # N_43
  4  4    -3.91857410E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.48174145E-03   # U_11
  1  2     9.99989957E-01   # U_12
  2  1    -9.99989957E-01   # U_21
  2  2     4.48174145E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54297125E-02   # V_11
  1  2    -9.98462592E-01   # V_12
  2  1    -9.98462592E-01   # V_21
  2  2    -5.54297125E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96109237E-01   # cos(theta_t)
  1  2    -8.81271125E-02   # sin(theta_t)
  2  1     8.81271125E-02   # -sin(theta_t)
  2  2     9.96109237E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999671E-01   # cos(theta_b)
  1  2    -8.11171925E-04   # sin(theta_b)
  2  1     8.11171925E-04   # -sin(theta_b)
  2  2     9.99999671E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05074439E-01   # cos(theta_tau)
  1  2     7.09133299E-01   # sin(theta_tau)
  2  1    -7.09133299E-01   # -sin(theta_tau)
  2  2    -7.05074439E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00282326E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.95272793E+02  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44575993E+02   # higgs               
         4     1.62724927E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.95272793E+02  # The gauge couplings
     1     3.60741478E-01   # gprime(Q) DRbar
     2     6.36034713E-01   # g(Q) DRbar
     3     1.03874890E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.95272793E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.67121779E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.95272793E+02  # The trilinear couplings
  1  1     2.08028643E-07   # A_d(Q) DRbar
  2  2     2.08048841E-07   # A_s(Q) DRbar
  3  3     3.72425138E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.95272793E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.59550198E-08   # A_mu(Q) DRbar
  3  3     4.64159409E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.95272793E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75204576E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.95272793E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85004715E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.95272793E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03299106E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.95272793E+02  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57982442E+07   # M^2_Hd              
        22    -1.01024392E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40341252E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.39653689E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45343204E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45343204E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54656796E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54656796E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.72161094E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.05885061E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.94114939E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19959539E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.68382385E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18831056E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03636221E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.34586451E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.49733115E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.11571499E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.34803420E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.03939070E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.92256920E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.11538801E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.96204279E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66372720E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51972168E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76040844E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62358948E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.74435736E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.56277955E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.34658136E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14990930E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.61836367E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.92224521E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.67125993E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.37797609E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77825407E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.21445144E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.34898796E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04270007E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76461942E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.32633329E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.51705173E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53380014E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60721067E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.45147100E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.62181874E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02394509E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.78041973E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44983629E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77844933E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83187964E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29243681E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.49662284E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76945529E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.48227356E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.54894723E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53601707E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53966814E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16126144E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.83955697E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.67117167E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.24970968E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85647811E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77825407E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.21445144E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.34898796E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04270007E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76461942E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.32633329E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.51705173E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53380014E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60721067E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.45147100E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.62181874E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02394509E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.78041973E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44983629E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77844933E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83187964E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29243681E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.49662284E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76945529E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.48227356E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.54894723E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53601707E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53966814E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16126144E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.83955697E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.67117167E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.24970968E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85647811E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13054077E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27706419E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.51698111E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.80025443E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77594887E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.04810766E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56580499E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06013235E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09166096E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.76448010E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86068923E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.00728629E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13054077E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27706419E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.51698111E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.80025443E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77594887E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.04810766E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56580499E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06013235E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09166096E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.76448010E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86068923E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.00728629E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08865792E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92027991E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28926942E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.07817058E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39659713E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46183380E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80019558E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09077905E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.03813234E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.41075214E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53272459E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42342221E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10676820E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85396883E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13162955E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38513996E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13620724E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.39941024E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77913878E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13090528E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54310911E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13162955E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38513996E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13620724E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.39941024E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77913878E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13090528E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54310911E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46240032E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25364473E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.93342127E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17164068E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51766298E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.70610604E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02158341E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.20327687E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33457741E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33457741E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11153491E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11153491E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10777536E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.76947113E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.86836227E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.57955432E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.76271467E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.03116503E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.76382673E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.90308122E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.18357319E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.89502188E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.66123096E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.79262023E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17514334E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52359391E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17514334E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52359391E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.46049686E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48948118E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48948118E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46734968E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.97530038E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.97530038E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.97529992E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.71858345E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.71858345E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.71858345E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.71858345E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.06195659E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.06195659E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.06195659E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.06195659E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.18715917E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.18715917E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.51407664E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.68114170E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.70070849E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.15943085E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50096802E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15943085E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50096802E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45580714E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.41951512E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.41951512E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.40711732E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.87967406E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.87967406E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.87966490E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.42542679E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.84818534E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.42542679E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.84818534E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.23349731E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.23349731E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.28142858E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.46066993E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.46066993E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.46067007E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.93044528E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.93044528E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.93044528E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.93044528E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.43476536E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.43476536E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.43476536E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.43476536E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.75534702E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.75534702E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.76852457E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.28736499E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.51883551E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.18245643E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.88733063E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.88733063E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.55922402E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.46966300E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.05507811E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90270998E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90270998E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.91302236E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.91302236E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.00840208E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96412958E-01    2           5        -5   # BR(h -> b       bb     )
     6.49146663E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29798222E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87468185E-04    2           3        -3   # BR(h -> s       sb     )
     2.11701463E-02    2           4        -4   # BR(h -> c       cb     )
     6.87456865E-02    2          21        21   # BR(h -> g       g      )
     2.38423535E-03    2          22        22   # BR(h -> gam     gam    )
     1.61167373E-03    2          22        23   # BR(h -> Z       gam    )
     2.16712672E-01    2          24       -24   # BR(h -> W+      W-     )
     2.73306957E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51829678E+01   # H decays
#          BR         NDA      ID1       ID2
     3.54408287E-01    2           5        -5   # BR(H -> b       bb     )
     6.00837198E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12441480E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51281660E-04    2           3        -3   # BR(H -> s       sb     )
     7.04838133E-08    2           4        -4   # BR(H -> c       cb     )
     7.06394553E-03    2           6        -6   # BR(H -> t       tb     )
     7.61368134E-07    2          21        21   # BR(H -> g       g      )
     2.77509058E-10    2          22        22   # BR(H -> gam     gam    )
     1.81575030E-09    2          23        22   # BR(H -> Z       gam    )
     1.85174778E-06    2          24       -24   # BR(H -> W+      W-     )
     9.25232527E-07    2          23        23   # BR(H -> Z       Z      )
     7.06187766E-06    2          25        25   # BR(H -> h       h      )
    -1.25983305E-24    2          36        36   # BR(H -> A       A      )
    -1.42811891E-20    2          23        36   # BR(H -> Z       A      )
     1.85031514E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61536260E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61536260E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.94313533E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03166598E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.04806609E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.81101156E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.82680515E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.00623836E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.39916977E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.90632832E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.28704373E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.07335766E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.06110758E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.06110758E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43817722E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51816913E+01   # A decays
#          BR         NDA      ID1       ID2
     3.54442276E-01    2           5        -5   # BR(A -> b       bb     )
     6.00854363E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12447382E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51329622E-04    2           3        -3   # BR(A -> s       sb     )
     7.09367820E-08    2           4        -4   # BR(A -> c       cb     )
     7.07721698E-03    2           6        -6   # BR(A -> t       tb     )
     1.45418960E-05    2          21        21   # BR(A -> g       g      )
     5.30829602E-08    2          22        22   # BR(A -> gam     gam    )
     1.59900283E-08    2          23        22   # BR(A -> Z       gam    )
     1.84791654E-06    2          23        25   # BR(A -> Z       h      )
     1.85389667E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61542088E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61542088E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.69388935E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.28733600E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.65591212E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.47880332E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.47169784E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.01715079E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.03370568E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.99530095E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.18754626E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.12625056E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.12625056E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52204265E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.67845129E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00582663E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12351316E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.63420573E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20301123E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47498096E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.61604500E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.84864899E-06    2          24        25   # BR(H+ -> W+      h      )
     2.35693983E-14    2          24        36   # BR(H+ -> W+      A      )
     6.76357971E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.81337548E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.96069492E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61635733E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.02594200E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59005091E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.24848208E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.09265054E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.17342861E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
