#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MG+Pythia8  single top + Z production with A14 tune'
evgenConfig.keywords    = ['SM','singleTop','tZ','lepton']
evgenConfig.contact     = ['alhroob@cern.ch' ]

include('MC15JobOptions/MadGraphControl_SM_tZq_LO_Pythia8_A14.py')

