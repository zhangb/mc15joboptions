include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 

evgenConfig.description = "ADD Graviton, n=6, MD=3000 GeV, Jet Filter"
evgenConfig.keywords = ["exotic", "graviton", "BSM"]
evgenConfig.process = "parton + parton -> graviton + jet"
evgenConfig.contact = ["Olof Lundberg"]

genSeq.Pythia8.Commands += [ 'ExtraDimensionsLED:qqbar2Gg = on',       # Process type.
                             'ExtraDimensionsLED:qg2Gq = on',
                             'ExtraDimensionsLED:gg2Gg = on',  
                             'ExtraDimensionsLED:n = 6',               # Number of extra dimensions.
                             'ExtraDimensionsLED:MD = 3000',           # Choice of the scale, MD.
                             'ExtraDimensionsLED:CutOffmode = 0',      # Treatment of the effective theory (0: all the events. 1 : truncate events with s_hat>MD^2, with a weight of MD^2/s_hat^4).
                             'PhaseSpace:pTHatMin = 150.',              # pT Cut at the generator level.
                             '5000039:m0 = 3300.',                      # Central value of the Breit-Wigner mass resonance
                             '5000039:mWidth = 1300.',                  # Resonance width
                             '5000039:mMin = 1.',                      # Minimum mass of the Breit-Wigner distribution.
                             '5000039:mMax = 13990.']#,                  # Maximum mass of the Breit-Wigner distribution.

######## Filter Section #######                                                                                                       

include("MC15JobOptions/JetFilterAkt4.py")                                                                                                                                                                   
filtSeq.QCDTruthJetFilter.MinPt = 100*GeV 
 
evgenLog.info('Leading jet 100 GeV filter is applied')

 
##### End of filter section ###### 

