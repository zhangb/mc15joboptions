import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=0
ihtmax=70
HTrange='lowHT'
include('MC15JobOptions/MadGraphControl_Wjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=2000

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter)"

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Wmunu_Ht0_70_13TeV"
