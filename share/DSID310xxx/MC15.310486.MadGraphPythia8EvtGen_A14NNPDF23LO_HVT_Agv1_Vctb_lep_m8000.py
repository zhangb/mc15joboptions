from EvgenProdTools.EvgenProdToolsConf import TestHepMC
testHepMC = TestHepMC()
testHepMC.MaxVtxDisp = 1500.0

include("MC15JobOptions/MadGraphControl_HVT.py")
