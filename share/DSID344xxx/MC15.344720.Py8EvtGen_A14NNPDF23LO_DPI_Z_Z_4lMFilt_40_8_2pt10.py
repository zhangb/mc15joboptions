
include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )


genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # generate Z bosons
                            "SecondHard:generate = on",       # second hard process
                            "SecondHard:SingleGmZ = on",      # another Z
                            "23:mMin = 4 ",                   # Z/Gm* mass down to 8 GeV
                            "23:onMode = off",                # switch off all Z decays
                            "23:onIfAny = 11 13 15",          # switch on Z->ll decays
                            "23:onIfAny = 11 13 15"]          


#--------------------------------------------------------------
# ZZ->llll m12 m34 cuts + 2l pt>10GeV filters
#--------------------------------------------------------------
include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt = 4000.
filtSeq.FourLeptonMassFilter.MaxEta = 3.5
filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu = False
filtSeq.FourLeptonMassFilter.AllowSameCharge = True

include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 10000.    
filtSeq.MultiLeptonFilter.Etacut = 3.5
filtSeq.MultiLeptonFilter.NLeptons = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "DDY 4l with at least two lepton pt>10 GeV + m12 m34 filter"
evgenConfig.keywords    = [ "SM","ZZ" ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch' ]
evgenConfig.minevents   = 200
evgenConfig.generators  = ['Pythia8', 'EvtGen']
