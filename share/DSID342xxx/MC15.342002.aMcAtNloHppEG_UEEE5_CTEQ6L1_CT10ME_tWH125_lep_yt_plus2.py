include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

# tWH sample with top-Yukawa coupling ct = +2
# To modify Higgs BR
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL

set /Herwig/Particles/h0/h0->b,bbar;:OnOff Off
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff Off
set /Herwig/Particles/h0/h0->c,cbar;:OnOff Off
set /Herwig/Particles/h0/h0->t,tbar;:OnOff Off
set /Herwig/Particles/h0/h0->g,g;:OnOff Off
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff Off
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio 0.0632
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio 0.2150
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio 0.0264
decaymode h0->s,sbar; 0.0 1 /Herwig/Decays/Hff
set /Herwig/Particles/h0/h0->s,sbar;:OnOff Off
decaymode h0->Z0,gamma; 0.0 1 /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff Off
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = "aMC@NLO tWH(125GeV) H->tautau/WW/ZZ ct=+2 showered with Herwig++"
evgenConfig.keywords += ['Higgs', 'SMHiggs', 'ttHiggs']
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'twh'

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
