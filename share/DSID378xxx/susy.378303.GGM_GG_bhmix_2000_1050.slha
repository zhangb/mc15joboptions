#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.05375698E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05407653E+01   # W+
        25     1.25000000E+02   # h
        35     2.00415115E+03   # H
        36     2.00000000E+03   # A
        37     2.00179026E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013267E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989264E+03   # ~u_L
   2000002     4.99994939E+03   # ~u_R
   1000003     5.00013267E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989264E+03   # ~c_L
   2000004     4.99994939E+03   # ~c_R
   1000005     4.99965197E+03   # ~b_1
   2000005     5.00050742E+03   # ~b_2
   1000006     4.99157586E+03   # ~t_1
   2000006     5.01287622E+03   # ~t_2
   1000011     5.00008206E+03   # ~e_L
   2000011     5.00007592E+03   # ~e_R
   1000012     4.99984202E+03   # ~nu_eL
   1000013     5.00008206E+03   # ~mu_L
   2000013     5.00007592E+03   # ~mu_R
   1000014     4.99984202E+03   # ~nu_muL
   1000015     4.99979957E+03   # ~tau_1
   2000015     5.00035902E+03   # ~tau_2
   1000016     4.99984202E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.04324671E+03   # ~chi_10
   1000023    -1.05238204E+03   # ~chi_20
   1000025     1.06128783E+03   # ~chi_30
   1000035     3.00160448E+03   # ~chi_40
   1000024     1.05135804E+03   # ~chi_1+
   1000037     3.00160421E+03   # ~chi_2+
   1000039     4.62714837E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.06100018E-01   # N_11
  1  2    -5.78875803E-03   # N_12
  1  3    -5.71407639E-01   # N_13
  1  4    -5.53265370E-01   # N_14
  2  1     2.06775765E-02   # N_21
  2  2    -1.91090579E-02   # N_22
  2  3     7.06666270E-01   # N_23
  2  4    -7.06986609E-01   # N_24
  3  1     7.95119483E-01   # N_31
  3  2     5.49194067E-03   # N_32
  3  3     4.17197293E-01   # N_33
  3  4     4.40115059E-01   # N_34
  4  1     4.63152993E-04   # N_41
  4  2    -9.99785563E-01   # N_42
  4  3    -7.90645877E-03   # N_43
  4  4     1.91337562E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.11730634E-02   # U_11
  1  2     9.99937579E-01   # U_12
  2  1     9.99937579E-01   # U_21
  2  2     1.11730634E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.70587849E-02   # V_11
  1  2    -9.99633844E-01   # V_12
  2  1     9.99633844E-01   # V_21
  2  2     2.70587849E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08047709E-01   # cos(theta_t)
  1  2    -7.06164600E-01   # sin(theta_t)
  2  1     7.06164600E-01   # -sin(theta_t)
  2  2     7.08047709E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.61248615E-01   # cos(theta_b)
  1  2     7.50166828E-01   # sin(theta_b)
  2  1    -7.50166828E-01   # -sin(theta_b)
  2  2    -6.61248615E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03216239E-01   # cos(theta_tau)
  1  2     7.10976034E-01   # sin(theta_tau)
  2  1    -7.10976034E-01   # -sin(theta_tau)
  2  2    -7.03216239E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201732E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51852753E+02   # vev(Q)              
         4     4.36927470E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52808600E-01   # gprime(Q) DRbar
     2     6.27338800E-01   # g(Q) DRbar
     3     1.08044461E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02723386E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72744747E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79778078E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.05375698E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.82021288E+06   # M^2_Hd              
        22    -6.44969575E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37121383E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.77243279E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.39648817E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.23186078E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.50281738E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.39716448E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.10394635E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.25343546E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.12488491E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.86512333E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.82054862E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.39716448E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.10394635E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.25343546E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.12488491E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.86512333E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.82054862E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.55453352E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.03380898E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.29380866E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.20833947E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.74044508E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.09676650E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.00753476E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.00753476E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.00753476E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.00753476E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.32623537E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.32623537E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.95471887E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.30677131E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.07697969E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.72881214E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.40825163E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.07286963E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.81252585E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.12451458E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.08921010E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.24901702E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11372362E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.04940332E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.17272204E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.12179442E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.34114158E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.18325356E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.29321615E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.44959110E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.65412719E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.62486345E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.55507180E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.35032565E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.11520019E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.11153719E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.35566351E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60427021E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.05403254E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.54325578E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.77976772E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.49646224E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.56524313E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.90550739E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.10013933E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.56419317E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.90552780E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.95839932E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.69254268E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.20276710E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.38412807E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86179142E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.95807334E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.82227172E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.11754678E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.12621380E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.77994385E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.50493965E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.10018666E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.17285805E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.33884545E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.68997877E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69431630E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.05080605E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38959759E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86234128E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88542123E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.73131168E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.49795851E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.11684293E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24109409E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87146346E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.10013933E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.56419317E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.90552780E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.95839932E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.69254268E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.20276710E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.38412807E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86179142E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.95807334E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.82227172E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.11754678E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.12621380E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.77994385E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.50493965E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.10018666E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.17285805E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.33884545E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.68997877E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69431630E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.05080605E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38959759E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86234128E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88542123E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.73131168E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.49795851E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.11684293E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24109409E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87146346E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.96884492E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.77203157E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.36950399E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.23195612E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.69484982E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.50380353E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.39415015E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26090540E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.68095954E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.27736244E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.31476214E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.61243737E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.96884492E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.77203157E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.36950399E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.23195612E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.69484982E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.50380353E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.39415015E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26090540E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.68095954E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.27736244E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.31476214E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.61243737E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.61401830E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.04796639E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.64841395E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.40304030E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51423547E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.14311870E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.03096566E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     6.42708296E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62454359E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.90456751E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.36012785E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.44788887E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54021254E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.07562217E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08297358E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96865895E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.24813314E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.68969885E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17290055E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69740722E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.81994044E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.39036927E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.96865895E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.24813314E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.68969885E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17290055E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69740722E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.81994044E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.39036927E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97159614E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.24096893E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.68407503E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17174123E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69474105E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.86959015E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.38504085E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.32362960E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.45350127E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.52700963E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04391493E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17567668E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17468049E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.33368137E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.59075094E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53091804E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49618616E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46040576E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.79400604E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53308930E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.36208012E-08    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.02350866E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.18331958E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.49250511E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.32417531E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.96160612E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.64669829E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02607341E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91154753E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.39549067E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.80685549E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18878504E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80046219E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.12611985E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.12329833E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.24900953E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.23992185E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.23992185E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.23992185E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.32105172E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.32105172E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.40350604E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.40350604E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.17927160E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.17927160E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.91262874E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.13435244E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.72907185E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.17634155E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.30194615E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.75057249E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.31467590E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.61460816E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.31167608E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.26342633E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     5.67492258E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     5.67202088E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.92324929E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.03557403E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.03557403E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.03557403E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.39990069E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.10737660E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.02601240E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.09581239E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.09609659E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.09099508E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.50696927E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.41704199E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.41704199E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.41704199E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.39592034E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.39592034E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26455903E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26455903E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.65301899E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.65301899E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.65038722E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.65038722E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.98515796E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.98515796E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     9.59100719E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.35790337E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.70694355E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.05913640E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46412911E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46412911E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.76665805E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.16198913E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.98575336E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.27556184E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.03435029E-08    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.96527403E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.90980354E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.06291613E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07835328E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17071240E-01    2           5        -5   # BR(h -> b       bb     )
     6.38631652E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26052084E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79024775E-04    2           3        -3   # BR(h -> s       sb     )
     2.06661877E-02    2           4        -4   # BR(h -> c       cb     )
     6.71038682E-02    2          21        21   # BR(h -> g       g      )
     2.30177178E-03    2          22        22   # BR(h -> gam     gam    )
     1.54024900E-03    2          22        23   # BR(h -> Z       gam    )
     2.01085227E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56632135E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78129612E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48091348E-03    2           5        -5   # BR(H -> b       bb     )
     2.46426545E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71209580E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11545567E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666617E-05    2           4        -4   # BR(H -> c       cb     )
     9.96053502E-01    2           6        -6   # BR(H -> t       tb     )
     7.97620926E-04    2          21        21   # BR(H -> g       g      )
     2.74679617E-06    2          22        22   # BR(H -> gam     gam    )
     1.15987368E-06    2          23        22   # BR(H -> Z       gam    )
     3.34584923E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66836216E-04    2          23        23   # BR(H -> Z       Z      )
     9.04156296E-04    2          25        25   # BR(H -> h       h      )
     8.76957320E-24    2          36        36   # BR(H -> A       A      )
     3.51743561E-11    2          23        36   # BR(H -> Z       A      )
     5.10000823E-12    2          24       -37   # BR(H -> W+      H-     )
     5.10000823E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82385969E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48373497E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895361E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62258207E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676296E-06    2           3        -3   # BR(A -> s       sb     )
     9.96166847E-06    2           4        -4   # BR(A -> c       cb     )
     9.96986348E-01    2           6        -6   # BR(A -> t       tb     )
     9.43666519E-04    2          21        21   # BR(A -> g       g      )
     2.97320415E-06    2          22        22   # BR(A -> gam     gam    )
     1.35247628E-06    2          23        22   # BR(A -> Z       gam    )
     3.26068856E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74524255E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38598194E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237919E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81146077E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51703996E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45684659E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729732E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402270E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34282566E-04    2          24        25   # BR(H+ -> W+      h      )
     6.45653520E-13    2          24        36   # BR(H+ -> W+      A      )
