# JO for Pythia 8 jet jet JZ0W slice

evgenConfig.description = "Dijet truth jet slice JZ0W, with the A14 NNPDF23 LO Var1Up tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

evgenConfig.contact = ["marco.vanadia@cern.ch"]
evgenConfig.process = "QCD dijet"
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ0W.py")
evgenConfig.minevents = 1000
