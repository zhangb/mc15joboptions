#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP ttbar production with Powheg hdamp equal top mass, UEEE5 tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'teng.jian.khoo@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwigpp", "EvtGen"]
evgenConfig.minevents = 1000

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp        = 172.5
# compensate filter efficiency
PowhegConfig.nEvents     *= 500.
PowhegConfig.generate()

#--------------------------------------------------------------
# HerwigPP (UEEE5) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')

## Add to commands                                                                                                                                                                                         
cmds = """                                                                                                                                                                                                    
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General                                                                                                                                       
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost                                                                                                                           
"""
genSeq.Herwigpp.Commands += cmds.splitlines()
 
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# Configure the HT filter
include('MC15JobOptions/HTFilter.py')
filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 1000.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 1500.*GeV # Max HT to keep event
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

#  Run EvtGen as afterburner
#include('MC15JobOptions/Herwigpp_EvtGen.py')

