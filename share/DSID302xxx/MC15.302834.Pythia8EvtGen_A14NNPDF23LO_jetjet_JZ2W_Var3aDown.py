# JO for Pythia 8 jet jet JZ2W slice

evgenConfig.description = "Dijet truth jet slice JZ2W, with the A14 NNPDF23 LO Var3aDown tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

evgenConfig.contact = ["marco.vanadia@cern.ch"]
evgenConfig.process = "QCD dijet"
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 15."]

include("MC15JobOptions/JetFilter_JZ2W.py")
evgenConfig.minevents = 500
