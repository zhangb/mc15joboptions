#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 4' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, QCD WmWm + 2 jets"
evgenConfig.keywords    = [ "SM", "VBS", "QCD", "WW", "2jet", "NLO" ]
evgenConfig.contact     = [ "Christian Johnson <christian.johnson@cern.ch>" ]
evgenConfig.process     = "QCD ssWW"
evgenConfig.inputfilecheck = 'QCD_WmWm'
evgenConfig.minevents   = 5000
