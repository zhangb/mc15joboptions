#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

# In 20.7.9.9.6, LHE merging means this is no longer needed
#evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
ma = 6 #Set scalar mass


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')


#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']


#--------------------------------------------------------------
# Higgs->aa at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = off',
                            '35:onMode = off',
                            '35:onIfMatch = 36 23', # h->Za
                            '36:onMode = off',
                            '23:onMode = off',
                            ]


#--------------------------------------------------------------
# a->XX at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            '23:mMin = 2.0',
                            '23:mMax = %.1f' % (125 - ma),
                            '23:onIfAny = 11 13 15', #Set to only non neutrino leptons
                            '36:onIfAny = 13', #a decay product
                            '36:m0 %.1f' % ma, #scalar mass
                            '36:mMin %.1f' % (ma-0.5), #scalar mass
                            '36:mMax %.1f' % (ma+0.5), #scalar mass
                            '36:mWidth 0.001', # narrow width
                            '36:tau0 10', #scalarlife time
                            ]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.minevents = 10000
evgenConfig.inputFilesPerJob = 5
evgenConfig.description = "POWHEG+PYTHIA8, ggH H->aZ -> (Z -> ll, a ->2mu)"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","Z", "mH125" ]
evgenConfig.contact     = [ 'trevor.willard.nelson@cern.ch' ] #Add my email to this thing
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen" ]