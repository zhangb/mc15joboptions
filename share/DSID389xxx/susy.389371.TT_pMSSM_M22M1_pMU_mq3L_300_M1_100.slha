#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.94980285E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     2.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03986753E+01   # W+
        25     1.25845291E+02   # h
        35     3.00021646E+03   # H
        36     3.00000041E+03   # A
        37     3.00101072E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00922400E+03   # ~d_L
   2000001     3.00378952E+03   # ~d_R
   1000002     3.00829627E+03   # ~u_L
   2000002     3.00653553E+03   # ~u_R
   1000003     3.00922400E+03   # ~s_L
   2000003     3.00378952E+03   # ~s_R
   1000004     3.00829627E+03   # ~c_L
   2000004     3.00653553E+03   # ~c_R
   1000005     2.71922669E+02   # ~b_1
   2000005     3.00351015E+03   # ~b_2
   1000006     2.63649580E+02   # ~t_1
   2000006     2.99020537E+03   # ~t_2
   1000011     3.00711178E+03   # ~e_L
   2000011     3.00038780E+03   # ~e_R
   1000012     3.00571281E+03   # ~nu_eL
   1000013     3.00711178E+03   # ~mu_L
   2000013     3.00038780E+03   # ~mu_R
   1000014     3.00571281E+03   # ~nu_muL
   1000015     2.98613891E+03   # ~tau_1
   2000015     3.02240670E+03   # ~tau_2
   1000016     3.00608299E+03   # ~nu_tauL
   1000021     2.31004575E+03   # ~g
   1000022     1.01125547E+02   # ~chi_10
   1000023     2.14286830E+02   # ~chi_20
   1000025    -3.00748431E+03   # ~chi_30
   1000035     3.00794010E+03   # ~chi_40
   1000024     2.14444481E+02   # ~chi_1+
   1000037     3.00862835E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888469E-01   # N_11
  1  2    -1.54173570E-03   # N_12
  1  3     1.48039883E-02   # N_13
  1  4    -1.23069524E-03   # N_14
  2  1     1.93396027E-03   # N_21
  2  2     9.99648544E-01   # N_22
  2  3    -2.62629965E-02   # N_23
  2  4     3.05000501E-03   # N_24
  3  1    -9.56936965E-03   # N_31
  3  2     1.64309944E-02   # N_32
  3  3     7.06829405E-01   # N_33
  3  4     7.07128448E-01   # N_34
  4  1    -1.13020614E-02   # N_41
  4  2     2.07468839E-02   # N_42
  4  3     7.06741317E-01   # N_43
  4  4    -7.07077464E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99308974E-01   # U_11
  1  2    -3.71695241E-02   # U_12
  2  1     3.71695241E-02   # U_21
  2  2     9.99308974E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990678E-01   # V_11
  1  2    -4.31786446E-03   # V_12
  2  1     4.31786446E-03   # V_21
  2  2     9.99990678E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98912015E-01   # cos(theta_t)
  1  2    -4.66346040E-02   # sin(theta_t)
  2  1     4.66346040E-02   # -sin(theta_t)
  2  2     9.98912015E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99924633E-01   # cos(theta_b)
  1  2     1.22771462E-02   # sin(theta_b)
  2  1    -1.22771462E-02   # -sin(theta_b)
  2  2     9.99924633E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06878153E-01   # cos(theta_tau)
  1  2     7.07335335E-01   # sin(theta_tau)
  2  1    -7.07335335E-01   # -sin(theta_tau)
  2  2     7.06878153E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.02026032E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.49802848E+02  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.45274351E+02   # higgs               
         4     5.57193680E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.49802848E+02  # The gauge couplings
     1     3.60645349E-01   # gprime(Q) DRbar
     2     6.39211455E-01   # g(Q) DRbar
     3     1.03652148E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.49802848E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.58812905E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.49802848E+02  # The trilinear couplings
  1  1     1.84847477E-07   # A_d(Q) DRbar
  2  2     1.84886339E-07   # A_s(Q) DRbar
  3  3     4.35705464E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.49802848E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.06251044E-08   # A_mu(Q) DRbar
  3  3     4.10990713E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.49802848E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.61696166E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.49802848E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.10487271E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.49802848E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04840736E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.49802848E+02  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.92332728E+04   # M^2_Hd              
        22    -9.22073428E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     2.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41835716E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.82694543E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48227910E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48227910E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51772090E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51772090E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.69194035E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09291127E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.92943994E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.66633387E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.34855932E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.13532144E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66963498E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.53598995E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.05809474E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.06141974E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.56252833E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.43747167E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.29083358E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.81555349E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.98178684E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -7.31702901E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28770692E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.89949854E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.25805615E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.14980355E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.08939287E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.69082212E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62471126E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.40462112E-11    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.52856973E-12    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.24923757E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.06914295E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.23536087E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54846485E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.75256673E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.45152940E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.09453285E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.87816262E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62169733E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.68139415E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.40921138E-11    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24350682E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.87312648E-11    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.07601423E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.72123221E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.40184462E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.63526888E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55981390E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.08939287E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.69082212E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62471126E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.40462112E-11    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.52856973E-12    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.24923757E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.06914295E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.23536087E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54846485E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.75256673E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.45152940E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.09453285E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.87816262E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62169733E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.68139415E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.40921138E-11    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24350682E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.87312648E-11    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.07601423E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.72123221E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.40184462E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.63526888E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55981390E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01380906E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.61605534E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01857825E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01981622E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54887255E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996289E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.71138323E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01380906E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.61605534E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01857825E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01981622E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54887255E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996289E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.71138323E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81048496E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.44220471E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19018165E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36761364E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75059988E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.52325533E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16337361E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.15977897E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.05733554E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.31323171E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.71828140E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01416707E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.71634095E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00373283E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.02463308E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.01416707E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.71634095E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00373283E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.02463308E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.01500738E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.71550924E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00348159E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.02496749E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.00750659E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.26259801E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.87332376E-13    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.87332376E-13    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.58902955E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.48766943E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.38225161E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.23640439E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     9.23640439E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.72178209E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.85279708E-10    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.85279708E-10    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.17424098E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.52206733E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.00779037E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.38236916E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.66123063E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.63598125E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.29553050E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.20690909E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.98506478E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.34627246E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.34627246E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.80031650E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.52251229E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     6.48812051E-12    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     6.48812051E-12    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.23100891E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.23100891E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     6.48812051E-12    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     6.48812051E-12    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.23100891E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.23100891E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.71522703E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.71522703E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.20408327E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.20408327E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.91035396E-13    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     4.91035396E-13    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     1.70335113E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.70335113E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.91035396E-13    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     4.91035396E-13    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.70335113E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.70335113E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.15018413E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.15018413E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     4.34588538E-11    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     4.34588538E-11    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     4.34588538E-11    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     4.34588538E-11    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.71971919E-11    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.71971919E-11    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     9.20095087E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.74079505E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52674628E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.41306234E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.41306234E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.24332658E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.14507033E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.94983246E-11    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.94983246E-11    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     3.93215123E-11    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.93215123E-11    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.94983246E-11    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.94983246E-11    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     3.93215123E-11    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.93215123E-11    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.69632101E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.69632101E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.22897549E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.22897549E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.16026450E-12    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     4.16026450E-12    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     2.71788825E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.71788825E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     4.16026450E-12    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     4.16026450E-12    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     2.71788825E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.71788825E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.40589317E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.40589317E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.07113003E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.07113003E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.07113003E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.07113003E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     7.44761783E-11    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     7.44761783E-11    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.12498029E-03   # h decays
#          BR         NDA      ID1       ID2
     6.69816579E-01    2           5        -5   # BR(h -> b       bb     )
     5.13218055E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.81677772E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.85070111E-04    2           3        -3   # BR(h -> s       sb     )
     1.66084350E-02    2           4        -4   # BR(h -> c       cb     )
     5.67880046E-02    2          21        21   # BR(h -> g       g      )
     1.87182745E-03    2          22        22   # BR(h -> gam     gam    )
     1.31398569E-03    2          22        23   # BR(h -> Z       gam    )
     1.79033935E-01    2          24       -24   # BR(h -> W+      W-     )
     2.26786805E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38303387E+01   # H decays
#          BR         NDA      ID1       ID2
     7.37597551E-01    2           5        -5   # BR(H -> b       bb     )
     1.79809701E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.35764134E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.80715558E-04    2           3        -3   # BR(H -> s       sb     )
     2.20483300E-07    2           4        -4   # BR(H -> c       cb     )
     2.19946428E-02    2           6        -6   # BR(H -> t       tb     )
     3.50542313E-05    2          21        21   # BR(H -> g       g      )
     6.40205899E-08    2          22        22   # BR(H -> gam     gam    )
     8.39081592E-09    2          23        22   # BR(H -> Z       gam    )
     3.80505167E-05    2          24       -24   # BR(H -> W+      W-     )
     1.90017976E-05    2          23        23   # BR(H -> Z       Z      )
     9.11729598E-05    2          25        25   # BR(H -> h       h      )
     8.09570296E-23    2          36        36   # BR(H -> A       A      )
     3.67671914E-17    2          23        36   # BR(H -> Z       A      )
     2.43055700E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.14120992E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.21015608E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.44815716E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.44918957E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.50806299E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.30784154E+01   # A decays
#          BR         NDA      ID1       ID2
     7.80066512E-01    2           5        -5   # BR(A -> b       bb     )
     1.90138811E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.72284457E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.25704900E-04    2           3        -3   # BR(A -> s       sb     )
     2.33002218E-07    2           4        -4   # BR(A -> c       cb     )
     2.32306742E-02    2           6        -6   # BR(A -> t       tb     )
     6.84120994E-05    2          21        21   # BR(A -> g       g      )
     3.38509148E-08    2          22        22   # BR(A -> gam     gam    )
     6.73637767E-08    2          23        22   # BR(A -> Z       gam    )
     4.00783948E-05    2          23        25   # BR(A -> Z       h      )
     2.68557531E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.23264822E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33709900E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.11250206E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.54690543E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.06408991E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.60561061E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     9.21280357E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.81016444E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.41414787E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.11386431E-03    2           4        -3   # BR(H+ -> c       sb     )
     6.95335885E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.49937492E-05    2          24        25   # BR(H+ -> W+      h      )
     1.45076215E-13    2          24        36   # BR(H+ -> W+      A      )
     2.18194457E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.15656401E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.87059215E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
