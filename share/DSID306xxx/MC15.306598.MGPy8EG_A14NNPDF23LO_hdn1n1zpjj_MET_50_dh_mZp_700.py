model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 700.
mHD = 125.
widthZp = 3.328931e+00
widthhd = 1.554719e+00
filteff = 8.503401e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
