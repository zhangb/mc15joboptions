from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile=runArgs.inputGeneratorFile.replace('.tar.gz','.evts')
evgenConfig.generators += ["HepMCAscii"]

#evgenConfig.inputfilecheck = "DY_mumu_PDF4LHCNNLO100_Mll_50_150_as118"
evgenConfig.inputfilecheck = "Geneva"
evgenConfig.description = 'Geneva+Py8 DYmumu PDF4LHCNNLO100 Mll_50_150 as118'
evgenConfig.keywords+=['SM','Z']
evgenConfig.contact  = [ 'mcfayden@cern.ch' ]
evgenConfig.minevents = 60000

del testSeq.TestHepMC
