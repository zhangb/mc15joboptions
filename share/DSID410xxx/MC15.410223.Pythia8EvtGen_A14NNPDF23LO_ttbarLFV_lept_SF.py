import subprocess, os

evgenConfig.description = "ttbar LFV decaying to b W_lep llq (q=u,c) with ll same flavour with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["ttbar", "BSM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# Retrieve SLHA files
get_SLHA = subprocess.Popen(['get_files','-data','ttdec_SF.SLHA'])
get_SLHA.wait()
# Check we found it
if 'ttdec_SF.SLHA' not in os.listdir("./"):
    raise RuntimeError("SLHA file has not been retrieved")

genSeq.Pythia8.Commands += ["Top:gg2ttbar = on",
                            "Top:qqbar2ttbar = on",
                            "24:onMode = off",
                            "24:onIfAny = 11 13 15"]

genSeq.Pythia8.Commands += ["SLHA:readFrom = 2",
                            "SLHA:file = ttdec_SF.SLHA",
                            "SLHA:keepSM = off"]

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("ParentChildFilter1")
filtSeq.ParentChildFilter1.PDGParent  = [6]
filtSeq.ParentChildFilter1.PDGChild = [24]
filtSeq += ParentChildFilter("ParentChildFilter2")
filtSeq.ParentChildFilter2.PDGParent  = [6]
filtSeq.ParentChildFilter2.PDGChild = [2,4]
