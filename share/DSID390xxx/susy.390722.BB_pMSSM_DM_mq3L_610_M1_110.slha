#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11093248E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.55959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04174485E+01   # W+
        25     1.26034198E+02   # h
        35     3.99999989E+03   # H
        36     3.99999584E+03   # A
        37     4.00104343E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02018306E+03   # ~d_L
   2000001     4.01798558E+03   # ~d_R
   1000002     4.01952835E+03   # ~u_L
   2000002     4.01911758E+03   # ~u_R
   1000003     4.02018306E+03   # ~s_L
   2000003     4.01798558E+03   # ~s_R
   1000004     4.01952835E+03   # ~c_L
   2000004     4.01911758E+03   # ~c_R
   1000005     6.64413759E+02   # ~b_1
   2000005     4.02214867E+03   # ~b_2
   1000006     6.46420360E+02   # ~t_1
   2000006     2.07173484E+03   # ~t_2
   1000011     4.00352507E+03   # ~e_L
   2000011     4.00311457E+03   # ~e_R
   1000012     4.00240902E+03   # ~nu_eL
   1000013     4.00352507E+03   # ~mu_L
   2000013     4.00311457E+03   # ~mu_R
   1000014     4.00240902E+03   # ~nu_muL
   1000015     4.00528478E+03   # ~tau_1
   2000015     4.00721223E+03   # ~tau_2
   1000016     4.00436453E+03   # ~nu_tauL
   1000021     1.97021491E+03   # ~g
   1000022     9.34947469E+01   # ~chi_10
   1000023    -1.36802278E+02   # ~chi_20
   1000025     1.52730760E+02   # ~chi_30
   1000035     2.05476746E+03   # ~chi_40
   1000024     1.31816132E+02   # ~chi_1+
   1000037     2.05493242E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61985520E-01   # N_11
  1  2     1.38054992E-02   # N_12
  1  3     5.33784248E-01   # N_13
  1  4     3.66417591E-01   # N_14
  2  1    -1.35804770E-01   # N_21
  2  2     2.72574715E-02   # N_22
  2  3    -6.85335888E-01   # N_23
  2  4     7.14932735E-01   # N_24
  3  1     6.33193745E-01   # N_31
  3  2     2.38819368E-02   # N_32
  3  3     4.95367365E-01   # N_33
  3  4     5.94227656E-01   # N_34
  4  1    -9.01285835E-04   # N_41
  4  2     9.99247763E-01   # N_42
  4  3    -5.19357398E-04   # N_43
  4  4    -3.87663052E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.38795414E-04   # U_11
  1  2     9.99999727E-01   # U_12
  2  1    -9.99999727E-01   # U_21
  2  2     7.38795414E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48372632E-02   # V_11
  1  2    -9.98495305E-01   # V_12
  2  1    -9.98495305E-01   # V_21
  2  2    -5.48372632E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95126407E-01   # cos(theta_t)
  1  2    -9.86074748E-02   # sin(theta_t)
  2  1     9.86074748E-02   # -sin(theta_t)
  2  2     9.95126407E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999923E-01   # cos(theta_b)
  1  2    -3.92428330E-04   # sin(theta_b)
  2  1     3.92428330E-04   # -sin(theta_b)
  2  2     9.99999923E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03077468E-01   # cos(theta_tau)
  1  2     7.11113264E-01   # sin(theta_tau)
  2  1    -7.11113264E-01   # -sin(theta_tau)
  2  2    -7.03077468E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00327506E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10932481E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43897731E+02   # higgs               
         4     1.61209521E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10932481E+03  # The gauge couplings
     1     3.61591700E-01   # gprime(Q) DRbar
     2     6.37143767E-01   # g(Q) DRbar
     3     1.03385524E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10932481E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.59685218E-07   # A_c(Q) DRbar
  3  3     2.55959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10932481E+03  # The trilinear couplings
  1  1     3.50630661E-07   # A_d(Q) DRbar
  2  2     3.50664035E-07   # A_s(Q) DRbar
  3  3     6.29086375E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10932481E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.54157225E-08   # A_mu(Q) DRbar
  3  3     7.61818230E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10932481E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72149746E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10932481E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80920798E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10932481E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04114338E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10932481E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58630497E+07   # M^2_Hd              
        22    -2.65281930E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40837217E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.57588349E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44197870E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44197870E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55802130E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55802130E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.59295483E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.22097420E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.40836740E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.28672616E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.08393225E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.27990299E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.42900092E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19761289E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.08487623E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.30767556E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.51253847E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.58638861E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.11933276E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.33760231E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.09888004E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.81256128E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.91307811E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.71527231E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.85590883E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66552344E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.79429278E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69709886E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41122051E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.85503284E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55048845E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.53764186E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15400180E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.72915622E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.90475195E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.23303109E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     7.02047882E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78614407E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48503941E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14369135E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80134458E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79477999E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25239943E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57731124E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52467249E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61160768E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.22044121E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02186698E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.22033065E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.47562686E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44570390E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78634015E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.18097054E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.44815345E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.02726981E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79941235E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.53409110E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60891389E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52688129E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54363187E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.40322500E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66639648E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.79359185E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.45721866E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85536537E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78614407E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48503941E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14369135E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80134458E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79477999E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25239943E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57731124E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52467249E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61160768E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.22044121E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02186698E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.22033065E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.47562686E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44570390E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78634015E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.18097054E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.44815345E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.02726981E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79941235E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.53409110E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60891389E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52688129E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54363187E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.40322500E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66639648E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.79359185E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.45721866E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85536537E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15273599E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97697571E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.26947871E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.50910719E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77509294E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.58555383E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56359840E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07870992E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81059609E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84338582E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00506092E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.41477399E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15273599E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97697571E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.26947871E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.50910719E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77509294E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.58555383E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56359840E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07870992E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81059609E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84338582E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00506092E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.41477399E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11126452E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27810832E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00670429E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60098433E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39088634E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.40863760E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78848682E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12058310E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11834497E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33587840E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35382100E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42160814E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22541934E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85009611E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15381072E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01938469E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.56491675E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75039216E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77794977E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.07538700E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54122329E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15381072E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01938469E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.56491675E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75039216E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77794977E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.07538700E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54122329E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48837288E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22069019E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.03366750E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20143961E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51450632E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77219872E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01572415E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.23004627E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33726307E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33726307E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11242876E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11242876E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10061635E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.24047857E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.76298638E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.63787958E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.61024778E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.58038545E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.53582184E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.77075644E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.49412882E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.16838279E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.77582950E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18526050E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53683932E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18526050E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53683932E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37336633E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52045377E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52045377E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47643776E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03793765E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03793765E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03793728E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.73845830E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.73845830E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.73845830E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.73845830E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.12821689E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.12821689E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.12821689E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.12821689E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.97097770E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.97097770E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.40153813E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.36133926E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.82744203E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.97448064E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.43255150E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.97448064E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.43255150E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.99856351E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.45971723E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.45971723E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.44517189E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.95254013E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.95254013E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.95253453E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.51672011E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.45051984E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.51672011E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.45051984E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.38757425E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.93631572E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.93631572E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.80254283E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.86946653E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.86946653E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.86946666E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.69227969E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.69227969E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.69227969E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.69227969E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.89737838E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.89737838E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.89737838E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.89737838E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.79577382E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.79577382E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.23946221E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.60890732E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.46787201E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.27980645E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.45083406E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.45083406E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.46189926E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.16742971E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.56286393E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.84710019E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.84710019E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.85356377E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.85356377E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.12281218E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88040073E-01    2           5        -5   # BR(h -> b       bb     )
     6.34617895E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.24652176E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.76058378E-04    2           3        -3   # BR(h -> s       sb     )
     2.06708918E-02    2           4        -4   # BR(h -> c       cb     )
     6.77264530E-02    2          21        21   # BR(h -> g       g      )
     2.36687307E-03    2          22        22   # BR(h -> gam     gam    )
     1.66168588E-03    2          22        23   # BR(h -> Z       gam    )
     2.26518984E-01    2          24       -24   # BR(h -> W+      W-     )
     2.88525397E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44275452E+01   # H decays
#          BR         NDA      ID1       ID2
     3.38979384E-01    2           5        -5   # BR(H -> b       bb     )
     6.09175430E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15389677E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54768907E-04    2           3        -3   # BR(H -> s       sb     )
     7.14749065E-08    2           4        -4   # BR(H -> c       cb     )
     7.16327361E-03    2           6        -6   # BR(H -> t       tb     )
     1.12223593E-06    2          21        21   # BR(H -> g       g      )
     3.01537599E-10    2          22        22   # BR(H -> gam     gam    )
     1.83711130E-09    2          23        22   # BR(H -> Z       gam    )
     2.12821666E-06    2          24       -24   # BR(H -> W+      W-     )
     1.06337112E-06    2          23        23   # BR(H -> Z       Z      )
     7.80801677E-06    2          25        25   # BR(H -> h       h      )
     4.40665996E-24    2          36        36   # BR(H -> A       A      )
    -6.49986709E-21    2          23        36   # BR(H -> Z       A      )
     1.86167291E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67051992E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67051992E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35377213E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.59957757E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68369151E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48165894E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.96434312E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.94082473E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04630642E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.42292492E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.38797771E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.39688284E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.21661137E-08    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.21661137E-08    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.38451584E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44280835E+01   # A decays
#          BR         NDA      ID1       ID2
     3.39002114E-01    2           5        -5   # BR(A -> b       bb     )
     6.09173802E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15388932E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54809532E-04    2           3        -3   # BR(A -> s       sb     )
     7.19189731E-08    2           4        -4   # BR(A -> c       cb     )
     7.17520817E-03    2           6        -6   # BR(A -> t       tb     )
     1.47432417E-05    2          21        21   # BR(A -> g       g      )
     4.06392366E-08    2          22        22   # BR(A -> gam     gam    )
     1.62074931E-08    2          23        22   # BR(A -> Z       gam    )
     2.12368455E-06    2          23        25   # BR(A -> Z       h      )
     1.86476104E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67052532E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67052532E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.92948475E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.21885584E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33170894E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.96928464E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.57617286E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.62499778E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30681403E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.30384471E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.82103459E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.50139258E-08    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.50139258E-08    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43025468E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.39988767E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10742000E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15943409E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.45592512E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22335921E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51684329E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.44324526E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.13101249E-06    2          24        25   # BR(H+ -> W+      h      )
     3.05746330E-14    2          24        36   # BR(H+ -> W+      A      )
     4.84214699E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.26609846E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07080328E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67656485E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.59813761E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57798750E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.23689595E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.37571638E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.96336188E-07    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
