evgenConfig.minevents = 5000

import math
import re

#parse channel, component and mass from joboption name
jo_name = runArgs.jobConfig[0]

if runArgs.maxEvents > 0:
    nEvents = runArgs.maxEvents
else:
    nEvents = evgenConfig.minevents

nLHEvents = int(nEvents * 1.5)

include("MC15JobOptions/aMcAtNloControl_bbH4FS.py")
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "Check:epTolErr = 1e-2",
                            '25:doForceWidth = true'
                            ]

include('MC15JobOptions/Pythia8_SMHiggs125_inc.py')
