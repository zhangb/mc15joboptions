#################################################################################
# job options fragment for bb->NegDs10->nutau->mu4mu2mu1
# For HF tau3mu based on Meson Decay(MD) study for run2
#################################################################################
# All production channels of bb are included in this fragment.
# D_s meson force decaying into tau+nu and tau also must decay into 3muons
# D_s pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>10.5 or 3.5GeV, mu2>5.5 or 3.5GeV, mu3> 2 or 3.5GeV, and D_s>10GeV
#################################################################################

f = open("B_TauNuX_3MuNuX_USER.DEC", "w")
f.write("Alias mytau- tau-\n")
f.write("Decay B-\n")
#Bp decays
f.write("0.4972   D*0    mytau- anti-nu_tau                ISGW2;\n")
f.write("0.2036   D0     mytau- anti-nu_tau                ISGW2;\n")
f.write("0.0557   D_10   mytau- anti-nu_tau                ISGW2;\n")
f.write("0.0557   D_0*0   mytau- anti-nu_tau                ISGW2;\n")
f.write("0.0858   D'_10   mytau- anti-nu_tau                ISGW2;\n")
f.write("0.0858   D_2*0   mytau- anti-nu_tau                ISGW2;\n")
f.write("0.0162   D*0    tau- anti-nu_tau       ISGW2;\n") #dummy channel to get BR correct

f.write("Enddecay\n")

f.write("Decay anti-B0\n")
f.write("0.486633888417779   D*+    mytau- anti-nu_tau                  ISGW2;\n")
f.write("0.269764438144638   D+     mytau- anti-nu_tau                  ISGW2;\n")
f.write("0.0479821477983118   D_1+   mytau- anti-nu_tau                  ISGW2;\n")
f.write("0.0479821477983118   D_0*+   mytau- anti-nu_tau                  ISGW2;\n")
f.write("0.0738186889204796   D'_1+   mytau- anti-nu_tau                  ISGW2;\n")
f.write("0.0738186889204796   D_2*+   mytau- anti-nu_tau                  ISGW2;\n")
f.write("Enddecay\n")

f.write("Decay Lambda_b0\n")
f.write("0.4549    Lambda_c+ mytau- anti-nu_tau             PHSP;\n")
f.write("0.1741    Lambda_c(2593)+ mytau- anti-nu_tau            PHSP;\n")
f.write("0.1296    Lambda_c(2625)+  mytau- anti-nu_tau            PHSP;\n")
f.write("0.2414    Lambda_c(2625)+  tau- anti-nu_tau            PHSP;\n")

f.write("Enddecay\n")

f.write("Decay anti-B_s0\n")
f.write("0.2116   D_s+     mytau-   anti-nu_tau                 ISGW2;\n")
f.write("0.4232   D_s*+    mytau-   anti-nu_tau                 ISGW2;\n")
f.write("0.0476   D_s1+    mytau-   anti-nu_tau                 ISGW2;\n")
f.write("0.0476   D_s0*+   mytau-   anti-nu_tau                 ISGW2;\n")
f.write("0.0741   D'_s1+   mytau-   anti-nu_tau                 ISGW2;\n")
f.write("0.0741   D_s2*+   mytau-   anti-nu_tau                 ISGW2;\n")
f.write("0.1218   D_s2*+   tau-   anti-nu_tau                 ISGW2;\n") #dummy channel to get BR correct
f.write("Enddecay\n")

f.write("Decay mytau-\n")
f.write("1.0000   mu-      mu+          mu-          PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.description = "bb->taunuX->3munu production"
evgenConfig.keywords = ["charmonium", "tau", "muon", "BSM"]
evgenConfig.minevents = 50
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch']
evgenConfig.process = "bb>taunuX>3munu"

genSeq.Pythia8B.Commands += ['HardQCD:all = on']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = False

genSeq.Pythia8B.NHadronizationLoops = 1
genSeq.Pythia8B.NDecayLoops = 1

genSeq.EvtInclusiveDecay.userDecayFile = "B_TauNuX_3MuNuX_USER.DEC"

from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter

filtSeq += TripletChainFilter("LowPtTripletFilter")
filtSeq += TripletChainFilter("HighPtTripletFilter")
filtSeq.Expression = "LowPtTripletFilter or HighPtTripletFilter"

TripletChainFilter = filtSeq.LowPtTripletFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = 13
TripletChainFilter.PdgId2 = -13
TripletChainFilter.PdgId3 = 13
TripletChainFilter.NStep1 = 1
TripletChainFilter.NStep2 = 1
TripletChainFilter.NStep3 = 1
TripletChainFilter.PtMin1 = 3500
TripletChainFilter.PtMin2 = 3500
TripletChainFilter.PtMin3 = 3500
TripletChainFilter.EtaMax1 = 3.0
TripletChainFilter.EtaMax2 = 3.0
TripletChainFilter.EtaMax3 = 3.0
TripletChainFilter.TripletPdgId = 15
TripletChainFilter.TripletPtMin = 0
TripletChainFilter.TripletEtaMax = 100
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000

TripletChainFilter2 = filtSeq.HighPtTripletFilter
TripletChainFilter2.NTriplet = 1
TripletChainFilter2.PdgId1 = 13
TripletChainFilter2.PdgId2 = -13
TripletChainFilter2.PdgId3 = 13
TripletChainFilter2.NStep1 = 1
TripletChainFilter2.NStep2 = 1
TripletChainFilter2.NStep3 = 1
TripletChainFilter2.PtMin1 = 10500
TripletChainFilter2.PtMin2 = 5500
TripletChainFilter2.PtMin3 = 2000
TripletChainFilter2.EtaMax1 = 3.0
TripletChainFilter2.EtaMax2 = 3.0
TripletChainFilter2.EtaMax3 = 3.0
TripletChainFilter2.TripletPdgId = 15
TripletChainFilter2.TripletPtMin = 0
TripletChainFilter2.TripletEtaMax = 100
TripletChainFilter2.TripletMassMin = 0
TripletChainFilter2.TripletMassMax = 10000000
