#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP ttbar production with Powheg hdamp equal top mass, UEEE5 tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'teng.jian.khoo@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwigpp", "EvtGen"]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp        = 172.5
# compensate filter efficiency
PowhegConfig.nEvents     *= 200.
PowhegConfig.generate()

#--------------------------------------------------------------
# HerwigPP (UEEE5) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')

## Add to commands                                                                                                                                                                                         
cmds = """                                                                                                                                                                                                    
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General                                                                                                                                       
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost                                                                                                                           
"""
genSeq.Herwigpp.Commands += cmds.splitlines()
 
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 200*GeV

#  Run EvtGen as afterburner
#include('MC15JobOptions/Herwigpp_EvtGen.py')

