include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa W+/W- -> mu nu + 0,1,2j@NLO + 3,4j@LO."
evgenConfig.keywords = ["SM", "W", "muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Wmunu"

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:= 4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 13 -14 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  Enhance_Factor 3 {5,6};
  End process;

  Process 93 93 -> -13 14 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  Enhance_Factor 3 {5,6};
  End process;
}(processes)

(selector){
  Mass 13 -14 2.0 E_CMS
  Mass -13 14 2.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppln", "pplnj", "pplnjj" ]

