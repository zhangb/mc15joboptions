#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune + fragmentation modified with a 3-parameters fit to LEP data, variation, at least one lepton + at least one soft muon filter, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'simone.amoroso@cern.ch','marco.vanadia@cern.ch']
evgenConfig.minevents = 1000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]
#modified fragmentation
genSeq.Pythia8.Commands += [ 'StringZ:useNonstandardB=on']
genSeq.Pythia8.Commands += [ 'StringZ:aNonstandardB = 1.74376' ]
genSeq.Pythia8.Commands += [ 'StringZ:bNonstandardB = 2.50921' ]
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.15' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 20000.

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter

filtSeq += TTbarWToLeptonFilter("onetWlep")
filtSeq.onetWlep.NumLeptons = 1
filtSeq.onetWlep.Ptcut = 0.

filtSeq += TTbarWToLeptonFilter("twotWlep")
filtSeq.twotWlep.NumLeptons = 2
filtSeq.twotWlep.Ptcut = 0.

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("Wmu")
filtSeq.Wmu.PDGParent  = [24]
filtSeq.Wmu.PDGChild = [13]

filtSeq += ParentChildFilter("Wetau")
filtSeq.Wetau.PDGParent  = [24]
filtSeq.Wetau.PDGChild = [11,15]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("OneMuonFilter")
filtSeq.OneMuonFilter.Ptcut = 3250.
filtSeq.OneMuonFilter.Etacut = 2.8
filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 2.8
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 3250.
filtSeq.ThreeMuonsFilter.Etacut = 2.8
filtSeq.ThreeMuonsFilter.NMuons = 3

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 20000.
filtSeq.ElectronFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 20000.
filtSeq.MuonFilter.Etacut = 2.8

filtSeq.Expression="TTbarWToLeptonFilter and (ElectronFilter or MuonFilter) and ((onetWlep and ((Wmu and TwoMuonsFilter) or (Wetau and OneMuonFilter))) or (twotWlep and ( ((not Wmu) and OneMuonFilter) or (Wmu and Wetau and TwoMuonsFilter) or (Wmu and (not Wetau) and ThreeMuonsFilter) )) )"


