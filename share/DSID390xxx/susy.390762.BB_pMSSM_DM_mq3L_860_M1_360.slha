#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13058843E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.89900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178413E+01   # W+
        25     1.25840027E+02   # h
        35     4.00000974E+03   # H
        36     3.99999709E+03   # A
        37     4.00103748E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02604455E+03   # ~d_L
   2000001     4.02264933E+03   # ~d_R
   1000002     4.02539277E+03   # ~u_L
   2000002     4.02349868E+03   # ~u_R
   1000003     4.02604455E+03   # ~s_L
   2000003     4.02264933E+03   # ~s_R
   1000004     4.02539277E+03   # ~c_L
   2000004     4.02349868E+03   # ~c_R
   1000005     9.24498621E+02   # ~b_1
   2000005     4.02549242E+03   # ~b_2
   1000006     9.06747122E+02   # ~t_1
   2000006     2.01907716E+03   # ~t_2
   1000011     4.00462561E+03   # ~e_L
   2000011     4.00332844E+03   # ~e_R
   1000012     4.00351270E+03   # ~nu_eL
   1000013     4.00462561E+03   # ~mu_L
   2000013     4.00332844E+03   # ~mu_R
   1000014     4.00351270E+03   # ~nu_muL
   1000015     4.00438217E+03   # ~tau_1
   2000015     4.00782330E+03   # ~tau_2
   1000016     4.00493066E+03   # ~nu_tauL
   1000021     1.97937355E+03   # ~g
   1000022     3.48261953E+02   # ~chi_10
   1000023    -4.01239872E+02   # ~chi_20
   1000025     4.14905701E+02   # ~chi_30
   1000035     2.05214094E+03   # ~chi_40
   1000024     3.98897414E+02   # ~chi_1+
   1000037     2.05230563E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40078082E-01   # N_11
  1  2    -1.56201247E-02   # N_12
  1  3    -4.09828371E-01   # N_13
  1  4    -3.55057085E-01   # N_14
  2  1    -4.35098519E-02   # N_21
  2  2     2.40540217E-02   # N_22
  2  3    -7.03935943E-01   # N_23
  2  4     7.08521337E-01   # N_24
  3  1     5.40716737E-01   # N_31
  3  2     2.81581579E-02   # N_32
  3  3     5.80069968E-01   # N_33
  3  4     6.08565001E-01   # N_34
  4  1    -1.05773061E-03   # N_41
  4  2     9.99191940E-01   # N_42
  4  3    -5.80747435E-03   # N_43
  4  4    -3.97570191E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.21881715E-03   # U_11
  1  2     9.99966225E-01   # U_12
  2  1    -9.99966225E-01   # U_21
  2  2     8.21881715E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62354504E-02   # V_11
  1  2    -9.98417535E-01   # V_12
  2  1    -9.98417535E-01   # V_21
  2  2    -5.62354504E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92916182E-01   # cos(theta_t)
  1  2    -1.18816899E-01   # sin(theta_t)
  2  1     1.18816899E-01   # -sin(theta_t)
  2  2     9.92916182E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999214E-01   # cos(theta_b)
  1  2    -1.25379400E-03   # sin(theta_b)
  2  1     1.25379400E-03   # -sin(theta_b)
  2  2     9.99999214E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05997217E-01   # cos(theta_tau)
  1  2     7.08214607E-01   # sin(theta_tau)
  2  1    -7.08214607E-01   # -sin(theta_tau)
  2  2    -7.05997217E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00268054E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30588428E+03  # DRbar Higgs Parameters
         1    -3.89900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43646185E+02   # higgs               
         4     1.61318447E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30588428E+03  # The gauge couplings
     1     3.61920031E-01   # gprime(Q) DRbar
     2     6.36035883E-01   # g(Q) DRbar
     3     1.03004353E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30588428E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37991563E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30588428E+03  # The trilinear couplings
  1  1     5.08075028E-07   # A_d(Q) DRbar
  2  2     5.08122073E-07   # A_s(Q) DRbar
  3  3     9.08992171E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30588428E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11713010E-07   # A_mu(Q) DRbar
  3  3     1.12853926E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30588428E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67410671E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30588428E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85209925E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30588428E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03257240E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30588428E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57394889E+07   # M^2_Hd              
        22    -1.24573370E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40346142E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.21645440E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42135191E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42135191E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57864809E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57864809E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.81510257E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.25954143E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.24466055E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.40661863E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.08917939E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19080928E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.61488927E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13920414E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.89444550E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.24638780E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.63459748E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.17494652E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.35393059E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.27557756E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.87692611E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.13839157E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.49020321E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.84944791E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66641079E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.55151573E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81263743E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61512901E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.19141527E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64232905E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.36188223E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13021345E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.64011394E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.21408961E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.71251034E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.31853109E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78545576E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.77979044E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14112183E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46334152E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80549849E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42505748E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59888547E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52149111E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60819819E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.88417922E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03696495E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.59934982E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.43892657E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45060979E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78565761E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.63776499E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.95091774E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.74098393E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81068664E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.15885158E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63134445E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52366728E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54119805E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01303477E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.70450814E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.17126165E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.96651215E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85671337E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78545576E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.77979044E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14112183E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46334152E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80549849E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42505748E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59888547E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52149111E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60819819E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.88417922E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03696495E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.59934982E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.43892657E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45060979E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78565761E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.63776499E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.95091774E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.74098393E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81068664E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.15885158E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63134445E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52366728E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54119805E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01303477E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.70450814E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.17126165E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.96651215E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85671337E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14346259E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.07961248E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.49107205E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65855836E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77971803E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.78897914E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57413227E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05112651E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07058189E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88720879E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.91053983E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.18547489E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14346259E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.07961248E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.49107205E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65855836E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77971803E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.78897914E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57413227E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05112651E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07058189E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88720879E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.91053983E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.18547489E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08646855E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55184091E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44642874E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23427464E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40344637E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51471327E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81432388E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08051519E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60790875E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05309897E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98455016E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43295360E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95830137E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87344745E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14452128E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.22965181E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19629459E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.91746929E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78360874E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17639425E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55126562E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14452128E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.22965181E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19629459E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.91746929E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78360874E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17639425E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55126562E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46962711E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11483956E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08460076E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.55171249E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52495036E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58746278E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03544655E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.26859032E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33552494E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33552494E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11185661E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11185661E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10523689E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.28290277E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.60008574E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.08099054E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.44249446E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.51229374E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.65419953E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.32727421E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.65305649E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.42726596E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.23488995E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.03357937E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17955248E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53011973E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17955248E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53011973E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42029451E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50903771E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50903771E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47875804E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01544070E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01544070E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01544054E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.22994258E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.22994258E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.22994258E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.22994258E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.43314848E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.43314848E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.43314848E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.43314848E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.49347049E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.49347049E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.53203846E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.49506499E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.74347348E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.13374086E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.91630720E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.13374086E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.91630720E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.58133167E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.78347553E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.78347553E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.78224734E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.64473376E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.64473376E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.64472471E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.85707952E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.89529937E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.85707952E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.89529937E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.25412683E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.04012184E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.04012184E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.85019003E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.07816866E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.07816866E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.07816870E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.13416687E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.13416687E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.13416687E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.13416687E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.71137129E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.71137129E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.71137129E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.71137129E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.61105401E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.61105401E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.28155306E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.62711942E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.83961576E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.01094635E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.32228159E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.32228159E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.02107278E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.64340497E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.74252616E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.75832714E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.75832714E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.76343081E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.76343081E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05933996E-03   # h decays
#          BR         NDA      ID1       ID2
     5.87348210E-01    2           5        -5   # BR(h -> b       bb     )
     6.43392671E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27759248E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82788789E-04    2           3        -3   # BR(h -> s       sb     )
     2.09680730E-02    2           4        -4   # BR(h -> c       cb     )
     6.86940311E-02    2          21        21   # BR(h -> g       g      )
     2.38825783E-03    2          22        22   # BR(h -> gam     gam    )
     1.65927396E-03    2          22        23   # BR(h -> Z       gam    )
     2.25278667E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86136716E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51848213E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59256202E-01    2           5        -5   # BR(H -> b       bb     )
     6.00817830E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12434632E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51273521E-04    2           3        -3   # BR(H -> s       sb     )
     7.04775023E-08    2           4        -4   # BR(H -> c       cb     )
     7.06331314E-03    2           6        -6   # BR(H -> t       tb     )
     8.08771693E-07    2          21        21   # BR(H -> g       g      )
     2.01815123E-09    2          22        22   # BR(H -> gam     gam    )
     1.81630013E-09    2          23        22   # BR(H -> Z       gam    )
     1.77678267E-06    2          24       -24   # BR(H -> W+      W-     )
     8.87775886E-07    2          23        23   # BR(H -> Z       Z      )
     7.04550011E-06    2          25        25   # BR(H -> h       h      )
     2.69424153E-24    2          36        36   # BR(H -> A       A      )
    -4.18800816E-21    2          23        36   # BR(H -> Z       A      )
     1.85301196E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58131564E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58131564E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.31408977E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.77244669E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.45780770E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.54157119E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.60239658E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.55347251E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.24147262E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.18981562E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.12204440E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.62414619E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.48166611E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.48166611E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.34417469E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51817015E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59302104E-01    2           5        -5   # BR(A -> b       bb     )
     6.00854487E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12447426E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51329662E-04    2           3        -3   # BR(A -> s       sb     )
     7.09367931E-08    2           4        -4   # BR(A -> c       cb     )
     7.07721809E-03    2           6        -6   # BR(A -> t       tb     )
     1.45418911E-05    2          21        21   # BR(A -> g       g      )
     6.21847813E-08    2          22        22   # BR(A -> gam     gam    )
     1.59872401E-08    2          23        22   # BR(A -> Z       gam    )
     1.77312162E-06    2          23        25   # BR(A -> Z       h      )
     1.87434597E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58130109E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58130109E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.00646345E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.35849194E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23470198E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.07278132E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.06130240E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.76825920E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.37970816E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.52912852E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.56074989E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.80267423E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.80267423E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53286516E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.77455321E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.99414512E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11938286E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69571092E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20066968E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47016363E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67593000E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.77041186E-06    2          24        25   # BR(H+ -> W+      h      )
     2.89907985E-14    2          24        36   # BR(H+ -> W+      A      )
     5.67936301E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.56602761E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.82956014E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57927028E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.31721695E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56814198E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.06762586E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.42085446E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.12787077E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
