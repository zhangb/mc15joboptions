evgenConfig.description = "Low-pT inelastic minimum bias events for pile-up, with the A3 NNPDF23LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]

evgenConfig.saveJets = True

#include("MC15JobOptions/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This is what goes inside the line above...
include("MC15JobOptions/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
#    "PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    #"Diffraction:PomFlux = 4",
    "SigmaDiffractive:PomFlux = 4",
    #"Diffraction:PomFluxEpsilon = 0.07",
    "SigmaDiffractive:PomFluxEpsilon = 0.07",
    #"Diffraction:PomFluxAlphaPrime = 0.25"]
    "SigmaDiffractive:PomFluxAlphaPrime = 0.25",
#    "BeamRemnants:reconnectRange  = 1.8",
	]

rel = os.popen("echo $AtlasVersion").read()
print "Atlas release " + rel

if rel[:2].isdigit() and int(rel[:2])<20:
# todo - replace BeamRemnants with new ColourReconnection syntax once Pythia 8.201 is in place
    ver =  os.popen("cmt show versions External/Pythia8").read()
    print "Pythia8 version: " + ver
    if 'Pythia8-01' in ver[:50]:
        genSeq.Pythia8.Commands += [
            "PDF:useLHAPDF = on",
            "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
            "BeamRemnants:reconnectRange  = 1.8"]
    else:
        genSeq.Pythia8.Commands += ["PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
            "ColourReconnection:range = 1.8"]
else:
    genSeq.Pythia8.Commands += ["PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
    "ColourReconnection:range = 1.8"]

evgenConfig.tune = "A3 NNPDF23LO"

include("MC15JobOptions/Pythia8_EvtGen.py")
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

genSeq.Pythia8.Commands += \
    [
    "SoftQCD:inelastic = on",
    ]

include("MC15JobOptions/JetFilter_MinbiasLow.py")

# include ("GeneratorFilters/FindJets.py")
# CreateJets(prefiltSeq,filtSeq,runArgs.ecmEnergy, 0.6)
# 
# from AthenaCommon.SystemOfUnits import GeV
# filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

evgenConfig.minevents = 1000

