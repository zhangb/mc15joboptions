#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.89503643E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.94900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04197184E+01   # W+
        25     1.25359828E+02   # h
        35     4.00000762E+03   # H
        36     3.99999615E+03   # A
        37     4.00096978E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01232954E+03   # ~d_L
   2000001     4.01171359E+03   # ~d_R
   1000002     4.01167128E+03   # ~u_L
   2000002     4.01315304E+03   # ~u_R
   1000003     4.01232954E+03   # ~s_L
   2000003     4.01171359E+03   # ~s_R
   1000004     4.01167128E+03   # ~c_L
   2000004     4.01315304E+03   # ~c_R
   1000005     4.36306111E+02   # ~b_1
   2000005     4.01798713E+03   # ~b_2
   1000006     4.17001003E+02   # ~t_1
   2000006     2.02374065E+03   # ~t_2
   1000011     4.00201171E+03   # ~e_L
   2000011     4.00265656E+03   # ~e_R
   1000012     4.00089537E+03   # ~nu_eL
   1000013     4.00201171E+03   # ~mu_L
   2000013     4.00265656E+03   # ~mu_R
   1000014     4.00089537E+03   # ~nu_muL
   1000015     4.00384787E+03   # ~tau_1
   2000015     4.00864164E+03   # ~tau_2
   1000016     4.00351038E+03   # ~nu_tauL
   1000021     1.95751898E+03   # ~g
   1000022     3.51325675E+02   # ~chi_10
   1000023    -4.04491576E+02   # ~chi_20
   1000025     4.17693022E+02   # ~chi_30
   1000035     2.05795770E+03   # ~chi_40
   1000024     4.02419308E+02   # ~chi_1+
   1000037     2.05812161E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.57898287E-01   # N_11
  1  2    -1.47654285E-02   # N_12
  1  3    -3.89797003E-01   # N_13
  1  4    -3.34441039E-01   # N_14
  2  1    -4.32454703E-02   # N_21
  2  2     2.40830791E-02   # N_22
  2  3    -7.03973870E-01   # N_23
  2  4     7.08498854E-01   # N_24
  3  1     5.11995342E-01   # N_31
  3  2     2.88488273E-02   # N_32
  3  3     5.93669845E-01   # N_33
  3  4     6.20148877E-01   # N_34
  4  1    -1.06261182E-03   # N_41
  4  2     9.99184534E-01   # N_42
  4  3    -5.93317859E-03   # N_43
  4  4    -3.99241238E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.39671357E-03   # U_11
  1  2     9.99964747E-01   # U_12
  2  1    -9.99964747E-01   # U_21
  2  2     8.39671357E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.64721062E-02   # V_11
  1  2    -9.98404177E-01   # V_12
  2  1    -9.98404177E-01   # V_21
  2  2    -5.64721062E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96087957E-01   # cos(theta_t)
  1  2    -8.83673125E-02   # sin(theta_t)
  2  1     8.83673125E-02   # -sin(theta_t)
  2  2     9.96087957E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999231E-01   # cos(theta_b)
  1  2    -1.24016104E-03   # sin(theta_b)
  2  1     1.24016104E-03   # -sin(theta_b)
  2  2     9.99999231E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05799501E-01   # cos(theta_tau)
  1  2     7.08411649E-01   # sin(theta_tau)
  2  1    -7.08411649E-01   # -sin(theta_tau)
  2  2    -7.05799501E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00250951E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.95036425E+02  # DRbar Higgs Parameters
         1    -3.94900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44633516E+02   # higgs               
         4     1.63781759E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.95036425E+02  # The gauge couplings
     1     3.60658164E-01   # gprime(Q) DRbar
     2     6.35576609E-01   # g(Q) DRbar
     3     1.03875319E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.95036425E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.66567195E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.95036425E+02  # The trilinear couplings
  1  1     2.08678450E-07   # A_d(Q) DRbar
  2  2     2.08698685E-07   # A_s(Q) DRbar
  3  3     3.73071146E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.95036425E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.66771110E-08   # A_mu(Q) DRbar
  3  3     4.71431976E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.95036425E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74860583E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.95036425E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87827890E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.95036425E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02814163E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.95036425E+02  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57116781E+07   # M^2_Hd              
        22    -1.88697306E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40135337E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.37884062E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45289284E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45289284E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54710716E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54710716E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.03524651E-03   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17429220E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.34494413E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15611774E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.41654222E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.29295786E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.53536453E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.14077536E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.39863588E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.06566270E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.05730468E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.38829440E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.54400926E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66521762E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51622215E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78568380E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61092141E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.19191703E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58715797E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.36245558E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14169306E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.76914865E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.42226967E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.06984773E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.16019848E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77773839E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.87218354E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.16564541E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.35103464E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.75860377E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.46266789E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.50509796E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53571841E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60614500E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.01406383E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.01504633E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.42090174E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.41442394E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45548806E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77793494E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.69701258E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.91362937E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.95393087E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76380333E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.20776610E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.53757690E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53792578E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53931932E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.04668746E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.64677542E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.70505673E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.89888534E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85801592E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77773839E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.87218354E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.16564541E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.35103464E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.75860377E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.46266789E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.50509796E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53571841E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60614500E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.01406383E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.01504633E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.42090174E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.41442394E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45548806E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77793494E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.69701258E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.91362937E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.95393087E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76380333E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.20776610E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.53757690E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53792578E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53931932E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.04668746E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.64677542E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.70505673E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.89888534E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85801592E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12098125E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.13051929E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.05228647E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.14193970E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77993302E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11942119E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57464073E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03632756E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.37235672E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.86387913E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.60899827E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.21650581E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12098125E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.13051929E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.05228647E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.14193970E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77993302E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11942119E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57464073E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03632756E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.37235672E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.86387913E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.60899827E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.21650581E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06717823E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.64578648E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45436354E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.13877921E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40311661E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53179125E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81370222E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06312883E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72785163E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05794192E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.85874630E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43483948E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95499816E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87726858E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12206457E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.27558180E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19263156E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.44971825E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78379014E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.21823050E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55154761E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12206457E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.27558180E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19263156E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.44971825E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78379014E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.21823050E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55154761E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44758867E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.15591880E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08075739E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.12612491E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52496109E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.60302997E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03539704E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.19646405E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33548096E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33548096E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11184250E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11184250E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10535309E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.72785335E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.88062508E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.19002565E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.77175321E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.97829108E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.15581817E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.83951051E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.66349403E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.83909808E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.12891203E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.65379231E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17617431E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52507516E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17617431E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52507516E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44767211E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49371738E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49371738E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46374366E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98350100E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98350100E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98350070E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.31905504E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.31905504E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.31905504E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.31905504E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.39685400E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.39685400E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.39685400E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.39685400E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.64001387E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.64001387E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.79849774E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.95826166E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.31333908E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.97649277E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.00591612E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.97649277E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.00591612E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.19283364E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.03137809E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.03137809E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.02944517E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.13877761E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.13877761E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.13876122E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.72342609E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.42145634E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.72342609E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.42145634E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.80405438E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.70027394E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.70027394E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.53047015E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.39817295E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.39817295E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.39817300E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.02952460E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.02952460E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.02952460E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.02952460E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.34316207E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.34316207E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.34316207E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.34316207E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.25702252E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.25702252E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.72688027E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.62541814E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.86563383E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.64947600E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.83424195E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.83424195E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.05098089E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.07223718E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.04863154E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90818636E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90818636E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.91820852E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.91820852E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.98572564E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94363291E-01    2           5        -5   # BR(h -> b       bb     )
     6.52724713E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31064881E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.90160805E-04    2           3        -3   # BR(h -> s       sb     )
     2.12897239E-02    2           4        -4   # BR(h -> c       cb     )
     6.91037580E-02    2          21        21   # BR(h -> g       g      )
     2.39808018E-03    2          22        22   # BR(h -> gam     gam    )
     1.61997401E-03    2          22        23   # BR(h -> Z       gam    )
     2.17767107E-01    2          24       -24   # BR(h -> W+      W-     )
     2.74643690E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.58718435E+01   # H decays
#          BR         NDA      ID1       ID2
     3.65831579E-01    2           5        -5   # BR(H -> b       bb     )
     5.93429725E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.09822377E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48183692E-04    2           3        -3   # BR(H -> s       sb     )
     6.96060954E-08    2           4        -4   # BR(H -> c       cb     )
     6.97597998E-03    2           6        -6   # BR(H -> t       tb     )
     5.44198794E-07    2          21        21   # BR(H -> g       g      )
     2.28651343E-09    2          22        22   # BR(H -> gam     gam    )
     1.79593765E-09    2          23        22   # BR(H -> Z       gam    )
     1.66827901E-06    2          24       -24   # BR(H -> W+      W-     )
     8.33561730E-07    2          23        23   # BR(H -> Z       Z      )
     6.58671436E-06    2          25        25   # BR(H -> h       h      )
     5.52374650E-26    2          36        36   # BR(H -> A       A      )
    -8.10381326E-21    2          23        36   # BR(H -> Z       A      )
     1.84417250E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.55397453E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.55397453E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.14170723E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.66899367E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.32636065E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.62903559E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.10222593E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.26125910E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.10667978E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.06468876E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.25859962E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.19754624E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.37515584E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.37515584E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.46115369E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.58729926E+01   # A decays
#          BR         NDA      ID1       ID2
     3.65849695E-01    2           5        -5   # BR(A -> b       bb     )
     5.93420248E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09818862E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48220024E-04    2           3        -3   # BR(A -> s       sb     )
     7.00591101E-08    2           4        -4   # BR(A -> c       cb     )
     6.98965346E-03    2           6        -6   # BR(A -> t       tb     )
     1.43619722E-05    2          21        21   # BR(A -> g       g      )
     6.18036816E-08    2          22        22   # BR(A -> gam     gam    )
     1.57945981E-08    2          23        22   # BR(A -> Z       gam    )
     1.66475258E-06    2          23        25   # BR(A -> Z       h      )
     1.86568801E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55383216E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55383216E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.86100547E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.23084176E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.12499090E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.17049282E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.10460717E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.46538712E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.22418204E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.38495738E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.73551892E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.60962791E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.60962791E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.60752769E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.88990400E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.91423483E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09112854E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.76953539E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18466559E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43723809E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.74704653E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.66053928E-06    2          24        25   # BR(H+ -> W+      h      )
     2.05318442E-14    2          24        36   # BR(H+ -> W+      A      )
     5.86371629E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.71484411E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.52690237E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55031854E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.71787754E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.53966240E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.09764422E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.68600505E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.49506681E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
