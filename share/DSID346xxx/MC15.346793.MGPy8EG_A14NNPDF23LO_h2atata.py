from MadGraphControl.MadGraphUtils import *

# General settings

multiplier=1.2
minevents=5000

# General settings
if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents*multiplier
else:
    nevents= minevents*multiplier



gridpack_dir=None
gridpack_mode=False
runName='run_01'
wH = 0.004
clB=10

#mode=1
#cluster_type="lsf"
#cluster_queue="1nd"
#njobs=10



mode=0
njobs=1

### DSID lists (extensions can include systematics samples)
ttbar_5fl=[346792,346793,346794,346795,346796]

if runArgs.runNumber in ttbar_5fl:
    #mgproc="generate p p > h, h > a ta+ ta- NP=1"
    mgproc="generate p p > h, h >  a ta+ ta- NP=1"
    name='nph2atata'
    process="pp>h>atata"
    gridpack_mode=False
    gridpack_dir='madevent/'

else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HiggsTauTauGamma_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
"""+mgproc+"""
output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


process_dir = new_process(grid_pack=gridpack_dir)

# 'pdlabel'    : "'nn23lo1'",
#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version':'2.0', 
          'cut_decays' :'T', 
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : '260000',
	    'use_syst'   : "False",
          'pta'     : '1.0',
          'ptj'     : '15.0',
          'draa'    : '0.0',
          'dral'    : '0.2',
          'draj'    : '0.0',
          'drll'    : '0.3',
          'drjl'    : '0.3',
          'drjj'    : '0.3',
          'etaa'     : '2.6',
          'etal'     : '2.6',
	    }


build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)

print_cards()


madspin_card_loc='madspin_card.dat'                                                                                                                                    

mscard = open(madspin_card_loc,'w')                                                                                                                                    
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay h > ta+ ta- g
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

    
print_cards()

paramcard = subprocess.Popen(['get_files','-data','param_card_clB.dat'])
#paramcard = subprocess.Popen(['cp','/afs/cern.ch/user/l/lixia/workspace/ZZH/share/param_card_default.dat','param_card_default.dat'])
#paramcard = subprocess.Popen(['cp','/afs/cern.ch/user/l/lixia/workspace/ZZH/share/param_card_heft.dat','param_card_default.dat'])
paramcard.wait()

if not os.access('param_card_clB.dat',os.R_OK):
   raise RunTimeError('Could not get param card: param_card_default.dat')
else:
   oldcard = open('param_card_clB.dat','r')
   newcard = open('param_card.dat','w')
   for line in oldcard:
      if 'DECAY  25' in line:
         newcard.write('DECAY  25 %e # WH \n'%(wH))
      elif 'clB' in line: 
         newcard.write('   27 %e # clB \n'%(clB))
      else:
         newcard.write(line)
   oldcard.close()
   newcard.close()

   
generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,
#generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,
#      madspin_card_loc=madspin_card_loc,
	grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed
      )
#	cluster_type=cluster_type,cluster_queue=cluster_queue)
#cluster_type=cluster_type,cluster_queue=cluster_queue)
#grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,
#madspin_card_loc=madspin_card_loc,


arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)

#evgenConfig.generators = ["MadGraph"]

############################
# Shower JOs will go here
#### Shower 
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=['Higgs', '2tau', '1photon']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


#theApp.finalize()
#theApp.exit()


