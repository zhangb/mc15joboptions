include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "QCD: up to 3 jets in ME (no electro-weak processes included). C and B quarks are treated as massive and all processes are included"
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "penghuo12@gmail.com"]
evgenConfig.minevents = 200

evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
Process 93 93 -> 93 93 93{1}
Order_EW 0;
CKKW sqr(20/E_CMS)
Integration_Error 0.02 {2,3};
End process;
}(processes)

(selector){
  NJetFinder  2  10.0  0.0  0.4  -1  999.0  10.0
  NJetFinder  1  1650.0  0.0  0.4  -1  999.0  10.0
}(selector)
"""

# Take tau BR's into account:
genSeq.Sherpa_i.CrossSectionScaleFactor=1.0


include("MC15JobOptions/JetFilter_JZ8.py")
