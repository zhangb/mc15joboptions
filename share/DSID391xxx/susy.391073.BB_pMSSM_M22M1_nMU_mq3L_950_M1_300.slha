#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871809E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04009680E+01   # W+
        25     1.24809290E+02   # h
        35     3.00001609E+03   # H
        36     2.99999994E+03   # A
        37     3.00108532E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04190251E+03   # ~d_L
   2000001     3.03674976E+03   # ~d_R
   1000002     3.04099641E+03   # ~u_L
   2000002     3.03806288E+03   # ~u_R
   1000003     3.04190251E+03   # ~s_L
   2000003     3.03674976E+03   # ~s_R
   1000004     3.04099641E+03   # ~c_L
   2000004     3.03806288E+03   # ~c_R
   1000005     1.05055100E+03   # ~b_1
   2000005     3.03424335E+03   # ~b_2
   1000006     1.04841683E+03   # ~t_1
   2000006     3.02453862E+03   # ~t_2
   1000011     3.00641920E+03   # ~e_L
   2000011     3.00184229E+03   # ~e_R
   1000012     3.00503201E+03   # ~nu_eL
   1000013     3.00641920E+03   # ~mu_L
   2000013     3.00184229E+03   # ~mu_R
   1000014     3.00503201E+03   # ~nu_muL
   1000015     2.98608958E+03   # ~tau_1
   2000015     3.02161562E+03   # ~tau_2
   1000016     3.00487216E+03   # ~nu_tauL
   1000021     2.35218797E+03   # ~g
   1000022     3.01774895E+02   # ~chi_10
   1000023     6.33645742E+02   # ~chi_20
   1000025    -2.99498908E+03   # ~chi_30
   1000035     2.99527015E+03   # ~chi_40
   1000024     6.33809296E+02   # ~chi_1+
   1000037     2.99607699E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890369E-01   # N_11
  1  2    -8.32625289E-07   # N_12
  1  3    -1.47884552E-02   # N_13
  1  4    -7.42641078E-04   # N_14
  2  1     3.98181465E-04   # N_21
  2  2     9.99636243E-01   # N_22
  2  3     2.66631901E-02   # N_23
  2  4     4.03707374E-03   # N_24
  3  1    -9.92913854E-03   # N_31
  3  2     1.60019341E-02   # N_32
  3  3    -7.06848372E-01   # N_33
  3  4     7.07114368E-01   # N_34
  4  1     1.09774124E-02   # N_41
  4  2    -2.17099130E-02   # N_42
  4  3     7.06707687E-01   # N_43
  4  4     7.07087280E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99289003E-01   # U_11
  1  2     3.77026249E-02   # U_12
  2  1    -3.77026249E-02   # U_21
  2  2     9.99289003E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99983713E-01   # V_11
  1  2    -5.70727214E-03   # V_12
  2  1    -5.70727214E-03   # V_21
  2  2    -9.99983713E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98450595E-01   # cos(theta_t)
  1  2    -5.56453892E-02   # sin(theta_t)
  2  1     5.56453892E-02   # -sin(theta_t)
  2  2     9.98450595E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99711722E-01   # cos(theta_b)
  1  2    -2.40098500E-02   # sin(theta_b)
  2  1     2.40098500E-02   # -sin(theta_b)
  2  2     9.99711722E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06956982E-01   # cos(theta_tau)
  1  2     7.07256549E-01   # sin(theta_tau)
  2  1    -7.07256549E-01   # -sin(theta_tau)
  2  2    -7.06956982E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00155342E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718094E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43845600E+02   # higgs               
         4     1.01921854E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718094E+03  # The gauge couplings
     1     3.62545584E-01   # gprime(Q) DRbar
     2     6.37381572E-01   # g(Q) DRbar
     3     1.02367458E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718094E+03  # The trilinear couplings
  1  1     2.71204772E-06   # A_u(Q) DRbar
  2  2     2.71208773E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718094E+03  # The trilinear couplings
  1  1     9.58771234E-07   # A_d(Q) DRbar
  2  2     9.58883247E-07   # A_s(Q) DRbar
  3  3     1.94572740E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718094E+03  # The trilinear couplings
  1  1     4.06149060E-07   # A_e(Q) DRbar
  2  2     4.06168937E-07   # A_mu(Q) DRbar
  3  3     4.11746312E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718094E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49959150E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718094E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.76533296E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718094E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01678064E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718094E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.84561793E+04   # M^2_Hd              
        22    -9.02532547E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41014736E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.14133078E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47882317E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47882317E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52117683E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52117683E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.14123852E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.40444597E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.96826769E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.79128771E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11334736E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.85687471E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.47384834E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.98698065E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.47645328E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.02551540E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66413362E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59514897E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11757726E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.98156068E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.67438737E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.54112293E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.19143833E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.26221856E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.12516196E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.42082489E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.75794375E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.60399018E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.89274283E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.58164545E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.88018911E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.86583479E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.43009146E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.77855423E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.08947290E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.58401478E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.55204810E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.78365757E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.16960828E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.10808479E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.18548107E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15120658E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58934948E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.35739844E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.45316144E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.75376884E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41064996E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.78370776E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.08688648E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.58290150E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.91263726E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.03418719E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.16393698E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.02150250E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.19228601E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64576275E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52215255E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.70706658E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.89689989E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.70113178E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54778459E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.77855423E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.08947290E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.58401478E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.55204810E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.78365757E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.16960828E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.10808479E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.18548107E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15120658E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58934948E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.35739844E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.45316144E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.75376884E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41064996E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.78370776E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.08688648E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.58290150E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.91263726E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.03418719E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.16393698E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.02150250E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.19228601E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64576275E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52215255E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.70706658E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.89689989E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.70113178E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54778459E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.70969124E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03811339E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98967760E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.02233604E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.29325313E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97220850E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.39214270E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53799613E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999848E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47737086E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.09325152E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.35323055E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.70969124E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03811339E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98967760E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.02233604E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.29325313E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97220850E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.39214270E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53799613E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999848E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47737086E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.09325152E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.35323055E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64857788E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.62702979E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12619323E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.24677698E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59672056E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70940972E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.09861902E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.64845675E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43855744E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19149576E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.66806706E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.70976476E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03760040E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98527564E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.83179180E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.07622277E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97712378E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.55246611E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.70976476E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03760040E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98527564E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.83179180E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.07622277E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97712378E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.55246611E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.70986940E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03751370E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98500309E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.61656054E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.04134648E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97746130E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.17477422E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.14475255E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.44708367E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.30428249E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.52365387E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.46363209E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.47771812E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.11306930E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.30316744E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.82665686E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.09171537E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.78717149E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.21282851E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.46597882E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.19227451E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.18410705E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.28586911E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.28586911E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.01374809E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.32063015E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.29587929E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.29587929E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.39993043E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.39993043E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.76188604E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.76188604E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.37300641E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.86891647E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.30296712E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.35805451E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.35805451E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.25799926E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.54057565E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.26262124E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.26262124E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.47149665E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.47149665E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.95684081E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.95684081E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.59095976E-03   # h decays
#          BR         NDA      ID1       ID2
     5.66287576E-01    2           5        -5   # BR(h -> b       bb     )
     7.21016041E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.55242759E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.41916613E-04    2           3        -3   # BR(h -> s       sb     )
     2.35466393E-02    2           4        -4   # BR(h -> c       cb     )
     7.62309952E-02    2          21        21   # BR(h -> g       g      )
     2.60358279E-03    2          22        22   # BR(h -> gam     gam    )
     1.71065475E-03    2          22        23   # BR(h -> Z       gam    )
     2.28263856E-01    2          24       -24   # BR(h -> W+      W-     )
     2.84579327E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.44012775E+01   # H decays
#          BR         NDA      ID1       ID2
     8.94508361E-01    2           5        -5   # BR(H -> b       bb     )
     7.22853866E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.55583853E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.13858700E-04    2           3        -3   # BR(H -> s       sb     )
     8.79769355E-08    2           4        -4   # BR(H -> c       cb     )
     8.77625845E-03    2           6        -6   # BR(H -> t       tb     )
     1.36244184E-05    2          21        21   # BR(H -> g       g      )
     6.38646593E-08    2          22        22   # BR(H -> gam     gam    )
     3.59285545E-09    2          23        22   # BR(H -> Z       gam    )
     8.37280274E-07    2          24       -24   # BR(H -> W+      W-     )
     4.18123931E-07    2          23        23   # BR(H -> Z       Z      )
     6.15435639E-06    2          25        25   # BR(H -> h       h      )
    -3.02648048E-24    2          36        36   # BR(H -> A       A      )
     8.10230306E-21    2          23        36   # BR(H -> Z       A      )
     7.93042451E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.36492683E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.96250234E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.68090416E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23332831E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.04579781E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.36443897E+01   # A decays
#          BR         NDA      ID1       ID2
     9.14671846E-01    2           5        -5   # BR(A -> b       bb     )
     7.39116977E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.61333735E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.20972093E-04    2           3        -3   # BR(A -> s       sb     )
     9.05737752E-08    2           4        -4   # BR(A -> c       cb     )
     9.03034263E-03    2           6        -6   # BR(A -> t       tb     )
     2.65934949E-05    2          21        21   # BR(A -> g       g      )
     7.08632114E-08    2          22        22   # BR(A -> gam     gam    )
     2.61963320E-08    2          23        22   # BR(A -> Z       gam    )
     8.52962185E-07    2          23        25   # BR(A -> Z       h      )
     9.57686652E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.60487617E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.78449086E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.93989025E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.69719836E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46034681E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.72837483E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.37898923E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.34620806E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.39807149E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.87628261E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.19455608E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.77522260E-07    2          24        25   # BR(H+ -> W+      h      )
     5.36048484E-14    2          24        36   # BR(H+ -> W+      A      )
     5.17044256E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.55589313E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07336207E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
