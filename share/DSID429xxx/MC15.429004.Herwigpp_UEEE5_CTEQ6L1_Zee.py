# setup Herwig++
include ('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )

evgenConfig.description = "Herwig++ Zee sample with CTEQ6L1 PDF and UE-EE5 tune"
evgenConfig.keywords = ["SM","Z","electron"]
evgenConfig.contact = ["Orel Gueta"]

# Configure Herwig
cmds = """\
## Set up qq -> Z -> e+ e- process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2gZ2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Electron
"""

genSeq.Herwigpp.Commands += cmds.splitlines()

# Set up lepton filter
include("MC15JobOptions/LeptonFilter.py")
evgenConfig.minevents = 5000

