evgenConfig.description = "Excited quark with Pythia8, A14 tune and NNPDF23LO PDF, m=4000 GeV"
evgenConfig.keywords = ["exotic","excitedQuark"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = [ "ning.zhou@cern.ch" ]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
    "ExcitedFermion:bg2bStar = on",
    "4000005:m0 = 4000", # b* mass
    "ExcitedFermion:Lambda =  4000",
    "ExcitedFermion:coupF = 1.0", # SU(2) coupling
    "ExcitedFermion:coupFprime = 1.0", # U(1) coupling
    "ExcitedFermion:coupFcol = 1.0", # SU(3) coupling
    "4000005:mayDecay = on"] # b* -> b g, b gam, b Z0, t W-



  
