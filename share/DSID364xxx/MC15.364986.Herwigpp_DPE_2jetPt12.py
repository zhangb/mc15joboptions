evgenConfig.description = "QCD dijet double diffractive production with J11"
evgenConfig.keywords = ["QCD", "jets"]

include("MC15JobOptions/Herwigpp_Base_Fragment.py")
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds("cteq6ll.LHpdf") + hw.pdf_pomeron_cmds("Pomeron2007", "2007")
genSeq.Herwigpp.Commands = cmds.splitlines()
del cmds

evgenConfig.tune = "NoneDiffractive" 


## Add to commands
cmds = """

#Pomeron flux
set /Herwig/Partons/QCDExtractor:FirstPDF /Herwig/Partons/PomeronFlux
set /Herwig/Partons/QCDExtractor:SecondPDF /Herwig/Partons/PomeronFlux

## Set up QCD jets process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 5*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 10*GeV
"""

genSeq.Herwigpp.Commands +=cmds.splitlines()
del cmds

evgenConfig.findJets = True

from JetRec.JetRecFlags import jetFlags
jetFlags.truthFlavorTags = []

jetFlags.useTracks = False

from JetRec.JetRecStandard import jtm
jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth",
                 modifiersin="none",
                 ptmin=7000.)
        
from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

