include("MC15JobOptions/Sherpa_CT10_Common.py")
evgenConfig.contact = [ "daniel.hayden@cern.ch" , "simone.mazza@mi.infn.it" ]

evgenConfig.description = "ADD->gammagamma production"
evgenConfig.keywords = [ "ADD" , "diphoton" , "exotic", "BSM" ]

evgenConfig.process="""
(processes){
  #
  # jet jet -> gamma gamma
  #
  Process 93 93 -> 22 22 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  PT 22 5.0 E_CMS
  Mass 22 22 1200.0 1800.0
  IsolationCut  22  0.3  2  0.025
  DeltaR 22 22 0.2 1000.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3
  M_S   = 3500
  M_CUT = 3500
  KK_CONVENTION = 5

  MASS[39] = 100.
  MASS[40] = 100.

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""

genSeq.Sherpa_i.Parameters += ["METS_SCALE_MUFMODE=0"]
evgenConfig.minevents = 5000
