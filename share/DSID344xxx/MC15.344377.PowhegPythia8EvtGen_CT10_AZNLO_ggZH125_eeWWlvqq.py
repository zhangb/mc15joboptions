#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z->e+e-WW->e+e-lvqq production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggHZ_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.vdecaymode = 1 # Z->e+e-
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 120
#PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.5

PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
#--------------------------------------------------------------
# Higgs->WW->lvqq at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16']

#--------------------------------------------------------------
# Filters
#--------------------------------------------------------------
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 24
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]
filtSeq.Expression = "XtoVVDecayFilterExtended"

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z->e+e-WW->e+e-lvqq production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'takashi.kubota@cern.ch', 'kathrin.becker@cern.ch' ]

evgenConfig.process = "gg->ZH, H->WW, Z->ee"
evgenConfig.generators  = [ "Powheg", "Pythia8", "EvtGen"]
