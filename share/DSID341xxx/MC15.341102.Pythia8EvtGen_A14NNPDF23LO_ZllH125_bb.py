evgenConfig.description = "PYTHIA8+EVTGEN, ZH, Z->ll, H->bb"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "mH125" ]
evgenConfig.contact     = [ 'paolo.francavilla@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 5 -5', # Higgs decay
                             'HiggsSM:ffbar2HZ = on',
                             '23:onMode = off',
                             '23:onIfAny = 11 13 15'
                             ]
