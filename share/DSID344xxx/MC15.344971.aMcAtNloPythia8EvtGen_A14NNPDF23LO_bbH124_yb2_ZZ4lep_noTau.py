evgenConfig.description = 'MadGraph5_aMC@NLO+Herwig++ bbH production'
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "ZZ" ]
evgenConfig.contact     = [ 'syed.haider.abidi@cern.ch', 'roberto.di.nardo@cern.ch', 'Eleni.Mountricha@cern.ch']
evgenConfig.inputconfcheck = "bbH_mH124_A14NNPDF23LO"

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbHZZ4l_noTau.py")
