#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.84700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.90000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.86000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419807E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399741E+03   # H
        36     2.00000000E+03   # A
        37     2.00151742E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.95285059E+03   # ~d_L
   2000001     4.95274061E+03   # ~d_R
   1000002     4.95260467E+03   # ~u_L
   2000002     4.95266273E+03   # ~u_R
   1000003     4.95285059E+03   # ~s_L
   2000003     4.95274061E+03   # ~s_R
   1000004     4.95260467E+03   # ~c_L
   2000004     4.95266273E+03   # ~c_R
   1000005     4.95202779E+03   # ~b_1
   2000005     4.95356477E+03   # ~b_2
   1000006     5.06397897E+03   # ~t_1
   2000006     5.35352656E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958317E+03   # ~tau_1
   2000015     5.00057571E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.97443170E+03   # ~g
   1000022     1.91983461E+03   # ~chi_10
   1000023    -1.97227441E+03   # ~chi_20
   1000025     2.00717732E+03   # ~chi_30
   1000035     3.12543279E+03   # ~chi_40
   1000024     1.96704217E+03   # ~chi_1+
   1000037     3.12542556E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.34808187E-01   # N_11
  1  2    -4.36968562E-02   # N_12
  1  3     4.79902862E-01   # N_13
  1  4    -4.77326676E-01   # N_14
  2  1     2.22012013E-03   # N_21
  2  2    -3.04626773E-03   # N_22
  2  3    -7.07042050E-01   # N_23
  2  4    -7.07161460E-01   # N_24
  3  1     6.78266584E-01   # N_31
  3  2     5.10824745E-02   # N_32
  3  3    -5.17424499E-01   # N_33
  3  4     5.19246483E-01   # N_34
  4  1     2.53771679E-03   # N_41
  4  2    -9.97733374E-01   # N_42
  4  3    -4.53505234E-02   # N_43
  4  4     4.96488087E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.40443150E-02   # U_11
  1  2     9.97947056E-01   # U_12
  2  1     9.97947056E-01   # U_21
  2  2     6.40443150E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.01205994E-02   # V_11
  1  2     9.97538521E-01   # V_12
  2  1     9.97538521E-01   # V_21
  2  2     7.01205994E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99822621E-01   # cos(theta_t)
  1  2     1.88341853E-02   # sin(theta_t)
  2  1    -1.88341853E-02   # -sin(theta_t)
  2  2     9.99822621E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81338855E-01   # cos(theta_b)
  1  2     7.31968145E-01   # sin(theta_b)
  2  1    -7.31968145E-01   # -sin(theta_b)
  2  2     6.81338855E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04967100E-01   # cos(theta_tau)
  1  2     7.09240007E-01   # sin(theta_tau)
  2  1    -7.09240007E-01   # -sin(theta_tau)
  2  2     7.04967100E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200095E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.86000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52308063E+02   # vev(Q)              
         4     3.11512914E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52716905E-01   # gprime(Q) DRbar
     2     6.26757350E-01   # g(Q) DRbar
     3     1.08126736E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02488578E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71445932E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79737316E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.84700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.90000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -8.28209267E+05   # M^2_Hd              
        22    -8.62386157E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36862149E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.45482352E-05   # gluino decays
#          BR         NDA      ID1       ID2
     6.51387135E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
     9.25315947E-09    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.57903899E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.20500941E-19    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.53465217E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.74171928E-19    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.57903899E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.20500941E-19    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.53465217E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.74171928E-19    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.38239939E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     6.20255684E-13    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.20255684E-13    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.20255684E-13    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.20255684E-13    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.67999960E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.53397107E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.73500515E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.35989908E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.78339953E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.24393373E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.47869968E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.12265922E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.22688380E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.47305598E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.30581455E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.39374427E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.71932635E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.86268302E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.48007464E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.74534035E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.97943149E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.24871296E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.22792291E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.74229682E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.55372821E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.77157634E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.18066460E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.25456449E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.23161375E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.80177392E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.38435066E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01704989E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.81399284E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.50182922E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.25805181E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.47571854E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.83434363E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.42332991E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.66342965E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.23879466E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.38196412E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.20679205E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.06297955E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.79738604E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.43582836E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.01222097E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.27707271E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.31995798E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.55788183E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.98426056E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.92831231E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.12739238E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.90432079E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.75366789E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.27827725E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61189079E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.06305972E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.15574998E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.69111526E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.67009284E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.28379617E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.27235965E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.56418024E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.98469472E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.87223431E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.47792982E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.90353510E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.51561334E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.29159950E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90006375E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.06297955E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.79738604E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.43582836E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.01222097E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.27707271E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.31995798E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.55788183E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.98426056E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.92831231E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.12739238E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.90432079E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.75366789E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.27827725E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61189079E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.06305972E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.15574998E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.69111526E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.67009284E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.28379617E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.27235965E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.56418024E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.98469472E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.87223431E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.47792982E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.90353510E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.51561334E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.29159950E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90006375E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.62777533E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.39179907E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71168890E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.79963526E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73971936E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.35746518E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49754544E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.77266331E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.47993094E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.90707646E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.51998661E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.33796167E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.62777533E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.39179907E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71168890E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.79963526E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73971936E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.35746518E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49754544E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.77266331E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.47993094E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.90707646E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.51998661E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.33796167E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.20520866E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.71349303E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44690216E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.36781641E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61589306E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.42136922E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.24250608E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.63081878E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.20246338E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.58042253E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.00887312E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.44190812E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.65093291E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.08110424E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.31264647E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.62803403E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12960478E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.78089328E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.71801663E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75470710E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.22257949E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.49156286E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.62803403E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12960478E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.78089328E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.71801663E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75470710E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.22257949E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.49156286E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.63032432E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12862120E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.77237681E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.71303781E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75230850E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.08579749E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48681082E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.63003778E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.03777726E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     2.98972166E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.98972166E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     9.96574936E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     9.96574936E-02    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.89629548E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.64631648E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53834856E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.05919352E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46109534E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.41596435E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52530449E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.37163682E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.48123414E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99645528E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89605548E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.07489237E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.18721998E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.83066762E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.05196729E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.99975769E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.43619235E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09244625E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.41464054E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.09244625E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.41464054E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.30656317E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.23121402E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.23121402E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.20241842E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.45318089E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.45318089E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.45318089E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.58600328E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.58600328E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.58600328E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.58600328E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.19533458E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.19533458E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.19533458E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.19533458E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.26478286E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.26478286E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.97425240E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.25459285E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.12110199E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.21240779E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.43521196E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.92393437E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
     4.93217391E-12    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.00969914E-12    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.48787398E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.76946915E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.48787398E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.76946915E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.81438371E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.08582133E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.08582133E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.23639815E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.63761777E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.63761777E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.63761777E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.65150084E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.13857881E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.65150084E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.13857881E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.79860036E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.88478309E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.88478309E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.80466031E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     9.75554313E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     9.75554313E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     9.75554313E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.20692437E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.20692437E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.20692437E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.20692437E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.02307550E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.02307550E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.02307550E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.02307550E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.98411654E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.98411654E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     4.73773446E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.14930299E-08    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     4.73773446E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.14930299E-08    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     8.13704143E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.64623880E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.36021176E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52483831E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.84149035E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46925871E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46925871E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.08080746E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.04844761E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.44549292E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.23433326E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.13600743E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.19912096E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.41491839E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.94896696E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22162993E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02215011E-01    2           5        -5   # BR(h -> b       bb     )
     6.21901873E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20126150E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65744430E-04    2           3        -3   # BR(h -> s       sb     )
     2.00933113E-02    2           4        -4   # BR(h -> c       cb     )
     6.63580289E-02    2          21        21   # BR(h -> g       g      )
     2.30460841E-03    2          22        22   # BR(h -> gam     gam    )
     1.62542707E-03    2          22        23   # BR(h -> Z       gam    )
     2.16468363E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80591925E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78094510E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46676335E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431057E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71225535E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548804E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668817E-05    2           4        -4   # BR(H -> c       cb     )
     9.96071030E-01    2           6        -6   # BR(H -> t       tb     )
     7.97753459E-04    2          21        21   # BR(H -> g       g      )
     2.71755348E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030902E-06    2          23        22   # BR(H -> Z       gam    )
     3.34040510E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66564731E-04    2          23        23   # BR(H -> Z       Z      )
     9.01485503E-04    2          25        25   # BR(H -> h       h      )
     7.19931291E-24    2          36        36   # BR(H -> A       A      )
     2.91300863E-11    2          23        36   # BR(H -> Z       A      )
     6.52326773E-12    2          24       -37   # BR(H -> W+      H-     )
     6.52326773E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380439E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46968226E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898888E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62270676E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677940E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181252E-06    2           4        -4   # BR(A -> c       cb     )
     9.97000765E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680165E-04    2          21        21   # BR(A -> g       g      )
     3.13356696E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290540E-06    2          23        22   # BR(A -> Z       gam    )
     3.25526372E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471670E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35473611E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238943E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81149697E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49704245E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45696970E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732190E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402952E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33630611E-04    2          24        25   # BR(H+ -> W+      h      )
     2.82529615E-13    2          24        36   # BR(H+ -> W+      A      )
