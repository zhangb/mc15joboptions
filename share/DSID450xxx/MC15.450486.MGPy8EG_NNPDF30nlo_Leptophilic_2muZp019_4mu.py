##
## 19.0GeV leptophilic Zprime from pp --> 2mu+Zp --> 4mu
##
zpm=19.0
gzpmul=1.600000e-02
include("MC15JobOptions/MGCtrl_Py8EG_NNPDF30nlo_Leptophilic_2muZp_4mu.py")
