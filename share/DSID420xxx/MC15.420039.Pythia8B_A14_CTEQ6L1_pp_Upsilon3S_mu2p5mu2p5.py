##############################################################
# Job options fragment for pp->Upsilon3S(mu2p5mu2p5)X  
##############################################################
evgenConfig.description = "pp->Upsi(mu2p5mu2p5)"
evgenConfig.keywords = ["inclusive","bottomonium","2muon","Upsilon"]
evgenConfig.minevents = 5000

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Bottomonium_Common.py")

genSeq.Pythia8B.Commands += ['200553:onMode = off']
genSeq.Pythia8B.Commands += ['200553:1:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [200553,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [2.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

