#--------------------------------------------------------------
# Powheg W setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_W_Common.py')
PowhegConfig.idvecbos   = 24 # W+
PowhegConfig.vdecaymode = 2  # munu

# Configure Powheg setup
PowhegConfig.withdamp   = 1
PowhegConfig.ptsqmin    = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 1.1 # increase number of generated events by 10%
PowhegConfig.running_width = 1
PowhegConfig.mass_low = 250.
PowhegConfig.mass_high = 400.

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Wplus->munu production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'W', 'drellYan', 'muon', 'neutrino' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
