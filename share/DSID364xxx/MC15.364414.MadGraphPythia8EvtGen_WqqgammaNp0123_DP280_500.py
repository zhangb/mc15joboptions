from MadGraphControl.MadGraphUtils import *

nevents=20000
mode=0

# merging parameters
ktdurham=30
dparameter=0.4
nJetMax=3
process='pp>w+a'
maxjetflavor=5

### DSID lists (extensions can include systematics samples)
test=[364414]

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = g u c d s b u~ c~ d~ s~ b~
    define w = w+ w-
    define e = e+ e-
    define v = vl vl~
    generate p p > w a, w > q q
    add process p p > w a j, w > q q
    add process p p > w a j j, w > q q
    add process p p > w a j j j, w > q q
    output -f""")
    fcard.close()
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version' : '2.0', 
           'cut_decays'  : 'T', 
           'pdlabel'     : "'nn23lo1'",
           'pta'         : 280.,
           'ptamax'      : 500.,
           'ickkw'       : 0,
           'maxjetflavor': maxjetflavor,
           'ktdurham'    : ktdurham,
           'dparameter'  : dparameter,
           'use_syst'    : 'False'}
process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut=0,
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
    
runName='run_01'     


generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=1)  
  


#### Shower
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_WqqgammaNp0123_LO'
evgenConfig.keywords = ['W','photon','allHadronic','jets','3jet']
evgenConfig.contact = ["Shu.Li@cern.ch","atlas-generators-madgraphcontrol@cern.ch"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process
PYTHIA8_TMS=ktdurham
PYTHIA8_nQuarksMerge=maxjetflavor
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")

genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

# Add filter
#include("MC15JobOptions/DirectPhotonFilter.py")
#filtSeq.DirectPhotonFilter.Ptmin = 280.*GeV
#filtSeq.DirectPhotonFilter.Ptmax = 500.*GeV
