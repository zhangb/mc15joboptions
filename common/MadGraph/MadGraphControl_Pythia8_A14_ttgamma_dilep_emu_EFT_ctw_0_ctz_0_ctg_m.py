from MadGraphControl.MadGraphUtils import *

nevents=1000
mode=0
gridpack_dir='madevent/'
gridpack_mode=True

### DSID lists (extensions can include systematics samples)
test=[412150]

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:
    fcard.write("""
    import model dim6top_LO_UFO
    define l+ = e+ mu+
    define l- = e- mu- 
    define q =  u d s c
    define q~ =  u~ d~ s~ c~
    generate p p > t t~ > e+ ve mu- vm~ b b~ a FCNC=0
    add process p p > t t~ > mu+ vm e- ve~ b b~ a FCNC=0
    output -f""")
    fcard.close()

else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'use_syst':"False"}
    
runName='ctw_0_ctz_0_ctg_m'

process_dir = new_process(grid_pack=gridpack_dir)
#process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
param_card_loc='param_card_'+runName+'.dat'
paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.wait()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card_loc)

#generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)
#for runName in runNames:
#try:
generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,mode=mode,proc_dir=process_dir,run_name=runName,
                 grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed)

outputDS=arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  
# except:
#print "IGNORE THIS ERROR"

   

#evgenConfig.generators = ["MadGraph"]

############################
# Shower JOs will go here
#runName = 'ctw_0_ctz_0_ctg_m'
#evgenConfig.generators += ["MadGraph", "Pythia8"]                                                                                                                                                          
evgenConfig.description = 'MadGraph_EFT'
evgenConfig.keywords += ['top', 'photon']
evgenConfig.contact = ["sreelakshmi.sindhu@cern.ch"]
#evgenConfig.inputfilecheck ='412148'                                                                                                                                                                       
runArgs.inputGeneratorFile=outputDS
#runArgs.inputGeneratorFile='412148._00001.events.tar.gz'                                                                                                                                                   
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")


#theApp.finalize()
#theApp.exit()

