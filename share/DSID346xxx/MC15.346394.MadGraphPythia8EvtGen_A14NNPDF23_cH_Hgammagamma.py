from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------------------------------
# Get run info and determine number of events - and setting it to higher nevents
#---------------------------------------------------------------------------------------------------
efficiency = 1.0
safety_factor = 1.1
if runArgs.maxEvents > 0:
	nevents = int(runArgs.maxEvents*safety_factor/efficiency)
else:
	nevents = int(5000*safety_factor/efficiency)

mode=0


#---------------------------------------------------------------------------------------------------
#Setup proc_card
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
    import model Higgs_Effective_Couplings_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > h c QED<=1
    add process p p > h c~ QED<=1
    output -f""")
fcard.close()



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
#Fetch default LO run_card.dat and set parameters //nn23lo1 with the systematics
extras = { 'lhe_version':'3.0',
           'cut_decays':'F',
           'pdlabel':"'lhapdf'",
           'lhaid'  : '260000',
           'parton_shower':'PYTHIA8',
           'use_syst':"True",
           'sys_scalefact':'1.0 0.5 2.0',
           'sys_pdf'      : 'NNPDF30_nlo_as_0118',
           'ptj':'10',
           'etaj':'4.7'
          }


#old version of PDF 247000, NNPDF23_lo_as_0130_qed
#new version of PDF 260000, NNPDF30_nlo_as_0118
#(recommendations from https://indico.cern.ch/event/777484/contributions/3244052/attachments/1766720/2871333/Recommendation.pdf)

runName='run_01'
#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
mh = 1.250000e+02
masses = {'25' :str(mh)+'  #  MH'}



process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


#create the param_card
import os
if os.access('param_card.dat',os.R_OK):
  print("Deleting old param_card.dat")
  os.remove('param_card.dat')
param_card = "%s/Cards/param_card.dat" % process_dir

build_param_card(param_card_old=param_card,param_card_new='param_card_new.dat',
                 masses=masses)


print_cards()
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
outputDS = arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)



#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords+=['Higgs','SMHiggs']
evgenConfig.contact = ['Anna Ivina <anna.ivina@cern.ch>']
runArgs.inputGeneratorFile=outputDS

#Showeing
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#Decaying Higgs
genSeq.Pythia8.Commands += [
	'25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
    ]

