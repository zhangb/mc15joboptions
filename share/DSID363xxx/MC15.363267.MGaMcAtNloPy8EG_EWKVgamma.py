from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_Vgamma_EW6_LO'
evgenConfig.keywords = ['SM','diboson','VBS','Zgamma','electroweak','2jet','LO']
evgenConfig.contact = ["schae@cern.ch"]

include("MC15JobOptions/MadGraph_NNPDF30_EWKVgamma_LO.py")

evgenConfig.inputconfcheck="MGaMcAtNloPy8EG_EWKVgamma"
