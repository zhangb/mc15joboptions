###############################################################
# Python snippet to generate EvtGen user decay file on the fly
# Bs->K3p5mu3p5nu
#pTHatCut lowered to 5
###############################################################
f = open("BS0_KMUNU.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Decay B_s0\n")
f.write("1.0000  K-    mu+  nu_mu       ISGW2;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
###############################################################

evgenConfig.description = "Exclusive Bs->Kmunu production"
evgenConfig.keywords = ["exclusive","Bs","1muon"]
evgenConfig.minevents = 100
include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 7.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.SignalPDGCodes = [531]
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.EvtInclusiveDecay.userDecayFile = "BS0_KMUNU.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500 
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutOn = False

filtSeq.BSignalFilter.B_PDGCode = 531;
filtSeq.BSignalFilter.Cuts_Final_mu_switch = False
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 3500
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

filtSeq.BSignalFilter.InvMass_switch = True;
filtSeq.BSignalFilter.InvMass_PartId1 = -13
filtSeq.BSignalFilter.InvMass_PartId2 = -321
filtSeq.BSignalFilter.InvMassMin = 3000.0
filtSeq.BSignalFilter.InvMassMax = 7000.0
