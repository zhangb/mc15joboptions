evgenConfig.description = 'Herwig7 NLO dijets with MMHT2014 and dipole shower, slice JZ 6'
evgenConfig.keywords+=['SM','QCD','jets']
evgenConfig.contact = ['javier.llorente.merino@cern.ch']
evgenConfig.minevents = 5000

include("MC15JobOptions/Herwig7EG_Matchbox_MG_H7UEMMHT2014_dipole_multijet_withGridpack.py")

include("MC15JobOptions/JetFilter_JZ6.py")
