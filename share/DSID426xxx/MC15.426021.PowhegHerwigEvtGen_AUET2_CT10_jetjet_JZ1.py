# This includes the setting of random seeds and c.o.m. energy
include('PowhegControl/PowhegControl_Dijet_Common.py')
PowhegConfig.bornktmin = int(runArgs.runNumber)
PowhegConfig.pdf = 11000

PowhegConfig.bornktmin = 2
PowhegConfig.bornsuppfact = 60

PowhegConfig.nEvents *= 3

print PowhegConfig

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

##--------------------------------------------------------------
## EVGEN transform
##--------------------------------------------------------------
evgenConfig.description    = "Powheg + fHerwig/Jimmy dijet production with bornktmin = 2 GeV, bornsuppfact = 60 GeV, muR=muF=1"
evgenConfig.keywords       = [ "QCD", "dijet", "jets", "SM" ]
evgenConfig.contact        = [ "jmonk@cern.ch" ]

evgenConfig.minevents = 1000

if runArgs.trfSubstepName == 'generate' :

  include("MC15JobOptions/nonStandard/Herwig_AUET2_CT10_Common.py")
  include("MC15JobOptions/nonStandard/Herwig_Powheg.py")
  include("MC15JobOptions/nonStandard/Herwig_Photos.py")
  include("MC15JobOptions/nonStandard/Herwig_Tauola.py")
  
## Jet filter
  include("MC15JobOptions/JetFilter_JZ1.py")

  evgenConfig.inputconfcheck = "jetjet_J1"

## Run EvtGen in afterburner mode
include("MC15JobOptions/nonStandard/Herwig_EvtGen.py")


