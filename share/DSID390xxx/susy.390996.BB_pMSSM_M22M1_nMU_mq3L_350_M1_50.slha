#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10164709E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04006465E+01   # W+
        25     1.27030147E+02   # h
        35     3.00021367E+03   # H
        36     3.00000033E+03   # A
        37     3.00111853E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01295624E+03   # ~d_L
   2000001     3.00716984E+03   # ~d_R
   1000002     3.01202601E+03   # ~u_L
   2000002     3.01159578E+03   # ~u_R
   1000003     3.01295624E+03   # ~s_L
   2000003     3.00716984E+03   # ~s_R
   1000004     3.01202601E+03   # ~c_L
   2000004     3.01159578E+03   # ~c_R
   1000005     3.85092116E+02   # ~b_1
   2000005     3.01092522E+03   # ~b_2
   1000006     3.78827021E+02   # ~t_1
   2000006     3.01693111E+03   # ~t_2
   1000011     3.00803446E+03   # ~e_L
   2000011     2.99870916E+03   # ~e_R
   1000012     3.00663093E+03   # ~nu_eL
   1000013     3.00803446E+03   # ~mu_L
   2000013     2.99870916E+03   # ~mu_R
   1000014     3.00663093E+03   # ~nu_muL
   1000015     2.98642001E+03   # ~tau_1
   2000015     3.02297670E+03   # ~tau_2
   1000016     3.00752981E+03   # ~nu_tauL
   1000021     2.31531530E+03   # ~g
   1000022     5.09221993E+01   # ~chi_10
   1000023     1.10184478E+02   # ~chi_20
   1000025    -3.01285130E+03   # ~chi_30
   1000035     3.01304176E+03   # ~chi_40
   1000024     1.10334888E+02   # ~chi_1+
   1000037     3.01388816E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890731E-01   # N_11
  1  2     1.91715720E-03   # N_12
  1  3    -1.46496581E-02   # N_13
  1  4     4.88954108E-04   # N_14
  2  1    -1.53364841E-03   # N_21
  2  2     9.99657263E-01   # N_22
  2  3     2.61307224E-02   # N_23
  2  4    -4.35919639E-04   # N_24
  3  1     1.00444000E-02   # N_31
  3  2    -1.81515380E-02   # N_32
  3  3     7.06791326E-01   # N_33
  3  4     7.07117849E-01   # N_34
  4  1    -1.07370853E-02   # N_41
  4  2     1.87670704E-02   # N_42
  4  3    -7.06787517E-01   # N_43
  4  4     7.07095410E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99318594E-01   # U_11
  1  2     3.69099854E-02   # U_12
  2  1    -3.69099854E-02   # U_21
  2  2     9.99318594E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999810E-01   # V_11
  1  2     6.17004170E-04   # V_12
  2  1     6.17004170E-04   # V_21
  2  2    -9.99999810E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98662294E-01   # cos(theta_t)
  1  2    -5.17070841E-02   # sin(theta_t)
  2  1     5.17070841E-02   # -sin(theta_t)
  2  2     9.98662294E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99704609E-01   # cos(theta_b)
  1  2    -2.43042125E-02   # sin(theta_b)
  2  1     2.43042125E-02   # -sin(theta_b)
  2  2     9.99704609E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06853156E-01   # cos(theta_tau)
  1  2     7.07360315E-01   # sin(theta_tau)
  2  1    -7.07360315E-01   # -sin(theta_tau)
  2  2    -7.06853156E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00255038E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.01647087E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44911664E+02   # higgs               
         4     1.22480738E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.01647087E+03  # The gauge couplings
     1     3.60812893E-01   # gprime(Q) DRbar
     2     6.40989471E-01   # g(Q) DRbar
     3     1.03502458E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.01647087E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.02848904E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.01647087E+03  # The trilinear couplings
  1  1     3.22567674E-07   # A_d(Q) DRbar
  2  2     3.22607031E-07   # A_s(Q) DRbar
  3  3     6.79827585E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.01647087E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.61956868E-07   # A_mu(Q) DRbar
  3  3     1.64284828E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.01647087E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.61542017E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.01647087E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.16057544E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.01647087E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03561862E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.01647087E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.74031655E+04   # M^2_Hd              
        22    -9.21725479E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42617111E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.65017276E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48063177E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48063177E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51936823E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51936823E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.60147061E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     9.80490540E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.19292100E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.70902994E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26909033E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.16172503E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.15060749E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.33177261E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.83505521E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72414980E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65387003E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.26427007E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.14201021E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.81736211E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.52967794E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.28858585E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.94001947E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.76614479E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -7.10834664E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.44012442E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.01464837E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.12398543E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.17436830E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.72197247E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12391086E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.89636194E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63678071E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.27767227E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.02658340E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.23588634E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55447426E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.65123435E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.44552209E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12901946E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.65794566E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63801541E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.96347624E-13    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.27190773E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.03349741E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.70618257E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43488699E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04168510E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55651026E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12391086E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.89636194E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63678071E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.27767227E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.02658340E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.23588634E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55447426E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.65123435E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.44552209E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12901946E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.65794566E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63801541E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.96347624E-13    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.27190773E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.03349741E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.70618257E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43488699E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04168510E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55651026E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06380666E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.64325671E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00979775E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.02587658E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55208027E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997652E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.34759151E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06380666E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.64325671E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00979775E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.02587658E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55208027E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997652E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.34759151E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.83687581E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42750110E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18962862E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38287028E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.77781007E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50755937E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16271117E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.49614421E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.68388805E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32966755E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.01105224E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06416895E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.50746734E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01852673E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.03072654E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.06416895E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.50746734E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01852673E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.03072654E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06572770E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.50666689E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01827641E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03105690E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.05991100E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39957732E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.77229069E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39957732E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.77229069E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06849761E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.90765011E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06849761E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.90765011E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06384967E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.03679081E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.60961467E-13    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.40907538E-10    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.60961467E-13    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.40907538E-10    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.54361102E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54394859E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.04644222E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.04644222E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.03902280E-07    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.43752127E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.43752127E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.53958439E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.79130507E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.81398472E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.65109297E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.86784628E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.23970096E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.48592194E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.19565320E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.96672926E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.19565320E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.96672926E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.81972734E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.46160373E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.46160373E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45337168E-04    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.95759663E-05    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.95759663E-05    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.97690872E-05    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     1.04205249E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.71497759E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.98207114E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.63327328E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.63327328E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.05621190E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.45877165E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.26367996E-11    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     2.26367996E-11    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.09329718E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.09329718E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     5.00794802E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     5.00794802E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     2.26367996E-11    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     2.26367996E-11    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.09329718E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.09329718E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     5.00794802E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     5.00794802E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.31559247E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31559247E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.60164153E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.60164153E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     9.41985014E-11    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     9.41985014E-11    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     6.62711113E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     6.62711113E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     9.41985014E-11    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     9.41985014E-11    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     6.62711113E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     6.62711113E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.79264363E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.79264363E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     5.69712381E-10    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     5.69712381E-10    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     5.69712381E-10    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     5.69712381E-10    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.17080050E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.17080050E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.03567713E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.55533318E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.80827889E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.66812480E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.66812480E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.05947479E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.49912714E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.60766809E-11    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.60766809E-11    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.65078798E-11    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.65078798E-11    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.88889542E-13    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.88889542E-13    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     6.13399228E-11    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     6.13399228E-11    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.60766809E-11    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.60766809E-11    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.65078798E-11    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.65078798E-11    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.88889542E-13    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.88889542E-13    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     6.13399228E-11    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     6.13399228E-11    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.25710284E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25710284E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.64947995E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.64947995E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.06139734E-10    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.06139734E-10    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     7.82490595E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     7.82490595E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.06139734E-10    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.06139734E-10    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     7.82490595E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     7.82490595E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.59369186E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.59369186E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     6.61293148E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     6.61293148E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     6.61293148E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     6.61293148E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     4.88996479E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     4.88996479E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.92639990E-03   # h decays
#          BR         NDA      ID1       ID2
     5.33807230E-01    2           5        -5   # BR(h -> b       bb     )
     6.71447589E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.37685324E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.02908914E-04    2           3        -3   # BR(h -> s       sb     )
     2.18429477E-02    2           4        -4   # BR(h -> c       cb     )
     7.25566661E-02    2          21        21   # BR(h -> g       g      )
     2.56492935E-03    2          22        22   # BR(h -> gam     gam    )
     1.90020137E-03    2          22        23   # BR(h -> Z       gam    )
     2.65227902E-01    2          24       -24   # BR(h -> W+      W-     )
     3.41899410E-02    2          23        23   # BR(h -> Z       Z      )
     2.48297584E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.52399281E+01   # H decays
#          BR         NDA      ID1       ID2
     9.16533721E-01    2           5        -5   # BR(H -> b       bb     )
     5.49707093E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.94363292E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.38677351E-04    2           3        -3   # BR(H -> s       sb     )
     6.69297646E-08    2           4        -4   # BR(H -> c       cb     )
     6.67667908E-03    2           6        -6   # BR(H -> t       tb     )
     2.19098475E-06    2          21        21   # BR(H -> g       g      )
     2.91906365E-08    2          22        22   # BR(H -> gam     gam    )
     2.72703424E-09    2          23        22   # BR(H -> Z       gam    )
     8.78419271E-07    2          24       -24   # BR(H -> W+      W-     )
     4.38668122E-07    2          23        23   # BR(H -> Z       Z      )
     4.73791693E-06    2          25        25   # BR(H -> h       h      )
    -1.36648224E-26    2          36        36   # BR(H -> A       A      )
     1.05333000E-17    2          23        36   # BR(H -> Z       A      )
     7.57247864E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.40636229E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.79908444E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.27749673E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.99732387E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.29719571E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.43380996E+01   # A decays
#          BR         NDA      ID1       ID2
     9.35159735E-01    2           5        -5   # BR(A -> b       bb     )
     5.60852699E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.98303835E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.43558283E-04    2           3        -3   # BR(A -> s       sb     )
     6.87286952E-08    2           4        -4   # BR(A -> c       cb     )
     6.85235506E-03    2           6        -6   # BR(A -> t       tb     )
     2.01795267E-05    2          21        21   # BR(A -> g       g      )
     4.14261481E-08    2          22        22   # BR(A -> gam     gam    )
     1.98770536E-08    2          23        22   # BR(A -> Z       gam    )
     8.92640233E-07    2          23        25   # BR(A -> Z       h      )
     7.79403837E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.50275206E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.91016435E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.34127832E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.77502622E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48938155E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.20969112E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.84201971E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.53203031E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.08250660E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.22706417E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.35754531E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.30300614E-07    2          24        25   # BR(H+ -> W+      h      )
     4.81708541E-14    2          24        36   # BR(H+ -> W+      A      )
     4.35333241E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.02336146E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.79574629E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
