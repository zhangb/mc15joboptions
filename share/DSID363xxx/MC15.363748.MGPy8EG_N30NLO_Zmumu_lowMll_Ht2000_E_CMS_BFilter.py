import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=2000
ihtmax=-1
HTslice='highHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns_Mll10to40_ptl5.py')
evgenConfig.minevents=2

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Zmumu_Mll10to40_ptl5_Ht2000_E_CMS_13TeV"
