#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373714E+01   # W+
        25     1.26000000E+02   # h
        35     2.00415562E+03   # H
        36     2.00000000E+03   # A
        37     2.00209195E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.01048258E+03   # ~d_L
   2000001     5.01037294E+03   # ~d_R
   1000002     5.01023757E+03   # ~u_L
   2000002     5.01029578E+03   # ~u_R
   1000003     5.01048258E+03   # ~s_L
   2000003     5.01037294E+03   # ~s_R
   1000004     5.01023757E+03   # ~c_L
   2000004     5.01029578E+03   # ~c_R
   1000005     5.01032993E+03   # ~b_1
   2000005     5.01052705E+03   # ~b_2
   1000006     5.14064723E+03   # ~t_1
   2000006     5.43786066E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     1.33583502E+03   # ~g
   1000022     1.91293972E+02   # ~chi_10
   1000023    -2.16736690E+02   # ~chi_20
   1000025     2.96406515E+02   # ~chi_30
   1000035     3.12904224E+03   # ~chi_40
   1000024     2.14507242E+02   # ~chi_1+
   1000037     3.12904183E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69120475E-01   # N_11
  1  2    -2.32981552E-02   # N_12
  1  3     6.31923445E-01   # N_13
  1  4    -6.16486769E-01   # N_14
  2  1     1.80290218E-02   # N_21
  2  2    -4.70673776E-03   # N_22
  2  3    -7.05102327E-01   # N_23
  2  4    -7.08860712E-01   # N_24
  3  1     8.82950031E-01   # N_31
  3  2     1.29514074E-02   # N_32
  3  3    -3.21342493E-01   # N_33
  3  4     3.42009512E-01   # N_34
  4  1     4.21100296E-04   # N_41
  4  2    -9.99633585E-01   # N_42
  4  3    -1.55714619E-02   # N_43
  4  4     2.21370418E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20139614E-02   # U_11
  1  2     9.99757663E-01   # U_12
  2  1     9.99757663E-01   # U_21
  2  2     2.20139614E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13026531E-02   # V_11
  1  2     9.99509952E-01   # V_12
  2  1     9.99509952E-01   # V_21
  2  2     3.13026531E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99864647E-01   # cos(theta_t)
  1  2    -1.64525888E-02   # sin(theta_t)
  2  1     1.64525888E-02   # -sin(theta_t)
  2  2     9.99864647E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71046267E-01   # cos(theta_b)
  1  2     8.82108505E-01   # sin(theta_b)
  2  1    -8.82108505E-01   # -sin(theta_b)
  2  2     4.71046267E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84681065E-01   # cos(theta_tau)
  1  2     7.28842808E-01   # sin(theta_tau)
  2  1    -7.28842808E-01   # -sin(theta_tau)
  2  2     6.84681065E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203165E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51763485E+02   # vev(Q)              
         4     3.86520727E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099764E-01   # gprime(Q) DRbar
     2     6.29225058E-01   # g(Q) DRbar
     3     1.08870429E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02638176E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72298515E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79970310E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02033569E+06   # M^2_Hd              
        22    -5.53129469E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961757E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.58902007E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.56142582E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.02173477E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.20016870E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.23260425E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.35881488E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.94057131E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     6.59279169E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.57268104E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.10269593E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.23260425E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.35881488E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.94057131E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     6.59279169E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.57268104E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.10269593E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.39725075E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.30078250E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.99745454E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.46825161E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.84392754E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.11421073E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.32851149E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.32851149E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.32851149E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.32851149E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36720765E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36720765E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.33149078E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.30104767E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.11013752E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.88971755E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.34150550E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.37774604E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.66844490E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.90395114E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.66973254E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.27764704E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.12129550E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.93717311E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.53589663E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.28635640E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -8.27097065E-06    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.90026170E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.64677804E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.92171592E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.21612761E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.62331218E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.85285539E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.70115150E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.99869103E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.52972580E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.19566391E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.16116659E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -6.14490190E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.19344187E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.23392613E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.12256739E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.45604117E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.48877512E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.34886267E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.61912010E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.41312866E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.76917093E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.84159306E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.49441412E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.46436151E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.20425302E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.37216480E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.43725974E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.88423531E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.51048167E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.76795473E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.10569229E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.35009354E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     9.96527601E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.47068664E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.51619289E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.00528717E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.54858085E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.46439336E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.51231373E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.03083137E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.77997194E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.88560630E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.47077147E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.77173765E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.10615619E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.27057330E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.57861036E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.80554227E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.09848609E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.77677015E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88319097E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.46436151E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.20425302E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.37216480E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.43725974E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.88423531E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.51048167E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.76795473E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.10569229E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.35009354E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.96527601E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.47068664E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.51619289E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.00528717E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.54858085E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.46439336E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.51231373E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.03083137E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.77997194E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.88560630E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.47077147E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.77173765E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.10615619E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.27057330E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.57861036E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.80554227E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.09848609E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.77677015E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88319097E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543380E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.02978183E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04708868E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80176892E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484157E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77821961E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342839E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528900E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20780271E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25817594E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78893846E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60356655E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543380E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.02978183E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04708868E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80176892E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484157E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77821961E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342839E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528900E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20780271E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25817594E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78893846E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60356655E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238512E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35910248E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64064235E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72997561E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460544E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259183E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59108312E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677929E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832537E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14085808E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06884224E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.46026235E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46196813E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85018579E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92603800E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528782E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74681806E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53669915E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62286333E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698338E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37051255E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022966E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528782E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74681806E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53669915E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62286333E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698338E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37051255E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022966E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849685E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74025166E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53494330E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62100903E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401603E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51123061E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430253E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26387726E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76496155E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34388299E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34388299E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462848E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462848E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08270055E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38815223E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03044831E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48722087E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98329117E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91610809E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03607141E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91818803E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94137530E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96085733E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90620214E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.85935104E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01630499E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94969733E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02590451E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93204268E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20528046E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.64704718E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59282163E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49167937E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56427336E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66220349E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22030196E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57944888E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22030196E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57944888E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13467000E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60419178E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60419178E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50094597E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19573032E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19573032E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19573032E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.06228285E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.06228285E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.06228285E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.06228285E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.02076114E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.02076114E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.02076114E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.02076114E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95896933E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95896933E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.64244601E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.35959256E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.78201640E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.78201640E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.00780973E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.90318662E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.49073977E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38818870E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03688735E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97355272E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80716360E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97924437E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97924437E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49024714E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49645982E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70872501E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00796958E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60693458E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91126792E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.87855445E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81792934E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37454968E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93513723E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93513723E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43550266E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72674234E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78959005E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93282819E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85678518E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21677787E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01599508E-01    2           5        -5   # BR(h -> b       bb     )
     6.22623174E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20381459E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66284535E-04    2           3        -3   # BR(h -> s       sb     )
     2.01163490E-02    2           4        -4   # BR(h -> c       cb     )
     6.64323679E-02    2          21        21   # BR(h -> g       g      )
     2.32236185E-03    2          22        22   # BR(h -> gam     gam    )
     1.62703320E-03    2          22        23   # BR(h -> Z       gam    )
     2.16861918E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80914786E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01423213E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38951122E-03    2           5        -5   # BR(H -> b       bb     )
     2.32127091E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20655687E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05072848E-06    2           3        -3   # BR(H -> s       sb     )
     9.48257687E-06    2           4        -4   # BR(H -> c       cb     )
     9.38260901E-01    2           6        -6   # BR(H -> t       tb     )
     7.51436807E-04    2          21        21   # BR(H -> g       g      )
     2.63480043E-06    2          22        22   # BR(H -> gam     gam    )
     1.09163835E-06    2          23        22   # BR(H -> Z       gam    )
     3.15583054E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57361066E-04    2          23        23   # BR(H -> Z       Z      )
     8.52480365E-04    2          25        25   # BR(H -> h       h      )
     8.34733423E-24    2          36        36   # BR(H -> A       A      )
     3.33095728E-11    2          23        36   # BR(H -> Z       A      )
     2.45175968E-12    2          24       -37   # BR(H -> W+      H-     )
     2.45175968E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49655661E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87374435E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75268833E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52344663E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45476107E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45952710E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11397442E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05890785E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39238996E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771572E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12325512E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093391E-06    2           3        -3   # BR(A -> s       sb     )
     9.38479608E-06    2           4        -4   # BR(A -> c       cb     )
     9.39251652E-01    2           6        -6   # BR(A -> t       tb     )
     8.89019532E-04    2          21        21   # BR(A -> g       g      )
     2.67563665E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307836E-06    2          23        22   # BR(A -> Z       gam    )
     3.07528451E-04    2          23        25   # BR(A -> Z       h      )
     5.99332355E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30074177E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12623643E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.73981650E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19770712E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48883561E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69825361E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853493E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23426688E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658525E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29602654E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42052682E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13753870E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367395E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40917095E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.15175699E-04    2          24        25   # BR(H+ -> W+      h      )
     1.32397318E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28582707E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26671623E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55347627E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
