#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.85454138E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05421408E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414512E+03   # H
        36     2.00000000E+03   # A
        37     2.00172906E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989275E+03   # ~u_L
   2000002     4.99994936E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989275E+03   # ~c_L
   2000004     4.99994936E+03   # ~c_R
   1000005     4.99932876E+03   # ~b_1
   2000005     5.00083047E+03   # ~b_2
   1000006     4.98342993E+03   # ~t_1
   2000006     5.02097944E+03   # ~t_2
   1000011     5.00008193E+03   # ~e_L
   2000011     5.00007596E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008193E+03   # ~mu_L
   2000013     5.00007596E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99958657E+03   # ~tau_1
   2000015     5.00057191E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     1.84333142E+03   # ~chi_10
   1000023    -1.85174821E+03   # ~chi_20
   1000025     1.86151345E+03   # ~chi_30
   1000035     3.00144471E+03   # ~chi_40
   1000024     1.85102740E+03   # ~chi_1+
   1000037     3.00144442E+03   # ~chi_2+
   1000039     2.33706464E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.96042270E-01   # N_11
  1  2    -1.02640120E-02   # N_12
  1  3    -5.72829183E-01   # N_13
  1  4    -5.62578875E-01   # N_14
  2  1     1.17559465E-02   # N_21
  2  2    -1.59567296E-02   # N_22
  2  3     7.06901106E-01   # N_23
  2  4    -7.07034657E-01   # N_24
  3  1     8.02866681E-01   # N_31
  3  2     8.73619894E-03   # N_32
  3  3     4.14914930E-01   # N_33
  3  4     4.27988751E-01   # N_34
  4  1     7.08786244E-04   # N_41
  4  2    -9.99781832E-01   # N_42
  4  3    -1.77591229E-03   # N_43
  4  4     2.07998099E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.49675295E-03   # U_11
  1  2     9.99996883E-01   # U_12
  2  1     9.99996883E-01   # U_21
  2  2     2.49675295E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.94187859E-02   # V_11
  1  2    -9.99567174E-01   # V_12
  2  1     9.99567174E-01   # V_21
  2  2     2.94187859E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07639368E-01   # cos(theta_t)
  1  2    -7.06573793E-01   # sin(theta_t)
  2  1     7.06573793E-01   # -sin(theta_t)
  2  2     7.07639368E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.81388526E-01   # cos(theta_b)
  1  2     7.31921906E-01   # sin(theta_b)
  2  1    -7.31921906E-01   # -sin(theta_b)
  2  2    -6.81388526E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04962333E-01   # cos(theta_tau)
  1  2     7.09244746E-01   # sin(theta_tau)
  2  1    -7.09244746E-01   # -sin(theta_tau)
  2  2    -7.04962333E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90198739E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52002523E+02   # vev(Q)              
         4     4.75862448E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52708426E-01   # gprime(Q) DRbar
     2     6.26701918E-01   # g(Q) DRbar
     3     1.07892255E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02717683E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73049601E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79615769E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.85454138E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.82053424E+05   # M^2_Hd              
        22    -8.57659121E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36837260E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.83278310E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.74014130E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     9.24464927E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.66666835E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.08163963E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.91981488E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.16056440E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.26671014E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.12471617E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.66666835E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.08163963E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.91981488E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.16056440E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.26671014E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.12471617E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.04483038E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.55637495E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.90704206E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.19908183E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.45392380E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.95519425E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.95519425E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.95519425E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.95519425E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.47688760E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.47688760E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.61651493E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.07580022E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.79722734E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.87901897E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.58926900E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.78239198E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.17203564E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.37042569E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.76935383E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.55256053E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.02022920E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.62754284E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.30416951E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.02598418E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.60169579E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.44518975E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.10222741E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.00341123E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.56789508E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.21580820E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.73033299E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.20736365E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.46859729E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.21009434E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.14890705E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.43051231E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.55145222E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.13125428E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.91073202E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32454581E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.83019778E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.04419209E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96456418E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.38796587E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.35075498E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80784789E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.93892614E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.23672974E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.87656651E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.79161248E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.80613971E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55882543E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.04663368E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.81081074E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.21291770E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.56297580E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96462577E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.06771446E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.35636982E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.43320733E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.94151043E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.90834497E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.88431685E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.79216351E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74699213E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.02910578E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.56287733E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.26512158E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.13511221E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88704207E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96456418E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.38796587E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.35075498E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80784789E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.93892614E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.23672974E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.87656651E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.79161248E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.80613971E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55882543E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.04663368E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.81081074E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.21291770E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.56297580E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96462577E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.06771446E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.35636982E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.43320733E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.94151043E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.90834497E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.88431685E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.79216351E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74699213E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.02910578E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.56287733E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.26512158E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.13511221E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88704207E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.85874903E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.39526676E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.43840715E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.07561745E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.79249438E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.34421805E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59185420E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.84051254E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.56698274E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.38360053E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.43163090E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.76416647E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.85874903E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.39526676E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.43840715E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.07561745E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.79249438E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.34421805E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59185420E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.84051254E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.56698274E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.38360053E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.43163090E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.76416647E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.34921707E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.78453898E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.89121066E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.13966369E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.68849792E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.26872591E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.38113944E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.80280106E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35721508E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.66761820E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.86752855E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.20073873E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.70387280E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.96132968E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.41194141E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.85855496E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.09787174E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.59216703E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.95547768E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79670809E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.80788970E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.58655691E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.85855496E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.09787174E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.59216703E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.95547768E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79670809E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.80788970E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.58655691E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86094528E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.09277698E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.59000128E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.94715990E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.79437145E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.71583898E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.58188647E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.80500856E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.25994934E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.55993576E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.02325845E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18665143E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18553464E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.18624789E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41999864E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53786879E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48897645E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46786766E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.95227455E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.51005962E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.49695571E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.93034048E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.95466235E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51065004E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.53468761E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.67321730E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.26480505E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02982172E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.99541636E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40753910E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82268002E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16245769E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81509136E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.16331223E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.15996736E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.11634029E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.31482738E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.31482738E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.31482738E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.38530514E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.38530514E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.12843513E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.12843513E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01644960E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01644960E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.54154687E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.74102174E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.72989718E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.56899522E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     9.30483740E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     7.32214875E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.84708541E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.78310677E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.83481220E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.01615379E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55494412E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55365764E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.22981243E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.33745568E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.33745568E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.33745568E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.05009820E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.24466941E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.52464197E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.22842382E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.29934361E-08    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.19797871E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.19726028E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.76009367E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.39249998E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.39249998E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.39249998E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.95010212E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.95010212E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.78448842E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.78448842E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.50027513E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.50027513E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.49697797E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.49697797E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.65574870E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.65574870E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.42010889E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.64130982E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.04194287E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.34186140E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47207782E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47207782E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.59642018E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.47106236E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.84879983E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.60064579E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.13658925E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.97879487E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.20559967E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.25889864E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07521517E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16816905E-01    2           5        -5   # BR(h -> b       bb     )
     6.39117718E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26224134E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79389444E-04    2           3        -3   # BR(h -> s       sb     )
     2.06821846E-02    2           4        -4   # BR(h -> c       cb     )
     6.71542956E-02    2          21        21   # BR(h -> g       g      )
     2.30501013E-03    2          22        22   # BR(h -> gam     gam    )
     1.54151072E-03    2          22        23   # BR(h -> Z       gam    )
     2.01199732E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56829757E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78125401E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48550204E-03    2           5        -5   # BR(H -> b       bb     )
     2.46429536E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71220155E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546976E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666577E-05    2           4        -4   # BR(H -> c       cb     )
     9.96052945E-01    2           6        -6   # BR(H -> t       tb     )
     7.97586706E-04    2          21        21   # BR(H -> g       g      )
     2.73438431E-06    2          22        22   # BR(H -> gam     gam    )
     1.16028678E-06    2          23        22   # BR(H -> Z       gam    )
     3.33675355E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66382725E-04    2          23        23   # BR(H -> Z       Z      )
     9.01530836E-04    2          25        25   # BR(H -> h       h      )
     8.55597277E-24    2          36        36   # BR(H -> A       A      )
     3.49210937E-11    2          23        36   # BR(H -> Z       A      )
     5.72436870E-12    2          24       -37   # BR(H -> W+      H-     )
     5.72436870E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82387389E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48826125E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894455E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62255005E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675874E-06    2           3        -3   # BR(A -> s       sb     )
     9.96163148E-06    2           4        -4   # BR(A -> c       cb     )
     9.96982645E-01    2           6        -6   # BR(A -> t       tb     )
     9.43663015E-04    2          21        21   # BR(A -> g       g      )
     3.04154930E-06    2          22        22   # BR(A -> gam     gam    )
     1.35293477E-06    2          23        22   # BR(A -> Z       gam    )
     3.25180672E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74516150E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39600959E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49235693E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81138206E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52345769E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45682043E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729212E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403195E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33349488E-04    2          24        25   # BR(H+ -> W+      h      )
     5.42613591E-13    2          24        36   # BR(H+ -> W+      A      )
