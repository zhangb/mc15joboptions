#--------------------------------------------------------------                                                                                                                                                          
# EVGEN configuration                                                                                                                                                                                                    
#--------------------------------------------------------------                                                                                                                                                          

# Provide config information                                                                                                                                                                                                                                                  
evgenConfig.generators  += ["aMcAtNlo", "Herwig7"]
evgenConfig.tune         = "H7.1-Default"
evgenConfig.description  = 'MG5_aMC@NLO+Herwig7+EvtGen, H7p1 default tune, ME NNPDF 3.0 NLO, with single lepton filter from DSID 412121 LHE files'
evgenConfig.keywords     = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact      = ['aknue@cern.ch']
#evgenConfig.minevents = 1000                                                                                                                                                                                                                                                   

# initialize Herwig7 generator configuration for showering of LHE files                                                                                               
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

#include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen                                                                                                                                                                                                                
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7                                                                                                                                                                                                          
Herwig7Config.run()

## NonAllHad filter                                                                                                                                                                                                                                    
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

filtSeq.Expression="TTbarWToLeptonFilter"
