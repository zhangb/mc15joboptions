#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376834E+01   # W+
        25     1.26000000E+02   # h
        35     2.00415407E+03   # H
        36     2.00000000E+03   # A
        37     2.00209659E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.01048270E+03   # ~d_L
   2000001     5.01037294E+03   # ~d_R
   1000002     5.01023745E+03   # ~u_L
   2000002     5.01029577E+03   # ~u_R
   1000003     5.01048270E+03   # ~s_L
   2000003     5.01037294E+03   # ~s_R
   1000004     5.01023745E+03   # ~c_L
   2000004     5.01029577E+03   # ~c_R
   1000005     5.01034618E+03   # ~b_1
   2000005     5.01051093E+03   # ~b_2
   1000006     5.14083347E+03   # ~t_1
   2000006     5.43797664E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011982E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     1.33583568E+03   # ~g
   1000022     1.46516414E+02   # ~chi_10
   1000023    -1.62939902E+02   # ~chi_20
   1000025     2.94796871E+02   # ~chi_30
   1000035     3.12909420E+03   # ~chi_40
   1000024     1.60728241E+02   # ~chi_1+
   1000037     3.12909381E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08704411E-01   # N_11
  1  2    -2.47367618E-02   # N_12
  1  3     6.79339408E-01   # N_13
  1  4    -6.65272612E-01   # N_14
  2  1     2.00319310E-02   # N_21
  2  2    -4.81330627E-03   # N_22
  2  3    -7.04243274E-01   # N_23
  2  4    -7.09659753E-01   # N_24
  3  1     9.50946968E-01   # N_31
  3  2     8.56813906E-03   # N_32
  3  3    -2.05691192E-01   # N_33
  3  4     2.30906009E-01   # N_34
  4  1     4.15225658E-04   # N_41
  4  2    -9.99645693E-01   # N_42
  4  3    -1.51826886E-02   # N_43
  4  4     2.18586793E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14642978E-02   # U_11
  1  2     9.99769615E-01   # U_12
  2  1     9.99769615E-01   # U_21
  2  2     2.14642978E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09092189E-02   # V_11
  1  2     9.99522196E-01   # V_12
  2  1     9.99522196E-01   # V_21
  2  2     3.09092189E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99845630E-01   # cos(theta_t)
  1  2    -1.75703207E-02   # sin(theta_t)
  2  1     1.75703207E-02   # -sin(theta_t)
  2  2     9.99845630E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08521652E-01   # cos(theta_b)
  1  2     9.12748629E-01   # sin(theta_b)
  2  1    -9.12748629E-01   # -sin(theta_b)
  2  2     4.08521652E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76624976E-01   # cos(theta_tau)
  1  2     7.36327809E-01   # sin(theta_tau)
  2  1    -7.36327809E-01   # -sin(theta_tau)
  2  2     6.76624976E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90203651E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51743150E+02   # vev(Q)              
         4     3.88339752E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146804E-01   # gprime(Q) DRbar
     2     6.29569974E-01   # g(Q) DRbar
     3     1.08870424E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02646309E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72323528E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79976606E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04072070E+06   # M^2_Hd              
        22    -5.51559168E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111710E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.89078311E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.71367972E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.93501639E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.73194855E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.25867783E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     9.87912170E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.18829618E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.45141213E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.67030508E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.30468745E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.24869879E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     9.87912170E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.18829618E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.45141213E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.67030508E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.30468745E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.24869879E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.18536436E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.31023223E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.47693033E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.72470978E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.90010716E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.55250790E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.83694915E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.83694915E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.83694915E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.83694915E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.38902261E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.38902261E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.33356464E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.70500649E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.11386149E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.47356174E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.34255125E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.46881138E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.67101865E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.89978753E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     2.77869923E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.37881365E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.12398923E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.75453491E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.56678145E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.45395022E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -8.74486252E-06    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.90192217E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.74350158E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.91806804E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.73507854E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.13551610E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.18082363E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.08679720E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.82360235E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.53208925E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.62176054E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.67008878E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.28779406E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.09705594E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.58463377E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.23003472E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.63263804E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.16105059E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.47084718E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -2.78685185E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.60630951E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.03425344E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.22860773E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.18023355E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.46457900E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.40966774E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.80983128E-08    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.67150956E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.88709794E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.47649975E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.77366790E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.10488997E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.35008847E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.32134280E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.81891841E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.08001644E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.92268209E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.54860301E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.46461061E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.25589843E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.66042275E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.20290898E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.88845799E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.12043579E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.77745274E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.10535529E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.27057231E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.11818677E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.70661695E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.05574154E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.56300036E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88319690E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.46457900E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.40966774E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.80983128E-08    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.67150956E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.88709794E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.47649975E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.77366790E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.10488997E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.35008847E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.32134280E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.81891841E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.08001644E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.92268209E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.54860301E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.46461061E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.25589843E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.66042275E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.20290898E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.88845799E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.12043579E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.77745274E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.10535529E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.27057231E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.11818677E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.70661695E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.05574154E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.56300036E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88319690E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762158E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54418629E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89079009E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04822848E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563267E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45642584E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497471E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513772E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57515141E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03022106E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03845400E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42243356E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762158E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54418629E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89079009E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04822848E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563267E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45642584E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497471E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513772E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57515141E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03022106E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03845400E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42243356E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957290E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08343215E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84765415E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56350795E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26706318E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289845E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53594076E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825927E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319049E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61314517E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12663841E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05231171E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49093321E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85170212E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98398900E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747271E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74518380E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80488568E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92076380E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774195E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33886509E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178233E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747271E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74518380E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80488568E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92076380E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774195E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33886509E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178233E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068723E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74204421E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80282147E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91856707E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477098E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48070184E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584769E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23461761E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87423474E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36073783E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36073783E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024649E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024649E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03764394E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36650868E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99039933E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247947E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94610768E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26185907E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99432730E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31617377E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33926770E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91672500E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29765052E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34163107E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02293374E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82546655E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.02115125E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03336532E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96129224E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.34244290E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.25912911E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.51030766E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65828222E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18219561E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01449669E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27777339E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65371961E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27777339E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65371961E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83549232E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77316490E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77316490E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52635611E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53267802E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53267802E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53267802E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.36114691E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.36114691E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.36114691E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.36114691E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.87049129E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.87049129E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.87049129E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.87049129E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36088884E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36088884E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63922690E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83680211E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19049268E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99725696E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99725696E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79620690E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.18427571E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.83917303E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47548624E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.37957183E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36654424E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03775302E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93137584E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02313754E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94056767E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94056767E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71043960E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76514337E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63781582E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36458825E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01152108E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77917745E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22317109E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48837533E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19293169E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33290227E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33290227E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44992751E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77862268E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80600625E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81042415E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61840280E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21665718E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01591867E-01    2           5        -5   # BR(h -> b       bb     )
     6.22641899E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20388087E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66298545E-04    2           3        -3   # BR(h -> s       sb     )
     2.01169117E-02    2           4        -4   # BR(h -> c       cb     )
     6.64342033E-02    2          21        21   # BR(h -> g       g      )
     2.32841915E-03    2          22        22   # BR(h -> gam     gam    )
     1.62709775E-03    2          22        23   # BR(h -> Z       gam    )
     2.16858342E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80922826E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01474534E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38950282E-03    2           5        -5   # BR(H -> b       bb     )
     2.32097087E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20549613E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05059277E-06    2           3        -3   # BR(H -> s       sb     )
     9.48137215E-06    2           4        -4   # BR(H -> c       cb     )
     9.38141658E-01    2           6        -6   # BR(H -> t       tb     )
     7.51341641E-04    2          21        21   # BR(H -> g       g      )
     2.62941732E-06    2          22        22   # BR(H -> gam     gam    )
     1.09158391E-06    2          23        22   # BR(H -> Z       gam    )
     3.15681348E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57410090E-04    2          23        23   # BR(H -> Z       Z      )
     8.52389903E-04    2          25        25   # BR(H -> h       h      )
     8.34404089E-24    2          36        36   # BR(H -> A       A      )
     3.32434830E-11    2          23        36   # BR(H -> Z       A      )
     2.41490664E-12    2          24       -37   # BR(H -> W+      H-     )
     2.41490664E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71021247E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52115620E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36077388E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64260736E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94578805E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53657007E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87754011E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05932193E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39241763E-03    2           5        -5   # BR(A -> b       bb     )
     2.29748134E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12242649E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082467E-06    2           3        -3   # BR(A -> s       sb     )
     9.38383876E-06    2           4        -4   # BR(A -> c       cb     )
     9.39155841E-01    2           6        -6   # BR(A -> t       tb     )
     8.88928846E-04    2          21        21   # BR(A -> g       g      )
     2.69763990E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304749E-06    2          23        22   # BR(A -> Z       gam    )
     3.07632923E-04    2          23        25   # BR(A -> Z       h      )
     5.85744729E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28908974E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65398421E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84285365E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65315501E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36566870E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93382523E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97901671E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23436731E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630657E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29504129E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42059221E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13692695E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02355205E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40805321E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.15278960E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33855701E-12    2          24        36   # BR(H+ -> W+      A      )
     5.57859731E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23535842E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29292717E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
