evgenConfig.description = "Single K_L with flat eta-phi and log Pt in [100, 2000] GeV"
evgenConfig.keywords = ["singleParticle"]
 
include("MC15JobOptions/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 130
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.LogSampler(100, 2000000.), eta=[-2.4, 2.4])

