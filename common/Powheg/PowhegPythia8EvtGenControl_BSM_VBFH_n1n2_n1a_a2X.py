# MC15.343890.PowhegPythia8EvtGen_BSM_VBFH_n1n2_n1a_ll_8_10_95_2LMET100.py
# MC15.343890.PowhegPythia8EvtGen_BSM_VBFH_n1n2_n1a_tt_8_10_95_2LMET100.py
# MC15.343890.PowhegPythia8EvtGen_BSM_VBFH_n1n2_n1a_bb_8_10_95_MET30.py

runconf=runArgs.jobConfig[0].split("_")
LepFlavor=runconf[2].replace("Z","").replace("H","")
fs_string=runconf[5]
A_Mass=runconf[6].replace("p",".")
N1_Mass=runconf[7].replace("p",".")
N2_Mass=runconf[8].replace("p",".").replace("..y","")
filteroptions=runconf[9].replace(".py","")

if fs_string == 'll':
    # Muons 
    axino_decay = '13 -13'
elif fs_string == 'tt':
    # Taus
    axino_decay = '15 -15'
elif fs_string == 'bb':
    # B quarks
    axino_decay = '5 -5'
else:
    # Not supported
    raise RuntimeError("Invalid final state string, exiting!") 

print ("-----------------------------------------------")
print ("Parsed JobOption configuration:")
print ("FS = " + str(fs_string))
print ("A MASS = " + str(A_Mass))
print ("N1 MASS = " + str(N1_Mass))
print ("N2 MASS = " + str(N2_Mass))
print ("PYTHIA DECAY MODE = " + axino_decay)
print ("FILTER OPTIONS = " + filteroptions)
print ("-----------------------------------------------")

#evgenConfig.inputfilecheck = "TXT"
#evgenConfig.inputconfcheck = "VBFH125_NNPDF30"

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
  infile = f
  f1 = open( infile )
  newfile = infile+'.temp'
  f2 = open(newfile,'w')
  counter = 0
  for line in f1:
      if line.startswith('      25     1'):
          f2.write(line.replace('      25     1','      35     1'))
          counter += 1
      else:
          f2.write(line)
  print ('Found %d events in LHE, replaced 25 with 35' % counter)
  f1.close()
  f2.close()
  os.system('mv %s %s '%(infile, infile+'.old') )
  os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
H_Mass = 125.0
H_Width = 0.00407
A_Width = ( float(A_Mass) / 100. ) * 0.001 #1 MeV width for 100 GeV a
A_MassMin = float(A_Mass) - 100*float(A_Width)
A_MassMax = float(A_Mass) + 100*float(A_Width)
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:oneChannel = 1 1 100 1000023 1000022',
           '1000023:onMode = off',
           '1000023:oneChannel = 1 1 100 36 1000022',
           '1000023:m0 = '+str(N2_Mass),
                             '1000023:tau0 0', 
           
           '1000022:onMode = off',
           '1000022:m0 = '+str(N1_Mass),
                             '1000022:tau0 0', 

                             '36:onMode = off', # decay of the a
                             '36:onIfAny = '+axino_decay, # decay a->xx
                             '36:m0 = '+str(A_Mass), #scalar mass 
           '36:mWidth = '+str(A_Width), # narrow width
           '36:mMin = '+str(A_MassMin), # narrow width
           '36:mMax = '+str(A_MassMax), # narrow width
                             '36:tau0 0', #lifetime 

           '15:onMode = off',
           '15:onIfAny = 11 13',
                             ]
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
filters=[]

if '2L' in filteroptions:
    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")
    ElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    ElecMuTauFilter.MinPt = 2000.
    ElecMuTauFilter.MaxEta = 2.8
    ElecMuTauFilter.NLeptons = 2
    ElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
    filters.append("MultiElecMuTauFilter")


# MET filter, interpreted from jobOptions
if 'MET' in filteroptions:
    filtSeq += MissingEtFilter("MissingEtFilter")
    filtSeq += MissingEtFilter("MissingEtFilterUpperCut")

    m = re.search('MET(\d+)', filteroptions)
    lowercut = int(m.group(1))
    print ("Met lower cut = " + str(lowercut))
    print ("Met upper cut = " + str(100000))

    filtSeq.MissingEtFilter.METCut          = lowercut*GeV
    filtSeq.MissingEtFilterUpperCut.METCut  = 100000*GeV
    filters.append("MissingEtFilter and not MissingEtFilterUpperCut")


for i in filters:
    if filtSeq.Expression == "":
        filtSeq.Expression=i
    else:
        filtSeq.Expression = "(%s) and (%s)" % (filtSeq.Expression,i)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG++Pythia8 VBF H125 production, H->N1N2 ; N2->N1a ; a->%s, ma(%s), mN2(%s), mN1(%s)" % ( fs_string, A_Mass, N2_Mass, N1_Mass )
evgenConfig.keywords    = [ "BSM", "Higgs", "MSSM", "mH125" ]
evgenConfig.contact     = [ 'andrew.todd.aukerman.@cern.ch' , 'benjamin.taylor.carlson@cern.ch']
#evgenConfig.generators = [ "Powheg", "Pythia8", "EvtGen"]
