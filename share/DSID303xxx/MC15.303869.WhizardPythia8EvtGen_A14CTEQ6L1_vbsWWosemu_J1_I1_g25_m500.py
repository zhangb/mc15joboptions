############################################################################################################
# Whizard lhef showered with Pythia8
############################################################################################################  
evgenConfig.description = "Whizard+Pythia8+A14CTEQ6L1 production W+W-qq -> lvlvqq"
evgenConfig.keywords = ["BSM" , "exotic" , "VBS"  ,"WW", "oppositeSign" ]
evgenConfig.inputfilecheck = "vbsWWosemum500rho"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Guangyi Zhang <g.zhang@cern.ch>"]

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
evgenConfig.generators += ["Whizard"]
