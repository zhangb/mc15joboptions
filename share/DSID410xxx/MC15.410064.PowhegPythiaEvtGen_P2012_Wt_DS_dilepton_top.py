#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 Wt production (top), DS scheme, dilepton, with CT10 and Perugia2012 tune'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', '2lepton']
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------
if runArgs.trfSubstepName == 'generate' :
  include('PowhegControl/PowhegControl_Wt_DS_Common.py')

  PowhegConfig.topdecaymode = 11100 # leptonic W-from-top decays
  PowhegConfig.wdecaymode = 11100 # leptonic W decays
  PowhegConfig.PDF     = 10800
  PowhegConfig.mu_F    = 1.0
  PowhegConfig.mu_R    = 1.0
  PowhegConfig.generate()

  #--------------------------------------------------------------
  # Pythia6 (Perugia2012) showering
  #--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')
