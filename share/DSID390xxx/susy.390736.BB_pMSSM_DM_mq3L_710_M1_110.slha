#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12003348E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04164070E+01   # W+
        25     1.25675861E+02   # h
        35     4.00000052E+03   # H
        36     3.99999618E+03   # A
        37     4.00105612E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02303303E+03   # ~d_L
   2000001     4.02025078E+03   # ~d_R
   1000002     4.02237713E+03   # ~u_L
   2000002     4.02126254E+03   # ~u_R
   1000003     4.02303303E+03   # ~s_L
   2000003     4.02025078E+03   # ~s_R
   1000004     4.02237713E+03   # ~c_L
   2000004     4.02126254E+03   # ~c_R
   1000005     7.70149192E+02   # ~b_1
   2000005     4.02373130E+03   # ~b_2
   1000006     7.54500883E+02   # ~t_1
   2000006     2.07081765E+03   # ~t_2
   1000011     4.00407522E+03   # ~e_L
   2000011     4.00324189E+03   # ~e_R
   1000012     4.00295724E+03   # ~nu_eL
   1000013     4.00407522E+03   # ~mu_L
   2000013     4.00324189E+03   # ~mu_R
   1000014     4.00295724E+03   # ~nu_muL
   1000015     4.00549532E+03   # ~tau_1
   2000015     4.00692828E+03   # ~tau_2
   1000016     4.00466166E+03   # ~nu_tauL
   1000021     1.97470984E+03   # ~g
   1000022     9.33526139E+01   # ~chi_10
   1000023    -1.36950112E+02   # ~chi_20
   1000025     1.52791078E+02   # ~chi_30
   1000035     2.05351925E+03   # ~chi_40
   1000024     1.31892650E+02   # ~chi_1+
   1000037     2.05368438E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61977722E-01   # N_11
  1  2     1.37988638E-02   # N_12
  1  3     5.33797020E-01   # N_13
  1  4     3.66415452E-01   # N_14
  2  1    -1.35821020E-01   # N_21
  2  2     2.72445488E-02   # N_22
  2  3    -6.85335584E-01   # N_23
  2  4     7.14930432E-01   # N_24
  3  1     6.33199644E-01   # N_31
  3  2     2.38709953E-02   # N_32
  3  3     4.95354024E-01   # N_33
  3  4     5.94232931E-01   # N_34
  4  1    -9.00973595E-04   # N_41
  4  2     9.99248468E-01   # N_42
  4  3    -5.19117331E-04   # N_43
  4  4    -3.87481260E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.38449391E-04   # U_11
  1  2     9.99999727E-01   # U_12
  2  1    -9.99999727E-01   # U_21
  2  2     7.38449391E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48115019E-02   # V_11
  1  2    -9.98496720E-01   # V_12
  2  1    -9.98496720E-01   # V_21
  2  2    -5.48115019E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94859228E-01   # cos(theta_t)
  1  2    -1.01267549E-01   # sin(theta_t)
  2  1     1.01267549E-01   # -sin(theta_t)
  2  2     9.94859228E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999922E-01   # cos(theta_b)
  1  2    -3.94968345E-04   # sin(theta_b)
  2  1     3.94968345E-04   # -sin(theta_b)
  2  2     9.99999922E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03216619E-01   # cos(theta_tau)
  1  2     7.10975658E-01   # sin(theta_tau)
  2  1    -7.10975658E-01   # -sin(theta_tau)
  2  2    -7.03216619E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00323553E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20033479E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43749545E+02   # higgs               
         4     1.60980072E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20033479E+03  # The gauge couplings
     1     3.61855333E-01   # gprime(Q) DRbar
     2     6.37230716E-01   # g(Q) DRbar
     3     1.03202318E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20033479E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14656722E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20033479E+03  # The trilinear couplings
  1  1     4.19901290E-07   # A_d(Q) DRbar
  2  2     4.19940946E-07   # A_s(Q) DRbar
  3  3     7.51659118E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20033479E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.00150614E-08   # A_mu(Q) DRbar
  3  3     9.09355312E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20033479E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69608944E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20033479E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80183468E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20033479E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04155742E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20033479E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58682550E+07   # M^2_Hd              
        22    -8.96300174E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40880228E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.06192374E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43924462E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43924462E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56075538E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56075538E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.06493832E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.18356036E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.44277077E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.33484755E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03882133E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.23818630E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.61635338E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23488561E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.35561026E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.37824103E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.06614678E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.54649744E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.08658074E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.25659860E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.10690523E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.64253240E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.75043496E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.64292852E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.89641041E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66533161E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.79757953E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69985670E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41371257E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.86563061E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55635544E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.55633026E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15257068E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.69614185E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.88368780E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.19378008E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     7.96350221E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78749651E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48760009E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.05978579E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80347498E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80520990E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.24919679E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59816093E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52150091E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61177523E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.22679830E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02414388E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.22479333E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.48280517E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44459915E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78769588E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.18383064E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.44944960E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.04382460E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80984294E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.52828973E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62978466E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52370471E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54376100E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.42024241E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.67247422E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.80553321E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.47655046E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85506970E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78749651E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48760009E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.05978579E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80347498E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80520990E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.24919679E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59816093E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52150091E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61177523E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.22679830E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02414388E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.22479333E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.48280517E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44459915E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78769588E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.18383064E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.44944960E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.04382460E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80984294E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.52828973E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62978466E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52370471E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54376100E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.42024241E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.67247422E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.80553321E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.47655046E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85506970E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15744784E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97836596E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27155410E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.50900760E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77504515E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.57427150E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56349638E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08181048E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81049096E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84381609E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00512302E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.41574614E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15744784E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97836596E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27155410E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.50900760E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77504515E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.57427150E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56349638E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08181048E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81049096E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84381609E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00512302E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.41574614E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11526892E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27764594E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00354704E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60083962E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39141361E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.40205380E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78954074E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12368358E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11974830E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33274473E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35462828E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42102325E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22405582E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84892011E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15851191E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01940763E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.56336103E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75196246E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77791335E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06917684E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54115740E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15851191E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01940763E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.56336103E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75196246E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77791335E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06917684E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54115740E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49279943E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22235917E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.03305715E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20368348E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51464349E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.76417905E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01600377E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.32358333E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33721840E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33721840E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241385E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241385E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10073549E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.90445023E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.69421502E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.56389569E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.97089944E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.66619762E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.89041524E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.02981231E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.86156820E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.31069160E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.76043377E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18495418E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53658530E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18495418E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53658530E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37425353E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52066778E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52066778E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47706231E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03867438E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03867438E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03867406E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.84270683E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.84270683E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.84270683E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.84270683E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.47571287E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.47571287E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.47571287E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.47571287E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.24971044E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.24971044E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.45830201E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.79888359E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.59059548E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.04286370E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.52161442E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.04286370E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.52161442E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.09087576E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.48027655E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.48027655E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.46566945E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.99421599E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.99421599E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.99421105E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.31107435E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.18461266E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.31107435E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.18461266E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.23577130E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.87581163E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.87581163E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.74481918E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.74872056E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.74872056E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.74872067E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.61341453E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.61341453E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.61341453E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.61341453E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.87109045E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.87109045E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.87109045E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.87109045E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.77025662E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.77025662E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.90338726E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.07900002E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.65704601E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.40426862E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.80108382E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.80108382E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.88353706E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.34587461E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.70837841E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.81068187E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.81068187E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.81861868E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.81861868E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07467085E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93728985E-01    2           5        -5   # BR(h -> b       bb     )
     6.40275646E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26656537E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80571976E-04    2           3        -3   # BR(h -> s       sb     )
     2.08672288E-02    2           4        -4   # BR(h -> c       cb     )
     6.82118340E-02    2          21        21   # BR(h -> g       g      )
     2.36322089E-03    2          22        22   # BR(h -> gam     gam    )
     1.62928503E-03    2          22        23   # BR(h -> Z       gam    )
     2.20528894E-01    2          24       -24   # BR(h -> W+      W-     )
     2.79357589E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43779163E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37938536E-01    2           5        -5   # BR(H -> b       bb     )
     6.09731523E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15586298E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55001472E-04    2           3        -3   # BR(H -> s       sb     )
     7.15390195E-08    2           4        -4   # BR(H -> c       cb     )
     7.16969908E-03    2           6        -6   # BR(H -> t       tb     )
     1.13635290E-06    2          21        21   # BR(H -> g       g      )
     3.22889470E-10    2          22        22   # BR(H -> gam     gam    )
     1.83861250E-09    2          23        22   # BR(H -> Z       gam    )
     2.10757081E-06    2          24       -24   # BR(H -> W+      W-     )
     1.05305528E-06    2          23        23   # BR(H -> Z       Z      )
     7.77164234E-06    2          25        25   # BR(H -> h       h      )
    -1.31726975E-24    2          36        36   # BR(H -> A       A      )
     7.68877907E-20    2          23        36   # BR(H -> Z       A      )
     1.86155966E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67346459E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67346459E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35713861E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60184406E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68544998E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48306712E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.96424672E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.95022130E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04844379E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.43596093E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.39563597E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.38118098E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.82149245E-09    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.82149245E-09    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.35617036E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43778270E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37965114E-01    2           5        -5   # BR(A -> b       bb     )
     6.09736857E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15588015E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55045050E-04    2           3        -3   # BR(A -> s       sb     )
     7.19854466E-08    2           4        -4   # BR(A -> c       cb     )
     7.18184009E-03    2           6        -6   # BR(A -> t       tb     )
     1.47568671E-05    2          21        21   # BR(A -> g       g      )
     4.06676040E-08    2          22        22   # BR(A -> gam     gam    )
     1.62195306E-08    2          23        22   # BR(A -> Z       gam    )
     2.10314199E-06    2          23        25   # BR(A -> Z       h      )
     1.86467834E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67348893E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67348893E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93245286E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.22172712E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33311089E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97117468E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.57491328E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.63307793E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30924711E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.31848284E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.82745799E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.66749429E-09    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.66749429E-09    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42499154E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.38260765E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11336461E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16153595E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.44486591E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22454947E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51929204E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.43252191E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.11051557E-06    2          24        25   # BR(H+ -> W+      h      )
     3.24519775E-14    2          24        36   # BR(H+ -> W+      A      )
     4.84737673E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.27925805E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07491520E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67962606E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.61622876E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58085400E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.25150573E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.36475113E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.75743287E-07    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
