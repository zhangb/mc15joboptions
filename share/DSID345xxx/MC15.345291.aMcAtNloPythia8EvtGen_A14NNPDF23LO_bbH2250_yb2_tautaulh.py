evgenConfig.description = 'MadGraph5_aMC@NLO+Pythia8 bbH production'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'MSSM', 'bbHiggs', '2tau' ]
evgenConfig.contact     = [ 'lorenz.hauswald@cern.ch', 'niklaos.rompotis@cern.ch', 'samina.jabbar@cern.ch' ]

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbHtautau.py")
