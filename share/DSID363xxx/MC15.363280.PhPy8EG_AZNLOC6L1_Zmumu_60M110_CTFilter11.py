#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
#PowhegConfig.vdecaymode = 2   # mumu
if hasattr(PowhegConfig, "vdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.vdecaymode = 2  # enu
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "z > mu+ mu-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 25.0 # increase number of generated events by 20% and filter efficiency
PowhegConfig.running_width = 1
PowhegConfig.mass_low = 60.
PowhegConfig.mass_high = 110.

PowhegConfig.generate()
#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->mumu production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2muon' ]
evgenConfig.minevents = 1000

#--------------------------------------------------------------
# FILTER
#--------------------------------------------------------------
include('MC15JobOptions/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2
#ChargedTrackFilter
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
chtrkfilter = ChargedTracksFilter("ChargedTracksFilter")
chtrkfilter.NTracks=-1
chtrkfilter.NTracksMax=11
chtrkfilter.Ptcut=500
chtrkfilter.Etacut=2.5

filtSeq += chtrkfilter


