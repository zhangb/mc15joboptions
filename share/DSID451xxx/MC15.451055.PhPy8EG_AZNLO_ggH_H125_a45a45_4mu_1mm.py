#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

#evgenConfig.inputfilecheck = "TXT"

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )


# a->mm 
nProcess = 0
ma = 45.
ctau = 1.
adecay = 13

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']

#--------------------------------------------------------------
# Higgs->2mu at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',

                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = on',
                            '35:onMode = off',
                            '35:onIfMatch = 36 36', # h->aa

                            '36:onMode = off', # decay of the a
                            '36:onIfAny = %d' % adecay, # decay a->XX
                            '36:m0 = %.1f' % ma, #scalar mass
                            '36:mMin = 0',
                            '36:tau0 = %.1f' % ctau, #nominal proper lifetime (in mm/c)
                            ]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',
                            'ParticleDecays:limitTau0 = off'
                           ]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+jet->aa->4l+vmu+mu-mu+mu- production"
evgenConfig.process = "ggH, H->2a->4mu" 
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.contact     = [ 'roger.caminal.armadans@cern.ch' ]
evgenConfig.minevents = 10000
evgenConfig.inputFilesPerJob = 5 #Specify the number of LHEs files needed 

#POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->4mu mh=125 GeV