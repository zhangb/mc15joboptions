#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13016378E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.03900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178779E+01   # W+
        25     1.25988608E+02   # h
        35     4.00000746E+03   # H
        36     3.99999677E+03   # A
        37     4.00105288E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02594260E+03   # ~d_L
   2000001     4.02258519E+03   # ~d_R
   1000002     4.02529176E+03   # ~u_L
   2000002     4.02337131E+03   # ~u_R
   1000003     4.02594260E+03   # ~s_L
   2000003     4.02258519E+03   # ~s_R
   1000004     4.02529176E+03   # ~c_L
   2000004     4.02337131E+03   # ~c_R
   1000005     9.68592321E+02   # ~b_1
   2000005     4.02542408E+03   # ~b_2
   1000006     9.46315954E+02   # ~t_1
   2000006     1.90088830E+03   # ~t_2
   1000011     4.00457350E+03   # ~e_L
   2000011     4.00343401E+03   # ~e_R
   1000012     4.00346125E+03   # ~nu_eL
   1000013     4.00457350E+03   # ~mu_L
   2000013     4.00343401E+03   # ~mu_R
   1000014     4.00346125E+03   # ~nu_muL
   1000015     4.00480237E+03   # ~tau_1
   2000015     4.00750426E+03   # ~tau_2
   1000016     4.00489549E+03   # ~nu_tauL
   1000021     1.97813215E+03   # ~g
   1000022     2.50575670E+02   # ~chi_10
   1000023    -3.14718408E+02   # ~chi_20
   1000025     3.24927412E+02   # ~chi_30
   1000035     2.05215033E+03   # ~chi_40
   1000024     3.12002789E+02   # ~chi_1+
   1000037     2.05231460E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.83434291E-01   # N_11
  1  2    -1.20347118E-02   # N_12
  1  3    -3.67020106E-01   # N_13
  1  4    -2.91024502E-01   # N_14
  2  1    -5.77744466E-02   # N_21
  2  2     2.49673466E-02   # N_22
  2  3    -7.02217184E-01   # N_23
  2  4     7.09175417E-01   # N_24
  3  1     4.64978488E-01   # N_31
  3  2     2.80868283E-02   # N_32
  3  3     6.10058268E-01   # N_33
  3  4     6.40964153E-01   # N_34
  4  1    -9.86187275E-04   # N_41
  4  2     9.99221160E-01   # N_42
  4  3    -4.02221590E-03   # N_43
  4  4    -3.92418376E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.69368117E-03   # U_11
  1  2     9.99983791E-01   # U_12
  2  1    -9.99983791E-01   # U_21
  2  2     5.69368117E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.55081587E-02   # V_11
  1  2    -9.98458234E-01   # V_12
  2  1    -9.98458234E-01   # V_21
  2  2    -5.55081587E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89926904E-01   # cos(theta_t)
  1  2    -1.41579394E-01   # sin(theta_t)
  2  1     1.41579394E-01   # -sin(theta_t)
  2  2     9.89926904E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999522E-01   # cos(theta_b)
  1  2    -9.77752408E-04   # sin(theta_b)
  2  1     9.77752408E-04   # -sin(theta_b)
  2  2     9.99999522E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05660750E-01   # cos(theta_tau)
  1  2     7.08549861E-01   # sin(theta_tau)
  2  1    -7.08549861E-01   # -sin(theta_tau)
  2  2    -7.05660750E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00290030E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30163784E+03  # DRbar Higgs Parameters
         1    -3.03900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43585609E+02   # higgs               
         4     1.60960856E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30163784E+03  # The gauge couplings
     1     3.61966114E-01   # gprime(Q) DRbar
     2     6.36273502E-01   # g(Q) DRbar
     3     1.03008587E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30163784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37071807E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30163784E+03  # The trilinear couplings
  1  1     5.03113781E-07   # A_d(Q) DRbar
  2  2     5.03160610E-07   # A_s(Q) DRbar
  3  3     9.01387096E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30163784E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09992465E-07   # A_mu(Q) DRbar
  3  3     1.11121302E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30163784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68292140E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30163784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83754866E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30163784E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03589202E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30163784E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57981603E+07   # M^2_Hd              
        22    -5.25368747E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40450866E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.95534025E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40058442E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40058442E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59941558E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59941558E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.15824382E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.63505593E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.28171551E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.87700865E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07777024E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18396059E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.87266583E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11847393E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.23714126E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.18756282E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.63020601E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.24191384E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.41086270E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.20746129E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.39264367E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.94194235E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.73188706E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.89335269E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66208969E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53927941E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80550083E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64784236E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.04938383E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64196573E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.19384752E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13205185E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.81277672E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.29761908E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.16951778E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.29100038E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78200579E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.10321529E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.70063701E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.16991359E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81850399E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33902725E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62482563E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51742666E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60521191E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.33430389E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.84581808E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.19466497E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.99553617E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44525700E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78220347E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.80491771E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11789574E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.12513437E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82345078E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.61771558E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65693958E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51961258E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53774739E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13088652E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.81601513E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.11705844E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.81372038E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85525908E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78200579E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.10321529E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.70063701E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.16991359E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81850399E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33902725E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62482563E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51742666E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60521191E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.33430389E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.84581808E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.19466497E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.99553617E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44525700E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78220347E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.80491771E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11789574E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.12513437E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82345078E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.61771558E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65693958E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51961258E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53774739E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13088652E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.81601513E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.11705844E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.81372038E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85525908E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14953072E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.22246074E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.15595915E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.32640333E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77671971E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.27963819E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56753566E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06828084E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.81372701E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.32664385E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.15300121E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.33408788E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14953072E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.22246074E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.15595915E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.32640333E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77671971E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.27963819E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56753566E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06828084E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.81372701E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.32664385E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.15300121E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.33408788E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10185929E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.81778922E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35411758E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.95817364E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39899595E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46884718E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80510099E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09921525E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.92455319E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25104022E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.66416553E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42434063E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05940041E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85589659E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15059083E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.34453794E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.69051315E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.82295682E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78015296E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11518286E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54495646E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15059083E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.34453794E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.69051315E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.82295682E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78015296E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11518286E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54495646E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47930477E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.21795319E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53136075E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.55719267E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51968027E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65882146E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02545151E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.73740712E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33475459E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33475459E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11159708E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11159708E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10729666E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.13715977E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.54982205E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.33423065E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.37869280E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.80068964E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11970728E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.61691729E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.45716096E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.71504207E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.43468066E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.57556599E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17599837E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52543512E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17599837E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52543512E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44756785E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49784204E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49784204E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47428271E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99326733E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99326733E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99326712E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.35956163E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.35956163E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.35956163E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.35956163E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86521416E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86521416E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86521416E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86521416E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.03167063E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.03167063E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.86452425E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.07921011E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.37304516E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.10224322E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42657106E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.10224322E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42657106E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.38018448E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.24551906E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.24551906E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.23612884E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.55159514E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.55159514E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.55158803E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.22543354E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.77840523E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.22543354E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.77840523E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     8.38185238E-06    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.55447269E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.55447269E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.29200901E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.10730385E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.10730385E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.10730387E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.73869676E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.73869676E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.73869676E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.73869676E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.91288228E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.91288228E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.91288228E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.91288228E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.74446991E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.74446991E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.13588884E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.10587546E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.70975329E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.58015724E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.60422323E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.60422323E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.30236311E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.06325806E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.00146614E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.73230305E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.73230305E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.73250170E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.73250170E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09258997E-03   # h decays
#          BR         NDA      ID1       ID2
     5.86322336E-01    2           5        -5   # BR(h -> b       bb     )
     6.38976846E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26195422E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79363317E-04    2           3        -3   # BR(h -> s       sb     )
     2.08174817E-02    2           4        -4   # BR(h -> c       cb     )
     6.82452608E-02    2          21        21   # BR(h -> g       g      )
     2.38141558E-03    2          22        22   # BR(h -> gam     gam    )
     1.66733268E-03    2          22        23   # BR(h -> Z       gam    )
     2.27059284E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89036452E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49687697E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52986344E-01    2           5        -5   # BR(H -> b       bb     )
     6.03178835E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13269426E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52260956E-04    2           3        -3   # BR(H -> s       sb     )
     7.07606862E-08    2           4        -4   # BR(H -> c       cb     )
     7.09169401E-03    2           6        -6   # BR(H -> t       tb     )
     6.73701727E-07    2          21        21   # BR(H -> g       g      )
     9.27384884E-10    2          22        22   # BR(H -> gam     gam    )
     1.82188581E-09    2          23        22   # BR(H -> Z       gam    )
     1.90020754E-06    2          24       -24   # BR(H -> W+      W-     )
     9.49445566E-07    2          23        23   # BR(H -> Z       Z      )
     7.37110774E-06    2          25        25   # BR(H -> h       h      )
    -2.27728343E-25    2          36        36   # BR(H -> A       A      )
     1.95964478E-20    2          23        36   # BR(H -> Z       A      )
     1.84843451E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61605668E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61605668E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.05500856E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.22851597E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.18653905E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.77269403E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53631462E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.15548648E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.01690496E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.07204272E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.98292519E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.53102995E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.34679098E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.48455054E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.48455054E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.30514481E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49640768E+01   # A decays
#          BR         NDA      ID1       ID2
     3.53042200E-01    2           5        -5   # BR(A -> b       bb     )
     6.03233461E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13288573E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52324757E-04    2           3        -3   # BR(A -> s       sb     )
     7.12176552E-08    2           4        -4   # BR(A -> c       cb     )
     7.10523912E-03    2           6        -6   # BR(A -> t       tb     )
     1.45994687E-05    2          21        21   # BR(A -> g       g      )
     5.65350961E-08    2          22        22   # BR(A -> gam     gam    )
     1.60506423E-08    2          23        22   # BR(A -> Z       gam    )
     1.89633631E-06    2          23        25   # BR(A -> Z       h      )
     1.85626811E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61618873E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61618873E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.78142270E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.03302165E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.87491195E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.40589775E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.24616046E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.21394524E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.12607360E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.91057551E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.08171138E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.69357412E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.69357412E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50123389E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.65690182E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02863377E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13157721E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.62041407E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20757741E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48437507E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.60285048E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.89683742E-06    2          24        25   # BR(H+ -> W+      h      )
     3.14281448E-14    2          24        36   # BR(H+ -> W+      A      )
     6.48497888E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.87445195E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.21772397E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61690754E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.37224875E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59775543E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.20749009E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.35975101E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.18006475E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
