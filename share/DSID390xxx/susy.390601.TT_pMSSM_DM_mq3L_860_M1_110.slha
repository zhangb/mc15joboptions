#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13060542E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04160648E+01   # W+
        25     1.25862343E+02   # h
        35     4.00000094E+03   # H
        36     3.99999652E+03   # A
        37     4.00106892E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02606639E+03   # ~d_L
   2000001     4.02266460E+03   # ~d_R
   1000002     4.02541248E+03   # ~u_L
   2000002     4.02352112E+03   # ~u_R
   1000003     4.02606639E+03   # ~s_L
   2000003     4.02266460E+03   # ~s_R
   1000004     4.02541248E+03   # ~c_L
   2000004     4.02352112E+03   # ~c_R
   1000005     9.22089749E+02   # ~b_1
   2000005     4.02541790E+03   # ~b_2
   1000006     9.04471779E+02   # ~t_1
   2000006     2.01881141E+03   # ~t_2
   1000011     4.00464939E+03   # ~e_L
   2000011     4.00340512E+03   # ~e_R
   1000012     4.00353276E+03   # ~nu_eL
   1000013     4.00464939E+03   # ~mu_L
   2000013     4.00340512E+03   # ~mu_R
   1000014     4.00353276E+03   # ~nu_muL
   1000015     4.00560841E+03   # ~tau_1
   2000015     4.00675330E+03   # ~tau_2
   1000016     4.00496925E+03   # ~nu_tauL
   1000021     1.97938345E+03   # ~g
   1000022     9.31842673E+01   # ~chi_10
   1000023    -1.37088260E+02   # ~chi_20
   1000025     1.52901960E+02   # ~chi_30
   1000035     2.05226245E+03   # ~chi_40
   1000024     1.31980439E+02   # ~chi_1+
   1000037     2.05242778E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.62005186E-01   # N_11
  1  2     1.37874966E-02   # N_12
  1  3     5.33762904E-01   # N_13
  1  4     3.66408465E-01   # N_14
  2  1    -1.35803837E-01   # N_21
  2  2     2.72223042E-02   # N_22
  2  3    -6.85346785E-01   # N_23
  2  4     7.14923806E-01   # N_24
  3  1     6.33170280E-01   # N_31
  3  2     2.38521785E-02   # N_32
  3  3     4.95375290E-01   # N_33
  3  4     5.94247248E-01   # N_34
  4  1    -9.00128591E-04   # N_41
  4  2     9.99249681E-01   # N_42
  4  3    -5.18704568E-04   # N_43
  4  4    -3.87168702E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.37854725E-04   # U_11
  1  2     9.99999728E-01   # U_12
  2  1    -9.99999728E-01   # U_21
  2  2     7.37854725E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47672295E-02   # V_11
  1  2    -9.98499149E-01   # V_12
  2  1    -9.98499149E-01   # V_21
  2  2    -5.47672295E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92981825E-01   # cos(theta_t)
  1  2    -1.18267050E-01   # sin(theta_t)
  2  1     1.18267050E-01   # -sin(theta_t)
  2  2     9.92981825E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999920E-01   # cos(theta_b)
  1  2    -3.99999992E-04   # sin(theta_b)
  2  1     3.99999992E-04   # -sin(theta_b)
  2  2     9.99999920E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03377676E-01   # cos(theta_tau)
  1  2     7.10816323E-01   # sin(theta_tau)
  2  1    -7.10816323E-01   # -sin(theta_tau)
  2  2    -7.03377676E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00326902E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30605418E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43524648E+02   # higgs               
         4     1.60733414E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30605418E+03  # The gauge couplings
     1     3.62141010E-01   # gprime(Q) DRbar
     2     6.37302472E-01   # g(Q) DRbar
     3     1.03004343E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30605418E+03  # The trilinear couplings
  1  1     1.38194291E-06   # A_u(Q) DRbar
  2  2     1.38195602E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30605418E+03  # The trilinear couplings
  1  1     5.05986489E-07   # A_d(Q) DRbar
  2  2     5.06033774E-07   # A_s(Q) DRbar
  3  3     9.06173847E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30605418E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08060813E-07   # A_mu(Q) DRbar
  3  3     1.09170839E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30605418E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67931215E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30605418E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.79654663E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30605418E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04255572E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30605418E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58739062E+07   # M^2_Hd              
        22     1.21204216E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40913460E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.23056449E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42193679E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42193679E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57806321E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57806321E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.34031481E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.13395506E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.44694992E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.40038889E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.01870613E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.23116293E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.58822172E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.21669158E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.11152430E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.33366413E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.57018358E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.13431600E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.27517011E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.37975467E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.50169381E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.61666163E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.58305663E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.92985879E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66301396E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.80485289E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.70759501E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41960370E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.89746410E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.57257256E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.61707770E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14887115E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.65477367E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.85386981E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.13305034E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.51215703E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78662018E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.49257648E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96643259E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80776181E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82201886E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.24903418E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.63176060E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51636767E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60964458E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23859836E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02754850E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.23254911E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49110210E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44260952E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78681912E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.18983602E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45258033E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.07242910E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82665696E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.52803664E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66342745E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51856724E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54161284E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.45195771E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.68165162E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.82640888E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.49925897E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85453462E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78662018E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.49257648E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96643259E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80776181E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82201886E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.24903418E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.63176060E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51636767E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60964458E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23859836E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02754850E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.23254911E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49110210E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44260952E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78681912E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.18983602E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45258033E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.07242910E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82665696E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.52803664E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66342745E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51856724E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54161284E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.45195771E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.68165162E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.82640888E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.49925897E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85453462E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.16215511E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.98204301E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27342008E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.50870201E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77493169E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.55904865E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56325405E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08518538E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81092894E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84333989E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00473266E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.41157313E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.16215511E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.98204301E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27342008E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.50870201E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77493169E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.55904865E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56325405E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08518538E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81092894E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84333989E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00473266E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.41157313E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11956767E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27752195E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00255393E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60047929E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39175902E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.39758643E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79022570E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12728353E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.12144521E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33158517E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35551564E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42016182E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22533496E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84718531E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16321198E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01962840E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.55927240E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75392199E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77781080E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06078995E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54096798E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.16321198E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01962840E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.55927240E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75392199E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77781080E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06078995E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54096798E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49738596E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22534875E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.02989847E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20601498E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51457860E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.76098105E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01588794E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.43459484E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33716700E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33716700E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11239671E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11239671E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10087257E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.35998313E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.56967952E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.17815940E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.41732745E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.65658298E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.82952296E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.56549513E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.52289939E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.55531203E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.46428734E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.74001628E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18460929E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53630085E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18460929E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53630085E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37530913E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52093214E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52093214E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47775104E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03954273E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03954273E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03954245E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.88434977E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.88434977E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.88434977E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.88434977E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.61452298E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.61452298E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.61452298E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.61452298E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.38926114E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.38926114E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.59937586E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.01005902E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.39930242E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.10318606E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.60036511E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.10318606E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.60036511E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.17522740E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.49856473E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.49856473E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.48394989E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.03128972E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.03128972E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.03128552E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.17177180E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.00480435E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.17177180E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.00480435E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.14895907E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.83507989E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.83507989E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.70650847E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.66749615E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.66749615E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.66749624E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.53838539E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.53838539E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.53838539E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.53838539E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.84608120E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.84608120E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.84608120E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.84608120E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.74634036E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.74634036E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.35893115E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.97227290E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.01669251E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.64089344E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.46772730E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.46772730E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.67986217E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.68331962E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.98294073E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.74438258E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.74438258E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.74939170E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.74939170E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.10111952E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90797986E-01    2           5        -5   # BR(h -> b       bb     )
     6.37101143E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25531969E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78049599E-04    2           3        -3   # BR(h -> s       sb     )
     2.07574145E-02    2           4        -4   # BR(h -> c       cb     )
     6.80436437E-02    2          21        21   # BR(h -> g       g      )
     2.36249612E-03    2          22        22   # BR(h -> gam     gam    )
     1.64548276E-03    2          22        23   # BR(h -> Z       gam    )
     2.23579327E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83999547E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43594039E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37270177E-01    2           5        -5   # BR(H -> b       bb     )
     6.09939215E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15659733E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55088329E-04    2           3        -3   # BR(H -> s       sb     )
     7.15643464E-08    2           4        -4   # BR(H -> c       cb     )
     7.17223737E-03    2           6        -6   # BR(H -> t       tb     )
     1.20793568E-06    2          21        21   # BR(H -> g       g      )
     4.12523022E-10    2          22        22   # BR(H -> gam     gam    )
     1.83884993E-09    2          23        22   # BR(H -> Z       gam    )
     2.12742579E-06    2          24       -24   # BR(H -> W+      W-     )
     1.06297587E-06    2          23        23   # BR(H -> Z       Z      )
     7.85028224E-06    2          25        25   # BR(H -> h       h      )
    -1.34956719E-25    2          36        36   # BR(H -> A       A      )
     1.28827911E-20    2          23        36   # BR(H -> Z       A      )
     1.85915470E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67549519E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67549519E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35805267E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60119575E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68623352E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48399118E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.97264925E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.95612925E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04905478E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.44516187E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.40138311E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.91762087E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.28476590E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.28476590E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.30453987E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43589887E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37298763E-01    2           5        -5   # BR(A -> b       bb     )
     6.09948216E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15662746E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55133456E-04    2           3        -3   # BR(A -> s       sb     )
     7.20103988E-08    2           4        -4   # BR(A -> c       cb     )
     7.18432952E-03    2           6        -6   # BR(A -> t       tb     )
     1.47619806E-05    2          21        21   # BR(A -> g       g      )
     4.06652593E-08    2          22        22   # BR(A -> gam     gam    )
     1.62241847E-08    2          23        22   # BR(A -> Z       gam    )
     2.12294908E-06    2          23        25   # BR(A -> Z       h      )
     1.86229133E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67552956E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67552956E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93322644E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.22095943E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33375505E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97233387E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.58022866E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.63772126E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30996960E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.32882939E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.83238248E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.45476242E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.45476242E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42264854E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.37085728E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11602561E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16247682E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43734568E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22508199E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52038762E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42521404E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.13059424E-06    2          24        25   # BR(H+ -> W+      h      )
     3.44189131E-14    2          24        36   # BR(H+ -> W+      A      )
     4.85027098E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.28601703E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07600274E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.68182717E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.62771824E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58296465E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.26298746E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.10632985E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     6.50801413E-06    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
