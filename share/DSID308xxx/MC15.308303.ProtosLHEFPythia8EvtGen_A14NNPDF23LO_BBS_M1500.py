evgenConfig.description = "Protos TT pair production (B singlet, M = 1500 GeV)"
evgenConfig.keywords = ["BSM","exotic","top","quark"]
evgenConfig.contact  = ["Steffen Henkelmann <steffen.henkelmann@cern.ch>"]
evgenConfig.inputfilecheck = "protoslhef"
evgenConfig.process = "pp>BB"

include("MC15JobOptions/ProtosLHEF_Common.py")
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
