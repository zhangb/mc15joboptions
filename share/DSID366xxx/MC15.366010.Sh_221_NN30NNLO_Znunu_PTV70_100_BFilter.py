evgenConfig.description = "Sherpa Z/gamma* -> nu nu + 0,1,2j@NLO + 3,4j@LO with 70 GeV < pTV < 100 GeV with b-jet filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 500

if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   evgenConfig.findJets = True
   from JetRec.JetRecFlags import jetFlags
   jetFlags.truthFlavorTags = [ ]
   jetFlags.useTracks = False

   from JetRec.JetRecStandard import jtm
   jtm.addJetFinder("AntiKt4TruthJetsNoWZ", "AntiKt", 0.4, "truthwz", modifiersin="none", ptmin=10000.)
 
   include("MC15JobOptions/BHadronFilter.py")
   HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
   HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
   HeavyFlavorBHadronFilter.RequireTruthJet = True
   HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
   HeavyFlavorBHadronFilter.JetEtaMax = 2.9
   HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJetsNoWZ"
   HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
   filtSeq += HeavyFlavorBHadronFilter

