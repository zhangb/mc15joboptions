#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.89540159E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.86900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04182429E+01   # W+
        25     1.25371042E+02   # h
        35     4.00000204E+03   # H
        36     3.99999517E+03   # A
        37     4.00100909E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01235631E+03   # ~d_L
   2000001     4.01175166E+03   # ~d_R
   1000002     4.01169660E+03   # ~u_L
   2000002     4.01317010E+03   # ~u_R
   1000003     4.01235631E+03   # ~s_L
   2000003     4.01175166E+03   # ~s_R
   1000004     4.01169660E+03   # ~c_L
   2000004     4.01317010E+03   # ~c_R
   1000005     4.28673510E+02   # ~b_1
   2000005     4.01781490E+03   # ~b_2
   1000006     4.09485415E+02   # ~t_1
   2000006     2.02363813E+03   # ~t_2
   1000011     4.00201317E+03   # ~e_L
   2000011     4.00279322E+03   # ~e_R
   1000012     4.00089430E+03   # ~nu_eL
   1000013     4.00201317E+03   # ~mu_L
   2000013     4.00279322E+03   # ~mu_R
   1000014     4.00089430E+03   # ~nu_muL
   1000015     4.00443018E+03   # ~tau_1
   2000015     4.00824646E+03   # ~tau_2
   1000016     4.00352317E+03   # ~nu_tauL
   1000021     1.95754497E+03   # ~g
   1000022     1.47120376E+02   # ~chi_10
   1000023    -1.96242496E+02   # ~chi_20
   1000025     2.09955434E+02   # ~chi_30
   1000035     2.05802203E+03   # ~chi_40
   1000024     1.92666617E+02   # ~chi_1+
   1000037     2.05818637E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.19578777E-01   # N_11
  1  2    -1.33313338E-02   # N_12
  1  3    -4.59839251E-01   # N_13
  1  4    -3.41556389E-01   # N_14
  2  1    -9.35470014E-02   # N_21
  2  2     2.64649883E-02   # N_22
  2  3    -6.95958782E-01   # N_23
  2  4     7.11470264E-01   # N_24
  3  1     5.65277563E-01   # N_31
  3  2     2.53507846E-02   # N_32
  3  3     5.51530928E-01   # N_33
  3  4     6.12888448E-01   # N_34
  4  1    -9.29137979E-04   # N_41
  4  2     9.99239320E-01   # N_42
  4  3    -1.69476042E-03   # N_43
  4  4    -3.89492854E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.40150026E-03   # U_11
  1  2     9.99997116E-01   # U_12
  2  1    -9.99997116E-01   # U_21
  2  2     2.40150026E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50957081E-02   # V_11
  1  2    -9.98481078E-01   # V_12
  2  1    -9.98481078E-01   # V_21
  2  2    -5.50957081E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96121041E-01   # cos(theta_t)
  1  2    -8.79935888E-02   # sin(theta_t)
  2  1     8.79935888E-02   # -sin(theta_t)
  2  2     9.96121041E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999833E-01   # cos(theta_b)
  1  2    -5.77927307E-04   # sin(theta_b)
  2  1     5.77927307E-04   # -sin(theta_b)
  2  2     9.99999833E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04205665E-01   # cos(theta_tau)
  1  2     7.09996043E-01   # sin(theta_tau)
  2  1    -7.09996043E-01   # -sin(theta_tau)
  2  2    -7.04205665E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00300836E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.95401591E+02  # DRbar Higgs Parameters
         1    -1.86900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44543887E+02   # higgs               
         4     1.62168533E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.95401591E+02  # The gauge couplings
     1     3.60807062E-01   # gprime(Q) DRbar
     2     6.36407814E-01   # g(Q) DRbar
     3     1.03874584E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.95401591E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.67597602E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.95401591E+02  # The trilinear couplings
  1  1     2.07954090E-07   # A_d(Q) DRbar
  2  2     2.07974329E-07   # A_s(Q) DRbar
  3  3     3.72241378E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.95401591E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.55342627E-08   # A_mu(Q) DRbar
  3  3     4.59914369E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.95401591E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75387662E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.95401591E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83455338E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.95401591E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03582151E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.95401591E+02  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58308043E+07   # M^2_Hd              
        22    -6.79427626E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40508571E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.40439396E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45335500E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45335500E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54664500E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54664500E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.01685535E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.87922886E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.71317387E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.44902314E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.95857412E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20907188E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.43080317E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20736860E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.64306366E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.36700842E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.48235377E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.10663496E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.32924758E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.72570728E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.59780675E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.59022481E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.58050882E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.22314596E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66271952E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64853375E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.73242262E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.50626653E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.30413423E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.54225155E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.48323927E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15537546E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.89233053E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.76677079E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.05519154E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.88220947E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77860534E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.74855019E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.72404391E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.52071627E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76936181E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.28865777E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.52652193E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53236306E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60747437E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.70743489E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.82088331E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.75918946E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.61043687E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44851642E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77880001E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.46943693E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69735321E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.77853715E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77409697E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.00484961E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.55823963E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53458603E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53977187E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.67261623E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.25775620E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.58967853E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.80720005E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85611923E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77860534E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.74855019E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.72404391E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.52071627E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76936181E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.28865777E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.52652193E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53236306E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60747437E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.70743489E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.82088331E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.75918946E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.61043687E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44851642E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77880001E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.46943693E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69735321E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.77853715E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77409697E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.00484961E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.55823963E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53458603E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53977187E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.67261623E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.25775620E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.58967853E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.80720005E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85611923E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13494894E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04476833E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.61358996E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.11720746E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77539637E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.90471594E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56444192E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06586490E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.72324110E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.74059380E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.18934827E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.68897974E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13494894E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04476833E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.61358996E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.11720746E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77539637E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.90471594E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56444192E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06586490E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.72324110E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.74059380E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.18934827E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.68897974E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09493617E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.53014061E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.15629190E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.32201648E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39402044E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43298438E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79489484E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10166573E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.46536438E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.74101827E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.07068619E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42160677E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.18032472E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85020837E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13603862E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17112100E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23294142E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.45222104E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77839336E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10595942E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54187453E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13603862E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17112100E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23294142E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.45222104E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77839336E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10595942E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54187453E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46914778E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05936881E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92445173E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.02739046E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51563597E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75246099E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01776556E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.84624813E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33607409E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33607409E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11203536E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11203536E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10378109E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.79480177E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.86167000E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.00358683E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.75643392E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.06446116E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.23987943E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.95714133E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.63275990E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.92471191E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.34245149E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.24126680E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18131873E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53149793E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18131873E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53149793E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41012230E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50700184E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50700184E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47209020E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01018034E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01018034E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01017989E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.04180753E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.04180753E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.04180753E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.04180753E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.01393757E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.01393757E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01393757E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01393757E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.01467276E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.01467276E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.58800279E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.11960813E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.58459935E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.67268678E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.92098401E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.67268678E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.92098401E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.35672670E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.25116226E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.25116226E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.23375974E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.55105107E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.55105107E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.55104005E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.65797344E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.74261590E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.65797344E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.74261590E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.23115488E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.08622550E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.08622550E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.85519173E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.17065908E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.17065908E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.17065914E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.95429682E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.95429682E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.95429682E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.95429682E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.31808219E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.31808219E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.31808219E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.31808219E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.25137682E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.25137682E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.79381763E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.34939576E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.33172556E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.09660587E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.92103085E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.92103085E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.20851593E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.70262978E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.58584547E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89960574E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89960574E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.90966128E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.90966128E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02134996E-03   # h decays
#          BR         NDA      ID1       ID2
     5.97537931E-01    2           5        -5   # BR(h -> b       bb     )
     6.47129306E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29084055E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85949131E-04    2           3        -3   # BR(h -> s       sb     )
     2.11026314E-02    2           4        -4   # BR(h -> c       cb     )
     6.85430834E-02    2          21        21   # BR(h -> g       g      )
     2.37602695E-03    2          22        22   # BR(h -> gam     gam    )
     1.60713637E-03    2          22        23   # BR(h -> Z       gam    )
     2.16146246E-01    2          24       -24   # BR(h -> W+      W-     )
     2.72589808E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48049416E+01   # H decays
#          BR         NDA      ID1       ID2
     3.47293078E-01    2           5        -5   # BR(H -> b       bb     )
     6.04981028E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13906638E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53014710E-04    2           3        -3   # BR(H -> s       sb     )
     7.09751901E-08    2           4        -4   # BR(H -> c       cb     )
     7.11319166E-03    2           6        -6   # BR(H -> t       tb     )
     9.27597423E-07    2          21        21   # BR(H -> g       g      )
     9.52255523E-12    2          22        22   # BR(H -> gam     gam    )
     1.82669536E-09    2          23        22   # BR(H -> Z       gam    )
     1.96466300E-06    2          24       -24   # BR(H -> W+      W-     )
     9.81650993E-07    2          23        23   # BR(H -> Z       Z      )
     7.33812786E-06    2          25        25   # BR(H -> h       h      )
     1.29864135E-24    2          36        36   # BR(H -> A       A      )
     6.18776567E-20    2          23        36   # BR(H -> Z       A      )
     1.85806637E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64406177E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64406177E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.84191812E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55321397E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.57894294E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.10694747E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.22397781E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.55974403E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.58850996E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.71980825E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.20753166E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.47414550E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.20449897E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.20449897E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43276391E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48040133E+01   # A decays
#          BR         NDA      ID1       ID2
     3.47324904E-01    2           5        -5   # BR(A -> b       bb     )
     6.04995054E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13911429E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53061623E-04    2           3        -3   # BR(A -> s       sb     )
     7.14256323E-08    2           4        -4   # BR(A -> c       cb     )
     7.12598856E-03    2           6        -6   # BR(A -> t       tb     )
     1.46421110E-05    2          21        21   # BR(A -> g       g      )
     4.68633496E-08    2          22        22   # BR(A -> gam     gam    )
     1.60985439E-08    2          23        22   # BR(A -> Z       gam    )
     1.96058916E-06    2          23        25   # BR(A -> Z       h      )
     1.85924700E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64412681E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64412681E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.46255988E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.92478427E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.27791408E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.68445426E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.89984458E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.42684141E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.78679529E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.24589283E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.81384983E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.36789551E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.36789551E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47547134E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.54810253E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05693270E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14158301E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55078257E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21324755E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49604037E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.53522785E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.96453413E-06    2          24        25   # BR(H+ -> W+      h      )
     2.57525169E-14    2          24        36   # BR(H+ -> W+      A      )
     5.59572385E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.42459940E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.25220910E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.64770102E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.00114208E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59938293E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00432542E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.10987322E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.04642142E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
