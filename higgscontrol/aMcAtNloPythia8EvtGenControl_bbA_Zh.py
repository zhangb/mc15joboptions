evgenConfig.description = 'MadGraph5_aMC@NLO+Pythia8 bbA production'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'ZHiggs', 'bottom' ]
evgenConfig.contact     = [ 'paul.daniel.thompson@cern.ch' ]
evgenConfig.generators  = [ "aMcAtNlo", "Pythia8", "EvtGen"] 

evgenConfig.minevents = 5000

import math
import re

#parse channel, component and mass from joboption name
jo_name = runArgs.jobConfig[0]
if "llbb" in jo_name:
    channel = "llbb"
elif "vvbb" in jo_name:
    channel = "vvbb"
else:
    raise Exception("Unable to parse subchannel from joboption name.")

# factor of events more needed 
if runArgs.maxEvents > 0:
    nEvents = runArgs.maxEvents
else:
    nEvents = evgenConfig.minevents

if channel == "llbb":
    lhe_factor = 1.4
elif channel == "vvbb":
    lhe_factor = 1.4

nLHEvents = int(nEvents * lhe_factor)

# event generation and lhe manipulation
include("MC15JobOptions/aMcAtNloControl_bbA4FS.py")

# Pythia fragmentation
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "Check:epTolErr = 1e-2",
                            "SLHA:readFrom = 2", # take A0 from slha file - issues with Pythia readding lhe 
                            "SLHA:file = narrowWidth.slha", 
                            "25:m0 125.1", #h mass
                            "25:onMode = off", 
                            "25:onIfMatch = 5 -5",
                            "23:onMode = off"] 

# Z decay
if channel == "llbb":
    genSeq.Pythia8.Commands += ["23:onIfAny = 11 13 15"]
elif channel == "vvbb":
    genSeq.Pythia8.Commands += ["23:onIfAny = 12 14 16"]


# set width for heavy Higgs
nwf=open('./narrowWidth.slha', 'w')
nwfinput = """#           PDG      WIDTH
DECAY   36  1.00
#          BR         NDA          ID1       ID2       ID3       ID4
1       2       23      25
"""
nwf.write(nwfinput)
nwf.close()
