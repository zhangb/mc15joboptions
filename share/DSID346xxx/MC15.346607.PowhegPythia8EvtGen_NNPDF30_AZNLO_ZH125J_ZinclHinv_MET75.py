#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
 
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
#--------------------------------------------------------------
# H->ZZ->4v decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 12 14 16']

 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->inv+all production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'valerio.dao.ch' ]
evgenConfig.process = "qq->ZH, H->inv, Z->all"
evgenConfig.minevents   = 5000
evgenConfig.inputFilesPerJob = 120

#--------------------------------------------------------------
# Missing Et filter 
#--------------------------------------------------------------
include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter" 

