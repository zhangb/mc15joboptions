##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B-\n")
f.write("1.0000  myJ/psi   K-             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
include("Sherpa_i/2.2.6_NNPDF30NNLO.py")

evgenConfig.description = "Photon with a B+/- meson"
evgenConfig.keywords = [ "photon", "LO", "QCD", "SM" ]
evgenConfig.contact  = [ "javier.llorente.merino@cern.ch" ]
evgenConfig.minevents = 100

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{PPerp2(p[2])};

  ALPHAQED_DEFAULT_SCALE=0.0

  % tags for process setup
  NJET:=2; LJET:=0; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
}(run)

(processes){
  Process 93 93 -> 22 93 93{NJET}
  Order (*,1)
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]))
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {3}
  Integration_Error 0.99 {3}
  PSI_ItMin 50000 {4,5}
  Integration_Error 0.99 {4,5}
  End process
}(processes)

(selector){
  IsolationCut  22  0.1  2  0.10
  PTNLO 22  70 180
  RapidityNLO  22  -2.7  2.7
}(selector)
"""

genSeq.Sherpa_i.NCores=10

#Let EvtGen do the B decay 
evgenConfig.generators += ["Sherpa"]
include("MC15JobOptions/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
genSeq.EvtInclusiveDecay.readExisting = True
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[-521]
genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"
evgenConfig.auxfiles += [ 'BU_JPSI_K_USER.DEC' ]

##Filter for Bp
include("MC15JobOptions/BSignalFilter.py")
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = -521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6
