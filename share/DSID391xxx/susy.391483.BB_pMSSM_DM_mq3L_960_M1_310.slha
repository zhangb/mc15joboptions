#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13003268E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.46990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04186962E+01   # W+
        25     1.26079897E+02   # h
        35     4.00000797E+03   # H
        36     3.99999679E+03   # A
        37     4.00105391E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02591041E+03   # ~d_L
   2000001     4.02256928E+03   # ~d_R
   1000002     4.02526081E+03   # ~u_L
   2000002     4.02330481E+03   # ~u_R
   1000003     4.02591041E+03   # ~s_L
   2000003     4.02256928E+03   # ~s_R
   1000004     4.02526081E+03   # ~c_L
   2000004     4.02330481E+03   # ~c_R
   1000005     1.01524644E+03   # ~b_1
   2000005     4.02542415E+03   # ~b_2
   1000006     9.87851954E+02   # ~t_1
   2000006     1.80243501E+03   # ~t_2
   1000011     4.00453675E+03   # ~e_L
   2000011     4.00346507E+03   # ~e_R
   1000012     4.00342600E+03   # ~nu_eL
   1000013     4.00453675E+03   # ~mu_L
   2000013     4.00346507E+03   # ~mu_R
   1000014     4.00342600E+03   # ~nu_muL
   1000015     4.00460608E+03   # ~tau_1
   2000015     4.00769115E+03   # ~tau_2
   1000016     4.00485924E+03   # ~nu_tauL
   1000021     1.97719177E+03   # ~g
   1000022     2.99429313E+02   # ~chi_10
   1000023    -3.58076714E+02   # ~chi_20
   1000025     3.70020366E+02   # ~chi_30
   1000035     2.05214732E+03   # ~chi_40
   1000024     3.55599517E+02   # ~chi_1+
   1000037     2.05231183E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.64141999E-01   # N_11
  1  2    -1.37447090E-02   # N_12
  1  3    -3.86278421E-01   # N_13
  1  4    -3.22271113E-01   # N_14
  2  1    -4.96255542E-02   # N_21
  2  2     2.44945390E-02   # N_22
  2  3    -7.03237196E-01   # N_23
  2  4     7.08798115E-01   # N_24
  3  1     5.00794239E-01   # N_31
  3  2     2.81794807E-02   # N_32
  3  3     5.96843660E-01   # N_33
  3  4     6.26249705E-01   # N_34
  4  1    -1.01999380E-03   # N_41
  4  2     9.99208195E-01   # N_42
  4  3    -4.90644289E-03   # N_43
  4  4    -3.94698496E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.94437705E-03   # U_11
  1  2     9.99975888E-01   # U_12
  2  1    -9.99975888E-01   # U_21
  2  2     6.94437705E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.58299858E-02   # V_11
  1  2    -9.98440290E-01   # V_12
  2  1    -9.98440290E-01   # V_21
  2  2    -5.58299858E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85729114E-01   # cos(theta_t)
  1  2    -1.68339282E-01   # sin(theta_t)
  2  1     1.68339282E-01   # -sin(theta_t)
  2  2     9.85729114E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999363E-01   # cos(theta_b)
  1  2    -1.12871590E-03   # sin(theta_b)
  2  1     1.12871590E-03   # -sin(theta_b)
  2  2     9.99999363E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05858451E-01   # cos(theta_tau)
  1  2     7.08352911E-01   # sin(theta_tau)
  2  1    -7.08352911E-01   # -sin(theta_tau)
  2  2    -7.05858451E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00284596E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30032679E+03  # DRbar Higgs Parameters
         1    -3.46990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43580398E+02   # higgs               
         4     1.60911901E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30032679E+03  # The gauge couplings
     1     3.61941817E-01   # gprime(Q) DRbar
     2     6.36094789E-01   # g(Q) DRbar
     3     1.03007624E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30032679E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36780153E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30032679E+03  # The trilinear couplings
  1  1     5.02580188E-07   # A_d(Q) DRbar
  2  2     5.02626834E-07   # A_s(Q) DRbar
  3  3     9.00184914E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30032679E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10537373E-07   # A_mu(Q) DRbar
  3  3     1.11672380E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30032679E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68728843E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30032679E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84999102E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30032679E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03453297E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30032679E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57702764E+07   # M^2_Hd              
        22    -7.14641563E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40368401E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.69267522E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.36935981E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.36935981E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.61858604E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.61858604E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     1.20541553E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     1.20541553E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.16170205E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.07657825E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.21118914E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.75801576E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.12313728E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12188939E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.16034368E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.09801866E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.62638547E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.13303183E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.64860264E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.29543573E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.44623822E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.21660179E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.52880643E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.96489182E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.59106361E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.89152381E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65908895E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54927585E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82175858E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64385743E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.81849536E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66646423E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.74532035E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12590692E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.28538540E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.72700912E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.86926787E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.95360224E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77805717E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.95847841E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96683044E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.31026902E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82624683E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.39136497E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64031836E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51506796E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60108272E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.14407415E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.36042452E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.38397935E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.21215654E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44583390E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77825280E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.74136292E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.94134498E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.85513895E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83132724E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.34172728E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67268046E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51725291E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53390493E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.08118568E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.54933092E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.61078378E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.37839985E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85541858E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77805717E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.95847841E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96683044E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.31026902E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82624683E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.39136497E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64031836E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51506796E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60108272E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.14407415E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.36042452E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.38397935E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.21215654E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44583390E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77825280E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.74136292E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.94134498E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.85513895E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83132724E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.34172728E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67268046E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51725291E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53390493E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.08118568E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.54933092E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.61078378E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.37839985E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85541858E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14604656E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.15728261E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.06202457E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.93793077E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77799768E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.86352688E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57036966E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06039480E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.47861244E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.45446875E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.49683714E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.72725183E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14604656E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.15728261E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.06202457E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.93793077E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77799768E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.86352688E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57036966E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06039480E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.47861244E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.45446875E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.49683714E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.72725183E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09462067E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.69727854E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40844720E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.10265940E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40088777E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.49296027E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80903355E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09025914E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.78058672E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.14165286E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.81246948E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42808586E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.01156268E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86353638E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14710888E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.29342775E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.40098062E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.31832699E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78164763E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14158516E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54766627E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14710888E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.29342775E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.40098062E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.31832699E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78164763E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14158516E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54766627E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47419144E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17208510E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26955235E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.00703171E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52195361E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62817246E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02974535E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.91257409E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33508157E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33508157E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11170804E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11170804E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10642078E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.94753685E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.50633059E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.81764791E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.31939174E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.05538814E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.44072076E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.86024972E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.38889741E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.96934421E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.78518737E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.91842333E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17753565E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52748872E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17753565E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52748872E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43579599E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50290956E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50290956E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47649018E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00331666E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00331666E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00331648E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.03677505E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.03677505E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.03677505E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.03677505E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.78925679E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.78925679E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.78925679E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.78925679E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.74445857E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.74445857E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.06367253E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.48001606E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.52888635E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.49824770E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22795180E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.49824770E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.22795180E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.18300141E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.78289430E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.78289430E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.77723415E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.64531846E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.64531846E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.64530916E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.25453190E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.92466220E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.25453190E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.92466220E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.59360131E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.70758983E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.70758983E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.88662663E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.34083143E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.34083143E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.34083144E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.95883711E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.95883711E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.95883711E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.95883711E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.52939549E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.52939549E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.52939549E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.52939549E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.06217139E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.06217139E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.94625922E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.16001333E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.02756540E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.41227551E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.85159726E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.85159726E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.51545789E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.99858997E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.09822566E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.70880682E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.70880682E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.76074848E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.76074848E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.70484719E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.70484719E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09959632E-03   # h decays
#          BR         NDA      ID1       ID2
     5.84359746E-01    2           5        -5   # BR(h -> b       bb     )
     6.38334262E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25967560E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78813111E-04    2           3        -3   # BR(h -> s       sb     )
     2.07940276E-02    2           4        -4   # BR(h -> c       cb     )
     6.81815081E-02    2          21        21   # BR(h -> g       g      )
     2.38565608E-03    2          22        22   # BR(h -> gam     gam    )
     1.67784245E-03    2          22        23   # BR(h -> Z       gam    )
     2.28884234E-01    2          24       -24   # BR(h -> W+      W-     )
     2.91787792E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51802058E+01   # H decays
#          BR         NDA      ID1       ID2
     3.56911241E-01    2           5        -5   # BR(H -> b       bb     )
     6.00867720E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12452272E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51294400E-04    2           3        -3   # BR(H -> s       sb     )
     7.04880276E-08    2           4        -4   # BR(H -> c       cb     )
     7.06436795E-03    2           6        -6   # BR(H -> t       tb     )
     5.17109531E-07    2          21        21   # BR(H -> g       g      )
     2.07330852E-09    2          22        22   # BR(H -> gam     gam    )
     1.81564938E-09    2          23        22   # BR(H -> Z       gam    )
     1.86390291E-06    2          24       -24   # BR(H -> W+      W-     )
     9.31305875E-07    2          23        23   # BR(H -> Z       Z      )
     7.29993666E-06    2          25        25   # BR(H -> h       h      )
    -5.90336815E-25    2          36        36   # BR(H -> A       A      )
     5.20678887E-20    2          23        36   # BR(H -> Z       A      )
     1.84597307E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59633773E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59633773E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.17388332E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.78516990E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.31895926E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.67127783E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.20500921E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.32547495E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.11237192E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.12379899E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.56490319E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.40673837E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.77896899E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     3.84783179E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.84783179E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.29281848E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51748756E+01   # A decays
#          BR         NDA      ID1       ID2
     3.56971443E-01    2           5        -5   # BR(A -> b       bb     )
     6.00928776E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12473693E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51360737E-04    2           3        -3   # BR(A -> s       sb     )
     7.09455644E-08    2           4        -4   # BR(A -> c       cb     )
     7.07809317E-03    2           6        -6   # BR(A -> t       tb     )
     1.45436905E-05    2          21        21   # BR(A -> g       g      )
     5.93611808E-08    2          22        22   # BR(A -> gam     gam    )
     1.59915979E-08    2          23        22   # BR(A -> Z       gam    )
     1.86011789E-06    2          23        25   # BR(A -> Z       h      )
     1.85962348E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59644507E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59644507E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.88177671E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.57903301E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.10692411E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.25611118E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.83771036E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.45128167E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.23571899E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.71161668E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.84623191E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.31816938E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.31816938E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52631171E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.72682431E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00127801E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12190488E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.66516444E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20209782E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47310179E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.64620359E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.85927100E-06    2          24        25   # BR(H+ -> W+      h      )
     3.14357455E-14    2          24        36   # BR(H+ -> W+      A      )
     6.10121244E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.41136431E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.49459488E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59604654E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.77712867E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58170010E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14145643E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.36615801E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.22143850E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
