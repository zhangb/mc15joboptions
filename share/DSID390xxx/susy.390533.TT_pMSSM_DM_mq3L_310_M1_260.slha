#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.84153708E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.07900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04179277E+01   # W+
        25     1.24927242E+02   # h
        35     4.00000699E+03   # H
        36     3.99999588E+03   # A
        37     4.00096963E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.00997310E+03   # ~d_L
   2000001     4.00973041E+03   # ~d_R
   1000002     4.00930822E+03   # ~u_L
   2000002     4.01178390E+03   # ~u_R
   1000003     4.00997310E+03   # ~s_L
   2000003     4.00973041E+03   # ~s_R
   1000004     4.00930822E+03   # ~c_L
   2000004     4.01178390E+03   # ~c_R
   1000005     3.41636367E+02   # ~b_1
   2000005     4.01646450E+03   # ~b_2
   1000006     3.30039157E+02   # ~t_1
   2000006     2.32369638E+03   # ~t_2
   1000011     4.00185212E+03   # ~e_L
   2000011     4.00209992E+03   # ~e_R
   1000012     4.00072923E+03   # ~nu_eL
   1000013     4.00185212E+03   # ~mu_L
   2000013     4.00209992E+03   # ~mu_R
   1000014     4.00072923E+03   # ~nu_muL
   1000015     4.00413553E+03   # ~tau_1
   2000015     4.00826149E+03   # ~tau_2
   1000016     4.00355112E+03   # ~nu_tauL
   1000021     1.95674870E+03   # ~g
   1000022     2.53289794E+02   # ~chi_10
   1000023    -3.17244576E+02   # ~chi_20
   1000025     3.26847781E+02   # ~chi_30
   1000035     2.05921507E+03   # ~chi_40
   1000024     3.14701147E+02   # ~chi_1+
   1000037     2.05937895E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.93498836E-01   # N_11
  1  2    -1.15037809E-02   # N_12
  1  3    -3.53230625E-01   # N_13
  1  4    -2.77048044E-01   # N_14
  2  1    -5.74258734E-02   # N_21
  2  2     2.50326664E-02   # N_22
  2  3    -7.02274825E-01   # N_23
  2  4     7.09144347E-01   # N_24
  3  1     4.45377498E-01   # N_31
  3  2     2.85314454E-02   # N_32
  3  3     6.18078642E-01   # N_33
  3  4     6.47150395E-01   # N_34
  4  1    -9.91906601E-04   # N_41
  4  2     9.99213183E-01   # N_42
  4  3    -4.12159623E-03   # N_43
  4  4    -3.94340374E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.83434984E-03   # U_11
  1  2     9.99982980E-01   # U_12
  2  1    -9.99982980E-01   # U_21
  2  2     5.83434984E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.57803664E-02   # V_11
  1  2    -9.98443063E-01   # V_12
  2  1    -9.98443063E-01   # V_21
  2  2    -5.57803664E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97853733E-01   # cos(theta_t)
  1  2    -6.54822689E-02   # sin(theta_t)
  2  1     6.54822689E-02   # -sin(theta_t)
  2  2     9.97853733E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999543E-01   # cos(theta_b)
  1  2    -9.56033363E-04   # sin(theta_b)
  2  1     9.56033363E-04   # -sin(theta_b)
  2  2     9.99999543E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05347972E-01   # cos(theta_tau)
  1  2     7.08861226E-01   # sin(theta_tau)
  2  1    -7.08861226E-01   # -sin(theta_tau)
  2  2    -7.05347972E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00254079E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.41537078E+02  # DRbar Higgs Parameters
         1    -3.07900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44832777E+02   # higgs               
         4     1.64052331E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.41537078E+02  # The gauge couplings
     1     3.60487749E-01   # gprime(Q) DRbar
     2     6.35813067E-01   # g(Q) DRbar
     3     1.04015254E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.41537078E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     4.82406709E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.41537078E+02  # The trilinear couplings
  1  1     1.77230219E-07   # A_d(Q) DRbar
  2  2     1.77247413E-07   # A_s(Q) DRbar
  3  3     3.17265127E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.41537078E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     3.91601323E-08   # A_mu(Q) DRbar
  3  3     3.95471420E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.41537078E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74223066E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.41537078E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85430008E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.41537078E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02992520E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.41537078E+02  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57675614E+07   # M^2_Hd              
        22    -1.93508426E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40247449E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.61688419E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46855394E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46855394E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53144606E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53144606E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.28532731E-03   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.38375547E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.76337167E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18071423E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.02857122E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.25895009E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.34186970E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.22199592E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.03408606E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.13734955E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.94695462E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.90636114E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.16126720E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.82403012E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.72615047E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.49819409E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66371535E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.50123227E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.75477688E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61724650E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.96432578E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.53875807E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.17686601E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15389710E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.06491309E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.42122612E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.39596206E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.68850030E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77733581E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.14629840E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.73525618E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.09542205E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.75228890E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37160488E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.49241948E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53756683E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60752698E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.37505223E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.79942389E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.08156776E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.96451174E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45253828E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77753672E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81996941E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11146171E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.67003381E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.75723574E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.87695331E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.52448456E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53978801E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53976177E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.14129538E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.69403562E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.82141067E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.72783356E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85718687E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77733581E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.14629840E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.73525618E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.09542205E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.75228890E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37160488E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.49241948E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53756683E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60752698E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.37505223E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.79942389E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.08156776E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.96451174E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45253828E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77753672E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81996941E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11146171E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.67003381E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.75723574E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.87695331E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.52448456E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53978801E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53976177E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.14129538E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.69403562E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.82141067E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.72783356E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85718687E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12392646E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.25253880E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.88130833E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.01481801E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77705178E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.46374663E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56829311E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05057511E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.99199580E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.28619620E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.97513687E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.36748809E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12392646E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.25253880E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.88130833E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.01481801E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77705178E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.46374663E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56829311E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05057511E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.99199580E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.28619620E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.97513687E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.36748809E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07883878E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.86919463E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.36039839E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.43421661E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39855762E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48517355E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80426890E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07845909E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.99549108E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25353325E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.58592710E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42685174E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05398068E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86097868E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12501203E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.37079239E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68610340E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.54839578E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78043856E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.16408473E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54542759E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12501203E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.37079239E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68610340E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.54839578E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78043856E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.16408473E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54542759E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45402073E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.24110802E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52659881E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.30732129E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51992321E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.67122910E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02584774E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.51952286E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33475305E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33475305E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11159657E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11159657E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10730075E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.90579253E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.88685892E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.79698024E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.90024433E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.60473492E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.77098871E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.97941105E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.75049088E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.20143912E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.62088107E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17187409E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.51928031E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17187409E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.51928031E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.48333135E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.47921658E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.47921658E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45571736E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.95437956E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.95437956E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.95437913E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.87689921E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.87689921E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.87689921E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.87689921E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.25633760E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.25633760E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.25633760E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.25633760E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.20769335E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.20769335E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.88221319E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.82503179E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.46542841E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.09309757E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.41417249E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.09309757E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.41417249E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.60126696E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.21464724E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.21464724E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.20512206E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.48390470E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.48390470E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.48389177E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.89219103E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.04626003E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.89219103E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.04626003E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.15574697E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.15574697E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.34380061E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.30971873E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.30971873E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.30971876E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.25908897E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.25908897E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.25908897E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.25908897E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.41968464E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.41968464E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.41968464E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.41968464E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.27925579E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.27925579E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.90476685E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.46641945E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.57360453E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.97916081E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.76076027E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.76076027E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.21421428E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.27901194E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.03604773E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.91304091E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.91304091E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.92908864E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.92908864E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.95104187E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01061632E-01    2           5        -5   # BR(h -> b       bb     )
     6.56185124E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32291806E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93095202E-04    2           3        -3   # BR(h -> s       sb     )
     2.14169462E-02    2           4        -4   # BR(h -> c       cb     )
     7.13517204E-02    2          21        21   # BR(h -> g       g      )
     2.36478488E-03    2          22        22   # BR(h -> gam     gam    )
     1.57226718E-03    2          22        23   # BR(h -> Z       gam    )
     2.09638753E-01    2          24       -24   # BR(h -> W+      W-     )
     2.62499974E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51657650E+01   # H decays
#          BR         NDA      ID1       ID2
     3.56825631E-01    2           5        -5   # BR(H -> b       bb     )
     6.01025046E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12507899E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51360205E-04    2           3        -3   # BR(H -> s       sb     )
     7.04978700E-08    2           4        -4   # BR(H -> c       cb     )
     7.06535435E-03    2           6        -6   # BR(H -> t       tb     )
     7.88559576E-07    2          21        21   # BR(H -> g       g      )
     6.21755004E-10    2          22        22   # BR(H -> gam     gam    )
     1.81792970E-09    2          23        22   # BR(H -> Z       gam    )
     1.70551593E-06    2          24       -24   # BR(H -> W+      W-     )
     8.52167206E-07    2          23        23   # BR(H -> Z       Z      )
     6.60365746E-06    2          25        25   # BR(H -> h       h      )
     3.90040703E-24    2          36        36   # BR(H -> A       A      )
     7.73375979E-20    2          23        36   # BR(H -> Z       A      )
     1.85910556E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60123096E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60123096E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.93223647E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.16012744E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.09540630E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.83819839E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.73033549E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     1.97188574E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.38254106E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.01250682E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.07396188E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.03733948E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.23978780E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.23978780E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.46453258E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51640032E+01   # A decays
#          BR         NDA      ID1       ID2
     3.56862625E-01    2           5        -5   # BR(A -> b       bb     )
     6.01047077E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12515521E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51410228E-04    2           3        -3   # BR(A -> s       sb     )
     7.09595330E-08    2           4        -4   # BR(A -> c       cb     )
     7.07948680E-03    2           6        -6   # BR(A -> t       tb     )
     1.45465583E-05    2          21        21   # BR(A -> g       g      )
     5.67297912E-08    2          22        22   # BR(A -> gam     gam    )
     1.59926100E-08    2          23        22   # BR(A -> Z       gam    )
     1.70203506E-06    2          23        25   # BR(A -> Z       h      )
     1.86694074E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60127079E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60127079E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.67839117E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.02476621E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.12560482E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.48012984E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.40838518E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.03591071E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.03468288E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.82668945E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.19655653E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.29696763E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.29696763E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.52700182E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.72963522E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00040228E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12159524E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.66696342E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20192559E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47274744E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.64794904E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.70060588E-06    2          24        25   # BR(H+ -> W+      h      )
     2.08439658E-14    2          24        36   # BR(H+ -> W+      A      )
     6.61644101E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.83693825E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.04354803E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60025751E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.00598442E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58162965E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.22676263E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.07273049E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     6.51646559E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
