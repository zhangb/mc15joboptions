evgenConfig.description = "Herwig W->had  1000 < pT < 1400 GeV"
evgenConfig.keywords = ["SM","W","jets"]
evgenConfig.process = "W + jets (W -> qqbar)"
evgenConfig.contact  = ["craig.sawyer@cern.ch"]
	
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEWJet
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:WDecay Quarks

set /Herwig/Cuts/WBosonKtCut:MinKT 1000.*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 1400.*GeV
"""

genSeq.Herwigpp.Commands += cmds.splitlines()
