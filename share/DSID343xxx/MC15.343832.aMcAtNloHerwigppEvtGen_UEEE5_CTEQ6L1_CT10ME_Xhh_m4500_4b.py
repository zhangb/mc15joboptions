from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
"""

# LHEHandler generated events/number of attempts ~75%
safefactor = 2
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> b bbar"
evgenConfig.keywords = ["BSM", "BSMHiggs", "resonance", "bbbar", "bottom"]
run_number_min = 343828 
run_number_max = 343833
offset = 26 

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")
evgenConfig.generators  = [  "aMcAtNlo", "Herwigpp", "EvtGen"] 
