from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]

evgenConfig.description = 'Reading HepMC to convert into EVNT'
evgenConfig.keywords+=['SM']
evgenConfig.contact  = [ 'zach.m@cern.ch' ]
