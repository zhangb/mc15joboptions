## Job options file for Herwig++, QCD jet slice production
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

evgenConfig.description = "QCD dijet production JZ2 R04 jets with PDF and UE-EE5 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "Andrzej.Olszewski@cern.ch" ]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 15*GeV
"""

genSeq.Herwigpp.Commands += cmds.splitlines()

include('MC15JobOptions/Herwigpp_EvtGen.py')
include("MC15JobOptions/JetFilter_JZ2R04.py")
evgenConfig.minevents = 1000

