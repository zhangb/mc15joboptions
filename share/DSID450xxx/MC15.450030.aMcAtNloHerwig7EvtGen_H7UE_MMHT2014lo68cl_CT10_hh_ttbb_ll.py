#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/tau-:PrintDecayModes
do /Herwig/Particles/tau+:PrintDecayModes
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->b,bbar;
do /Herwig/Particles/tau-:SelectDecayModes tau-->nu_tau,nu_ebar,e-; tau-->nu_tau,nu_mubar,mu-;
do /Herwig/Particles/tau+:SelectDecayModes tau+->nu_taubar,nu_e,e+; tau+->nu_taubar,nu_mu,mu+;
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio 0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio 0.5
set /Herwig/Particles/tau-/tau-->nu_tau,nu_ebar,e-;:BranchingRatio 0.5
set /Herwig/Particles/tau-/tau-->nu_tau,nu_mubar,mu-;:BranchingRatio 0.5
set /Herwig/Particles/tau+/tau+->nu_taubar,nu_e,e+;:BranchingRatio 0.5
set /Herwig/Particles/tau+/tau+->nu_taubar,nu_mu,mu+;:BranchingRatio 0.5
""")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
if (runArgs.runNumber == 450030):
    evgenConfig.description = "SM diHiggs production, decay to bbtautau (leplep), with MG5_aMC@NLO, inclusive of box diagram FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "bbbar", "tau"]

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10'
evgenConfig.minevents   = 5000

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hTauTauFilter", PDGParent = [25], PDGChild = [15])
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [11,13]

#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (no tau(had)) with pt/eta cuts on e/mu and tau(had)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("LepTauPtFilter")
filtSeq.LepTauPtFilter.UseNewOptions = True
filtSeq.LepTauPtFilter.Ntaus = 2
filtSeq.LepTauPtFilter.Nleptaus = 2
filtSeq.LepTauPtFilter.Nhadtaus = 0
filtSeq.LepTauPtFilter.EtaMaxlep = 3.0
filtSeq.LepTauPtFilter.EtaMaxhad = 3.0
filtSeq.LepTauPtFilter.Ptcutlep = 7000.0
filtSeq.LepTauPtFilter.Ptcutlep_lead = 13000.0
filtSeq.LepTauPtFilter.Ptcuthad = 15000.0
filtSeq.LepTauPtFilter.Ptcuthad_lead = 15000.0

filtSeq.Expression = "hbbFilter and hTauTauFilter and XtoVVDecayFilterExtended and LepTauPtFilter"

# run Herwig7
Herwig7Config.run()
