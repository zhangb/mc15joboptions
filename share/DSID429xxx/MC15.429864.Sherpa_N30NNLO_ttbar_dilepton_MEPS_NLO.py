include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.7 ttbar production with tt+0,1j@NLO(QCD+EWvirt)+2,3,4j@LO in the dlepton channel."
evgenConfig.keywords = ["SM", "top", "ttbar", "2lepton" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "ttbar" # universal for all decay channels

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE QCD;
  EXCLUSIVE_CLUSTER_MODE 1;

  %tags for process setup
  NJET:=4; LJET:=2,3; QCUT:=30.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;

  INTEGRATION_ERROR=0.05;

  %decay settings
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  HDH_STATUS[24,12,-11]=2
  HDH_STATUS[24,14,-13]=2
  HDH_STATUS[24,16,-15]=2
  HDH_STATUS[-24,-12,11]=2
  HDH_STATUS[-24,-14,13]=2
  HDH_STATUS[-24,-16,15]=2
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;

  % alphas scale factor 1 for better high jet multi
  CSS_IS_AS_FAC=1.0

  % stores veto information as named weight instead of doing a veto
  USERHOOK = FUSING_HOOK
  FUSING_STORE_IN_HEPMC=1
}(run)

(processes){
  Process : 93 93 ->  6 -6 93{NJET};
  NLO_QCD_Mode 3 {LJET}; CKKW sqr(QCUT/E_CMS);
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN;
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Order (*,0);
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0", "OL_PARAMETERS=ew_cheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptt", "ppttj", "pptt_ew", "ppttj_ew" ]

genSeq.Sherpa_i.PluginCode="""
#include "SHERPA/Tools/Userhook_Base.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Math/Random.H"
#include "ATOOLS/Org/Run_Parameter.H"

#include "SHERPA/Single_Events/Event_Handler.H"

#include "ATOOLS/Org/Data_Reader.H"
#include "ATOOLS/Phys/Weight_Info.H"
#include <algorithm>
#include "MODEL/Main/Running_AlphaS.H"
#include "SHERPA/Main/Sherpa.H"
#include "SHERPA/Initialization/Initialization_Handler.H"
#include "ATOOLS/Org/Data_Reader.H"


/* implements the Fusing algorithm based on a combined cluster amplitude.
 * 
 * Hook is applied in tt+jets sample
 *
 * compile instruction:
     --> C++11 required, may use "-std=c++0x" below
 *      SHERPA_PREFIX=<path to sherpa directory>
 *      g++ -shared -g -I`$SHERPA_PREFIX/bin/Sherpa-config --incdir`  `$SHERPA_PREFIX/bin/Sherpa-config --ldflags`  -fPIC -o libFUSING_Hook.so FUSING_Hook.C

*/


using namespace ATOOLS;
using namespace SHERPA;



class FUSING_Hook : public Userhook_Base {

private:
  MODEL::Running_AlphaS *p_as;
  Blob_List * p_bl;
  Sherpa* p_sherpa;
  size_t m_tops_in_proc;
  size_t m_modus;
  bool m_nlo;
  bool m_store;
  bool m_extra_run;


public:

  FUSING_Hook(const Userhook_Arguments args) :
    Userhook_Base("FUSING"), p_as(MODEL::as), p_sherpa(args.p_sherpa)
  {
       msg_Debugging()<<"FUSING Hook active."<<std::endl;
       ATOOLS::Data_Reader *reader =  p_sherpa->GetInitHandler()->DataReader();
       m_modus = reader->GetValue<int>("FUSING_SELMODUS", 2);

       // mode 0:            do not perform any veto
       // mode 1:            use previous amplitude  -> CheckFinal
       // mode 2 (default):  use current amplitude and look for extra partons -> CheckFinalDir
       // mode 3:            as mode 2, but veto if there is no previous amplitude  -> CheckFinalDir

       m_nlo = reader->GetValue<int>("FUSING_NLO_Mode", 1);
       m_store = reader->GetValue<int>("FUSING_STORE_IN_HEPMC", 0);

       m_extra_run = reader->GetValue<int>("FUSING_EXTRA_RUN",0);
       msg_Debugging()<<METHOD<<"(): { modus(" << m_modus <<
                        "), NLO(" << m_nlo << ") \\n }" << std::endl;


       m_tops_in_proc=0;
       PHASIC::Process_Base  * proc = p_sherpa->GetInitHandler()->GetMatrixElementHandler()->AllProcesses().at(0);
       ATOOLS::Flavour_Vector flavs = proc->Flavours();
       for (int i=2; i< flavs.size(); i++){
           ATOOLS::Flavour flav = flavs.at(i);
           if  (flav.Kfcode()==6) m_tops_in_proc ++;
         }


 }

  ~FUSING_Hook() {}

  ATOOLS::Return_Value::code Run(ATOOLS::Blob_List* blobs, double &weight) {
    p_bl = blobs;


    if (m_modus==0) return ReturnFunc(true, weight);

    if (m_modus>3){
       THROW(fatal_error,"Only modus 1,2 and 3 is implemented!");
      }


    else {
       ATOOLS::String_BlobDataBase_Map  bdmap = p_bl->FindFirst(ATOOLS::btp::Shower)->GetData();

       auto search = bdmap.find("AllAmplitudes");
       if (search==bdmap.end()) {
           THROW(fatal_error,"No matching amplitude found in blob. This algorithm works only with rel-2-2-5 or later!");
         }

       else{

       ATOOLS::Cluster_Amplitude * orig_ampl = search->second->Get<ATOOLS::Cluster_Amplitude*>();

       ATOOLS::Cluster_Amplitude * ampl = orig_ampl;


       bool veto   = true;

       // find first amplitude with b
       size_t pos=0;
       while (true){
           if (ampl==NULL) break;
           pos = FindB(ampl);
           if (pos==0) {
               ampl = ampl->Next();
             }
           else break;
         }

       if (pos==0) veto=false;
       else{
           if (m_modus==1) veto = !CheckFinal(ampl->Prev());  // keep amplitude -> do not a veto
           if ((m_modus==2) || (m_modus==3)) veto = !CheckFinalDir(ampl);
       }


       // redo veto, if amplitude gives a configuration of higher order
       // do only, if there's no previous amplitude?
       if (veto && CheckHigherOrder(ampl)) {
            msg_Debugging() << "skip Fusing Veto, configuration is of higher order.";
           veto=false;
       }

       return ReturnFunc(!veto, weight);

      }
    }

  }

  Return_Value::code ReturnFunc(bool retval, double & event_weight){
    //retval=false: veto Event
    //retval=true:  keep Event

    String_BlobDataBase_Map  bdmap = p_bl->FindFirst(btp::Signal_Process)->GetData();
    auto search_weight = bdmap.find("Weight");
    if (search_weight==bdmap.end()) {
        THROW(fatal_error,"No Weight found in singnal blob!");
      }
    double weight_bl = search_weight->second->Get<double>();



    // default mode, generate corrections as part of the 5FS sample.
    if (!m_extra_run){

        if (retval==false){
            msg_Debugging() << "false, veto configuration.  \\n";
          }

        // if not using HepMC:  do either Veto or not
        if (!m_store) {
            if (retval) return Return_Value::Nothing;
            else return Return_Value::New_Event;
          }
        // if m_store: store additional weight(zero) in bloblist and slip veto
        double new_weight = weight_bl;
        if (retval==false) new_weight=0;

        p_bl->FindFirst(ATOOLS::btp::Signal_Process)->AddData("UserHook",new ATOOLS::Blob_Data<double>(new_weight));
        return Return_Value::Nothing;

      }
    // separate mode. only the vetoed events are kept with their negative weights 
    else{
        if (retval) return Return_Value::New_Event;
        event_weight =-1.*event_weight;
        p_bl->FindFirst(btp::Signal_Process)->AddData("Weight",new Blob_Data<double>(-1.*weight_bl));
        return Return_Value::Nothing;
      }

  }

  ///////////////////////   helper functions Veto
      bool CheckFinal(ATOOLS::Cluster_Amplitude *ampl){
        /*return only true, if a sufficient high number of light partons is present in this amplitude's final state
          if the tops are clustered to a gluon before, there is one more light parton allowed in this step */

        //  true:   keep amplitude
        //  false:  do veto
        if (ampl==NULL) return false;
        size_t num_q(0);
        size_t num_g(0);
        size_t num_tops(0);
        for (int i=2; i<ampl->Legs().size(); i++){
            ATOOLS::Cluster_Leg *leg = ampl->Legs().at(i);
            if (leg->Flav().IsGluon() && !leg->FromDec()) num_g++;
            if (abs(leg->Flav().Kfcode())<5 && !leg->FromDec()) num_q++;
            if (abs(leg->Flav().Kfcode())==6 && !leg->FromDec()) num_tops++;
        }


        if (m_nlo){
            if (num_tops < m_tops_in_proc){
                if (num_q >=2 || (num_q + num_g)>3) return true;
                else return false;
              }
            else{
                if (num_q >=2 || (num_q + num_g)>2) return true;
                else return false;
             }
         }
        else{
            if (num_tops < m_tops_in_proc){
                if ((num_q >=1) || (num_g>2)) return true; 
                else return false;
              }
            else{
              if ((num_q >=1) || (num_g>1)) return true;
              else return false;
            }
         }
      }

      bool CheckFinalDir(ATOOLS::Cluster_Amplitude *ampl){
        /*return only true, if a sufficient high number of light partons is present in this amplitude's final state
          if the tops are clustered to a gluon before, there is one more light parton allowed in this step */

        //  true:   keep amplitude
        //  false:  do veto
        if (m_modus==3 && ampl->Prev()==NULL ) return false;
        size_t num_q(0);
        size_t num_g(0);
        size_t num_tops(0);
        for (int i=2; i<ampl->Legs().size(); i++){
            ATOOLS::Cluster_Leg *leg = ampl->Legs().at(i);
            if (leg->Flav().IsGluon() && !leg->FromDec()) num_g++;
            if (abs(leg->Flav().Kfcode())<5 && !leg->FromDec()) num_q++;
            if (abs(leg->Flav().Kfcode())==6 && !leg->FromDec()) num_tops++;
        }


        if (m_nlo){
            if (num_tops < m_tops_in_proc){
                if ((num_q + num_g)>2) return true;
                else return false;
              }
            else{
                if ( (num_q + num_g)>1) return true;
                else return false;
             }
         }
        else{
            if (num_tops < m_tops_in_proc){
                if ((num_q >0) || (num_g>1)) return true;
                else return false;
              }
            else{
              if ((num_q >0) || (num_g>0)) return true;
              else return false;
            }
         }
      }


      size_t FindB(ATOOLS::Cluster_Amplitude *ampl){
        if (ampl==NULL) return 0;
        for (int i=2; i<ampl->Legs().size(); i++){
            ATOOLS::Cluster_Leg *leg = ampl->Legs().at(i);
            if ( abs(leg->Flav().Kfcode())==5 && !leg->FromDec()) return i;
          }
        return 0;
      }


      bool CheckHigherOrder (ATOOLS::Cluster_Amplitude *ampl){
        /*check for configurations as bb->ttbb or bb->bb which should not be vetoed,
         * since this of higher order than 4F ttbb.
         implemantation: redo veto, if bb-> x bb and in x is no particle of the 93 container.

         returns true, if such a config is found
        */
        msg_Debugging() << "Fusing, check higher order: " << ampl;
        if (ampl->Prev()) return false;
        for (int i =0; i < ampl->Legs().size(); i++ ){
          ATOOLS::Flavour flav = ampl->Leg(i)->Flav();
          if (i<2 && flav.Kfcode()!=5) return false;
          if (flav.Kfcode()<5 || flav.IsGluon()) return false;
        }
        return true;
      }




  void Finish() {
  }



};

DECLARE_GETTER(FUSING_Hook,"FUSING_HOOK",
               Userhook_Base,Userhook_Arguments);

Userhook_Base *ATOOLS::Getter<Userhook_Base,Userhook_Arguments,FUSING_Hook>::
operator()(const Userhook_Arguments &args) const
{
  return new FUSING_Hook(args);
}

void ATOOLS::Getter<Userhook_Base,Userhook_Arguments,FUSING_Hook>::
PrintInfo(std::ostream &str,const size_t width) const
{
  str<<"FUSING_UserHook";
}
"""

