model="LightVector"
mDM1 = 5.
mDM2 = 90.
mZp = 60.
mHD = 125.
widthZp = 2.864679e+01
widthN2 = 1.977182e+00
filteff = 1.387040e-02

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]
evgenConfig.minevents = 500

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
