######################################################################
# SM QCD diphoton with Sherpa (binned in m_gg)
######################################################################

include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "gamma gamma + 0,1,2,3 jets. At least two photons with pT > 20 GeV, with mass cut 100<Mgg<166 GeV"
evgenConfig.process = "QCD direct diphoton production"
evgenConfig.keywords = ["SM", "diphoton"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" , "leonardo.carminati@cern.ch" , "stefano.manzoni@cern.ch" ]
evgenConfig.inputconfcheck = "Sherpa_CT10_2DP20"
evgenConfig.minevents = 2000
evgenConfig.generators = ["Sherpa" ]

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])}

  % tags for process setup
  NJET:=3; QCUT:=10;
}(run)

(processes){
  Process 21 21 -> 22 22
  ME_Generator Internal;
  Loop_Generator gg_yy
  Scales VAR{Abs2(p[2]+p[3])};
  End process;

  Process 93 93 -> 22 22 93{NJET};
  Order_EW 2;
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]));
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {7,8};
  Integration_Error 0.03 {5,6,7,8};
  End process;
}(processes)

(selector){
  PT      22      18.0 E_CMS
  IsolationCut  22  0.3  2  0.025
  DeltaR 22 22 0.2 1000.0
  Mass    22  22  100.0 165.0
}(selector)
"""

######################################################################
## Filter
######################################################################

if not hasattr( filtSeq, "DirectPhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
    filtSeq += DirectPhotonFilter()
    pass

DirectPhotonFilter = filtSeq.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 20000.
DirectPhotonFilter.Etacut = 2.7
DirectPhotonFilter.NPhotons = 2

