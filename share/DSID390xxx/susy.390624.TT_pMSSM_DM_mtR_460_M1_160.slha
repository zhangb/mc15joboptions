#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90351768E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.05959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.80485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04199230E+01   # W+
        25     1.24563608E+02   # h
        35     4.00000194E+03   # H
        36     3.99999501E+03   # A
        37     4.00101341E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01279753E+03   # ~d_L
   2000001     4.01225939E+03   # ~d_R
   1000002     4.01214198E+03   # ~u_L
   2000002     4.01287955E+03   # ~u_R
   1000003     4.01279753E+03   # ~s_L
   2000003     4.01225939E+03   # ~s_R
   1000004     4.01214198E+03   # ~c_L
   2000004     4.01287955E+03   # ~c_R
   1000005     1.82863038E+03   # ~b_1
   2000005     4.01849561E+03   # ~b_2
   1000006     4.44702636E+02   # ~t_1
   2000006     1.84250247E+03   # ~t_2
   1000011     4.00168026E+03   # ~e_L
   2000011     4.00359705E+03   # ~e_R
   1000012     4.00056564E+03   # ~nu_eL
   1000013     4.00168026E+03   # ~mu_L
   2000013     4.00359705E+03   # ~mu_R
   1000014     4.00056564E+03   # ~nu_muL
   1000015     4.00410751E+03   # ~tau_1
   2000015     4.00894145E+03   # ~tau_2
   1000016     4.00316223E+03   # ~nu_tauL
   1000021     1.96370037E+03   # ~g
   1000022     1.44986950E+02   # ~chi_10
   1000023    -1.95720470E+02   # ~chi_20
   1000025     2.11352139E+02   # ~chi_30
   1000035     2.06413226E+03   # ~chi_40
   1000024     1.91786345E+02   # ~chi_1+
   1000037     2.06429610E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15221843E-01   # N_11
  1  2    -1.34909468E-02   # N_12
  1  3    -4.64237716E-01   # N_13
  1  4    -3.45998097E-01   # N_14
  2  1    -9.38611949E-02   # N_21
  2  2     2.64493066E-02   # N_22
  2  3    -6.95880853E-01   # N_23
  2  4     7.11505692E-01   # N_24
  3  1     5.71491522E-01   # N_31
  3  2     2.52121389E-02   # N_32
  3  3     5.47932826E-01   # N_33
  3  4     6.10353509E-01   # N_34
  4  1    -9.28550313E-04   # N_41
  4  2     9.99241100E-01   # N_42
  4  3    -1.67326865E-03   # N_43
  4  4    -3.89045161E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.37107970E-03   # U_11
  1  2     9.99997189E-01   # U_12
  2  1    -9.99997189E-01   # U_21
  2  2     2.37107970E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50322556E-02   # V_11
  1  2    -9.98484577E-01   # V_12
  2  1    -9.98484577E-01   # V_21
  2  2    -5.50322556E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.01039979E-01   # cos(theta_t)
  1  2     9.94882366E-01   # sin(theta_t)
  2  1    -9.94882366E-01   # -sin(theta_t)
  2  2    -1.01039979E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999743E-01   # cos(theta_b)
  1  2    -7.16937887E-04   # sin(theta_b)
  2  1     7.16937887E-04   # -sin(theta_b)
  2  2     9.99999743E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04444640E-01   # cos(theta_tau)
  1  2     7.09758937E-01   # sin(theta_tau)
  2  1    -7.09758937E-01   # -sin(theta_tau)
  2  2    -7.04444640E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00285221E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.03517682E+02  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44555768E+02   # higgs               
         4     1.61805831E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.03517682E+02  # The gauge couplings
     1     3.60965858E-01   # gprime(Q) DRbar
     2     6.35685471E-01   # g(Q) DRbar
     3     1.03766173E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.03517682E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.72773608E-07   # A_c(Q) DRbar
  3  3     2.05959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.03517682E+02  # The trilinear couplings
  1  1     2.10812075E-07   # A_d(Q) DRbar
  2  2     2.10832482E-07   # A_s(Q) DRbar
  3  3     3.75704127E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.03517682E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.67101764E-08   # A_mu(Q) DRbar
  3  3     4.71867438E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.03517682E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74355486E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.03517682E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84581608E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.03517682E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03562199E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.03517682E+02  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58309563E+07   # M^2_Hd              
        22    -2.81146668E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.80485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40182453E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.72536640E+01   # gluino decays
#          BR         NDA      ID1       ID2
     9.75464894E-03    2     1000005        -5   # BR(~g -> ~b_1  bb)
     9.75464894E-03    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.90245351E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     4.90245351E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.57046218E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.66067107E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.62205889E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.87172122E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.84015278E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.83495583E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.40450995E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.27137580E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.56546509E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.31074533E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.82226589E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.16936769E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.22432075E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.80572112E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.99360115E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.93996259E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.04179252E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.28427900E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.62745864E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.69327224E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77718970E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53942746E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.57543547E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63481506E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.02560731E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13413927E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.07203920E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.97256994E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.31147985E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.68102942E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74123856E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.76379259E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.69403873E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.57834073E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.84171651E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37841327E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.67108088E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.50985357E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57110738E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.75638396E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.96987743E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.84109308E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.65824217E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43528215E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74141786E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.50464957E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.73574771E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.10596494E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84655443E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.98481573E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.70326876E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51211949E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.50404381E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.80813913E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29766341E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.80719824E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.93931137E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85254889E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74123856E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.76379259E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.69403873E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.57834073E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.84171651E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37841327E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.67108088E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.50985357E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57110738E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.75638396E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.96987743E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.84109308E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.65824217E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43528215E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74141786E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.50464957E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.73574771E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.10596494E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84655443E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.98481573E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.70326876E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51211949E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.50404381E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.80813913E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29766341E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.80719824E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.93931137E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85254889E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11774474E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03933344E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.70125184E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.27504393E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77192866E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.77446101E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55747451E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06811869E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65241044E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.79956420E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25958925E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.66436930E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11774474E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03933344E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.70125184E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.27504393E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77192866E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.77446101E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55747451E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06811869E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65241044E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.79956420E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25958925E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.66436930E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08737876E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51681056E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16418548E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34887181E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38901508E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44032042E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78485196E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09471640E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44697714E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76036797E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.10489550E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41515391E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.19673028E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83726363E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11884984E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16713551E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.26332955E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.59307177E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77490615E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10869251E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53493095E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11884984E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16713551E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.26332955E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.59307177E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77490615E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10869251E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53493095E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45181596E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05524034E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95047717E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.15274255E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51122422E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.79779755E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00897666E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.98302789E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33592531E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33592531E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11198564E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11198564E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10417812E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.94531299E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.02889434E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.85501221E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.59998615E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.12983048E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.47511262E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.09040172E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.61237734E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07148993E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.77883996E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.77287186E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18209355E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53288459E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18209355E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53288459E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41023851E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51228339E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51228339E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47880819E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02155897E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02155897E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02155854E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.07099402E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.07099402E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.07099402E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.07099402E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.35700033E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.35700033E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.35700033E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.35700033E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.04387688E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.04387688E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.36975556E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.61653378E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.78954350E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.16049334E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.26144853E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.16049334E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.26144853E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.83028195E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.10315233E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.10315233E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.08950513E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.25138373E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.25138373E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.25137375E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.81190769E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.24026332E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.81190769E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.24026332E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.38617589E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.43009629E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.43009629E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.32801008E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.85817073E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.85817073E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.85817081E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.02215272E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.02215272E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.02215272E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.02215272E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.67402928E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.67402928E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.67402928E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.67402928E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.60731759E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.60731759E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.94390158E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.29420644E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.16973372E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.31067320E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.07896195E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.07896195E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.22960836E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.46644305E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.98355672E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.99225408E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.99225408E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30034630E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     2.30034630E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.92686762E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.92686762E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.92554217E-03   # h decays
#          BR         NDA      ID1       ID2
     6.11472200E-01    2           5        -5   # BR(h -> b       bb     )
     6.58602437E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.33149193E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.95195575E-04    2           3        -3   # BR(h -> s       sb     )
     2.15055552E-02    2           4        -4   # BR(h -> c       cb     )
     6.86038396E-02    2          21        21   # BR(h -> g       g      )
     2.36691138E-03    2          22        22   # BR(h -> gam     gam    )
     1.53161428E-03    2          22        23   # BR(h -> Z       gam    )
     2.02691797E-01    2          24       -24   # BR(h -> W+      W-     )
     2.52394937E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45692562E+01   # H decays
#          BR         NDA      ID1       ID2
     3.46215190E-01    2           5        -5   # BR(H -> b       bb     )
     6.07594029E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14830532E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54107521E-04    2           3        -3   # BR(H -> s       sb     )
     7.12772856E-08    2           4        -4   # BR(H -> c       cb     )
     7.14346792E-03    2           6        -6   # BR(H -> t       tb     )
     8.32875338E-07    2          21        21   # BR(H -> g       g      )
     2.29470389E-12    2          22        22   # BR(H -> gam     gam    )
     1.83654278E-09    2          23        22   # BR(H -> Z       gam    )
     1.88812630E-06    2          24       -24   # BR(H -> W+      W-     )
     9.43409247E-07    2          23        23   # BR(H -> Z       Z      )
     6.98028964E-06    2          25        25   # BR(H -> h       h      )
     9.26069906E-25    2          36        36   # BR(H -> A       A      )
     7.19307228E-20    2          23        36   # BR(H -> Z       A      )
     1.86201882E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64424377E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64424377E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.88213343E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56576227E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60429499E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.08529060E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.78489882E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.63214363E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62421944E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.71974946E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.13586824E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.38722495E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.16197420E-07    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     6.82326273E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     6.82326273E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     5.98182929E-08    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.45679710E+01   # A decays
#          BR         NDA      ID1       ID2
     3.46249300E-01    2           5        -5   # BR(A -> b       bb     )
     6.07612030E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14836728E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54156271E-04    2           3        -3   # BR(A -> s       sb     )
     7.17345925E-08    2           4        -4   # BR(A -> c       cb     )
     7.15681289E-03    2           6        -6   # BR(A -> t       tb     )
     1.47054480E-05    2          21        21   # BR(A -> g       g      )
     4.69459372E-08    2          22        22   # BR(A -> gam     gam    )
     1.61729103E-08    2          23        22   # BR(A -> Z       gam    )
     1.88429521E-06    2          23        25   # BR(A -> Z       h      )
     1.86324217E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64431883E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64431883E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.49577163E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94030460E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.29794242E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.65969759E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.55001366E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.49096766E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.82869775E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.24334470E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.75313658E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.17028231E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.17028231E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44420887E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.51636991E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09172022E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15388303E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.53047370E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22021558E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51037584E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.51581227E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.89075617E-06    2          24        25   # BR(H+ -> W+      h      )
     2.64783988E-14    2          24        36   # BR(H+ -> W+      A      )
     5.56212043E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46053039E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.33534402E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65022307E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.14805646E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60138986E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.92461075E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.45711283E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.08982761E-07    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
