m_dch = 1100.0

evgenConfig.description = "Doubly charged higgs ("+str(m_dch)+") in lepton mode."
evgenConfig.process = "DCH -> same sign 2lepton"
evgenConfig.keywords = ["BSM", "chargedHiggs" ,"2lepton"]
evgenConfig.contact = ["Katja Mankinen <katja.hannele.karppinen@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
"9900042:m0 = " + str(m_dch), # H++_R mass [GeV]
"9900041:m0 = " + str(m_dch), # H++_L mass [GeV]
"LeftRightSymmmetry:ffbar2HLHL=on", #HL pair production
"LeftRightSymmmetry:ffbar2HRHR=on", #HR pair production
# set the VEV value
"LeftRightSymmmetry:vL=0.0",
# set all couplings to leptons to 0.02
"LeftRightSymmmetry:coupHee=0.02",
"LeftRightSymmmetry:coupHmue=0.02",
"LeftRightSymmmetry:coupHmumu=0.02",
"LeftRightSymmmetry:coupHtaue=0.0",
"LeftRightSymmmetry:coupHtaumu=0.0",
"LeftRightSymmmetry:coupHtautau=0.0"]
