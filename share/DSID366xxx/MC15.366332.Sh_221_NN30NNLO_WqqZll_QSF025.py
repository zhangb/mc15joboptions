include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "W(->qq)Z(->ll) with 0,1j@NLO + 2,3j@LO, QSF down variation."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "2jet", "NLO", "systematic" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "WqqZvv_QSF025"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  NUM_ACCURACY=1e-6
  DIPOLE_AMIN=1.e-8

  % decay setup
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[23,11,-11]=2
  HDH_STATUS[23,13,-13]=2
  HDH_STATUS[23,15,-15]=2
}(run)

(processes){
  Process 93 93 -> 24 23 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;

  Process 93 93 -> -24 23 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[24]=0", "WIDTH[23]=0" ]

