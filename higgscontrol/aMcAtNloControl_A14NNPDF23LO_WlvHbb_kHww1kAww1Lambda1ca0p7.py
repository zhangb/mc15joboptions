from MadGraphControl.MadGraphUtils import *

mode=0

#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in HC model
#---------------------------------------------------------------------------------------------------
parameters={
	'frblock':{ 
		'Lambda':'1.000000e+03',
		'cosa':  '7.071067e-01',
		'kSM':   '0.000000e+00',
		'kHtt':  '1.000000e+00',
		'kAtt':  '0.000000e+00',
		'kHbb':  '1.000000e+00',
		'kAbb':  '0.000000e+00',
		'kHll':  '1.000000e+00',
		'kAll':  '0.000000e+00',
		'kHaa':  '0.000000e+00',
		'kAaa':  '0.000000e+00',
		'kHza':  '0.000000e+00',
		'kAza':  '0.000000e+00',
		'kHzz':  '0.000000e+00',
		'kAzz':  '0.000000e+00',
		'kHww':  '1.000000e+00',
		'kAww':  '1.000000e+00',
		'kHda':  '0.000000e+00',
		'kHdz':  '0.000000e+00',
		'kHdwR': '0.000000e+00',
		'kHdwI': '0.000000e+00'}
	}

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'3.0', 
           'pdlabel':"'lhapdf'",
           'lhaid':'90400',
           'PDF_set_min':'90401',
           'PDF_set_max':'90432',
           'reweight_PDF':'.true.',
           'parton_shower':'PYTHIA8',
           'bwcutoff':'15', 
           'ptj':'10',
           'ptl':'0',
           'etaj':'-1',
           'etal':'-1',
           'drll':'0',
           'maxjetflavor':'5',
           'ptgmin':'20'}

#---------------------------------------------------------------------------------------------------
# Generating Vh in HC model
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
        import model HC_NLO_X0_UFO
        define j = g u c d s u~ c~ d~ s~ b b~ 
        define l+ = e+ mu+ ta+
        define l- = e- mu- ta-
        generate    p p > x0 l+ vl [QCD]
        add process p p > x0 l- vl~ [QCD]
	output -f""")
fcard.close()
#else: 
#	raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
	beamEnergy = runArgs.ecmEnergy / 2.
else: 
	raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 1.1
nevents    = 10000*safefactor
if runArgs.maxEvents > 0:
	nevents=runArgs.maxEvents*safefactor

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------
process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for EFT parameters, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
runName='run_01'


build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat',masses=higgsMass,params=parameters)
print_cards()
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#runArgs.inputGeneratorFile="tmp_"+runName+'._00001.events.tar.gz'
if hasattr(runArgs,'outputTXTFile'):
        runArgs.inputGeneratorFile = runArgs.outputTXTFile
        evgenConfig.inputfilecheck = runArgs.outputTXTFile.split('.tar.gz')[0]
else:
        runArgs.inputGeneratorFile=tmp_+runName+'._00001.events.tar.gz'
        evgenConfig.inputfilecheck = runName
#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")


#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["aMcAtNlo", "Pythia8", "EvtGen"]
evgenConfig.description = "Vh 125 GeV Higgs production in the Higgs Characterization model. kSM=0, kHww=1, kAww=1, Lambda=1 TeV, cosalpha=1/sqrt2"
evgenConfig.keywords = ['Higgs','mH125','BSMHiggs','bbbar']

evgenConfig.contact = ['Paolo Francavilla <paolo.francavilla@cern.ch>']
#evgenConfig.inputfilecheck = runName
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",                                                                                                                  
                            "SpaceShower:pTmaxFudge = 1",                                                                                                                  
                            "SpaceShower:MEcorrections = off",                                                                                                             
                            "TimeShower:pTmaxMatch = 1",                                                                                                                   
                            "TimeShower:pTmaxFudge = 1",                                                                                                                   
                            "TimeShower:MEcorrections = off",                                                                                                              
                            "TimeShower:globalRecoil = on",                                                                                                                
                            "TimeShower:limitPTmaxGlobal = on",                                                                                                            
                            "TimeShower:nMaxGlobalRecoil = 1",                                                                                                             
                            "TimeShower:globalRecoilMode = 2",                                                                                                             
                            "TimeShower:nMaxGlobalBranch = 1.",                                                                                                            
                            "Check:epTolErr = 1e-2" ]
genSeq.Pythia8.Commands += ["25:onMode=off",
                            "25:onIfAny=5"]
