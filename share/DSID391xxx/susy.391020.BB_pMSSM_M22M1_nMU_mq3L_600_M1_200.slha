#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13385253E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03998037E+01   # W+
        25     1.25761513E+02   # h
        35     3.00014656E+03   # H
        36     2.99999990E+03   # A
        37     3.00109770E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02878189E+03   # ~d_L
   2000001     3.02347025E+03   # ~d_R
   1000002     3.02786803E+03   # ~u_L
   2000002     3.02607821E+03   # ~u_R
   1000003     3.02878189E+03   # ~s_L
   2000003     3.02347025E+03   # ~s_R
   1000004     3.02786803E+03   # ~c_L
   2000004     3.02607821E+03   # ~c_R
   1000005     6.86470621E+02   # ~b_1
   2000005     3.02350071E+03   # ~b_2
   1000006     6.83385521E+02   # ~t_1
   2000006     3.02128678E+03   # ~t_2
   1000011     3.00698493E+03   # ~e_L
   2000011     3.00052004E+03   # ~e_R
   1000012     3.00559435E+03   # ~nu_eL
   1000013     3.00698493E+03   # ~mu_L
   2000013     3.00052004E+03   # ~mu_R
   1000014     3.00559435E+03   # ~nu_muL
   1000015     2.98622541E+03   # ~tau_1
   2000015     3.02209055E+03   # ~tau_2
   1000016     3.00588609E+03   # ~nu_tauL
   1000021     2.33559078E+03   # ~g
   1000022     2.02077637E+02   # ~chi_10
   1000023     4.26222552E+02   # ~chi_20
   1000025    -3.00325874E+03   # ~chi_30
   1000035     3.00333621E+03   # ~chi_40
   1000024     4.26384803E+02   # ~chi_1+
   1000037     3.00423247E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891590E-01   # N_11
  1  2     1.90814305E-04   # N_12
  1  3    -1.47210996E-02   # N_13
  1  4    -2.45786110E-04   # N_14
  2  1     1.95877798E-04   # N_21
  2  2     9.99653622E-01   # N_22
  2  3     2.62252410E-02   # N_23
  2  4     2.19860983E-03   # N_24
  3  1    -1.02358284E-02   # N_31
  3  2     1.69899498E-02   # N_32
  3  3    -7.06827235E-01   # N_33
  3  4     7.07108075E-01   # N_34
  4  1     1.05828656E-02   # N_41
  4  2    -2.00982765E-02   # N_42
  4  3     7.06746621E-01   # N_43
  4  4     7.07102027E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99312323E-01   # U_11
  1  2     3.70793791E-02   # U_12
  2  1    -3.70793791E-02   # U_21
  2  2     9.99312323E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995172E-01   # V_11
  1  2    -3.10731339E-03   # V_12
  2  1    -3.10731339E-03   # V_21
  2  2    -9.99995172E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98614227E-01   # cos(theta_t)
  1  2    -5.26272328E-02   # sin(theta_t)
  2  1     5.26272328E-02   # -sin(theta_t)
  2  2     9.98614227E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99721404E-01   # cos(theta_b)
  1  2    -2.36032706E-02   # sin(theta_b)
  2  1     2.36032706E-02   # -sin(theta_b)
  2  2     9.99721404E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06926206E-01   # cos(theta_tau)
  1  2     7.07287310E-01   # sin(theta_tau)
  2  1    -7.07287310E-01   # -sin(theta_tau)
  2  2    -7.06926206E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00134550E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33852532E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44292536E+02   # higgs               
         4     1.10992505E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.33852532E+03  # The gauge couplings
     1     3.61770609E-01   # gprime(Q) DRbar
     2     6.38001355E-01   # g(Q) DRbar
     3     1.02889745E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33852532E+03  # The trilinear couplings
  1  1     1.68709697E-06   # A_u(Q) DRbar
  2  2     1.68712292E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33852532E+03  # The trilinear couplings
  1  1     5.98179814E-07   # A_d(Q) DRbar
  2  2     5.98251391E-07   # A_s(Q) DRbar
  3  3     1.23565345E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33852532E+03  # The trilinear couplings
  1  1     2.74388630E-07   # A_e(Q) DRbar
  2  2     2.74402223E-07   # A_mu(Q) DRbar
  3  3     2.78247674E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33852532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55706151E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33852532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.94225859E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33852532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02360630E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33852532E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.16514118E+04   # M^2_Hd              
        22    -9.10985861E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41294236E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.84608768E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48037837E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48037837E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51962163E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51962163E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.95147213E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.44410015E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.44724759E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.30834239E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19993533E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.46002280E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.74354161E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.51783113E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.94760421E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.89675378E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71280832E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62987608E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.20580342E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.67289206E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.27605294E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.11394983E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.55844488E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.58468991E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.95066650E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.29575452E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59707007E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.54428783E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.24513315E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.29454951E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.58914827E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.07846878E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.57268721E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.95932022E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.94978830E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61317635E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.17415091E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.68633169E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.22822287E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.24736591E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.09910259E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19497049E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57512274E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.86915532E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.44567393E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.72087585E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42487711E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96442513E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.92333311E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61229339E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.98966357E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.69701290E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.22252846E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.41625101E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.10594294E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67826939E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48691979E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.67180884E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.00155435E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.06264015E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55130798E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.95932022E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.94978830E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61317635E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.17415091E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.68633169E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.22822287E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.24736591E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.09910259E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19497049E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57512274E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.86915532E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.44567393E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.72087585E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42487711E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96442513E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.92333311E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61229339E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.98966357E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.69701290E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.22252846E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.41625101E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.10594294E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67826939E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48691979E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.67180884E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.00155435E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.06264015E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55130798E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89153302E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.97341137E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00275989E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.40149238E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.65748977E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99989894E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.88065124E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54803391E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.71798382E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.89153302E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.97341137E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00275989E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.40149238E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.65748977E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99989894E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.88065124E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54803391E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.71798382E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.74669632E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52323518E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16045493E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31630989E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69107736E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60454513E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13329353E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.24655049E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.83634212E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26193141E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.91041045E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89174672E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.95475803E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99975905E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.92133900E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.80570088E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00476514E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.95689128E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89174672E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.95475803E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99975905E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.92133900E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.80570088E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00476514E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.95689128E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89248407E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95385593E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99950582E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.96024392E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.12530795E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00510857E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.92628566E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.61902999E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.42355227E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61185904E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.07971862E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.89796523E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.88779565E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.74636349E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.11369182E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.03171061E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99929580E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.04203352E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.64483473E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.97303461E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.42895508E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.73013117E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.73013117E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.60715753E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.48830533E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33236361E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33236361E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.00851009E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.00851009E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.82298095E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.82298095E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.82298095E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.82298095E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.27814877E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.27814877E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.54494393E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.43168901E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.47903034E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.79119517E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.79119517E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.04101068E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.64797572E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.30712498E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.30712498E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.08189448E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.08189448E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.22400967E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.22400967E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.22400967E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.22400967E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     6.77047680E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.77047680E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.71745833E-03   # h decays
#          BR         NDA      ID1       ID2
     5.51123694E-01    2           5        -5   # BR(h -> b       bb     )
     7.01749468E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48417765E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.26646925E-04    2           3        -3   # BR(h -> s       sb     )
     2.28849628E-02    2           4        -4   # BR(h -> c       cb     )
     7.51452260E-02    2          21        21   # BR(h -> g       g      )
     2.59674418E-03    2          22        22   # BR(h -> gam     gam    )
     1.79827600E-03    2          22        23   # BR(h -> Z       gam    )
     2.44556202E-01    2          24       -24   # BR(h -> W+      W-     )
     3.09448836E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.95803102E+01   # H decays
#          BR         NDA      ID1       ID2
     9.05846597E-01    2           5        -5   # BR(H -> b       bb     )
     6.28296821E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.22150742E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.72801042E-04    2           3        -3   # BR(H -> s       sb     )
     7.64618011E-08    2           4        -4   # BR(H -> c       cb     )
     7.62755792E-03    2           6        -6   # BR(H -> t       tb     )
     5.70074756E-06    2          21        21   # BR(H -> g       g      )
     4.43720084E-08    2          22        22   # BR(H -> gam     gam    )
     3.12575117E-09    2          23        22   # BR(H -> Z       gam    )
     6.75813406E-07    2          24       -24   # BR(H -> W+      W-     )
     3.37490040E-07    2          23        23   # BR(H -> Z       Z      )
     5.07379476E-06    2          25        25   # BR(H -> h       h      )
    -1.01364387E-24    2          36        36   # BR(H -> A       A      )
     2.01948649E-18    2          23        36   # BR(H -> Z       A      )
     7.83878679E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.87605141E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.91816428E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.48275011E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.17212509E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.31741720E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.87263298E+01   # A decays
#          BR         NDA      ID1       ID2
     9.25825427E-01    2           5        -5   # BR(A -> b       bb     )
     6.42124865E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.27039690E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.78851885E-04    2           3        -3   # BR(A -> s       sb     )
     7.86880495E-08    2           4        -4   # BR(A -> c       cb     )
     7.84531777E-03    2           6        -6   # BR(A -> t       tb     )
     2.31037105E-05    2          21        21   # BR(A -> g       g      )
     6.08431530E-08    2          22        22   # BR(A -> gam     gam    )
     2.27541003E-08    2          23        22   # BR(A -> Z       gam    )
     6.88026343E-07    2          23        25   # BR(A -> Z       h      )
     8.57057790E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.02110852E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.28358043E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.61296592E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.22491702E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47592174E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.88798287E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08184713E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.44588757E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22344798E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51702590E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.28167873E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.31745077E-07    2          24        25   # BR(H+ -> W+      h      )
     4.96558013E-14    2          24        36   # BR(H+ -> W+      A      )
     4.72369259E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.70103912E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05218078E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
