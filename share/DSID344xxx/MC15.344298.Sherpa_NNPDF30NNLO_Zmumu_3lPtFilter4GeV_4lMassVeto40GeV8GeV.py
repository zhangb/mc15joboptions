include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0j@NLO + 1,2j@LO with 4l mass veto (m1(ll)>40 GeV and m2(ll)>8 GeV) and 3l filter (pT>4GeV)."
evgenConfig.keywords = ["SM", "Z", "2muon", "3lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_NNPDF30NNLO_Zmumu_4lMassFilter40GeV8GeV"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=2; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
}(run)

(processes){
  Process 93 93 -> 13 -13 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
}(selector)
"""

include("MC15JobOptions/MultiElecMuTauFilter.py")
filtSeq.MultiElecMuTauFilter.NLeptons  = 3
filtSeq.MultiElecMuTauFilter.MaxEta = 10.0
filtSeq.MultiElecMuTauFilter.MinPt = 4000.0
filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 10000.0

include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt = 4000.
filtSeq.FourLeptonMassFilter.MaxEta = 3.
filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu = True
filtSeq.FourLeptonMassFilter.AllowSameCharge = True

filtSeq.Expression = "(not FourLeptonMassFilter) and (MultiElecMuTauFilter)"

