#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954376E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03999612E+01   # W+
        25     1.25338730E+02   # h
        35     3.00010889E+03   # H
        36     2.99999983E+03   # A
        37     3.00093082E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03895856E+03   # ~d_L
   2000001     3.03374444E+03   # ~d_R
   1000002     3.03804009E+03   # ~u_L
   2000002     3.03486875E+03   # ~u_R
   1000003     3.03895856E+03   # ~s_L
   2000003     3.03374444E+03   # ~s_R
   1000004     3.03804009E+03   # ~c_L
   2000004     3.03486875E+03   # ~c_R
   1000005     9.47537064E+02   # ~b_1
   2000005     3.03282151E+03   # ~b_2
   1000006     9.43388205E+02   # ~t_1
   2000006     3.01680261E+03   # ~t_2
   1000011     3.00641488E+03   # ~e_L
   2000011     3.00203920E+03   # ~e_R
   1000012     3.00501193E+03   # ~nu_eL
   1000013     3.00641488E+03   # ~mu_L
   2000013     3.00203920E+03   # ~mu_R
   1000014     3.00501193E+03   # ~nu_muL
   1000015     2.98584855E+03   # ~tau_1
   2000015     3.02188587E+03   # ~tau_2
   1000016     3.00479891E+03   # ~nu_tauL
   1000021     2.34804518E+03   # ~g
   1000022     4.98945187E+01   # ~chi_10
   1000023     1.08551209E+02   # ~chi_20
   1000025    -2.99605920E+03   # ~chi_30
   1000035     2.99646759E+03   # ~chi_40
   1000024     1.08702992E+02   # ~chi_1+
   1000037     2.99717474E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886262E-01   # N_11
  1  2    -2.69590095E-03   # N_12
  1  3     1.48062207E-02   # N_13
  1  4    -9.85393386E-04   # N_14
  2  1     3.08380315E-03   # N_21
  2  2     9.99652389E-01   # N_22
  2  3    -2.60939362E-02   # N_23
  2  4     2.16766201E-03   # N_24
  3  1    -9.72392944E-03   # N_31
  3  2     1.69467388E-02   # N_32
  3  3     7.06818236E-01   # N_33
  3  4     7.07125331E-01   # N_34
  4  1    -1.11084836E-02   # N_41
  4  2     2.00160503E-02   # N_42
  4  3     7.06758702E-01   # N_43
  4  4    -7.07084221E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99316952E-01   # U_11
  1  2    -3.69544143E-02   # U_12
  2  1     3.69544143E-02   # U_21
  2  2     9.99316952E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995284E-01   # V_11
  1  2    -3.07110121E-03   # V_12
  2  1     3.07110121E-03   # V_21
  2  2     9.99995284E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98507237E-01   # cos(theta_t)
  1  2    -5.46195721E-02   # sin(theta_t)
  2  1     5.46195721E-02   # -sin(theta_t)
  2  2     9.98507237E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99912991E-01   # cos(theta_b)
  1  2     1.31913013E-02   # sin(theta_b)
  2  1    -1.31913013E-02   # -sin(theta_b)
  2  2     9.99912991E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06883201E-01   # cos(theta_tau)
  1  2     7.07330291E-01   # sin(theta_tau)
  2  1    -7.07330291E-01   # -sin(theta_tau)
  2  2     7.06883201E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01756488E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59543762E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44075737E+02   # higgs               
         4     7.59043586E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59543762E+03  # The gauge couplings
     1     3.62337213E-01   # gprime(Q) DRbar
     2     6.41825035E-01   # g(Q) DRbar
     3     1.02499408E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59543762E+03  # The trilinear couplings
  1  1     2.41827150E-06   # A_u(Q) DRbar
  2  2     2.41830703E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59543762E+03  # The trilinear couplings
  1  1     5.94711468E-07   # A_d(Q) DRbar
  2  2     5.94831549E-07   # A_s(Q) DRbar
  3  3     1.39661389E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59543762E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.31855620E-07   # A_mu(Q) DRbar
  3  3     1.33461146E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59543762E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51885037E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59543762E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11143738E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59543762E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04798541E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59543762E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.21303367E+04   # M^2_Hd              
        22    -9.05232968E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42992223E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.69260567E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47713693E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47713693E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52286307E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52286307E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.14458032E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.15297870E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.16222353E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.72247860E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13746523E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.81065639E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.14594089E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.30459792E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.83721466E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.93919936E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68800988E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61233136E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.16925951E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.12977849E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.28072543E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.42481766E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.44710980E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19046214E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.99069645E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.41792021E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.96822537E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.94874240E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.86508145E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.32042592E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.79924395E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.09718244E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.90460814E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.11426983E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.72235066E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.66188756E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.29699912E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.54886325E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.32217786E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.58289449E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.95871026E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17197313E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60528116E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.52462501E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.13274893E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.44822772E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39470334E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.11924331E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.05756020E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65739234E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.29960203E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.21082587E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.31639029E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.90087666E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.96563662E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66058122E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.57211199E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.34238414E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.04944267E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.89631738E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54278439E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.11426983E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.72235066E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.66188756E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.29699912E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.54886325E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.32217786E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.58289449E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.95871026E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17197313E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60528116E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.52462501E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.13274893E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.44822772E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39470334E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.11924331E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.05756020E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65739234E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.29960203E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.21082587E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.31639029E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.90087666E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.96563662E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66058122E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.57211199E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.34238414E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.04944267E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.89631738E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54278439E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07475995E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.53622739E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02382477E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.87697019E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.49283527E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02255213E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.11047296E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56699547E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990505E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.49230149E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.49893341E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.69834723E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07475995E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.53622739E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02382477E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.87697019E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.49283527E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02255213E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.11047296E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56699547E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990505E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.49230149E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.49893341E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.69834723E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84999364E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42910417E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19635245E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37454337E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78958445E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50926816E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16989742E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.30263880E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46565993E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32040857E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.49023894E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07512208E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.71475281E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00115817E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.38425292E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.73315246E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02736642E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.54672189E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07512208E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.71475281E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00115817E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.38425292E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.73315246E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02736642E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.54672189E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07517538E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.71393633E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00090506E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.13173677E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.40243072E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02768577E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.54168944E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.80984049E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35837547E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.27230288E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35837547E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.27230288E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09617401E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.42413826E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09617401E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.42413826E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09090085E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.14135974E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.59111484E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.10430868E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.41400054E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.34847444E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.25923997E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.19796057E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.56008928E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.38179235E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.18088397E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     5.77487449E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     7.32387914E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     5.77487449E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     7.32387914E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     7.04060697E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     6.25881704E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     6.25881704E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     6.58454947E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.01800724E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.01800724E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.01719251E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.18317348E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.32924477E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.24378398E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.13700751E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.13700751E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.04607875E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.10725219E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.58181021E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.58181021E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.18168382E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.18168382E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.67450752E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.67450752E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.07631949E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.03195571E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.08342309E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.23298812E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.23298812E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.38469014E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.44868521E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.55806450E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.55806450E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.21194887E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.21194887E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.09606107E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.09606107E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.86251096E-03   # h decays
#          BR         NDA      ID1       ID2
     6.66704185E-01    2           5        -5   # BR(h -> b       bb     )
     5.38160171E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.90509052E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.04110876E-04    2           3        -3   # BR(h -> s       sb     )
     1.74482327E-02    2           4        -4   # BR(h -> c       cb     )
     5.70541032E-02    2          21        21   # BR(h -> g       g      )
     1.96102941E-03    2          22        22   # BR(h -> gam     gam    )
     1.32446012E-03    2          22        23   # BR(h -> Z       gam    )
     1.78602348E-01    2          24       -24   # BR(h -> W+      W-     )
     2.24532261E-02    2          23        23   # BR(h -> Z       Z      )
     4.17785837E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.41257877E+01   # H decays
#          BR         NDA      ID1       ID2
     7.37249753E-01    2           5        -5   # BR(H -> b       bb     )
     1.76043043E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.22446131E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.64364766E-04    2           3        -3   # BR(H -> s       sb     )
     2.15633502E-07    2           4        -4   # BR(H -> c       cb     )
     2.15108270E-02    2           6        -6   # BR(H -> t       tb     )
     2.98831207E-05    2          21        21   # BR(H -> g       g      )
     2.06761689E-08    2          22        22   # BR(H -> gam     gam    )
     8.30058726E-09    2          23        22   # BR(H -> Z       gam    )
     2.94813763E-05    2          24       -24   # BR(H -> W+      W-     )
     1.47225116E-05    2          23        23   # BR(H -> Z       Z      )
     7.94817771E-05    2          25        25   # BR(H -> h       h      )
     2.46639642E-24    2          36        36   # BR(H -> A       A      )
     1.70164053E-18    2          23        36   # BR(H -> Z       A      )
     2.41523650E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13003584E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19944941E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.37114801E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.91854183E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.53006597E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32911021E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83626362E-01    2           5        -5   # BR(A -> b       bb     )
     1.87096137E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61526307E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12491678E-04    2           3        -3   # BR(A -> s       sb     )
     2.29273635E-07    2           4        -4   # BR(A -> c       cb     )
     2.28589288E-02    2           6        -6   # BR(A -> t       tb     )
     6.73173608E-05    2          21        21   # BR(A -> g       g      )
     4.27313379E-08    2          22        22   # BR(A -> gam     gam    )
     6.63004993E-08    2          23        22   # BR(A -> Z       gam    )
     3.12130827E-05    2          23        25   # BR(A -> Z       h      )
     2.62374062E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21846102E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30298301E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.97115318E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03016458E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09692328E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.41464871E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.53760889E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.02029801E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.01736915E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03223417E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13536639E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.03335906E-05    2          24        25   # BR(H+ -> W+      h      )
     8.93324624E-14    2          24        36   # BR(H+ -> W+      A      )
     2.02544658E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.92718922E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98925775E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
