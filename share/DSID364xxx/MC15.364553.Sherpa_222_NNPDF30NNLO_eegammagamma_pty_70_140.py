include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa eegammagamma + 0j@NLO + 1,2j@LO with 70<pT_y(lead)<140."
evgenConfig.keywords = ["SM", "2electron", "2photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Sherpa_222_NNPDF30NNLO_eegammagamma_pty_70_140"

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=2; LJET:=4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 22 22 11 -11 93{NJET}
  Order (*,4); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  PSI_ItMin 20000 {5}
  Integration_Error 0.99 {5}
  PSI_ItMin 50000 {6}
  Integration_Error 0.99 {6}
  End process
}(processes)

(selector){
  "PT"  22  70,140:7.0,E_CMS [PT_UP]
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO 22 22 0.2 1000
  Mass  11  -11  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=redlib1=5=redlib2=5=write_parameters=1" ]
