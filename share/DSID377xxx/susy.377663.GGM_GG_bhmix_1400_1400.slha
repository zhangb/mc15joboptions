#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.39100000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.40000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05412945E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401404E+03   # H
        36     2.00000000E+03   # A
        37     2.00150848E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994926E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994926E+03   # ~c_R
   1000005     4.99951283E+03   # ~b_1
   2000005     5.00064684E+03   # ~b_2
   1000006     4.98802385E+03   # ~t_1
   2000006     5.01640357E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007610E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007610E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99970597E+03   # ~tau_1
   2000015     5.00045292E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.34999440E+03   # ~chi_10
   1000023    -1.40008198E+03   # ~chi_20
   1000025     1.43728174E+03   # ~chi_30
   1000035     3.00380583E+03   # ~chi_40
   1000024     1.39630613E+03   # ~chi_1+
   1000037     3.00380317E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.27967307E-01   # N_11
  1  2    -3.21583542E-02   # N_12
  1  3     4.86025157E-01   # N_13
  1  4    -4.82502837E-01   # N_14
  2  1     3.12783627E-03   # N_21
  2  2    -3.52679373E-03   # N_22
  2  3    -7.06995409E-01   # N_23
  2  4    -7.07202425E-01   # N_24
  3  1     6.85603427E-01   # N_31
  3  2     3.60932696E-02   # N_32
  3  3    -5.12770264E-01   # N_33
  3  4     5.15472475E-01   # N_34
  4  1     1.32596605E-03   # N_41
  4  2    -9.98824648E-01   # N_42
  4  3    -3.16811341E-02   # N_43
  4  4     3.66588283E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.47692938E-02   # U_11
  1  2     9.98997353E-01   # U_12
  2  1     9.98997353E-01   # U_21
  2  2     4.47692938E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.18095777E-02   # V_11
  1  2     9.98656982E-01   # V_12
  2  1     9.98656982E-01   # V_21
  2  2     5.18095777E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07813896E-01   # cos(theta_t)
  1  2     7.06398959E-01   # sin(theta_t)
  2  1    -7.06398959E-01   # -sin(theta_t)
  2  2     7.07813896E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.72741171E-01   # cos(theta_b)
  1  2     7.39877907E-01   # sin(theta_b)
  2  1    -7.39877907E-01   # -sin(theta_b)
  2  2     6.72741171E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04224267E-01   # cos(theta_tau)
  1  2     7.09977592E-01   # sin(theta_tau)
  2  1    -7.09977592E-01   # -sin(theta_tau)
  2  2     7.04224267E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202004E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.40000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52194339E+02   # vev(Q)              
         4     3.36744585E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52764728E-01   # gprime(Q) DRbar
     2     6.27060405E-01   # g(Q) DRbar
     3     1.08619147E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02509162E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71727604E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79788174E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.39100000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     8.50858410E+05   # M^2_Hd              
        22    -7.29204115E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36997456E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.19378157E-06   # gluino decays
#          BR         NDA      ID1       ID2
     2.93861741E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.31113604E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.29380613E-06    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.31113604E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.29380613E-06    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.68893741E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     4.54889952E-14    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.54889952E-14    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.54889952E-14    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.54889952E-14    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.32563021E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.04824641E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.40386747E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.79254235E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.00334367E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.89424247E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.98693946E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.68708182E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.24631080E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.15615434E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.29137366E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.57464265E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.38849043E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.81223543E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.75512104E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.70219824E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.57722794E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.18920214E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.21949380E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.01472117E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.28743142E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.00756189E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.59731239E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.54120255E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.70045800E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.27419533E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.20806138E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.55960261E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.63136863E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.30888133E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.29186593E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.14953643E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43930723E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.25269646E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.68964065E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76807071E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.16252368E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.53642020E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.32608588E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02266353E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29145134E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.11402922E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.85651581E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83595781E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.33992902E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60499711E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43935079E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.89489849E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11358668E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.57669648E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.16611155E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.64073309E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.33127303E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02308399E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22361197E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.44642270E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.93563534E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.73002411E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.60498900E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89823445E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43930723E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.25269646E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.68964065E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76807071E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.16252368E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.53642020E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.32608588E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02266353E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29145134E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.11402922E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.85651581E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83595781E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.33992902E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60499711E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43935079E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.89489849E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11358668E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.57669648E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.16611155E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.64073309E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.33127303E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02308399E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22361197E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.44642270E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.93563534E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.73002411E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.60498900E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89823445E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.92895705E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.17300455E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77099958E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.99703632E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71673565E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.27536037E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.44348895E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.10706658E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.35190460E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.76327124E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.64798933E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.43776026E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.92895705E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.17300455E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77099958E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.99703632E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71673565E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.27536037E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.44348895E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.10706658E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.35190460E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.76327124E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.64798933E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.43776026E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.52254373E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78475345E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46527676E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.48830309E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.55994389E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.54521192E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.12565633E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.42583748E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.52203169E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.64031554E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.46053537E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.56109520E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59483914E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.77858562E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.19551100E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.92904753E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.11952455E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.58468606E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.86735322E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72435227E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.04697182E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.43875967E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.92904753E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.11952455E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.58468606E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.86735322E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72435227E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.04697182E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.43875967E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.93177755E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11848207E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.58321043E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.86095846E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72181540E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.97382194E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.43371014E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.34960244E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.23992947E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.26630838E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.25130659E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08877075E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08874265E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08087868E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.71811121E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53223384E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08828768E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46273880E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.38035796E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53630629E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.54280773E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.00944901E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.05266797E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.83892140E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.08410622E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.61477657E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.73986742E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.81157383E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.27038448E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.78760185E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.16381109E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.50696558E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15646414E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.50676903E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38193662E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.44168537E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.44156950E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.40899331E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.87325942E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.87325942E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.87325942E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.75107858E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.75107858E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.88981251E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.88981251E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.83692896E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.83692896E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.81418588E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.81418588E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.98302928E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.98302928E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
     7.62087059E-26    3     1000021        -2         2   # BR(~chi_20 -> ~g      ub      u)
     9.85212022E-26    3     1000021        -1         1   # BR(~chi_20 -> ~g      db      d)
     7.62087059E-26    3     1000021        -4         4   # BR(~chi_20 -> ~g      cb      c)
     9.85212022E-26    3     1000021        -3         3   # BR(~chi_20 -> ~g      sb      s)
#
#         PDG            Width
DECAY   1000025     1.96101164E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.23490282E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.87378578E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.36300333E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.23046195E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.56653496E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.67271414E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.01722111E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.29697231E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.02890141E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.24029471E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.65718501E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.65714280E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.05603509E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     9.76235934E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     9.76235934E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     9.76235934E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.25390275E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.91847909E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.23072574E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.91781666E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.50490327E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.66538457E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.66503643E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.56669776E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.33111094E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.33111094E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.33111094E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31230517E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31230517E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30457550E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30457550E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37434425E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37434425E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37419930E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37419930E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33370932E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33370932E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     7.28894233E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.86640669E-08    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     7.28894233E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.86640669E-08    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     1.41684906E-08    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.71812705E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.03915901E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51699117E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.30585063E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46456482E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46456482E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.11394271E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.31089278E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41340711E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80750964E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.73440988E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.18502080E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.83633534E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.32013874E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08522611E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17725411E-01    2           5        -5   # BR(h -> b       bb     )
     6.37557761E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25671966E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78219263E-04    2           3        -3   # BR(h -> s       sb     )
     2.06314122E-02    2           4        -4   # BR(h -> c       cb     )
     6.69903718E-02    2          21        21   # BR(h -> g       g      )
     2.30396699E-03    2          22        22   # BR(h -> gam     gam    )
     1.53768693E-03    2          22        23   # BR(h -> Z       gam    )
     2.00731445E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56200387E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78102068E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47074483E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427546E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71213121E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547080E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668098E-05    2           4        -4   # BR(H -> c       cb     )
     9.96064377E-01    2           6        -6   # BR(H -> t       tb     )
     7.97665366E-04    2          21        21   # BR(H -> g       g      )
     2.71209786E-06    2          22        22   # BR(H -> gam     gam    )
     1.16008854E-06    2          23        22   # BR(H -> Z       gam    )
     3.34622902E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66855113E-04    2          23        23   # BR(H -> Z       Z      )
     9.03381934E-04    2          25        25   # BR(H -> h       h      )
     7.45057011E-24    2          36        36   # BR(H -> A       A      )
     2.97399475E-11    2          23        36   # BR(H -> Z       A      )
     6.86637621E-12    2          24       -37   # BR(H -> W+      H-     )
     6.86637621E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382211E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47364697E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897758E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266681E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677413E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176637E-06    2           4        -4   # BR(A -> c       cb     )
     9.96996146E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675793E-04    2          21        21   # BR(A -> g       g      )
     3.16753595E-06    2          22        22   # BR(A -> gam     gam    )
     1.35266789E-06    2          23        22   # BR(A -> Z       gam    )
     3.26152728E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472454E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36337304E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237308E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143916E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50257013E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693720E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731543E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402307E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34268723E-04    2          24        25   # BR(H+ -> W+      h      )
     2.74304925E-13    2          24        36   # BR(H+ -> W+      A      )
