#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410189E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402292E+03   # H
        36     2.00000000E+03   # A
        37     2.00150754E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.99643628E+03   # ~d_L
   2000001     4.99632687E+03   # ~d_R
   1000002     4.99619166E+03   # ~u_L
   2000002     4.99624948E+03   # ~u_R
   1000003     4.99643628E+03   # ~s_L
   2000003     4.99632687E+03   # ~s_R
   1000004     4.99619166E+03   # ~c_L
   2000004     4.99624948E+03   # ~c_R
   1000005     4.99586671E+03   # ~b_1
   2000005     4.99689786E+03   # ~b_2
   1000006     5.11680914E+03   # ~t_1
   2000006     5.41102673E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.52199795E+03   # ~g
   1000022     1.28281409E+03   # ~chi_10
   1000023    -1.33137844E+03   # ~chi_20
   1000025     1.37001095E+03   # ~chi_30
   1000035     3.12827779E+03   # ~chi_40
   1000024     1.32800394E+03   # ~chi_1+
   1000037     3.12827591E+03   # ~chi_2+
   1000039     1.70150000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19463440E-01   # N_11
  1  2    -2.91871797E-02   # N_12
  1  3     4.92489877E-01   # N_13
  1  4    -4.88849863E-01   # N_14
  2  1     3.28643993E-03   # N_21
  2  2    -3.48262058E-03   # N_22
  2  3    -7.06988890E-01   # N_23
  2  4    -7.07208442E-01   # N_24
  3  1     6.94521731E-01   # N_31
  3  2     3.17765972E-02   # N_32
  3  3    -5.06787972E-01   # N_33
  3  4     5.09701642E-01   # N_34
  4  1     1.05997673E-03   # N_41
  4  2    -9.99062675E-01   # N_42
  4  3    -2.80424987E-02   # N_43
  4  4     3.29585642E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.96332222E-02   # U_11
  1  2     9.99214295E-01   # U_12
  2  1     9.99214295E-01   # U_21
  2  2     3.96332222E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.65866729E-02   # V_11
  1  2     9.98914252E-01   # V_12
  2  1     9.98914252E-01   # V_21
  2  2     4.65866729E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99975415E-01   # cos(theta_t)
  1  2     7.01208924E-03   # sin(theta_t)
  2  1    -7.01208924E-03   # -sin(theta_t)
  2  2     9.99975415E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68542179E-01   # cos(theta_b)
  1  2     7.43674226E-01   # sin(theta_b)
  2  1    -7.43674226E-01   # -sin(theta_b)
  2  2     6.68542179E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03860045E-01   # cos(theta_tau)
  1  2     7.10338678E-01   # sin(theta_tau)
  2  1    -7.10338678E-01   # -sin(theta_tau)
  2  2     7.03860045E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202975E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52154470E+02   # vev(Q)              
         4     3.43381785E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784071E-01   # gprime(Q) DRbar
     2     6.27183016E-01   # g(Q) DRbar
     3     1.08619150E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02524779E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71787605E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79805064E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29741243E+06   # M^2_Hd              
        22    -6.92395298E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37052210E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.01252564E-07   # gluino decays
#          BR         NDA      ID1       ID2
     3.55755854E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.98781505E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.08593284E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     8.09422023E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.33948100E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.81448584E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.19917352E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     4.06171436E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.19611213E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     4.60338122E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.33948100E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.81448584E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.19917352E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     4.06171436E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.19611213E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.60338122E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.33203156E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.71838870E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.25806050E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.85621751E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.85621751E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.85621751E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.85621751E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     4.68607173E-04    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     4.68607173E-04    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.10660530E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.96080328E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.02653066E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.47789533E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.46737126E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     5.71998280E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.89790834E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.07445781E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.41071469E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     6.39345552E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.81950178E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.73273472E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.00713241E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.91235665E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.52326326E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.80427625E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.12153586E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.12920530E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.06629030E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     7.38916248E-06    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.08560918E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.55767028E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.50263222E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.29604883E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.53732449E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.23514811E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.17096537E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01393296E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.35953805E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.56695100E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.64411721E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.23011954E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.55162752E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.70620951E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.53756368E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.38002320E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.09884608E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.10601737E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.36002704E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.80381662E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.67568195E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.79653471E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.96141326E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.00072637E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.92343238E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.08273987E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.23540951E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.14668034E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.43059255E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.96094934E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.98044876E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58923240E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.36007882E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.88030263E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.16057305E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.63464093E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.96422187E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.17190113E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.92789373E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.08316727E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.16658923E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.53728088E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.14285503E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.05819814E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.10867254E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89404402E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.36002704E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.80381662E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.67568195E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.79653471E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.96141326E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.00072637E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.92343238E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.08273987E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.23540951E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.14668034E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.43059255E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.96094934E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.98044876E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58923240E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.36007882E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.88030263E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.16057305E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.63464093E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.96422187E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.17190113E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.92789373E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.08316727E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.16658923E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.53728088E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.14285503E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.05819814E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.10867254E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89404402E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.71397034E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.87050152E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.66152230E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.10052533E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.66161933E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.95645372E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.33122404E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.14024906E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.22596698E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.07861423E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.77392034E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.81393127E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.71397034E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.87050152E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.66152230E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.10052533E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.66161933E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.95645372E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.33122404E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.14024906E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.22596698E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.07861423E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.77392034E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.81393127E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43205989E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.87470777E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.70192022E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.68063941E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.46772938E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.23476683E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.93987381E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     5.10843582E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43048710E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72141568E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.82650874E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.75619614E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.50339896E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.87989261E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.01127473E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.71405013E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18417413E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.76846634E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.94225439E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.66744369E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.70292632E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.32695063E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.71405013E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18417413E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.76846634E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.94225439E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.66744369E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.70292632E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.32695063E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.71682529E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18296453E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.76665990E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.93414160E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.66471897E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.72042806E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.32152140E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.18336876E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.85426591E-07    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33615458E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33615458E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11205281E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11205281E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10357836E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.84107070E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53102804E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12183880E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46284517E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34540575E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53888223E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.09103446E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.31284925E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.03681579E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00288776E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88533770E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11774538E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.37536100E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.53178937E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.61918738E-12    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.28871712E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.61458534E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18568462E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53525261E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18568462E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53525261E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40061661E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50612042E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50612042E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47133663E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00182997E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00182997E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00182997E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.17360413E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.17360413E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.17360413E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.17360413E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.91201400E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.91201400E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.91201400E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.91201400E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.87580124E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.87580124E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.15677080E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.89730543E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.86262207E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.43079813E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.02924371E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.60292075E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.05192636E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.24939596E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.05192636E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.24939596E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.37538173E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.16803314E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.16803314E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.89513753E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.12166601E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.12166601E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.12166601E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.43348226E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.15093500E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.43348226E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.15093500E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.73384907E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.19593666E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.19593666E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.09555648E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.43704187E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.43704187E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.43704187E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.32776894E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.32776894E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.32776894E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.32776894E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.42589008E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.42589008E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.42589008E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.42589008E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.38678377E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.38678377E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.84109627E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.54668357E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51374618E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.75426954E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46375890E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46375890E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14854816E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.60450213E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37784189E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.41280613E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.34943130E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.13682496E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.24989385E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.83465294E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21904598E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01941433E-01    2           5        -5   # BR(h -> b       bb     )
     6.22288106E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20262860E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66033606E-04    2           3        -3   # BR(h -> s       sb     )
     2.01055398E-02    2           4        -4   # BR(h -> c       cb     )
     6.63971940E-02    2          21        21   # BR(h -> g       g      )
     2.30692205E-03    2          22        22   # BR(h -> gam     gam    )
     1.62636776E-03    2          22        23   # BR(h -> Z       gam    )
     2.16631059E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80763770E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78104987E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47089685E-03    2           5        -5   # BR(H -> b       bb     )
     2.46426415E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71209124E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546497E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667997E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063624E-01    2           6        -6   # BR(H -> t       tb     )
     7.97765465E-04    2          21        21   # BR(H -> g       g      )
     2.71240886E-06    2          22        22   # BR(H -> gam     gam    )
     1.16000037E-06    2          23        22   # BR(H -> Z       gam    )
     3.34920355E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67003427E-04    2          23        23   # BR(H -> Z       Z      )
     9.03437979E-04    2          25        25   # BR(H -> h       h      )
     7.44300556E-24    2          36        36   # BR(H -> A       A      )
     3.00699019E-11    2          23        36   # BR(H -> Z       A      )
     7.00191541E-12    2          24       -37   # BR(H -> W+      H-     )
     7.00191541E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382359E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47380411E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897664E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266347E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677369E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176251E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995760E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675428E-04    2          21        21   # BR(A -> g       g      )
     3.17091887E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257450E-06    2          23        22   # BR(A -> Z       gam    )
     3.26378576E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472499E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36385677E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237161E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143397E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50287972E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693433E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731486E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402075E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34500000E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73451022E-13    2          24        36   # BR(H+ -> W+      A      )
