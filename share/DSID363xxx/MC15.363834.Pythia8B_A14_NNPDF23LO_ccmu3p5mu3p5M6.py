##############################################################
# Job options fragment for cc->mu3.5mu3.5X M>6
##############################################################
include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description = "cc->mu3.5mu3.5X Mmumu>6 production"
evgenConfig.keywords = ["charm","2muon","inclusive"]
evgenConfig.minevents = 50

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] 
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = False
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 4.0
genSeq.Pythia8B.AntiQuarkPtCut = 4.0
genSeq.Pythia8B.QuarkEtaCut = 3.
genSeq.Pythia8B.AntiQuarkEtaCut = 3.
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

# BR cc->mumu is approx 0.01, repeat hadronization 100 times to get approx 1 dimuon for each c-cbar event
genSeq.Pythia8B.NHadronizationLoops = 100

include("MC15JobOptions/Pythia8B_BPDGCodes.py")

# pythia8B inv mass filter does not work, use MassRangeFilter instead
#genSeq.Pythia8B.TriggerPDGCode = 13
#genSeq.Pythia8B.TriggerStatePtCut = [3.5]
#genSeq.Pythia8B.TriggerStateEtaCut = 2.5
#genSeq.Pythia8B.MinimumCountPerCut = [2]

include('MC15JobOptions/MassRangeFilter.py')
filtSeq.MassRangeFilter.PtCut = 3500.
filtSeq.MassRangeFilter.PtCut2 = 3500.
filtSeq.MassRangeFilter.EtaCut = 2.5
filtSeq.MassRangeFilter.EtaCut2 = 2.5
filtSeq.MassRangeFilter.InvMassMin = 6000.
filtSeq.MassRangeFilter.PartId = 13
filtSeq.MassRangeFilter.PartId2 = 13
filtSeq.MassRangeFilter.PartStatus = 1

# Print the output by DumpMC
#from TruthIO.TruthIOConf import DumpMC
#genSeq += DumpMC()


