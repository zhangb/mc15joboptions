include('MC15JobOptions/MadGraphControl_HC_LO_X2toZy.py')

evgenConfig.description = "Higgs Characterisation model LO Spin-2 qq->X2->Zgamma 400 GeV narrow width resonance in fully leptonic decay model"
evgenConfig.keywords = ["exotic", "Zgamma", "HiggsCharacterisation", "LO", "leptonic", "spin2"]
