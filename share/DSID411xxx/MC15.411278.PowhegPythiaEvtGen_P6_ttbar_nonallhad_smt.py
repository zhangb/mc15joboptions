#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, at least one lepton filter + soft muon filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','onofrio@liverpool.ac.uk' ]
evgenConfig.minevents = 5000

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  #PowhegConfig.topdecaymode = 22222
  PowhegConfig.decay_mode = "t t~ > all"
  PowhegConfig.hdamp        = 258.75
  #PowhegConfig.qmass = 172.5
  PowhegConfig.width_t = 1.320
  PowhegConfig.PDF          = 260000
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 15.
  #PowhegConfig.generateRunCard()
  #PowhegConfig.generateEvents()
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
   

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
#  include('MC15JobOptions/TTbarWToLeptonFilter.py')
#  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
#  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
  filtSeq.TTbarWToLeptonFilter.Ptcut = 20000.

  from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter

  filtSeq += TTbarWToLeptonFilter("onetWlep")
  filtSeq.onetWlep.NumLeptons = 1
  filtSeq.onetWlep.Ptcut = 0.

  filtSeq += TTbarWToLeptonFilter("twotWlep")
  filtSeq.twotWlep.NumLeptons = 2
  filtSeq.twotWlep.Ptcut = 0.

  from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
  filtSeq += ParentChildFilter("Wmu")
  filtSeq.Wmu.PDGParent  = [24]
  filtSeq.Wmu.PDGChild = [13]

  filtSeq += ParentChildFilter("Wetau")
  filtSeq.Wetau.PDGParent  = [24]
  filtSeq.Wetau.PDGChild = [11,15]

  from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
  filtSeq += MultiMuonFilter("OneMuonFilter")
  filtSeq.OneMuonFilter.Ptcut = 3250.
  filtSeq.OneMuonFilter.Etacut = 2.8
  filtSeq.OneMuonFilter.NMuons = 1

  filtSeq += MultiMuonFilter("TwoMuonsFilter")
  filtSeq.TwoMuonsFilter.Ptcut = 3250.
  filtSeq.TwoMuonsFilter.Etacut = 2.8
  filtSeq.TwoMuonsFilter.NMuons = 2

  filtSeq += MultiMuonFilter("ThreeMuonsFilter")
  filtSeq.ThreeMuonsFilter.Ptcut = 3250.
  filtSeq.ThreeMuonsFilter.Etacut = 2.8
  filtSeq.ThreeMuonsFilter.NMuons = 3

  from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
  filtSeq += ElectronFilter("ElectronFilter")
  filtSeq.ElectronFilter.Ptcut = 20000.
  filtSeq.ElectronFilter.Etacut = 2.8

  from GeneratorFilters.GeneratorFiltersConf import MuonFilter
  filtSeq += MuonFilter("MuonFilter")
  filtSeq.MuonFilter.Ptcut = 20000.
  filtSeq.MuonFilter.Etacut = 2.8

  filtSeq.Expression="TTbarWToLeptonFilter and (ElectronFilter or MuonFilter) and ((onetWlep and ((Wmu and TwoMuonsFilter) or (Wetau and OneMuonFilter))) or (twotWlep and ( ((not Wmu) and OneMuonFilter) or (Wmu and Wetau and TwoMuonsFilter) or (Wmu and (not Wetau) and ThreeMuonsFilter) )) )"

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')
