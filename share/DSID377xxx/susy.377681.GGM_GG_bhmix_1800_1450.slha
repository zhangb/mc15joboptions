#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413790E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401363E+03   # H
        36     2.00000000E+03   # A
        37     2.00151127E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994926E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994926E+03   # ~c_R
   1000005     4.99949288E+03   # ~b_1
   2000005     5.00066678E+03   # ~b_2
   1000006     4.98751283E+03   # ~t_1
   2000006     5.01691417E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.40098567E+03   # ~chi_10
   1000023    -1.45008040E+03   # ~chi_20
   1000025     1.48816891E+03   # ~chi_30
   1000035     3.00392582E+03   # ~chi_40
   1000024     1.44618518E+03   # ~chi_1+
   1000037     3.00392289E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19596749E-01   # N_11
  1  2    -3.36078096E-02   # N_12
  1  3     4.92117414E-01   # N_13
  1  4    -4.88744806E-01   # N_14
  2  1     3.01757472E-03   # N_21
  2  2    -3.48703725E-03   # N_22
  2  3    -7.07001240E-01   # N_23
  2  4    -7.07197272E-01   # N_24
  3  1     6.94384199E-01   # N_31
  3  2     3.68758514E-02   # N_32
  3  3    -5.06846646E-01   # N_33
  3  4     5.09487225E-01   # N_34
  4  1     1.41318429E-03   # N_41
  4  2    -9.98748480E-01   # N_42
  4  3    -3.28051065E-02   # N_43
  4  4     3.77266565E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.63554594E-02   # U_11
  1  2     9.98925008E-01   # U_12
  2  1     9.98925008E-01   # U_21
  2  2     4.63554594E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.33161168E-02   # V_11
  1  2     9.98577684E-01   # V_12
  2  1     9.98577684E-01   # V_21
  2  2     5.33161168E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07789274E-01   # cos(theta_t)
  1  2     7.06423629E-01   # sin(theta_t)
  2  1    -7.06423629E-01   # -sin(theta_t)
  2  2     7.07789274E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73937865E-01   # cos(theta_b)
  1  2     7.38788031E-01   # sin(theta_b)
  2  1    -7.38788031E-01   # -sin(theta_b)
  2  2     6.73937865E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04328324E-01   # cos(theta_tau)
  1  2     7.09874364E-01   # sin(theta_tau)
  2  1    -7.09874364E-01   # -sin(theta_tau)
  2  2     7.04328324E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90204203E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52209284E+02   # vev(Q)              
         4     3.31970177E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52758913E-01   # gprime(Q) DRbar
     2     6.27023625E-01   # g(Q) DRbar
     3     1.08213390E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02531231E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71651555E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781112E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.91146650E+05   # M^2_Hd              
        22    -7.38245059E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36981026E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.59965775E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.79356538E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.12900883E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.57649090E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.52672108E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     8.20002843E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.13844643E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.52798949E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.32554664E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.39999540E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.52672108E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     8.20002843E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.13844643E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.52798949E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.32554664E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.39999540E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.53820020E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     5.16081611E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.38300551E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.21765632E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.00107866E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     7.16259324E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.16259324E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.16259324E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.16259324E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.26541596E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.26541596E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.10687002E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.55686107E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.94879068E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.90826983E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.06971065E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.04812039E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.11709829E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.49180656E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.00949278E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.45731516E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.88814499E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.96136982E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.50457448E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.36366508E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.98413493E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.48407955E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.35743167E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.45923582E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.81137488E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.24160700E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.40637718E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.08475443E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.83866582E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.42295171E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.48068628E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.39106976E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.86127007E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.79810529E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.77728942E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.40953855E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.58820541E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.00103409E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22351937E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.40593030E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.07932932E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.96982175E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.46959970E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.05435794E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.94052424E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.92782302E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.07443890E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.25226207E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.91270486E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.05170959E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.19196013E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.56959850E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22357457E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.04892318E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.17031698E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.14726497E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.47373961E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.06495913E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.94626371E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.92828651E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00752446E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.81845439E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.01080172E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.30035445E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.08297489E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88881079E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22351937E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.40593030E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.07932932E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.96982175E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.46959970E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.05435794E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.94052424E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.92782302E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.07443890E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.25226207E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.91270486E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.05170959E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.19196013E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.56959850E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22357457E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.04892318E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.17031698E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.14726497E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.47373961E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.06495913E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.94626371E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.92828651E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00752446E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.81845439E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.01080172E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.30035445E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.08297489E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88881079E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.92196718E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.83148301E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.79746653E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01610819E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72198603E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.41501189E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45458939E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08016270E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.23307460E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.09078130E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76682479E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.70705356E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.92196718E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.83148301E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.79746653E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01610819E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72198603E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.41501189E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45458939E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08016270E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.23307460E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.09078130E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76682479E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.70705356E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.50562333E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.70309267E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45626916E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.53789567E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.57006415E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.67853143E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.14627048E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.43544695E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50495670E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.56059354E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40230752E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61066175E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60467010E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.12590843E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21554640E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.92207083E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09230821E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.50891030E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.95705262E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.73014227E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19440955E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.44974927E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.92207083E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09230821E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.50891030E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.95705262E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.73014227E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19440955E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.44974927E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92476695E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09130129E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.50751935E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.95063944E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72762556E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.11169322E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44474152E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22360927E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.94752186E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.24305389E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.22739975E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08101922E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08098990E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07278506E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.72298155E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53265586E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11771294E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46263650E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35136953E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53554982E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.53388218E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.54047889E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95905712E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92842655E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12516327E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.49673501E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.72939278E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.17575622E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.35767470E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.30343820E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.15800672E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.49946034E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15046258E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.49925743E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37051095E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.42459400E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.42447546E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.39114050E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.83915823E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.83915823E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.83915823E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.20664144E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.20664144E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.16823001E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.16823001E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.35547192E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.35547192E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.32853482E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.32853482E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.67706151E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.67706151E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18673947E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.77343359E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.82208724E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.49278236E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.30078622E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.72489037E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.03531065E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22797875E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.44128558E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.23557597E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.67418946E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.11920767E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.11918352E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.33016114E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.02297670E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.02297670E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.02297670E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.23441092E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.89326028E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.21234170E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.89263216E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.50037941E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.60788315E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.60755051E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.51364028E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.31963388E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.31963388E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.31963388E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31133789E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31133789E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30398413E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30398413E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37111987E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37111987E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37098200E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37098200E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33245513E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33245513E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.72299247E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.71062751E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51810275E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54810664E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46485733E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46485733E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14406738E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20312139E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38374992E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80478633E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.72819214E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.43272524E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.35020531E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.47339502E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08609072E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17808826E-01    2           5        -5   # BR(h -> b       bb     )
     6.37427040E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25625696E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78121153E-04    2           3        -3   # BR(h -> s       sb     )
     2.06269859E-02    2           4        -4   # BR(h -> c       cb     )
     6.69758683E-02    2          21        21   # BR(h -> g       g      )
     2.30338892E-03    2          22        22   # BR(h -> gam     gam    )
     1.53736649E-03    2          22        23   # BR(h -> Z       gam    )
     2.00686497E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56146172E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78104273E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46935749E-03    2           5        -5   # BR(H -> b       bb     )
     2.46425333E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71205296E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546075E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668154E-05    2           4        -4   # BR(H -> c       cb     )
     9.96064921E-01    2           6        -6   # BR(H -> t       tb     )
     7.97663787E-04    2          21        21   # BR(H -> g       g      )
     2.71270802E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010530E-06    2          23        22   # BR(H -> Z       gam    )
     3.35290210E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67187860E-04    2          23        23   # BR(H -> Z       Z      )
     9.03228206E-04    2          25        25   # BR(H -> h       h      )
     7.45790584E-24    2          36        36   # BR(H -> A       A      )
     2.97246467E-11    2          23        36   # BR(H -> Z       A      )
     6.82260780E-12    2          24       -37   # BR(H -> W+      H-     )
     6.82260780E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381937E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47228509E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897932E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62267298E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677495E-06    2           3        -3   # BR(A -> s       sb     )
     9.96177349E-06    2           4        -4   # BR(A -> c       cb     )
     9.96996859E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676468E-04    2          21        21   # BR(A -> g       g      )
     3.16289999E-06    2          22        22   # BR(A -> gam     gam    )
     1.35269733E-06    2          23        22   # BR(A -> Z       gam    )
     3.26805492E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472469E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36039369E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237645E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145109E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50066334E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45694356E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731670E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401639E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34938891E-04    2          24        25   # BR(H+ -> W+      h      )
     2.76850368E-13    2          24        36   # BR(H+ -> W+      A      )
