include('MC15JobOptions/MadGraphControl_HC_LO_X2toZy.py')

evgenConfig.description = "Higgs Characterisation model LO Spin-2 gg->X2->Zgamma 650 GeV narrow width resonance in fully hadronic decay model"
evgenConfig.keywords = ["exotic", "Zgamma", "HiggsCharacterisation", "LO", "allHadronic", "spin2"]
