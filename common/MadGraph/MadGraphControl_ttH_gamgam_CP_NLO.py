from MadGraphControl.MadGraphUtils import *

######
## number of events to generate + safety margin
nevents=1.1*runArgs.maxEvents
runName='run_01'
###generate events
#gridpack_dir=None
#gridpack_mode=False
#cluster_type='condor'
#cluster_queue='tomorrow'
#mode=0
#njobs=1
###gridpack production
gridpack_dir='madevent/'
gridpack_mode=False
mode=2 #mode=0 for single-core run, mode=1 if access to acluster, mode=2 for multicore production
njobs=132
cluster_type='lsf'
cluster_queue='8nh'
#mode=1#NO
#njobs=20#NO
#cluster_type='lsf'#NO
#cluster_queue='1nd'#NO


######

## map DSID to process settings
### DSID lists (extensions can include systematics samples)
ttH_CP   = [346303, 346304, 346595, 346596, 346597, 346598, 346798]

# select any BSM top Yukawa couplings (default: SM):
ttH_CPalpha_0 = [ 346798 ]
ttH_CPalpha_15 = [ 346595 ]
ttH_CPalpha_30 = [ 346596 ]
ttH_CPalpha_45 = [ 346304 ]
ttH_CPalpha_60 = [ 346597 ]
ttH_CPalpha_75 = [ 346598 ]
ttH_CPalpha_90 = [ 346303 ]

######
mgproc="generate p p > t t~ x0 [QCD]"
name='ttbarH_CP'
process="pp>tt~x0"  
topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- >  all all'''
nevents=5300
#gridpack_mode=True
gridpack_mode=False#MARIA
gridpack_dir='madevent/'
    
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define q = u c d s b t
define q~ = u~ c~ d~ s~ b~ t~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
"""+mgproc+"""
output -f
""")
fcard.close()
    
extras = {'pdlabel'        : "'lhapdf'",
          'lhaid'          : 260000,
          'parton_shower'  :'PYTHIA8',
          'reweight_scale' : 'True',
          'reweight_PDF'   : 'True',
          'PDF_set_min'    : 260001,
          'PDF_set_max'    : 260100,
          'bwcutoff'       : 50.,
          'fixed_ren_scale' : "False",
	  'fixed_fac_scale' : "False",
          'dynamical_scale_choice' : 3 }


parameters_cosa_1={
    'frblock':{
        'Lambda':'1.000000e+03',
        'cosa':  '1.000000e+00',
        'kSM':   '1.000000e+00',
        'kHtt':  '1.000000e+00',
        'kAtt':  '0.000000e+00'}
}

parameters = parameters_cosa_1

runName = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)


build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)


######
## write madspin card
madspin_dir = 'my_madspin'
madspin_card_loc='madspin_card.dat'
if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open('madspin_card.dat','w')
    fMadSpinCard.write('import Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+madspin_dir+'\n')
    fMadSpinCard.write('#set use_old_dir True\n')
else:
    os.unlink(gridpack_dir+'Cards/madspin_card.dat')
    fMadSpinCard = open(gridpack_dir+'Cards/madspin_card.dat','w')
    fMadSpinCard.write('import '+gridpack_dir+'Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+gridpack_dir+'MadSpin\n')
    fMadSpinCard.write('set ms_dir '+madspin_dir+'\n')
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
# for these numbers I get negligible(<1) amount of events above weights / 10k decayed events
fMadSpinCard.write('''set Nevents_for_max_weigth 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()
    


######
## select param card; default/None = SM
#param_card_loc=None
build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat',params=parameters)

if runArgs.runNumber not in ttH_CP:
    generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001)

# run with modified param card
else:

    # BSM case: run with modified param card
    mod_paramcard_name = ''
    if runArgs.runNumber in ttH_CPalpha_0:
        mod_paramcard_name = 'param_card_CPalpha_0.dat'
    elif runArgs.runNumber in ttH_CPalpha_15:
        mod_paramcard_name = 'param_card_CPalpha_15.dat'
    elif runArgs.runNumber in ttH_CPalpha_30:
        mod_paramcard_name = 'param_card_CPalpha_30.dat'
    elif runArgs.runNumber in ttH_CPalpha_45:
        mod_paramcard_name = 'param_card_CPalpha_45.dat'
    elif runArgs.runNumber in ttH_CPalpha_60:
        mod_paramcard_name = 'param_card_CPalpha_60.dat'
    elif runArgs.runNumber in ttH_CPalpha_75:
        mod_paramcard_name = 'param_card_CPalpha_75.dat'
    elif runArgs.runNumber in ttH_CPalpha_90:
        mod_paramcard_name = 'param_card_CPalpha_90.dat'
    else:
        raise RuntimeError("No modified param card instuction found for %i ."%runArgs.runNumber)

    mod_paramcard = subprocess.Popen(['get_files','-data',mod_paramcard_name]).communicate()
    if not os.access(mod_paramcard_name, os.R_OK):
        print 'ERROR: Could not get param card'
        raise RuntimeError("parameter card '%s' missing!"%mod_paramcard_name)
    param_card_loc=mod_paramcard_name

    generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,
         madspin_card_loc=madspin_card_loc,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001)


###### 
outputDS=arrange_output(run_name=runName,proc_dir=process_dir,lhe_version=3,saveProcDir=True)



###### 
evgenConfig.description = 'aMcAtNlo ttH'
evgenConfig.keywords+=['ttHiggs','ttbar','Higgs','BSM']
evgenConfig.contact = ['maria.moreno.llacer@cern.ch']
evgenConfig.inputfilecheck = outputDS
runArgs.inputGeneratorFile=outputDS


###### 
## shower settings: if you change these to another shower eg Hw++,
## make sure you update subtraction term ('parton_shower'  :'PYTHIA8') 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py") 
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")
## don't include shower weights, see ATLMCPROD-6135
#include("MC15JobOptions/Pythia8_ShowerWeights.py")


