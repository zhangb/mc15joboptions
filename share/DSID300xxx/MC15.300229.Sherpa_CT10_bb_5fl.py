include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa bb + jet"
evgenConfig.keywords = ["SM"]
evgenConfig.contact  = [ "gavin.hesketh@ucl.ac.uk" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "bb"

evgenConfig.process="""
(run){
    EVENTS 2000
    MI_HANDLER=Amisic;
    BEAM_1 2212; BEAM_ENERGY_1=E_CMS/2;
    BEAM_2 2212; BEAM_ENERGY_2=E_CMS/2;

}(run)

(processes){
    Process 93 93 -> 93 93 93{1}
    Order_EW 0;
    Integration_Error 0.02;
    CKKW sqr(13./E_CMS);
    End process;

}(processes)

(selector){
    NJetFinder 1 10.0 2.7 0.2
    PT 5 13.0 E_CMS
    PT -5 13.0 E_CMS
    PseudoRapidity 5 -2.6 2.6
    PseudoRapidity -5 -2.6 2.6
}(selector)
"""
from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
HeavyFlavorBHadronFilter = HeavyFlavorHadronFilter(name="HeavyFlavorBHadronFilter")
HeavyFlavorBHadronFilter.RequestBottom=True
HeavyFlavorBHadronFilter.RequestCharm=False
HeavyFlavorBHadronFilter.Request_cQuark=False
HeavyFlavorBHadronFilter.Request_bQuark=False
#HeavyFlavorBHadronFilter.RequestSpecificPDGID=False
#HeavyFlavorBHadronFilter.RequireTruthJet=False
HeavyFlavorBHadronFilter.BottomPtMin=15.*GeV
HeavyFlavorBHadronFilter.BottomEtaMax=2.6

genSeq += HeavyFlavorBHadronFilter
#StreamEVGEN.RequireAlgs += [ "HeavyFlavorBHadronFilter" ]
