#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419678E+01   # W+
        25     1.25000000E+02   # h
        35     2.00399683E+03   # H
        36     2.00000000E+03   # A
        37     2.00151866E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994923E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994923E+03   # ~c_R
   1000005     4.99933265E+03   # ~b_1
   2000005     5.00082698E+03   # ~b_2
   1000006     4.98344407E+03   # ~t_1
   2000006     5.02095566E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     1.79827728E+03   # ~chi_10
   1000023    -1.85007025E+03   # ~chi_20
   1000025     1.88553251E+03   # ~chi_30
   1000035     3.00526046E+03   # ~chi_40
   1000024     1.84484589E+03   # ~chi_1+
   1000037     3.00525326E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.30160584E-01   # N_11
  1  2    -4.39670610E-02   # N_12
  1  3     4.83512663E-01   # N_13
  1  4    -4.80778456E-01   # N_14
  2  1     2.36659468E-03   # N_21
  2  2    -3.19866953E-03   # N_22
  2  3    -7.07034052E-01   # N_23
  2  4    -7.07168309E-01   # N_24
  3  1     6.83266790E-01   # N_31
  3  2     5.06939305E-02   # N_32
  3  3    -5.14080676E-01   # N_33
  3  4     5.16040384E-01   # N_34
  4  1     2.53261580E-03   # N_41
  4  2    -9.97740844E-01   # N_42
  4  3    -4.51598557E-02   # N_43
  4  4     4.96727473E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.37750447E-02   # U_11
  1  2     9.97964300E-01   # U_12
  2  1     9.97964300E-01   # U_21
  2  2     6.37750447E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.01550404E-02   # V_11
  1  2     9.97536100E-01   # V_12
  2  1     9.97536100E-01   # V_21
  2  2     7.01550404E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07641383E-01   # cos(theta_t)
  1  2     7.06571775E-01   # sin(theta_t)
  2  1    -7.06571775E-01   # -sin(theta_t)
  2  2     7.07641383E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81189955E-01   # cos(theta_b)
  1  2     7.32106717E-01   # sin(theta_b)
  2  1    -7.32106717E-01   # -sin(theta_b)
  2  2     6.81189955E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954851E-01   # cos(theta_tau)
  1  2     7.09252182E-01   # sin(theta_tau)
  2  1    -7.09252182E-01   # -sin(theta_tau)
  2  2     7.04954851E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199848E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305885E+02   # vev(Q)              
         4     3.09656856E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52717963E-01   # gprime(Q) DRbar
     2     6.26764184E-01   # g(Q) DRbar
     3     1.07892770E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02490467E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71370480E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738250E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.85811321E+05   # M^2_Hd              
        22    -8.54453122E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36865188E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.44335290E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.29296930E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.91201704E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     7.66311949E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.36389301E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.16137028E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.90155291E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     3.90831755E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.41632245E-08    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.19301933E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.36389301E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.16137028E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.90155291E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     3.90831755E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.41632245E-08    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.19301933E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.36871500E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.50970874E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.02436523E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.67427734E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     4.88936467E-06    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.41742735E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.41742735E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.41742735E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.41742735E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.11480717E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.11480717E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.74403048E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.47291345E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.00078485E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.07542721E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.14995988E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.07334957E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.25911232E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.43012430E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.63654864E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.33468439E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.94448854E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.12087151E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.80163091E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.21057995E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.55444724E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.40332975E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.08111441E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.68431690E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.97076594E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.05194569E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.64244637E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.13171770E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.33569885E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.30230808E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.16783003E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.68956519E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.80731060E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.71118615E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.97389897E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.38099595E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.00985137E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.96564078E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96476774E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.29058982E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.42419958E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.25897356E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.90940800E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     7.04853318E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.82309769E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.79081515E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.80658921E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.37428729E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.45137780E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.01846787E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.54390262E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.56072049E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96484248E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.38109672E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.48465867E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.23867875E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.91732347E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.82505712E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.83073960E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.79130951E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74712145E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.13790524E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.33720016E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.21806162E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.99133053E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88643930E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96476774E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.29058982E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.42419958E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.25897356E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.90940800E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     7.04853318E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.82309769E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.79081515E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.80658921E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.37428729E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.45137780E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.01846787E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.54390262E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.56072049E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96484248E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.38109672E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.48465867E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.23867875E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.91732347E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.82505712E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.83073960E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.79130951E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74712145E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.13790524E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.33720016E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.21806162E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.99133053E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88643930E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.86272187E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.96597539E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77189872E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.51196452E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76413836E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.14315325E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.54661840E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.85052422E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.40533990E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.58034277E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.59456929E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.50020949E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.86272187E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.96597539E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77189872E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.51196452E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76413836E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.14315325E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.54661840E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.85052422E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.40533990E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.58034277E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.59456929E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.50020949E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36118321E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.60923246E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.30074961E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.34503931E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.65875745E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25842628E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.32856267E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.52309456E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35960387E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47807256E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.90461807E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.41603951E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.69370499E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.76102434E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.39851730E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.86293491E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07030967E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04339226E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.59545349E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77933471E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.01280536E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54057787E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.86293491E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07030967E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04339226E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.59545349E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77933471E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.01280536E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54057787E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86532783E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06941583E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.04252090E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.59078057E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77701361E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.84087121E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.53597954E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.49289439E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.21505287E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.06658927E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.05266453E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.02219757E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.02217149E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.01487187E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55082325E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53819779E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07438291E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46136965E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.40076802E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52520252E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.90888171E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.64965290E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95462244E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93612558E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09251981E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02955711E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.32174236E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.99678678E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.34560107E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.80902724E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11355204E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44196913E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10688336E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44179234E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32932308E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.29362683E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.29352099E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.26376940E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.57782271E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.57782271E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.57782271E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.04291272E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.04291272E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.95342854E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.95342854E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.34763774E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.34763774E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.34284958E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.34284958E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.35026381E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.35026381E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.00296897E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.12790552E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.04222698E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.27115794E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.61296912E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.36928550E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.42698821E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.70192372E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.42895852E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.72066684E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.28378020E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.97460903E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.97457514E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.42733849E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.30257437E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.30257437E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.30257437E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.74196728E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.25572363E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.72252459E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.25516329E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.90727053E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.15234296E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.15205277E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.06998635E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02898846E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02898846E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02898846E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26072787E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26072787E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25319559E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25319559E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.20242029E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.20242029E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.20227902E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.20227902E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.16282485E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.16282485E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55075402E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.64042918E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52456147E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.10977408E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46911559E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46911559E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09607249E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.58517526E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42972037E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88600173E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.02173793E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.02772096E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.60506834E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.84500816E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08800820E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18006977E-01    2           5        -5   # BR(h -> b       bb     )
     6.37119769E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25516933E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77890792E-04    2           3        -3   # BR(h -> s       sb     )
     2.06174311E-02    2           4        -4   # BR(h -> c       cb     )
     6.69440279E-02    2          21        21   # BR(h -> g       g      )
     2.30174582E-03    2          22        22   # BR(h -> gam     gam    )
     1.53667729E-03    2          22        23   # BR(h -> Z       gam    )
     2.00575153E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56026032E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78094035E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46632008E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431377E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71226667E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548954E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668844E-05    2           4        -4   # BR(H -> c       cb     )
     9.96071284E-01    2           6        -6   # BR(H -> t       tb     )
     7.97654413E-04    2          21        21   # BR(H -> g       g      )
     2.71586260E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030689E-06    2          23        22   # BR(H -> Z       gam    )
     3.33965548E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66527351E-04    2          23        23   # BR(H -> Z       Z      )
     9.01887546E-04    2          25        25   # BR(H -> h       h      )
     7.25910726E-24    2          36        36   # BR(H -> A       A      )
     2.91089914E-11    2          23        36   # BR(H -> Z       A      )
     6.49938006E-12    2          24       -37   # BR(H -> W+      H-     )
     6.49938006E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380269E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46924188E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898997E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62271061E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677991E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181696E-06    2           4        -4   # BR(A -> c       cb     )
     9.97001210E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680586E-04    2          21        21   # BR(A -> g       g      )
     3.14000931E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290166E-06    2          23        22   # BR(A -> Z       gam    )
     3.25515243E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471614E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35363868E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239135E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150375E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49634009E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697344E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732265E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402964E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33619549E-04    2          24        25   # BR(H+ -> W+      h      )
     2.83685100E-13    2          24        36   # BR(H+ -> W+      A      )
