evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, two leptons filter, C filter and B veto, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'tpelzer@cern.ch']
evgenConfig.minevents   = 1000
#evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"

#--------------------------------------------------------------
# dilepton filter
# This is a filter on the input LHE files, applied before any other algorithm is processed
#--------------------------------------------------------------
include('MC15JobOptions/LHEFilter.py')
include('MC15JobOptions/LHEFilter_NLeptons.py')

nleptonFilter = LHEFilter_NLeptons()
nleptonFilter.NumLeptons = 2
nleptonFilter.Ptcut = 0.
lheFilters.addFilter(nleptonFilter)

lheFilters.run_filters()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# B filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBFilter.py')

#--------------------------------------------------------------
# C filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusCFilter.py')

# Combine the filters
filtSeq.Expression = "(TTbarPlusCFilter) and (not TTbarPlusBFilter)"
