Mchi =  250.
Mxi =  400.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 11.3091758478
evgenConfig.description = "Wimp pair monoZ with dmV"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmV_Zll_MET40.py")
