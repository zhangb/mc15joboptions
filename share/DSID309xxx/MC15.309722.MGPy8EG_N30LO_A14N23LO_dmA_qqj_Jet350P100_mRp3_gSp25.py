model       = 'dmA'
mR          = 300
mDM         = 10000
gSM         = 0.25
gDM         = 1.00
widthR      = 7.458066
xptj        = 100
filteff     = 0.01100
jetminpt    = 350
quark_decays= ['u', 'd', 's', 'c']

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetjet_flavfilt.py")

evgenConfig.description = "Zprime sample - mR300 - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>"]
evgenConfig.minevents = 1000

