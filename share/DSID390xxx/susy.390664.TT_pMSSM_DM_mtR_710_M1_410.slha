#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12012519E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.17900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.05485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04207843E+01   # W+
        25     1.25636514E+02   # h
        35     4.00001030E+03   # H
        36     3.99999700E+03   # A
        37     4.00100670E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02307594E+03   # ~d_L
   2000001     4.02036441E+03   # ~d_R
   1000002     4.02242710E+03   # ~u_L
   2000002     4.02094968E+03   # ~u_R
   1000003     4.02307594E+03   # ~s_L
   2000003     4.02036441E+03   # ~s_R
   1000004     4.02242710E+03   # ~c_L
   2000004     4.02094968E+03   # ~c_R
   1000005     2.08381565E+03   # ~b_1
   2000005     4.02417830E+03   # ~b_2
   1000006     7.40696065E+02   # ~t_1
   2000006     2.09644297E+03   # ~t_2
   1000011     4.00384024E+03   # ~e_L
   2000011     4.00354901E+03   # ~e_R
   1000012     4.00273131E+03   # ~nu_eL
   1000013     4.00384024E+03   # ~mu_L
   2000013     4.00354901E+03   # ~mu_R
   1000014     4.00273131E+03   # ~nu_muL
   1000015     4.00423671E+03   # ~tau_1
   2000015     4.00818176E+03   # ~tau_2
   1000016     4.00441200E+03   # ~nu_tauL
   1000021     1.98621763E+03   # ~g
   1000022     3.89856061E+02   # ~chi_10
   1000023    -4.29384086E+02   # ~chi_20
   1000025     4.52031345E+02   # ~chi_30
   1000035     2.06524487E+03   # ~chi_40
   1000024     4.27086736E+02   # ~chi_1+
   1000037     2.06541722E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.35321630E-01   # N_11
  1  2     2.06441268E-02   # N_12
  1  3     5.00728200E-01   # N_13
  1  4     4.56231510E-01   # N_14
  2  1    -3.94401122E-02   # N_21
  2  2     2.37652986E-02   # N_22
  2  3    -7.04328017E-01   # N_23
  2  4     7.08379653E-01   # N_24
  3  1     6.76568827E-01   # N_31
  3  2     2.54406077E-02   # N_32
  3  3     5.03142428E-01   # N_33
  3  4     5.37080157E-01   # N_34
  4  1    -1.09584106E-03   # N_41
  4  2     9.99180567E-01   # N_42
  4  3    -6.40402759E-03   # N_43
  4  4    -3.99497368E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.06250015E-03   # U_11
  1  2     9.99958935E-01   # U_12
  2  1    -9.99958935E-01   # U_21
  2  2     9.06250015E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.65070938E-02   # V_11
  1  2    -9.98402198E-01   # V_12
  2  1    -9.98402198E-01   # V_21
  2  2    -5.65070938E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.01111749E-01   # cos(theta_t)
  1  2     9.94875075E-01   # sin(theta_t)
  2  1    -9.94875075E-01   # -sin(theta_t)
  2  2    -1.01111749E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998454E-01   # cos(theta_b)
  1  2    -1.75840769E-03   # sin(theta_b)
  2  1     1.75840769E-03   # -sin(theta_b)
  2  2     9.99998454E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06122972E-01   # cos(theta_tau)
  1  2     7.08089223E-01   # sin(theta_tau)
  2  1    -7.08089223E-01   # -sin(theta_tau)
  2  2    -7.06122972E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00249028E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20125193E+03  # DRbar Higgs Parameters
         1    -4.17900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43865640E+02   # higgs               
         4     1.62019030E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20125193E+03  # The gauge couplings
     1     3.61732291E-01   # gprime(Q) DRbar
     2     6.35247891E-01   # g(Q) DRbar
     3     1.03122818E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20125193E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.13689797E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20125193E+03  # The trilinear couplings
  1  1     4.19532023E-07   # A_d(Q) DRbar
  2  2     4.19571193E-07   # A_s(Q) DRbar
  3  3     7.49793768E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20125193E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.36391816E-08   # A_mu(Q) DRbar
  3  3     9.45974056E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20125193E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68994949E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20125193E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87682476E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20125193E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03072693E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20125193E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57108602E+07   # M^2_Hd              
        22    -1.66112478E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.05485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39985772E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.13710738E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.27387325E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     9.71481053E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.89842970E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.90653898E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.22355027E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.18069308E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.04883675E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.25182683E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.18834847E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.42427852E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.46883923E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.06586334E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.90022792E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.98348130E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.29732519E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.53979274E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.96586519E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.17939731E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     6.82645036E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.54927703E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     2.03760568E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64508517E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65027887E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83619391E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54548231E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.69517595E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68638964E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.36748882E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12130145E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.42714942E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.93162367E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.44653735E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.37021363E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75978269E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.22774910E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.29139355E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.04194610E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81089647E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.52303798E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60966887E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51949434E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58575224E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.00132937E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.60071406E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.52481029E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.70011794E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44652559E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75997366E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.22833480E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.44514888E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.14785859E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81626844E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.42067726E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64239143E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52171622E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51942382E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.82966848E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.24369452E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.58654498E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.65068810E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85561340E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75978269E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.22774910E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.29139355E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.04194610E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81089647E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.52303798E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60966887E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51949434E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58575224E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.00132937E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.60071406E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.52481029E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.70011794E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44652559E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75997366E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.22833480E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.44514888E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.14785859E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81626844E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.42067726E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64239143E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52171622E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51942382E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.82966848E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.24369452E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.58654498E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.65068810E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85561340E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10866697E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.03788143E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.62599693E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.49965617E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77682383E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.29994952E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56858379E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03887483E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.42335906E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.55387650E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.56109557E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.61229841E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10866697E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.03788143E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.62599693E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.49965617E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77682383E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.29994952E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56858379E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03887483E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.42335906E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.55387650E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.56109557E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.61229841E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06190155E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.99405802E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49727822E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.79917977E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39787474E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.55878537E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80328112E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05495266E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.96849570E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02781449E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.63966180E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42875839E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95144434E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86515822E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10976527E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.78545921E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.07868568E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.51989245E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78089767E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.22482453E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54553206E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10976527E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.78545921E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.07868568E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.51989245E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78089767E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.22482453E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54553206E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43374597E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.86604546E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.77338842E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.90733514E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52110649E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.64291878E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02749018E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.89102443E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33746947E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33746947E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11250300E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11250300E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10005505E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.64745704E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.06934874E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44581408E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.21351766E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38414319E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.45725285E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38450324E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.21818881E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.93768892E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18924506E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54287505E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18924506E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54287505E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35282879E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53935996E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53935996E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49238697E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07617020E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07617020E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07617007E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.89905530E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.89905530E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.89905530E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.89905530E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.96635336E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.96635336E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.96635336E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.96635336E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.38166415E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.38166415E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.60950565E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.56032279E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.36585833E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.40277336E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.78571535E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.40277336E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.78571535E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.71761735E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.83712330E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.83712330E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.86717849E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.32434298E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.32434298E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.32424884E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.23989198E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.90601590E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.23989198E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.90601590E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.93357429E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.66683106E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.66683106E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.43530394E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.33273201E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.33273201E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.33273203E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.24842080E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.24842080E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.24842080E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.24842080E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.16134071E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.16134071E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.16134071E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.16134071E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.05826906E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.05826906E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.64561578E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.46989373E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.60891622E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.92346962E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38492079E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38492079E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.81991543E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.21263398E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.76825651E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.00912638E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.00912638E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.04359190E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92080799E-01    2           5        -5   # BR(h -> b       bb     )
     6.44802329E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28259146E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84001298E-04    2           3        -3   # BR(h -> s       sb     )
     2.10223297E-02    2           4        -4   # BR(h -> c       cb     )
     6.85324716E-02    2          21        21   # BR(h -> g       g      )
     2.38136215E-03    2          22        22   # BR(h -> gam     gam    )
     1.63638357E-03    2          22        23   # BR(h -> Z       gam    )
     2.21139952E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80142085E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48734653E+01   # H decays
#          BR         NDA      ID1       ID2
     3.60573874E-01    2           5        -5   # BR(H -> b       bb     )
     6.04227114E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13640073E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52699346E-04    2           3        -3   # BR(H -> s       sb     )
     7.08720189E-08    2           4        -4   # BR(H -> c       cb     )
     7.10285193E-03    2           6        -6   # BR(H -> t       tb     )
     7.27102247E-07    2          21        21   # BR(H -> g       g      )
     2.19653573E-09    2          22        22   # BR(H -> gam     gam    )
     1.82935108E-09    2          23        22   # BR(H -> Z       gam    )
     1.68885480E-06    2          24       -24   # BR(H -> W+      W-     )
     8.43842556E-07    2          23        23   # BR(H -> Z       Z      )
     6.61186757E-06    2          25        25   # BR(H -> h       h      )
    -2.10418661E-24    2          36        36   # BR(H -> A       A      )
     2.89663843E-20    2          23        36   # BR(H -> Z       A      )
     1.86723222E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56459701E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56459701E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.73210091E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.17442811E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.81162129E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.93655290E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.39173644E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.72877194E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.86087573E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.20798347E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.76228572E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.90416725E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.24434537E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.24434537E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.48684194E+01   # A decays
#          BR         NDA      ID1       ID2
     3.60632697E-01    2           5        -5   # BR(A -> b       bb     )
     6.04285171E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13660432E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52764673E-04    2           3        -3   # BR(A -> s       sb     )
     7.13418194E-08    2           4        -4   # BR(A -> c       cb     )
     7.11762672E-03    2           6        -6   # BR(A -> t       tb     )
     1.46249210E-05    2          21        21   # BR(A -> g       g      )
     6.41177359E-08    2          22        22   # BR(A -> gam     gam    )
     1.60867621E-08    2          23        22   # BR(A -> Z       gam    )
     1.68545031E-06    2          23        25   # BR(A -> Z       h      )
     1.89492221E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56458675E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56458675E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.36655153E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.63576522E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.54175682E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.34396905E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.98777573E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.13399323E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.08899627E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.36337068E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.19376245E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.56982309E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.56982309E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49119493E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.77720164E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03958556E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13544949E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69740591E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20977288E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48889186E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67811049E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.68597939E-06    2          24        25   # BR(H+ -> W+      h      )
     2.51491106E-14    2          24        36   # BR(H+ -> W+      A      )
     4.24486790E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.20964365E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.25331239E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.56551542E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.87186607E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55616074E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.96711367E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.51541471E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
