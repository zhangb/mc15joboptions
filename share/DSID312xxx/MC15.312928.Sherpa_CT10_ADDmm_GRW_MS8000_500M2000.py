include("MC15JobOptions/Sherpa_CT10_Common.py")
#include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "ADD Graviton to ll"
evgenConfig.keywords = ["exotic", "graviton", "BSM"]
evgenConfig.contact = ["shellesu@cern.ch"]


evgenConfig.process="""
(processes){
  #
  # jet jet -> m+ m-
  #
  Process 93 93 -> 13 -13 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 13 -13 500.0 2000.
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3           ! number of extra dimensions
  M_S   = 8000       ! string scale (GeV)
  M_CUT = 8000        ! invariant mass cut. Ultra violet cut-off. 
  KK_CONVENTION = 5   

  MASS[39] = 100.
  MASS[40] = 100.

}(model)

 
"""




#genSeq.Sherpa_i.Parameters += ["METS_SCALE_MUFMODE=0"]
#evgenConfig.minevents = 5000

