#Pythia8 hard diffraction with A2

evgenConfig.description = "Hard-diffractive events, with A2 tune"
evgenConfig.keywords = ["QCD", "minBias","diffraction"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.minevents = 500


include("Pythia8_i/Pythia8_A2_MSTW2008LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["Diffraction:doHard = on"]
genSeq.Pythia8.Commands += ["HardQCD:all = on"]
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = 5."] 
genSeq.Pythia8.Commands += ["MultipartonInteractions:bProfile = 2 "]
genSeq.Pythia8.Commands += ["Diffraction:sampleType = 4 "] 


evgenConfig.findJets = True
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

