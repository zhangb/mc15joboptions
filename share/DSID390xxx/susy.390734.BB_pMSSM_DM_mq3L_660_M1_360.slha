#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12088353E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.91900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04171210E+01   # W+
        25     1.25342887E+02   # h
        35     4.00001046E+03   # H
        36     3.99999712E+03   # A
        37     4.00100859E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02324298E+03   # ~d_L
   2000001     4.02037784E+03   # ~d_R
   1000002     4.02258675E+03   # ~u_L
   2000002     4.02156037E+03   # ~u_R
   1000003     4.02324298E+03   # ~s_L
   2000003     4.02037784E+03   # ~s_R
   1000004     4.02258675E+03   # ~c_L
   2000004     4.02156037E+03   # ~c_R
   1000005     7.32757093E+02   # ~b_1
   2000005     4.02392789E+03   # ~b_2
   1000006     7.21539985E+02   # ~t_1
   2000006     2.25061904E+03   # ~t_2
   1000011     4.00419949E+03   # ~e_L
   2000011     4.00297694E+03   # ~e_R
   1000012     4.00308262E+03   # ~nu_eL
   1000013     4.00419949E+03   # ~mu_L
   2000013     4.00297694E+03   # ~mu_R
   1000014     4.00308262E+03   # ~nu_muL
   1000015     4.00434024E+03   # ~tau_1
   2000015     4.00782519E+03   # ~tau_2
   1000016     4.00474738E+03   # ~nu_tauL
   1000021     1.97680881E+03   # ~g
   1000022     3.49297562E+02   # ~chi_10
   1000023    -4.02915199E+02   # ~chi_20
   1000025     4.16061611E+02   # ~chi_30
   1000035     2.05353484E+03   # ~chi_40
   1000024     4.00577349E+02   # ~chi_1+
   1000037     2.05369924E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.47460538E-01   # N_11
  1  2    -1.52696411E-02   # N_12
  1  3    -4.01720903E-01   # N_13
  1  4    -3.46695531E-01   # N_14
  2  1    -4.34083154E-02   # N_21
  2  2     2.40585775E-02   # N_22
  2  3    -7.03951629E-01   # N_23
  2  4     7.08511825E-01   # N_24
  3  1     5.29079609E-01   # N_31
  3  2     2.84331247E-02   # N_32
  3  3     5.85694569E-01   # N_33
  3  4     6.13374434E-01   # N_34
  4  1    -1.05948465E-03   # N_41
  4  2     9.99189462E-01   # N_42
  4  3    -5.85593232E-03   # N_43
  4  4    -3.98121240E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.28738712E-03   # U_11
  1  2     9.99965659E-01   # U_12
  2  1    -9.99965659E-01   # U_21
  2  2     8.28738712E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.63134772E-02   # V_11
  1  2    -9.98413137E-01   # V_12
  2  1    -9.98413137E-01   # V_21
  2  2    -5.63134772E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96522403E-01   # cos(theta_t)
  1  2    -8.33252682E-02   # sin(theta_t)
  2  1     8.33252682E-02   # -sin(theta_t)
  2  2     9.96522403E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999236E-01   # cos(theta_b)
  1  2    -1.23612274E-03   # sin(theta_b)
  2  1     1.23612274E-03   # -sin(theta_b)
  2  2     9.99999236E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05947816E-01   # cos(theta_tau)
  1  2     7.08263850E-01   # sin(theta_tau)
  2  1    -7.08263850E-01   # -sin(theta_tau)
  2  2    -7.05947816E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00254474E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20883532E+03  # DRbar Higgs Parameters
         1    -3.91900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43908749E+02   # higgs               
         4     1.62243972E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20883532E+03  # The gauge couplings
     1     3.61648810E-01   # gprime(Q) DRbar
     2     6.36009858E-01   # g(Q) DRbar
     3     1.03189250E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20883532E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16241327E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20883532E+03  # The trilinear couplings
  1  1     4.28422929E-07   # A_d(Q) DRbar
  2  2     4.28463117E-07   # A_s(Q) DRbar
  3  3     7.65687358E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20883532E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.41427382E-08   # A_mu(Q) DRbar
  3  3     9.50963487E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20883532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67729330E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20883532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84993848E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20883532E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03112685E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20883532E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57331204E+07   # M^2_Hd              
        22    -1.69647902E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40337805E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.25863001E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45347643E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45347643E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54652357E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54652357E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.51970067E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.52874951E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.13747608E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.11978011E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.21399429E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.30406589E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.57686701E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.17816526E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.49315231E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.01275515E-09    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.33800918E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.20959284E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.34338985E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.39933595E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.28972802E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.01417706E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.87004461E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.47753659E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.76978688E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.87318669E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.68794898E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66985700E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53405288E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79416108E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60821153E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.18265674E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60502941E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.34443097E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13803871E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.68333686E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.27059428E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.82006775E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.53273940E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78818110E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.81595495E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14354866E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.41518649E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78467464E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42591274E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55725578E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52785528E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61221477E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.93423228E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02721369E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.52402324E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.42898996E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45314689E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78838694E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.65931167E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.91617726E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.40980647E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78985214E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.17519044E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58966398E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53003633E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54506531E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.02598793E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.67880937E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.97440635E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.93870413E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85738917E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78818110E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.81595495E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14354866E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.41518649E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78467464E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42591274E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55725578E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52785528E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61221477E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.93423228E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02721369E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.52402324E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.42898996E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45314689E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78838694E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.65931167E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.91617726E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.40980647E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78985214E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.17519044E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58966398E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53003633E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54506531E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.02598793E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.67880937E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.97440635E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.93870413E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85738917E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13920033E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.10054096E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.96019176E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.44532919E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77983614E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.90960556E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57439706E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04782700E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.19488881E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87812795E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.78632371E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.19935982E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13920033E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.10054096E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.96019176E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.44532919E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77983614E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.90960556E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57439706E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04782700E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.19488881E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87812795E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.78632371E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.19935982E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08230682E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.59104172E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44583683E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.19457322E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40356769E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51652295E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81458139E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07643429E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.65811791E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05080966E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.93305388E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43360052E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95388032E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87475869E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14026027E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.24853223E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19401507E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.72458540E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78372178E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18837964E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55146351E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14026027E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.24853223E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19401507E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.72458540E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78372178E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18837964E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55146351E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46526239E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13191910E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08249847E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.37672671E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52518809E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58498987E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03589616E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.30400344E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33546595E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33546595E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183710E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183710E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10539390E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.94988068E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.74274854E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.62308145E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.69573315E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.40017187E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.53313834E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.11463267E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.59802409E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.26122276E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.49307761E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17925159E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52956046E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17925159E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52956046E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42362874E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50677899E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50677899E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47702291E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01061480E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01061480E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01061461E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.16183787E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.16183787E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.16183787E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.16183787E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.20613257E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.20613257E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.20613257E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.20613257E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.36995645E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.36995645E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.50816120E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.71585330E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.74629910E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.81234866E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.79380025E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.81234866E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.79380025E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.41446345E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.98266662E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.98266662E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.98129367E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.04635868E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.04635868E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.04634762E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.77753895E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.49404629E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.77753895E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.49404629E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.61527437E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.71826899E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.71826899E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.54525524E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.43463586E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.43463586E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.43463589E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.44436229E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.44436229E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.44436229E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.44436229E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.48143929E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.48143929E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.48143929E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.48143929E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.38888676E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.38888676E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.94851014E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.60781127E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.31973796E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.81564668E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.52865925E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.52865925E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.61480836E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.34390372E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.37504395E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.83489518E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.83489518E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.84840918E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.84840918E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.98975827E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94777335E-01    2           5        -5   # BR(h -> b       bb     )
     6.51985818E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30803387E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89618885E-04    2           3        -3   # BR(h -> s       sb     )
     2.12658913E-02    2           4        -4   # BR(h -> c       cb     )
     6.94261053E-02    2          21        21   # BR(h -> g       g      )
     2.38710755E-03    2          22        22   # BR(h -> gam     gam    )
     1.61575743E-03    2          22        23   # BR(h -> Z       gam    )
     2.17229837E-01    2          24       -24   # BR(h -> W+      W-     )
     2.73789621E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51352320E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59352785E-01    2           5        -5   # BR(H -> b       bb     )
     6.01358404E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12625766E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51499593E-04    2           3        -3   # BR(H -> s       sb     )
     7.05370753E-08    2           4        -4   # BR(H -> c       cb     )
     7.06928360E-03    2           6        -6   # BR(H -> t       tb     )
     8.01872765E-07    2          21        21   # BR(H -> g       g      )
     1.25071429E-09    2          22        22   # BR(H -> gam     gam    )
     1.81858235E-09    2          23        22   # BR(H -> Z       gam    )
     1.70847728E-06    2          24       -24   # BR(H -> W+      W-     )
     8.53646813E-07    2          23        23   # BR(H -> Z       Z      )
     6.81795701E-06    2          25        25   # BR(H -> h       h      )
    -1.78554349E-25    2          36        36   # BR(H -> A       A      )
    -4.50952529E-21    2          23        36   # BR(H -> Z       A      )
     1.85900516E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58047381E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58047381E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.25879090E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.76288856E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.41371415E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.59359034E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.62502572E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.44903230E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.19297744E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.19215347E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.21444218E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.15920060E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.62478015E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.62478015E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.41132598E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51290154E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59418793E-01    2           5        -5   # BR(A -> b       bb     )
     6.01428720E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12650461E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51569854E-04    2           3        -3   # BR(A -> s       sb     )
     7.10045870E-08    2           4        -4   # BR(A -> c       cb     )
     7.08398174E-03    2           6        -6   # BR(A -> t       tb     )
     1.45557885E-05    2          21        21   # BR(A -> g       g      )
     6.24027164E-08    2          22        22   # BR(A -> gam     gam    )
     1.60005121E-08    2          23        22   # BR(A -> Z       gam    )
     1.70509190E-06    2          23        25   # BR(A -> Z       h      )
     1.88079045E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58054343E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58054343E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.96030281E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.34922307E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.19817530E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.13263696E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.92293234E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.66036413E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.32343096E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.51971794E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.67095722E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.78526981E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.78526981E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53019327E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.78150595E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.99699787E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12039153E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.70016067E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20124219E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47134148E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.68030539E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.70164537E-06    2          24        25   # BR(H+ -> W+      h      )
     2.51914952E-14    2          24        36   # BR(H+ -> W+      A      )
     5.79130822E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.63294239E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.72061198E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57774855E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.10273970E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56670412E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.08738373E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.15614062E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.13725618E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
