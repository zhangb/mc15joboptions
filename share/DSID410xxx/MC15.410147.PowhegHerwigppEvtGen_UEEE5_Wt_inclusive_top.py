#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP Wt production (top), inclusive, with CT10 for ME and CTEQ6L1 for PS, UEEE5 tune and with EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', 'inclusive']
evgenConfig.contact     = [ 'cescobar@cern.ch']
evgenConfig.generators += [ 'Powheg', "Herwigpp", "EvtGen" ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Wt_DR_Common.py')

PowhegConfig.topdecaymode = 11111 # inclusive W-from-top decays
PowhegConfig.wdecaymode = 11111 # inclusive W decays
# PowhegConfig.nEvents *= 3.
PowhegConfig.PDF     = 10800
PowhegConfig.mu_F    = 1.00000000000000000000
PowhegConfig.mu_R    = 1.00000000000000000000
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")
