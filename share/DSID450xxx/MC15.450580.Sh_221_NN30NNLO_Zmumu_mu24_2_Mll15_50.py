include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2j@NLO + 3,4j@LO with and 15<mll<50 GeV."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Zmumu_mu24_2_Mll15_50"

Sherpa_iRunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
}(run)

(processes){
  Process 93 93 -> 13 -13 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  Enhance_Factor  5 {4};
  Enhance_Factor 10 {5};
  End process;
}(processes)

(selector){
  # low mass
  Mass 13 -13 15.0 50.0

  # pT_lep1>24 GeV, pT_lep2>2 GeV, |eta|<2.9
  "PT" 90 24.0,E_CMS:2.0,E_CMS [PT_UP]
  "Eta" 90 -2.9,2.9:-2.9,2.9 [PT_UP]
}(selector)
"""

Sherpa_iNCores = 96
Sherpa_iOpenLoopsLibs = [ "ppll", "ppllj", "pplljj" ]

# Set up HF filters
### HF filter is not needed for integration step. Once the integration grid is there, we could obtain the filter eff.
###include("MC15JobOptions/BHadronFilter.py")
###filtSeq += HeavyFlavorBHadronFilter
