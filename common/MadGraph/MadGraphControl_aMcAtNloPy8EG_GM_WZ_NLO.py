#! /usr/bin/env python 
from MadGraphControl.MadGraphUtils import *



# multi-core running, if allowed!
import os
import fileinput
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts


# extract dataset short name from filename, should be of the form MC12.999000.MadGraphPythia8EvtGen_AU2MSTW2008LO_HVT_VzWW_qqqq_m1000.py
shortname=runArgs.jobConfig[0].split('/')[-1].split('.')[2].split('_')

# decode dataset short name, should be of the form MadGraphPythia8EvtGen_AU14NNPDF23LO_HVT_Agv1_VzWW_qqqq_m1000 (split by '_')
scenario=shortname[3]
GMboson=shortname[4][:2]
GMdecay=shortname[4][3:]
VVdecay=shortname[5]
mass=int(shortname[6][1:])
# -- Check if VBF
isVBF = ( 'vbf' in shortname[2] )
pdfErrSize=100

# PDF information, in MadGraph's PDF naming scheme.  
# Should probably extract this from shortname[1] in some fancy way.
# For now, specify lhapdf and the ID so that madgraph doesn't get 
# confused trying to interpret some shorthand name (which it can).
# Note that if you change these numbers, you'll probably want to 
# change the "sys_pdf" tag in the run card too.  That's not done
# automatically yet.
pdf='lhapdf'
lhaid=280400 # NNPDF23_lo_as_0130_qed
parton_shower='PYTHIA8'


evgenConfig.inputfilecheck = "H5p_WZ_lvll"
#runArgs.inputGeneratorFile=name+'.events.tar.gz'
# Some more information
evgenConfig.generators = ["MadGraph"]
evgenConfig.description = "GM Signal Point"
evgenConfig.keywords = ["exotic"] 
evgenConfig.contact = ["Benjamin Freund <benjamin.freund@cern.ch>"]
evgenConfig.process = "pp>%s>%s>%s" % (GMboson,GMdecay,VVdecay) # e.g. pp>Vc>WZ>qqqq

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")    
include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 3
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
genSeq.Pythia8.Commands +=["SpaceShower:dipoleRecoil = on"]
