#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.51900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.55000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05427314E+01   # W+
        25     1.25000000E+02   # h
        35     2.00397394E+03   # H
        36     2.00000000E+03   # A
        37     2.00152493E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013293E+03   # ~d_L
   2000001     5.00002540E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994919E+03   # ~u_R
   1000003     5.00013293E+03   # ~s_L
   2000003     5.00002540E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994919E+03   # ~c_R
   1000005     4.99905238E+03   # ~b_1
   2000005     5.00110719E+03   # ~b_2
   1000006     4.97632255E+03   # ~t_1
   2000006     5.02801088E+03   # ~t_2
   1000011     5.00008213E+03   # ~e_L
   2000011     5.00007621E+03   # ~e_R
   1000012     4.99984166E+03   # ~nu_eL
   1000013     5.00008213E+03   # ~mu_L
   2000013     5.00007621E+03   # ~mu_R
   1000014     4.99984166E+03   # ~nu_muL
   1000015     4.99939895E+03   # ~tau_1
   2000015     5.00075993E+03   # ~tau_2
   1000016     4.99984166E+03   # ~nu_tauL
   1000021     2.60000000E+03   # ~g
   1000022     2.48397870E+03   # ~chi_10
   1000023    -2.55005838E+03   # ~chi_20
   1000025     2.57194624E+03   # ~chi_30
   1000035     3.01313344E+03   # ~chi_40
   1000024     2.53705865E+03   # ~chi_1+
   1000037     3.01302800E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.76403621E-01   # N_11
  1  2    -9.35970367E-02   # N_12
  1  3     4.41856741E-01   # N_13
  1  4    -4.39544802E-01   # N_14
  2  1     1.72271496E-03   # N_21
  2  2    -2.79481483E-03   # N_22
  2  3    -7.07062499E-01   # N_23
  2  4    -7.07143439E-01   # N_24
  3  1     6.30060954E-01   # N_31
  3  2     1.38429702E-01   # N_32
  3  3    -5.39842447E-01   # N_33
  3  4     5.40768475E-01   # N_34
  4  1     1.47527462E-02   # N_41
  4  2    -9.85935597E-01   # N_42
  4  3    -1.15738394E-01   # N_43
  4  4     1.19657760E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.62411743E-01   # U_11
  1  2     9.86723074E-01   # U_12
  2  1     9.86723074E-01   # U_21
  2  2     1.62411743E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.67923048E-01   # V_11
  1  2     9.85800106E-01   # V_12
  2  1     9.85800106E-01   # V_21
  2  2     1.67923048E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07494491E-01   # cos(theta_t)
  1  2     7.06718859E-01   # sin(theta_t)
  2  1    -7.06718859E-01   # -sin(theta_t)
  2  2     7.07494491E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.88356452E-01   # cos(theta_b)
  1  2     7.25372590E-01   # sin(theta_b)
  2  1    -7.25372590E-01   # -sin(theta_b)
  2  2     6.88356452E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05569258E-01   # cos(theta_tau)
  1  2     7.08640968E-01   # sin(theta_tau)
  2  1    -7.08640968E-01   # -sin(theta_tau)
  2  2     7.05569258E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90186116E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.55000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52448621E+02   # vev(Q)              
         4     2.68468002E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52665196E-01   # gprime(Q) DRbar
     2     6.26429891E-01   # g(Q) DRbar
     3     1.07628192E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02398555E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.70897963E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79670954E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.51900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -4.21673409E+06   # M^2_Hd              
        22    -1.13133513E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36715985E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.36823060E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.80747098E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.81495595E-05    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.11195182E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.72077854E-06    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.01236919E-12    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.61479263E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     6.55262485E-06    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.76959474E-13    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     4.94305932E-09    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.72077854E-06    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.01236919E-12    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.61479263E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     6.55262485E-06    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.76959474E-13    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.94305932E-09    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.62436219E-06    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     9.98720679E-10    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.97215855E-10    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     5.13784300E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.13784300E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.13784300E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.13784300E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.22298625E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.29579768E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.10903111E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.52105525E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.05861214E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.08420715E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.91478809E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.62586442E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.11788392E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.30688606E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.97435190E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.46364888E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.89166610E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     7.04053873E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.47436844E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.58485399E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.73182596E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.23282975E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.15250549E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.46679638E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.97359746E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.02480178E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.23626943E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.28650002E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.78043998E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.50567047E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.54727690E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.88781769E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.28948084E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.16987221E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.91126806E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.05526329E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67042720E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.73677877E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.77240547E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.01964513E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.43497541E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.50759897E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.91822765E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.58766670E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.49365622E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.43029088E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.15493924E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.52637849E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.29264156E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60426898E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67052964E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.66378183E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.00466467E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.48820738E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.48549883E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28131086E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.93579018E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.58816535E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.44937957E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.26155326E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.97566188E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.93266449E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.62129907E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89804131E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67042720E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.73677877E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.77240547E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.01964513E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.43497541E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.50759897E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.91822765E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.58766670E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.49365622E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.43029088E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.15493924E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.52637849E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.29264156E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60426898E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67052964E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.66378183E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.00466467E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.48820738E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.48549883E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28131086E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.93579018E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.58816535E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.44937957E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.26155326E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.97566188E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.93266449E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.62129907E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89804131E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.74223481E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.76424032E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.29768646E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.36091623E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.75895852E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.07025694E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.62148716E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.37763290E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.14222805E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.91807664E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.85615732E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.58544593E-04    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.74223481E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.76424032E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.29768646E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.36091623E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.75895852E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.07025694E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.62148716E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.37763290E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.14222805E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.91807664E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.85615732E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.58544593E-04    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.06383092E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.42677282E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.71839218E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.89030757E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.80683905E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89006885E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.68161307E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.74221306E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.06167024E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.31205679E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     4.84000294E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.93448974E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.86223487E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.44908160E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.79188779E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.74266894E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.13687261E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.52068287E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.80007785E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.85260798E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.21254140E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.60920228E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.74266894E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.13687261E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.52068287E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.80007785E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.85260798E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.21254140E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.60920228E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.74442578E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13614484E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.51714880E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.79892553E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.85078188E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.27356693E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.60576886E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.17672758E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.94843035E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     3.76837553E-03    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     2.67600908E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.66675461E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     8.92003784E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     8.91986470E-02    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     8.87131945E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.00615678E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.60699832E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.69327125E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.44169995E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.68076242E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40107180E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.40199116E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.87040211E-08    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.93493550E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.94171482E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     8.73959185E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
     3.44978768E-03    2     1000039        35   # BR(~chi_10 -> ~G        H)
     1.45588162E-04    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     7.09117329E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.28042258E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.96328912E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.35335275E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.34370158E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
     7.04117272E-05    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.84689841E-03    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.07406251E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.39092141E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.06962377E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.39081275E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32098629E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.17744751E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.17737348E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.15658359E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.34606418E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.34606418E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.34606418E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.89207331E-04    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.89207331E-04    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.78537354E-04    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.78537354E-04    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.30691199E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.30691199E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.30482900E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.30482900E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.75731718E-05    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.75731718E-05    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.75369115E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.85675300E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.00713498E-07    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.81212506E-01    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.32531009E-01    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.10884572E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
     4.94476170E-03    2     1000039        35   # BR(~chi_30 -> ~G        H)
     2.00839135E-04    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     6.33839250E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.88151313E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.47112984E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.03807508E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.78446574E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.53205848E-05    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.53204102E-05    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.08673733E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.69237634E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.69237634E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.69237634E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.87544645E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.42872298E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.82511209E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.42719460E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.54257980E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.54822379E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.54750864E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.34128371E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.10809869E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.10809869E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.10809869E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.63993208E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.63993208E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.57780127E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.57780127E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.54664059E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.54664059E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.54652383E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.54652383E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.51401673E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.51401673E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     2.00456917E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.06433793E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51731040E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.43858124E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.52974761E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.52974761E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.23324937E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.48864957E-05    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.59832980E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.16201076E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.08583959E-05    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.67982144E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.82715441E-08    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.25567728E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.09119504E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18330216E-01    2           5        -5   # BR(h -> b       bb     )
     6.36597384E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25332028E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77499329E-04    2           3        -3   # BR(h -> s       sb     )
     2.06017501E-02    2           4        -4   # BR(h -> c       cb     )
     6.68912687E-02    2          21        21   # BR(h -> g       g      )
     2.29939100E-03    2          22        22   # BR(h -> gam     gam    )
     1.53552062E-03    2          22        23   # BR(h -> Z       gam    )
     2.00396622E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55826616E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78069173E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46130661E-03    2           5        -5   # BR(H -> b       bb     )
     2.46449302E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71290036E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11557283E-06    2           3        -3   # BR(H -> s       sb     )
     1.00670346E-05    2           4        -4   # BR(H -> c       cb     )
     9.96085511E-01    2           6        -6   # BR(H -> t       tb     )
     7.97626181E-04    2          21        21   # BR(H -> g       g      )
     2.71834003E-06    2          22        22   # BR(H -> gam     gam    )
     1.16060681E-06    2          23        22   # BR(H -> Z       gam    )
     3.29814397E-04    2          24       -24   # BR(H -> W+      W-     )
     1.64457457E-04    2          23        23   # BR(H -> Z       Z      )
     8.98901870E-04    2          25        25   # BR(H -> h       h      )
     6.98275594E-24    2          36        36   # BR(H -> A       A      )
     2.82874013E-11    2          23        36   # BR(H -> Z       A      )
     6.12635032E-12    2          24       -37   # BR(H -> W+      H-     )
     6.12635032E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82376769E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46417557E-03    2           5        -5   # BR(A -> b       bb     )
     2.43901229E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62278953E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13679031E-06    2           3        -3   # BR(A -> s       sb     )
     9.96190814E-06    2           4        -4   # BR(A -> c       cb     )
     9.97010335E-01    2           6        -6   # BR(A -> t       tb     )
     9.43689223E-04    2          21        21   # BR(A -> g       g      )
     3.12257606E-06    2          22        22   # BR(A -> gam     gam    )
     1.35317139E-06    2          23        22   # BR(A -> Z       gam    )
     3.21462077E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74468459E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34238695E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49242015E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81160558E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48913889E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45703419E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08733475E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99407123E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.29468332E-04    2          24        25   # BR(H+ -> W+      h      )
     2.89591361E-13    2          24        36   # BR(H+ -> W+      A      )
