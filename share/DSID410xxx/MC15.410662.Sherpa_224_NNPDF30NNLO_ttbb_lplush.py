include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa 2.2.4 ttbb production with NLO+PS in the 4FS, cf. arXiv:1309.5912." 
evgenConfig.keywords = ["top", "SM" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "Thorsten.Kuhl@cern.ch"]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Sherpa_224_NNPDF30NNLO_ttbb"

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES VAR{FSF*H_TM2/4}{RSF*sqrt(MPerp(p[2])*MPerp(p[3])*MPerp(p[4])*MPerp(p[5]))}{QSF*H_TM2/4}

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % settings relevant for 4-flavour scheme
  MASSIVE[5]=1
  MASS[5]=4.95
  PDF_LIBRARY LHAPDFSherpa
  #LHAPDF_GRID_PATH=./PDFsets
  PDF_SET NNPDF30_nnlo_as_0118_nf_4
  USE_PDF_ALPHAS=1
  CSS_ENHANCE S{G}{t}{tb} 0
  CSS_ENHANCE S{G}{tb}{t} 0
  MCATNLO_MASSIVE_SPLITTINGS=0     # deactivates g->bb first emission in MC@NLO
  EXCLUSIVE_CLUSTER_MODE=1
  CSS_KMODE=34

  % decays
  HARD_DECAYS=1
  STABLE[6]=0
  WIDTH[6]=0
  STABLE[24]=0
  HDH_STATUS[24,2,-1]=0
  HDH_STATUS[24,4,-3]=0
  HDH_STATUS[-24,-12,11]=0
  HDH_STATUS[-24,-14,13]=0
  HDH_STATUS[-24,-16,15]=0
}(run)

(processes){
  Process 93 93 -> 6 -6 5 -5
  Order (*,0)
  NLO_QCD_Mode MC@NLO
  Loop_Generator LOOPGEN
  ME_Generator Amegic
  RS_ME_Generator Comix
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]
genSeq.Sherpa_i.NCores = 48
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppttjj"]
genSeq.Sherpa_i.Parameters += [ "PDF_VARIATIONS=NNPDF30_nnlo_as_0118_nf_4[all]" ]




