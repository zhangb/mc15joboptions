#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.64200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416910E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400497E+03   # H
        36     2.00000000E+03   # A
        37     2.00151417E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.96252848E+03   # ~d_L
   2000001     4.96241863E+03   # ~d_R
   1000002     4.96228286E+03   # ~u_L
   2000002     4.96234086E+03   # ~u_R
   1000003     4.96252848E+03   # ~s_L
   2000003     4.96241863E+03   # ~s_R
   1000004     4.96228286E+03   # ~c_L
   2000004     4.96234086E+03   # ~c_R
   1000005     4.96179272E+03   # ~b_1
   2000005     4.96315578E+03   # ~b_2
   1000006     5.07735655E+03   # ~t_1
   2000006     5.36917079E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007613E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007613E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99963923E+03   # ~tau_1
   2000015     5.00051965E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.88523465E+03   # ~g
   1000022     1.70131600E+03   # ~chi_10
   1000023    -1.75206962E+03   # ~chi_20
   1000025     1.78858963E+03   # ~chi_30
   1000035     3.12643708E+03   # ~chi_40
   1000024     1.74767391E+03   # ~chi_1+
   1000037     3.12643281E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.28430619E-01   # N_11
  1  2    -3.73122768E-02   # N_12
  1  3     4.85151532E-01   # N_13
  1  4    -4.82311744E-01   # N_14
  2  1     2.49823577E-03   # N_21
  2  2    -3.18303819E-03   # N_22
  2  3    -7.07029194E-01   # N_23
  2  4    -7.07172784E-01   # N_24
  3  1     6.85112665E-01   # N_31
  3  2     4.23017042E-02   # N_32
  3  3    -5.13149624E-01   # N_33
  3  4     5.15275330E-01   # N_34
  4  1     1.79694643E-03   # N_41
  4  2    -9.98402839E-01   # N_42
  4  3    -3.76187942E-02   # N_43
  4  4     4.21113906E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.31467804E-02   # U_11
  1  2     9.98586711E-01   # U_12
  2  1     9.98586711E-01   # U_21
  2  2     5.31467804E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.94995743E-02   # V_11
  1  2     9.98228331E-01   # V_12
  2  1     9.98228331E-01   # V_21
  2  2     5.94995743E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99903968E-01   # cos(theta_t)
  1  2     1.38583829E-02   # sin(theta_t)
  2  1    -1.38583829E-02   # -sin(theta_t)
  2  2     9.99903968E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.78014933E-01   # cos(theta_b)
  1  2     7.35048128E-01   # sin(theta_b)
  2  1    -7.35048128E-01   # -sin(theta_b)
  2  2     6.78014933E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04680640E-01   # cos(theta_tau)
  1  2     7.09524626E-01   # sin(theta_tau)
  2  1    -7.09524626E-01   # -sin(theta_tau)
  2  2     7.04680640E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202295E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52258969E+02   # vev(Q)              
         4     3.22500640E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52737027E-01   # gprime(Q) DRbar
     2     6.26884826E-01   # g(Q) DRbar
     3     1.08213402E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02508966E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71561540E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79759398E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.64200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.66884557E+03   # M^2_Hd              
        22    -7.95573214E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36919060E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.75885379E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.74665712E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.35065011E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.89972423E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.95831149E-05    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.59466387E-10    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.13246613E-06    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.04636092E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.05000001E-10    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     8.51472941E-06    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.95831149E-05    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.59466387E-10    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.13246613E-06    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.04636092E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.05000001E-10    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     8.51472941E-06    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.85875104E-05    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     5.15638025E-07    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.24999603E-06    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.03355064E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.03355064E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.03355064E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.03355064E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.79296796E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.78084832E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.01068758E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.49551834E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.68618951E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     4.54728208E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.30897658E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.06013288E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.84511623E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.31203719E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.45548199E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.60489548E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.94367292E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.94761271E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.09869494E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.79127424E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.63462251E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.15478973E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.62170362E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.06872826E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.45617909E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     9.83111329E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.25749547E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.33056608E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.51727337E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.99265603E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.32623703E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.04583144E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.68454937E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.48910597E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.35211874E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.42837090E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.27516310E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.60428851E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.62958757E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.31184027E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.29927723E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.14401914E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.12700406E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.76408044E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.97228767E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.94519667E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.20464968E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.78478563E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.41140981E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.00738825E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.99694991E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.18166580E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.52503330E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.87635530E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.22810604E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59419474E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.12707590E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.07492606E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.99859756E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.95447411E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.20944479E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.81775259E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.41694383E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.00783065E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.93622274E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.62536298E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.51073000E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.83813342E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.60595152E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89536422E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.12700406E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.76408044E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.97228767E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.94519667E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.20464968E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.78478563E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.41140981E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.00738825E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.99694991E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.18166580E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.52503330E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.87635530E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.22810604E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59419474E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.12707590E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.07492606E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.99859756E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.95447411E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.20944479E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.81775259E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.41694383E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.00783065E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.93622274E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.62536298E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.51073000E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.83813342E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.60595152E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89536422E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.65974217E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.97512557E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78631563E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02276254E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71156499E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.19902384E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43615181E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.91050643E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.37516510E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.22254620E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.62475716E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.55179553E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.65974217E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.97512557E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78631563E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02276254E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71156499E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.19902384E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43615181E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.91050643E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.37516510E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.22254620E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.62475716E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.55179553E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.29030439E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.77862218E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57172610E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.48868773E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.55838424E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.38864594E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.12427735E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.57031412E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.28772378E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.63943261E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.30501466E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.56544197E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59240054E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04725593E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.19237261E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.65993565E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14882578E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19055475E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.58136088E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72190644E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.00891796E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.43092346E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.65993565E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14882578E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19055475E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.58136088E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72190644E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.00891796E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.43092346E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.66240807E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14775893E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.18944915E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.57524915E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71937876E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.93173325E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42590111E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.43416674E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.51058022E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.11881951E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.11881951E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.03960764E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.03960764E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03208768E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.82352878E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53461651E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08612500E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46189079E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.38490229E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53237582E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.95757646E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.66094029E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99637222E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89377286E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09854920E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.81008843E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.15389697E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.98901643E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.00134647E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.94951628E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.12380202E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.45520861E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.12380202E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.45520861E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33731689E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.32370971E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.32370971E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.29272209E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.63780082E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.63780082E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.63780082E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.33614569E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.33614569E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.33614569E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.33614569E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.11204863E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.11204863E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.11204863E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.11204863E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.06801648E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.06801648E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.03953348E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.11407911E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.49907333E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.94402745E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.51674153E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.02873511E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22592333E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.45221581E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.22592333E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.45221581E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.99833496E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.50622282E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.50622282E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.38370709E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.11817411E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.11817411E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.11817411E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.98341924E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.56832781E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.98341924E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.56832781E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.19208480E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.86609118E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.86609118E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.77666088E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.17151492E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.17151492E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.17151492E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26499053E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26499053E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26499053E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26499053E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.21662916E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.21662916E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.21662916E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.21662916E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.17734562E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.17734562E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.82350882E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.68100734E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52207799E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.34682663E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46630787E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46630787E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.11151897E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     7.84478045E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41782510E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.14223299E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.81423899E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.82170633E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.62043649E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.45493456E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22082481E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02129946E-01    2           5        -5   # BR(h -> b       bb     )
     6.22024585E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20169585E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65836272E-04    2           3        -3   # BR(h -> s       sb     )
     2.00970848E-02    2           4        -4   # BR(h -> c       cb     )
     6.63702338E-02    2          21        21   # BR(h -> g       g      )
     2.30528502E-03    2          22        22   # BR(h -> gam     gam    )
     1.62572079E-03    2          22        23   # BR(h -> Z       gam    )
     2.16518721E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80645445E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78099461E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46806777E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428034E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71214846E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547371E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668487E-05    2           4        -4   # BR(H -> c       cb     )
     9.96067977E-01    2           6        -6   # BR(H -> t       tb     )
     7.97757482E-04    2          21        21   # BR(H -> g       g      )
     2.71637753E-06    2          22        22   # BR(H -> gam     gam    )
     1.16020998E-06    2          23        22   # BR(H -> Z       gam    )
     3.34709160E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66898136E-04    2          23        23   # BR(H -> Z       Z      )
     9.02232283E-04    2          25        25   # BR(H -> h       h      )
     7.37163964E-24    2          36        36   # BR(H -> A       A      )
     2.94059731E-11    2          23        36   # BR(H -> Z       A      )
     6.66657536E-12    2          24       -37   # BR(H -> W+      H-     )
     6.66657536E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381193E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47099163E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898407E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62268976E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677716E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179287E-06    2           4        -4   # BR(A -> c       cb     )
     9.96998799E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678304E-04    2          21        21   # BR(A -> g       g      )
     3.14150278E-06    2          22        22   # BR(A -> gam     gam    )
     1.35280511E-06    2          23        22   # BR(A -> Z       gam    )
     3.26177869E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472059E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35762542E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238279E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147350E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49889163E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695636E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731925E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402283E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34297004E-04    2          24        25   # BR(H+ -> W+      h      )
     2.79516801E-13    2          24        36   # BR(H+ -> W+      A      )
