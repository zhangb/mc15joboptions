# JOs for testing TauolaPP functionaity, adopted from DSID 361108
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.minevents = 1000

if runArgs.trfSubstepName == 'generate' :
#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
  include('PowhegControl/PowhegControl_Z_Common.py')
  if hasattr(PowhegConfig, "vdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.vdecaymode = 3  # tautau
  else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
  PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
  PowhegConfig.nEvents   *= 1.2 # increase number of generated events by 20%
  PowhegConfig.running_width = 1

  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
  include('MC15JobOptions/nonStandard/Pythia8_AZNLO_CTEQ6L1_Common.py')
  evgenConfig.generators += ["EvtGen"]
  include('MC15JobOptions/nonStandard/Pythia8_TauolaPP.py')
  include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

if runArgs.trfSubstepName == 'afterburn':
    evgenConfig.generators += ["Powheg"]
    evgenConfig.generators += ["Pythia8"]
    include("MC15JobOptions/EvtGen_Fragment.py")
    genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
    genSeq.EvtInclusiveDecay.readExisting = True
    evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
    genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
    genSeq.EvtInclusiveDecay.whiteList+=[-5334, 5334]
