# original JOs: MC15.410003....
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------                                                                                                        
evgenConfig.generators  += ["aMcAtNlo", "Herwig7"]
evgenConfig.description  = 'MG5_aMC@NLO+Herwig7 ttbar non-allhad, UE-EE-5 tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'lepton']
evgenConfig.contact      = [ 'maria.moreno.llacer@cern.ch' ]

#--------------------------------------------------------------
# Showering with Herwig7, H7-UE-MMHT tune
#--------------------------------------------------------------
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_EvtGen_Common.py')

## commands specific to showering MG5_aMC@NLO events
genSeq.Herwig7.Commands += hw.mg5amc_cmds().splitlines()

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(non-all had)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

