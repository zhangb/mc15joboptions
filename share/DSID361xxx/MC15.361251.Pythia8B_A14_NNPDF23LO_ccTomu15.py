##############################################################
# Job options fragment for cc->mu15X 
# no EvtGen, because method of multiple hadronisation in Pythia8B is incompatible with EvtGen
##############################################################
include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description = "Inclusive cc->mu15X production"
evgenConfig.keywords = ["charm","muon","inclusive"]
evgenConfig.minevents = 200

genSeq.Pythia8B.Commands += ['HardQCD:all = on']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 15.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = on']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = False
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 15.0
genSeq.Pythia8B.AntiQuarkPtCut = 15.0
genSeq.Pythia8B.QuarkEtaCut = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops = 8

include("common/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [15.0]
genSeq.Pythia8B.TriggerStateEtaCut = 3.0
genSeq.Pythia8B.MinimumCountPerCut = [1]
