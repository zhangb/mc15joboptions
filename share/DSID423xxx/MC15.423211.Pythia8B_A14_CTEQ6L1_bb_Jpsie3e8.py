evgenConfig.description = "Inclusive pp->bb->J/psi(e3e8) production with Photos"
evgenConfig.keywords = ["egamma","charmonium","2electron","inclusive"]
evgenConfig.process  = "bb -> Jpsi -> ee"
evgenConfig.contact  = [ "ocariz@in2p3.fr" ]
evgenConfig.minevents = 100

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->ee
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:1:onMode = on']

genSeq.Pythia8B.SignalPDGCodes = [443,-11,11]

genSeq.Pythia8B.NHadronizationLoops = 1

genSeq.Pythia8B.TriggerPDGCode = 11
genSeq.Pythia8B.TriggerStatePtCut = [8.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [1]

# Filter
include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 3000.
filtSeq.MultiLeptonFilter.Etacut =  2.7
filtSeq.MultiLeptonFilter.NLeptons =  2
