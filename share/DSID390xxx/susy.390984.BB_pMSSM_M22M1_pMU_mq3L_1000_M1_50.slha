#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17311834E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03991976E+01   # W+
        25     1.25037437E+02   # h
        35     3.00004407E+03   # H
        36     2.99999997E+03   # A
        37     3.00088892E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04348180E+03   # ~d_L
   2000001     3.03831655E+03   # ~d_R
   1000002     3.04256448E+03   # ~u_L
   2000002     3.03917064E+03   # ~u_R
   1000003     3.04348180E+03   # ~s_L
   2000003     3.03831655E+03   # ~s_R
   1000004     3.04256448E+03   # ~c_L
   2000004     3.03917064E+03   # ~c_R
   1000005     1.10023684E+03   # ~b_1
   2000005     3.03720112E+03   # ~b_2
   1000006     1.09657048E+03   # ~t_1
   2000006     3.01973494E+03   # ~t_2
   1000011     3.00628387E+03   # ~e_L
   2000011     3.00231606E+03   # ~e_R
   1000012     3.00488026E+03   # ~nu_eL
   1000013     3.00628387E+03   # ~mu_L
   2000013     3.00231606E+03   # ~mu_R
   1000014     3.00488026E+03   # ~nu_muL
   1000015     2.98576647E+03   # ~tau_1
   2000015     3.02178698E+03   # ~tau_2
   1000016     3.00455923E+03   # ~nu_tauL
   1000021     2.35419416E+03   # ~g
   1000022     4.98205056E+01   # ~chi_10
   1000023     1.08628802E+02   # ~chi_20
   1000025    -2.99413519E+03   # ~chi_30
   1000035     2.99455716E+03   # ~chi_40
   1000024     1.08780704E+02   # ~chi_1+
   1000037     2.99526239E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886243E-01   # N_11
  1  2    -2.69482146E-03   # N_12
  1  3     1.48076979E-02   # N_13
  1  4    -9.85491623E-04   # N_14
  2  1     3.08256947E-03   # N_21
  2  2     9.99652735E-01   # N_22
  2  3    -2.60808949E-02   # N_23
  2  4     2.16658100E-03   # N_24
  3  1    -9.72494852E-03   # N_31
  3  2     1.69382731E-02   # N_32
  3  3     7.06818439E-01   # N_33
  3  4     7.07125318E-01   # N_34
  4  1    -1.11096497E-02   # N_41
  4  2     2.00060549E-02   # N_42
  4  3     7.06758950E-01   # N_43
  4  4    -7.07084238E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99317634E-01   # U_11
  1  2    -3.69359707E-02   # U_12
  2  1     3.69359707E-02   # U_21
  2  2     9.99317634E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995289E-01   # V_11
  1  2    -3.06957053E-03   # V_12
  2  1     3.06957053E-03   # V_21
  2  2     9.99995289E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98413082E-01   # cos(theta_t)
  1  2    -5.63144537E-02   # sin(theta_t)
  2  1     5.63144537E-02   # -sin(theta_t)
  2  2     9.98413082E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99905992E-01   # cos(theta_b)
  1  2     1.37115704E-02   # sin(theta_b)
  2  1    -1.37115704E-02   # -sin(theta_b)
  2  2     9.99905992E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06889220E-01   # cos(theta_tau)
  1  2     7.07324275E-01   # sin(theta_tau)
  2  1    -7.07324275E-01   # -sin(theta_tau)
  2  2     7.06889220E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01726225E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73118338E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43916761E+02   # higgs               
         4     7.87247181E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73118338E+03  # The gauge couplings
     1     3.62611283E-01   # gprime(Q) DRbar
     2     6.41922371E-01   # g(Q) DRbar
     3     1.02313944E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73118338E+03  # The trilinear couplings
  1  1     2.83758571E-06   # A_u(Q) DRbar
  2  2     2.83762742E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73118338E+03  # The trilinear couplings
  1  1     7.02796656E-07   # A_d(Q) DRbar
  2  2     7.02936918E-07   # A_s(Q) DRbar
  3  3     1.64263596E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73118338E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.56769020E-07   # A_mu(Q) DRbar
  3  3     1.58691193E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73118338E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49677347E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73118338E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12254138E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73118338E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04844296E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73118338E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.19025706E+04   # M^2_Hd              
        22    -9.02209940E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43039000E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.86955010E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47611681E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47611681E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52388319E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52388319E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.33730531E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.16559890E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.19900097E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.68443914E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10198468E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.01268565E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.41553778E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.84597274E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.09655352E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.00847508E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.65619022E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59609444E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12661363E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.32568255E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.26175517E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.38753393E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.48629055E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.16266219E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.02968186E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.34904561E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.74436116E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.71998315E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.41332792E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.33118076E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.75043880E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.06067010E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.84175966E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.10373897E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.74910932E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.66759380E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.34508578E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.00700301E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.33358182E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.42755556E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.94133221E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15473076E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.61691750E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.53444732E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.51845423E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.94578526E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.38306680E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.10868691E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.08554594E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.66308509E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.94884790E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.47288967E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.32778434E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.14611986E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.94826832E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64398727E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.60753063E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.37252129E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.17146057E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.34350412E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.53924247E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.10373897E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.74910932E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.66759380E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.34508578E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.00700301E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.33358182E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.42755556E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.94133221E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15473076E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.61691750E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.53444732E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.51845423E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.94578526E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.38306680E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.10868691E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.08554594E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.66308509E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.94884790E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.47288967E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.32778434E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.14611986E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.94826832E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64398727E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.60753063E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.37252129E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.17146057E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.34350412E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.53924247E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07628337E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.54678059E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02347207E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.57602137E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.45504520E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02184937E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.41820663E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56951430E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990509E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.48466116E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.80330585E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.29123610E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07628337E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.54678059E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02347207E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.57602137E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.45504520E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02184937E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.41820663E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56951430E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990509E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.48466116E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.80330585E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.29123610E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.85196121E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43179956E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19545147E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37274897E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.79153936E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.51205844E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16894332E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.49972201E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.67381908E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.31850935E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.71539868E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07664083E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.72532610E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00080665E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.74792483E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.81815353E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02666056E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.32566628E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07664083E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.72532610E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00080665E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.74792483E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.81815353E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02666056E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.32566628E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07654968E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.72450336E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00055147E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.29359205E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     9.21916872E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02697492E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.31113067E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.83508465E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35833059E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.20505356E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35833059E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.20505356E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09619196E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.40172663E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09619196E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.40172663E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09095472E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     7.70537884E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.58579370E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.97133264E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.54018441E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.74621453E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.38380054E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.58548128E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.98535814E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.81983402E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.25941246E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.73485864E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     9.81176508E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.73485864E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     9.81176508E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.06662039E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     8.41367203E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     8.41367203E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     8.85139420E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.37809168E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.37809168E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.37644027E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     7.74660850E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.40256917E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.47245857E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.51953003E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.51953003E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.10603534E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.28096634E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51747644E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.51747644E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.17467290E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.17467290E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.79909274E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.79909274E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.63970648E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.08971573E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.25206557E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.62649534E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.62649534E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.46506262E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.70181749E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49126662E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.49126662E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.20647738E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.20647738E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.04082963E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.04082963E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.78228328E-03   # h decays
#          BR         NDA      ID1       ID2
     6.68419249E-01    2           5        -5   # BR(h -> b       bb     )
     5.45804041E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93216108E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.10045871E-04    2           3        -3   # BR(h -> s       sb     )
     1.77066184E-02    2           4        -4   # BR(h -> c       cb     )
     5.76005866E-02    2          21        21   # BR(h -> g       g      )
     1.97390745E-03    2          22        22   # BR(h -> gam     gam    )
     1.31098498E-03    2          22        23   # BR(h -> Z       gam    )
     1.75774260E-01    2          24       -24   # BR(h -> W+      W-     )
     2.19885444E-02    2          23        23   # BR(h -> Z       Z      )
     4.21831895E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.41394497E+01   # H decays
#          BR         NDA      ID1       ID2
     7.41358702E-01    2           5        -5   # BR(H -> b       bb     )
     1.75869197E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.21831455E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.63612134E-04    2           3        -3   # BR(H -> s       sb     )
     2.15395146E-07    2           4        -4   # BR(H -> c       cb     )
     2.14870391E-02    2           6        -6   # BR(H -> t       tb     )
     2.41387366E-05    2          21        21   # BR(H -> g       g      )
     3.06435816E-08    2          22        22   # BR(H -> gam     gam    )
     8.30108611E-09    2          23        22   # BR(H -> Z       gam    )
     2.86361983E-05    2          24       -24   # BR(H -> W+      W-     )
     1.43004418E-05    2          23        23   # BR(H -> Z       Z      )
     7.80195478E-05    2          25        25   # BR(H -> h       h      )
     7.95918468E-25    2          36        36   # BR(H -> A       A      )
    -6.10705868E-20    2          23        36   # BR(H -> Z       A      )
     2.41037086E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12922097E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19703367E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.36118967E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.52927164E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.10638502E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33593884E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84737446E-01    2           5        -5   # BR(A -> b       bb     )
     1.86139807E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.58144957E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.08338672E-04    2           3        -3   # BR(A -> s       sb     )
     2.28101715E-07    2           4        -4   # BR(A -> c       cb     )
     2.27420867E-02    2           6        -6   # BR(A -> t       tb     )
     6.69732681E-05    2          21        21   # BR(A -> g       g      )
     4.25183973E-08    2          22        22   # BR(A -> gam     gam    )
     6.59529261E-08    2          23        22   # BR(A -> Z       gam    )
     3.01951887E-05    2          23        25   # BR(A -> Z       h      )
     2.60766528E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21255182E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29500042E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92750341E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04580901E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10853384E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.37849436E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.40977592E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.09460560E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94225341E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.01678045E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.20349075E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.86304216E-05    2          24        25   # BR(H+ -> W+      h      )
     6.98453989E-14    2          24        36   # BR(H+ -> W+      A      )
     1.99440580E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.89612735E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.67456248E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
