#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10165096E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03976558E+01   # W+
        25     1.26921010E+02   # h
        35     3.00021971E+03   # H
        36     3.00000031E+03   # A
        37     3.00111342E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01288277E+03   # ~d_L
   2000001     3.00716467E+03   # ~d_R
   1000002     3.01195865E+03   # ~u_L
   2000002     3.01160954E+03   # ~u_R
   1000003     3.01288277E+03   # ~s_L
   2000003     3.00716467E+03   # ~s_R
   1000004     3.01195865E+03   # ~c_L
   2000004     3.01160954E+03   # ~c_R
   1000005     3.86274365E+02   # ~b_1
   2000005     3.01090539E+03   # ~b_2
   1000006     3.79912204E+02   # ~t_1
   2000006     3.01704111E+03   # ~t_2
   1000011     3.00797054E+03   # ~e_L
   2000011     2.99868409E+03   # ~e_R
   1000012     3.00657474E+03   # ~nu_eL
   1000013     3.00797054E+03   # ~mu_L
   2000013     2.99868409E+03   # ~mu_R
   1000014     3.00657474E+03   # ~nu_muL
   1000015     2.98642739E+03   # ~tau_1
   2000015     3.02288564E+03   # ~tau_2
   1000016     3.00747546E+03   # ~nu_tauL
   1000021     2.31531713E+03   # ~g
   1000022     1.01631895E+02   # ~chi_10
   1000023     2.16529422E+02   # ~chi_20
   1000025    -3.01289383E+03   # ~chi_30
   1000035     3.01300003E+03   # ~chi_40
   1000024     2.16687421E+02   # ~chi_1+
   1000037     3.01387983E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891923E-01   # N_11
  1  2     7.65844008E-04   # N_12
  1  3    -1.46797875E-02   # N_13
  1  4     2.45349641E-04   # N_14
  2  1    -3.82864505E-04   # N_21
  2  2     9.99659658E-01   # N_22
  2  3     2.60812476E-02   # N_23
  2  4     4.35860916E-04   # N_24
  3  1     1.02173203E-02   # N_31
  3  2    -1.87443225E-02   # N_32
  3  3     7.06777995E-01   # N_33
  3  4     7.07113232E-01   # N_34
  4  1    -1.05642158E-02   # N_41
  4  2     1.81282071E-02   # N_42
  4  3    -7.06802050E-01   # N_43
  4  4     7.07100154E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99320296E-01   # U_11
  1  2     3.68638782E-02   # U_12
  2  1    -3.68638782E-02   # U_21
  2  2     9.99320296E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999811E-01   # V_11
  1  2    -6.14800828E-04   # V_12
  2  1    -6.14800828E-04   # V_21
  2  2    -9.99999811E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98662503E-01   # cos(theta_t)
  1  2    -5.17030473E-02   # sin(theta_t)
  2  1     5.17030473E-02   # -sin(theta_t)
  2  2     9.98662503E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99709860E-01   # cos(theta_b)
  1  2    -2.40872543E-02   # sin(theta_b)
  2  1     2.40872543E-02   # -sin(theta_b)
  2  2     9.99709860E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06882492E-01   # cos(theta_tau)
  1  2     7.07330999E-01   # sin(theta_tau)
  2  1    -7.07330999E-01   # -sin(theta_tau)
  2  2    -7.06882492E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00241328E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.01650956E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44887401E+02   # higgs               
         4     1.23069702E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.01650956E+03  # The gauge couplings
     1     3.60848635E-01   # gprime(Q) DRbar
     2     6.39183064E-01   # g(Q) DRbar
     3     1.03502101E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.01650956E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.02793694E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.01650956E+03  # The trilinear couplings
  1  1     3.22316346E-07   # A_d(Q) DRbar
  2  2     3.22355382E-07   # A_s(Q) DRbar
  3  3     6.76345812E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.01650956E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.58995380E-07   # A_mu(Q) DRbar
  3  3     1.61255676E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.01650956E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.61557856E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.01650956E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.12378403E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.01650956E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03075728E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.01650956E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.72900661E+04   # M^2_Hd              
        22    -9.21669906E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41827621E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.64727932E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48058880E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48058880E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51941120E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51941120E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.53443313E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.75240822E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.82475918E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26570721E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.17074000E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.29743536E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.62337292E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.84324515E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72861239E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65055512E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.25359254E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.45303193E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.16084896E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.38391510E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.89157340E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.78599312E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -2.31918380E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.16373217E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.06371697E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.04626027E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.15771445E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.69534463E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07983521E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.85471284E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62726263E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.25716465E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.05702560E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.23524015E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55306827E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.26086967E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.44693151E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.08493733E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.75784391E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62708309E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.25144322E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.06389525E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.70590601E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43048299E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.44950502E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55695164E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07983521E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.85471284E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62726263E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.25716465E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.05702560E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.23524015E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55306827E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.26086967E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.44693151E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.08493733E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.75784391E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62708309E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.25144322E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.06389525E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.70590601E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43048299E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.44950502E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55695164E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.01439933E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.70722040E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01028464E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01899332E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54970441E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.45425861E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.01439933E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.70722040E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01028464E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01899332E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54970441E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.45425861E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.81047911E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45137468E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18348324E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.36514207E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.75243076E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.53179922E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15659733E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.45051829E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.66890350E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.31154243E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.98286308E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.01472323E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.64940434E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01121911E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.02384046E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.01472323E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.64940434E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01121911E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.02384046E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.01628417E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.64855697E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01096935E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.02417495E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.08539417E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.03078235E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.75461113E-13    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.74340083E-10    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.75461113E-13    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.74340083E-10    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.52599344E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.58112906E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.05289363E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.05289363E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.07650009E-07    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.47821209E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.47821209E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.53994594E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.71951490E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.80091679E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.58089613E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.82727195E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.74958586E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     1.03168453E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.47226885E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.76226975E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.57973460E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.57973460E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.88748881E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.27965180E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.09542385E-11    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     3.09542385E-11    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.19393323E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.19393323E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     3.09652078E-15    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     3.09652078E-15    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     5.31968536E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     5.31968536E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     3.09542385E-11    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     3.09542385E-11    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.19393323E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.19393323E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     3.09652078E-15    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     3.09652078E-15    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     5.31968536E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     5.31968536E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.34896093E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34896093E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.54152686E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.54152686E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.06547625E-10    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     1.06547625E-10    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     6.99366967E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     6.99366967E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.06547625E-10    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     1.06547625E-10    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     6.99366967E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     6.99366967E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.80244698E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.80244698E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     6.26068738E-10    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     6.26068738E-10    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     6.26068738E-10    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     6.26068738E-10    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.60447600E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.60447600E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.03467302E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.83862549E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.94767099E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.56341863E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.56341863E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09978930E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.70553846E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49756708E-11    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.49756708E-11    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.48357179E-11    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.48357179E-11    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     3.31550029E-13    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     3.31550029E-13    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.87414557E-11    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.87414557E-11    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.49756708E-11    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.49756708E-11    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.48357179E-11    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.48357179E-11    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     3.31550029E-13    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     3.31550029E-13    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.87414557E-11    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.87414557E-11    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.25969886E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25969886E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.52100323E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.52100323E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.74247294E-11    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.74247294E-11    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     7.56634810E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     7.56634810E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.74247294E-11    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.74247294E-11    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     7.56634810E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     7.56634810E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.58545091E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.58545091E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     6.23433492E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     6.23433492E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     6.23433492E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     6.23433492E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     4.61032543E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     4.61032543E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.89968821E-03   # h decays
#          BR         NDA      ID1       ID2
     5.34323246E-01    2           5        -5   # BR(h -> b       bb     )
     6.75427623E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.39094695E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.05975753E-04    2           3        -3   # BR(h -> s       sb     )
     2.19773493E-02    2           4        -4   # BR(h -> c       cb     )
     7.28923254E-02    2          21        21   # BR(h -> g       g      )
     2.57526168E-03    2          22        22   # BR(h -> gam     gam    )
     1.89527471E-03    2          22        23   # BR(h -> Z       gam    )
     2.64074337E-01    2          24       -24   # BR(h -> W+      W-     )
     3.39743738E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.51099750E+01   # H decays
#          BR         NDA      ID1       ID2
     9.16329331E-01    2           5        -5   # BR(H -> b       bb     )
     5.51291878E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.94923634E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.39365385E-04    2           3        -3   # BR(H -> s       sb     )
     6.71190176E-08    2           4        -4   # BR(H -> c       cb     )
     6.69555859E-03    2           6        -6   # BR(H -> t       tb     )
     2.20466148E-06    2          21        21   # BR(H -> g       g      )
     2.74952307E-08    2          22        22   # BR(H -> gam     gam    )
     2.73434684E-09    2          23        22   # BR(H -> Z       gam    )
     8.45327767E-07    2          24       -24   # BR(H -> W+      W-     )
     4.22142636E-07    2          23        23   # BR(H -> Z       Z      )
     4.75092644E-06    2          25        25   # BR(H -> h       h      )
     9.67990446E-26    2          36        36   # BR(H -> A       A      )
     1.21048607E-17    2          23        36   # BR(H -> Z       A      )
     7.42367649E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.43394384E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.71516783E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.26391033E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.00234622E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.23655214E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.42088882E+01   # A decays
#          BR         NDA      ID1       ID2
     9.34988577E-01    2           5        -5   # BR(A -> b       bb     )
     5.62491927E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.98883426E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.44270141E-04    2           3        -3   # BR(A -> s       sb     )
     6.89295715E-08    2           4        -4   # BR(A -> c       cb     )
     6.87238272E-03    2           6        -6   # BR(A -> t       tb     )
     2.02385064E-05    2          21        21   # BR(A -> g       g      )
     4.79009523E-08    2          22        22   # BR(A -> gam     gam    )
     1.99248772E-08    2          23        22   # BR(A -> Z       gam    )
     8.59051403E-07    2          23        25   # BR(A -> Z       h      )
     7.71012414E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.53165122E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.85836523E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.33294634E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.77282349E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48934757E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.21208660E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.84286669E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.53181286E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.08300459E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.22808869E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.35736195E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.97092647E-07    2          24        25   # BR(H+ -> W+      h      )
     4.71066013E-14    2          24        36   # BR(H+ -> W+      A      )
     4.30933012E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.34449560E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.79440420E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
