#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal top mass, A14 tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'sonja.verena.bartkowski@cern.ch' ]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.decay_mode = "t t~ > none"

PowhegConfig.MadSpin_decays= ["decay t > w+ b, w+ > l+ vl", "decay t~ > w- s~, w- > l- vl~","decay t~ > w- d~, w- > l- vl~","decay t > w+ b, w+ > j j", "decay t~ > w- s~, w- > j j","decay t~ > w- d~, w- > j j"]
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]\n add process b g > t t~ b [QCD]\n add process b~ g > t t~ b~ [QCD]\n  add process b b~ > t t~[QCD]"
PowhegConfig.hdamp        = 258.75
# compensate filter efficiency
PowhegConfig.nEvents     *= 3.

PowhegConfig.alphaem_inv  = 1.323489e+02
PowhegConfig.G_F          = 1.166370e-05
PowhegConfig.alphaqcd     = 1.184000e-01
PowhegConfig.mass_W       = 80.399
PowhegConfig.mass_Z       = 9.118760e+01
PowhegConfig.mass_H       = 125.0

PowhegConfig.BR_t_to_Wb = 0.60
PowhegConfig.BR_t_to_Ws = 0.30
PowhegConfig.BR_t_to_Wd = 0.10
PowhegConfig.MadSpin_mode = "full"


PowhegConfig.generate()



#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.



