#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'MG5_aMC@NLO+Pythia8 ttbar production A14 tune + rb=1.05 NNPDF23LO EvtGen with single lepton filter from DSID 410440 LHE files'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'marco.vanadia@cern.ch', 'simone.amoroso@cern.ch' ]
evgenConfig.minevents = 1000
evgenConfig.generators += ["aMcAtNlo","Pythia8"]

#if runArgs.trfSubstepName == 'generate' :
#  evgenConfig.inputfilecheck = "aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttbar_incl_LHE"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 20000.

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter

filtSeq += TTbarWToLeptonFilter("onetWlep")
filtSeq.onetWlep.NumLeptons = 1
filtSeq.onetWlep.Ptcut = 0.

filtSeq += TTbarWToLeptonFilter("twotWlep")
filtSeq.twotWlep.NumLeptons = 2
filtSeq.twotWlep.Ptcut = 0.

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("Wmu")
filtSeq.Wmu.PDGParent  = [24]
filtSeq.Wmu.PDGChild = [13]

filtSeq += ParentChildFilter("Wetau")
filtSeq.Wetau.PDGParent  = [24]
filtSeq.Wetau.PDGChild = [11,15]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("OneMuonFilter")
filtSeq.OneMuonFilter.Ptcut = 3250.
filtSeq.OneMuonFilter.Etacut = 2.8
filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 2.8
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 3250.
filtSeq.ThreeMuonsFilter.Etacut = 2.8
filtSeq.ThreeMuonsFilter.NMuons = 3

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 20000.
filtSeq.ElectronFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 20000.
filtSeq.MuonFilter.Etacut = 2.8

filtSeq.Expression="TTbarWToLeptonFilter and (ElectronFilter or MuonFilter) and ((onetWlep and ((Wmu and TwoMuonsFilter) or (Wetau and OneMuonFilter))) or (twotWlep and ( ((not Wmu) and OneMuonFilter) or (Wmu and Wetau and TwoMuonsFilter) or (Wmu and (not Wetau) and ThreeMuonsFilter) )) )"


