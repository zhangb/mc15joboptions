#################################################################################
# job options fragment for cc->PosDs10->phipi->mu8mu4pi1 
# For HF tau3mu based on Meson Decay(MD) study for run2
################################################################################# 
# All production channels of cc are included in this fragment.
# D_s meson force decaying into phi+pi and phi also must decay into dimuon.
# D_s pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>8GeV, mu2>4GeV, pi>1GeV, and D_s>10GeV
#################################################################################

f = open("PosDs_PhiPi_2MuPi_USER.DEC","w")
f.write("Alias myPhi phi\n")
f.write("Decay D_s+\n")
f.write("1.0000   myPhi   pi+           SVS;\n")
f.write("Enddecay\n")
f.write("Decay myPhi\n")
f.write("1.0000    mu+  mu-          PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description  = "cc->PosDs->phipi->2mupi from cc production"
evgenConfig.keywords     = [ "charmonium", "muon", "SM" ]
evgenConfig.minevents    = 200
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "cc>PosDs>phipi>2mupi"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.SelectBQuarks      = False
genSeq.Pythia8B.SelectCQuarks      = True
genSeq.Pythia8B.QuarkPtCut         = 10.0
genSeq.Pythia8B.AntiQuarkPtCut     = 10.0
genSeq.Pythia8B.QuarkEtaCut        = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut    = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = False
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops   = 10
genSeq.Pythia8B.NDecayLoops           = 1

genSeq.EvtInclusiveDecay.userDecayFile = "PosDs_PhiPi_2MuPi_USER.DEC"

#Add the Filter:
from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
genSeq += TripletChainFilter()

TripletChainFilter = genSeq.TripletChainFilter
TripletChainFilter.NTriplet       = 1
TripletChainFilter.PdgId1         = 13
TripletChainFilter.PdgId2         = -13
TripletChainFilter.PdgId3         = 211
TripletChainFilter.NStep1         = 2
TripletChainFilter.NStep2         = 2
TripletChainFilter.NStep3         = 1
TripletChainFilter.PtMin1         = 8000
TripletChainFilter.PtMin2         = 4000
TripletChainFilter.PtMin3         = 1000
TripletChainFilter.EtaMax1        = 3.0
TripletChainFilter.EtaMax2        = 3.0
TripletChainFilter.EtaMax3        = 3.0
TripletChainFilter.TripletPdgId   = 431
TripletChainFilter.TripletPtMin   = 10000
TripletChainFilter.TripletEtaMax  = 3.0
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.DoubletPdgId   = 333
TripletChainFilter.DoubletPtMin   = 0
TripletChainFilter.DoubletEtaMax  = 100
TripletChainFilter.DoubletMassMin = 0
TripletChainFilter.DoubletMassMax = 10000000

