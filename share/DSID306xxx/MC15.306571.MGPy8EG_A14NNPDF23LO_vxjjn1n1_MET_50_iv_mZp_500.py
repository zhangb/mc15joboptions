model="InelasticVectorEFT"
mDM1 = 250.
mDM2 = 1000.
mZp = 500.
mHD = 125.
widthZp = 2.346527e+00
widthN2 = 4.558223e-01
filteff = 9.813543e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
