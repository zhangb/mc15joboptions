include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "mu nu gamma production with up to three jets in ME+PS and 70<pT_gamma<140 GeV."
evgenConfig.keywords = ["electroweak", "muon", "photon", "neutrino", "SM" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.inputconfcheck = "munugammaPt70_140"
evgenConfig.minevents = 2000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  14 -13 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;

  Process 93 93 ->  -14 13 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  PT 22  70 140
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""
