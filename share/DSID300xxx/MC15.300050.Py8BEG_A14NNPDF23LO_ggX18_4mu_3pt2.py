##############################################################
# Job options fragment for gg->X->4mu  
# Created: 19 Aug 2016 by Tiesheng.Dai@cern.ch
##############################################################
evgenConfig.description = "PYTHIA8 gg->X->4mu with A14 NNPDF23LO"
evgenConfig.keywords = ["heavyFlavour","Upsilon","4muon"]
evgenConfig.contact = [ 'Tiesheng.Dai@cern.ch' ]
evgenConfig.minevents = 5000
 
include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8B.Commands += ['Bottomonium:all = off',
                             'Higgs:useBSM = on',
                             'HiggsBSM:gg2H2 = on',
                             'HiggsH2:coup2d = 1.0',
                             'HiggsH2:coup2u = 1.0',
                             'HiggsH2:coup2Z = 0.0',
                             'HiggsH2:coup2W = 0.0',
                             'HiggsA3:coup2H2Z = 0.0',
                             'HiggsH2:coup2A3A3 = 0.0',
                             'HiggsH2:coup2H1H1 = 0.0',
                             '35:mMin = 0',
                             '35:mMax = 25',
                             '35:m0   = 18.0',
                             '35:mWidth = 0.00',
                             '35:addChannel 1 1.00 100 13 -13 13 -13',
                             '35:onMode = off',
                             '35:onIfMatch 13 -13 13 -13' ## 4mu
                           ] 
#
### 3 lepton filter
#
include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.NLeptons = 3
filtSeq.MultiLeptonFilter.Ptcut = 2000.0
filtSeq.MultiLeptonFilter.Etacut = 2.8
