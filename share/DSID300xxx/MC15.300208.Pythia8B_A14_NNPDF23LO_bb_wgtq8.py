evgenConfig.description = "Pythia8 bbbar production"
evgenConfig.keywords = ["inclusive","bottom","bbbar"]
evgenConfig.minevents = 1000

include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 2.7
genSeq.Pythia8B.AntiQuarkEtaCut = 2.7
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1


##g->bb weight
genSeq.Pythia8B.Commands += ['TimeShower:weightGluonToQuark = 8']


