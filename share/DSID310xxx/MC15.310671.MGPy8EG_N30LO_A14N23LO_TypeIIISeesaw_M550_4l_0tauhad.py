evgenConfig.description = 'MadGraph5+Pythia8 Type III Seesaw Model - multilepton, Mass: 550 GeV, 4l 0tauhad, Vu=0.055 Ve=0.055 Vt=0.055'
evgenConfig.contact = ['tadej.novak@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic', 'seeSaw']

# mass of the heavy leptons
mL = 550.0

# filters
enableFilters = True
hadronicTau = False
minLeptons = 4
maxLeptons = -1
filterSafeFactor = 100

# load configuration
include('MC15JobOptions/MadGraphControl_MGPy8EG_N30LO_A14N23LO_TypeIIISeesaw_multilep.py')
