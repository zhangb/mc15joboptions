##############################################################
# JO  fragment for W->lnu
# To be used for comparing with Sherpa for DPS studies
##############################################################
evgenConfig.description = "pp -> W(lv) "
evgenConfig.keywords = ["electroweak","W"]
evgenConfig.minevents = 10000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 11", # switch on W->lv decays
                            "24:onIfAny = 13",
                            "24:onIfAny = 15"
                            ] 






