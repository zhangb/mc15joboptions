#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Protos+Pythia8+EvtGen single top-quark t-channel production (2->3) (lept.) Pz=+1 polarization, A14 tune, ME CTEQ6L1 LO, A14 NNPDF23 LO'
evgenConfig.keywords = ["top", "singleTop", "tChannel", "leptonic"]
evgenConfig.contact  = ["rubi@cern.ch", "nbruscin@cern.ch"]
evgenConfig.inputfilecheck = "st_tchan_lept_tbj"
evgenConfig.minevents  = 5000

#--------------------------------------------------------------
# Base configuration for ProtosLHEF_i interface
#--------------------------------------------------------------
include('MC15JobOptions/ProtosLHEF_Common.py')
# genSeq.protos2lhef.OutputLevel = DEBUG

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_LHEF.py")

# Configure Pythia 8 to shower Protos input using Main31 shower veto?
# genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 2' ]
# genSeq.Pythia8.Commands += [ 'TimeShower:pTmaxMatch = 2' ]
# genSeq.Pythia8.Commands += [ 'PDF:pSet=LHAPDF6:cteq6l1' ]

# include shower weights (needed?)
# include("MC15JobOptions/Pythia8_ShowerWeights.py")
