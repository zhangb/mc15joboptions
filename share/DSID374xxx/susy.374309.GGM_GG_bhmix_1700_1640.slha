#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.63200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.64000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416758E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400519E+03   # H
        36     2.00000000E+03   # A
        37     2.00151353E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.97172755E+03   # ~d_L
   2000001     4.97161783E+03   # ~d_R
   1000002     4.97148220E+03   # ~u_L
   2000002     4.97154015E+03   # ~u_R
   1000003     4.97172755E+03   # ~s_L
   2000003     4.97161783E+03   # ~s_R
   1000004     4.97148220E+03   # ~c_L
   2000004     4.97154015E+03   # ~c_R
   1000005     4.97099667E+03   # ~b_1
   2000005     4.97235008E+03   # ~b_2
   1000006     5.08649354E+03   # ~t_1
   2000006     5.37798470E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007613E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007613E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99964190E+03   # ~tau_1
   2000015     5.00051698E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.79543095E+03   # ~g
   1000022     1.69076421E+03   # ~chi_10
   1000023    -1.74156378E+03   # ~chi_20
   1000025     1.77804842E+03   # ~chi_30
   1000035     3.12648734E+03   # ~chi_40
   1000024     1.73720129E+03   # ~chi_1+
   1000037     3.12648318E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.29007974E-01   # N_11
  1  2    -3.70024480E-02   # N_12
  1  3     4.84737090E-01   # N_13
  1  4    -4.81879804E-01   # N_14
  2  1     2.51342375E-03   # N_21
  2  2    -3.18987155E-03   # N_22
  2  3    -7.07028476E-01   # N_23
  2  4    -7.07173416E-01   # N_24
  3  1     6.84498301E-01   # N_31
  3  2     4.20017346E-02   # N_32
  3  3    -5.13564620E-01   # N_33
  3  4     5.15702735E-01   # N_34
  4  1     1.76980264E-03   # N_41
  4  2    -9.98427012E-01   # N_42
  4  3    -3.73104227E-02   # N_43
  4  4     4.18127051E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.27118926E-02   # U_11
  1  2     9.98609762E-01   # U_12
  2  1     9.98609762E-01   # U_21
  2  2     5.27118926E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.90784482E-02   # V_11
  1  2     9.98253343E-01   # V_12
  2  1     9.98253343E-01   # V_21
  2  2     5.90784482E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99894044E-01   # cos(theta_t)
  1  2     1.45568119E-02   # sin(theta_t)
  2  1    -1.45568119E-02   # -sin(theta_t)
  2  2     9.99894044E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.77837262E-01   # cos(theta_b)
  1  2     7.35211974E-01   # sin(theta_b)
  2  1    -7.35211974E-01   # -sin(theta_b)
  2  2     6.77837262E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04665162E-01   # cos(theta_tau)
  1  2     7.09539998E-01   # sin(theta_tau)
  2  1    -7.09539998E-01   # -sin(theta_tau)
  2  2     7.04665162E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202061E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.64000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52256281E+02   # vev(Q)              
         4     3.23686475E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52738011E-01   # gprime(Q) DRbar
     2     6.26891009E-01   # g(Q) DRbar
     3     1.08305274E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02506053E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71586034E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79760668E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.63200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.30475121E+04   # M^2_Hd              
        22    -7.93736573E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36921825E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.15000151E-05   # gluino decays
#          BR         NDA      ID1       ID2
     6.78418662E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.09500504E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.85437014E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.54473619E-06    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     9.57547437E-12    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.35373314E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.63283143E-05    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.33399129E-12    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.13572498E-09    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.54473619E-06    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     9.57547437E-12    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.35373314E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.63283143E-05    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.33399129E-12    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.13572498E-09    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.29703312E-06    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     6.85729278E-09    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.93815628E-10    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.84696254E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.84696254E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.84696254E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.84696254E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.85686064E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.70109286E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.94239659E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.40783112E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.64092889E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     7.25385828E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.22031436E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.10931396E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.70502559E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.16540168E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.51780399E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.52260202E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.82952842E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.88405422E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.58378507E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.76830163E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.73063084E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.20333849E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.31502199E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.29437079E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     9.03239185E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.05122944E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.32007593E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.24196224E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.35945895E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.94798380E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.29933109E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.02478323E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.62968362E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.51967989E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.41512089E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.38152450E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.09737507E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.55200776E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59734204E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.28546659E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.23346482E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.18120766E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.18864793E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.67115247E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.87815605E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.89379376E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.13515926E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.61429801E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.27232899E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02902291E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.05773840E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.13405752E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.49651594E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.82979323E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.90156666E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60361184E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.18871669E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.02461679E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.84909731E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.86784202E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.13978956E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.67353761E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.27772486E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02945216E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.99661356E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.49860644E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.43251939E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.71464461E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.52064507E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89786669E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.18864793E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.67115247E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.87815605E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.89379376E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.13515926E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.61429801E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.27232899E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02902291E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.05773840E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.13405752E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.49651594E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.82979323E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.90156666E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60361184E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.18871669E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.02461679E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.84909731E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.86784202E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.13978956E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.67353761E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.27772486E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02945216E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.99661356E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.49860644E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.43251939E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.71464461E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.52064507E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89786669E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.66124010E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.02377034E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78789577E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02262872E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71019472E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.15564427E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43322520E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.91693976E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.38304049E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.29836209E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.61688152E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.50016299E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.66124010E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.02377034E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78789577E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02262872E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71019472E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.15564427E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43322520E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.91693976E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.38304049E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.29836209E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.61688152E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.50016299E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.29427769E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78799611E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57654768E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.48772834E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.55574888E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.34921311E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.11889039E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.56759879E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.29170669E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.64849861E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.31842116E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.56456546E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.58974985E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.91268180E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.18695497E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.66143051E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.15198723E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.20190708E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.59863928E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72036736E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.96337850E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.42802751E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.66143051E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.15198723E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.20190708E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.59863928E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72036736E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.96337850E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.42802751E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.66391126E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.15091445E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.20078781E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.59249434E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71783404E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.88885780E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42299342E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.44115022E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.28630217E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.12629269E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.12629269E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.04209872E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.04209872E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03458698E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.82875397E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53448092E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08420139E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46191798E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.38667810E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53263213E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.94628493E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.63322156E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00467543E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88576628E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09558285E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.81238023E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.19762804E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.99111558E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.93937722E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.79561311E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.12567442E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.45763147E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.12567442E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.45763147E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33973444E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.32923545E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.32923545E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.29823506E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.64883116E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.64883116E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.64883116E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.20813287E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.20813287E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.20813287E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.20813287E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.06937769E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.06937769E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.06937769E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.06937769E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.81361441E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.81361441E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.02198788E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.15361355E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.61383983E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.85387647E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.44746162E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.00896117E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.26669763E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50312806E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.26669763E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50312806E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.10173798E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.60420507E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.60420507E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.56138036E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.35965589E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.35965589E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.35965589E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.99358886E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.58149340E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.99358886E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.58149340E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.20265815E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.89614734E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.89614734E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.80611869E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.17751649E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.17751649E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.17751649E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26626961E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26626961E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26626961E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26626961E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.22089279E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.22089279E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.22089279E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.22089279E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.18143657E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.18143657E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.82873586E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.74440008E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52191798E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.44060790E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46620398E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46620398E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10964165E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.00042584E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41975751E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.13976864E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.80541869E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.71466899E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.59359244E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.37225812E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22058408E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02107058E-01    2           5        -5   # BR(h -> b       bb     )
     6.22059629E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20181989E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65862523E-04    2           3        -3   # BR(h -> s       sb     )
     2.00982374E-02    2           4        -4   # BR(h -> c       cb     )
     6.63737614E-02    2          21        21   # BR(h -> g       g      )
     2.30543144E-03    2          22        22   # BR(h -> gam     gam    )
     1.62581262E-03    2          22        23   # BR(h -> Z       gam    )
     2.16531547E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80661453E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78099338E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46841017E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428219E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71215499E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547453E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668459E-05    2           4        -4   # BR(H -> c       cb     )
     9.96067706E-01    2           6        -6   # BR(H -> t       tb     )
     7.97759658E-04    2          21        21   # BR(H -> g       g      )
     2.71631785E-06    2          22        22   # BR(H -> gam     gam    )
     1.16020608E-06    2          23        22   # BR(H -> Z       gam    )
     3.34638171E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66862738E-04    2          23        23   # BR(H -> Z       Z      )
     9.02264970E-04    2          25        25   # BR(H -> h       h      )
     7.32829198E-24    2          36        36   # BR(H -> A       A      )
     2.94140494E-11    2          23        36   # BR(H -> Z       A      )
     6.67809259E-12    2          24       -37   # BR(H -> W+      H-     )
     6.67809259E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381296E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47132966E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898341E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62268743E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677685E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179019E-06    2           4        -4   # BR(A -> c       cb     )
     9.96998530E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678050E-04    2          21        21   # BR(A -> g       g      )
     3.14195724E-06    2          22        22   # BR(A -> gam     gam    )
     1.35279962E-06    2          23        22   # BR(A -> Z       gam    )
     3.26108386E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472105E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35838779E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238169E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81146960E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49937954E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695419E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731881E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402354E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34225517E-04    2          24        25   # BR(H+ -> W+      h      )
     2.78926423E-13    2          24        36   # BR(H+ -> W+      A      )
