
#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

H_Mass = 700.0
H_Width = 0.00407 

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = H_Mass
PowhegConfig.width_H = H_Width

# CPS for the BSM Higgs
PowhegConfig.whiggsfixedwidth = 0
PowhegConfig.complexpolescheme = 0 

 # Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 16.

# Generate Powheg events
PowhegConfig.generate()

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 25 25', # H->hh

                             '25:onMode = off', 
                             '25:oneChannel = 1 0.5 100 5 -5', #h->bb
                             '25:addChannel = 1 0.5 100 15 -15', #h->tautau
                             '25:m0 125.0', #scalar mass
                             '25:mMin 124.5', #scalar mass
                             '25:mMax 125.5', #scalar mass
                             '25:mWidth 0.01', # narrow width
                             '25:tau0 0', #scalarlife time
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H(700GeV)->hh->bbtautau, hadhad"
evgenConfig.inputconfcheck="VBF700_hh_bbtautau_13TeV"
evgenConfig.generators = [ "Powheg", "Pythia8", "EvtGen"]
evgenConfig.keywords = [ "BSMHiggs", "VBF" ]
evgenConfig.contact     = [ 'puja.saha@cern.ch']



if not hasattr(filtSeq, "XtoVVDecayFilter"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
    filtSeq += XtoVVDecayFilter("scalarFilter")
filtSeq.scalarFilter.PDGGrandParent = 35
filtSeq.scalarFilter.PDGParent = 25
filtSeq.scalarFilter.StatusParent = 22
filtSeq.scalarFilter.PDGChild1 = [15]
filtSeq.scalarFilter.PDGChild2 = [5]



#------------------------------------------------------------
#XtoVVDecayFilterExtended
#------------------------------------------------------------
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [24,211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [24,211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]

