#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373692E+01   # W+
        25     1.26000000E+02   # h
        35     2.00414924E+03   # H
        36     2.00000000E+03   # A
        37     2.00208086E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.38578864E+03   # ~d_L
   2000001     4.38566553E+03   # ~d_R
   1000002     4.38551354E+03   # ~u_L
   2000002     4.38557890E+03   # ~u_R
   1000003     4.38578864E+03   # ~s_L
   2000003     4.38566553E+03   # ~s_R
   1000004     4.38551354E+03   # ~c_L
   2000004     4.38557890E+03   # ~c_R
   1000005     4.38561735E+03   # ~b_1
   2000005     4.38583846E+03   # ~b_2
   1000006     4.53305145E+03   # ~t_1
   2000006     4.86933252E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     4.51228808E+03   # ~g
   1000022     1.91288013E+02   # ~chi_10
   1000023    -2.16728683E+02   # ~chi_20
   1000025     2.96404511E+02   # ~chi_30
   1000035     3.12904268E+03   # ~chi_40
   1000024     2.14499265E+02   # ~chi_1+
   1000037     3.12904227E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69091198E-01   # N_11
  1  2    -2.32983809E-02   # N_12
  1  3     6.31934218E-01   # N_13
  1  4    -6.16497995E-01   # N_14
  2  1     1.80291953E-02   # N_21
  2  2    -4.70672299E-03   # N_22
  2  3    -7.05102258E-01   # N_23
  2  4    -7.08860777E-01   # N_24
  3  1     8.82965582E-01   # N_31
  3  2     1.29505197E-02   # N_32
  3  3    -3.21321467E-01   # N_33
  3  4     3.41989152E-01   # N_34
  4  1     4.21093602E-04   # N_41
  4  2    -9.99633591E-01   # N_42
  4  3    -1.55713086E-02   # N_43
  4  4     2.21368648E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20137447E-02   # U_11
  1  2     9.99757668E-01   # U_12
  2  1     9.99757668E-01   # U_21
  2  2     2.20137447E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13024030E-02   # V_11
  1  2     9.99509960E-01   # V_12
  2  1     9.99509960E-01   # V_21
  2  2     3.13024030E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98232637E-01   # cos(theta_t)
  1  2    -5.94272869E-02   # sin(theta_t)
  2  1     5.94272869E-02   # -sin(theta_t)
  2  2     9.98232637E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.70758893E-01   # cos(theta_b)
  1  2     8.82261903E-01   # sin(theta_b)
  2  1    -8.82261903E-01   # -sin(theta_b)
  2  2     4.70758893E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84680323E-01   # cos(theta_tau)
  1  2     7.28843505E-01   # sin(theta_tau)
  2  1    -7.28843505E-01   # -sin(theta_tau)
  2  2     6.84680323E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90178663E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51761554E+02   # vev(Q)              
         4     3.83357140E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099943E-01   # gprime(Q) DRbar
     2     6.29226199E-01   # g(Q) DRbar
     3     1.06610560E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02610895E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71914083E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79971720E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02067344E+06   # M^2_Hd              
        22    -4.67470313E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37962267E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.73433635E+00   # gluino decays
#          BR         NDA      ID1       ID2
     4.98353486E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.98353486E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99310143E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99310143E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.00492446E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.00492446E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.99983856E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.99983856E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.98353486E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.98353486E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99310143E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99310143E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.00492446E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.00492446E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.99983856E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.99983856E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.47035809E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.47035809E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.50483237E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     4.50483237E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     1.24021854E-03    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     7.46107788E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.04568370E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.23013066E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.23375416E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.40711398E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     6.30852031E-03    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.28017297E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
    -1.09425218E-02    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
    -3.84112871E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     1.51356670E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.92584079E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23953614E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.48289468E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.23640422E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.42566397E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.15026778E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.68785260E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.04752669E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.86637529E-03    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.66906601E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.83388161E-03    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005    -1.07024245E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
    -6.22804537E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
    -3.05119463E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
    -3.23286778E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.94034415E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.20526854E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     7.89315472E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     1.83668978E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.51090495E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.72153144E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -6.91113748E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.52304878E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.70615484E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.05468595E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     1.28830487E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.34275108E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.28784373E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.06118649E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.17193693E-01    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.51862230E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.34330781E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     9.24279828E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     2.20875055E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.25965385E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.78798936E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.39313772E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
#
#         PDG            Width
DECAY   1000001     1.28712708E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.58768597E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.39639859E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.96879294E-02    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.17682153E-01    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.24685321E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.35422240E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
#
#         PDG            Width
DECAY   2000001     2.31073768E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.20875018E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.25965345E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.78798973E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.39349269E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
#
#         PDG            Width
DECAY   1000004     1.28830487E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.34275108E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.28784373E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.06118649E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.17193693E-01    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.51862230E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.34330781E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     9.24279828E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.20875055E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.25965385E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.78798936E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.39313772E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
#
#         PDG            Width
DECAY   1000003     1.28712708E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.58768597E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.39639859E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.96879294E-02    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.17682153E-01    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.24685321E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.35422240E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
#
#         PDG            Width
DECAY   2000003     2.31073768E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.20875018E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.25965345E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.78798973E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.39349269E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
#
#         PDG            Width
DECAY   1000011     2.80544153E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.02921515E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04716588E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80182164E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484294E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77809394E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19343109E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46529150E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20752754E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25823955E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78921356E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60335421E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80544153E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.02921515E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04716588E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80182164E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484294E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77809394E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19343109E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46529150E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20752754E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25823955E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78921356E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60335421E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238981E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35893674E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64072466E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.73014385E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460459E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259385E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59108138E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677685E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64833106E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14070554E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06885703E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.46040141E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197257E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85043969E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604686E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80529555E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74616042E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53671530E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62292522E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698471E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37049222E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19023239E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80529555E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74616042E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53671530E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62292522E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698471E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37049222E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19023239E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80850463E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.73959469E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53495941E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62107082E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401732E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51122535E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430517E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26180846E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76542491E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34388484E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34388484E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462904E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462904E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08269568E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38814417E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03042736E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48726323E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98327218E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91549220E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03607102E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91835084E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94146136E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96112530E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90637468E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.85841361E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01630816E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94976308E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.27883047E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02572863E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93220979E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20615861E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.64454501E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.58332519E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49200234E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56525087E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66244388E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22032255E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57947414E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22032255E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57947414E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13462408E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60424866E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60424866E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50098629E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19584273E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19584273E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19584273E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.06325132E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.06325132E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.06325132E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.06325132E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.02108398E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.02108398E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.02108398E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.02108398E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95925304E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95925304E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.65809325E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.33583931E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.78320409E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.78320409E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.97244761E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.89186502E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.47624612E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38818063E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03705889E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97353182E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80698531E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97922530E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97922530E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49029092E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49713603E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70891634E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00951940E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60702265E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91126659E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.87795676E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81868486E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37349848E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93531061E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93531061E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43550542E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72677099E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.79015354E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93276852E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85654827E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21740430E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01639512E-01    2           5        -5   # BR(h -> b       bb     )
     6.22485152E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20332605E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66181809E-04    2           3        -3   # BR(h -> s       sb     )
     2.01140212E-02    2           4        -4   # BR(h -> c       cb     )
     6.64457301E-02    2          21        21   # BR(h -> g       g      )
     2.32181081E-03    2          22        22   # BR(h -> gam     gam    )
     1.62678822E-03    2          22        23   # BR(h -> Z       gam    )
     2.16829799E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80873091E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01389763E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38852734E-03    2           5        -5   # BR(H -> b       bb     )
     2.32153316E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20748403E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05084831E-06    2           3        -3   # BR(H -> s       sb     )
     9.48264733E-06    2           4        -4   # BR(H -> c       cb     )
     9.38267707E-01    2           6        -6   # BR(H -> t       tb     )
     7.51246633E-04    2          21        21   # BR(H -> g       g      )
     2.63373127E-06    2          22        22   # BR(H -> gam     gam    )
     1.09173796E-06    2          23        22   # BR(H -> Z       gam    )
     3.08617060E-04    2          24       -24   # BR(H -> W+      W-     )
     1.53887568E-04    2          23        23   # BR(H -> Z       Z      )
     8.52590668E-04    2          25        25   # BR(H -> h       h      )
     8.34566441E-24    2          36        36   # BR(H -> A       A      )
     3.30574551E-11    2          23        36   # BR(H -> Z       A      )
     2.48006858E-12    2          24       -37   # BR(H -> W+      H-     )
     2.48006858E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49805222E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87604918E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75335661E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52461079E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45469627E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.46310217E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11443380E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05887578E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39126811E-03    2           5        -5   # BR(A -> b       bb     )
     2.29773388E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12331930E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07094237E-06    2           3        -3   # BR(A -> s       sb     )
     9.38487022E-06    2           4        -4   # BR(A -> c       cb     )
     9.39259072E-01    2           6        -6   # BR(A -> t       tb     )
     8.89026556E-04    2          21        21   # BR(A -> g       g      )
     2.67566190E-06    2          22        22   # BR(A -> gam     gam    )
     1.27308772E-06    2          23        22   # BR(A -> Z       gam    )
     3.00720456E-04    2          23        25   # BR(A -> Z       h      )
     5.99327853E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30058482E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12629802E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.73968237E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19722794E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48917507E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69832260E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97848160E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23183741E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34660371E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29609179E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41897188E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13758296E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02368277E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40923585E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.08194929E-04    2          24        25   # BR(H+ -> W+      h      )
     1.28927230E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28567749E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26670539E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55367501E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
