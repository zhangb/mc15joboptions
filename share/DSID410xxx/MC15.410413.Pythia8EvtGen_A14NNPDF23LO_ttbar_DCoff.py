import subprocess, os

evgenConfig.description = "Pythia8 ttbar production with A14 tune, at least one lepton filter and the deadcone effect off"
evgenConfig.keywords = ["ttbar", "SM", "QCD"]
evgenConfig.contact     = [ 'jrawling@cern.ch' ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["Top:gg2ttbar = on",
                            "Top:qqbar2ttbar = on",
                            "TimeShower:MEextended = off",
                            "TimeShower:recoilDeadCone = off",
							]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.