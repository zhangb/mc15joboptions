model="InelasticVectorEFT"
mDM1 = 275.
mDM2 = 1100.
mZp = 550.
mHD = 125.
widthZp = 2.596678e+00
widthN2 = 6.065915e-01
filteff = 9.90099e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
