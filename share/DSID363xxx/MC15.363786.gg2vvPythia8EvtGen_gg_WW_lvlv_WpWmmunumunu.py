
# gg2VV 3.1.6 + Pythia8 gg->WW : WpWmmunumunu
# Notes: 
# - Include non-resonant gg->WW (via BOX diagram) + resonant gg->H->WW + interference
# - Generator Cut: invariance of dilepton from W decays larger than 4 GeV, m(ll) > 4 GeV 
# - CT10 PDF used at Matrix-element calculation of gg2VV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'gg2vv', 'Pythia8' ]
evgenConfig.description = 'gg2VV 3.1.6 + Pythia8, gg->WW; WpWmmunumunu; Include non-resonant gg->WW (via BOX diagram) + resonant gg->H->WW + interference; Generator cut of m(ll) > 4 GeV, l=e,mu,tau'
evgenConfig.keywords = ['diboson', 'SM', 'electroweak', 'WW', '2lepton']
evgenConfig.contact = ['jochen.meyer@cern.ch', 'yusheng.wu@cern.ch']
evgenConfig.inputfilecheck = 'gg2VV0316.363786.WpWmmunumunu_13TeV'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_LHEF.py')

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]

