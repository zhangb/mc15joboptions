from MadGraphControl.MadGraphUtils import *

name="4top_CI"

#--------------------------------------------------------------
# MADGRAPH                                                                                    
#--------------------------------------------------------------

### Set generation card
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
    import model 4topVectSing_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > t t~ t t~    
    output -f
    """)
fcard.close()

### Read the proc_card and generate the directory (containing diagramms)
process_dir = new_process()

### Build run_card.dat
nevents = runArgs.maxEvents

beamEnergy = 6500
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RunTimeError("No center of mass energy found.")

extras = { 'lhe_version'  : '2.0',
           'pdlabel'      : "'nn23lo1'" }

build_run_card(run_card_old=get_default_runcard(),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,
               extras=extras)
print_cards()

# Run the hard process generation (ie ./generate_event of MG)
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=0,njobs=1,run_name='Test',proc_dir=process_dir)


#--------------------------------------------------------------
# SHOWERING + HADRONIZATION PYTHIA
#--------------------------------------------------------------

### Set the output name
stringy = 'madgraph.' + str(runArgs.runNumber) + '.MadGraph_' + str(name)
arrange_output( run_name = 'Test' , proc_dir = process_dir , outputDS = stringy + '._00001.events.tar.gz' )

### PYTHIA 8 
evgenConfig.description = 'MadGraph_' + str(name)
evgenConfig.keywords+=['BSM','top']
evgenConfig.contact = ['romain.madar@cern.ch']
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile= stringy + '._00001.events.tar.gz'

### NNPDF
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
