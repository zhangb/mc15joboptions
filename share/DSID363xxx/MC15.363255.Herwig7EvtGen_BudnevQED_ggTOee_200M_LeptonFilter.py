# based on the JobOptions MC15.363685
evgenConfig.generators += ["Herwig7", "EvtGen"]
evgenConfig.description = "gammagamma->ee with Budnev parameterization, M>200 GeV, central lepton filter pt>9 GeV"
evgenConfig.keywords    = ["QED", "2lepton", "exclusive", "diphoton"]
evgenConfig.contact     = ["Oldrich Kepka (oldrich.kepka@cern.ch), Tetiana Moskalets (tetiana.moskalets@cern.ch)"]

# Add common job option for gamma gamma -> ll processes. Min/Max and lepton flavour will be determined from the JO name
include("MC15JobOptions/Herwig71_QED_EvtGen_ll.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 9000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

evgenConfig.minevents = 5000
