
model  = 'dmA'
mR     = 950
mDM    = 10000
gSM    = 0.05
gDM    = 1.00
widthR = 1.097535303

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet.py")

evgenConfig.description = "DM axial Z'->jj, m_{R}=950 g_{SM}=0.05 m_{DM}=10000 g_{DM}=1.00"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = "p p > xi, xi > j j"
evgenConfig.contact = ["Caterina Doglioni <caterina.doglioni@cern.ch>"]
