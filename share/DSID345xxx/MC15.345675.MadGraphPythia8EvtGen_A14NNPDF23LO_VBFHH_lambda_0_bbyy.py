from AthenaCommon.AppMgr import ServiceMgr
ServiceMgr.MessageSvc.OutputLevel = DEBUG

from MadGraphControl.MadGraphUtils import *

mode=0

#---------------------------------------------------------------------------------------------------
# Setting mHH and WHH for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters={'1560':'1.000000e+03', #MHH
            '188': '0.000000e+00', #ctr
            '1561':'4.000000e-03', #WHH
            '190':'1.000000e-07',  #BSM H to 2 SM H. set to 10^-7. 
            '191':'1.000000e-07'}  #BSM H to top

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------

extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',      
           'parton_shower':'PYTHIA8',
           'ptj':'0',
           'ptb':'0',
           'pta':'0',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'-1',
           'etab':'-1',
           'etaa':'-1',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'0',
           'mmaamax':'-1',
           'mmbb':'0',
           'mmbbmax':'-1',
           'drjj':'0',
           'drbb':'0',
           'draa':'0',
           'drbj':'0',
           'draj':'0',
           'drab':'0',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1' }

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
#---------------------------------------------------------------------------------------------------
# generate p p > h h j j QCD=0 forces EW production

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~ 
    define j = g u c d s u~ c~ d~ s~ 
    import model HeavyHiggsTHDM
    generate p p > h h j j QCD=0 
    output -f""")
fcard.close()

beamEnergy=6500
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

runName='run_01'

process_dir = new_process(card_loc='proc_card_mg5.dat')

#---------------------------------------------------------------------------------------------------
# Filter efficiency is low.
# Thus, setting the number of generated events to 50 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
nevents=40000

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------

build_param_card(param_card_old='param_card.HeavyScalar.dat',param_card_new='param_card_new.dat',masses=higgsMass,extras=parameters)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir), run_card_new='run_card.dat', xqcut=0,
               nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy,
               extras=extras)
   
print_cards()

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
# Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts


#### Ev Gen Configuration
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_vbfhh_bbyy'
evgenConfig.keywords+=['Higgs', "hh"]
evgenConfig.contact = ['Tyler James Burch <tyler.james.burch@cern.ch>']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py") 
genSeq.Pythia8.Commands += ["TimeShower:globalRecoil=On"]

#---------------------------------------------------------------------------------------------------                                      
# Decaying hh to bbyy with Pythia8                                                                                                        
#---------------------------------------------------------------------------------------------------                                      
genSeq.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ",  # bb decay                                                               
                            "25:addChannel = on 0.5 100 22 22 "] # gammagamma decay   
#---------------------------------------------------------------------------------------------------
# Generator Filters
# Use ParentTwoChildren filter to require:
#   Higgs(25) -> b(5)bbar(-5) AND
#   Higgs(25) -> gamma(22)gamma(22)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentTwoChildrenFilter
filtSeq += ParentTwoChildrenFilter("HiggsToBBGamGamFilter")
filtSeq.HiggsToBBGamGamFilter.PDGParent = [25]
filtSeq.HiggsToBBGamGamFilter.PDGChild = [5,22]
#
