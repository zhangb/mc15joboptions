#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14031608E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.32990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04171641E+01   # W+
        25     1.25543689E+02   # h
        35     4.00000931E+03   # H
        36     3.99999736E+03   # A
        37     4.00105816E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02860363E+03   # ~d_L
   2000001     4.02468099E+03   # ~d_R
   1000002     4.02795162E+03   # ~u_L
   2000002     4.02541016E+03   # ~u_R
   1000003     4.02860363E+03   # ~s_L
   2000003     4.02468099E+03   # ~s_R
   1000004     4.02795162E+03   # ~c_L
   2000004     4.02541016E+03   # ~c_R
   1000005     1.07507714E+03   # ~b_1
   2000005     4.02687131E+03   # ~b_2
   1000006     1.05671600E+03   # ~t_1
   2000006     1.97881914E+03   # ~t_2
   1000011     4.00511668E+03   # ~e_L
   2000011     4.00344200E+03   # ~e_R
   1000012     4.00400304E+03   # ~nu_eL
   1000013     4.00511668E+03   # ~mu_L
   2000013     4.00344200E+03   # ~mu_R
   1000014     4.00400304E+03   # ~nu_muL
   1000015     4.00413593E+03   # ~tau_1
   2000015     4.00798439E+03   # ~tau_2
   1000016     4.00518941E+03   # ~nu_tauL
   1000021     1.98411797E+03   # ~g
   1000022     3.96336604E+02   # ~chi_10
   1000023    -4.44903371E+02   # ~chi_20
   1000025     4.60485482E+02   # ~chi_30
   1000035     2.05139384E+03   # ~chi_40
   1000024     4.42623268E+02   # ~chi_1+
   1000037     2.05155855E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.12247929E-01   # N_11
  1  2    -1.75908216E-02   # N_12
  1  3    -4.35279491E-01   # N_13
  1  4    -3.87911884E-01   # N_14
  2  1    -3.87249086E-02   # N_21
  2  2     2.36106747E-02   # N_22
  2  3    -7.04445208E-01   # N_23
  2  4     7.08307749E-01   # N_24
  3  1     5.82024465E-01   # N_31
  3  2     2.80051024E-02   # N_32
  3  3     5.60574201E-01   # N_33
  3  4     5.88404454E-01   # N_34
  4  1    -1.09813207E-03   # N_41
  4  2     9.99174066E-01   # N_42
  4  3    -6.72899286E-03   # N_43
  4  4    -4.00587255E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.52217658E-03   # U_11
  1  2     9.99954663E-01   # U_12
  2  1    -9.99954663E-01   # U_21
  2  2     9.52217658E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.66613426E-02   # V_11
  1  2    -9.98393456E-01   # V_12
  2  1    -9.98393456E-01   # V_21
  2  2    -5.66613426E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90721389E-01   # cos(theta_t)
  1  2    -1.35908533E-01   # sin(theta_t)
  2  1     1.35908533E-01   # -sin(theta_t)
  2  2     9.90721389E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998994E-01   # cos(theta_b)
  1  2    -1.41844950E-03   # sin(theta_b)
  2  1     1.41844950E-03   # -sin(theta_b)
  2  2     9.99998994E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06156265E-01   # cos(theta_tau)
  1  2     7.08056021E-01   # sin(theta_tau)
  2  1    -7.08056021E-01   # -sin(theta_tau)
  2  2    -7.06156265E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00255071E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40316078E+03  # DRbar Higgs Parameters
         1    -4.32990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43530351E+02   # higgs               
         4     1.60890043E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40316078E+03  # The gauge couplings
     1     3.62141608E-01   # gprime(Q) DRbar
     2     6.35971408E-01   # g(Q) DRbar
     3     1.02833740E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40316078E+03  # The trilinear couplings
  1  1     1.60564712E-06   # A_u(Q) DRbar
  2  2     1.60566233E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40316078E+03  # The trilinear couplings
  1  1     5.93147368E-07   # A_d(Q) DRbar
  2  2     5.93201880E-07   # A_s(Q) DRbar
  3  3     1.05855889E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40316078E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.30353758E-07   # A_mu(Q) DRbar
  3  3     1.31689826E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40316078E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.64934796E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40316078E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85222534E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40316078E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03145827E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40316078E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57091283E+07   # M^2_Hd              
        22    -1.42024622E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40320649E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.30552499E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40228853E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40228853E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59771147E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59771147E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.10228718E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.40042978E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.25865610E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.26216885E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07874527E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09696570E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.30736907E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18417831E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.75237630E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.32690101E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.58560359E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.14867119E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.24867138E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.14471897E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.94023204E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.03500736E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.24008012E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.87846805E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66206174E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57152732E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82133596E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59913457E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59880543E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65559114E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.17343240E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12590666E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.99176321E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.71351850E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.53884797E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.25880527E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78218978E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.60428399E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26259878E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63823865E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82329236E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.49618200E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.63446146E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51607695E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60363366E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.63236247E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.21575457E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.85269871E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.72822140E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45067193E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78239324E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.53735524E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.20366320E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.81052609E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82864199E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.55233395E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66726586E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51824954E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53693342E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.47318932E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.14266222E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.83182039E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.72081920E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85673554E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78218978E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.60428399E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26259878E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63823865E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82329236E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.49618200E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.63446146E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51607695E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60363366E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.63236247E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.21575457E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.85269871E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.72822140E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45067193E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78239324E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.53735524E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.20366320E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.81052609E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82864199E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.55233395E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66726586E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51824954E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53693342E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.47318932E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.14266222E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.83182039E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.72081920E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85673554E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14325603E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.95044797E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.21621613E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.44874547E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78137448E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.07067124E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57778695E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04349524E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.61301144E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.49542249E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.37202763E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.70405687E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14325603E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.95044797E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.21621613E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.44874547E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78137448E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.07067124E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57778695E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04349524E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.61301144E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.49542249E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.37202763E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.70405687E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08007661E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.38852891E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47454332E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.38503429E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40593894E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53551691E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81949184E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07254287E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.41987113E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97862123E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.17108609E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43769750E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90363565E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88311959E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14430761E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.15790569E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04198260E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.59262361E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78553564E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.20973123E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55477917E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14430761E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.15790569E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04198260E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.59262361E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78553564E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.20973123E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55477917E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46718805E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05039965E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.45242386E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16623766E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52794780E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.54441608E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04113475E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.15051205E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33598173E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33598173E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11200872E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11200872E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10401911E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.67286987E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.42953772E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.12427425E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.24870260E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.43507676E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.24468462E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.22168280E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.96382654E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.35120183E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.20264738E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.48074914E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18158913E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53293624E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18158913E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53293624E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40373799E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51652481E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51652481E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48229495E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03060692E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03060692E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03060680E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.61722050E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.61722050E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.61722050E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.61722050E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.72407534E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.72407534E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.72407534E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.72407534E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.19522150E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.19522150E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.33311300E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.21789280E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.47168322E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.69254113E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.46665446E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.69254113E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.46665446E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.31409048E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     7.74423697E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     7.74423697E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.75745158E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.60006041E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.60006041E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.60005537E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.28452984E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.66653089E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.28452984E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.66653089E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     6.34324555E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.82323139E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.82323139E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.54967249E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     7.64291672E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     7.64291672E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     7.64291679E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.65287379E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.65287379E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.65287379E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.65287379E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.88425855E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.88425855E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.88425855E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.88425855E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.74733720E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.74733720E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.67133910E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.41982860E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.60242660E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.99312318E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.21956498E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.21956498E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.44410568E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.80295482E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.08074195E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.66728646E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.66728646E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.67249028E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.67249028E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01559041E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91663238E-01    2           5        -5   # BR(h -> b       bb     )
     6.48833444E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29686562E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87097892E-04    2           3        -3   # BR(h -> s       sb     )
     2.11563303E-02    2           4        -4   # BR(h -> c       cb     )
     6.90965041E-02    2          21        21   # BR(h -> g       g      )
     2.38898819E-03    2          22        22   # BR(h -> gam     gam    )
     1.63415055E-03    2          22        23   # BR(h -> Z       gam    )
     2.20572610E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78880503E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51335082E+01   # H decays
#          BR         NDA      ID1       ID2
     3.60487180E-01    2           5        -5   # BR(H -> b       bb     )
     6.01377029E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12632352E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51507393E-04    2           3        -3   # BR(H -> s       sb     )
     7.05394312E-08    2           4        -4   # BR(H -> c       cb     )
     7.06951970E-03    2           6        -6   # BR(H -> t       tb     )
     7.35184328E-07    2          21        21   # BR(H -> g       g      )
     4.68472189E-09    2          22        22   # BR(H -> gam     gam    )
     1.81863643E-09    2          23        22   # BR(H -> Z       gam    )
     1.71157298E-06    2          24       -24   # BR(H -> W+      W-     )
     8.55193591E-07    2          23        23   # BR(H -> Z       Z      )
     6.87240422E-06    2          25        25   # BR(H -> h       h      )
    -2.69727712E-24    2          36        36   # BR(H -> A       A      )
     6.05369396E-20    2          23        36   # BR(H -> Z       A      )
     1.85990476E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56721637E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56721637E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.44156744E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.00295098E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.58006034E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.38930597E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.43835422E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.80965889E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.39021850E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.25587617E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.66124354E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.01961850E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     8.23467843E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     7.10398735E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.10398735E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.29610253E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51296489E+01   # A decays
#          BR         NDA      ID1       ID2
     3.60538051E-01    2           5        -5   # BR(A -> b       bb     )
     6.01421844E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12648030E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51566977E-04    2           3        -3   # BR(A -> s       sb     )
     7.10037747E-08    2           4        -4   # BR(A -> c       cb     )
     7.08390070E-03    2           6        -6   # BR(A -> t       tb     )
     1.45556209E-05    2          21        21   # BR(A -> g       g      )
     6.48831746E-08    2          22        22   # BR(A -> gam     gam    )
     1.60004482E-08    2          23        22   # BR(A -> Z       gam    )
     1.70809403E-06    2          23        25   # BR(A -> Z       h      )
     1.89124632E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56715153E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56715153E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.12613874E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.43827794E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.35222103E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.86324124E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     4.48102905E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.13669020E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.54144610E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.34698385E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.23312802E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.65516607E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.65516607E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53072491E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.79933093E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.99649571E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12021397E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.71156865E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20113974E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47113070E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.69143212E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.70456041E-06    2          24        25   # BR(H+ -> W+      h      )
     3.19602095E-14    2          24        36   # BR(H+ -> W+      A      )
     5.22909843E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.63766355E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.19583267E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.56436178E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.93917380E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55551885E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.88491306E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.24973043E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.47857431E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
