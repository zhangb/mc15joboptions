evgenConfig.description = "PYTHIA8+EVTGEN, ZH, Z->any, H->llgam mH=5000GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "2lepton", "photon" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 5000.

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 22 23', # Higgs decay
                             'HiggsSM:ffbar2HZ = on',
                             '23:onMode = off',
			     '23:mMin = 2.0',
                             '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]

if not hasattr(filtSeq, "ParentChildwStatusFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentChildwStatusFilter
    filtSeq += ParentChildwStatusFilter("ParentChildwStatusFilter")

## Add this filter to the algs required to be successful for streaming
filtSeq.ParentChildwStatusFilter.PDGParent  = [23]
filtSeq.ParentChildwStatusFilter.StatusParent = [22,52]
filtSeq.ParentChildwStatusFilter.PtMinParent =  -1e12
filtSeq.ParentChildwStatusFilter.PtMaxParent = 1e12
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 1e12
filtSeq.ParentChildwStatusFilter.MassMinParent = -1e12
filtSeq.ParentChildwStatusFilter.MassMaxParent = 1e12
filtSeq.ParentChildwStatusFilter.PDGChild = [11,13,15]
filtSeq.ParentChildwStatusFilter.PtMinChild = -1e12
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 1e12
evgenConfig.generators  = [ "Pythia8", "EvtGen"] 
