# based on MC15.361223 ...
## Job options file for Herwig 7, minimum bias production
include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_Common.py")

evgenConfig.description = "Minimum bias events with MMHT2014lo68cl PDF and H7UE tune."
evgenConfig.keywords = ["QCD", "minBias"]
evgenConfig.contact  = [ "orel.gueta@cern.ch" ]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

set /Herwig/Generators/LHCGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
# The default MinBias cuts of the Herwig authors are X1Min == X2Min == 0.055, but to be consistent with previous productions it is set to 0.01 here.
set /Herwig/Cuts/MinBiasCuts:X1Min 0.01
set /Herwig/Cuts/MinBiasCuts:X2Min 0.01

# These settings have no effect on the MinBiasCuts, it is set here just to avoid confusion in the log file.
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/PhotonKtCut:MinKT 0.0*GeV
"""

genSeq.Herwig7.Commands += cmds.splitlines()

evgenConfig.minevents = 5000

