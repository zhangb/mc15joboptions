##########################################################################################################
# 
# MCFM 8.0, gg->(H->)ZZ->4e
# m4l > 100 GeV, mll > 4 GeV,
# lepton pt > 2 GeV, |eta|<3 and leading lepton pt > 5 GeV
#

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'MCFM', 'Pythia8' ]
evgenConfig.description = 'MCFM, gg->(H->)ZZ->4e using CT10NNLO PDF, m(4l)>130 GeV'

evgenConfig.keywords = ['diboson', '4lepton', 'electroweak', 'Higgs', 'ZZ']
evgenConfig.contact = ['lailin.xu@cern.ch']
evgenConfig.inputfilecheck = 'MCFMPowhegPythia8EvtGen_ggH_gg_ZZ_5SMW_4e_m4l130'
evgenConfig.minevents = 5000

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_LHEF.py')

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]
