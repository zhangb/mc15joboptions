###############################################################
#
# Job options file
# Pythia8 Z'->emu 0 width
# July 2018
#===============================================================

evgenConfig.description = "Zp(500)->emu 0 width production with the A14 NNPDF23LO tune"
evgenConfig.process = "Zprime -> e+- mu-+"
evgenConfig.contact = [ "thrynova@mail.cern.ch" ]
evgenConfig.keywords = [ "electron", "muon", "exotic", "Zprime" ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",
				"Zprime:gmZmode = 3",
				"32:m0 = 500.0", # set Z' mass
                                "PhaseSpace:mHatMin = 499.99",         # set lower invariant mass
                                "PhaseSpace:mHatMax = 500.01",       # set upper invariant mass
				"32:8:products = 11 -13",
				"32:10:products = 13 -11",
				"32:0:onMode = 0",
				"32:1:onMode = 0",
				"32:2:onMode = 0",
				"32:3:onMode = 0",
				"32:4:onMode = 0",
				"32:5:onMode = 0",
				"32:6:onMode = 0",
				"32:7:onMode = 0",
				"32:9:onMode = 0",
				"32:11:onMode = 0",
				"32:13:onMode = 0",
				"32:12:onMode = 0"]


#==============================================================
#
# End of job options file
#
###############################################################

