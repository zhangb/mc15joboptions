#--------------------------------------------------------------                                                                                                       # Configuration for EvgenJobTransforms                                                                                                                                #--------------------------------------------------------------                                                                                                        
evgenConfig.generators  += ["MadGraph", "Pythia8"]
evgenConfig.description  = 'MG5_aMC@NLO+Pythia8 ttbar + gamma allhadronic, A14 tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'allHadronic']
evgenConfig.contact      = [ 'andrey.loginov@yale.edu', 'bnachman@cern.ch' ]

#--------------------------------------------------------------
# Showering with Pythia 8, A14 tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

