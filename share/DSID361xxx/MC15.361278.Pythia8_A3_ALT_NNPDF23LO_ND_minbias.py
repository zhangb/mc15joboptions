# Pythia8 minimum bias ND with A3

evgenConfig.description = "ND Minbias with Pythia8 A3"
evgenConfig.keywords = ["minBias","diffraction","ND"]
evgenConfig.generators = ["Pythia8"]

include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")


genSeq.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]

