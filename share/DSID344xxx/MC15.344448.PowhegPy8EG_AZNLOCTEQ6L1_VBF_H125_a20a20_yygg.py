#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

H_Mass = 125.0
H_Width = 0.00407

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = H_Mass
PowhegConfig.width_H = H_Width

# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 1

#
PowhegConfig.withdamp = 1
PowhegConfig.ncall1 = 10000
PowhegConfig.ncall2 = 10000
PowhegConfig.nubound = 100000

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.


# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 36 36', # h->aa

                             '36:onMode = off', # decay of the a
                             '36:oneChannel = 1 0.5 100 22 22', #a->gamgam
                             '36:addChannel = 1 0.5 100 21 21', #a->gg
                             '36:m0 20.0', #scalar mass
                             '36:mMin 19.5', #scalar mass
                             '36:mMax 20.5', #scalar mass
                             '36:mWidth 0.01', # narrow width
                             '36:tau0 0', #scalarlife time
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->aa->gamgamgg"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"]
evgenConfig.contact     = [ 'rubbo@cern.ch' ]

if not hasattr(genSeq, "XtoVVDecayFilter"):
  from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
  genSeq += XtoVVDecayFilter()
  
### Add this filter to the algs required to be successful for streaming
#if "XtoVVDecayFilter" not in StreamEVGEN.RequireAlgs:
#    StreamEVGEN.RequireAlgs += ["XtoVVDecayFilter"]
genSeq.XtoVVDecayFilter.PDGGrandParent = 35
genSeq.XtoVVDecayFilter.PDGParent = 36
genSeq.XtoVVDecayFilter.StatusParent = 22
genSeq.XtoVVDecayFilter.PDGChild1 = [22]
genSeq.XtoVVDecayFilter.PDGChild2 = [21]
