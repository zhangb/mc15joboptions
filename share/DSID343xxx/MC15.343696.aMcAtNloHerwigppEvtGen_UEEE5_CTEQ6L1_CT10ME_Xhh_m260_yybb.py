from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
set /Herwig/EventHandlers/LHEReader:AllowedToReOpen 0
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma; h0->b,bbar;
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
"""


# LHEHandler generated events/number of attempts ~75%
safefactor = 3 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> b bbar, yybb"
evgenConfig.keywords = ["BSM", "BSMHiggs", "resonance", "bbbar", "diphoton"]
run_number_min = 343696 
run_number_max = 343703 
offset = 0

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "hbbFilter and hyyFilter"

