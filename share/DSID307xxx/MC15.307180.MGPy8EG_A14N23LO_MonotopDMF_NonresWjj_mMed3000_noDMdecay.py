include("MC15JobOptions/MadGraphControl_MonotopDMF_nonres_Wjj_noDMdecay.py")

evgenConfig.contact  = ["renjie.wang@cern.ch"]
evgenConfig.description = "Nonresonant Model for MonoTop(W->jj) with mDM="+str(mDM)+"GeV and mMed="+str(mMed)+"GeV"
evgenConfig.keywords = ["exotic"]

