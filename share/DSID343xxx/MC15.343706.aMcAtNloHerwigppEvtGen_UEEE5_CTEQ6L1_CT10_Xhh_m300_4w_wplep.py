from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff On
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff On
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff On
set /Herwig/Particles/W+/W+->u,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->c,sbar;:OnOff Off
set /Herwig/Particles/W+/W+->u,sbar;:OnOff Off
set /Herwig/Particles/W+/W+->c,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->c,bbar;:OnOff Off
##set W- decay
set /Herwig/Particles/W-/W-->ubar,d;:OnOff On
set /Herwig/Particles/W-/W-->cbar,s;:OnOff On
set /Herwig/Particles/W-/W-->ubar,s;:OnOff On
set /Herwig/Particles/W-/W-->cbar,d;:OnOff On
set /Herwig/Particles/W-/W-->cbar,b;:OnOff On
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff Off
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff Off
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff Off
"""



# LHEHandler generated events/number of attempts ~75%
safefactor = 1.5 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> W+ W-, W+ -> lep, W- -> jj"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "W", "WW"] 
evgenConfig.generators  = [  "aMcAtNlo", "Herwigpp", "EvtGen"] 
run_number_min = 343704 
run_number_max = 343711
offset = 0

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")




