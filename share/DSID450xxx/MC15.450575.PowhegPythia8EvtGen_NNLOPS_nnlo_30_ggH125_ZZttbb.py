#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_ggF_H_Common.py')

evgenConfig.process     = "ggH H->ZZ->tauleptaulepbb"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+jet production with NNLOPS and A14 tune, HZZtauleptaulepbb filtered mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZZ", "mH125" ]
evgenConfig.contact     = [ 'christopher.hayes@cern.ch, ljiljana.morvaj@cern.ch' ]
evgenConfig.minevents   = 5000
evgenConfig.inputFilesPerJob = 105
#evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off', #decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 15 15',
                             '23:onIfMatch = 5 5' ]

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
filtSeq += XtoVVDecayFilter()

filtSeq.XtoVVDecayFilter.PDGGrandParent = 25
filtSeq.XtoVVDecayFilter.PDGParent = 23
filtSeq.XtoVVDecayFilter.StatusParent = 22
filtSeq.XtoVVDecayFilter.PDGChild1 = [15]
filtSeq.XtoVVDecayFilter.PDGChild2 = [5]

from GeneratorFilters.GeneratorFiltersConf import TauFilter
lfvfilter = TauFilter("lfvfilter")
filtSeq += lfvfilter
filtSeq.lfvfilter.UseNewOptions = True
filtSeq.lfvfilter.Ntaus = 2
filtSeq.lfvfilter.Nleptaus = 2
filtSeq.lfvfilter.Nhadtaus = 0
filtSeq.lfvfilter.EtaMaxlep = 2.9
filtSeq.lfvfilter.EtaMaxhad = 2.9
filtSeq.lfvfilter.Ptcutlep = 2000.0 #MeV                                                                       
filtSeq.lfvfilter.Ptcutlep_lead = 2000.0 #MeV
filtSeq.lfvfilter.Ptcuthad = 12500.0 #MeV
filtSeq.lfvfilter.Ptcuthad_lead = 12500.0 #MeV
