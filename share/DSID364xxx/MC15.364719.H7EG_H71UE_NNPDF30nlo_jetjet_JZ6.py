evgenConfig.description = "Herwig7 multijets with pdf NNPDF30nlo, slice JZ6"
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.keywords+=['QCD', 'jets']
evgenConfig.contact = ['jdickinson@lbl.gov']
evgenConfig.minevents = 5000

include("MC15JobOptions/Herwig71EvtGen_H7UE_NNPDF30nlo_jetjet.py");
include("MC15JobOptions/JetFilter_JZ6.py")
