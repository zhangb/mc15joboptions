#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13078524E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.18900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.26485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04195386E+01   # W+
        25     1.25454906E+02   # h
        35     4.00001199E+03   # H
        36     3.99999740E+03   # A
        37     4.00101302E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02610938E+03   # ~d_L
   2000001     4.02276199E+03   # ~d_R
   1000002     4.02546097E+03   # ~u_L
   2000002     4.02329079E+03   # ~u_R
   1000003     4.02610938E+03   # ~s_L
   2000003     4.02276199E+03   # ~s_R
   1000004     4.02546097E+03   # ~c_L
   2000004     4.02329079E+03   # ~c_R
   1000005     2.29222435E+03   # ~b_1
   2000005     4.02585630E+03   # ~b_2
   1000006     8.07377801E+02   # ~t_1
   2000006     2.30277525E+03   # ~t_2
   1000011     4.00446629E+03   # ~e_L
   2000011     4.00362757E+03   # ~e_R
   1000012     4.00335703E+03   # ~nu_eL
   1000013     4.00446629E+03   # ~mu_L
   2000013     4.00362757E+03   # ~mu_R
   1000014     4.00335703E+03   # ~nu_muL
   1000015     4.00429423E+03   # ~tau_1
   2000015     4.00803473E+03   # ~tau_2
   1000016     4.00477086E+03   # ~nu_tauL
   1000021     1.99472493E+03   # ~g
   1000022     3.90046480E+02   # ~chi_10
   1000023    -4.30752615E+02   # ~chi_20
   1000025     4.52542488E+02   # ~chi_30
   1000035     2.06669207E+03   # ~chi_40
   1000024     4.28400374E+02   # ~chi_1+
   1000037     2.06685898E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.40993243E-01   # N_11
  1  2     2.04334707E-02   # N_12
  1  3     4.96446692E-01   # N_13
  1  4     4.51721340E-01   # N_14
  2  1    -3.93970441E-02   # N_21
  2  2     2.37429502E-02   # N_22
  2  3    -7.04336908E-01   # N_23
  2  4     7.08373959E-01   # N_24
  3  1     6.70354896E-01   # N_31
  3  2     2.56150134E-02   # N_32
  3  3     5.07354863E-01   # N_33
  3  4     5.40887445E-01   # N_34
  4  1    -1.09558112E-03   # N_41
  4  2     9.99180973E-01   # N_42
  4  3    -6.42225440E-03   # N_43
  4  4    -3.99366708E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.08826768E-03   # U_11
  1  2     9.99958701E-01   # U_12
  2  1    -9.99958701E-01   # U_21
  2  2     9.08826768E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.64885639E-02   # V_11
  1  2    -9.98403246E-01   # V_12
  2  1    -9.98403246E-01   # V_21
  2  2    -5.64885639E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.58611053E-02   # cos(theta_t)
  1  2     9.96307117E-01   # sin(theta_t)
  2  1    -9.96307117E-01   # -sin(theta_t)
  2  2    -8.58611053E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998196E-01   # cos(theta_b)
  1  2    -1.89947275E-03   # sin(theta_b)
  2  1     1.89947275E-03   # -sin(theta_b)
  2  2     9.99998196E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06170525E-01   # cos(theta_tau)
  1  2     7.08041799E-01   # sin(theta_tau)
  2  1    -7.08041799E-01   # -sin(theta_tau)
  2  2    -7.06170525E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00247178E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30785237E+03  # DRbar Higgs Parameters
         1    -4.18900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43703329E+02   # higgs               
         4     1.61871680E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30785237E+03  # The gauge couplings
     1     3.62014424E-01   # gprime(Q) DRbar
     2     6.35339202E-01   # g(Q) DRbar
     3     1.02925843E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30785237E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37070661E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30785237E+03  # The trilinear couplings
  1  1     5.05900733E-07   # A_d(Q) DRbar
  2  2     5.05947611E-07   # A_s(Q) DRbar
  3  3     9.04098983E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30785237E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11983215E-07   # A_mu(Q) DRbar
  3  3     1.13129241E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30785237E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66113322E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30785237E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86339158E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30785237E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03116304E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30785237E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57154486E+07   # M^2_Hd              
        22    -1.77897112E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.26485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40031451E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.98164272E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.21608678E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.04243319E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.02984008E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.91735349E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.01037325E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.56696206E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.30006110E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.06144209E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.12615169E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.45242256E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.77127574E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.07499984E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.36698584E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.43454089E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.47847158E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     7.39160879E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.35963801E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.12202393E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.19598595E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     4.38067595E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.11979150E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     6.29790200E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.40452225E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.90113569E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64738959E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64008584E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82462532E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54472796E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.77961924E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66307226E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.53647140E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12655815E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.31280913E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.73087870E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.07738565E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.02309288E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76413859E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.25465051E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.28272801E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.01472555E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80483564E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.51138734E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59761330E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52132169E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58937940E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.04760689E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.58034944E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.47840742E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.69754988E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44654016E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76433314E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.25195632E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.41889614E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.91085368E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81019119E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.42668641E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63024755E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52354115E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52294739E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.95026130E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.23834470E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.46538552E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.64401576E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85561960E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76413859E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.25465051E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.28272801E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.01472555E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80483564E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.51138734E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59761330E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52132169E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58937940E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.04760689E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.58034944E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.47840742E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.69754988E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44654016E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76433314E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.25195632E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.41889614E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.91085368E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81019119E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.42668641E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63024755E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52354115E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52294739E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.95026130E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.23834470E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.46538552E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.64401576E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85561960E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10866629E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.19188249E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.46962105E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.37425417E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77586517E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.34979876E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56667772E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04213630E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.50713758E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.55022563E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.47735356E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60250392E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10866629E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.19188249E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.46962105E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.37425417E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77586517E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.34979876E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56667772E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04213630E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.50713758E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.55022563E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.47735356E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60250392E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06327831E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.02636417E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49723110E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.77123970E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39644329E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.55811048E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80041867E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05589778E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.00260812E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02692065E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61099760E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42698580E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95104871E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86161154E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10976258E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.93673022E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.07749136E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.39726859E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77994024E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.22370902E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54364787E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10976258E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.93673022E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.07749136E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.39726859E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77994024E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.22370902E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54364787E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43340748E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.00335986E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.76284086E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.79639209E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52007493E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.64731202E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02545584E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.46026078E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33722851E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33722851E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11242276E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11242276E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10069746E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.63537497E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.23371297E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46772310E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.10065250E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40469969E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.48983480E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40430585E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.82440957E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.54878966E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18779209E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54114032E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18779209E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54114032E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36107550E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53627270E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53627270E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49143972E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07027097E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07027097E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07027085E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.80700597E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.80700597E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.80700597E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.80700597E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.93567023E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.93567023E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.93567023E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.93567023E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.76709653E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.76709653E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.40110347E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.39795899E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.52768116E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.98682153E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.53377222E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.98682153E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.53377222E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.43765197E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     5.47774545E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     5.47774545E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     5.51547025E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.17973350E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.17973350E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.17972330E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.15659746E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.79822316E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.15659746E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.79822316E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.79678347E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.42115917E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.42115917E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.18144376E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.28367056E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.28367056E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.28367058E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.23610045E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.23610045E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.23610045E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.23610045E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.12027457E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.12027457E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.12027457E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.12027457E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.01145924E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.01145924E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.63359597E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.42718990E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.62437269E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.03197199E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40554814E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40554814E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.74369933E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.26878945E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.98101171E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.59632397E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.59632397E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.02237811E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94980794E-01    2           5        -5   # BR(h -> b       bb     )
     6.47258965E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29129586E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85983814E-04    2           3        -3   # BR(h -> s       sb     )
     2.11086074E-02    2           4        -4   # BR(h -> c       cb     )
     6.89171716E-02    2          21        21   # BR(h -> g       g      )
     2.37635271E-03    2          22        22   # BR(h -> gam     gam    )
     1.61877910E-03    2          22        23   # BR(h -> Z       gam    )
     2.18020455E-01    2          24       -24   # BR(h -> W+      W-     )
     2.75368309E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44720605E+01   # H decays
#          BR         NDA      ID1       ID2
     3.58651362E-01    2           5        -5   # BR(H -> b       bb     )
     6.08679934E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15214482E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54561586E-04    2           3        -3   # BR(H -> s       sb     )
     7.13937737E-08    2           4        -4   # BR(H -> c       cb     )
     7.15514265E-03    2           6        -6   # BR(H -> t       tb     )
     8.54114934E-07    2          21        21   # BR(H -> g       g      )
     1.50139400E-09    2          22        22   # BR(H -> gam     gam    )
     1.84242093E-09    2          23        22   # BR(H -> Z       gam    )
     1.69185054E-06    2          24       -24   # BR(H -> W+      W-     )
     8.45339327E-07    2          23        23   # BR(H -> Z       Z      )
     6.61251046E-06    2          25        25   # BR(H -> h       h      )
     3.45589549E-24    2          36        36   # BR(H -> A       A      )
    -1.29766884E-20    2          23        36   # BR(H -> Z       A      )
     1.87896506E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57387913E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57387913E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.74218797E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.19993726E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.81691654E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.98360068E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.23236560E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.68581601E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.84214193E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.26227998E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.84612498E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.22097473E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.31742395E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     6.31742395E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.44615416E+01   # A decays
#          BR         NDA      ID1       ID2
     3.58746239E-01    2           5        -5   # BR(A -> b       bb     )
     6.08799797E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15256693E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54653079E-04    2           3        -3   # BR(A -> s       sb     )
     7.18748146E-08    2           4        -4   # BR(A -> c       cb     )
     7.17080256E-03    2           6        -6   # BR(A -> t       tb     )
     1.47341820E-05    2          21        21   # BR(A -> g       g      )
     6.46186683E-08    2          22        22   # BR(A -> gam     gam    )
     1.62034309E-08    2          23        22   # BR(A -> Z       gam    )
     1.68862355E-06    2          23        25   # BR(A -> Z       h      )
     1.90738884E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57402407E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57402407E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.37620541E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.67117815E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.54702980E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.39934871E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.46284369E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.09012748E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.06654166E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.40228051E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.29259468E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.52480309E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.52480309E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44946506E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.74426656E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.08584396E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15180533E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.67632747E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21903854E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50795429E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.65807680E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.68947421E-06    2          24        25   # BR(H+ -> W+      h      )
     2.60943222E-14    2          24        36   # BR(H+ -> W+      A      )
     4.34979626E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.25736188E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.21319932E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57525291E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.78682600E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56585788E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.15201416E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.31039120E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
