#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
if hasattr(PowhegConfig, "vdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.vdecaymode = 3  # tautau
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.running_width = 1

# Ensure identical run cards (for testing only)
PowhegConfig.bornktmin = -1
PowhegConfig.bottomthr = -1
PowhegConfig.bottomthrpdf = -1
PowhegConfig.btlscalect = -1
PowhegConfig.btlscalereal = -1
PowhegConfig.charmthr = -1
PowhegConfig.charmthrpdf = -1
if hasattr(PowhegConfig, "foldcsi"):
  PowhegConfig.foldcsi = 2
else:
  PowhegConfig.foldx = 2
PowhegConfig.hdamp = -1
PowhegConfig.itmx2 = 5
#PowhegConfig.iupperfsr = 2 # not user-configurable
#PowhegConfig.iupperisr = 1 # not user-configurable
PowhegConfig.mass_low = 60.0
PowhegConfig.ncall1 = 120000
PowhegConfig.ncall2 = 250000
PowhegConfig.nubound = 20000
#PowhegConfig.runningwidth = 0 # does not exist for monitored release
PowhegConfig.xupbound = 2

# Generate Powheg events
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.minevents = 1000
