
# Inverse of M scale
invMscale=0.00250000

# Wilson coefficients
c1=1.000000e+00
c2=0.000000e+00

evt_multiplier=10
filter_string="2L"

include("MC15JobOptions/MadGraphControl_DarkEnergy_ttphiphi.py")
