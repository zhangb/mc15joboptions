f = open("Bc_USER.DEC", "w")
f.write("Alias myJ/psi J/psi\n")
f.write("Alias myD_s*-  D_s*- \n")
f.write("Alias myD_s-  D_s- \n")
f.write("Alias myphi   phi  \n")
f.write("Decay B_c-\n")
f.write("1.0000    myJ/psi myD_s*-       SVV_HELAMP 0.0 0.0 0.0 0.0 1.0 0.0;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+ mu-               VLL;\n")
f.write("Enddecay\n")
f.write("Decay myD_s*-\n")
f.write("0.942000000 myD_s-    gamma     VSP_PWAVE;\n")
f.write("0.058000000 myD_s-    pi0       VSS;\n")
f.write("Enddecay\n")
f.write("Decay myD_s-\n")
f.write("1.0000    myphi pi-             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myphi\n")
f.write("1.0000    K+ K-                 VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_BCVEGPY.py")
include("MC15JobOptions/Pythia8_BcStates.py")

evgenConfig.description = "Bc- -> J/psi(mu3p5mu3p5) Ds*-(Ds-(phi(K+ K-) pi-) gamma/pi0) in H001"
evgenConfig.keywords = ["exclusive", "2muon", "Jpsi"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "BcMinus"

genSeq.EvtInclusiveDecay.userDecayFile = "Bc_USER.DEC"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG18.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG18.pdt"

include("MC15JobOptions/BSignalFilter.py")

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL1MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.7
filtSeq.BSignalFilter.LVL2MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.7

filtSeq.BSignalFilter.B_PDGCode = -541
