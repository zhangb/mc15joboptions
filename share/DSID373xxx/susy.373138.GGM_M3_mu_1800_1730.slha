#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.72000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.73000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05418075E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400196E+03   # H
        36     2.00000000E+03   # A
        37     2.00151529E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.96252848E+03   # ~d_L
   2000001     4.96241863E+03   # ~d_R
   1000002     4.96228285E+03   # ~u_L
   2000002     4.96234085E+03   # ~u_R
   1000003     4.96252848E+03   # ~s_L
   2000003     4.96241863E+03   # ~s_R
   1000004     4.96228285E+03   # ~c_L
   2000004     4.96234085E+03   # ~c_R
   1000005     4.96175991E+03   # ~b_1
   2000005     4.96318857E+03   # ~b_2
   1000006     5.07589672E+03   # ~t_1
   2000006     5.36671816E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007614E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007614E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99961788E+03   # ~tau_1
   2000015     5.00054101E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.88523365E+03   # ~g
   1000022     1.78454620E+03   # ~chi_10
   1000023    -1.83600505E+03   # ~chi_20
   1000025     1.87185860E+03   # ~chi_30
   1000035     3.12603781E+03   # ~chi_40
   1000024     1.83132445E+03   # ~chi_1+
   1000037     3.12603265E+03   # ~chi_2+
   1000039     2.85050000E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.31573096E-01   # N_11
  1  2    -3.94532356E-02   # N_12
  1  3     4.82635661E-01   # N_13
  1  4    -4.79903185E-01   # N_14
  2  1     2.38446817E-03   # N_21
  2  2    -3.12945723E-03   # N_22
  2  3    -7.07034547E-01   # N_23
  2  4    -7.07168063E-01   # N_24
  3  1     6.81755806E-01   # N_31
  3  2     4.53267922E-02   # N_32
  3  3    -5.15309776E-01   # N_33
  3  4     5.17310679E-01   # N_34
  4  1     2.03510369E-03   # N_41
  4  2    -9.98187923E-01   # N_42
  4  3    -4.02591959E-02   # N_43
  4  4     4.46757752E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.68697597E-02   # U_11
  1  2     9.98381606E-01   # U_12
  2  1     9.98381606E-01   # U_21
  2  2     5.68697597E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.31144970E-02   # V_11
  1  2     9.98006293E-01   # V_12
  2  1     9.98006293E-01   # V_21
  2  2     6.31144970E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99870400E-01   # cos(theta_t)
  1  2     1.60991678E-02   # sin(theta_t)
  2  1    -1.60991678E-02   # -sin(theta_t)
  2  2     9.99870400E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.79378575E-01   # cos(theta_b)
  1  2     7.33787947E-01   # sin(theta_b)
  2  1    -7.33787947E-01   # -sin(theta_b)
  2  2     6.79378575E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04798192E-01   # cos(theta_tau)
  1  2     7.09407858E-01   # sin(theta_tau)
  2  1    -7.09407858E-01   # -sin(theta_tau)
  2  2     7.04798192E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201446E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.73000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52277948E+02   # vev(Q)              
         4     3.18649091E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52729029E-01   # gprime(Q) DRbar
     2     6.26834251E-01   # g(Q) DRbar
     3     1.08213407E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02500260E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71526563E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79750937E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.72000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.06380118E+05   # M^2_Hd              
        22    -8.20569360E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36896473E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.81340334E-08   # gluino decays
#          BR         NDA      ID1       ID2
     7.33230255E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.95403342E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.15635765E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.86069770E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.74892005E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     7.19071587E-09    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.75743265E-07    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.67755729E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.49999983E-09    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.09705592E-07    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.74892005E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     7.19071587E-09    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.75743265E-07    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.67755729E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.49999983E-09    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.09705592E-07    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.47525630E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     5.30315527E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.47705041E-08    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.79204785E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.79204785E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.79204785E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.79204785E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.77475646E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.64824559E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.89913088E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.41547390E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.70383098E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.05987850E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.33668076E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.10412871E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.55100286E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.92190657E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.42252182E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.48699181E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.81240165E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.89450783E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.04900660E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.76463719E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.11422203E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.21008856E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.09209704E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.11381058E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.12318230E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.29553835E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.25317808E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.26648613E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.34618944E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.89547315E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.33796703E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.02738002E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.71204697E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.50526436E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.34056562E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.43112180E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.03322208E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.51279172E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.62829743E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.27245381E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.30121606E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.18425239E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.12694496E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.38126816E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.71986136E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.94372923E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.20362221E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.23521515E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.40984207E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.00759508E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.99474093E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.14462371E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.23904569E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.80761506E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.99953051E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60477308E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.12701884E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.08389510E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.39337069E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.41554137E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.20898815E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.25068690E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.41558695E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.00802892E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.93566358E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.52533496E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.76860412E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.65708245E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.06103654E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89817504E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.12694496E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.38126816E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.71986136E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.94372923E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.20362221E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.23521515E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.40984207E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.00759508E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.99474093E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.14462371E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.23904569E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.80761506E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.99953051E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60477308E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.12701884E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.08389510E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.39337069E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.41554137E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.20898815E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.25068690E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.41558695E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.00802892E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.93566358E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.52533496E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.76860412E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.65708245E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.06103654E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89817504E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.64786082E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.78615565E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.76550311E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.00408162E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72227974E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.57906949E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45921473E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.85936085E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.42526472E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.66550933E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.57465817E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.04571368E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.64786082E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.78615565E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.76550311E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.00408162E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72227974E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.57906949E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45921473E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.85936085E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.42526472E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.66550933E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.57465817E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.04571368E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.25836715E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.76227184E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52926423E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.43812844E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.57974041E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.73148997E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.16801513E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.95989963E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.25607142E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62502797E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.19454139E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.51371979E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.61372657E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.28255584E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.23604857E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.64807864E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14354688E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.10426506E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.24433516E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.73410711E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.40753775E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.45372669E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.64807864E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14354688E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.10426506E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.24433516E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.73410711E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.40753775E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.45372669E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.65048336E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14250937E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.10326319E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.23866983E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73162653E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.30843057E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44880249E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.39825516E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.03947760E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33596075E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33596075E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11198813E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11198813E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10409185E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.77153500E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53583180E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07353447E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46165531E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.39882714E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53015129E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.11668063E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.35497689E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.95813925E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00516359E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88617013E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.08666277E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.83540518E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.80211213E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.75500542E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.07208577E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.60513191E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18449140E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53380996E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18449140E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53380996E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41258954E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50330556E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50330556E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47126260E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99651745E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99651745E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99651745E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.50110775E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.50110775E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.50110775E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.50110775E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.50036935E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.50036935E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.50036935E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.50036935E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.51800568E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.51800568E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.79764399E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.67195887E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.84049326E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     6.88171337E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.16226458E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.81899529E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.51009857E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.79369864E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.51009857E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.79369864E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.28888585E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.11762026E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.11762026E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.43632167E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.77098348E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.77098348E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.77098348E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.06463679E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.67352204E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.06463679E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.67352204E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.26862814E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.10647673E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.10647673E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.01058546E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.21953021E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.21953021E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.21953021E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.37996425E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.37996425E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.37996425E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.37996425E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.59987437E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.59987437E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.59987437E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.59987437E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.55620576E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.55620576E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.77149740E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.12030399E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52327259E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.72816419E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46725663E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46725663E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09774410E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.67751852E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.43094406E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.66796883E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.49740184E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.20592840E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.29530820E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.18552163E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22104900E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02154888E-01    2           5        -5   # BR(h -> b       bb     )
     6.21989971E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20157333E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65810371E-04    2           3        -3   # BR(h -> s       sb     )
     2.00960402E-02    2           4        -4   # BR(h -> c       cb     )
     6.63667700E-02    2          21        21   # BR(h -> g       g      )
     2.30506670E-03    2          22        22   # BR(h -> gam     gam    )
     1.62564102E-03    2          22        23   # BR(h -> Z       gam    )
     2.16503575E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80630540E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78097590E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46769815E-03    2           5        -5   # BR(H -> b       bb     )
     2.46429164E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71218840E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547907E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668600E-05    2           4        -4   # BR(H -> c       cb     )
     9.96069009E-01    2           6        -6   # BR(H -> t       tb     )
     7.97756910E-04    2          21        21   # BR(H -> g       g      )
     2.71687433E-06    2          22        22   # BR(H -> gam     gam    )
     1.16024944E-06    2          23        22   # BR(H -> Z       gam    )
     3.34450961E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66769392E-04    2          23        23   # BR(H -> Z       Z      )
     9.01956166E-04    2          25        25   # BR(H -> h       h      )
     7.29015874E-24    2          36        36   # BR(H -> A       A      )
     2.92958725E-11    2          23        36   # BR(H -> Z       A      )
     6.61153056E-12    2          24       -37   # BR(H -> W+      H-     )
     6.61153056E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380953E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47061901E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898560E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269518E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677787E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179914E-06    2           4        -4   # BR(A -> c       cb     )
     9.96999425E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678897E-04    2          21        21   # BR(A -> g       g      )
     3.13814610E-06    2          22        22   # BR(A -> gam     gam    )
     1.35284522E-06    2          23        22   # BR(A -> Z       gam    )
     3.25926330E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471955E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35680137E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238488E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81148089E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49836423E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45696053E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732008E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402541E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34039649E-04    2          24        25   # BR(H+ -> W+      h      )
     2.80552032E-13    2          24        36   # BR(H+ -> W+      A      )
