#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp = 1.5*mtop, A14 tune, at least one lepton filter, colour-flipped'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk', 'fabian.wilk@cern.ch' ]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp        = 258.75 #1.5*172.5
# compensate filter efficiency
PowhegConfig.PDF = 260000

# Weight group configuration
PowhegConfig.define_event_weight_group( group_name='scales', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, NNPDF
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_NNPDF',              parameter_values=[ 2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_NNPDF',            parameter_values=[ 0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_NNPDF',              parameter_values=[ 1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_NNPDF',            parameter_values=[ 1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_NNPDF',     parameter_values=[ 0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_NNPDF',         parameter_values=[ 2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_NNPDF',       parameter_values=[ 0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_NNPDF',       parameter_values=[ 2.0, 0.5, 260000] )

# Scale variations, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='MMHT',                   parameter_values=[ 1.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_MMHT',              parameter_values=[ 2.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_MMHT',            parameter_values=[ 0.5, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_MMHT',              parameter_values=[ 1.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_MMHT',            parameter_values=[ 1.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_MMHT',     parameter_values=[ 0.5, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_MMHT',         parameter_values=[ 2.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_MMHT',       parameter_values=[ 0.5, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_MMHT',       parameter_values=[ 2.0, 0.5, 25200] )

# Scale variations, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='CT14',                   parameter_values=[ 1.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_CT14',              parameter_values=[ 2.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_CT14',            parameter_values=[ 0.5, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_CT14',              parameter_values=[ 1.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_CT14',            parameter_values=[ 1.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_CT14',     parameter_values=[ 0.5, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_CT14',         parameter_values=[ 2.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_CT14',       parameter_values=[ 0.5, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_CT14',       parameter_values=[ 2.0, 0.5, 13165] )

# Scale variations, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='PDF4LHC15',                   parameter_values=[ 1.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_PDF4LHC15',              parameter_values=[ 2.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_PDF4LHC15',            parameter_values=[ 0.5, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muR_PDF4LHC15',              parameter_values=[ 1.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muR_PDF4LHC15',            parameter_values=[ 1.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_0p5muR_PDF4LHC15',     parameter_values=[ 0.5, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_2muR_PDF4LHC15',         parameter_values=[ 2.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='0p5muF_2muR_PDF4LHC15',       parameter_values=[ 0.5, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales', weight_name='2muF_0p5muR_PDF4LHC15',       parameter_values=[ 2.0, 0.5, 90900] )

PowhegConfig.nEvents     *= 3.
PowhegConfig.generate()

#--------------------------------------------------------------
# Colour flipping
#--------------------------------------------------------------

import os, subprocess

os.rename('./PowhegOTF._1.events', './UnFlippedLHEFile.events')

# Attempt to retrieve the LHE parsing code and flip the colour connections
get_LHE_class = subprocess.Popen(['get_files','-data','ColourFlow_LHE.py'])
get_LHE_class.wait()
get_FlipLHE_class = subprocess.Popen(['get_files','-data','ColourFlow_FlipLHE_NonAllHad.py'])
get_FlipLHE_class.wait()

try:
    fIn = open( './UnFlippedLHEFile.events', 'r' )
    fOut = open( './PowhegOTF._1.events', 'w' )

    # Import the code - This runs a non-all-had filter
    import ColourFlow_FlipLHE_NonAllHad as FlipLHE

    flipper = FlipLHE.ColourStringFlipper(fIn=fIn, fOut=fOut)
    flipper.perform_colour_flip = True
    flipper.remove_allhadronic = True
    flipper.remove_dilepton = False
    flipper.remove_wboson = True
    flipper.remove_top = True

    flipper()

finally:
    fIn.close()
    fOut.close()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
# Required as LHE file is manipulated and top/W particles are removed
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 6' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter - Run inside colour flipping code
#--------------------------------------------------------------
#include('MC15JobOptions/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
