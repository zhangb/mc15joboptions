# JO for Pythia 8 jet jet JZ6W slice

evgenConfig.description = "Dijet truth jet slice JZ6W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 600.",
                            "TimeShower:weightGluonToQuark=1"]

include("MC15JobOptions/JetFilter_JZ6W.py")
evgenConfig.minevents = 200
