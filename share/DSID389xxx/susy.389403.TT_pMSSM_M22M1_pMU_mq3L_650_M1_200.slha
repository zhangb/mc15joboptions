#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13972955E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04001990E+01   # W+
        25     1.24271872E+02   # h
        35     3.00016923E+03   # H
        36     2.99999968E+03   # A
        37     3.00093494E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03138234E+03   # ~d_L
   2000001     3.02622727E+03   # ~d_R
   1000002     3.03046628E+03   # ~u_L
   2000002     3.02779562E+03   # ~u_R
   1000003     3.03138234E+03   # ~s_L
   2000003     3.02622727E+03   # ~s_R
   1000004     3.03046628E+03   # ~c_L
   2000004     3.02779562E+03   # ~c_R
   1000005     7.37614152E+02   # ~b_1
   2000005     3.02553965E+03   # ~b_2
   1000006     7.35609407E+02   # ~t_1
   2000006     3.01230510E+03   # ~t_2
   1000011     3.00648494E+03   # ~e_L
   2000011     3.00157199E+03   # ~e_R
   1000012     3.00509042E+03   # ~nu_eL
   1000013     3.00648494E+03   # ~mu_L
   2000013     3.00157199E+03   # ~mu_R
   1000014     3.00509042E+03   # ~nu_muL
   1000015     2.98577275E+03   # ~tau_1
   2000015     3.02207220E+03   # ~tau_2
   1000016     3.00504618E+03   # ~nu_tauL
   1000021     2.33864142E+03   # ~g
   1000022     2.01284502E+02   # ~chi_10
   1000023     4.24309591E+02   # ~chi_20
   1000025    -2.99931947E+03   # ~chi_30
   1000035     2.99996478E+03   # ~chi_40
   1000024     4.24472134E+02   # ~chi_1+
   1000037     3.00058142E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887582E-01   # N_11
  1  2    -9.61617153E-04   # N_12
  1  3     1.48632449E-02   # N_13
  1  4    -1.72725755E-03   # N_14
  2  1     1.36481371E-03   # N_21
  2  2     9.99634102E-01   # N_22
  2  3    -2.65785171E-02   # N_23
  2  4     4.83539746E-03   # N_24
  3  1    -9.27095960E-03   # N_31
  3  2     1.53860900E-02   # N_32
  3  3     7.06851997E-01   # N_33
  3  4     7.07133348E-01   # N_34
  4  1    -1.17051598E-02   # N_41
  4  2     2.22262207E-02   # N_42
  4  3     7.06705682E-01   # N_43
  4  4    -7.07061570E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99292714E-01   # U_11
  1  2    -3.76041484E-02   # U_12
  2  1     3.76041484E-02   # U_21
  2  2     9.99292714E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976590E-01   # V_11
  1  2    -6.84252104E-03   # V_12
  2  1     6.84252104E-03   # V_21
  2  2     9.99976590E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98856757E-01   # cos(theta_t)
  1  2    -4.78035458E-02   # sin(theta_t)
  2  1     4.78035458E-02   # -sin(theta_t)
  2  2     9.98856757E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99916948E-01   # cos(theta_b)
  1  2     1.28878665E-02   # sin(theta_b)
  2  1    -1.28878665E-02   # -sin(theta_b)
  2  2     9.99916948E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06930709E-01   # cos(theta_tau)
  1  2     7.07282810E-01   # sin(theta_tau)
  2  1    -7.07282810E-01   # -sin(theta_tau)
  2  2     7.06930709E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01869479E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39729546E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44406617E+02   # higgs               
         4     7.16958876E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39729546E+03  # The gauge couplings
     1     3.61936936E-01   # gprime(Q) DRbar
     2     6.38201175E-01   # g(Q) DRbar
     3     1.02794835E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39729546E+03  # The trilinear couplings
  1  1     1.83485081E-06   # A_u(Q) DRbar
  2  2     1.83487842E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39729546E+03  # The trilinear couplings
  1  1     4.73708440E-07   # A_d(Q) DRbar
  2  2     4.73798133E-07   # A_s(Q) DRbar
  3  3     1.06808151E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39729546E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.01251432E-07   # A_mu(Q) DRbar
  3  3     1.02460615E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39729546E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53503734E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39729546E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13472886E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39729546E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06103070E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39729546E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.54457322E+04   # M^2_Hd              
        22    -9.08308709E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41381015E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.64370626E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48243520E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48243520E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51756480E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51756480E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.89249737E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.09342754E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.71082763E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.07982961E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03346605E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.32973131E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.19901610E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.42560022E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.58608477E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.26655185E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.59739777E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50824608E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.98494795E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.62581569E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.65794682E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.84087279E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.89333252E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.23302000E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.90383739E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.07933520E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.01476123E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.82526287E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -2.20760319E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28289505E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.93355480E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.23615805E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.09549342E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96229803E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.88634427E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61632662E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.52145474E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.14781689E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.23310409E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.07257952E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.09170531E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18532273E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58131801E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.86152799E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.62594797E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.57700390E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41867898E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96742826E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.00483095E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61400912E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.54113828E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.96675768E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.22738439E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.26292670E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.09855517E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67624182E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.49830516E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.13980666E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.43486459E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.18173893E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55016863E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96229803E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.88634427E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61632662E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.52145474E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.14781689E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.23310409E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.07257952E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.09170531E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18532273E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58131801E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.86152799E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.62594797E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.57700390E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41867898E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96742826E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.00483095E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61400912E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.54113828E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.96675768E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.22738439E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.26292670E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.09855517E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67624182E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.49830516E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.13980666E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.43486459E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.18173893E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55016863E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89453826E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.93333580E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00675171E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.27261315E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.42664016E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99991455E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.36154100E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55011170E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998194E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.80561598E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.95269718E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.58504485E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89453826E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.93333580E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00675171E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.27261315E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.42664016E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99991455E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.36154100E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55011170E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998194E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.80561598E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.95269718E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.58504485E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.75051193E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51934310E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16357649E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31708042E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69207943E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60284853E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13580611E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.04432830E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.18297675E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26100408E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.18548863E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89480642E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.99553425E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99574399E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.96143141E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.02561598E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00470253E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.63210241E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89480642E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.99553425E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99574399E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.96143141E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.02561598E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00470253E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.63210241E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89508040E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.99468138E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99548558E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.93139506E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.97349083E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00504087E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.35657975E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.21367117E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.62648959E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.71888767E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.22575393E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.55640158E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.92052631E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.14968817E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.75104354E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.20225948E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.23390708E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.76716588E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.52328341E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.64961386E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.36227946E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.65755897E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.72185280E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.72185280E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.80446764E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.26922222E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.64295307E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.64295307E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26381484E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26381484E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.79826315E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.79826315E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.56501446E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.63130554E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.25681865E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.79096217E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.79096217E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42217975E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.95342494E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.61712828E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.61712828E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28991093E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28991093E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.71110539E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.71110539E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.76910161E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85077832E-01    2           5        -5   # BR(h -> b       bb     )
     5.44263857E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92673756E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.09381480E-04    2           3        -3   # BR(h -> s       sb     )
     1.76680032E-02    2           4        -4   # BR(h -> c       cb     )
     5.69989433E-02    2          21        21   # BR(h -> g       g      )
     1.92405866E-03    2          22        22   # BR(h -> gam     gam    )
     1.22687494E-03    2          22        23   # BR(h -> Z       gam    )
     1.62053751E-01    2          24       -24   # BR(h -> W+      W-     )
     2.00220960E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39567085E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43956545E-01    2           5        -5   # BR(H -> b       bb     )
     1.78179105E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29998738E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.73637308E-04    2           3        -3   # BR(H -> s       sb     )
     2.18347839E-07    2           4        -4   # BR(H -> c       cb     )
     2.17816091E-02    2           6        -6   # BR(H -> t       tb     )
     3.09359589E-05    2          21        21   # BR(H -> g       g      )
     3.29646755E-08    2          22        22   # BR(H -> gam     gam    )
     8.36989927E-09    2          23        22   # BR(H -> Z       gam    )
     3.30249782E-05    2          24       -24   # BR(H -> W+      W-     )
     1.64921290E-05    2          23        23   # BR(H -> Z       Z      )
     8.55397114E-05    2          25        25   # BR(H -> h       h      )
    -5.40237312E-24    2          36        36   # BR(H -> A       A      )
     1.10335383E-17    2          23        36   # BR(H -> Z       A      )
     2.22866908E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11061445E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.11107306E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.07647263E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.03483094E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.09198944E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32602825E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83094794E-01    2           5        -5   # BR(A -> b       bb     )
     1.87530978E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63063796E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.14380039E-04    2           3        -3   # BR(A -> s       sb     )
     2.29806503E-07    2           4        -4   # BR(A -> c       cb     )
     2.29120566E-02    2           6        -6   # BR(A -> t       tb     )
     6.74738212E-05    2          21        21   # BR(A -> g       g      )
     3.32880252E-08    2          22        22   # BR(A -> gam     gam    )
     6.64573183E-08    2          23        22   # BR(A -> Z       gam    )
     3.46276000E-05    2          23        25   # BR(A -> Z       h      )
     2.64444902E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21839022E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31829087E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.97718239E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00593007E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09542921E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.47282497E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.74330596E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.01073593E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.13825198E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05710366E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13301921E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.57177692E-05    2          24        25   # BR(H+ -> W+      h      )
     9.36023633E-14    2          24        36   # BR(H+ -> W+      A      )
     2.02857457E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.74438722E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.42560289E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
