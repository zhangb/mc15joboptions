## Pythia8 photon-induced 750 GeV Higgs to gamma-gamma, gamma-gamma -> H -> gamma-gamma (large width)
 
evgenConfig.description = "gamma-gamma -> H(750) -> gamma-gamma production with CT14QED"
evgenConfig.keywords = ["QCD", "exclusive", "dissociation", "diphoton"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]
 
## Config for Py8 with CT14 QED pdf
 
include("MC15JobOptions/Pythia8_Base_Fragment.py")
 
genSeq.Pythia8.Commands += [
   "PDF:pSet= LHAPDF6:CT14qed_proton"
]
 
evgenConfig.tune = "CT14_QED"
 
#EvtGen for b fragmentation as default.  No EvtGen is available in "nonStandard"
include("MC15JobOptions/Pythia8_EvtGen.py")

 
genSeq.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "25:onMode = off",    
    "25:oneChannel = 1   1.0   100 22 22",
    "25:m0 = 750.",
    "25:mWidth = 45.",
    "HiggsSM:gmgm2H= on"
]
