include("MC15JobOptions/MadGraphControl_HVT.py")
evgenConfig.description = "heavyVectorTriplet Spin-1 Wgamma 6 TeV resonance in fully hadronic decay model"
evgenConfig.keywords = ["exotic", "Wgamma", "heavyVectorTriplet", "allHadronic", "spin1"]
