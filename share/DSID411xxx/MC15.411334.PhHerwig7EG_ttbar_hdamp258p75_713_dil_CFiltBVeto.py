# based on the JobOptions 411087 and 411234

# Provide config information

evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7.1-Default"
evgenConfig.description    = "PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal 1.5*top mass, H7.1-Default tune, exactly two leptons filter, C filter and B veto, ME NNPDF30 NLO, from DSID 410450 LHE files"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'nedaa.asbah@cern.ch', 'aknue@cern.ch', 'tpelzer@cern.ch']
evgenConfig.minevents   = 1000
evgenConfig.inputFilesPerJob = 25

#--------------------------------------------------------------
# dilepton filter
# This is a filter on the input LHE files, applied before any other algorithm is processed
#--------------------------------------------------------------
include('MC15JobOptions/LHEFilter.py')
include('MC15JobOptions/LHEFilter_NLeptons.py')

nleptonFilter = LHEFilter_NLeptons()
nleptonFilter.NumLeptons = 2
nleptonFilter.Ptcut = 0.
lheFilters.addFilter(nleptonFilter)

lheFilters.run_filters()

#--------------------------------------------------------------
# Herwig7.1.3 H7.1-Default tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# B filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBFilter.py')

#--------------------------------------------------------------
# C filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusCFilter.py')

# Combine the filters
filtSeq.Expression = "(TTbarPlusCFilter) and (not TTbarPlusBFilter)"
