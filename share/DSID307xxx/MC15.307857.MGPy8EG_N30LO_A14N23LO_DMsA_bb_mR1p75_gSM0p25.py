
MXd =   10000. #DM mass
MY1 =   1750. #mediator mass
gVXd = 0. #vector coupling to DM
gAXd = 1. #axial coupling to DM
gAd11 = 0.25 #axial couplings to quarks
gAu11 = 0.25
gAd22 = 0.25
gAu22 = 0.25
gAd33 = 0.25
gAu33 = 0.25
gVd11 = 0. #vector couplings to quarks
gVu11 = 0.
gVd22 = 0.
gVu22 = 0.
gVd33 = 0.
gVu33 = 0.
include("MC15JobOptions/MadGraphControl_MGPy8EG_N30LO_A14N23LO_dmA_bb.py")
