from MadGraphControl.MadGraphUtils import *

# Generate A->ZH, H->hxx, h->bb, Z->ll

# Get run info and determine number of events
efficiency = 1.0
safety_factor = 1.1
if runArgs.maxEvents > 0:
    nevents = int(runArgs.maxEvents*safety_factor/efficiency)
else:
    nevents = int(5000*safety_factor/efficiency)
dsid = runArgs.runNumber
mode = 0
dsid_list = [344082, 344083]

if int(dsid) not in dsid_list:
    raise RuntimeError('Given run number not used for this request. Please use one in: ' + str(dsid_list))

print "Starting MadGraph generation for DSID " + str(dsid) + " with " + str(nevents) + " events"

# Set up parameters according to DSID
mA = 750.0
mh = 125.0
mx = 60.0
mH = 275.0

if int(dsid) == 344082:
    mH = 275.0
elif int(dsid) == 344083:
    mH = 300.0

print "mh = " + str(mh) + " GeV"
print "mA = " + str(mA) + " GeV"
print "mH = " + str(mH) + " GeV"
print "mX = " + str(mx) + " GeV"

# Set up proc card
fcard = open('proc_card_mg5.dat','w')

fcard.write("""
import model 2HDM_GF
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~ a
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
# Define multiparticle labels
# Specify process(es) to run
generate g g > h3, h3 > z h2 
    output -f""")
fcard.close()

process_dir = new_process(card_loc='proc_card_mg5.dat')

# Set up param card
masses ={'25':mh,  
         '35':mH,
         '36':mA, 
         '37':mA} 

print masses

decays ={'25':'DECAY 25 4.070000e-03 # Wh1',  
         '35':'DECAY 35 1.000000e+00 # Wh2',  
         '36':'DECAY 36 1.000000e+00 # Wh3',  
         '37':'DECAY 37 1.000000e+00 # whc'}  

print decays

# Set up run card
beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'parton_shower':'PYTHIA8'}

# Build all cards
build_run_card(run_card_old=get_default_runcard('PROC_2HDM_GF_0'),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
build_param_card(param_card_old='MadGraph_param_card_2HDM_A_ZH2.dat',param_card_new='param_card_new.dat',masses=masses,decays=decays)
print_cards()

runName = 'run_01'

# Generate events and rename output
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')  

# Pythia8 Showering with A14_NNPDF23LO
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

# Evgen description set up
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.ch>', 'Chad Pelwan <chad.dean.pelwan@cern.ch>']
evgenConfig.generators += ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.description = '2HDM A production, decaying to Z->ll and H->hxx where h->bb'
evgenConfig.keywords += ['ZHiggs','BSMHiggs','bottom']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

genSeq.Pythia8.Commands += ['25:onMode = off',
                            '25:onIfAny = -5 5', # h->bb
                            '35:onMode = off',
                            '35:oneChannel = 1 1 103 25 1000022 1000022', # H->hxx where x is a neutralino
                            '1000022:m0 = ' + str(mx), # set x mass
                            '23:onMode = off',
                            '23:onIfAny = 11 -11 13 -13 15 -15' # Z->ll (includes taus)
                            ]
