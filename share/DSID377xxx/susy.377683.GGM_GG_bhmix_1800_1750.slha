#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.74000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.75000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05418344E+01   # W+
        25     1.25000000E+02   # h
        35     2.00400123E+03   # H
        36     2.00000000E+03   # A
        37     2.00151556E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013291E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994924E+03   # ~u_R
   1000003     5.00013291E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994924E+03   # ~c_R
   1000005     4.99937251E+03   # ~b_1
   2000005     5.00078713E+03   # ~b_2
   1000006     4.98446185E+03   # ~t_1
   2000006     5.01994513E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007614E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007614E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99961254E+03   # ~tau_1
   2000015     5.00054635E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.69899165E+03   # ~chi_10
   1000023    -1.75007247E+03   # ~chi_20
   1000025     1.78623390E+03   # ~chi_30
   1000035     3.00484693E+03   # ~chi_40
   1000024     1.74525993E+03   # ~chi_1+
   1000037     3.00484132E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.27845840E-01   # N_11
  1  2    -4.07696533E-02   # N_12
  1  3     4.85463815E-01   # N_13
  1  4    -4.82600408E-01   # N_14
  2  1     2.50147426E-03   # N_21
  2  2    -3.26614623E-03   # N_22
  2  3    -7.07027585E-01   # N_23
  2  4    -7.07174002E-01   # N_24
  3  1     6.85732852E-01   # N_31
  3  2     4.64179420E-02   # N_32
  3  3    -5.12568708E-01   # N_33
  3  4     5.14673829E-01   # N_34
  4  1     2.15223774E-03   # N_41
  4  2    -9.98084436E-01   # N_42
  4  3    -4.13545377E-02   # N_43
  4  4     4.59633306E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.84131381E-02   # U_11
  1  2     9.98292495E-01   # U_12
  2  1     9.98292495E-01   # U_21
  2  2     5.84131381E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.49296733E-02   # V_11
  1  2     9.97889842E-01   # V_12
  2  1     9.97889842E-01   # V_21
  2  2     6.49296733E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07672014E-01   # cos(theta_t)
  1  2     7.06541096E-01   # sin(theta_t)
  2  1    -7.06541096E-01   # -sin(theta_t)
  2  2     7.07672014E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.79699757E-01   # cos(theta_b)
  1  2     7.33490450E-01   # sin(theta_b)
  2  1    -7.33490450E-01   # -sin(theta_b)
  2  2     6.79699757E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04825864E-01   # cos(theta_tau)
  1  2     7.09380364E-01   # sin(theta_tau)
  2  1    -7.09380364E-01   # -sin(theta_tau)
  2  2     7.04825864E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201227E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.75000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52282649E+02   # vev(Q)              
         4     3.17679112E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52727097E-01   # gprime(Q) DRbar
     2     6.26821957E-01   # g(Q) DRbar
     3     1.08213409E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02498097E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71517884E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79748826E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.74000000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.84258812E+05   # M^2_Hd              
        22    -8.27001519E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36890989E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.17750491E-05   # gluino decays
#          BR         NDA      ID1       ID2
     7.10390814E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.91820499E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.21494103E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.52451746E-06    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.56241237E-12    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.59947633E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.31234482E-05    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.02024067E-12    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     6.47756944E-10    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.52451746E-06    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.56241237E-12    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.59947633E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.31234482E-05    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.02024067E-12    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     6.47756944E-10    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.30977106E-06    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     4.45305601E-09    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.59462614E-11    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.59286552E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.59286552E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.59286552E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.59286552E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.02583759E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.03982849E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.39496541E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.79272047E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.05918836E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.00207341E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.08590396E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.66066592E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.93231363E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.14640914E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.28282108E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.55789072E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.59711096E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.65860190E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.15629335E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.66008729E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.34691959E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.24066928E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.30254768E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.91841642E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.45337082E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.03731063E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.94583780E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.46044740E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.43850174E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.41386924E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.08066880E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.53463389E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.76134281E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.27669926E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.57083275E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.13969008E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22293167E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.09159661E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.11490116E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98016430E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.46033004E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.52145286E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.92359048E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.93018814E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.06722936E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.13056064E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.47671929E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83962633E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.74843740E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60297785E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22299336E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.08840823E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.12342693E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.36773447E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.46637765E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.46898100E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.93006776E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.93062554E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00572289E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.48986586E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.38182452E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.74021308E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.51196858E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89769832E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22293167E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.09159661E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.11490116E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98016430E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.46033004E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.52145286E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.92359048E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.93018814E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.06722936E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.13056064E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.47671929E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83962633E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.74843740E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60297785E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22299336E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.08840823E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.12342693E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.36773447E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.46637765E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.46898100E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.93006776E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.93062554E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00572289E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.48986586E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.38182452E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.74021308E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.51196858E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89769832E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.87843678E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.22504482E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.80562123E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.65563559E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.75349042E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.57284366E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.52269505E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.91176998E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.36652468E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.23819212E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.63338846E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.44766905E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.87843678E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.22504482E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.80562123E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.65563559E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.75349042E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.57284366E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.52269505E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.91176998E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.36652468E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.23819212E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.63338846E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.44766905E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.39969754E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.64107255E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35125052E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.39210596E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.63471461E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.74656096E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.27879133E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.49868943E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.39829267E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.50728632E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.03375382E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.46421041E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.66912146E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.68110465E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.34766695E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.87862120E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07715028E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.14175767E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.95298467E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.76627648E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.41386525E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.51702194E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.87862120E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07715028E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.14175767E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.95298467E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.76627648E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.41386525E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.51702194E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.88109497E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.07622541E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.14077734E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.94787331E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.76390130E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.26624746E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.51230940E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.42078379E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.53049568E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.12296796E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.10859771E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.04099050E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.04096358E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03343068E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.63262315E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53629035E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08459223E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46178048E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.38843983E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52881982E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.72808360E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.36061099E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.96267332E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92727954E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10047137E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.87090882E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.95872327E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.37811131E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.92532454E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.76272707E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.12677721E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.45907868E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.11988163E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.45889516E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34222837E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.33263124E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.33252208E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.30183597E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.65567148E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.65567148E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.65567148E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.07173941E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.07173941E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.35014432E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.35014432E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.69057992E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.69057992E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.68651825E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.68651825E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.79924737E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.79924737E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.02800398E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.07491233E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.69375686E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.97299070E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.48757763E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.02552629E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.33150192E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.58605946E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.15387701E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.60144403E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.52612185E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.76515634E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.76512484E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.24101231E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.74749599E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.74749599E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.74749599E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.89022998E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.44768642E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.86983065E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.44710042E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.08267370E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.59068255E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.59037729E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.50409029E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.11652212E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.11652212E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.11652212E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.28092682E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.28092682E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.27338169E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.27338169E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.26974999E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.26974999E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.26960850E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.26960850E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.23008522E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.23008522E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.63258370E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.24833324E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52330299E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.80806408E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46759767E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46759767E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10820314E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.87767420E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41928717E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.84584599E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.88117798E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.87493931E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.32525982E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.96823874E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08693201E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17902055E-01    2           5        -5   # BR(h -> b       bb     )
     6.37290164E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25577247E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78018565E-04    2           3        -3   # BR(h -> s       sb     )
     2.06228221E-02    2           4        -4   # BR(h -> c       cb     )
     6.69617519E-02    2          21        21   # BR(h -> g       g      )
     2.30246871E-03    2          22        22   # BR(h -> gam     gam    )
     1.53707475E-03    2          22        23   # BR(h -> Z       gam    )
     2.00631870E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56093449E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78097385E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46806296E-03    2           5        -5   # BR(H -> b       bb     )
     2.46429280E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71219250E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547966E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668557E-05    2           4        -4   # BR(H -> c       cb     )
     9.96068565E-01    2           6        -6   # BR(H -> t       tb     )
     7.97656251E-04    2          21        21   # BR(H -> g       g      )
     2.71526215E-06    2          22        22   # BR(H -> gam     gam    )
     1.16025800E-06    2          23        22   # BR(H -> Z       gam    )
     3.34384152E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66736079E-04    2          23        23   # BR(H -> Z       Z      )
     9.02237576E-04    2          25        25   # BR(H -> h       h      )
     7.18270925E-24    2          36        36   # BR(H -> A       A      )
     2.92692009E-11    2          23        36   # BR(H -> Z       A      )
     6.59825152E-12    2          24       -37   # BR(H -> W+      H-     )
     6.59825152E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381092E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47098019E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898471E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269203E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677746E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179550E-06    2           4        -4   # BR(A -> c       cb     )
     9.96999062E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678553E-04    2          21        21   # BR(A -> g       g      )
     3.14418264E-06    2          22        22   # BR(A -> gam     gam    )
     1.35285379E-06    2          23        22   # BR(A -> Z       gam    )
     3.25923270E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472149E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35744673E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238392E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147750E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49877726E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695833E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731964E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402544E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34036413E-04    2          24        25   # BR(H+ -> W+      h      )
     2.80802190E-13    2          24        36   # BR(H+ -> W+      A      )
