# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
#include("MC15JobOptions/Herwig71_EvtGen.py")
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->bb and W->lv decays
Herwig7Config.add_commands("""
# force H->cc decays
do /Herwig/Particles/h0:SelectDecayModes h0->c,cbar;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
do /Herwig/Particles/W-:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Herwig7 H+W-jet->l-vccbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.process     = "qq->W+H, H->cc, W+->l+vbar"
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'spyridon.argyropoulos@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7-MMHT2014LO"
evgenConfig.minevents   = 5000
evgenConfig.inputFilesPerJob = 100
#evgenConfig.inputfilecheck = "TXT"
