from MadGraphControl.MadGraphUtils import *

mode=0


cmdspsh = """set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-; h0->gamma,gamma;
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
"""

safefactor=2.2
evgenConfig.description = "X->hh diHiggs production, decay to bbbb, with MG5_aMC@NLO"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "bbbar", "bottom"]
run_number_min=343415
run_number_max=343418

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CTEQ6L1_CT10ME_Xhh.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
filtSeq.Expression = "hyyFilter and hWWFilter"
