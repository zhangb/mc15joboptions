#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12071793E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.79990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04181651E+01   # W+
        25     1.25853972E+02   # h
        35     4.00001265E+03   # H
        36     3.99999744E+03   # A
        37     4.00099201E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02318742E+03   # ~d_L
   2000001     4.02032907E+03   # ~d_R
   1000002     4.02253388E+03   # ~u_L
   2000002     4.02151258E+03   # ~u_R
   1000003     4.02318742E+03   # ~s_L
   2000003     4.02032907E+03   # ~s_R
   1000004     4.02253388E+03   # ~c_L
   2000004     4.02151258E+03   # ~c_R
   1000005     7.35355893E+02   # ~b_1
   2000005     4.02394247E+03   # ~b_2
   1000006     7.22234734E+02   # ~t_1
   2000006     2.25010150E+03   # ~t_2
   1000011     4.00418247E+03   # ~e_L
   2000011     4.00291428E+03   # ~e_R
   1000012     4.00306872E+03   # ~nu_eL
   1000013     4.00418247E+03   # ~mu_L
   2000013     4.00291428E+03   # ~mu_R
   1000014     4.00306872E+03   # ~nu_muL
   1000015     4.00391884E+03   # ~tau_1
   2000015     4.00816382E+03   # ~tau_2
   1000016     4.00473246E+03   # ~nu_tauL
   1000021     1.97672242E+03   # ~g
   1000022     4.46783205E+02   # ~chi_10
   1000023    -4.91261675E+02   # ~chi_20
   1000025     5.08288961E+02   # ~chi_30
   1000035     2.05353626E+03   # ~chi_40
   1000024     4.89155199E+02   # ~chi_1+
   1000037     2.05370053E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.98999298E-01   # N_11
  1  2    -1.89128230E-02   # N_12
  1  3    -4.45419167E-01   # N_13
  1  4    -4.03539580E-01   # N_14
  2  1    -3.47379040E-02   # N_21
  2  2     2.31877549E-02   # N_22
  2  3    -7.04846667E-01   # N_23
  2  4     7.08129072E-01   # N_24
  3  1     6.00326651E-01   # N_31
  3  2     2.84195160E-02   # N_32
  3  3     5.52025803E-01   # N_33
  3  4     5.77985947E-01   # N_34
  4  1    -1.14514205E-03   # N_41
  4  2     9.99148119E-01   # N_42
  4  3    -7.77525185E-03   # N_43
  4  4    -4.05125888E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.10019902E-02   # U_11
  1  2     9.99939476E-01   # U_12
  2  1    -9.99939476E-01   # U_21
  2  2     1.10019902E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.73025629E-02   # V_11
  1  2    -9.98356858E-01   # V_12
  2  1    -9.98356858E-01   # V_21
  2  2    -5.73025629E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96234257E-01   # cos(theta_t)
  1  2    -8.67023943E-02   # sin(theta_t)
  2  1     8.67023943E-02   # -sin(theta_t)
  2  2     9.96234257E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998835E-01   # cos(theta_b)
  1  2    -1.52643331E-03   # sin(theta_b)
  2  1     1.52643331E-03   # -sin(theta_b)
  2  2     9.99998835E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06174051E-01   # cos(theta_tau)
  1  2     7.08038283E-01   # sin(theta_tau)
  2  1    -7.08038283E-01   # -sin(theta_tau)
  2  2    -7.06174051E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00245599E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20717934E+03  # DRbar Higgs Parameters
         1    -4.79990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43891439E+02   # higgs               
         4     1.62784860E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20717934E+03  # The gauge couplings
     1     3.61603184E-01   # gprime(Q) DRbar
     2     6.35785976E-01   # g(Q) DRbar
     3     1.03193165E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20717934E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16180196E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20717934E+03  # The trilinear couplings
  1  1     4.28473696E-07   # A_d(Q) DRbar
  2  2     4.28513505E-07   # A_s(Q) DRbar
  3  3     7.66761402E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20717934E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.49433389E-08   # A_mu(Q) DRbar
  3  3     9.59002729E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20717934E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68438581E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20717934E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87273804E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20717934E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02861133E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20717934E+03  # The soft SUSY breaking masses at the scale Q
         1     4.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56574406E+07   # M^2_Hd              
        22    -2.51062129E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40234461E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.25284989E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44988548E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44988548E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55011452E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55011452E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.25493585E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.50366503E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.79333350E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.18504695E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.51795452E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.32610130E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.99502230E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12362308E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.38242436E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.14654811E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.23110284E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -9.06478972E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.24012673E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.49142480E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.81295668E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.11079687E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.62262776E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.96571549E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.90715435E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.94655529E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.41805749E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.67103870E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.55734052E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80681586E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.57563075E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.03857376E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62277827E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.04883040E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13169072E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.56406769E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.51219783E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.90382941E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.17744736E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78790764E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.49542236E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.33359114E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.70918215E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78132042E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.56147792E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55058179E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52896890E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61126476E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.46565655E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.51762200E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.94245775E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.00708212E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45853641E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78811239E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47072521E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.50564278E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.16115428E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78680169E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.05026891E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58353627E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53114221E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54480470E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.03405084E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.69896670E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.06345249E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.04412766E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85885497E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78790764E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.49542236E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.33359114E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.70918215E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78132042E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.56147792E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55058179E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52896890E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61126476E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.46565655E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.51762200E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.94245775E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.00708212E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45853641E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78811239E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47072521E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.50564278E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.16115428E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78680169E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.05026891E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58353627E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53114221E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54480470E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.03405084E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.69896670E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.06345249E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.04412766E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85885497E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13186268E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.50962752E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.86940387E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.80748287E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78385328E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.20766300E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58316932E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02557238E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.40113938E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20358967E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.58681740E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.31973233E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13186268E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.50962752E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.86940387E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.80748287E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78385328E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.20766300E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58316932E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02557238E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.40113938E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20358967E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.58681740E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.31973233E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06306630E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.30207618E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.50114950E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.45531395E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40923011E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56963505E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82630130E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05411636E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.32791097E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.91888680E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.25364437E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44467317E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.84576649E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89730616E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13292009E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12261513E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.19197264E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.87283054E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78829540E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.27398437E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55987460E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13292009E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12261513E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.19197264E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.87283054E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78829540E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.27398437E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55987460E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45393331E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01872233E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.34133548E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.42190049E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53170019E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.50786109E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04825999E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.15178762E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33651067E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33651067E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11218504E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11218504E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10260858E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.92480227E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.75466043E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.62760813E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.65474526E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.92413960E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.48879300E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.55570261E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.55393388E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.26086806E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.25254349E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18394609E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53571332E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18394609E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53571332E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38708465E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52126253E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52126253E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48234597E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03947686E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03947686E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03947671E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.58937190E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.58937190E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.58937190E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.58937190E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.63124611E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.63124611E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.63124611E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.63124611E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.06142691E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.06142691E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.10306006E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.18083164E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.98976782E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18262532E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.51870026E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.18262532E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.51870026E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45913344E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.36301613E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.36301613E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.37686508E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.02022467E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.02022467E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.02018368E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.69593521E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.19987408E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.69593521E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.19987408E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.01714085E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.04440154E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.04440154E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.74154763E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.00833854E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.00833854E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.00833856E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.03657296E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.03657296E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.03657296E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.03657296E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.45520282E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.45520282E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.45520282E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.45520282E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.31161908E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.31161908E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.92336726E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.73446332E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.56207254E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.41996532E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.48792339E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.48792339E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.27909206E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.06327632E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.19491663E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.84012407E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.84012407E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.85144514E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.85144514E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04526683E-03   # h decays
#          BR         NDA      ID1       ID2
     5.85356596E-01    2           5        -5   # BR(h -> b       bb     )
     6.45644780E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28556428E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84468722E-04    2           3        -3   # BR(h -> s       sb     )
     2.10429010E-02    2           4        -4   # BR(h -> c       cb     )
     6.91036935E-02    2          21        21   # BR(h -> g       g      )
     2.39715982E-03    2          22        22   # BR(h -> gam     gam    )
     1.66710133E-03    2          22        23   # BR(h -> Z       gam    )
     2.26392576E-01    2          24       -24   # BR(h -> W+      W-     )
     2.87624695E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54687044E+01   # H decays
#          BR         NDA      ID1       ID2
     3.67064473E-01    2           5        -5   # BR(H -> b       bb     )
     5.97743476E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11347615E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49987744E-04    2           3        -3   # BR(H -> s       sb     )
     7.01105611E-08    2           4        -4   # BR(H -> c       cb     )
     7.02653805E-03    2           6        -6   # BR(H -> t       tb     )
     7.50588258E-07    2          21        21   # BR(H -> g       g      )
     2.70142312E-09    2          22        22   # BR(H -> gam     gam    )
     1.80872561E-09    2          23        22   # BR(H -> Z       gam    )
     1.65355110E-06    2          24       -24   # BR(H -> W+      W-     )
     8.26202801E-07    2          23        23   # BR(H -> Z       Z      )
     6.70669663E-06    2          25        25   # BR(H -> h       h      )
     1.58463474E-24    2          36        36   # BR(H -> A       A      )
     8.86471091E-20    2          23        36   # BR(H -> Z       A      )
     1.86291630E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.53640911E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.53640911E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.43905536E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.34579215E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.59517219E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.31196211E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.22480117E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.82767164E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.43147820E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.24073926E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.34612482E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.76123715E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.63043614E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.63043614E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43789205E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54630964E+01   # A decays
#          BR         NDA      ID1       ID2
     3.67127010E-01    2           5        -5   # BR(A -> b       bb     )
     5.97806072E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11369581E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50054547E-04    2           3        -3   # BR(A -> s       sb     )
     7.05768970E-08    2           4        -4   # BR(A -> c       cb     )
     7.04131199E-03    2           6        -6   # BR(A -> t       tb     )
     1.44681115E-05    2          21        21   # BR(A -> g       g      )
     6.72304192E-08    2          22        22   # BR(A -> gam     gam    )
     1.59070246E-08    2          23        22   # BR(A -> Z       gam    )
     1.65021328E-06    2          23        25   # BR(A -> Z       h      )
     1.90706085E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.53629140E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.53629140E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.14142558E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.65637730E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.38282100E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.73769022E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.51834134E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.26875583E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.57493045E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.07521719E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.06597429E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     9.92795451E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     9.92795451E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57367116E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.92211026E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95019313E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10384252E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.79014737E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19186748E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45205469E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.76757746E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.64391238E-06    2          24        25   # BR(H+ -> W+      h      )
     2.29763058E-14    2          24        36   # BR(H+ -> W+      A      )
     4.95102441E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.06831935E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.30771071E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.53099246E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.07981916E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.52405681E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.38900351E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.47574608E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.94764072E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
