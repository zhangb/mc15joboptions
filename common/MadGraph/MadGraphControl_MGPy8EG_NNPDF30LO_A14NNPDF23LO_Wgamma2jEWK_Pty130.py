from MadGraphControl.MadGraphUtils import *

nevents=10000
mode=0
gridpack_dir='madevent/'
ptgmin=130


#---------------------------------------------------------------------------
# Process type based on runNumber:
#---------------------------------------------------------------------------
if runArgs.runNumber==361273:
   mgproc="""generate p p > eall veall a j j QCD=0 QED=5"""
   name='enugamma2j'
   process='pp>enua'
   keyword=['SM','W','photon','electron','diboson','VBS']
   description = 'MadGraph W->evg plus two EWK jets'
elif runArgs.runNumber==361274:
   mgproc="""generate p p > muall vmall a j j QCD=0 QED=5"""
   name='munugamma2j'
   process='pp>munua'
   keyword=['SM','W','photon','muon','diboson','VBS']
   description = 'MadGraph W->muvg plus two EWK jets'
elif runArgs.runNumber==361275:
   mgproc="""generate p p > taall vtall a j j QCD=0 QED=5"""
   name='taunugamma2j'
   process='pp>taunua'
   keyword=['SM','W','photon','tau','diboson','VBS']
   description = 'MadGraph W->tauvg plus two EWK jets'

else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

print("Debug::I have choosen process:"+name)
#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define eall = e+ e-
define veall = ve ve~
define muall = mu+ mu-
define vmall = vm vm~
define taall = ta+ ta-
define vtall = vt vt~
"""+mgproc+"""
output -f
""")
fcard.close()


#----------------------------------------------------------------------------
# Run Number
#----------------------------------------------------------------------------
if not hasattr(runArgs,'runNumber'):
    raise RunTimeError("No run number found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(int(runArgs.maxEvents)*safefactor)
else: nevents = int(nevents*safefactor)

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = { 'lhe_version' : '3.0',
           'cut_decays'  : 'T', 
           'pdlabel'     :"'lhapdf'",
           'lhaid'       :"260000",
           'auto_ptj_mjj': 'F',
           'ptj':"15",
           'ptb':"15",
           'pta':"10",
           'ptl':"10",
           'etaj':"5.5",
           'etab':"5.5",
           'etal':"3.0",
           'etaa':"3.0",
           'drjj':"0.1",
           'drll':"0.1",
           'draa':"0",
           'draj':"0.1",
           'drjl':"0.1",
           'dral':"0.1",
           'mmjj':"0",
           'mmll':"40",
           'ptgmin'      : ptgmin,
           'epsgamma':'0.1',
           'R0gamma' :'0.1', 
           'xn'    :'2',
           'isoEM' :'True', 
           'bwcutoff'    :'15',
           'maxjetflavor': '5',
           'asrwgtflavor': '5', 
           'use_syst'     : 'True',
           'systematics_program':'systematics',
           'systematics_arguments':"['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1', '--pdf=errorset,13100@0,25200@0,265000@0,266000@0']" }
           

process_dir = new_process(grid_pack=gridpack_dir)
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', 
               nevts=nevents,
               rand_seed=randomSeed,
               beamEnergy=beamEnergy,
               extras=extras)

print_cards()


#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=name,grid_pack=True,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed)


#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


arrange_output(run_name=name,
               proc_dir=process_dir,
               outputDS=runArgs.outputTXTFile
               )


#### Shower
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

#evgenConfig.generators = ["MadGraph"]
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
#evgenConfig.inputfilecheck = name

evgenConfig.contact = ['Evgeny Soldatov <Evgeny.Soldatov@cern.ch>']
evgenConfig.keywords+=keyword
evgenConfig.description = description
#evgenConfig.minevents = 10000

runArgs.inputGeneratorFile=runArgs.outputTXTFile
#runArgs.inputGeneratorFile=name+'._00001.events.tar.gz'

