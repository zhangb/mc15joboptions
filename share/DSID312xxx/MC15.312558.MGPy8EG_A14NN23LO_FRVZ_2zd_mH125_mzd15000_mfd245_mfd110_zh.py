mH = 125
mfd2 = 45
mfd1 = 10
mZd = 15000
nGamma = 2
avgtau = 1000
decayMode = 'normal'
include("MC15JobOptions/MadGraphControl_A14N23LO_FRVZdisplaced_zh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
