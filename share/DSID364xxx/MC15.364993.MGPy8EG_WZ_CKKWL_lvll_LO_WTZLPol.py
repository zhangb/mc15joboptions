name='.WZ_CKKWL_lvll_LO_WTZLPol_13TeV'
stringy = str(runArgs.runNumber)+str(name)
keyword=['SM','WZ']

ktdurham = 25
nJetMax = 1
dparameter = 0.4
maxjetflavor = 4

#### Shower                                                                                                                                                                                                
evgenConfig.description = 'MGPy8EG_'+str(name)
evgenConfig.keywords+=keyword

evgenConfig.generators  = [ "MadGraph", "Pythia8", "EvtGen"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


PYTHIA8_TMS=ktdurham
PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process= "pp>LEPTONS,NEUTRINOS"
PYTHIA8_nQuarksMerge=maxjetflavor
include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")
