#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12071027E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.71990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04185693E+01   # W+
        25     1.25851884E+02   # h
        35     4.00001428E+03   # H
        36     3.99999775E+03   # A
        37     4.00097963E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02317971E+03   # ~d_L
   2000001     4.02031667E+03   # ~d_R
   1000002     4.02252640E+03   # ~u_L
   2000002     4.02149457E+03   # ~u_R
   1000003     4.02317971E+03   # ~s_L
   2000003     4.02031667E+03   # ~s_R
   1000004     4.02252640E+03   # ~c_L
   2000004     4.02149457E+03   # ~c_R
   1000005     7.38470739E+02   # ~b_1
   2000005     4.02396845E+03   # ~b_2
   1000006     7.25239302E+02   # ~t_1
   2000006     2.25029460E+03   # ~t_2
   1000011     4.00417272E+03   # ~e_L
   2000011     4.00284527E+03   # ~e_R
   1000012     4.00305948E+03   # ~nu_eL
   1000013     4.00417272E+03   # ~mu_L
   2000013     4.00284527E+03   # ~mu_R
   1000014     4.00305948E+03   # ~nu_muL
   1000015     4.00347149E+03   # ~tau_1
   2000015     4.00851115E+03   # ~tau_2
   1000016     4.00471613E+03   # ~nu_tauL
   1000021     1.97671751E+03   # ~g
   1000022     5.44705351E+02   # ~chi_10
   1000023    -5.83307546E+02   # ~chi_20
   1000025     6.03724666E+02   # ~chi_30
   1000035     2.05351865E+03   # ~chi_40
   1000024     5.81314167E+02   # ~chi_1+
   1000037     2.05368277E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.59151954E-01   # N_11
  1  2    -2.21646316E-02   # N_12
  1  3    -4.76357115E-01   # N_13
  1  4    -4.43036047E-01   # N_14
  2  1    -2.88570647E-02   # N_21
  2  2     2.23506022E-02   # N_22
  2  3    -7.05390714E-01   # N_23
  2  4     7.07878282E-01   # N_24
  3  1     6.50272264E-01   # N_31
  3  2     2.87889122E-02   # N_32
  3  3     5.24794854E-01   # N_33
  3  4     5.48550402E-01   # N_34
  4  1    -1.25047310E-03   # N_41
  4  2     9.99089775E-01   # N_42
  4  3    -9.90966573E-03   # N_43
  4  4    -4.14711541E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.40206178E-02   # U_11
  1  2     9.99901706E-01   # U_12
  2  1    -9.99901706E-01   # U_21
  2  2     1.40206178E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.86563349E-02   # V_11
  1  2    -9.98278235E-01   # V_12
  2  1    -9.98278235E-01   # V_21
  2  2    -5.86563349E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96222148E-01   # cos(theta_t)
  1  2    -8.68414177E-02   # sin(theta_t)
  2  1     8.68414177E-02   # -sin(theta_t)
  2  2     9.96222148E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998323E-01   # cos(theta_b)
  1  2    -1.83139214E-03   # sin(theta_b)
  2  1     1.83139214E-03   # -sin(theta_b)
  2  2     9.99998323E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06334014E-01   # cos(theta_tau)
  1  2     7.07878705E-01   # sin(theta_tau)
  2  1    -7.07878705E-01   # -sin(theta_tau)
  2  2    -7.06334014E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00227012E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20710267E+03  # DRbar Higgs Parameters
         1    -5.71990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43923454E+02   # higgs               
         4     1.63307579E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20710267E+03  # The gauge couplings
     1     3.61567174E-01   # gprime(Q) DRbar
     2     6.35591801E-01   # g(Q) DRbar
     3     1.03193052E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20710267E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16162861E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20710267E+03  # The trilinear couplings
  1  1     4.29761797E-07   # A_d(Q) DRbar
  2  2     4.29801340E-07   # A_s(Q) DRbar
  3  3     7.68028901E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20710267E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.59819756E-08   # A_mu(Q) DRbar
  3  3     9.69471305E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20710267E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68254882E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20710267E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.89228054E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20710267E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02573741E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20710267E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55621826E+07   # M^2_Hd              
        22    -3.47907320E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40147052E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.23631180E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44961351E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44961351E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55038649E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55038649E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.96427731E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.85054862E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.14945138E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.30145900E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.36172138E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.09949636E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.70420269E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.09738828E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.18444918E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.18571183E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.36846901E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.52256357E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.00011903E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.14993362E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.01082279E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.78312424E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.26805254E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.94882322E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.67170245E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57066692E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80979370E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54308011E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.25794872E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62379056E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.47683130E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12778110E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.68928198E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     5.10481801E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.57647453E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.78445520E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78762036E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.24669421E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38174405E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.92816823E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77845487E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.75445035E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54485304E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52992796E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61005103E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.09331984E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.44565099E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.25069368E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.78078884E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46515360E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78782644E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.31957400E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.47203071E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.44671487E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78431220E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28803077E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57856418E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53209390E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54444415E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.05922964E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.15825098E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.86385099E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24508925E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86065324E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78762036E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.24669421E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38174405E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.92816823E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77845487E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.75445035E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54485304E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52992796E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61005103E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.09331984E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.44565099E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.25069368E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.78078884E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46515360E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78782644E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.31957400E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.47203071E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.44671487E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78431220E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28803077E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57856418E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53209390E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54444415E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.05922964E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.15825098E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.86385099E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24508925E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86065324E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12363591E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.32912893E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.73814603E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.82067441E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78883668E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.94075637E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59406842E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.99842174E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.78418547E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.31152515E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.20749416E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.84476781E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12363591E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.32912893E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.73814603E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.82067441E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78883668E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.94075637E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59406842E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.99842174E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.78418547E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.31152515E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.20749416E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.84476781E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.03956352E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.06484107E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52831600E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66203113E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41646790E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.62551681E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84127662E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.02761328E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.07568884E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.81021558E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48700978E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45855155E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.72155241E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92557303E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12468434E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02226013E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.41161653E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.72132459E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79396527E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.39460822E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.57028444E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12468434E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02226013E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.41161653E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.72132459E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79396527E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.39460822E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.57028444E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44082510E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.28748715E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.73366917E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.19800338E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53980755E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.41265070E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06364465E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.41101570E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33760962E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33760962E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11255033E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11255033E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09968009E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.89495848E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.76403264E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.63378474E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.61844306E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.33152001E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.45048677E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.11495046E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.50642589E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.59013340E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.28835933E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18858238E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54178206E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18858238E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54178206E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35019639E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53549966E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53549966E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48697232E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06791901E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06791901E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06791889E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.42732864E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.42732864E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.42732864E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.42732864E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.14244363E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.14244363E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.14244363E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.14244363E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.75752575E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.75752575E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.07361264E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.58563379E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.56906106E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.63771613E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.08329805E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.63771613E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.08329805E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.34140355E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.46437046E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.46437046E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.52630819E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     9.71020785E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     9.71020785E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     9.71009622E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.17291615E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.81867613E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.17291615E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.81867613E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.70482310E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.46384238E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.46384238E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.19101426E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.29209435E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.29209435E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.29209437E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.18678194E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.18678194E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.18678194E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.18678194E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.95588860E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.95588860E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.95588860E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.95588860E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.83505686E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.83505686E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.89356156E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.06288966E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.80427431E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.10798265E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.45189706E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.45189706E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.65535477E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.78719550E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.04883884E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.84394819E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.84394819E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.85536535E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.85536535E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03142836E-03   # h decays
#          BR         NDA      ID1       ID2
     5.84019519E-01    2           5        -5   # BR(h -> b       bb     )
     6.47802174E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29320149E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86089612E-04    2           3        -3   # BR(h -> s       sb     )
     2.11148557E-02    2           4        -4   # BR(h -> c       cb     )
     6.93330559E-02    2          21        21   # BR(h -> g       g      )
     2.40542230E-03    2          22        22   # BR(h -> gam     gam    )
     1.67254154E-03    2          22        23   # BR(h -> Z       gam    )
     2.27105182E-01    2          24       -24   # BR(h -> W+      W-     )
     2.88537962E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.57836263E+01   # H decays
#          BR         NDA      ID1       ID2
     3.73652120E-01    2           5        -5   # BR(H -> b       bb     )
     5.94369317E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10154594E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48576593E-04    2           3        -3   # BR(H -> s       sb     )
     6.97096062E-08    2           4        -4   # BR(H -> c       cb     )
     6.98635406E-03    2           6        -6   # BR(H -> t       tb     )
     7.31047976E-07    2          21        21   # BR(H -> g       g      )
     4.79784787E-09    2          22        22   # BR(H -> gam     gam    )
     1.80000718E-09    2          23        22   # BR(H -> Z       gam    )
     1.55313612E-06    2          24       -24   # BR(H -> W+      W-     )
     7.76030112E-07    2          23        23   # BR(H -> Z       Z      )
     6.45644975E-06    2          25        25   # BR(H -> h       h      )
     2.07163702E-24    2          36        36   # BR(H -> A       A      )
     4.03444945E-20    2          23        36   # BR(H -> Z       A      )
     1.87365973E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.48603317E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.48603317E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.46228248E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.40358451E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.63095345E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.08996072E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.59876682E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.00713963E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.59355359E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.24921840E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.65210158E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.94507620E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.52950487E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.52950487E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.47489981E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.57803103E+01   # A decays
#          BR         NDA      ID1       ID2
     3.73699656E-01    2           5        -5   # BR(A -> b       bb     )
     5.94406488E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10167572E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48632541E-04    2           3        -3   # BR(A -> s       sb     )
     7.01755419E-08    2           4        -4   # BR(A -> c       cb     )
     7.00126962E-03    2           6        -6   # BR(A -> t       tb     )
     1.43858332E-05    2          21        21   # BR(A -> g       g      )
     7.17428147E-08    2          22        22   # BR(A -> gam     gam    )
     1.58176769E-08    2          23        22   # BR(A -> Z       gam    )
     1.54993508E-06    2          23        25   # BR(A -> Z       h      )
     1.95122625E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.48560078E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.48560078E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.20772334E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.54850659E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.45556853E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.41100517E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.34637977E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.72555497E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.71873294E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.59049702E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.58840936E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.57680028E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.57680028E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.61607685E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.04514047E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.90524633E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08795043E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.86888666E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18286477E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43353320E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.84389517E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.54110795E-06    2          24        25   # BR(H+ -> W+      h      )
     2.13846290E-14    2          24        36   # BR(H+ -> W+      A      )
     4.28733172E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.21368946E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.68104643E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.47796662E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.70474809E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.47342758E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.20886912E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.08111194E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.08952658E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
