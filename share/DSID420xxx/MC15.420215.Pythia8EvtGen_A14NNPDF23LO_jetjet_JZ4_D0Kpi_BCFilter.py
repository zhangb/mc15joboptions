# JO for Pythia 8 jet jet JZ4 slice

evgenConfig.description = "Dijet events slice JZ4 with the A14 NNPDF23 LO tune and DzeroFilter"
evgenConfig.process = "pp to jetjet"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:hardccbar = on"]
genSeq.Pythia8.Commands += ["HardQCD:hardbbbar = on"]
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = 150."]

include("MC15JobOptions/JetFilter_JZ4R04.py")

include("MC15JobOptions/DzeroFilter.py")

genSeq.EvtInclusiveDecay.userDecayFile = "D02Kpi.DEC"
evgenConfig.auxfiles += [ 'D02Kpi.DEC']

evgenConfig.minevents = 500
