
# EVGEN configuration
LQmass = 1050
evgenConfig.description = "Pythia 8 Pair produced scalar leptoquarks (M = "+str(LQmass)+" GeV) to btaubtau channel, A14 tune and NNPDF23LO PDF"
evgenConfig.contact     = ["Katharine Leney <katharine.leney@cern.ch>"]
evgenConfig.keywords    = [ 'exotic', 'BSM', 'leptoquark', 'scalar', 'tau' ]
evgenConfig.process     = "pp>LQ3LQ3>btaubtau"

      
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
      
genSeq.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",
                            "LeptoQuark:qqbar2LQLQbar = on",
                            "LeptoQuark:kCoup = 0.01",                           
                            "42:0:products = 5 15",
                            "42:m0 = "+ str(LQmass),
                            "42:mMin ="+ str(LQmass-200),
                            "42:mMax ="+ str(LQmass+200)]



